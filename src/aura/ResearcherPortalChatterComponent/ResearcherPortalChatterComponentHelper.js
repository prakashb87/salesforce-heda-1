({
       // Function to fetch data from server called in initial loading of page
        fetchComments : function(component, event, helper, loggedInUser) {
        // Assign server method to action variable
        var action = component.get("c.getChatterComments");
        // Getting the projectId from page
        var projectId = component.get("v.projectId");;
        // Setting parameters for server method
        console.log('projectId', projectId)
        action.setParams({
            'projectId': projectId
        });
        // Callback function to get the response
        action.setCallback(this, function(response) {
            // Getting the response state
            console.log('response', response)
            var state = response.getState();
            console.log('state', state)
            // Check if response state is success
            if(state === 'SUCCESS') {
                var commentList = []
                // Getting the list of comments from response and storing in js variable
                var result = response.getReturnValue();
                console.log(result)
                // Set the list attribute in component with the value returned by function
                result.forEach(function(item, i){
                   if(item.comment){
                       result[i].comment = helper.getFormatedComment(item.comment)
                   }
                    if(item.timeStamp){
                        result[i].formatedDate = helper.getFormatedDate(item.timeStamp)
                    }
                    if(item.userRole){
                        result[i].role = helper.getUserRole(item.userRole)
                    }
                    result[i].owner = item.userId === loggedInUser ? true:false
                    if(item.feedComments && item.feedComments.length>0){
                       item.feedComments.forEach(function(nestedFeed, index){
                        	let nestedReply = nestedFeed.feedComment
                            if(nestedReply.comment){
                                result[i].feedComments[index].feedComment.comment = helper.getFormatedComment(nestedReply.comment)
                            }
                           	if(nestedReply.timeStamp){
                                result[i].feedComments[index].feedComment.formatedDate = helper.getFormatedDate(nestedReply.timeStamp)
                            }
                           if(nestedReply.userRole){
                               result[i].feedComments[index].feedComment.role = helper.getUserRole(nestedReply.userRole)
                           }
                           result[i].feedComments[index].feedComment.owner = nestedReply.userId === loggedInUser ? true:false
                    	}) 
                    }
                })
                var commentList = result
                console.log('commentList', commentList)
                component.set("v.commentList",commentList);
                component.set("v.commentListLength",commentList.length)
                if(result && result.length>5){
                    var visibleCommentList = result.slice(0,5)
                    component.set("v.showMoreChatter",true);
                    component.set("v.visibleCommentList",visibleCommentList);
                    component.set("v.visibleCommentListLength",visibleCommentList.length)
                    if(result.length == visibleCommentList.length){
                        component.set("v.showMoreChatter",false);
                    }
                } else {
                    component.set("v.showMoreChatter",false);
                    component.set("v.visibleCommentList",result);
                    component.set("v.visibleCommentListLength",result.length)
                }
            }
            else {
                // Show an alert if the state is incomplete or error
                alert('Error in getting data');
            }
        });
        // Adding the action variable to the global action queue
        $A.enqueueAction(action);
    },
    getFormatedDate:function(timeStamp){
        var monthNames = ["Jan", "Feb", "Mar", "Apr", "May", "Jun","Jul", "Aug", "Sep", "Oct", "Nov", "Dec"];
        var dateExtract = timeStamp.substr(0,10);
        var timeExtract = timeStamp.substr(11);
        var t = new Date(dateExtract);
        var formatedDate =  t.getDate()+' '+monthNames[t.getMonth()]+' '+t.getFullYear();
        var formatedTime = timeExtract ? timeExtract.substr(0,5):''
        var settingAMPM = formatedTime? (Number(formatedTime.split(':')[0])>=12 ? 'PM':'AM'):'' 
        return `${formatedDate} ${formatedTime}${settingAMPM}`
    },
    getIndex:function(ID){
        var index
        if(ID && ID.split('-') && ID.split('-').length>0){
            index = ID.split('-')[0]
        }
        return index
    },
    getFormatedComment:function(html){
        var temporalDivElement = document.createElement("div");
        temporalDivElement.innerHTML = html;
        return temporalDivElement.textContent || temporalDivElement.innerText || "";
    },
    hidePopover:function(event){
        event.stopPropagation() 
        document.querySelectorAll('.chatter-delete-popup').forEach(function(el) {
            if(!(el.classList.contains("slds-hide"))){
                el.classList.add("slds-hide")
            }
        })
    },
    deleteFeed:function(component, event, helperFunc, feedId,feedItemId,loggedInUser, type){
        var feedAction = component.get("c.deleteChatterPost");
        feedAction.setParams({
            'feedId':feedId,
            'idType':type,
            'feedItemId':feedItemId
        });
        feedAction.setCallback(this, function(a) {
            var state = a.getState();
            if (state == "SUCCESS") {
                 var result = a.getReturnValue();  
               helperFunc.fetchComments(component, event, helperFunc, loggedInUser); 
            } else if (state == "ERROR") {
                alert('Error in calling server side action');
            }
        });      
        $A.enqueueAction(feedAction);
    },
    loadMoreComments:function(component, event){
        var totalComments = component.get("v.commentList");
        var visibleCommentList = component.get("v.visibleCommentList");
        if(totalComments.length > visibleCommentList.length){
            var newLength = visibleCommentList.length + 5
            var newList = totalComments.slice(0,newLength)
            component.set("v.visibleCommentList", newList);
            component.set("v.showMoreChatter",true);
            component.set("v.visibleCommentListLength",newList.length)
            if(totalComments.length == newList.length){
                component.set("v.showMoreChatter",false);
            }
        } else {
            component.set("v.visibleCommentList", totalComments);   
            component.set("v.showMoreChatter",false);
            component.set("v.visibleCommentListLength",totalComments.length)
       }
    },
    getUserRole:function(role){
        let result =""
        if(role && ((role.toUpperCase() == 'CHIEF INVESTIGATOR') || (role.toUpperCase() == 'CI'))){
            result = $A.get("$Label.c.chief_investigator")
        } else if(role && role.toUpperCase().indexOf('CHIEF INVESTIGATOR')>-1){
            result = $A.get("$Label.c.chief_investigator")
        } else if(role && role.toUpperCase() == 'CI DELEGATE'){
            result = $A.get("$Label.c.Chief_Investigator_Delegate")
        }
        return result
    },
})