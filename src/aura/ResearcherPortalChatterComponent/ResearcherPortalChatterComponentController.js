({
    // Function called on initial page loading to get contact list from server
    getCommentList : function(component, event, helper) {
        // Helper function - fetchComments called for interaction with server
        var fetchUserAction = component.get("c.getLoggedInuserId");
        fetchUserAction.setCallback(this, function(response) {
            var state = response.getState();
            if (state === "SUCCESS") {
                var loggedInUser = response.getReturnValue();
                // set current user information on userInfo attribute
                component.set("v.loggedInUser", loggedInUser);
                helper.fetchComments(component, event, helper, loggedInUser);
            } else {
            	helper.fetchComments(component, event, helper);
            }
        });
        $A.enqueueAction(fetchUserAction);
       // helper.fetchComments(component, event, helper);
        window.addEventListener('click',helper.hidePopover);
       
    },
	onCommentChange : function (component, event, helper) {
       if(event.target.value === ''){
            component.set("v.isCommentBoxEmpty", true);
            component.set("v.commentBoxValue", event.target.value);
        } else {
            component.set("v.isCommentBoxEmpty", false);
            component.set("v.commentBoxValue", event.target.value);
        }
    },
    onReplyCommentChange : function (component, event, helper) {
        console.log(event.target.id)
        var index = helper.getIndex(event.target.id)
        var id = event.target.id
         if(event.target.value === ''){
            component.set("v.isReplyCommentBoxEmpty", true);
            $('#'+index+'-replyAddCommentBtn').attr("disabled", "disabled");
        } else {
            component.set("v.isReplyCommentBoxEmpty", false);
            $('#'+index+'-replyAddCommentBtn').removeAttr("disabled", "disabled");
        }
    },
    clearTextarea: function (component, event, helper) {
        console.log(event)
        component.set("v.commentBoxValue", '');
        component.set("v.isCommentBoxEmpty", true);
    },
    cancelReplyText: function (component, event, helper) {
        var id = event.target.dataset.index;
        $('#'+id+'-chatterReplyCommentBox').val('')
        $('#'+id+'-replyIndex').removeClass("show");  
        $('#'+id+'-replyIndex').addClass("hide"); 
         $('#'+id+'-replyAddCommentBtn').attr("disabled", "disabled");
    },
    openReplyCommentBox: function (component, event, helper) {
        var id = event.target.id
        $('#'+id+'-chatterReplyCommentBox').val('')
         $('#'+id+'-replyIndex').addClass("show");
         $('#'+id+'-replyIndex').removeClass("hide");
        $('#'+id+'-chatterReplyCommentBox').focus()
    },
    onCommentBoxSubmit: function (component, event, helper) {
        var projectId = component.get("v.projectId");
        var commentBoxValue = component.get("v.commentBoxValue");
    	var action = component.get("c.createFeedItem");
        var loggedInUser = component.get("v.loggedInUser");
        action.setParams({
            'chatterComment':commentBoxValue,
            'recordIdfromcontroller':projectId
        });

        //Setting the Callback
        action.setCallback(this, function(a) {
            component.set("v.commentBoxValue", '');
            component.set("v.isCommentBoxEmpty", true);
            //get the response state 
            var state = a.getState();
            //check if result is successfull
            if (state == "SUCCESS") {
                 var result = a.getReturnValue();
               helper.fetchComments(component, event, helper, loggedInUser);             
            } else if (state == "ERROR") {
                alert('Error in calling server side action');
            }
        });

        //adds the server-side action to the queue        
        $A.enqueueAction(action);
    },
     onReplyCommentBoxSubmit: function (component, event, helper) {
         var index = helper.getIndex(event.target.id)
        var replyComment = $('#'+index+'-chatterReplyCommentBox').val();
        var feedId = $('#'+index+'-replyAddCommentBtn').attr("data-feedid");
    	var feedAction = component.get("c.createCommentForFeed");
        var loggedInUser = component.get("v.loggedInUser");
        feedAction.setParams({
            'feedId':feedId,
            'feedComment':replyComment
        });

        //Setting the Callback
        feedAction.setCallback(this, function(a) {
            //get the response state
             $('#'+index+'-chatterReplyCommentBox').val('')
			 $('#'+index+'-replyIndex').removeClass("show");  
             $('#'+index+'-replyIndex').addClass("hide"); 
            var state = a.getState();
            //check if result is successfull
            if (state == "SUCCESS") {
                 var result = a.getReturnValue();
                console.log('result', result)
               console.log('success reply posted')   
               helper.fetchComments(component, event, helper, loggedInUser); 
            } else if (state == "ERROR") {
                alert('Error in calling server side action');
            }
        });

        //adds the server-side action to the queue        
        $A.enqueueAction(feedAction);
    },
    deleteFeedClicked: function (component, event, helper) {
        event.stopPropagation() 
        helper.hidePopover(event)
        console.log(event.target.dataset.feedid)
        var classList = event.target.nextElementSibling.classList
        classList.remove('slds-hide')
        
    },
    cancelPopover:function (component, event, helper) {
        event.stopPropagation() 
        var index = helper.getIndex(event.target.id)
        $('#'+index+'-deleteSection').addClass('slds-hide')
        console.log(event)
    },
    cancelReplyPopover:function (component, event, helper) {
        event.stopPropagation() 
        var index = helper.getIndex(event.target.id)
        $('#'+index+'-deleteReplySection').addClass('slds-hide')
        console.log(event)
    },
    stopPropagation:function(component, event, helper){
       event.stopPropagation()  
    },
    deleteFeed: function (component, event, helper) {
        var loggedInUser = component.get("v.loggedInUser");
         var index = helper.getIndex(event.target.id)
        $('#'+index+'-deleteSection').addClass('slds-hide')
        var feedId = event.target.dataset.feedid;
        var type = 'feed'
        helper.deleteFeed(component, event, helper, feedId,'',loggedInUser, type)
    },
    deleteReplyFeed: function (component, event, helper) {
        var loggedInUser = component.get("v.loggedInUser");
         var index = helper.getIndex(event.target.id)
        $('#'+index+'-deleteReplySection').addClass('slds-hide')
        var feedItemId = event.target.dataset.feeditemid;
        var feedId = event.target.dataset.feedid;
        var type = 'feedComment'
       helper.deleteFeed(component, event, helper, feedId,feedItemId,loggedInUser, type)
    },
    loadMoreChatter: function (component, event, helper) {
        helper.loadMoreComments(component, event)
    }
    
    
})