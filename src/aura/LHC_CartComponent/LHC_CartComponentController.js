({
    NavigateToTandC : function(component,event,helper){
        component.set("v.Spinner", true); 
        console.log('promoCode12--->');
        //var promoCode = component.find("promocode").get("v.value");
        var promoCode = component.get("v.promocode");
        component.set("v.promocode",promoCode);
        console.log('promoCode--->'+component.get("v.promocode"));  
        //Added for ECB-5766 : On checkout,to validate if the promo code applied is still valid or not
        var action = component.get('c.validatePromocode');
        action.setParams({
            "promoCode" : promoCode,
        });
        action.setCallback(this, function(response) {
            
            var result = response.getReturnValue();
            //  var state = response.getState();
            console.log(result.Status);
            console.log(result.TotalPrice);
            console.log(result);
            if(result.isValid == false) { 
                console.log('error');
                component.set("v.Spinner", false);
                component.set("v.message","Invalid code. The entered promo code is invalid, or is no longer available"); //ECB-5764 modified message
                component.set("v.isOpen", true); 
                component.set("v.promocode","");
                component.find("promocode").set("v.value","");
                helper.updateCartDetails(component);
            } else if(result.isValid == true){
            	component.set("v.Spinner", false);
                //ECB-5015: Starts
                component.set('v.displayNextButton',false); 
                helper.navigate(component,event,helper);
                component.set('v.displayNextButton',true);
                //ECB-5015: Ends  
            }
        });
        
        $A.enqueueAction(action);   
    },
    
    //ECB -2463 Method for delete functionality
    delete : function(component, event, helper) {
    		
           var name = event.currentTarget.id;
           console.log(name);
           //ECB-4506:Added to handle PromoCode association
    	   var courseList = component.get("v.ProductBasket").Courses;
           console.log(courseList.length);
           var totalPrice = 0; 
           if(!$A.util.isEmpty(courseList)){
                for(var i=0;i<courseList.length;i++){
                    if(courseList[i].CourseName != name){
                    	console.log(courseList[i].Price);
                    	totalPrice = totalPrice+Number(courseList[i].Price); 
                    	console.log(totalPrice);
                    }
                    
                } 
            }
           console.log('t::::'+totalPrice);
            
            var action = component.get('c.deleteProduct');
                action.setParams({
                "courseName" : name
            });
           
		   	
           action.setCallback(this, function(response) {
            
            var result = response.getReturnValue();
            var state = response.getState();
            
            if (state === "SUCCESS") {
                
                // in this result you should get the list of product after delete
                component.set('v.ProductBasket', result);
                //ECB-4506
                var cList = result.Courses;
                if(!$A.util.isEmpty(cList)){
                   var disAmt = Number(component.get("v.ProductBasket.TotalPrice")) - totalPrice;
            	   component.set("v.promoCodeDiscountAmt",disAmt.toFixed(2));  
                }else{
                   $A.util.addClass(component.find('promodiv'),'hideDiv'); 
                   $A.util.removeClass(component.find('promodiv'),'showDiv');  
                   var promoCode = component.find("promocode").set("v.value","");
                }
                
            }
            else {
                var errors = response.getError();
                console.log(errors);
            }
            //ECB -3783  Next button is enabled after deleting the product in the cart. 
            var finalProdBasket = component.get("v.ProductBasket");
            if($A.util.isEmpty(component.get("v.ProductBasket.Courses"))){
                component.set("v.displayNextButton",true); 
                
            }else{
                component.set("v.displayNextButton",false);
            }
            
            console.log('displayNextButton:::'+component.get("v.displayNextButton"));
        });
        $A.enqueueAction(action);

	},
    
    loadData: function(component, event, helper) {
        
        var status = component.searchParam('Status');
        var Message = component.searchParam("Message");
        var Ref = component.searchParam("Ref");
        var MPB = component.searchParam("MPB");
        console.log('Message:::::'+Message);
        console.log('status:::::'+status);
        
        //component.set("v.displayNextButton",true);
        if(status=='Failure')
        {
            var url = window.location.href;
            console.log('url='+ url);
            if(url.toString().includes("Ref")&& url.toString().includes("MPB"))
            {
                console.log('Testing...')
                component.set("v.message","Your payment was unsuccessful, please try again."); 
                
            }
            else if(history.length===0)
            {
                
            }
            else
            {
                component.set("v.message",Message); 
            }
            
            
        }
        // ECB -3762 Error Message - Rishbah :Ends
        var action = component.get('c.getresult');
        console.log('course:::'+component.get("v.ProductBasket.TotalPrice"));
       // var currentBasketTotal = Number(component.get("v.ProductBasket.TotalPrice"));
        
        var disAmt;
        action.setCallback(this, function(response) {
             //  store state of response
            var state = response.getState();
            //console.log(JSON.parse(response.getReturnValue()));
            if (state === "SUCCESS") {
                var courseList = response.getReturnValue().Courses;
                /*console.log(courseList.length);
                console.log('Price-->'+courseList[0].Price);
                console.log('Price-->'+courseList[0].Fees);*/
                
                
                if(!$A.util.isEmpty(courseList)){
                    var priceBeforeDiscount = 0;
                    for(var i=0;i<courseList.length;i++){
                        /*if(courseList[i].PromoCodeDiscount!=null && courseList[i].PromoCodeDiscountType=='Amount'){
                            priceBeforeDiscount = priceBeforeDiscount + Number(courseList[i].Price) + Number(courseList[i].PromoCodeDiscount);
                            console.log('dis::::'+priceBeforeDiscount); 
                        }else if(courseList[i].PromoCodeDiscount!=null && courseList[i].PromoCodeDiscountType=='Percentage'){
                            var amt = (Number(courseList[i].PromoCodeDiscount)/100)*Number(courseList[i].Price);
                            priceBeforeDiscount = priceBeforeDiscount + courseList[i].Price + amt;
                        }*/
                        var promocode;
                        if(courseList[i].PromoCode!=null){
                           promocode = courseList[i].PromoCode; 
                        }
                        priceBeforeDiscount = priceBeforeDiscount + Number(courseList[i].Price);
                        console.log('dis::::'+priceBeforeDiscount);  

                    }
                    
                }
                component.set("v.promocode",promocode);
                console.log('dis::::'+priceBeforeDiscount);
                var promoAmt = (priceBeforeDiscount - response.getReturnValue().TotalPrice).toFixed(2);
                if(promoAmt > 0){
                  component.set("v.promoCodeDiscountAmt",promoAmt);
                  $A.util.addClass(component.find('promodiv'),'showDiv');
                  $A.util.removeClass(component.find('promodiv'),'hideDiv');  
                }
                
                
                component.set("v.ProductBasket",response.getReturnValue());
                
            }
            //ECB -3783  Next button is enabled after deleting the product in the cart. 
            var finalProdBasket = component.get("v.ProductBasket");
            if($A.util.isEmpty(component.get("v.ProductBasket.Courses"))){
                component.set("v.displayNextButton",true); 
                
            }else{
                component.set("v.displayNextButton",false);
            }
            
            console.log('displayNextButton:::'+component.get("v.displayNextButton"));
        });
        helper.jsLoad(component);
        //For Delivery Mode
        helper.getDeliveryMode(component);
        
        $A.enqueueAction(action);
    }, 
        keepShopping : function(component, event, helper) {
            var referrer = localStorage.getItem("marketplace-referrer");
            console.log('referrer: ' + referrer);
            
            var action = component.get('c.getShoppingURL');
           
            action.setCallback(this, function(response) {
                var state = response.getState();
                if (state === "SUCCESS") {
                   if(response.getReturnValue() !=null){
                        var shoppingURL = response.getReturnValue();
                        //window.open(shoppingURL,'_Top'); 
                        self.location = shoppingURL;
                    }                    
                }    
            });  
       
            $A.enqueueAction(action);
            
        },    
            
            //Changes For 4189
            openModel: function(component, event, helper) {
                // for Display Model,set the "isOpen" attribute to "true"
                component.set("v.isOpen", true);
                
            },
                
                closeModel: function(component, event, helper) {
                    
                    component.set("v.isOpen", false);
                                      
                }, 
                    
                    //ECB-3811
                    searchParam : function(component, event, helper) {
                        
                        var params = event.getParam('arguments');
                        var name;
                        if(params){
                            name = params.param1;
                        }
                        console.log('name:::'+name);
                        name = name.replace(/[\[]/, '\\[').replace(/[\]]/, '\\]');
                        var regex = new RegExp('[\\?&]' + name + '=([^&#]*)');
                        var results = regex.exec(location.search);
                        return results === null ? '' : decodeURIComponent(results[1].replace(/\+/g, ' '));
                        
                    },
                                                
                        termsCondition :function(component, event, helper){
                            console.log('tc:::');
                            
                            var cname = event.currentTarget.getAttribute('data-coursename');
                            //var cname = src.get("v.data-course");
                            //console.log('name:::'+cname);
                            console.log('name:::'+cname);
                            var action = component.get('c.getTCURL');
                            action.setCallback(this, function(response) {
                                var state = response.getState();
                                if (state === "SUCCESS") {
                                    if(response.getReturnValue() !=null){
                                        var shoppingURL = response.getReturnValue()+'?courseName='+cname; 
                                        //console.log('shoppingURL:::'+shoppingURL);
                                        window.open(shoppingURL,'_self');  
                                    }
                                    
                                }    
                            });
                            
                            $A.enqueueAction(action);
                            
                        },       
                            updateproduct :function(component, event, helper){
                                component.set("v.Spinner", true);
                                var src = event.getSource();
                                var itemname = src.get("v.value");
                                console.log('item name ---> '+itemname);
                                var cname = src.get("v.class");
                                console.log('course--->'+cname);
                                var action = component.get('c.updatedProduct');
                                action.setParams({
                                    "deliveryMode" : itemname,
                                    "courseName" : cname
                                });
                                
                                action.setCallback(this, function(response) {
                                    
                                    var result = response.getReturnValue();
                                    var state = response.getState();
                                    
                                    if (state === "SUCCESS") {
                                        component.set('v.ProductBasket', result); 
                                        component.set("v.Spinner", false);
                                    }
                                    else {
                                        var errors = response.getError();
                                        console.log(errors);
                                        component.set("v.Spinner", false);
                                    }
                                });
                                $A.enqueueAction(action);
                                
                            },
       applyPromoCode : function(component, event, helper) {
           component.set("v.Spinner", true); 
            console.log('promoCode12--->');
            var promoCode = component.find("promocode").get("v.value");
            component.set("v.promocode",promoCode);
            console.log('promoCode--->'+component.get("v.promocode"));
            
            
             var action = component.get('c.getAppliedPromocodePrice');
             action.setParams({
                "promoCode" : promoCode,
            });
            action.setCallback(this, function(response) {
                 	
                var result = response.getReturnValue();
                var state = response.getState();
                console.log(result.Status);
                console.log(result.TotalPrice);
                console.log(result);
                if(result.Status =="Pass") {
                    helper.getUpdatedBasketDetails(component);
                }else if(result.Status == "Fail"){
                    console.log('error');
                    component.set("v.Spinner", false);
                    component.set("v.headermessage","PROMO CODES - INVALID");
                    component.set("v.message","Invalid code. The entered promo code is invalid, or is no longer available"); //ECB-5764 modified message
                    component.set("v.isOpen", true);                    
                    component.set("v.promocode","");
                    component.find("promocode").set("v.value","");
                    //added for 5766
                    helper.updateCartDetails(component);
                   /* $A.util.addClass(component.find('promodiv'),'hideDiv');
                    $A.util.removeClass(component.find('promodiv'),'showDiv');
                    
                    var courseList = component.get("v.ProductBasket").Courses;
                        console.log(courseList.length);
                        var totalPrice = 0; 
                        if(!$A.util.isEmpty(courseList)){
                            for(var i=0;i<courseList.length;i++){
                                    console.log(courseList[i].Price);
                                    totalPrice = totalPrice+Number(courseList[i].Price); 
                                    console.log(totalPrice);
                            }
                                
                       } 
            
            			console.log('tot::::'+totalPrice); 
                        component.set("v.ProductBasket.TotalPrice",totalPrice);*/
                }     
            });
            
			$A.enqueueAction(action);  
           
        } ,
            
            getPrice :function(component,event){
                var params = event.getParam('arguments');
                if (params) {
                    var param1 = params.param1;
                    var param2 = params.param2;
                    var price = param1 + param2;
                    component.set("v.price",price);
                    // add your code here
                }
                
           },                             
})
