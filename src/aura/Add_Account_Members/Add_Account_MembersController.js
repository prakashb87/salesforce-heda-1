({
	handleClick : function(component, event, helper) {
		var searchVal = component.get("v.searchText");
        console.log('Searced Account'+searchVal);
        if (searchVal == '' || searchVal == null) {
            alert('Please enter a account name');
        } else {
            helper.SearchHelper(component, event);
        }
	},
    
    checkboxSelect: function(component, event, helper) {
        var selectedRec = event.getSource().get("v.value");
        var getSelectedNumber = component.get("v.selectedCount");
        if (selectedRec == true) {
            getSelectedNumber++;
        } else {
            getSelectedNumber--;
        }
        component.set("v.selectedCount", getSelectedNumber);
    },
   
    selectAll: function(component, event, helper) {
        var selectedHeaderCheck = event.getSource().get("v.value");
        var getAllId = component.find("checkbox");
        if(component.get("v.searchResult") == null || component.get("v.searchText") == null || component.get("v.searchText") == ""){
            alert('Please enter a account name');
        }else{
            if(! Array.isArray(getAllId)){
                if(selectedHeaderCheck == true){ 
                    component.find("checkbox").set("v.value", true);
                    component.set("v.selectedCount", 1);
                }else{
                    component.find("checkbox").set("v.value", false);
                    component.set("v.selectedCount", 0);
                }
            }else{
                if (selectedHeaderCheck == true) {
                    for (var i = 0; i < getAllId.length; i++) {
                        component.find("checkbox")[i].set("v.value", true);
                        component.set("v.selectedCount", getAllId.length);
                    }
                } else {
                    for (var i = 0; i < getAllId.length; i++) {
                        component.find("checkbox")[i].set("v.value", false);
                        component.set("v.selectedCount", 0);
                    }
                } 
            }      
        }
    },
    
    updateSelected: function(component, event, helper) {
        var updateId = [];
        var getAllId = component.find("checkbox");
        if(component.get("v.searchResult") == null || component.get("v.searchText") == null || component.get("v.searchText") == ""){
            alert('Please enter a account name');
        }else{
            if(! Array.isArray(getAllId)){
                if (getAllId.get("v.value") == true) {
                    updateId.push(getAllId.get("v.text"));
                }
            }else{
                for (var i = 0; i < getAllId.length; i++) {
                    if (getAllId[i].get("v.value") == true) {
                        updateId.push(getAllId[i].get("v.text"));
                    }
                }
            }
        }
        console.log('Selected Ids-->'+updateId);
        
        helper.deleteSelectedHelper(component, event, updateId);
        
    },
})