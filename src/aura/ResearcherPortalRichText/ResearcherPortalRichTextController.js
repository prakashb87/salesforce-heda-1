({
    doRender: function(component, event, helper) {
        var root = component.find('rich_text_root').getElement();
        root.innerHTML = component.get('v.value');
        
        helper.initAccordions(root);
    }
});