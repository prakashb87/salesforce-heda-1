({

    onInit:function(component){
        console.log("dropdown onInit")
        let dropdownId = component.get("v.dropdownId")
        window.addEventListener('click', function(event){
            if (!event.target.matches('.dropbtn')) {
                var dropdowns = document.getElementsByClassName("rp-dd-dropdown-content");
                var i;
                for (i = 0; i < dropdowns.length; i++) {
                    var openDropdown = dropdowns[i];
                    if (!openDropdown.classList.contains('hide')) {
                        openDropdown.classList.add('hide');
                    }
                }
            }
        });

        let labelInline = component.get("v.labelInline")
        let widthClass = component.get("v.widthClass")
        let classes =""
        if(labelInline && widthClass){
            classes= 'relative inline-block '+widthClass
        } else if(labelInline){
            classes= 'relative inline-block defaultWidth250'
        } else if(widthClass){
            classes= 'relative '+widthClass
        } else {
            classes = 'relative defaultWidth250'
        }
        component.set("v.componentBoxCss", classes)
        setTimeout(function(){
            let dropdownData = component.get("v.dropdownData")
            let defaultSelected = component.get("v.defaultSelected")
            if(defaultSelected){
                component.set('v.selectedValue',defaultSelected)
            }
            console.log(dropdownData)
            component.set("v.orignalDropownDataCopy", dropdownData)
        },10)
    },
    dropDownFocusFunction:function(component, event, helper){
        let dropdownSearchable = component.get('v.dropdownSearchable')
        if(dropdownSearchable){
            let selectedValue = component.get('v.selectedValue')
            let orignalDropownDataCopy = component.get("v.orignalDropownDataCopy")
            if(selectedValue==""){
                component.set("v.dropdownData", orignalDropownDataCopy)
            }
        }
    },
    dropDownBlurFunction:function(component, event, helper){
        let dropdownSearchable = component.get('v.dropdownSearchable')
        if(dropdownSearchable){
            let id = component.get("v.dropdownId")
            let selectedValue = component.get('v.selectedValue')
            if(selectedValue==""){
                $('#'+id+'-inputbox').val('')
            }
        }
    },
    toggleDropdown: function(component, event, helper) {
        let dropdownId = component.get("v.dropdownId")
        let id = dropdownId+'-List'
        document.querySelectorAll('.rp-dropdown').forEach((e)=>{
            if(e.id==id){
                console.log('same')
            } else {
                $('#'+e.id).addClass("hide")
            }
        })
        $('#'+id).toggleClass("hide")
        /*if($('#'+id+'-List').hasClass('hide')){
            $('#'+dropdownId+'-arrowIcon').addClass("fa fa-angle-down");
            $('#'+dropdownId+'-arrowIcon').removeClass("fa fa-angle-up");
        } else {
            $('#'+dropdownId+'-arrowIcon').removeClass("fa fa-angle-down");
            $('#'+dropdownId+'-arrowIcon').addClass("fa fa-angle-up");
        }*/

        //document.getElementById(id).classList.toggle("show");
    },
    selectTopic: function(component, event, helper) {
        event.preventDefault()
        let isJson = component.get("v.isJson")
        if(isJson){
            let dropdownData = component.get("v.dropdownData")
            let selectedId = event.target.getAttribute('data-value')
            let filterData = dropdownData.filter(function(item){
                return item.id == selectedId
            })
            if(filterData && filterData[0] && filterData[0].str){
                component.set('v.selectedValue', filterData[0].str)
            } else{
                component.set('v.selectedValue', selectedValue)
            }

        } else {
            let dropdownId = component.get("v.dropdownId")
            let selectedValue = event.target.getAttribute('data-value')
            component.set('v.selectedValue', selectedValue)
            var dropdownCallback = component.get("v.dropdownCallback");
            dropdownCallback.parentDropdownMethod(dropdownId, selectedValue);
        }
    },
    CheckboxSelect: function(component, event, helper) {
        event.stopPropagation()
        let dropdownId = component.get("v.dropdownId")
        let id = dropdownId+'-List'
        var selectedList = [];
        var checkBoxes = $("#"+id+" input");
        for (var i = 0; i < checkBoxes.length; i++) {
            if (checkBoxes[i].checked) {
                var value = checkBoxes[i].name;
                selectedList.push(value);
            }
        }
        component.set("v.selstatusListCount", selectedList.length);
        var dropdownCallback = component.get("v.dropdownCallback");
        dropdownCallback.parentDropdownMethod(dropdownId, selectedList);
    },
    filterForFunction:function(component, event, helper){
        let dropdownSearchable = component.get('v.dropdownSearchable')
        if(dropdownSearchable){
            let val=event.target.value ? event.target.value.toUpperCase() :''
            let dropdownData = component.get("v.dropdownData")
            let orignalDropownDataCopy = component.get("v.orignalDropownDataCopy")
            let isJson = component.get("v.isJson")

            console.log(orignalDropownDataCopy)
            console.log(dropdownData)
            if(val && val.trim().length>0){
                component.set("v.selectedValue", '')
                let filteredData=  orignalDropownDataCopy.filter(function(item){
                    let itemUpperCase=''
                    if(isJson){
                        itemUpperCase = item.one.toUpperCase()
                    } else {
                        itemUpperCase = item.toUpperCase()
                    }
                    return itemUpperCase.includes(val);


                })
                if(filteredData && filteredData.length>0){
                    component.set("v.dropdownData", filteredData)
                } else {
                    component.set("v.dropdownData", [])
                }

            } else {
                component.set("v.dropdownData", orignalDropownDataCopy)
            }
        }
    }

})