({
    // Method call to get all the list of RMIT public projects on load
    getRMITPublicProjects : function(component, event, helper) {
        if(window.localStorage){
            let PREV_URL = window.localStorage.getItem('PREV_URL');
            let searchCriteria = window.localStorage.getItem('SEARCH_CRITERIA');
            console.log('PREV_URL onload', PREV_URL)
            console.log('totalSessionStore onload', searchCriteria)
            window.localStorage.removeItem('PREV_URL');
            window.localStorage.removeItem('SEARCH_CRITERIA');
            if(searchCriteria && searchCriteria!='undefined' && PREV_URL == 'PROJECT_DETAIL'){
                var parseData = searchCriteria ? JSON.parse(searchCriteria):searchCriteria
                document.getElementById('search-input').value = parseData.searchText
                component.set("v.searchBoxValue", parseData.searchText)
                component.set("v.selectedImpactCodes", parseData.impactCodeDetailSearch)
                component.set("v.selectedForCodes", parseData.forCodeDetailSearch)
                helper.getFORCodes(component,event,helper, parseData.forCodeDetailSearch);
                helper.getImpactCategory(component,event,helper, parseData.impactCodeDetailSearch);
                component.set("v.selstatusListCount", parseData.forCodeDetailSearch.length);
                component.set("v.selstatusImpactListCount", parseData.impactCodeDetailSearch.length);
                if(parseData && parseData.tabSelected){
                    let selectedTabId = parseData.tabSelected
                    let tabId = selectedTabId == 'ALL' ? 'filter-all' : selectedTabId == 'RM' ? 'filter-R' : selectedTabId == 'UC' ? 'filter-U':'filter-all'
                    document.getElementById(tabId).click()
                    console.log(parseData)
                }  else {
                    setTimeout(function(){
                        helper.searchProject(component, event, helper);
                    },1000)
                }
            } else {
                helper.getRecordSizeLimit(component, event, helper);
                helper.getRMITPublicProjectsList(component,event,helper,"", "All");
                helper.getFORCodes(component,event,helper);
                helper.getImpactCategory(component,event,helper)
            }

        }
        component.set("v.pageCount",1);
        
        
        $('#createPjtBtnAllPjts').hide();
        helper.callApexServer(component,helper,"c.isCreateProjectApplicable",helper.successisCreateProjectApplicableAction,helper.errorAction)
    },
    // to navigate to create new project page on click of create project button
    goToCreateProjectPage: function() {
        var urlEvent = $A.get("e.force:navigateToURL");
        urlEvent.setParams({
            "url": "/create-new-project"
        });
        
        urlEvent.fire();
    },
    
    //Method call to filter data on click of All tab with any search data on input search box
    filterAllDataSet:function(component, event, helper){
        helper.filterTab(component, event, helper, "filter-all","ALL" );        
    },
    
    //Method call to filter data on click of RM tab with any search data on input search box
    filterRMDataSet:function(component, event, helper){
        helper.filterTab(component, event, helper, "filter-R","RM" );       
    },
    //Method call to filter data on click of User created tab with any search data on input search box
    filterUserDataSet:function(component, event, helper){
        helper.filterTab(component, event, helper, "filter-U","UC" );       
    },
    //Method call to clear the user entered search results
    clearSearch:function(component, event, helper){
        //helper.removeHighlightText(component, event, helper)
        component.set("v.dataSet", []);
        component.set("v.totalDataSet",[]);
        component.set("v.pageCount",1);
        component.set("v.prevPageCount", 0);
        
        var selectedTabId = document.querySelectorAll('.active-tab')[0].id;
        var selectedTab = (selectedTabId =='filter-R') ? 'RM': ((selectedTabId == 'filter-U') ?'UC' : 'ALL');
        var obj = {
                searchText : '',
                tabSelected : selectedTab,
                pageNo : component.get("v.pageCount"),
                forCodeDetailSearch : helper.resetFilter(component, event, helper, 'FOR_CODES'),
                impactCodeDetailSearch :helper.resetFilter(component, event, helper, 'IMPACT_CODES'),
            
            } 
        var jsonParams = JSON.stringify(obj);
        component.set("v.searchCriteria","");
        helper.getNextRMITPublicProjectsList(component,event,helper,jsonParams);
        $("#search-input").val("");
        component.set("v.searchBoxValue", '');
        $("#searchError").css("display","none");
        $("#search-input").css("border-color",  "");
    },
    //method call on click of search button
    searchHandler:function(component, event, helper){
        helper.searchProject(component, event, helper);
    },
    //on click of view more button, navigate to project details page
    showProjectDetail:function(component,event,helper){
        event.preventDefault();
        var index = event.target.id? helper.getIndex(event.target.id): event.target.parentElement.id?helper.getIndex(event.target.parentElement.id):''
        var dataset = component.get("v.dataSet");
        var recordId = dataset[index].recordId;
        let searchCriteria = component.get("v.searchCriteria")
        helper.setLocalStorage("SEARCH_CRITERIA", searchCriteria);
        helper.setLocalStorage("PREV_URL", 'PUBLIC_VIEW');
        var urlEvent = $A.get("e.force:navigateToURL");
        urlEvent.setParams({
            "url": '/Researcherportal/s/project-details?recordId='+recordId
        });
        urlEvent.fire();
    },
    
    //Search records when user types and clicks enter in the input box
    blockSpecialChar:function(component, event, helper){
        var char = String.fromCharCode(event.charCode || event.keyCode)
        var pattern = /^[A-Za-z0-9-,_.''"" ]+$/;
        if(event.target.value && event.target.value.trim() && event.target.value.trim().length>=2){
            $("#searchError").css("display","none");
            $("#search-input").css("border-color",  "");
        }
        if(!pattern.test(char)){
            event.returnValue = false;
        } 
        if (event.keyCode === 13) {
            helper.searchProject(component, event, helper);
        }
    },
    //Load next 7 records on click of load more button
    loadmoreHandler : function(component, event, helper){
        helper.removeHighlightText(component, event, helper)
        var hasmore = component.get("v.hasMore");
        var totalPageProjects = component.get("v.projectsLength");
        var offsetCount = component.get("v.offsetLimit");
        if(hasmore && totalPageProjects >= offsetCount){
            var selectedTabId = document.querySelectorAll('.active-tab')[0].id;
            var selectedTab = (selectedTabId =='filter-R') ? 'RM': ((selectedTabId == 'filter-U') ?'UC' : 'ALL');
            var serverPageCount = component.get("v.serverPageCount");
            serverPageCount++;
            component.set("v.serverPageCount", serverPageCount);
            let searchBoxValue = $('#search-input').val();
            let searchBoxTrimVal = searchBoxValue == "" ? searchBoxValue : searchBoxValue.trim();
            if(searchBoxTrimVal.length>2 || searchBoxTrimVal.length ==0){
                var obj = {
                    searchText : searchBoxTrimVal,
                    tabSelected : selectedTab,
                    pageNo : serverPageCount,
                    forCodeDetailSearch : component.get("v.selectedForCodes"),
                    impactCodeDetailSearch : component.get("v.selectedImpactCodes")
                } 
                var jsonParams = JSON.stringify(obj);
                component.set("v.searchCriteria",jsonParams);
                helper.getloadmoreRMITPublicProjectsList(component,event,helper,jsonParams);
            }          
        }   
        else{
              helper.updateDataSet(component, event, helper, true);
        }
    },
    // method call to toggle impact category dropdown and on edit based on data check the checkbox
    toggleDropdown: function(component, event, helper) {
        var targetCmp = event.currentTarget;        
        var targetId = targetCmp.id;        
        var type = targetCmp.dataset.type;        
        $('#' + targetId).val('');
        var topicDropdown = $('#' + type + 'Dropdown');
        if ((topicDropdown.css("display") === "none") || (topicDropdown.css("display") === "")) {
            topicDropdown.css("display", "block");
            helper.toggleArrowState('#' + type + 'Dropdown', '#arrow' + type);
            var inputId =""
            var dropdownId =""
            if(type == 'impactcategory'){
                inputId='#impactcategoryfilter'
         	    dropdownId="#impactcategoryDropdown" 
            } if(type == 'forcodes'){
                inputId='#forcodesfilter'
         	    dropdownId="#forcodesDropdown"
            }
             helper.filterList(component, event, helper, inputId, dropdownId)
        } else {
            topicDropdown.css("display", "none");
            helper.toggleArrowState('#' + type + 'Dropdown', '#arrow' + type);
        }
        
        $(document).click(component, function(e) {
            if ($(e.target).closest('#forcodesfilter').length === 0 && $(e.target).closest('#forcodesDropdown').length === 0 && $(e.target).closest('.input-group-btn').length === 0 && $('#forcodesDropdown').is(":visible")) {
                $('#forcodesDropdown').toggle(false);
                helper.toggleArrowState('#forcodesDropdown', '#arrowforcodes');
                var topic = $('#forcodesfilter').data('selected');
                $('#forcodesfilter').val(topic);
            }         
            else if ($(e.target).closest('#impactcategoryfilter').length === 0 && $(e.target).closest('#impactcategoryDropdown').length === 0 && $(e.target).closest('.input-group-btn').length === 0 && $('#impactcategoryDropdown').is(":visible")) {
                $('#impactcategoryDropdown').toggle(false);
                helper.toggleArrowState('#impactcategoryDropdown', '#arrowimpactcategory');
                var topic = $('#impactcategoryfilter').data('selected');
                $('#impactcategoryfilter').val(topic);
            }
            
        });
    },
    // Function call to collect all the selected data
    CheckboxForSelect: function(component, event, helper) {
        var selectedForList = [];
        var forCheckBoxes = $("#forcodesDropdown input");
        for (var i = 0; i < forCheckBoxes.length; i++) {
            if (forCheckBoxes[i].checked) {
                var value = (forCheckBoxes[i].name).toLowerCase().split("-")[0].trim();  
                selectedForList.push(value);
            }
        }
        component.set("v.selectedForCodes", selectedForList);
        component.set("v.selstatusListCount", selectedForList.length);
        if(selectedForList.length){
            $('input#forcodesfilter').attr('placeholder','')
        } else {
           $('input#forcodesfilter').attr('placeholder','No Selection')
        }
        
    },
    CheckboxForImpactSelect: function(component, event, helper) {
        var selectedImpactCodeList = [];
        var impactCheckBoxes = $("#impactcategoryDropdown input");
        for (var i = 0; i < impactCheckBoxes.length; i++) {
            if (impactCheckBoxes[i].checked) {
                var value = (impactCheckBoxes[i].name).toLowerCase().split("-")[0].trim();  
                selectedImpactCodeList.push(value);
            }
        }
        component.set("v.selectedImpactCodes", selectedImpactCodeList);
        component.set("v.selstatusImpactListCount", selectedImpactCodeList.length);
        if(selectedImpactCodeList.length){
            $('input#impactcategoryfilter').attr('placeholder','')
        } else {
           $('input#impactcategoryfilter').attr('placeholder','No Selection')
        }
    },
      //function call to display the list of data for dropdown and filter based on input data entered
    filterForFunction: function(component, event, helper) {
        var inputId='#'+event.target.id
        var dropdownId =""
        if(event.target.id == 'forcodesfilter'){
            inputId='#forcodesfilter'
         	dropdownId="#forcodesDropdown"   
        }
        if(event.target.id == 'impactcategoryfilter'){
            inputId='#impactcategoryfilter'
         	dropdownId="#impactcategoryDropdown" 
        }
        helper.filterList(component, event, helper, inputId, dropdownId)    
    },
});