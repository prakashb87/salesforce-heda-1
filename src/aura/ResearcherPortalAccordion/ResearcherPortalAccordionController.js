({
    onAccordionLoad : function(component, event, helper) {
        let id = component.get("v.accordionId")
        let parent = component.get("v.parent")
        $(document).ready(function(){
          $("#"+id+"_rp-accordion").click(function(){
            $("#"+id+"_rp-panel").slideToggle("slow");
            $("#"+id+"_rp-accordion").toggleClass('active')
            if(!$("#"+id+"_rp-accordion").hasClass('active')){
                parent.parentAccordionMethod('INACTIVE', id)
            } else {
                parent.parentAccordionMethod('ACTIVE', id)
            }
          });     
        });
	}
	/*onAccordionLoad : function(component, event, helper) {
    let id = component.get("v.accordionId")
    var acc = document.getElementById("#"+id+"_rp-accordion")
          acc.addEventListener("click", function() {
            this.classList.toggle("active");
            var panel = this.nextElementSibling;
            if (panel.style.maxHeight){
              panel.style.maxHeight = null;
            } else {
              panel.style.maxHeight = panel.scrollHeight + "px";
            } 
          });
	}*/
})