({
    doInit: function(component, event, helper) {
        if (component.get('v.contentBundle')) {
            var taxonomyBundles = component.get('v.contentBundle').taxonomyTagPaths;

            if (taxonomyBundles) {
                var tagPathsAttribute = taxonomyBundles.map(function(bundle) {
                    var tagPath = bundle.languageMapOfPaths.en_US;

                    return {
                        path: tagPath,
                        label: tagPath.substring(tagPath.lastIndexOf('/') + 1),
                    };
                });
                component.set('v.tagPaths', tagPathsAttribute);
            }
        }

        var contentAttrs = component.get('v.contentAttrs');
        var htmlContent = contentAttrs.htmlContent;

        var sitePrefix = window.location.pathname.split('/')[1];
        if (sitePrefix == 's' || !sitePrefix) {
            sitePrefix = '';
        }

        if (sitePrefix) {
            htmlContent = htmlContent.replace(new RegExp('"(\/servlet\/servlet\.FileDownload[^"]+)"', 'g'), '"/' + sitePrefix + '$1"');
            contentAttrs.htmlContent = htmlContent;
            component.set('v.contentAttrs', contentAttrs);
        }
    },

    navigateToList: function(component, event, helper) {
        var tag = event.currentTarget.dataset.tag;
        var targetUrl = '/article-list?tags=' + encodeURIComponent(tag);

        var urlEvent = $A.get("e.force:navigateToURL");
        urlEvent.setParams({ url: targetUrl });
        urlEvent.fire();
    }
})