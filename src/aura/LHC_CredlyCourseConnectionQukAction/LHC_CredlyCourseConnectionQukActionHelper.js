({
	processCredlyIntegration : function(component) {


        var recordId = component.get("v.recordId")


        var action = component.get("c.updateCourseConn");

        action.setParams({courseConnectionId : recordId});

        action.setCallback(this, function(response) {
            var state = response.getState();
            
            if (state === "SUCCESS") {
                console.log("LC_CredlyCourseConnectionQuickActionController.processCredlyIntegration: SUCCESS");
            } else {
                console.log("LC_CredlyCourseConnectionQuickActionController.processCredlyIntegration: FAILED");

            }

        });
        $A.enqueueAction(action);


        //Display the total in a "toast" status message
        var resultsToast = $A.get("e.force:showToast");
        resultsToast.setParams({
            "title": "",
            "message": "Credly integration process is called.",
            "type": "success"
        });
        resultsToast.fire();
	}
})