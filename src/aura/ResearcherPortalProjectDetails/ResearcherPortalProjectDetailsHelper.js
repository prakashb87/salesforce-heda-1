({
    // Add the list of ethics from response to table
    addRows : function(component, tableId, dataset, isInit) {
        var table = $(tableId).DataTable();
        
       // var testdata = [];
        var currentPageLength = table.page.len();
        if(!Array.isArray(dataset)){
            dataset = [dataset];
        }
        
        var addedSet = [];
        
        var iseditable = component.get('v.projectRecord').IsEdit;
        
        dataset.forEach(function(ethic) {
            var temp = [];
            
            temp.push(ethic["ethicsTitle"] != undefined ? ethic["ethicsTitle"]:'');
            temp.push(ethic["status"] != undefined ? ethic["status"]:'');
            temp.push(ethic["approvedDate"] != undefined ? ethic["approvedDate"] : '');
            temp.push(ethic["expiryDate"] != undefined ? ethic["expiryDate"] : '');
            temp.push((iseditable == true) ? "true" : "false");
            table.row.add(temp).draw();
        });
        
        if(!isInit){
           table.page.len(4).draw();           
       }
    },
    
    addLinkedContractRows :function(component, tableId, dataset, isInit){
      var table = $(tableId).DataTable();
        
        if(!Array.isArray(dataset)){
            dataset = [dataset];
        }
        
        dataset.forEach(function(contract) {
            var temp = [];
            
            temp.push(contract["contractTitle"] != undefined ? contract["contractTitle"]:'');
            temp.push(contract["status"] != undefined ? contract["status"]:'');
            temp.push(contract["fullyExecutedDate"] != undefined ? contract["fullyExecutedDate"] : '');
            temp.push(contract["currentLocation"] != undefined ? contract["currentLocation"] : '');
            temp.push(contract["isHyperlinkActive"] != undefined ? contract["isHyperlinkActive"] : '');
            temp.push(contract["contractId"] != undefined ? contract["contractId"] : '');
            table.row.add(temp).draw();
        });
        
        if(!isInit){
           table.page.len(4).draw();           
       }   
    },
    
    addLinkedMilestonesRows :function(component, tableId, dataset, isInit){
      var table = $(tableId).DataTable();
        
        if(!Array.isArray(dataset)){
            dataset = [dataset];
        }
        
        dataset.forEach(function(milestone) {
            var temp = [];
            
            temp.push("");
            temp.push(milestone["dueDate"] != undefined ? milestone["dueDate"] : '');           
            temp.push(milestone["description"] != undefined ? milestone["description"] : '');
            temp.push(milestone["details"] != undefined ? milestone["details"] : '');
            temp.push(milestone["dateCompleted"] != undefined ? milestone["dateCompleted"] : '');
            temp.push(milestone["isDueWithin30Days"] != undefined ? milestone["isDueWithin30Days"] : '');
            temp.push(milestone["isOverDue"] != undefined ? milestone["isOverDue"] : '');
            table.row.add(temp).draw();
        });
        
        if(!isInit){
           table.page.len(4).draw();           
       }   
    },
    
    // Redraw the list of associated ethics table on save based on search selected Ethics
    redrawTable : function(component, tableId, dataset) {
        var table = $(tableId).DataTable();
                 
         var pendArr = dataset.filter(function (el) {
                    if(el.status ==="Pending"){ return el}
                });
         var nonPendArr = dataset.filter(function (el) {
                    if(el.status!=="Pending"){ return el}
                });
         dataset = pendArr.concat(nonPendArr);
        
        
        component.set('v.ethics', dataset);
        
        table.clear().draw();
        
        this.addRows(component, tableId, dataset, false);
        
        table.page('first').draw(false);
        
    },
    
    //Methd call to clear the values on close of modal popup
    clearValues : function(component) {
		$("#searchInput").val("");
        //component.set("v.searchKeyword", "");
        component.set("v.selectedEthics", []);
        component.set("v.server_result", []);
    },
    searchEthics: function(component, event, helper, inputKeyword,isAddEthicsPopup){
        
        var searchEthicsAction = component.get("c.searchEthicswithTitle");
		var selectedEthics = component.get("v.selectedEthics");
        var projectEthics = component.get("v.ethics");
            searchEthicsAction.setParams({
                searchText: inputKeyword,
                isAddEthicsPopup  : isAddEthicsPopup
            });
            //Setting the Callback
            searchEthicsAction.setCallback(this, function(a) {
                //get the response state
                var state = a.getState();

                //check if result is successfull
                if (state == "SUCCESS") {
                    var result = a.getReturnValue();

                    if (!$A.util.isUndefined(result)) {

                        var temp = [];

                        result.forEach(function(member) {
                            if (selectedEthics.findIndex(function(item) {
                                    return item.ethicsId === member.ethicsId;
                                }) == -1 && projectEthics.findIndex(function(item) {
                                    return item.ethicsId === member.ethicsId;
                                }) == -1) {
                                temp.push(member);
                            }
                        });
                        temp = temp.sort(function(x,y){ 
                        var a = String(x.ethicsTitle).toUpperCase(); 
                        var b = String(y.ethicsTitle).toUpperCase(); 
                        if (a > b){
                            return 1 
                        }                             
                        if (a < b) {
                            return -1 
                                   }
                                
                       return 0; 
                    });						
                        component.set("v.server_result", temp);
                         $('.add-ethic-search').toggle(true);
                    }

                } else if (state == "ERROR") {
                    alert('Error in calling server side action:' + JSON.stringify(a) + '\nReturn Value:' + JSON.stringify(a.getReturnValue()));
                }
            });

            //adds the server-side action to the queue        
            $A.enqueueAction(searchEthicsAction);
    },
    getLinkedContacts : function(component, event, helper, recordId){
        var getLinkedContactsAction = component.get("c.getProjectContractWrapperList");
        
        getLinkedContactsAction.setParams({
            projectId: recordId
        });
        getLinkedContactsAction.setCallback(this, function(a) {
            var contractlist = [];
            var state = a.getState();
            if (state == "SUCCESS") {
                var contractlist = a.getReturnValue();
                console.log(contractlist);
                if (!$A.util.isUndefined(contractlist)) {
                    
                    component.set("v.linkedContacts", contractlist);
                    var table = $('#contactList').DataTable({
                        "data": [],
                        "language": {
                            "emptyTable": $A.get("$Label.c.Nocontractshavebeenassociatedwiththisproject")
                        },
                        "pageLength": 5,
                        "dom": '<"top"i>rt<"bottom"><"clear">',
                        "columns": [{
                            title: $A.get("$Label.c.Title")
                        }, {
                            title: $A.get("$Label.c.Status")
                        }, {
                            title: $A.get("$Label.c.submittedDate")
                        }, {
                            title: $A.get("$Label.c.PROCESS_STAGE")
                        }],
                        "info": false,
                        "order": [[ 2, "desc" ]],
                        "searching": false,
                        "dom": 'frt',
                        "drawCallback": function() {
                            // Show or hide "Load more" button based on whether there is more data available
                            $('#linkedcontact-load-more').toggle(this.api().page.hasMore());
                        },
                        "columnDefs": [{
                            type: 'natural',
                            targets: 0,
                            className: "table-title-column",
                            'render': function(data, type, full, meta) {
                                if(!full[4]){
                                    return "<div class='truncate' data-container='body' data-toggle='tooltip' data-placement='top' title='" + data + "'>" + data + "</div>";
                                }
                                else{
                                    return '<div class="truncate"><a class="title-text rmit-blue-color rmit-blue-color_hover" data-container="body" data-toggle="tooltip" data-placement="top" title="' + data + '"  href="' + '/Researcherportal/s/contract-details?recordId=' + full[5] + '">' + data + '</a>';
                                }
                            }
                        },{
                            targets: [2],
                            "render": function(data, type, full, meta, row) {
                                if (type === 'display' || type === 'filter') {
                                    if (data !== '')
                                        return moment(data).format('DD/MM/YYYY');
                                    else
                                        return '';
                                }
                                
                                return data;
                            }
                        }]
                        
                    });
                    $('#linkedcontact-load-more').on('click', function() {
                                table.page.loadMore();
                            });
                    this.addLinkedContractRows(component, "#contactList", contractlist, true);
                }
            }
        });
        
        //adds the server-side action to the queue        
        $A.enqueueAction(getLinkedContactsAction); 
    },
    getLinkedMilestones : function(component, event, helper, recordId){
        var getLinkedMilestonesAction = component.get("c.getProjectMilestonesListWrapper");
        
        getLinkedMilestonesAction.setParams({
            projectId: recordId
        });
        getLinkedMilestonesAction.setCallback(this, function(a) {
            var milestoneslist = [];
            var state = a.getState();
            if (state == "SUCCESS") {
                var milestoneslist = a.getReturnValue();
                if (!$A.util.isUndefined(milestoneslist)) {                    
                    var table = $('#linkedMilestonesList').DataTable({
                        "data": [],
                        "language": {
                            "emptyTable": $A.get("$Label.c.nomilestonesassociatedwithproject")
                        },
                        "pageLength": 4,
                        "dom": '<"top"i>rt<"bottom"><"clear">',
                        "columns": [{
                            title: " "
                        },{
                            title: $A.get("$Label.c.Milestone_Due_Date")
                        }, {
                            title: $A.get("$Label.c.Milestone_Description")
                        }, {
                            title: $A.get("$Label.c.Milestone_Details")
                        }, {
                            title: $A.get("$Label.c.Milestone_Date_Completed")
                        }],
                        "info": false,
                        "order": [],
                        "searching": false,
                        "dom": 'frt',
                        "drawCallback": function() {
                            // Show or hide "Load more" button based on whether there is more data available
                            $('#linkedmilestones-load-more').toggle(this.api().page.hasMore());
                        },
                        "columnDefs": [{
                            targets: 0,
                            className: "table-icon-column",
                             bSortable: false,
                            'render': function(data, type, full, meta) {
                	        if(component.get("v.projectRecord").IsEdit == true){
                                    if (full[6] === true) {
                                        return '<img class="milestone-list-icon" src="'+$A.get("$Resource.MilestoneOverdueIcon")+'"></img>';
                                    } else if(full[5] === true){
                                        return '<img class="milestone-list-icon" src="'+$A.get("$Resource.MilestoneDueIcon")+'"></img>';
                                    } else {
                                        return null;
                                    }
                                }
                                else {
                                    return null;
                                }
                            }
                            
                        },{
                            targets: [1,4],
                            "render": function(data, type, full, meta, row) {
                                if (type === 'display' || type === 'filter') {
                                    if (data !== '')
                                        return moment(data).format('DD/MM/YYYY');
                                    else
                                        return '';
                                }
                                
                                return data;
                            }
                        }]
                        
                    });
                    $('#linkedmilestones-load-more').on('click', function() {
                                table.page.loadMore();
                            });
                    this.addLinkedMilestonesRows(component, "#linkedMilestonesList", milestoneslist, true);
                }
            }
        });
        
        //adds the server-side action to the queue        
        $A.enqueueAction(getLinkedMilestonesAction);
    },
    getcheckInternalDualAccessUser : function(component, event, helper){
        var getcheckInternalDualAccessUserAction = component.get("c.checkInternalDualAccessUser");
        
        getcheckInternalDualAccessUserAction.setCallback(this, function(a) { 
            var state = a.getState();
            if (state == "SUCCESS") {
                 var result = a.getReturnValue();
                if (!$A.util.isUndefined(result)) {  
					component.set('v.isInternalDualAccessUser', result);
                }
            }        
        });
          $A.enqueueAction(getcheckInternalDualAccessUserAction);
    }
})