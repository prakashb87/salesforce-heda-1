({
	goToMyProjectPage: function(component,event) {
        var urlEvent = $A.get("e.force:navigateToURL");
        urlEvent.setParams({
            "url": "/view-all-project?tab=myprojects"
        });
        
        urlEvent.fire();
    },
    goToPublicProjectPage: function(component,event) {
        var urlEvent = $A.get("e.force:navigateToURL");
        urlEvent.setParams({
            "url": "/view-rmitpublic-projects"
        });
        
        urlEvent.fire();
    }


});