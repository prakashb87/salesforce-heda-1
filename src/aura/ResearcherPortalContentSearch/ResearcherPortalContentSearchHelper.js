({
    callGetKeywords: function(component, cb) {
        var action = component.get('c.getKeywords');

        action.setCallback(this, function(response) {
            var state = response.getState();

            if (state === "ERROR") {
                cb(response.getError(), null);
            } else {
                cb(null, response.getReturnValue());
            }
        });

        $A.enqueueAction(action);
    },

    callGetResults: function(component, term, cb) {
        var action = component.get('c.getResults');

        action.setParams({ term: term });

        action.setCallback(this, function(response) {
            var state = response.getState();

            if (state === "ERROR") {
                cb(response.getError(), null);
            } else {
                cb(null, JSON.parse(response.getReturnValue()));
            }
        });

        $A.enqueueAction(action);
    },

    callGetResultPage: function(component, originIds, cb) {
        var action = component.get('c.getResultPage');

        action.setParams({ originIds: originIds });

        action.setCallback(this, function(response) {
            var state = response.getState();

            if (state === "ERROR") {
                cb(response.getError(), null);
            } else {
                cb(null, JSON.parse(response.getReturnValue()));
            }
        });

        $A.enqueueAction(action);
    }
})