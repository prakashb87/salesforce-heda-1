/*******************************************
 Purpose: To get current user Informatio
 History:
 Created by Esther Ethelbert on 08/16/2018
 *******************************************/
({
    // on Load, get the logged in user information and store in variable
    doInit : function(component, event, helper) {
        $('#createPjtBtn').css("display","none");
        var getUserInfoAction = component.get("c.fetchUser");
        getUserInfoAction.setCallback(this, function(response) {
            var state = response.getState();
            if (state === "SUCCESS") {
                var storeResponse = response.getReturnValue();
                // set current user information on userInfo attribute
                if(storeResponse.ContactId !=undefined){
                    $('#createPjtBtn').css("display","block");
                }
                else{
                    $('#createPjtBtn').css("display","none");
                }
                component.set("v.userInfo", storeResponse);
            }
        });
        $A.enqueueAction(getUserInfoAction);
    },
    //To navigate to create page on click of create button
    goToCreatePage: function(component,event) {
        var urlEvent = $A.get("e.force:navigateToURL");
        urlEvent.setParams({
            "url": "/create-new-project"
        });
        
        urlEvent.fire();
    }
})