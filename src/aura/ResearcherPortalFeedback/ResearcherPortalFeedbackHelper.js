({
    //set Feedback page labels
    setLabels: function(component, event, helper){
        let ratingList =[
            {'label':'Very dissatisfied', 'value':'1', 'iconClass':'far fa-angry font-40 rating-icon'},
            {'label':'Dissatisfied', 'value':'2', 'iconClass':'far fa-frown font-40 rating-icon'},
            {'label':'Neutral', 'value':'3', 'iconClass':'far fa-meh font-40 rating-icon'},
            {'label':'Satisfied', 'value':'4', 'iconClass':'far fa-grin-alt font-40 rating-icon'},
            {'label':'Very satisfied', 'value':'5', 'iconClass':'far fa-grin-hearts font-40 rating-icon'}
        ]
        let feedbackLabelList ={
            'Popup_helptext1':'File size limit is 2MB.',
            'Popup_helptext3':'pdf, doc, docx, xls, xlsx, csv, png, jpeg.',
        }
        component.set('v.feedbackLabels', feedbackLabelList)
        component.set('v.ratingList', ratingList)
    },
    setFeedbackTopic:function(component, result){
        let feedbackList =[
            {'sublabel':$A.get("$Label.c.Your_ideas_for_new_features_or_tweaks_to_the_Researcher_Portal"), 'checked':'checked'},
            {'sublabel':$A.get("$Label.c.Improving_the_information_within_the_Research_Lifecycle"), 'checked':''},
            {'sublabel':$A.get("$Label.c.Any_other_comments_you_may_have_regarding_the_portal"), 'checked':''}
        ]
        result.forEach(function(item,index){
              feedbackList[index].label = item
        })
        component.set('v.feedbackList', feedbackList)
        component.set('v.showLoader', false)
    },
	 // on close of dropdown hide the list of projects
    restoreProjectList: function() {
        var a, i;
        a = $("#topicDropdown li");
        for (i = 0; i < a.length; i++) {
            a[i].style.display = "";
        }
    },
    // change arrow up when dropdown is shown and arrow down when dropdown is closed
    toggleArrowState: function(dropdownId, arrowId) {
        if ($(dropdownId).is(':visible')) {
            $(arrowId).removeClass('fa-angle-down');
            $(arrowId).addClass('fa-angle-up');
        } else {
            $(arrowId).removeClass('fa-angle-up');
            $(arrowId).addClass('fa-angle-down');
        }
    },
     //Function call to submit Research Feedback form and send to save request
    submitFeedbackForm: function(component, event, helper, formData, caseAttachments){
        let numberOfFiles= caseAttachments && caseAttachments.length > 0 ? caseAttachments.length :0
        if(caseAttachments && caseAttachments.length>0){
            formData.caseAttachments=[caseAttachments[0]]
        }
         
        //formData.caseAttachments= []//caseAttachments.length >0 ? arr.push(caseAttachments[0]):[]
        var submitSupportForm = component.get("c.createCase");
        submitSupportForm.setParams({
            caseWrapperString: JSON.stringify(formData)
        });
        submitSupportForm.setCallback(this,function(response) {
            var state = response.getState();
            
            if(state == "SUCCESS"){
                let caseId = response.getReturnValue();
                var fileSendCount =1;
                if(numberOfFiles>1){
					helper.sendNextFile(component, event, helper, caseId, fileSendCount, numberOfFiles, caseAttachments)                    
                } else {
                    component.set('v.submitLoader', false)
                    component.set("v.submittedSuccess", true);
                }
            } else if(state == "ERROR"){
                component.set('v.submitLoader', false)
                console.error('Error in calling server side action');
                 $('.feedbackcancelbtn')[0].disabled = false;
            	$('.feedbacksubmitbtn')[0].disabled = false;
            }
            
        });
        $A.enqueueAction(submitSupportForm); 
    },
    sendNextFile: function(component, event, helper, caseId, fileSendCount, numberOfFiles, caseAttachments){  
		if(numberOfFiles>fileSendCount){
           let formData= caseAttachments[fileSendCount];
           this.callServer(component, event, helper, formData, caseId, fileSendCount, numberOfFiles, caseAttachments)
        } else{
            component.set("v.submittedSuccess", true);
        }
    },
    callServer: function(component, event, helper, formData, caseId, fileSendCount, numberOfFiles, caseAttachments){
        var submitRemainingFiles = component.get("c.attachFilesToCaseRecord");
        submitRemainingFiles.setParams({
            caseAttachments : JSON.stringify(formData),
            caseId:caseId
        });
        submitRemainingFiles.setCallback(this,function(response) {
            var state = response.getState();
            component.set('v.submitLoader', false)
            if(state == "SUCCESS"){
                fileSendCount = fileSendCount+1
                this.sendNextFile(component, event, helper, caseId, fileSendCount, numberOfFiles, caseAttachments)
                component.set("v.submittedSuccess", true);
            } else if(state == "ERROR"){
                alert('Error in calling server side action');
                 $('.feedbackcancelbtn')[0].disabled = false;
            	$('.feedbacksubmitbtn')[0].disabled = false;
            }
            
        });
        $A.enqueueAction(submitRemainingFiles); 
    },
    hideModal:function(component, event, helper, idName, index){
        var childCmp = component.find(idName);
        childCmp.showModal('HIDE',index);
    },
    showModal:function(component, event, helper, idName, index){
        var childCmp = component.find(idName);
        childCmp.showModal('SHOW', index);
    }
})