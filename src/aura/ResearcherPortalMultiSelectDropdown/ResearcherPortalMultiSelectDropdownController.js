({

    onInit:function(component){
        let msDropownFilteredData = component.get("v.msDropdownData")
        setTimeout(function(){
            component.set('v.msDropownFilteredData', msDropownFilteredData)
        },500)
        window.addEventListener('click', function(event){
            if (!event.target.matches('.ms-dropbtn')) {
                var dropdowns = document.getElementsByClassName("ms-dropdown-content");
                var i;
                for (i = 0; i < dropdowns.length; i++) {
                    var openDropdown = dropdowns[i];
                    if (!openDropdown.classList.contains('hide')) {
                        openDropdown.classList.add('hide');
                        if(openDropdown.previousSibling.nodeName == 'INPUT'){
                            openDropdown.previousSibling.value = ''
                        }
                    }
                }
            }
        });
    },

    filterForFunction: function(component, event, helper) {
        let msDropdownId = component.get("v.msDropdownId")
        let dropdownListId = msDropdownId+'-DropdownList'
        let searchBoxId = msDropdownId+'-searchBox'
        let msDropdownData = component.get("v.msDropdownData")
        if($("#"+dropdownListId).hasClass("hide")){
            $("#"+dropdownListId).removeClass("hide")
        }
        let inputVal = $("#"+searchBoxId).val();
        let inputValUpperCase = inputVal.toUpperCase();
        let allList = $("#"+dropdownListId+" li");
        if(inputVal && inputVal.trim().length>0){
            let filteredData=  msDropdownData.filter(function(item){
                let itemUpperCase = item.seperator1.toUpperCase()
                return itemUpperCase.includes(inputValUpperCase);
            })
            component.set("v.msDropownFilteredData", filteredData)
        } else {
            component.set("v.msDropownFilteredData", msDropdownData)
        }
    },
    toggleForDropdown: function(component, event, helper) {
        let msDropdownId = component.get("v.msDropdownId")
        let dropdownListId = msDropdownId+'-DropdownList'
        let msDropdownData = component.get("v.msDropdownData")
        let searchBoxId = msDropdownId+'-searchBox'
        if($("#"+dropdownListId).hasClass("hide")){
            $("#"+dropdownListId).removeClass("hide")
        }
        let inputVal = $("#"+searchBoxId).val();
        let inputValUpperCase = inputVal.toUpperCase();
        if(inputVal && inputVal.trim().length>0){
            let filteredData=  msDropdownData.filter(function(item){
                let itemUpperCase = item.seperator1.toUpperCase()
                return itemUpperCase.includes(inputValUpperCase);
            })
            component.set("v.msDropownFilteredData", filteredData)
        } else {
            component.set("v.msDropownFilteredData", msDropdownData)
        }
    },
    keyEnterFunction :function(component, event, helper){
        if(event.keyCode ===13){
            event.preventDefault();
        }
    },
    CheckboxForSelect: function(component, event, helper) {
        let msDropdownId = component.get("v.msDropdownId")
        let dropdownListId = msDropdownId+'-DropdownList'
        let msDropdownData = component.get("v.msDropdownData")
        var selectedIds = [];
        msDropdownData.forEach(function(item,index){
            if(item.id == event.target.name){
                msDropdownData[index].checked = event.target.checked ? true:false
            }
        })
        msDropdownData.forEach(function(item,index){
            if(item.checked){
                selectedIds.push(item.id)
            }
        })

        component.set("v.selectedDropdownListId", selectedIds);
        let selectedList = msDropdownData.filter(function(item){
            return item.checked
        })
        component.set("v.selectedDropdownList", selectedList);
        component.set("v.msDropdownData", msDropdownData);
    },
    delForSelected: function(component, event, helper) {
        let msDropdownId = component.get("v.msDropdownId")
        let dropdownListId = msDropdownId+'-DropdownList'
        var selectedItem = event.currentTarget;
        var id = selectedItem.dataset.id;
        let msDropdownData = component.get("v.msDropdownData")
        let selectedDropdownList = component.get("v.selectedDropdownList")
        let selectedDropdownListId = component.get("v.selectedDropdownListId")
        var indexInSelectedList = selectedDropdownList.findIndex(function(item){
            return item.id == id
        })
        var indexInSelectedListId = selectedDropdownListId.findIndex(function(item){
            return item == id
        })
        if(indexInSelectedList > -1){
            selectedDropdownList.splice(indexInSelectedList,1)
            selectedDropdownListId.splice(indexInSelectedListId,1)
            component.set("v.selectedDropdownList", selectedDropdownList)
            component.set("v.selectedDropdownListId", selectedDropdownListId)
            msDropdownData.forEach(function(item, index){
                if(item.id === id){
                    msDropdownData[index].checked= false
                }
            })
            component.set("v.msDropdownData", msDropdownData)
        }
    },
    resetMultiSelectSearchBox:function(component, event, helper) {
        let params = event.getParams().arguments
        let msDropdownId = params.msDropdownId
        let dropdownSearchBoxId = msDropdownId+'-searchBox'
        $('#'+dropdownSearchBoxId).val('')
        let msDropdownData = component.get("v.msDropdownData")
        component.set("v.msDropownFilteredData", msDropdownData)
    },
    getMultiSelectBoxSelectedIds:function(component, event, helper) {
        let params = event.getParams().arguments
        let callback =params.successCallback
        let selectedDropdownListId = component.get("v.selectedDropdownListId")
        let arr =[]
        selectedDropdownListId.forEach(function(item){
            let id = item.split('-')[1]
            arr.push(id)
        })
        callback(arr)
    }

})