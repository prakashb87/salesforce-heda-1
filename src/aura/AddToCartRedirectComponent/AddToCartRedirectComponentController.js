({
    /**
     * @description This method redirects the user to the add to cart page on page load. 
     * It will take all query string parameters and pass them on to the add to cart 
     * page. 
     */
    doInit : function(component, event, helper) {
        let urlStringParams = decodeURIComponent(escape(window.location.search.substring(1))); 
        let redirectUrl= $A.get("e.force:navigateToURL");
        redirectUrl.setParams({
            "url": '/addtocart?' + urlStringParams
        });
        redirectUrl.fire();
    }
})