({
    //Initialization function
    doInit: function(component, event, helper) {
    	//Calls helper method for getting the account
        helper.callAction(component, 'c.createLead');
    },
    //Calls on the submit of the form and take school and Lead form as parameter
    clickCreateLead: function(component, event, helper) {
        var leadnew = component.get("v.newLead");
        var recordId = component.get("v.recordId");
        if(leadnew.I_am_currently__c == '--None--' ){
            component.set("v.requiredMissing", true);
            component.find('notifLib').showNotice({
                "variant": "error",
                "header": "Something has gone wrong!",
                "message": "Please select value in 'I am currently'."
            });
        }else if(leadnew.Residency_status__c == '--None--'){
            component.set("v.requiredMissing", true);
            component.find('notifLib').showNotice({
                "variant": "error",
                "header": "Something has gone wrong!",
                "message": "Please select value in 'Residency Status'."
            });
        }else if(leadnew.Interest_Area__c == '--None--'){
            component.set("v.requiredMissing", true);
            component.find('notifLib').showNotice({
                "variant": "error",
                "header": "Something has gone wrong!",
                "message": "Please select value in 'Select an area you are interested in'."
            });
        }else{
            helper.clickCreateLeadHelper(component,event,leadnew);
        }
        
    }
})