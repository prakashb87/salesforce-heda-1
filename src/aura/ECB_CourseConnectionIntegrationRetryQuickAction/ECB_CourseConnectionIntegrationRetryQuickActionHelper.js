({
	processSAMSIntegration : function(component) {


        var recordId = component.get("v.recordId")


        var action = component.get("c.sendCourseConn");

        action.setParams({courseConnectionId : recordId});

        action.setCallback(this, function(response) {
            var state = response.getState();
            console.log(state);
            if (state === "SUCCESS") {
                console.log("ECB_CourseConnectionIntegrationRetryQuickActionHelper.processSAMSIntegration: SUCCESS");
            } else {
                console.log("ECB_CourseConnectionIntegrationRetryQuickActionHelper.processSAMSIntegration: FAILED");

            }

        });
        $A.enqueueAction(action);


        //Display the total in a "toast" status message
        var resultsToast = $A.get("e.force:showToast");
        resultsToast.setParams({
            "title": "",
            "message": "SAMS integration process is called.",
            "type": "success"
        });
        resultsToast.fire();
	}
})