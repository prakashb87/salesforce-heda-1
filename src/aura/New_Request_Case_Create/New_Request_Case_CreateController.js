({	//function to get the selected contact Id from fired event
    handleLookupChooseEvent: function(component, event){
    	component.set("v.selectedId", event.getParam("recordId"));
	},
    //function to handle the saving of the case
	handleSaveRequest : function(component, event, helper) {
        var selectedContact = component.get("v.selectedId");
        var leadId = component.get("v.recordId");
        var newCase = component.get("v.newCase");
        var saveCase = component.get("c.insertCase");
        saveCase.setParams({
            "Objcase": newCase,
            "conId": selectedContact,
            "leadId": leadId
        });
        saveCase.setCallback(this, function(a) {
           console.log('Inside Callback!');
           var state = a.getState();
            if (state === "SUCCESS") {
                var name = a.getReturnValue();
                if(name == 'true'){
                    var dismissActionPanel = $A.get("e.force:closeQuickAction");
                    var resultsToast = $A.get("e.force:showToast");
                    resultsToast.setParams({
                        "title": "Success: ",
                        "message": "Request created successfully!",
                        "type": "success"
                    });
                    dismissActionPanel.fire();
                    resultsToast.fire();
                }else{
                    var dismissActionPanel1 = $A.get("e.force:closeQuickAction");
                    var resultsToast1 = $A.get("e.force:showToast");
                    resultsToast1.setParams({
                        "title": "Error: ",
                        "message": "An error occured while creating your request!",
                        "type": "error"
                    });
                    dismissActionPanel1.fire();
                    resultsToast1.fire();
                }
            }
        });
    $A.enqueueAction(saveCase)
	}
})