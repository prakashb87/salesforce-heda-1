({
	checkToggleDropdown : function(component, event, helper, id) {
		let isDropdownClose = $('#'+id+'-List').hasClass('hide')
        if(isDropdownClose){
           component.set("v.isDropdownClose", true) 
        } else {
           component.set("v.isDropdownClose", false)
        }
	}
})