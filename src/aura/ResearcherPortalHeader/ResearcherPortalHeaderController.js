/*******************************************
 Purpose: To get current user Information
 History:
 Created by Esther Ethelbert on 16/08/2018
 *******************************************/
({
    afterScriptsLoaded:function(component, event, helper) {
        (function(w,d,s,l,i){w[l]=w[l]||[];w[l].push({'gtm.start':
                new Date().getTime(),event:'gtm.js'});var f=d.getElementsByTagName(s)[0],
            j=d.createElement(s),dl=l!='dataLayer'?'&l='+l:'';j.async=true;j.src=
            'https://www.googletagmanager.com/gtm.js?id='+i+dl;f.parentNode.insertBefore(j,f);
        })(window,document,'script','dataLayer','GTM-NGVP2MK');
        $('body').append('<noscript><iframe src="https://www.googletagmanager.com/ns.html?id=GTM-NGVP2MK" height="0" width="0" style="display:none;visibility:hidden"></iframe></noscript>')

    },
    //method call to get the logged in user details. Make the border and color change for the selcted item
    doInit: function(component, event, helper) {
        // Commented Oct 25 - this was causing an error in community builder
        helper.checkSandbox(component, event);
        helper.fetchUserDetails(component, event, helper)
       
        var sPageURL = decodeURIComponent(window.location.search.substring(1)); //You get the whole decoded URL of the page.
        var sURLVariables = sPageURL.split('&'); //Split by & so that you get the key value pairs separately in a list
        var sParameterName, i;

        var urlParams = {};
        if (window.location.search) {
            urlParams = window.location.search.substr(1).split('&').map(function(param) {
                return param.split('=').map(function(part) {
                    return decodeURIComponent(part).replace(/\+/g, ' ');
                });
            }).reduce(function(params, param) {
                params[param[0]] = param[1];
                return params;
            }, {});
        }

        sParameterName = [ 'tab', urlParams.tab || 'Not found' ];
        component.set('v.searchTerm', urlParams.q);

        var linkId = undefined;

        switch (sParameterName[1]) {
            case "dashboard":
                linkId = "dashboardSidenav";
                break;
            case "fundingopportunities":
                linkId = "fundingopportunitiesSidenav";
                break;
            case "translation":
                linkId = "translationSidenav";
                break;
            case "tools":
                linkId = "toolsSidenav";
                break;
            case "engagement":
                linkId = "engagementSidenav";
                break;
            case "collaboration":
                linkId = "collaborationSidenav";
                break;
            case "mymilestones":
                linkId = "mymilestonesSidenav";
                break;
            case "funding":
                linkId = "fundingSidenav";
                break;
            case "ethics":
                linkId = "ethicsSidenav";
                break;
            case "publications":
                linkId = "publicationsSidenav";
                break;
            case "hdrsupervision":
                linkId = "hdrsupervisionSidenav";
                break;
            case "faq":
                linkId = "faqSidenav";
                break;
            case "researchconnect":
                linkId = "researchconnectSidenav";
                break;
        }

        if (sPageURL == "")
            linkId = "dashboardSidenav";

        if (linkId != undefined) {
            var cmpTarget = component.find(linkId);

            $A.util.addClass(cmpTarget, "tabActive");
        }

        var url = window.location.protocol + '//' + window.location.hostname+'/Researcherportal/s/research-support';
        component.set("v.siteURL", url);
        window.setTimeout(function(){
            var allInputs = document.querySelectorAll('input')
            var arr= Array.from(allInputs)
            if(arr && arr.length>0){
                arr.forEach((item)=>{
                    if(item.getAttribute("autocomplete") != "true"){
                        item.setAttribute("autocomplete","true");
                    }
               })
            }
        },5000)
          
    },

    // to navigate to respective tab on click of header items
    goToNewPage: function(component, event, helper) {
       //Start :: Added by Subhajit :: 245/233 :: 11/14/2018
        var candidaturecentreNONPROD_URL = $A.get("$Label.c.Candidature_Centre_URL");
        var candidaturecentrePROD_URL = $A.get("$Label.c.Candidature_Centre_PROD_URL");
        var hdrsupervisionNONPROD_URL = $A.get("$Label.c.hdrsupervision_URL");
        var hdrsupervisionPROD_URL = $A.get("$Label.c.hdrsupervision_PROD_URL");
        //Start :: Added by Subhajit :: 245/233 :: 11/14/2018
        
        var urlEvent = $A.get("e.force:navigateToURL");
        
        var ctarget = event.currentTarget;
        var id_str = ctarget.dataset.value;
        
        var targetUrl;
        console.log('@@@@@id_str ===>'+id_str);
        switch(id_str){
            case "myprojects":
                targetUrl = "/view-all-project?tab=";
                break;
            case "ethics":
                targetUrl = "/view-my-ethics?tab=";
                break;
            case "funding":
                targetUrl = "/view-funding-details?tab=";
                break;
            case "mymilestones":
                targetUrl = "/view-my-milestones?tab=";
                break;
            case "publications":
                targetUrl = "/view-my-publications?tab=";
                break;
            case "contracts":
                targetUrl = "/defaultpage?tab=";
                break;
                //Start :: Modified by Subhajit :: 245/233 :: 11/14/2018
			case "researchSupport":
                targetUrl = "/research-support";
                break;
            case "portalFeedback":
                targetUrl = "/portal-feedback";
                break
            case "candidature":
                //console.log('@@@@@isSandbox'+component.get("v.isSandbox") +'==>'+candidaturecentreNONPROD_URL);
                if(component.get("v.isSandbox"))
                    targetUrl = candidaturecentreNONPROD_URL;
                else
                    targetUrl = candidaturecentrePROD_URL;
                break;
                
            case "hdrsupervision":
                //console.log('@@@@@isSandbox'+component.get("v.isSandbox") +'==>'+hdrsupervisionNONPROD_URL);
                if(component.get("v.isSandbox"))
                    targetUrl = hdrsupervisionNONPROD_URL;
                else
                    targetUrl = hdrsupervisionPROD_URL;
                break;
                //End :: Modified  by Subhajit :: 245/233 :: 11/14/2018
            default:
                targetUrl = "/";
                id_str = "";
        }
        
        //Start :: Modified by Subhajit :: 245/233 :: 11/14/2018  
        if(id_str=='candidature' || id_str=='hdrsupervision')
        {
            console.log('@@@@@targetUrl ===>'+targetUrl);
            window.open(targetUrl, '_blank');
        }
        else
        {
            targetUrl = targetUrl + id_str;
            
            urlEvent.setParams({
                "url" : targetUrl
            });
            
            urlEvent.fire();  
        }
        //End :: Modified by Subhajit :: 245/233 :: 11/14/2018  
    },

    doSearch: function(component, event, helper) {
        var term = component.find("contentSearchbox").getElement().value.trim();
        var urlEvent = $A.get("e.force:navigateToURL");

        event.preventDefault();

        urlEvent.setParams({
            "url": "/search?q=" + encodeURIComponent(term)
        });

        urlEvent.fire();
    },

    //To navigate to Ethics page in mobile view
    goToViewEthicsPage: function(component, event, helper) {
        var urlEvent = $A.get("e.force:navigateToURL");
        urlEvent.setParams({
            "url": "/view-my-ethics"
        });

        urlEvent.fire();
    },

    //click on hamber icon to show dashboard and left nav items in mobile
    openNav: function(component, event) {
        var targetComponent = component.find("mobilesidenav");
        $A.util.addClass(targetComponent, "openPanel");
    },

    //close the details shown on click of hamber icon
    closeNav: function(component) {
        var targetComponent = component.find("mobilesidenav");
        $A.util.removeClass(targetComponent, "openPanel");
    },
    goToLifecyclePage : function(component, event, helper) {

        var urlEvent = $A.get("e.force:navigateToURL");

        var ctarget = event.currentTarget;
        var id_str = ctarget.dataset.value;

        urlEvent.setParams({
            "url" : "/" + id_str
        });

        urlEvent.fire();
    },
    
    // method call to toggle For codes dropdown and on edit based on data check the checkbox
    toggleUsername: function(component, event, helper) {
        component.set("v.isSizeError",false);
        var targetCmp = event.currentTarget;
        
        var targetId = targetCmp.id;
        
        var type = targetCmp.dataset.type;
        
        var topicDropdown = $('#' + type + 'Dropdown');
        if ((topicDropdown.css("display") === "none") || (topicDropdown.css("display") === "")) {
            topicDropdown.css("display", "block");
            helper.toggleforArrowState('#' + type + 'Dropdown', '#' + type);
            
        } else {
            topicDropdown.css("display", "none");
            helper.toggleforArrowState('#' + type + 'Dropdown', '#' + type);
        }
        
        $(document).click(component, function(e) {
            if ($(e.target).closest('#logoutdesktopDropdown').length === 0 && $('#logoutdesktopDropdown').is(":visible")) {
                $('#logoutdesktopDropdown').toggle(false);
                helper.toggleforArrowState('#logoutdesktopDropdown', '#logoutdesktop');
            }         
            else if ($(e.target).closest('#logoutmobileDropdown').length === 0 && $('#logoutmobileDropdown').is(":visible")) {
                $('#logoutmobileDropdown').toggle(false);
                helper.toggleforArrowState('#logoutmobileDropdown', '#logoutmobile');
            }
            
        });
    },
    portalLogout: function(component, event, helper) {
        var LOGOUT_URL
	    if(component.get("v.isSandbox")){
	    	LOGOUT_URL = $A.get("$Label.c.RSTP_LOGOUT_URL");
	    } else {
            LOGOUT_URL = $A.get("$Label.c.RSTP_LOGOUT_PROD_URL");
	    }
        $("#forDropdown").css("display", "none"); 
        helper.toggleforArrowState();
        // var url = window.location.protocol + '//' + window.location.hostname+'/Researcherportal/s/login/?startURL=%2FResearcherportal%2Fs%2F&ec=302';
        //  var url = "https://uatnew-rmitheda.cs72.force.com/Researcherportal/s/login/?startURL=%2FResearcherportal%2Fs%2F&ec=302";
        //var  url = "https://myapps-sit.idm-npe.its.rmit.edu.au/nidp/app/logout";
        //component.set("v.logoutURL", url);
        //window.location.replace(retUrl);
       window.location.replace(window.location.protocol + '//' + window.location.hostname+"/Researcherportal/secur/logout.jsp");
       window.location.replace(LOGOUT_URL);          
        
    },
    clickUserPhoto:function(component,event,helper){
        document.getElementById('userphoto').click()
    },
    handlePhoto:function(component,event,helper){
        event.stopPropagation();
       component.set("v.isLoading",true);
         var file = event.target.files[0];
        var imageType = file.type ? file.type.split('/')[1]:file.type
        var FileSize = file.size / 1024 / 1024; // in MB
        if (FileSize > 2) {
            component.set("v.isSizeError",true);
            component.set("v.isLoading",false);
        } else {
			component.set("v.isSizeError",false);
            var reader = new FileReader();
        reader.addEventListener("load", function (e) { // Setting up base64 URL on image
            var photoStream = reader.result.split(',')[1];
            helper.getPhotoUrl(component,event, helper, photoStream, imageType)
        }, false);
         if (file) {
           reader.readAsDataURL(file)
          }
        }
    }
})