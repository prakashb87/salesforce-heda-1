({
    //ECB-2820   My progress page       
    myProgress : function(component, event, helper) {
       
        helper.getEnrollmentDetails(component);
        //ECB-5399 - 19/2/2019 - Rishabh
        helper.getTodaysDate(component);
        //helper.closeDropDown(component);
        
        //ECB-4530 Start
        var action = component.get("c.getCanvasDashboardRedirectURL");
        action.setCallback(this, function(a) 
         {        
          component.set("v.getCustomSettingRedirect", a.getReturnValue());
         });

        $A.enqueueAction(action);
        //ECB-4530 End
        
    },
    
    //ECB-4530
    customSetting21CCClick :function(component, event, helper){
        var urlEvent = $A.get("e.force:navigateToURL");
        urlEvent.setParams({
            "url": component.get("{!v.getCustomSettingRedirect[0]}") 
        });
        urlEvent.fire();
    },
    
    //ECB-4530
    customSettingRmitOnlineClick :function(component, event, helper){
        var urlEvent = $A.get("e.force:navigateToURL");
        var course_id = event.currentTarget.id;   
        console.log('course_id:::'+course_id);
        urlEvent.setParams({
            "url": component.get("{!v.getCustomSettingRedirect[1]}") + course_id
        });
        urlEvent.fire();
    },
    
    myDetails : function(component, event, helper) {
        
        var urlEvent = $A.get("e.force:navigateToURL");
        urlEvent.setParams({
            "url": "/mydetails"
            
        });
        urlEvent.fire();     
    },  
    
    purchaseHistory : function(component, event, helper) {
        
        var urlEvent = $A.get("e.force:navigateToURL");
        urlEvent.setParams({
            "url": "/purchasehistory"
            
        });
        urlEvent.fire();     
    },
    
    showAccordion:function(component, event)
    {
        var parent=event.currentTarget.parentElement;
        if (parent !== undefined) {
            if (parent.classList.contains('myplan-content-is-collapsed')) {
                
                parent.classList.remove('myplan-content-is-collapsed');
                event.currentTarget.childNodes[1].removeAttribute('href');
                event.currentTarget.childNodes[2].setAttribute('href','');
            } else {
                parent.classList.add('myplan-content-is-collapsed');
                event.currentTarget.childNodes[2].removeAttribute('href');
                event.currentTarget.childNodes[1].setAttribute('href','');
            }
        }
        
    },
    
    //ECB-3838 :Click through to Canvas from My dashboard
    canvasRedirection : function(component, event, helper) { 
        helper.getCanvasRedirectionURL(component);
        
    },
    
    displayCourseList : function(component, event, helper) { 
        event.stopPropagation();                   
        var parent=event.currentTarget.parentElement.parentElement;
        if (parent !== undefined) {
            if (parent.classList.contains('open')) {
                parent.classList.remove('open');
            } else {
                helper.resetClass(component);
                parent.classList.add('open');                
            }           
        }
    },
    togglevisible : function(component, event, helper){       
        var elem=document.querySelectorAll('.badge-list');         
        for(var i=0;i<elem.length;i++)
        {
            var element=elem[i];
            element.classList.remove('open');
        }
    },
    
    hideCourseList : function(component, event, helper) { 
        var parent=event.currentTarget.parentElement;
        parent.classList.remove('open');
        if (parent !== undefined) {
            parent.classList.toggle('open');
        } 
        else{
            parent.classList.toggle('open');
        }
    },
    enrolllist : function(component, event, helper) {                
        var elem = document.querySelector('.dashboard-datelist');
        var backelem = document.querySelector('.dashboard-background');        
        elem.style.display="block";
        backelem.style.display="block";
        var selem = document.querySelector('.success-enroll');
        selem.style.display="none";
        var colem = document.querySelector('.coffer-list');
        colem.style.display="block";        
        
        helper.getCourseOfferings(component,event);
        
        //component.set("v.popmessage",'Please accept T&Cs to proceed with payment.');
        
    },
    closeschedulelist:function(component,event,helper){
        var elem = document.querySelector('.dashboard-datelist');
        var backelem = document.querySelector('.dashboard-background');        
        elem.style.display="none";
        backelem.style.display="none";
        
        helper.getEnrollmentDetails(component);
        helper.closeDropDown(component);
    },
    sucessenroll:function(component,event,helper){
        helper.createCourseEnrollment(component);
        
    },
    // When we write the redirection for the Program add event.stopPropagation(); 
    openModel: function(component, event, helper) {
        // for Display Model,set the "isOpen" attribute to "true"
        component.set("v.isOpen", true);
        
    },
    
    closeModel: function(component, event, helper) {
        
        component.set("v.isOpen", false);
        
        
    }, 
    
    //ECB- 4535: SF Update Program Tile (grade and view certificate)
    Navigate :function(component, event, helper){
        
        var shoppingURL = event.currentTarget.getAttribute('data-coursename');
        console.log(shoppingURL);
        var urlEvent = $A.get("e.force:navigateToURL");
        urlEvent.setParams({
            "url": shoppingURL
        });
        urlEvent.fire();
        $A.enqueueAction(action);
        
    },
    
    getCourseOfferId : function(component,event,helper){
        
        var cofferId = event.getSource().get("v.value");
        console.log('cofferId:::'+cofferId);
        component.set("v.courseOfferId",cofferId);
        
        // var startdate = event.target.id;
        var startdate = event.getSource().getAttribute('data-startdate');
        console.log('startdate:::'+startdate);
        /* if(startdate!=null && startdate!=''){
           component.set("v.cOfferStartDate",startdate); 
        }else{
            component.set("v.cOfferStartDate","Starts Now"); 
        }*/
        component.set("v.cOfferStartDate",startdate); 
        
    },
    
    refreshDashboard : function(component,event,helper){
        
        helper.getEnrollmentDetails(component);
        helper.closeDropDown(component);
    },
    
})