({    
    enableDataTable: function(component, event, helper) {
        helper.getcheckInternalDualAccessUser(component, event, helper);
        var filterByDueDate = [$A.get("$Label.c.All"), $A.get("$Label.c.Due_Date"), $A.get("$Label.c.Milestone_Overdue")];
        component.set("v.filterByDueDate", filterByDueDate);
        component.set("v.selectedDueDate", filterByDueDate[0]);        
        helper.getRecordTypes(component, event, helper);
        
        var getMilestonesAction = component.get("c.getMyMilestonesListWrapper");
        
        getMilestonesAction.setCallback(this, function(response) {
            var milestonesstate = response.getState();
            if (milestonesstate == "SUCCESS") {
                var milestones = JSON.parse(response.getReturnValue()); 
                
                if (!$A.util.isUndefined(milestones.milestoneWrapperList)) {
                    component.set("v.milestones", milestones.milestoneWrapperList);				
                    var dataSet = helper.processData(milestones.milestoneWrapperList);
                    if(component.get('v.isInternalDualAccessUser') == true){
                        dataSet = [];
                        component.set("v.milestones", dataSet);
                    }
                    var table = $('#myMilestoneList').DataTable({
                        "data": dataSet,
                        "pageLength": 25,
                        "language": {
                            "emptyTable": $A.get("$Label.c.youdonthaveanymilestones")
                        },
                        "dom": '<"top"i>rt<"bottom"><"clear">',
                        "columns": [{
                            title: " "
                        }, {
                            title: $A.get("$Label.c.Milestone_Due_Date")
                        }, {
                            title: $A.get("$Label.c.Milestone_Type")
                        }, {
                            title: $A.get("$Label.c.Milestone_Name")
                        }, {
                            title: $A.get("$Label.c.Milestone_Description")
                        }, {
                            title: $A.get("$Label.c.Milestone_Details")
                        }, {
                            title: $A.get("$Label.c.Milestone_Date_Completed")
                        }],
                        "info": false,
                        "aaSorting": [],
                        "searching": false,
                        "dom": 'frt',
                        "drawCallback": function(settings) {
                            if(this.api() && this.api().page && this.api().page.info){
                                var info = this.api().page.info()
                                if(info.length <info.recordsTotal){
                                    $('#btn-loadMore-Miles').toggle(true);
                                } else {
                                    $('#btn-loadMore-Miles').toggle(false);
                                }
                            } 
                            
                        },
                        "columnDefs": [{
                            targets: 1,
                            width: "10%",
                            className: "",
                        }, {
                            targets: 0,
                            className: "table-icon-column",
                            bSortable: false,
                            "render": function(data, type, full, meta, row) {
                                if (full[8] === true) {
                                    return '<img class="milestone-list-icon" src="'+$A.get("$Resource.MilestoneOverdueIcon")+'"></img>';
                                } else if(full[7] === true){
                                    return '<img class="milestone-list-icon" src="'+$A.get("$Resource.MilestoneDueIcon")+'"></img>';
                                } else {
                                    return null;
                                }
                            }
                        }, {
                            targets: 2,
                            width: "12%",
                            className: "",
                        }, {
                            targets: 3,
                            className: "table-name-column",
                            "createdCell": function (td, cellData, rowData, row, col) {
                                $(td).addClass('truncate');
                                $(td).attr('data-toggle', 'tooltip');
                                $(td).attr('data-placement', 'top');
                                $(td).attr('title', cellData);
                            },
                            "render": function(data, type, full, meta, row) {
                                console.log(full)
                                var page = "";
                                if(full[2] == "Contracts"){
                                   page = "contract-details?recordId="+full[9];
                                }
                                else if(full[2] == "Publications"){
                                  page = "publications-details-page?recordId="+full[9];
                                }else if(full[2] == "Ethics"){
                                    page=""
                                } else{
                                     page = "project-details?recordId="+full[9];
                                }
                                if(page == ""){
                                    return '<div class="truncate rmit-blue-color" title="' + data + '">' + data + '</div>';
                                } else {
                                    return '<div class="truncate"><a class="rmit-blue-color rmit-blue-color_hover" data-container="body" data-toggle="tooltip" data-placement="top" title="' + data + '" href="' + '/Researcherportal/s/' + page + '">' + data + '</a>';
                                }
                             },
                            
                        }, {
                            targets: 4,
                            className: "table-description-column",
                            "createdCell": function (td, cellData, rowData, row, col) {
                                $(td).addClass('truncate');
                                $(td).attr('data-toggle', 'tooltip');
                                $(td).attr('data-placement', 'top');
                                $(td).attr('title', cellData);
                            }
                        }, {
                            targets: 5,
                            className: "table-description-column",
                            "createdCell": function (td, cellData, rowData, row, col) {
                                $(td).addClass('truncate');
                                $(td).attr('data-toggle', 'tooltip');
                                $(td).attr('data-placement', 'top');
                                $(td).attr('title', cellData);
                            }
                        }, {
                            targets: 6,
                            width: "12%",
                            className: "",
                        }, {
                            targets: [1,6],
                            "render": function(data, type, full, meta, row) {
                                if(type === 'display' || type === 'filter'){
                                    if(data !== '')
                                        return moment(data).format('DD/MM/YYYY') ;
                                    else
                                        return '';
                                }
                                
                                return data;
                            }
                        }]
                        
                    });
                    var settings = table.settings()
                    // Handle click on "Load more" button
                    $('#btn-loadMore-Miles').on('click', function() {
                        // Load more data
                        settings[0]._iDisplayLength = settings[0]._iDisplayLength + 25
                        //table.page.loadMore();
                        table.draw()
                    });
                }
            }
        });
        
        //adds the server-side action to the queue        
        $A.enqueueAction(getMilestonesAction);  
            $("document").ready(function() {
            var offset = 250;
            
            var duration = 300;
            
            $(window).scroll(function() {
                if ($(this).scrollTop() > offset) {
                    $("#back-to-top").fadeIn(duration);
                } else {
                    $("#back-to-top").fadeOut(duration);
                }
                
            });
            
            $("#back-to-top").click(function(a) {
                
                a.preventDefault();
                
                $("html, body").animate({
                    scrollTop: 0
                }, duration);
                
                return false;
            });
            
            $('[data-toggle="tooltip"]').tooltip();
        }); 
        
    },
    // method call to toggle impact category dropdown and on edit based on data check the checkbox
    toggleDropdown: function(component, event, helper) {
        var targetCmp = event.currentTarget;
        
        var targetId = targetCmp.id;
        
        var type = targetCmp.dataset.type;
        
        var topicDropdown = $('#' + type + 'Dropdown');
        if ((topicDropdown.css("display") === "none") || (topicDropdown.css("display") === "")) {
            topicDropdown.css("display", "block");
            helper.toggleArrowState('#' + type + 'Dropdown', '#arrow' + type);
            
        } else {
            topicDropdown.css("display", "none");
            helper.toggleArrowState('#' + type + 'Dropdown', '#arrow' + type);
        }
        
        $(document).click(component, function(e) {
            if ($(e.target).closest('#typefilter').length === 0 && $(e.target).closest('#typeDropdown').length === 0 && $(e.target).closest('.input-group-btn').length === 0 && $('#typeDropdown').is(":visible")) {
                $('#typeDropdown').toggle(false);
                helper.toggleArrowState('#typeDropdown', '#arrowtype');
                var topic = $('#typefilter').data('selected');
                $('#typefilter').val(topic);
            }         
            else if ($(e.target).closest('#duedatefilter').length === 0 && $(e.target).closest('#duedateDropdown').length === 0 && $(e.target).closest('.input-group-btn').length === 0 && $('#duedateDropdown').is(":visible")) {
                $('#duedateDropdown').toggle(false);
                helper.toggleArrowState('#duedateDropdown', '#arrowduedate');
                var topic = $('#duedatefilter').data('selected');
                $('#duedatefilter').val(topic);
            }
            
        });
    },
    selectTopic: function(component, event, helper) {
        var targetCmp = event.currentTarget;
        var topic = targetCmp.dataset.value;
        var targetId = targetCmp.parentElement.id;
        var duedateval = $('#duedatefilter').val();
        var typeval = $('#typefilter').val(); 
        var milestones = component.get("v.milestones");
        if (targetId === 'typeDropdown') {
            component.set("v.selectedType", topic);
            $('#typefilter').data('selected', topic);
            $('#typefilter').val(topic);
            $('#typeDropdown').toggle(false);
            helper.toggleArrowState('#typeDropdown', '#arrowtype');
            var typeval = $('#typefilter').val(); 
            helper.fetchFilteredMilesStones(typeval,duedateval,milestones);
        } else if (targetId === 'duedateDropdown') {
            component.set("v.selectedDueDate", topic);
            $('#duedatefilter').data('selected',topic);
            $('#duedatefilter').val(topic);
            $('#duedateDropdown').toggle(false);
            helper.toggleArrowState('#duedateDropdown', '#arrowduedate');  
            var duedateval = $('#duedatefilter').val();
            helper.fetchFilteredMilesStones(typeval,duedateval,milestones);
        }        
    }
})