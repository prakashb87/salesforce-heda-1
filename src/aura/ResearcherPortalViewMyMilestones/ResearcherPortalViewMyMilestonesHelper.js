({
    addRows: function(tableId, dataset) {
        var table = $(tableId).ready().DataTable();
        if (!Array.isArray(dataset))
            dataset = [dataset];
        
        var data = [];
        dataset.forEach(function(milestone) {
            var temp = [];
            
            temp.push("");
            temp.push(milestone["dueDate"] != undefined ? milestone["dueDate"] : '');
            temp.push(milestone["type"] != undefined ? milestone["type"] : '');
            temp.push(milestone["name"] != undefined ? milestone["name"] : '');
            temp.push(milestone["description"] != undefined ? milestone["description"] : '');
            temp.push(milestone["details"] != undefined ? milestone["details"] : '');
            temp.push(milestone["dateCompleted"] != undefined ? milestone["dateCompleted"] : '');
            temp.push(milestone["isDueWithin30Days"] != undefined ? milestone["isDueWithin30Days"] : '');
            temp.push(milestone["isOverDue"] != undefined ? milestone["isOverDue"] : '');
            temp.push(milestone["milestoneTypeRecordId"] != undefined ? milestone["milestoneTypeRecordId"] : '');
            
            data.push(temp);
        });
        
        //table.order.neutral().draw();
        table.rows.add(data).draw();
    },
    
    processData: function(dataset) {
        if (dataset == null || dataset == undefined)
            return [];
        
        if (!Array.isArray(dataset))
            dataset = [dataset];
        
        var data = [];
        
        if (dataset.length !== 0) {
            dataset.forEach(function(milestone) {
                var temp = [];
                
                temp.push("");
                temp.push(milestone["dueDate"] != undefined ? milestone["dueDate"] : '');
                temp.push(milestone["type"] != undefined ? milestone["type"] : '');
                temp.push(milestone["name"] != undefined ? milestone["name"] : '');
                temp.push(milestone["description"] != undefined ? milestone["description"] : '');
                temp.push(milestone["details"] != undefined ? milestone["details"] : '');
                temp.push(milestone["dateCompleted"] != undefined ? milestone["dateCompleted"] : '');
                temp.push(milestone["isDueWithin30Days"] != undefined ? milestone["isDueWithin30Days"] : '');
                temp.push(milestone["isOverDue"] != undefined ? milestone["isOverDue"] : '');
                temp.push(milestone["milestoneTypeRecordId"] != undefined ? milestone["milestoneTypeRecordId"] : '');
                
                data.push(temp);
            });
        }
        
        return data;
    },
    // change arrow up when impact category dropdown is shown and arrow down when dropdown is closed
    toggleArrowState: function(dropdownId, arrowId) {
        if ($(dropdownId).is(':visible')) {
            $(arrowId).removeClass('fa-angle-down');
            $(arrowId).addClass('fa-angle-up');
        } else {
            $(arrowId).removeClass('fa-angle-up');
            $(arrowId).addClass('fa-angle-down');
        }
    },
    
    fetchFilteredMilesStones : function(typeval,duedateval,milestones){
        var filtermilestones =   $.map(milestones,function(val,key){
            if((val.type == typeval || typeval == "") || typeval == $A.get("$Label.c.All")) {
                if(duedateval == $A.get("$Label.c.Due_Date") && val.isDueWithin30Days == true){
                    return val;
                }
                else if(duedateval == $A.get("$Label.c.Milestone_Overdue") && val.isOverDue == true){
                    return val;
                }
                    else if(duedateval == $A.get("$Label.c.All")){
                        return val;
                    }
            }  
        });
        $('#myMilestoneList').DataTable().clear();
        this.addRows("#myMilestoneList", filtermilestones);
    },
    getRecordTypes : function(component, event, helper){
        var getrecordtypesAction = component.get("c.getMyMilestonesRecordTypeList");
        
        getrecordtypesAction.setCallback(this, function(response) {
            var state = response.getState();
            if (state == "SUCCESS") {
                var data = response.getReturnValue();
                data.push($A.get("$Label.c.All"))
                data.sort();
                component.set("v.filterByType",data);
                component.set("v.selectedType", data[0]);
            }
        });
        
        $A.enqueueAction(getrecordtypesAction);
    },
    getcheckInternalDualAccessUser : function(component, event, helper){
        var getcheckInternalDualAccessUserAction = component.get("c.checkInternalDualAccessUser");
        
        getcheckInternalDualAccessUserAction.setCallback(this, function(a) { 
            var state = a.getState();
            if (state == "SUCCESS") {
                 var result = a.getReturnValue();
                if (!$A.util.isUndefined(result)) {
					component.set('v.isInternalDualAccessUser', result);
                }
            }        
        });
          $A.enqueueAction(getcheckInternalDualAccessUserAction);
    }
})