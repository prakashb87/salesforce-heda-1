({
	 // to navigate to respective tab on click of header items
    goToNewPage: function(component, event, helper) {
        var urlEvent = $A.get("e.force:navigateToURL");

        var ctarget = event.currentTarget;
        var id_str = ctarget.dataset.value;

        var targetUrl;

        switch (id_str) {
            case "portalFeedback":
                targetUrl = "/portal-feedback";
                break
            default:
                targetUrl = "/defaultpage?tab=";
        }

        urlEvent.setParams({
            "url": targetUrl
        });

        urlEvent.fire();
    },
    
    onInit: function(component, event, helper){
    	var url = window.location.protocol + '//' + window.location.hostname+'/Researcherportal/s/portal-feedback';
        component.set("v.siteURL", url);
        
        component.set("v.isPortalFeedback", window.location.pathname.includes("portal-feedback"));
        console.log('### on dashboard: ' + document.querySelectorAll('[data-value="dashboard"].tabActive').length > 0);
        // component.set("v.isPortalDashboard", window.location.pathname.includes("portal-feedback"));
    },
    
     addHandler: function(component, event, helper) {
        $.fn.followTo = function(pos) {
            var $this = this,
                $window = $(window);

            $window.scroll(function(e) {
                /*if ($window.scrollTop() > pos) {
                    $this.css({
                        position: 'absolute',
                        bottom: pos
                    });
                }*/
               if (Math.round($('#rp-footer').offset().top) < $('.portal-feedback').offset().top + $('.portal-feedback').height()) {
                    $this.css({
                        //position: 'absolute',
                        bottom: $('#rp-footer').height()
                    });
                }
                if (Math.round($('#rp-footer').offset().top) > $('.portal-feedback').offset().top + $('.portal-feedback').height()) {
                    $this.css({
                        position: 'fixed',
                        bottom: 0
                    });
                } 
            });
        };

        $('.portal-feedback').followTo(250);
    }
})