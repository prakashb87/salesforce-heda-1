({
	 onInit: function(component, event, helper) {
		let isFeedbackPage = window.location.pathname.includes('/portal-feedback')
        component.set('v.hideLeftNavbar', isFeedbackPage)
        component.set('v.hideHeader', isFeedbackPage)
		 let isSupportPage = window.location.pathname.includes('/research-support')
		 component.set('v.hideLeftNavbar', isSupportPage)
		 component.set('v.hideHeader', isSupportPage)

         var pathParts = window.location.pathname.split('/');
        if (pathParts[pathParts.length - 1] === '' && pathParts[pathParts.length - 2] === 's') {
            component.set('v.isDashboard', true);
        }
	}
})