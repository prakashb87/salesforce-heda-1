({
	handleRecordUpdated : function(component, event, helper) {
        var eventParams = event.getParams();
        console.log(eventParams.changeType);
        if(eventParams.changeType === "LOADED") {
            console.log("Record is loaded successfully.");
            var val = component.get("v.simpleLead.Event_type__c");
            var val1 = component.get("v.simpleLead.Email");
            var val2 = component.get("v.simpleLead.matched_school__c");
            console.log(val+','+val1+','+val2);
            
            
            if(val2 == "" || val2 == null){
                console.log("Inside Apex Call1");
                component.set("v.missingVal1",'Please enter matched school.');
                component.set("v.processRender",false);
            } 
            if(val1 == "" || val1 == null){
                console.log("Inside Apex Call2");
                component.set("v.missingVal2",'Please enter email.');
                component.set("v.processRender",false);
            } 
            if(val == "" || val == null){
                console.log("Inside Apex Call3");
                component.set("v.missingVal3",'Please enter Event type.');
                component.set("v.processRender",false);
            }
            console.log(component.get("v.missingVal"));
            if(component.get("v.missingVal") == null || component.get("v.missingVal") == ""){
                
                console.log("Inside Apex Call4");
                var action = component.get("c.eventConversionUnqualified");
                action.setParams({
                    "Id": component.get("v.recordId")
                });
                action.setCallback(this, function(response) {
                    var state = response.getState();
                    console.log('State-->'+state);
                    if(state === "SUCCESS") {
                        var returnValue = response.getReturnValue();
                        console.log('Return From Apex--> '+returnValue);
                        if(returnValue == "Success!"){
                            //component.set('v.successCreate', 'Campaign Created');
                            var dismissActionPanel1 = $A.get("e.force:closeQuickAction");
                        	dismissActionPanel1.fire();
                            var toastEvent = $A.get("e.force:showToast");
                            toastEvent.setParams({
                                "title": "Success!",
                                "message": "The record has been updated successfully.",
                                "type": "success"
                            });
                            toastEvent.fire();
                        }else{
                            console.log(returnValue);
                        }
                        $A.get("e.force:refreshView").fire();
                        
                       
                    } 
                });
                $A.enqueueAction(action);
            }
        }else if(eventParams.changeType === "ERROR"){
            console.log("Error while loading");
            console.log('Error: ' + component.get("v.leadError"));
		}
    }
	
})