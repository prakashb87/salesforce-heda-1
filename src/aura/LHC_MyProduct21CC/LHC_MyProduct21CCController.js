({
    //ECB-2820   My progress page
    myProgress : function(component, event, helper) {
        var action = component.get('c.getProgressDetails');
        
        action.setCallback(this, function(response){
            var result = response.getReturnValue();
            var state = response.getState();
            console.log('Result----------->'+result);
            console.log('State---------->'+state);
            
            if(state ==='SUCCESS'){
                component.set('v.ProgressDetails',result);
            }else{
                var errors = response.getError();
                console.log(errors);
                console.log('Error in Details');
            }
            
        });
        $A.enqueueAction(action);
    },
    
    myDetails : function(component, event, helper) {
        
        var urlEvent = $A.get("e.force:navigateToURL");
        urlEvent.setParams({
            "url": "/mydetails"
            
        });
        urlEvent.fire();     
    },  
    
    purchaseHistory : function(component, event, helper) {
        
        var urlEvent = $A.get("e.force:navigateToURL");
        urlEvent.setParams({
            "url": "/purchasehistory"
            
        });
        urlEvent.fire();     
    },
    
    showAccordion:function(component, event)
    {
        var parent=event.currentTarget.parentElement;
        if (parent !== undefined) {
            if (parent.classList.contains('myplan-content-is-collapsed')) {
                
                parent.classList.remove('myplan-content-is-collapsed');
            } else {
                parent.classList.add('myplan-content-is-collapsed');
                }
            }
            
        },
    
    //ECB-3838 :Click through to Canvas from My dashboard
    canvasRedirection : function(component, event, helper) { 
              
        var action = component.get('c.geCanvasRedirectURL');
            
        action.setCallback(this, function(response) {
            var state = response.getState();
            if (state === "SUCCESS") {
                if(response.getReturnValue() !=null){
                     var urlEvent = $A.get("e.force:navigateToURL");
                    var CanvasRedirect = response.getReturnValue();
                    urlEvent.setParams({
                        "url": CanvasRedirect          
                    });
                    urlEvent.fire();  
                }
                
            }    
        });
        
        $A.enqueueAction(action); 
    },
    
    
    
})