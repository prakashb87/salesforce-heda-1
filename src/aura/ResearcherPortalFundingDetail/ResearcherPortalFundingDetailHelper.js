({
    //based on expense method response draw the expense table
    addRows: function(tableId, dataset) {
        var table = $(tableId).ready().DataTable();

        if (!Array.isArray(dataset.projectFundingList))
            dataset.projectFundingList = [dataset.projectFundingList];

        var formatter = new Intl.NumberFormat('en-US', {
            style: 'currency',
            currency: 'USD',
            minimumFractionDigits: 2,
        });

        var data = [];
        dataset.projectFundingList.forEach(function(expense) {
            var temp = [];

            temp.push(expense["costElememtDescription"] != undefined ? expense["costElememtDescription"] : '');
            temp.push(expense["income"] != undefined && expense["income"] != '0' ? formatter.format(expense["income"]) : '');
            temp.push(expense["salariesOncosts"] != undefined && expense["salariesOncosts"] != '0' ? formatter.format(expense["salariesOncosts"]) : '');
            temp.push(expense["otherOperatingExpenses"] != undefined && expense["otherOperatingExpenses"] != '0' ? formatter.format(expense["otherOperatingExpenses"]) : '');
            temp.push(expense["capital"] != undefined && expense["capital"] != '0' ? formatter.format(expense["capital"]) : '');
            temp.push('');
            temp.push('');

            data.push(temp);
        });
        
        //Regroup the expense items based on the expense type
        var incomes = [];
        var salaries = [];
        var otheroperating = [];
        var capitals = [];
        
        data.forEach(function(item){
        	if(item[1]!==''){
        		incomes.push(item);
        		return;
        	}else if(item[2]!==''){
        		salaries.push(item);
        		return;
        	}else if(item[3]!==''){
        		otheroperating.push(item);
        		return;
        	}else if(item[4]!==''){
        		capitals.push(item);
        		return;
        	}
        });

        table.clear().draw();
        var preTable = ["Total", formatter.format(dataset.totalIncome), formatter.format(dataset.totalSalariesOncosts), formatter.format(dataset.totalOtherOperatingExpenses), formatter.format(dataset.totalcapital), formatter.format(dataset.totalFundingExpense), formatter.format(dataset.remainingBalance)];

        table.row.add(preTable).draw();

        table.rows.add(incomes);
        table.rows.add(salaries);
        table.rows.add(otheroperating);
        table.rows.add(capitals).draw();
    },
    // method call to get project details based on project selection and call budget and expense for the same project.
    getProjectDetails: function(component, recordId, toggleArrow) {
        var getProjectDetailsAction = component.get("c.getProjectDetailViewByProjectId");
        getProjectDetailsAction.setParams({
            projectId: recordId
        });
        //Setting the Callback
        getProjectDetailsAction.setCallback(this, function(a) {
            //get the response state
            var state = a.getState();

            //check if result is successfull
            if (state == "SUCCESS") {
                component.set("v.projectSelected", true);
                $("#expenseList").toggle(true);
                this.getBudgetDetails(component, recordId);
                this.getExpensesDetails(component, recordId, 'All');
                var result = a.getReturnValue();
                if (!$A.util.isUndefined(result)) {
                    var parseResult = JSON.parse(result);
                    var yearList = this.generateFilterList(parseResult.projectStartDate, parseResult.projectEndDate);
                    
                    component.set("v.filterYears", yearList); 
                    component.set("v.selectedType", yearList[0]);
                    parseResult.projectEndDate = parseResult.projectEndDate != undefined ? moment(parseResult.projectEndDate).format('DD/MM/YYYY') : '';
                    parseResult.projectStartDate = parseResult.projectStartDate != undefined ? moment(parseResult.projectStartDate).format('DD/MM/YYYY') : '';
                    parseResult.lastmodifiedDate = parseResult.lastmodifiedDate != undefined ? moment(parseResult.lastmodifiedDate).format('DD/MM/YYYY') : '';
                    component.set("v.projectRecord", parseResult);
                    if (toggleArrow) {
                        this.toggleArrowState();
                    }
                    

                }
            } else if (state == "ERROR") {
                alert('Error in calling server side action');
            }
        });

        //adds the server-side action to the queue        
        $A.enqueueAction(getProjectDetailsAction);
    },

    //method call to get Expense details and update the expense table based on response    
    getExpensesDetails: function(component, recordId, year) {

        if (!$.fn.dataTable.isDataTable('#expenseDetailList')) {
            var table = $('#expenseDetailList').DataTable({
                "data": [],
                "language": {
                    "emptyTable": "No expenses report avialable for viewing."
                },
                "dom": '<"top"i>rt<"bottom"><"clear">',
                "columns": [{
                    title: '<div class="text-left">' + $A.get("$Label.c.Cost_Element") + '</div>'
                }, {
                    title: $A.get("$Label.c.Income")
                }, {
                    title: $A.get("$Label.c.Salaries_and_Oncosts")
                }, {
                    title: $A.get("$Label.c.Other_Operating_Expenses")
                }, {
                    title: $A.get("$Label.c.Capital")
                }, {
                    title: $A.get("$Label.c.Total_Expense")
                }, {
                    title: $A.get("$Label.c.Balance")
                }],
                "info": false,
                "paging": false,
                "searching": false,
                "ordering": false,
                "dom": 'frt',
                "headerCallback": function(thead, data, start, end, display) {
                    $(thead).find('th').eq(0).addClass('border-right-0');
                },
                "rowCallback": function(row, data) {
                    if (data[0] == "Total") {
                        $('td', row).addClass('font-weight-bold');
                    }
                },
                "columnDefs": [{
                    targets: 0,
                    className: "text-left first-column-border",
                }]

            });

            var formatter = new Intl.NumberFormat('en-US', {
                style: 'currency',
                currency: 'USD',
                minimumFractionDigits: 2,
            });

            var data = ["Total", formatter.format(0), formatter.format(0),
                formatter.format(0), formatter.format(0),
                formatter.format(0), formatter.format(0)
            ];

            table.row.add(data).draw(); 
        }


        var getExpensesTableAction = component.get("c.getProjectFundingExpenseDetails");

        getExpensesTableAction.setParams({
            projectId: recordId,
            selectedYear: year
        });

        getExpensesTableAction.setCallback(this, function(response) {
            var formatter = new Intl.NumberFormat('en-US', {
                style: 'currency',
                currency: 'AUD',
                minimumFractionDigits: 2,
            });


            if (response.getState() == "SUCCESS") {
                var expenseTable = JSON.parse(response.getReturnValue());
                this.addRows("#expenseDetailList", expenseTable);
                var lastRefreshedDate = expenseTable.lastRefreshedDate != undefined ? moment(expenseTable.lastRefreshedDate).format('DD/MM/YYYY') : '';
                component.set("v.lastRefreshedDate", lastRefreshedDate);

            } else if (state == "ERROR") {
                var table = $("#expenseDetailList").DataTable();

                var data = ["Total", formatter.format(0), formatter.format(0), formatter.format(0), formatter.format(0), formatter.format(0), formatter.format(0)];

                table.row.add(data).draw();
            }
        });

        $A.enqueueAction(getExpensesTableAction);
    },

    // method call to get project budget details , format the currency and provide with based on number of years
    getBudgetDetails: function(component, recordId) {
        var getProjectBudgetAction = component.get("c.getProjectFundingBudgetDetails");

        getProjectBudgetAction.setParams({
            projectId: recordId
        });
        //Setting the Callback
        getProjectBudgetAction.setCallback(this, function(a) {
            //get the response state
            var state = a.getState();

            //check if result is successfull
            if (state == "SUCCESS") {
                var result = a.getReturnValue();

                var formatter = new Intl.NumberFormat('en-US', {
                    style: 'currency',
                    currency: 'USD',
                    minimumFractionDigits: 0,
                });

                if (!$A.util.isUndefined(result)) {
                    result = JSON.parse(result);
                    var totalPjtFundAwardedBudget  = 0;
                    for (var i = 0; i < result.length; i++) {
                        for (var j = 0; j < result[i].projectFundingBudgetList.length; j++) {
                            result[i].projectFundingBudgetList[j].fundingAppliedBudget = formatter.format(result[i].projectFundingBudgetList[j].fundingAppliedBudget);
                            result[i].projectFundingBudgetList[j].fundingReceivedBudget = formatter.format(result[i].projectFundingBudgetList[j].fundingReceivedBudget);
                        }
						totalPjtFundAwardedBudget  = totalPjtFundAwardedBudget+result[i].totalFundingReceivedBudget;
                        console.log(totalPjtFundAwardedBudget)
                        result[i].totalFundingAppliedBudget = formatter.format(result[i].totalFundingAppliedBudget);
                        result[i].totalFundingReceivedBudget = formatter.format(result[i].totalFundingReceivedBudget);
						
                        switch (result[i].projectFundingBudgetList.length) {
                            case 1:
                                result[i].width = "38%";
                                break;
                            case 2:
                                result[i].width = "43%";
                                break;
                            case 3:
                                result[i].width = "57%";
                                break;
                            case 4:
                                result[i].width = "70%";
                                break;
                            case 5:
                                result[i].width = "80%";
                                break;
                            case 6:
                                result[i].width = "90%";
                                break;
                            default:
                                result[i].width = "90%";
                        }					
                     }
                    component.set("v.fundSchemes", result);
                    component.set("v.totalPjtAwaredBudget",formatter.format(totalPjtFundAwardedBudget));
                }
            } else if (state == "ERROR") {
                alert('Error in calling server side action');
            }
        });

        //adds the server-side action to the queue        
        $A.enqueueAction(getProjectBudgetAction);
    },

    // on close of dropdown hide the list of projects
    restoreProjectList: function() {
        var a, i;
        a = $("#pjtDropdown li");
        for (i = 0; i < a.length; i++) {
            a[i].style.display = "";
        }
    },
    
    generateFilterList: function(startDate, endDate) {
        var startYear = parseInt(startDate.substring(0,4));
        var endYear = parseInt(endDate.substring(0,4));
        
        startYear = startYear - 1;
        endYear = endYear + 1;
        
        var yearList = [];
        
        while(endYear>=startYear){
            yearList.push(endYear);
            endYear = endYear - 1;
        }
        
        /*if(startYear === endYear){
            yearList.push(startYear);
        }else {
            while(endYear>=startYear){
                yearList.push(endYear);
                endYear = endYear - 1;
            }
        }*/
        //yearList[0] = "All"; 
        yearList.unshift("All")
        return yearList;
    },
       // change arrow up when impact category dropdown is shown and arrow down when dropdown is closed
    toggleArrowState: function(dropdownId, arrowId) {
         if ($(dropdownId).is(':visible')) {
            $(arrowId).removeClass('fa-angle-down');
            $(arrowId).addClass('fa-angle-up');
        } else {
            $(arrowId).removeClass('fa-angle-up');
            $(arrowId).addClass('fa-angle-down');
        }
    },
        getcheckInternalDualAccessUser : function(component, event, helper){
        var getcheckInternalDualAccessUserAction = component.get("c.checkInternalDualAccessUser");
        
        getcheckInternalDualAccessUserAction.setCallback(this, function(a) { 
            var state = a.getState();
            if (state == "SUCCESS") {
                 var result = a.getReturnValue();
                if (!$A.util.isUndefined(result)) {
					component.set('v.isInternalDualAccessUser', result);
                }
            }        
        });
          $A.enqueueAction(getcheckInternalDualAccessUserAction);
    }

})