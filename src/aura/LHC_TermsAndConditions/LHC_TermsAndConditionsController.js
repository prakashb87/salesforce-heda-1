({	
    doInit : function(component,event,helper){
        
        var courseName = component.searchParam("courseName");
        console.log('c-->'+courseName);
        var action = component.get('c.getTermsAndConditions');
        console.log('ac-->'+action);
        action.setParams({
            courseName : courseName,
        });
        
        action.setCallback(this, function(a){
            console.log('inside set call back');
            if(a.getState() == "SUCCESS"){
                console.log('tc-->'+a.getReturnValue().Terms_and_ConditionId__r.Long_Description__c);
                component.set("v.termsDetail",a.getReturnValue().Terms_and_ConditionId__r.Long_Description__c);
            }else{
                console.log('ERROR in T&C');
            }
        });
        
        $A.enqueueAction(action);
    },
    
    
    
    searchParam : function(component, event, helper) {
        
        var params = event.getParam('arguments');
        var name;
        if(params){
            name = params.param1;
        }
        console.log('name:::'+name);
        name = name.replace(/[\[]/, '\\[').replace(/[\]]/, '\\]');
        var regex = new RegExp('[\\?&]' + name + '=([^&#]*)');
        var results = regex.exec(location.search);
        return results === null ? '' : decodeURIComponent(results[1].replace(/\+/g, ' '));
        
    },
    
    NavigateToCartComp : function(component,event,helper){
        
        var urlEvent = $A.get("e.force:navigateToURL");
        urlEvent.setParams({
            "url": "/viewcart"
        });
        urlEvent.fire();
        
    },
    
})