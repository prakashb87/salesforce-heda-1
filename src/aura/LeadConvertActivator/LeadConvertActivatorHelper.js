({
    checkProfile: function(component,event,helper){
        var apexaction = component.get("c.verifyProfile");
        apexaction.setCallback(this,function(response){
            var state = response.getState();
            if(state === "SUCCESS"){
                var returnVal = response.getReturnValue();                
                if(returnVal){
                    this.convertLead(component,event,helper);
                }else{
                    component.set("v.message",'Sorry! you cannot convert the lead.');
            		component.set("v.processRender",false);
                }
            }
        });
        $A.enqueueAction(apexaction);
    },
    convertLead: function(component,event,helper){
        var leadRecordObject = component.get("v.simpleLead");
        if(leadRecordObject.Matched_Contact__c == null){
            component.set("v.message",'Error! Please enter a contact under \'Activator Lead Conversion\' to continue');
            component.set("v.processRender",false);           
        }else{
            var action = component.get("c.convertActivatorLead");
            action.setParams({
                "idforConversion": leadRecordObject
            });              
            
            var navEvent = $A.get("e.force:navigateToURL");
            action.setCallback(this, function(response) {
                var state = response.getState();
                console.log(state);
                if(state === "SUCCESS") {                
                    var returnValforLead = response.getReturnValue();
                    var result = returnValforLead.split(','); 
                    if(result[0]=='true'){
                        component.set("v.processRender",true);
                        navEvent.setParams({
                            "url": "/"+result[2]
                        });
                        var dismissActionPanel = $A.get("e.force:closeQuickAction").fire;
                        navEvent.fire();
                    }                       
                }else{ 
                    var errors = response.getError(); 
                    component.set("v.message",errors[0].message);
                    component.set("v.processRender",false);
                }
            });    
            $A.enqueueAction(action);
        }
    }
})