({
    
    doInit : function(component,event,helper) {
        
        this.getCitizenshipStatus(component,event,helper);
        this.getContactDetails(component,event,helper);
        this.getresult(component,event,helper);
        this.getAddressDetails(component,event,helper);
    },
    
    
    getCitizenshipStatus :function(component,event,helper){
        console.log('getCitizenshipStatus'); 
        var action = component.get("c.getCitizenship");
        var opts=[];
        opts.push({"class": "optionClass", label: "Choose Citizenship Status", value: ""});
        action.setCallback(this, function(a) {
            for(var i=0;i< a.getReturnValue().length;i++){
                opts.push({"class": "optionClass", label: a.getReturnValue()[i], value: a.getReturnValue()[i]});
            }
            component.set("v.citizenshipOptions",opts);
        });
        $A.enqueueAction(action);
    },
    
    
    
    upsertHomeAddress : function(component,event,helper){
       
        var homeAddress =  component.get("v.HomeAddressDetails");
        
        var contact = component.get("v.MyDetails");
        
        homeAddress.Last_Name__c = contact.LastName;
        homeAddress.First_Name__c = contact.FirstName;
        var action4 = component.get("c.upsertHomeAddressDetails");
        action4.setParams({ homeAddress : homeAddress});
        action4.setCallback(this, function(response) {
            var state = response.getState();
            console.log('Status--------->'+state);
            if (state === "SUCCESS") {
                this.NavigateToFinalCart(component,event,helper);
            }
            else{
                console.log('state Error');
            }
            
        });
        $A.enqueueAction(action4);
        
    },
    
    NavigateToFinalCart : function(component,event,helper){

        var finalProductBasket = component.get("v.ProductBasket");
        console.log("detail12:::"+finalProductBasket.BasketNumber);
        console.log("detail12:::"+finalProductBasket.TotalPrice);
        
        
        if(finalProductBasket.TotalPrice == '0.00'){
            console.log('price 0');
            var basketNumber = finalProductBasket.BasketNumber;
            console.log('price 0'+basketNumber);
            
            var enrollaction = component.get("c.confirmEnroll");
            enrollaction.setParams({
                "receiptNo" : "n/a",
                "basketNo"  : basketNumber
            }); 
            
            console.log('price action');
            enrollaction.setCallback(this, function(response){
                console.log('price res');
                var result = response.getReturnValue();
                console.log('price 0'+result);
                var flag = result.Status;
                
                var state = response.getState();
                if (state === "SUCCESS") {
                    console.log('Enroll');
                    if(flag === "Pass"){
                        var urlEvent = $A.get("e.force:navigateToURL");
                        urlEvent.setParams({
                            "url": "/successpage",
                            
                        });
                        urlEvent.fire(); 
                    }else{
                        var errors = response.getError();
                       
                    }
                    console.log(result);
                }
            });
             $A.enqueueAction(enrollaction);
        }else{
            var finalProductBasket = component.get("v.ProductBasket");
            
            var orderId = finalProductBasket.BasketNumber;
            var amount = finalProductBasket.TotalPrice;
            var action = component.get("c.getEmail");
            action.setCallback(this, function(response){
                var state = response.getState();
                if(state === 'SUCCESS'){
                    var email= response.getReturnValue();
                    //ECB-3702 Open OneStop link in same window
                    var urlEvent1 = $A.get("$Label.c.PAYNOW_ONESTOP_URL")+'&MPB='+orderId+'&EMAIL='+email+'&UNITAMOUNTINCTAX='+amount;
                    window.open(urlEvent1,'_top');
                }else{
                    console.log('Email Missing');
                }
            });
             $A.enqueueAction(action);
        }  
       
    },
    
    homeFieldsValidation : function(component,event,helper){
        console.log('homeFieldsValidation');
        
        var homeValid = component.find('homeFields').reduce(function (validSoFar, inputCmp) {
            inputCmp.showHelpMessageIfInvalid();
            return validSoFar && !inputCmp.get('v.validity').valueMissing;
        }, true);
        if(homeValid){
            helper.upsertContact(component);
            helper.upsertHomeAddress(component);
        }else{
            console.log('Please complete all fields');
        }
    },
    
    getContactDetails : function(component,event,helper){
        var action = component.get('c.getContactDetails');
        
        action.setCallback(this, function(response){
            var result = response.getReturnValue();
            var state = response.getState();
            
            if(state ==='SUCCESS'){
                component.set('v.MyDetails',result);
                component.set('v.MyDetails.Email_Subscription__c',false);
                if(result.Birthdate == '' || result.Birthdate == null){
                    component.set('v.birthdateDisable',false);
                    component.set('v.MyDetails.Birthdate','');
                }
                if(result.hed__Gender__c == '' || result.hed__Gender__c == null){
                    component.set('v.genderDisable',false);
                    component.set('v.MyDetails.hed__Gender__c','');
                }
                if(result.Citizenship_Status__c == '' || result.Citizenship_Status__c == null){
                    component.set('v.citizenshipDisable',false);
                    component.set('v.MyDetails.Citizenship_Status__c','');
                }
                if(result.Legal_First_Name__c == '' || result.Legal_First_Name__c == null){
                    component.set('v.legalFirstDisable',false);
                    component.set('v.MyDetails.Legal_First_Name__c','');
                }
                if(result.Legal_Last_Name__c == '' || result.Legal_Last_Name__c == null){
                    component.set('v.legalLastDisable',false);
                    component.set('v.MyDetails.Legal_Last_Name__c','');
                }
            }else{
                var errors = response.getError();
                console.log('Error in Details');
            }
            
        });
        $A.enqueueAction(action);
    },
    
    upsertContact : function(component,event,helper){
        console.log('contact upsert');
        var contact = component.get("v.MyDetails");
        var action6 = component.get("c.upsertContactDetails");
        action6.setParams({ contactUpdate : contact});
        action6.setCallback(this, function(response) {
            var state = response.getState();
            console.log('Status--------->'+state);
            if (state === "SUCCESS") {
                console.log("From server: " + JSON.stringify(response.getReturnValue()));
            }
            else{
                console.log('state Error');
            }
        });
        $A.enqueueAction(action6);
    },
    
    getresult : function(component,event,helper){
        
        var action = component.get('c.getresult');
        action.setCallback(this, function(response) {
            var state = response.getState();
            if (state === "SUCCESS") {      
                component.set("v.ProductBasket",response.getReturnValue());
            }
        });
         $A.enqueueAction(action);
    },
    
     getAddressDetails : function(component,event,helper){
        var action2 = component.get('c.getAddressDetails');
        
        action2.setCallback(this, function(response){
            var result2 = response.getReturnValue();
            var state = response.getState();
            
            if(state ==='SUCCESS'){
                component.set('v.HomeAddressDetails',result2);
                console.log('-----Success-----');
            }else{
                var errors = response.getError();
                console.log('Error in Details');
            }
            
        });
        $A.enqueueAction(action2);
    },
    
})