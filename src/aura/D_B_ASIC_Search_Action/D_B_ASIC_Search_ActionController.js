({
	handleRecordUpdated : function(component, event, helper) {
        var eventParams = event.getParams();
        if(eventParams.changeType === "LOADED") {
            console.log("Record is loaded successfully.");
            var val = component.get("v.simpleLead.Company");
            console.log(val);
            var urlEvent = $A.get("e.force:navigateToURL");
            urlEvent.setParams({
                "url": "http://www.dnb.com.au/express/results/asic_list.asp?Name="+val
            });
    		urlEvent.fire();
            // Close the action panel
            var dismissActionPanel = $A.get("e.force:closeQuickAction");
            dismissActionPanel.fire();
        }
    }
	
})