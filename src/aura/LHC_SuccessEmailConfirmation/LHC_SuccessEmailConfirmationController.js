({
    loadData : function(component, event, helper) {
        var email = component.searchParam("email");
        component.set("v.message",email); 
    },
    
    //Onclick of the Resend Activation Email
    resendEmail :function(component, event, helper) {
        
        var contactId = component.searchParam("contactId");
        console.log('id--->'+contactId);
        var action = component.get('c.resendConfirmationEmail');
        action.setParams({
            "contactId" : contactId
        });
        action.setCallback(this, function(response){
            var result = response.getReturnValue();
            console.log('result--->'+result);
            var state = response.getState();
            console.log('State --->'+state);
            if(state === "SUCCESS" && result === true){
                component.set("v.finalMessage",'Activation email has been resent'); 
                component.set("v.isOpen", true);
            }else{
                component.set("v.finalMessage",'Activation email was not able to be sent. Please try again'); 
                component.set("v.isOpen", true);
            }
        });
        $A.enqueueAction(action);
    },
    
    searchParam : function(component, event, helper) {
        
        var params = event.getParam('arguments');
        var name;
        if(params){
            name = params.param1;
        }
        console.log('name:::'+name);
        name = name.replace(/[\[]/, '\\[').replace(/[\]]/, '\\]');
        var regex = new RegExp('[\\?&]' + name + '=([^&#]*)');
        var results = regex.exec(location.search);
        return results === null ? '' : decodeURIComponent(results[1].replace(/\+/g, ' '));
        
    },
    
    closeModel: function(component, event, helper) {
        component.set("v.isOpen", false);
    },
    
    
})