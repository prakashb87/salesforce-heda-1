({
	handleRecordSent : function(component, event, helper) {
        var recId = component.get("v.recordId");
        var ssId = component.get("v.simpleCourse.SIS_Course_Id__c");
        console.log(ssId);
        var action = component.get("c.returntoIpass");
        action.setParams({
            "incomingId": recId,
            "ssid":ssId
        });
        action.setCallback(this, function(response){
            var loadState = response.getState();
            if(loadState === 'SUCCESS')
            {
                //console.log(response.getReturnValue());
                if(response.getReturnValue() == 'Error2')
                {
                    console.log('AAAAA');
                     var dismissActionPanel = $A.get("e.force:closeQuickAction");
	   				dismissActionPanel.fire();
                    var resultsToast = $A.get("e.force:showToast");
                     resultsToast.setParams({
                        "title": "",
                        "message": $A.get("$Label.c.No_user_Permission"),
                        "type": "Error"
                    });
                    resultsToast.fire();
                }
                else
                {
                    console.log('ZZZZ');
                    
					$A.get("e.force:closeQuickAction").fire();
					
                    
                }
                //console.log('Succeess');
                //console.log(ssId);
                //$A.get("e.force:closeQuickAction").fire();
            }
        });
        $A.enqueueAction(action);
		console.log('handleRecordSent'+ recId);
	}
})