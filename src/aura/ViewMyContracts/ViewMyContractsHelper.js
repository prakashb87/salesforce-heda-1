({
    loadMyContracts : function(dataSet, component, event) {
        var returnData = [];
        var pendArr = dataSet.filter(function(el) {
            if (el.status === "Pending") {
                return el
            }
        });
        var nonPendArr = dataSet.filter(function(el) {
            if (el.status !== "Pending") {
                return el
            }
        });
        dataSet = pendArr.concat(nonPendArr);
        $.each(dataSet, function(index, value) {
            var rowData = [];
            rowData.push(value.contractCode != undefined ? value.contractCode : '');
            rowData.push(value.contractTitle != undefined ? value.contractTitle : '');
            rowData.push(value.status != undefined ? value.status : '');
            rowData.push(value.submittedDate != undefined ? value.submittedDate : '');
            rowData.push(value.currentLocation != undefined ? value.currentLocation : '');
            rowData.push(value.fullyExecutedDate != undefined ? value.fullyExecutedDate : '');
            rowData.push(value.primaryContact != undefined ? value.primaryContact : '');
            rowData.push(value.researchType != undefined ? value.researchType : '');
            rowData.push(value.agreementType != undefined ? value.agreementType : '');
            rowData.push(value.contractId != undefined ? value.contractId : '');
            
            returnData.push(rowData);
        });
        component.set("v.contractsList", returnData);
        //Date Filtering Code Ends      
        jQuery("document").ready(function() {
            var table = $('#myContractsList').DataTable({
                data: returnData,
                "pageLength": 25,
                "language": {
                    "emptyTable": $A.get("$Label.c.YoudonothaveanyContracts")
                },
                "dom": '<"top"i>rt<"bottom"><"clear">',
                "columns": [{
                    title: $A.get("$Label.c.contractCode")
                }, {
                    title: $A.get("$Label.c.Title")
                }, {
                    title: $A.get("$Label.c.Status")
                }, {
                    title: $A.get("$Label.c.submittedDate")
                }, {
                    title: $A.get("$Label.c.process_stage")
                }, {
                    title: $A.get("$Label.c.fullyExecutedDate")
                }, {
                    title: $A.get("$Label.c.Primary_Contact")
                }, {
                    title: $A.get("$Label.c.researchType")
                },  {
                    title: $A.get("$Label.c.formofAgreement")
                }],
                "info": false,
                "dom": 'frt',
                "drawCallback": function() {
                    // Show or hide "Load more" button based on whether there is more data available
                    $('#btnLoadMore').toggle(this.api().page.hasMore());
                },
                "aaSorting": [],
                "columnDefs": [{
                    targets: [2,4,6,7,8],
                    "render": function(data, type, full, meta) {
                        return '<div class="truncate"><a data-container="body" data-toggle="tooltip" data-placement="top" title="' + data + '"</a>' + data + '</div>'
                    }
                },
                               {
                                   targets: [1],
                                   type: 'natural',
                                   "render": function(data, type, full, meta) {
                                        return '<div class="truncate"><a class="rmit-blue-color rmit-blue-color_hover" data-container="body" data-toggle="tooltip" data-placement="top" title="' + data + '" href="' + '/Researcherportal/s/contract-details?recordId=' + full[9] + '">' + data + '</a>';
                                   }
                               },
                               {
                                   targets: [3,5],
                                   "render": function(data, type, full, meta, row) {
                                       if(type === 'display' || type === 'filter'){
                                           if(data !== '')
                                               return moment(data).format('DD/MM/YYYY') ;
                                           else
                                               return '';
                                       }
                                       
                                       return data;
                                   }
                               }]
                
            });
            
            // Handle click on "Load more" button
            $('#btnLoadMore').on('click', function() {
                // Load more data
                table.page.loadMore();
            });
        });
    },
    //Method call to fetch Impact category list
    fetchContractStatusValue: function(component, fieldName, elementId) {
        var action = component.get("c.getContractStatusValue");
        action.setCallback(this, function(response) {            
            if (response.getState() == "SUCCESS") {
                var filterByStatus = response.getReturnValue();        
                filterByStatus.sort();
                filterByStatus.unshift($A.get("$Label.c.All"))
                component.set("v.filterByStatus", filterByStatus);  
                component.set("v.selectedStatus", filterByStatus[0]);                
            }
        });
        $A.enqueueAction(action);
    },
    //Method call to fetch Category list
    fetchContractResearchTypeValue : function(component, fieldName, elementId) {
        var action = component.get("c.getContractResearchTypeValue");
        action.setCallback(this, function(response) {           
            if (response.getState() == "SUCCESS") {
                var filterByType = response.getReturnValue();
                filterByType.push($A.get("$Label.c.All"))
                filterByType.sort();
                component.set("v.filterByType", filterByType); 
                component.set("v.selectedType", filterByType[0]);
            }
        });
        $A.enqueueAction(action);
    },
    // change arrow up when impact category dropdown is shown and arrow down when dropdown is closed
    toggleArrowState: function(dropdownId, arrowId) {
        if ($(dropdownId).is(':visible')) {
            $(arrowId).removeClass('fa-angle-down');
            $(arrowId).addClass('fa-angle-up');
        } else {
            $(arrowId).removeClass('fa-angle-up');
            $(arrowId).addClass('fa-angle-down');
        }
    },   
    //Filter contracts based on the dropdown selection 
    fetchFilteredContracts : function(status,type,contracts){
        var filtermilestones =   $.map(contracts,function(val,key){
            if((val.researchType == type || type == "") || type == $A.get("$Label.c.All")) {
                if(val.status == status){
                    return val;
                }
                else if(status == $A.get("$Label.c.All")){
                    return val;
                }
            }  
        });
        $('#myContractsList').DataTable().clear();
        this.addRows("#myContractsList", filtermilestones);
    },
    
    // redraw table based on filtered result
    addRows: function(tableId, dataset) {
        var table = $(tableId).ready().DataTable();
        if (!Array.isArray(dataset))
            dataset = [dataset];
        
        var data = [];
        dataset.forEach(function(milestone) {
            var temp = [];
            temp.push(milestone["contractCode"] != undefined ? milestone["contractCode"] : '');
            temp.push(milestone["contractTitle"] != undefined ? milestone["contractTitle"] : '');
            temp.push(milestone["status"] != undefined ? milestone["status"] : '');
            temp.push(milestone["submittedDate"] != undefined ? milestone["submittedDate"] : '');
            temp.push(milestone["currentLocation"] != undefined ? milestone["currentLocation"] : '');
            temp.push(milestone["fullyExecutedDate"] != undefined ? milestone["fullyExecutedDate"] : '');
            temp.push(milestone["primaryContact"] != undefined ? milestone["primaryContact"] : '');
            temp.push(milestone["researchType"] != undefined ? milestone["researchType"] : '');
            temp.push(milestone["agreementType"] != undefined ? milestone["agreementType"] : '');
            temp.push(milestone["contractId"] != undefined ? milestone["contractId"] : '');
            
            data.push(temp);
        });
        table.rows.add(data).draw();
    },
        getcheckInternalDualAccessUser : function(component, event, helper){
        var getcheckInternalDualAccessUserAction = component.get("c.checkInternalDualAccessUser");
        
        getcheckInternalDualAccessUserAction.setCallback(this, function(a) { 
            var state = a.getState();
            if (state == "SUCCESS") {
                 var result = a.getReturnValue();
                if (!$A.util.isUndefined(result)) {
					component.set('v.isInternalDualAccessUser', result);
                }
            }        
        });
          $A.enqueueAction(getcheckInternalDualAccessUserAction);
    }
})