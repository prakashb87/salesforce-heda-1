<?xml version="1.0" encoding="UTF-8"?>
<CustomObject xmlns="http://soap.sforce.com/2006/04/metadata">
    <actionOverrides>
        <actionName>Accept</actionName>
        <type>Default</type>
    </actionOverrides>
    <actionOverrides>
        <actionName>CancelEdit</actionName>
        <type>Default</type>
    </actionOverrides>
    <actionOverrides>
        <actionName>Clone</actionName>
        <type>Default</type>
    </actionOverrides>
    <actionOverrides>
        <actionName>Delete</actionName>
        <type>Default</type>
    </actionOverrides>
    <actionOverrides>
        <actionName>Edit</actionName>
        <type>Default</type>
    </actionOverrides>
    <actionOverrides>
        <actionName>List</actionName>
        <type>Default</type>
    </actionOverrides>
    <actionOverrides>
        <actionName>New</actionName>
        <type>Default</type>
    </actionOverrides>
    <actionOverrides>
        <actionName>SaveEdit</actionName>
        <type>Default</type>
    </actionOverrides>
    <actionOverrides>
        <actionName>Tab</actionName>
        <type>Default</type>
    </actionOverrides>
    <actionOverrides>
        <actionName>View</actionName>
        <type>Default</type>
    </actionOverrides>
    <allowInChatterGroups>false</allowInChatterGroups>
    <compactLayoutAssignment>SYSTEM</compactLayoutAssignment>
    <deploymentStatus>Deployed</deploymentStatus>
    <enableActivities>false</enableActivities>
    <enableBulkApi>true</enableBulkApi>
    <enableFeeds>true</enableFeeds>
    <enableHistory>true</enableHistory>
    <enableLicensing>false</enableLicensing>
    <enableReports>true</enableReports>
    <enableSearch>true</enableSearch>
    <enableSharing>true</enableSharing>
    <enableStreamingApi>true</enableStreamingApi>
    <fields>
        <fullName>AQF_Code__c</fullName>
        <externalId>false</externalId>
        <label>AQF Code</label>
        <length>255</length>
        <required>false</required>
        <trackFeedHistory>false</trackFeedHistory>
        <trackHistory>false</trackHistory>
        <trackTrending>false</trackTrending>
        <type>Text</type>
        <unique>false</unique>
    </fields>
    <fields>
        <fullName>Academic_Career__c</fullName>
        <externalId>false</externalId>
        <inlineHelpText>A four-character code that represents the grouping of courses and programs at RMIT. Careers at RMIT are: undergraduate (UGRD), postgraduate (PGRD), TAFE, research (RSCH), preparatory (PREP), non-award (NONA) and continuing education (CONT).</inlineHelpText>
        <label>Academic Career</label>
        <length>18</length>
        <required>false</required>
        <trackFeedHistory>false</trackFeedHistory>
        <trackHistory>true</trackHistory>
        <trackTrending>false</trackTrending>
        <type>Text</type>
        <unique>false</unique>
    </fields>
    <fields>
        <fullName>Academic_Organization__c</fullName>
        <deleteConstraint>SetNull</deleteConstraint>
        <externalId>false</externalId>
        <label>Academic Organization</label>
        <referenceTo>Account</referenceTo>
        <relationshipLabel>Plans (Academic Organization)</relationshipLabel>
        <relationshipName>Plans1</relationshipName>
        <required>false</required>
        <trackFeedHistory>false</trackFeedHistory>
        <trackHistory>false</trackHistory>
        <trackTrending>false</trackTrending>
        <type>Lookup</type>
    </fields>
    <fields>
        <fullName>Academic_Plan_Type__c</fullName>
        <externalId>false</externalId>
        <label>Academic Plan Type</label>
        <required>false</required>
        <trackFeedHistory>false</trackFeedHistory>
        <trackHistory>false</trackHistory>
        <trackTrending>false</trackTrending>
        <type>Picklist</type>
        <valueSet>
            <valueSetDefinition>
                <sorted>false</sorted>
                <value>
                    <fullName>Default</fullName>
                    <default>false</default>
                    <label>Default</label>
                </value>
            </valueSetDefinition>
        </valueSet>
    </fields>
    <fields>
        <fullName>Academic_Plan__c</fullName>
        <externalId>false</externalId>
        <label>Academic Plan</label>
        <length>255</length>
        <required>false</required>
        <trackFeedHistory>false</trackFeedHistory>
        <trackHistory>true</trackHistory>
        <trackTrending>false</trackTrending>
        <type>Text</type>
        <unique>false</unique>
    </fields>
    <fields>
        <fullName>Academic_Program__c</fullName>
        <deleteConstraint>SetNull</deleteConstraint>
        <externalId>false</externalId>
        <inlineHelpText>A program of study leading to a qualification, e.g. Bachelor of Engineering (Mechanical Engineering). Each Program has a unique code, e.g. BP066.</inlineHelpText>
        <label>Academic Program</label>
        <referenceTo>Account</referenceTo>
        <relationshipLabel>Plans</relationshipLabel>
        <relationshipName>Plans</relationshipName>
        <required>false</required>
        <trackFeedHistory>false</trackFeedHistory>
        <trackHistory>false</trackHistory>
        <trackTrending>false</trackTrending>
        <type>Lookup</type>
    </fields>
    <fields>
        <fullName>CRICOS_Code__c</fullName>
        <externalId>false</externalId>
        <inlineHelpText>Commonwealth Register of Institutions and Courses for Overseas Students Code.Given to registered Programs provided to international students</inlineHelpText>
        <label>CRICOS Code</label>
        <length>255</length>
        <required>false</required>
        <trackFeedHistory>false</trackFeedHistory>
        <trackHistory>false</trackHistory>
        <trackTrending>false</trackTrending>
        <type>Text</type>
        <unique>false</unique>
    </fields>
    <fields>
        <fullName>Degree__c</fullName>
        <externalId>false</externalId>
        <label>Degree</label>
        <length>18</length>
        <required>false</required>
        <trackFeedHistory>false</trackFeedHistory>
        <trackHistory>true</trackHistory>
        <trackTrending>false</trackTrending>
        <type>Text</type>
        <unique>false</unique>
    </fields>
    <fields>
        <fullName>Description__c</fullName>
        <externalId>false</externalId>
        <label>Description</label>
        <length>255</length>
        <required>false</required>
        <trackFeedHistory>false</trackFeedHistory>
        <trackHistory>true</trackHistory>
        <trackTrending>false</trackTrending>
        <type>Text</type>
        <unique>false</unique>
    </fields>
    <fields>
        <fullName>Effective_Date__c</fullName>
        <externalId>false</externalId>
        <label>Effective Date</label>
        <required>false</required>
        <trackFeedHistory>false</trackFeedHistory>
        <trackHistory>true</trackHistory>
        <trackTrending>false</trackTrending>
        <type>Date</type>
    </fields>
    <fields>
        <fullName>Effective_Status__c</fullName>
        <externalId>false</externalId>
        <label>Effective Status</label>
        <required>false</required>
        <trackFeedHistory>false</trackFeedHistory>
        <trackHistory>false</trackHistory>
        <trackTrending>false</trackTrending>
        <type>Picklist</type>
        <valueSet>
            <restricted>true</restricted>
            <valueSetDefinition>
                <sorted>false</sorted>
                <value>
                    <fullName>Active</fullName>
                    <default>false</default>
                    <label>Active</label>
                </value>
                <value>
                    <fullName>Inactive</fullName>
                    <default>false</default>
                    <label>Inactive</label>
                </value>
            </valueSetDefinition>
        </valueSet>
    </fields>
    <fields>
        <fullName>External_Unique_ID__c</fullName>
        <caseSensitive>true</caseSensitive>
        <externalId>true</externalId>
        <label>External Unique ID</label>
        <length>255</length>
        <required>false</required>
        <trackFeedHistory>false</trackFeedHistory>
        <trackHistory>false</trackHistory>
        <trackTrending>false</trackTrending>
        <type>Text</type>
        <unique>true</unique>
    </fields>
    <fields>
        <fullName>Field_of_Education_Code__c</fullName>
        <deleteConstraint>SetNull</deleteConstraint>
        <externalId>false</externalId>
        <label>Field of Education Code</label>
        <referenceTo>Field_of_education__c</referenceTo>
        <relationshipLabel>Plans</relationshipLabel>
        <relationshipName>Plans</relationshipName>
        <required>false</required>
        <trackFeedHistory>false</trackFeedHistory>
        <trackHistory>true</trackHistory>
        <trackTrending>false</trackTrending>
        <type>Lookup</type>
    </fields>
    <fields>
        <fullName>Field_of_Education_Description__c</fullName>
        <externalId>false</externalId>
        <label>Field of Education Description</label>
        <length>131072</length>
        <trackFeedHistory>false</trackFeedHistory>
        <trackHistory>true</trackHistory>
        <trackTrending>false</trackTrending>
        <type>LongTextArea</type>
        <visibleLines>3</visibleLines>
    </fields>
    <fields>
        <fullName>Field_of_Study_Code__c</fullName>
        <deleteConstraint>SetNull</deleteConstraint>
        <externalId>false</externalId>
        <label>Field of Study Code</label>
        <referenceTo>Field_of_Study__c</referenceTo>
        <relationshipLabel>Plans</relationshipLabel>
        <relationshipName>Plans</relationshipName>
        <required>false</required>
        <trackFeedHistory>false</trackFeedHistory>
        <trackHistory>true</trackHistory>
        <trackTrending>false</trackTrending>
        <type>Lookup</type>
    </fields>
    <fields>
        <fullName>Field_of_Study_Description__c</fullName>
        <externalId>false</externalId>
        <label>Field of Study Description</label>
        <length>255</length>
        <required>false</required>
        <trackFeedHistory>false</trackFeedHistory>
        <trackHistory>true</trackHistory>
        <trackTrending>false</trackTrending>
        <type>Text</type>
        <unique>false</unique>
    </fields>
    <fields>
        <fullName>Field_of_Study_Group__c</fullName>
        <externalId>false</externalId>
        <label>Field of Study Group</label>
        <length>255</length>
        <required>false</required>
        <trackFeedHistory>false</trackFeedHistory>
        <trackHistory>false</trackHistory>
        <trackTrending>false</trackTrending>
        <type>Text</type>
        <unique>false</unique>
    </fields>
    <fields>
        <fullName>First_Term_Valid__c</fullName>
        <externalId>false</externalId>
        <inlineHelpText>The first academic term that a Program is active</inlineHelpText>
        <label>First Term Valid</label>
        <length>18</length>
        <required>false</required>
        <trackFeedHistory>false</trackFeedHistory>
        <trackHistory>false</trackHistory>
        <trackTrending>false</trackTrending>
        <type>Text</type>
        <unique>false</unique>
    </fields>
    <fields>
        <fullName>Institution__c</fullName>
        <deleteConstraint>SetNull</deleteConstraint>
        <externalId>false</externalId>
        <label>Institution</label>
        <referenceTo>Account</referenceTo>
        <relationshipLabel>Plans (Institution)</relationshipLabel>
        <relationshipName>InstitutionPlans</relationshipName>
        <required>false</required>
        <trackFeedHistory>false</trackFeedHistory>
        <trackHistory>false</trackHistory>
        <trackTrending>false</trackTrending>
        <type>Lookup</type>
    </fields>
    <fields>
        <fullName>Percentage_owned__c</fullName>
        <externalId>false</externalId>
        <label>Percentage owned</label>
        <length>255</length>
        <required>false</required>
        <trackFeedHistory>false</trackFeedHistory>
        <trackHistory>false</trackHistory>
        <trackTrending>false</trackTrending>
        <type>Text</type>
        <unique>false</unique>
    </fields>
    <fields>
        <fullName>Plan_key__c</fullName>
        <caseSensitive>false</caseSensitive>
        <externalId>true</externalId>
        <label>Plan_key</label>
        <length>255</length>
        <required>false</required>
        <trackFeedHistory>false</trackFeedHistory>
        <trackHistory>false</trackHistory>
        <trackTrending>false</trackTrending>
        <type>Text</type>
        <unique>true</unique>
    </fields>
    <fields>
        <fullName>Short_Description__c</fullName>
        <externalId>false</externalId>
        <label>Short Description</label>
        <length>18</length>
        <required>false</required>
        <trackFeedHistory>false</trackFeedHistory>
        <trackHistory>true</trackHistory>
        <trackTrending>false</trackTrending>
        <type>Text</type>
        <unique>false</unique>
    </fields>
    <fields>
        <fullName>Specialisation_Code__c</fullName>
        <externalId>false</externalId>
        <label>Specialisation Code</label>
        <length>255</length>
        <required>false</required>
        <trackFeedHistory>false</trackFeedHistory>
        <trackHistory>false</trackHistory>
        <trackTrending>false</trackTrending>
        <type>Text</type>
        <unique>false</unique>
    </fields>
    <fields>
        <fullName>Specialisation_Description__c</fullName>
        <externalId>false</externalId>
        <label>Specialisation Description</label>
        <required>false</required>
        <trackFeedHistory>false</trackFeedHistory>
        <trackHistory>true</trackHistory>
        <trackTrending>false</trackTrending>
        <type>TextArea</type>
    </fields>
    <fields>
        <fullName>Transcript_Level__c</fullName>
        <externalId>false</externalId>
        <label>Transcript Level</label>
        <length>255</length>
        <required>false</required>
        <trackFeedHistory>false</trackFeedHistory>
        <trackHistory>false</trackHistory>
        <trackTrending>false</trackTrending>
        <type>Text</type>
        <unique>false</unique>
    </fields>
    <fields>
        <fullName>Unique_Reference_ID__c</fullName>
        <caseSensitive>false</caseSensitive>
        <externalId>true</externalId>
        <label>Unique Reference ID</label>
        <length>18</length>
        <required>false</required>
        <trackFeedHistory>false</trackFeedHistory>
        <trackHistory>false</trackHistory>
        <trackTrending>false</trackTrending>
        <type>Text</type>
        <unique>true</unique>
    </fields>
    <label>Plan</label>
    <listViews>
        <fullName>All</fullName>
        <columns>NAME</columns>
        <columns>Academic_Plan__c</columns>
        <columns>Academic_Program__c</columns>
        <columns>Academic_Organization__c</columns>
        <columns>Effective_Date__c</columns>
        <columns>Academic_Career__c</columns>
        <filterScope>Everything</filterScope>
        <label>All</label>
    </listViews>
    <nameField>
        <label>Plan Name</label>
        <trackFeedHistory>false</trackFeedHistory>
        <trackHistory>true</trackHistory>
        <type>Text</type>
    </nameField>
    <pluralLabel>Plans</pluralLabel>
    <searchLayouts>
        <customTabListAdditionalFields>Academic_Plan__c</customTabListAdditionalFields>
        <customTabListAdditionalFields>Academic_Program__c</customTabListAdditionalFields>
        <customTabListAdditionalFields>Effective_Date__c</customTabListAdditionalFields>
        <customTabListAdditionalFields>Academic_Career__c</customTabListAdditionalFields>
        <customTabListAdditionalFields>Academic_Organization__c</customTabListAdditionalFields>
        <searchResultsAdditionalFields>Academic_Plan__c</searchResultsAdditionalFields>
        <searchResultsAdditionalFields>Academic_Program__c</searchResultsAdditionalFields>
        <searchResultsAdditionalFields>Effective_Date__c</searchResultsAdditionalFields>
        <searchResultsAdditionalFields>Academic_Career__c</searchResultsAdditionalFields>
        <searchResultsAdditionalFields>Academic_Organization__c</searchResultsAdditionalFields>
    </searchLayouts>
    <sharingModel>Read</sharingModel>
    <visibility>Public</visibility>
</CustomObject>
