<?xml version="1.0" encoding="UTF-8"?>
<CustomObject xmlns="http://soap.sforce.com/2006/04/metadata">
    <actionOverrides>
        <actionName>Accept</actionName>
        <type>Default</type>
    </actionOverrides>
    <actionOverrides>
        <actionName>CancelEdit</actionName>
        <type>Default</type>
    </actionOverrides>
    <actionOverrides>
        <actionName>Clone</actionName>
        <type>Default</type>
    </actionOverrides>
    <actionOverrides>
        <actionName>Delete</actionName>
        <type>Default</type>
    </actionOverrides>
    <actionOverrides>
        <actionName>Edit</actionName>
        <type>Default</type>
    </actionOverrides>
    <actionOverrides>
        <actionName>List</actionName>
        <type>Default</type>
    </actionOverrides>
    <actionOverrides>
        <actionName>New</actionName>
        <type>Default</type>
    </actionOverrides>
    <actionOverrides>
        <actionName>SaveEdit</actionName>
        <type>Default</type>
    </actionOverrides>
    <actionOverrides>
        <actionName>Tab</actionName>
        <type>Default</type>
    </actionOverrides>
    <actionOverrides>
        <actionName>View</actionName>
        <type>Default</type>
    </actionOverrides>
    <allowInChatterGroups>false</allowInChatterGroups>
    <compactLayoutAssignment>SYSTEM</compactLayoutAssignment>
    <deploymentStatus>Deployed</deploymentStatus>
    <description>It describes Ethics Application Core Details and stored from Researcher Master (RRI) via Ipass integration.</description>
    <enableActivities>false</enableActivities>
    <enableBulkApi>true</enableBulkApi>
    <enableFeeds>false</enableFeeds>
    <enableHistory>false</enableHistory>
    <enableLicensing>false</enableLicensing>
    <enableReports>false</enableReports>
    <enableSearch>true</enableSearch>
    <enableSharing>true</enableSharing>
    <enableStreamingApi>true</enableStreamingApi>
    <fields>
        <fullName>Application_Title__c</fullName>
        <description>Application Title</description>
        <externalId>false</externalId>
        <label>Application Title</label>
        <length>1024</length>
        <trackTrending>false</trackTrending>
        <type>LongTextArea</type>
        <visibleLines>3</visibleLines>
    </fields>
    <fields>
        <fullName>Application_Type__c</fullName>
        <externalId>false</externalId>
        <label>Application Type</label>
        <required>false</required>
        <trackTrending>false</trackTrending>
        <type>Picklist</type>
        <valueSet>
            <valueSetDefinition>
                <sorted>false</sorted>
                <value>
                    <fullName>HUMAN = Human Ethics</fullName>
                    <default>false</default>
                    <label>HUMAN = Human Ethics</label>
                </value>
                <value>
                    <fullName>ANIMAL = Animal Ethics</fullName>
                    <default>false</default>
                    <label>ANIMAL = Animal Ethics</label>
                </value>
                <value>
                    <fullName>GENETIC = Genetic</fullName>
                    <default>false</default>
                    <label>GENETIC = Genetic</label>
                </value>
                <value>
                    <fullName>Old = Human High Risk</fullName>
                    <default>false</default>
                    <label>Old = Human High Risk</label>
                </value>
            </valueSetDefinition>
        </valueSet>
    </fields>
    <fields>
        <fullName>Applied_Date__c</fullName>
        <description>Date Applied</description>
        <externalId>false</externalId>
        <label>Applied Date</label>
        <required>false</required>
        <trackTrending>false</trackTrending>
        <type>Date</type>
    </fields>
    <fields>
        <fullName>Approved_Date__c</fullName>
        <description>Date Approved</description>
        <externalId>false</externalId>
        <label>Approved Date</label>
        <required>false</required>
        <trackTrending>false</trackTrending>
        <type>Date</type>
    </fields>
    <fields>
        <fullName>Clearence_Purpose__c</fullName>
        <externalId>false</externalId>
        <label>Clearence Purpose</label>
        <required>false</required>
        <trackTrending>false</trackTrending>
        <type>TextArea</type>
    </fields>
    <fields>
        <fullName>Committee_College__c</fullName>
        <externalId>false</externalId>
        <label>Committee College</label>
        <length>255</length>
        <required>false</required>
        <trackTrending>false</trackTrending>
        <type>Text</type>
        <unique>false</unique>
    </fields>
    <fields>
        <fullName>Committee_School__c</fullName>
        <externalId>false</externalId>
        <label>Committee School</label>
        <length>255</length>
        <required>false</required>
        <trackTrending>false</trackTrending>
        <type>Text</type>
        <unique>false</unique>
    </fields>
    <fields>
        <fullName>Ethics_Id__c</fullName>
        <caseSensitive>false</caseSensitive>
        <description>use this External Id to do the update of Ethics</description>
        <externalId>true</externalId>
        <label>Ethics Id</label>
        <length>50</length>
        <required>true</required>
        <trackTrending>false</trackTrending>
        <type>Text</type>
        <unique>true</unique>
    </fields>
    <fields>
        <fullName>Expiry_Date__c</fullName>
        <description>Expiry Date (of Ethics Approval)</description>
        <externalId>false</externalId>
        <label>Expiry Date</label>
        <required>false</required>
        <trackTrending>false</trackTrending>
        <type>Date</type>
    </fields>
    <fields>
        <fullName>RM_Created_Date__c</fullName>
        <description>Add  RM Created Date -RPORW-1304 mapping with RRI</description>
        <externalId>false</externalId>
        <label>RM Created Date</label>
        <required>false</required>
        <trackTrending>false</trackTrending>
        <type>DateTime</type>
    </fields>
    <fields>
        <fullName>RM_Ethics_Id__c</fullName>
        <description>Application Code (Reference Number in RM)</description>
        <externalId>false</externalId>
        <label>RM Ethics Id</label>
        <length>50</length>
        <required>false</required>
        <trackTrending>false</trackTrending>
        <type>Text</type>
        <unique>false</unique>
    </fields>
    <fields>
        <fullName>RM_LastModified_Date__c</fullName>
        <description>Add RM Last Modified Date :: RPORW-990</description>
        <externalId>false</externalId>
        <label>RM LastModified Date</label>
        <required>false</required>
        <trackTrending>false</trackTrending>
        <type>DateTime</type>
    </fields>
    <fields>
        <fullName>Received_Date__c</fullName>
        <description>Date Received (by the Research Office)</description>
        <externalId>false</externalId>
        <label>Received Date</label>
        <required>false</required>
        <trackTrending>false</trackTrending>
        <type>Date</type>
    </fields>
    <fields>
        <fullName>Research_Committee__c</fullName>
        <externalId>false</externalId>
        <label>Research Committee</label>
        <required>false</required>
        <trackTrending>false</trackTrending>
        <type>TextArea</type>
    </fields>
    <fields>
        <fullName>Review_Date__c</fullName>
        <description>Review Date</description>
        <externalId>false</externalId>
        <label>Review Date</label>
        <required>false</required>
        <trackTrending>false</trackTrending>
        <type>Date</type>
    </fields>
    <fields>
        <fullName>Risk_Level_Code__c</fullName>
        <externalId>false</externalId>
        <label>Risk Level Code</label>
        <length>50</length>
        <required>false</required>
        <trackTrending>false</trackTrending>
        <type>Text</type>
        <unique>false</unique>
    </fields>
    <fields>
        <fullName>Risk_Level_Description__c</fullName>
        <externalId>false</externalId>
        <label>Risk Level Description</label>
        <required>false</required>
        <trackTrending>false</trackTrending>
        <type>TextArea</type>
    </fields>
    <fields>
        <fullName>Start_Date__c</fullName>
        <description>Start Date of Project</description>
        <externalId>false</externalId>
        <label>Start Date</label>
        <required>false</required>
        <trackTrending>false</trackTrending>
        <type>Date</type>
    </fields>
    <fields>
        <fullName>Status__c</fullName>
        <externalId>false</externalId>
        <label>Status</label>
        <required>false</required>
        <trackTrending>false</trackTrending>
        <type>Picklist</type>
        <valueSet>
            <valueSetDefinition>
                <sorted>false</sorted>
                <value>
                    <fullName>Amended</fullName>
                    <default>false</default>
                    <label>Amended</label>
                </value>
                <value>
                    <fullName>Approved</fullName>
                    <default>false</default>
                    <label>Approved</label>
                </value>
                <value>
                    <fullName>Approved - Not administered at RMIT</fullName>
                    <default>false</default>
                    <label>Approved - Not administered at RMIT</label>
                </value>
                <value>
                    <fullName>Pending</fullName>
                    <default>false</default>
                    <label>Pending</label>
                </value>
                <value>
                    <fullName>Approved - Transferred from external HREC</fullName>
                    <default>false</default>
                    <label>Approved - Transferred from external HREC</label>
                </value>
                <value>
                    <fullName>Withdrawn</fullName>
                    <default>false</default>
                    <label>Withdrawn</label>
                </value>
                <value>
                    <fullName>Old values = no count with records</fullName>
                    <default>false</default>
                    <label>Old values = no count with records</label>
                </value>
                <value>
                    <fullName>Rejected</fullName>
                    <default>false</default>
                    <label>Rejected</label>
                </value>
            </valueSetDefinition>
        </valueSet>
    </fields>
    <label>Ethics</label>
    <listViews>
        <fullName>All</fullName>
        <columns>NAME</columns>
        <columns>Application_Title__c</columns>
        <columns>CREATEDBY_USER</columns>
        <columns>OWNER.ALIAS</columns>
        <filterScope>Everything</filterScope>
        <label>All</label>
    </listViews>
    <listViews>
        <fullName>Ethics</fullName>
        <columns>NAME</columns>
        <columns>Application_Title__c</columns>
        <columns>Application_Type__c</columns>
        <columns>Status__c</columns>
        <columns>RM_Ethics_Id__c</columns>
        <filterScope>Everything</filterScope>
        <label>Ethics</label>
        <language>en_US</language>
    </listViews>
    <nameField>
        <displayFormat>ET-{0000000}</displayFormat>
        <label>Ethics Name</label>
        <type>AutoNumber</type>
    </nameField>
    <pluralLabel>Ethics</pluralLabel>
    <searchLayouts>
        <searchResultsAdditionalFields>Application_Title__c</searchResultsAdditionalFields>
        <searchResultsAdditionalFields>Application_Type__c</searchResultsAdditionalFields>
        <searchResultsAdditionalFields>Status__c</searchResultsAdditionalFields>
        <searchResultsAdditionalFields>RM_Ethics_Id__c</searchResultsAdditionalFields>
    </searchLayouts>
    <sharingModel>Private</sharingModel>
    <visibility>Public</visibility>
</CustomObject>
