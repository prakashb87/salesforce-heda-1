/********************************************************************************************************/
/**    Author : Resmi Ramakrishnan                     Date : 27/05/2018     ****************************/               
/***   This is helper class for Customopportunitytrigger      *******************************************/ 
/********************************************************************************************************/

public without sharing class CustomOpportunityTriggerHelper
{
    public static void updateOpportunityStageField(List<Custom_Opportunity__c> CustomOpplist)
    {
        System.debug('RRRR - inside helper method updateCustomOppField=' + CustomOpplist);
        Set<Id> OppIdSet = new Set<Id>();
        
        for (Custom_Opportunity__c coObj : CustomOpplist)
        { 
            system.debug('XXXX= ' + coObj.Opportunity__c);
            if( !String.ISBLANK(coObj.Opportunity__c ) )
            {
                if( coObj.Stage__c == 'Closed Won' )  
                {
                    OppIdSet.add( coObj.Opportunity__c  );
                }            	   
            }
                      
        }
        system.debug( 'AAAA= Opp Id List'+ OppIdSet );
        List <Opportunity> OppList = new List<Opportunity>();
        List <Opportunity> OppListToUpdate = new List<Opportunity>();
        OppList = [SELECT id, StageName from Opportunity where id IN:OppIdSet ];
        system.debug(' GGGG=' + OppList);
        
        id userId = UserInfo.getUserId();
        
        
        for(Opportunity opp : OppList )
        {
            opp.StageName = 'Closed Won';
            //opp.OwnerId = userId;
            OppListToUpdate.add(opp);
        }
        
        if( !OppListToUpdate.IsEmpty())
        {
            System.debug(' Final list to update =' + opplistToUpdate);
            update OppListToUpdate;
        }
    }
    

}