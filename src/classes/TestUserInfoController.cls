/*****************************
Name: TestUserInfoController
Purpose: Test class for UserInfoController class
History:
Created by Ankit Bhagat on 20/08/2018
************************************/

@isTest
private class TestUserInfoController {
    /************************************************************************************
// Purpose      :  Creating Test data setup for UserInfo this test class
// Developer    :  Subhajit
// Created Date :  10/27/2018                 
//***********************************************************************************/
    @testSetup static void testDataSetup() {
        // Setup test data
        // This code runs as the system user
        RSTP_TestDataFactoryUtils.createNonCommunityUsersWithProfile(1,'System Administrator','Activator');
        
        User adminUser =[SELECT Id,ContactId FROM User WHERE UserName='test.nonCommunityUser1@test.com.testDev' LIMIT 1];
        System.runAs(adminUser)   
        {
            Map<String,Integer> allnumbersOfDataMap = new Map<String,Integer>();
            allnumbersOfDataMap.put('user',1);
            allnumbersOfDataMap.put('account',1);
            allnumbersOfDataMap.put('contact',1);
            
            Map<String,String> requiredInfoMap = new Map<String,String>();
            requiredInfoMap.put('userType','CommunityUsers');
            requiredInfoMap.put('profile','RMIT Researcher Portal Plus Community User');
            
            Boolean userCreatedFlag = RSTP_TestDataFactoryUtils.testRSTPDataSetup(allnumbersOfDataMap,requiredInfoMap);
            System.assert(userCreatedFlag==true,'Community User Created'); 
        }
        
    }    
    @isTest
    public static void testPositiveUseCaseForUserInfoControllerMethods(){
        
        User usr =[SELECT Id,ContactId,UserName FROM User WHERE UserName='test.nonCommunityUser1@test.com.testDev' LIMIT 1];   
         System.Assert(usr.UserName =='test.nonCommunityUser1@test.com.testDev' );
        //All Positive Use Cases: Starts
        system.runAs(usr)
        {
            // The following code runs as user 'usr' 
            Test.StartTest();
            
            System.debug('Current admin User: ' + UserInfo.getUserName());
            System.debug('Current admin Profile: ' + UserInfo.getProfileId());
            UserInfoController.fetchUser();
           
            Test.StopTest();    
        }
      
        //All Positive Use Cases: Ends
    }
    
    @isTest
    public static void testNegativeUseCaseForUserInfoControllerMethods(){
        try{
        User usr =[SELECT Id,ContactId,UserName FROM User WHERE UserName='test.nonCommunityUser1@test.com.testDev' LIMIT 1];   
            System.Assert(usr.UserName =='test.nonCommunityUser1@test.com.testDev' );
            
            //All Negative Use Cases: Starts
        system.runAs(usr)
        {
            // The following code runs as user 'usr' 
            Test.StartTest();
            
            System.debug('Current communityUser1: ' + UserInfo.getUserName());
            System.debug('Current communityprofile1: ' + UserInfo.getProfileId());
            UserInfoController.fetchUser();
			UserInfoController.checkIsSandbox();
            
            Test.StopTest();    
        }
        //All Negative Use Cases: Ends
       
        } catch (System.DmlException e){
             System.debug('The following exception has occurred: ' + e.getMessage());
        }
   
    } 
    
    @isTest
    public static void testExceptionHandlingMethod(){
        
        User u =[SELECT Id,ContactId FROM User WHERE UserName='test.communityUser1@test.com.testDev' LIMIT 1]; 
        System.assert(u!=null);
         
        System.runAs(u) {      
            
            UserInfoController.isForceExceptionRequired=true;
            UserInfoController.fetchUser();

        }
    }
    
    @isTest
    public static void testUserProfileUpload(){
        
        User u =[SELECT Id,ContactId FROM User WHERE UserName='test.communityUser1@test.com.testDev' LIMIT 1]; 
       	 System.assert(u!=null);
        System.runAs(u) {      
            
            UserInfoController.updateUserProfilePhoto('test','jpeg');

        }
    }
    
}