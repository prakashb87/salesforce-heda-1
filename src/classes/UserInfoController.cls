/*********************************************************
Name: UserInfoController
Purpose: To get the current login user information
History:
Created by Esther Ethelbert on 16/08/2018
**********************************************************/

public with sharing class UserInfoController {
    
   
   /*****************************************************************************************
     Global variable Declaration
    //****************************************************************************************/
    public static Boolean isForceExceptionRequired=false;//added by Ali
   
   /************************************************************************************
    // Purpose      :  Fetching Current User record
    // Parameters   :  void     
    // Developer    :  Esther
    // Created Date :  08/16/2018                 
    //***********************************************************************************/
    @AuraEnabled 
    public static user fetchUser(){
        User oUser = new User();
        try
        {
            oUser = SobjectUtils.fetchUser();
            ResearcherExceptionHandlingUtil.createForceExceptionForTestCoverage(isForceExceptionRequired);
            
        }
        catch(Exception ex) 
        {            
            //Exception handling Error Log captured :: Starts
            ResearcherExceptionHandlingUtil.addExceptionLog(ex,ResearcherExceptionHandlingUtil.getParamMap()); //added by Ali
            //Exception handling Error Log captured :: Ends
            System.debug('Exception occured : '+ex.getMessage());  
        } 
        
        return oUser;
    }
    
     /************************************************************************************
    // JIRA         : RPORW-1083
    // SPRINT       : 7
    // Purpose      : checking Organisation is Sandbox
    // Parameters   : Boolean       
    // Developer    : Prakash
    // Created Date : 22/03/2019
    //***********************************************************************************/ 
    @AuraEnabled    
    public static Boolean checkIsSandbox(){
       Boolean isSandbox =false; 
     
       try{
           isSandbox = SobjectUtils.checkOrgType();
           system.debug('Istrue==>'+isForceExceptionRequired);
         
           ResearcherExceptionHandlingUtil.createForceExceptionForTestCoverage(isForceExceptionRequired);
       }
       catch(Exception ex) 
       {    
           //Exception handling Error Log captured :: Starts
          ResearcherExceptionHandlingUtil.addExceptionLog(ex,ResearcherExceptionHandlingUtil.getParamMap()); //added by Ali
           //Exception handling Error Log captured :: Ends
           System.debug('Exception occured : '+ex.getMessage());
       } 
       return isSandbox;
    }
    
    
    /************************************************************************************
    // JIRA         : RPORW-1015
    // Purpose      : To upload user profile photo
    // Parameters   : String, String       
    // Developer    : Prakash
    // Created Date : 29/03/2019
     ***********************************************************************************/
    
    @AuraEnabled 
    public static String updateUserProfilePhoto(String usrPhoto, String imageType){
        
        ConnectApi.Photo photo;
        try{
             photo = ConnectApi.UserProfiles.setPhoto(Network.getNetworkId(), UserInfo.getUserID(), new ConnectApi.BinaryInput(EncodingUtil.base64Decode(usrPhoto), 'image/'+imageType, 'userImage'));
             ResearcherExceptionHandlingUtil.createForceExceptionForTestCoverage(isForceExceptionRequired);
            
        }catch(Exception ex){
            ResearcherExceptionHandlingUtil.addExceptionLog(ex,ResearcherExceptionHandlingUtil.getParamMap());
        }
        if(photo != null){
            return photo.smallPhotoUrl;
        }
        return null;
    }
    
    /************************************************************************************
    // JIRA         : RPORW-1638
    // Purpose      : To show the CreateProject Button
    // Parameters   :        
    // Developer    : Ankit
    // Created Date : 19/06/2019
     ***********************************************************************************/
    
    @AuraEnabled 
    public static Boolean isCreateProjectApplicable (){
        
        Boolean isCreateProjectApplicable = false;
        // To check the Profile Access of the User as only 9 Profiles should be applicable
        
        Boolean isCurrentUserProfileAccessibleFlag = SobjectUtils.getProfileAccessCheckInfo(userinfo.getProfileId()); 
              System.debug('@@@isCurrentUserProfileAccessibleFlag'+isCurrentUserProfileAccessibleFlag);
              if(isCurrentUserProfileAccessibleFlag)   { 
                  isCreateProjectApplicable =  true ;
                 }  
              System.debug('@@@isCreateProjectApplicable'+isCreateProjectApplicable);
        return isCreateProjectApplicable;
    }
}