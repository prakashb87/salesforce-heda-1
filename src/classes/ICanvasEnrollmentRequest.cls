public interface ICanvasEnrollmentRequest {
    void sendEnrollDetailToCanvas(CourseConnCanvasRequestWrapper canvasEnrollmentRequestBody);
    Map<Id, CourseConnCanvasResponseWrapper.Response> getCanvasEnrollResByCourseConnId();
    Integer getTotalNumberOfFailedEnrollments();
}