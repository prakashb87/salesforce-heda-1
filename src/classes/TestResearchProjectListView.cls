/*******************************************
Purpose: Test class ResearchProjectListView
History:
Created by Ankit Bhagat on 03/10/2018
Modified by Subhajit with Test
*******************************************/
@isTest
public class TestResearchProjectListView {
    
    /************************************************************************************
// Purpose      :  Creating Test data for Community User for all test methods 
// Developer    :  Subhajit
// Created Date :  10/24/2018                 
//***********************************************************************************/
    @testSetup static void testDataSetup() {
        
        RSTP_TestDataFactoryUtils.createNonCommunityUsersWithProfile(1,'System Administrator','Activator');
        
        User adminUser =[SELECT Id,ContactId FROM User WHERE UserName='test.nonCommunityUser1@test.com.testDev' LIMIT 1];
        System.runAs(adminUser)   
        {
            Map<String,Integer> allnumbersOfDataMap = new Map<String,Integer>();
            allnumbersOfDataMap.put('user',1);
            allnumbersOfDataMap.put('account',1);
            allnumbersOfDataMap.put('contact',1);
            
            Map<String,String> requiredInfoMap = new Map<String,String>();
            requiredInfoMap.put('userType','CommunityUsers');
            requiredInfoMap.put('profile','RMIT Researcher Portal Plus Community User');
            
            Boolean userCreatedFlag = RSTP_TestDataFactoryUtils.testRSTPDataSetup(allnumbersOfDataMap,requiredInfoMap);
            System.assert(userCreatedFlag==true,'Community User Created'); 
        }
        
    }    
    
    /************************************************************************************
// Purpose      :  Test functionality SearchProjectListView functionality
// Developer    :  Subhajit
// Created Date :  10/24/2018                 
//***********************************************************************************/
    @isTest
    public static void testMyResearchProjectListViewPositiveMethod()
    {
        User usr =[SELECT Id,ContactId FROM User WHERE UserName='test.communityUser1@test.com.testDev' LIMIT 1];
        System.runAs(usr)
        {
            
            String projectRecordTypeId=Schema.SObjectType.ResearcherPortalProject__c.getRecordTypeInfosByName().get(Label.RSTP_ResearchProjectRMRecordtype).getRecordTypeId();
            String access = 'Private';
            List<ResearcherPortalProject__c> newresearchProjectList = RSTP_TestDataFactoryUtils.createResearchProj(3, projectRecordTypeId, access);
            system.assert(newresearchProjectList.size() > 0);
            ProjectDetailsWrapper.ResearchProjectListView myResearchProjectListView = ResearchProjectListViewController.getMyResearchProjectListView(  );
            system.assert(myResearchProjectListView!=null);
            
            User portalUsr = ResearchProjectListViewController.researchProjectListViewFetchUser();
            system.assert(portalUsr!=null);  
            
            ResearchProjectListViewController.getGroupStatusSelectOptions();
            
            projectRecordTypeId=Schema.SObjectType.ResearcherPortalProject__c.getRecordTypeInfosByName().get(Label.RSTP_ResearchProjectRMRecordtype).getRecordTypeId();
            access = 'RMIT Public';
            newresearchProjectList = RSTP_TestDataFactoryUtils.createResearchProj(3, projectRecordTypeId, access);
            system.assert(newresearchProjectList.size() > 0);
            //List<ProjectDetailsWrapper.ResearchProjectPublicProjectListView> myResearchPublicProjectListViews = ResearchProjectListViewController.getMyResearchProjectPublicListView();
            //system.assert(myResearchPublicProjectListViews!=null);
            
            ResearchProjectListViewController.getMyResearchProjectPublicListView(access,'UC');
            
            ResearcherPublicProjectsHelper.ResearchProjectPublicListViewConfig configuration = new ResearcherPublicProjectsHelper.ResearchProjectPublicListViewConfig();
            configuration.tabSelected = 'UC';
            configuration.searchText = access;
            configuration.pageNo = 1;
            configuration.forCodeDetailSearch = new Set<String>();
            configuration.impactCodeDetailSearch = new Set<String>(); 
            
            Test.StartTest();
            ResearchProjectListViewController.getRMITPublicResearchProjectsFiltered(JSON.serialize(configuration));             
            ResearchProjectListViewController.getNextSetOfRMITPublicResearchProjects(JSON.serialize(configuration));
            
            List<String> forValues = ResearchProjectListViewController.getFORValues();
            system.assert(forValues!=null);
            List<String> impactCategory = ResearchProjectListViewController.getImpactCategory();
            system.assert(impactCategory!=null);
            Integer recordSize = ResearchProjectListViewController.getRecordSizeLimit();
            system.assert(recordSize!=null);
            Test.StopTest();
        }
    }
    /************************************************************************************
// Purpose      :  Test functionality SearchProjectListView functionality
// Developer    :  Md ali
// Created Date :  10/24/2018                 
//***********************************************************************************/  
    @isTest
    public static void testMyResearchProjectListViewNegativeMethod()
    {
        
        User usr =[SELECT Id,ContactId FROM User WHERE UserName='test.communityUser1@test.com.testDev' LIMIT 1];
        System.runAs(usr)
        {
            Boolean result = false;
            ResearcherPortalProject__c researchProject = new ResearcherPortalProject__c();
            researchProject.Status__c='idea';
            // insert researchProject;
            system.assert(researchProject.Status__c=='idea');
            List<id> researchId = new List<id>();
            researchId.add(researchProject.id);
            
            Contact con =[SELECT Id,Firstname,lastname FROM contact WHERE LastName='Contact1' LIMIT 1 ];
            con.hed__UniversityEmail__c='raj@rmit.edu.au';
            con.lastname='';
            //update con; 
            try
            {
                update con;    
            }
            catch(DmlException ex)
            {
                result =true;                   
                
            }
            system.assert(result);
            
            Researcher_Member_Sharing__c memberResearch = new Researcher_Member_Sharing__c();
            try
            {
                memberResearch .Contact__c = con.Id;
                memberresearch.User__c = usr.Id;
                memberresearch.Primary_Contact_Flag__c=true;
                // insert memberresearch;   
            }
            catch(Exception ex)
            {
                result =true;  
                //ResearcherExceptionHandlingUtil.addExceptionLog(ex,new Map<String, String>{ERR_MAP_KEYPARAM1=>ResearcherExceptionHandlingUtil.getErrMethod(ex.getStackTraceString()),ERR_MAP_KEYPARAM2=>ResearcherExceptionHandlingUtil.getErrClassName(ex.getStackTraceString()),ERR_MAP_KEYPARAM3=>'Research ListView',ERR_MAP_KEYPARAM4=>'No'});
                
            }  
            system.assert(result);
            //use case covering exception block
            String s = '{}';
            //ProjectDetailsWrapper.ResearchProjectListView myResearchProjectListView1 = ResearchProjectListViewController.getMyResearchProjectListView(null);
            //ResearchProjectHelper.getPrimaryContactNameofProject(researchId);
            
        }
    }
    @isTest
    public static void testMyResearchProjectListViewExceptionMethod1()
    {
        User usr =[SELECT Id,ContactId FROM User WHERE UserName='test.communityUser1@test.com.testDev' LIMIT 1];
        System.assert(usr!=null);
        System.runAs(usr){
            Test.StartTest();
            ResearchProjectListViewController.isForceExceptionRequired=true; 
            //  ResearchProjectListViewController.getMyResearchProjectListView(null,null);
           
            User portalUsr = ResearchProjectListViewController.researchProjectListViewFetchUser();
            system.assert(portalUsr!=null); 
            ResearchProjectListViewController.getMyResearchProjectListView(); 
            ResearchProjectListViewController.getRMITPublicResearchProjectsFiltered('param');
            
            Test.StopTest();
            //List<ProjectDetailsWrapper.ResearchProjectPublicProjectListView> myResearchPublicProjectListViews = ResearchProjectListViewController.getMyResearchProjectPublicListView(null);
            //system.assert(myResearchPublicProjectListViews.size()==0);
        }
    }
}