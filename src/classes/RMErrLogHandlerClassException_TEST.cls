@isTest
public class RMErrLogHandlerClassException_TEST {
    static testMethod void testMethod1() 
    { 
    List<RMErrLog__c> lstErrLog = new List<RMErrLog__c>();
        for( integer i=0 ; i<=201;i++)
        {  
            RMErrLog__c err = new RMErrLog__c();
            err.Error_Cause__c = String.valueOf('Test Reason');
            err.Error_Line_Number__c = 1234;
            err.Error_Message__c = 'Test Reason';
            err.Error_Time__c = system.today();
            err.Error_Type__c = 'Test Reason';
            lstErrLog.add(err);
        }
        insert lstErrLog;
    }
}