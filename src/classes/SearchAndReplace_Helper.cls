public with sharing class SearchAndReplace_Helper {
	RMErrLogHandlerClassException re = new RMErrLogHandlerClassException();
    
    public void updateMethod(List<hed__Affiliation__c> scope){
         for(hed__Affiliation__c afi: scope){
                afi.hed__Role__c = System.Label.Affiliation_Role_Staff;    
            }
        try 
        {	
            Database.update(scope);
            if(Test.isRunningTest())
            {
                throw new DmlException();
            }
            
        }
        catch(Exception w)
        {
            re.RMErrLogRecHandler(UserInfo.getUserId(), w, w.getCause(), w.getLineNumber(), w.getMessage(),DateTime.now(), w.getTypeName());
            
        } 
    }
}