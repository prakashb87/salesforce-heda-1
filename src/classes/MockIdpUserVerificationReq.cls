/**
 * @description Mock class for testing IdentityProviderUserVerificationReq
 * This mock class can be used to test the positive case, e.g.
 * a user has been successfully verified by the external system.
 * This mock class will not make any callouts.
 * @group Identity Management
 */
public class MockIdpUserVerificationReq implements IdentityProviderUserVerificationReq {

    /**
     * @description Mock method that will simulate verifying a user
     * @param emailAddress The email address of the customer to be verified
     */
    public void updateUserToVerified(String emailAddress) {
        System.debug('User ' + emailAddress + ' now verified...');
    }
}