public with sharing class CourseConnectionIntegrationController {
	
	@AuraEnabled
	public static void sendCourseConn (Id courseConnectionId) {
		System.debug('CourseConnectionIntegrationController.sendCourseConn entered');

		Set <Id> idSet = new Set <Id>();
		idSet.add(courseConnectionId);

		// need to check if there is an error or not
		boolean hasError = false;

		List<Course_Connection_Life_Cycle__c> dbResults = [Select Id from Course_Connection_Life_Cycle__c
		where Stage__c = 'Post to SAMS' AND Status__c = 'Error' ];

		if (dbResults.size() == 1) {
			hasError = true;
		}

		if (hasError== true)
		{
			CourseConnectionDetailIntegration.sendCourseConn(idSet);
		}

		System.debug('CourseConnectionIntegrationController.sendCourseConn exited');


	}
}