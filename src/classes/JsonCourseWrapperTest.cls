/*****************************************************************************************************
 *** @Class             : JsonCourseWrapperTest
 *** @Author            : Avinash Machineni
 *** @Requirement       : To test the JsonCourseWrapper class
 *** @Created date      : 21/05/2018
 *** @JIRA ID           : ECB-2954
 *****************************************************************************************************/
/*****************************************************************************************************
 *** @About Class
 *** This class is used to cover the code coverage for JsonCourseWrapper Class
 *****************************************************************************************************/ 


@IsTest
public class JsonCourseWrapperTest {
	/********************************************************************
    // Purpose              : To send the response structure form IPaas                          
    // Author               : Capgemini [Avinash]
    //  Returns             : void
    //JIRA Reference        : ECB-2954 : Integration between Salesforce and IPaaS for sending course connection details
    //********************************************************************/

	static testMethod void testParse() {
		String json = '{'+
		''+
		'\"id\" : \"ID-dev-6r87t87587\" ,'+
		''+
		'\"result\" : \"success\",'+
		''+
		'\"code\" : \"200\",'+
		''+
		'\"application\" : \"rmit-system-api-sfdc\",'+
		''+
		'\"provider\" : \"SAMS\",'+
		''+
		'\"payload\" :'+
		''+
		'['+
		''+
		'{'+
		''+
		'\"courseConnectionId\":\"\",'+ 
		''+
		'\"result\": \"Error\",'+
		''+
		'\"errorCode\": \"101\",'+
		''+
		'\"errorText\": \"Invalid Student ID\"'+
		''+
		'}'+
		']'+
		''+
		'}';

		JsonCourseWrapper obj = JsonCourseWrapper.parse(json);
		System.assert(obj != null);
	}
    
    
}