/**************************************************************
Purpose: To View All project list 
History:
Created by Subhajit on 10/03/2018
RPORW-47 
*************************************************************/
public class ResearchProjectListViewController {
    
     /*****************************************************************************************
     Global variable Declaration
    //****************************************************************************************/
    public static Boolean isForceExceptionRequired=false;//added by Ali
    public static String fieldName='Group_Status__c'; //added by Subhajit :: 982
    
    /************************************************************************************
    // Purpose      :  Fetching Current User record
    // Parameters   :  void     
    // Developer    :  Subhajit
    // Created Date :  04/03/2018                 
    //***********************************************************************************/
    @AuraEnabled 
    public static user researchProjectListViewFetchUser(){
        User oUser = new User();
        try
        {
            oUser = SobjectUtils.fetchUser();
            ResearcherExceptionHandlingUtil.createForceExceptionForTestCoverage(isForceExceptionRequired);
            
        }
        catch(Exception ex) 
        {            
            //Exception handling Error Log captured :: Starts
            ResearcherExceptionHandlingUtil.addExceptionLog(ex,ResearcherExceptionHandlingUtil.getParamMap()); //added by Ali
            //Exception handling Error Log captured :: Ends
            System.debug('Exception occured : '+ex.getMessage());  
        } 
        
        return oUser;
    }
   
    /************************************************************************************
    // RPORW-721     :  RPORW-721:Filter projects list
    // SPRINT       :  SPRINT-13
    // Purpose      :  to fetch the picklist value 
    // Parameters   :  sObject objObject, string fld		   	
    // Developer    :  Subhajit
    // Created Date :  05/03/2019                 
    //***********************************************************************************/
    @AuraEnabled 
    public static List <String> getGroupStatusSelectOptions()
    {
        List < String >allSelectOptions = new  List < String >(); 
      
        try{
            
            allSelectOptions = SobjectUtils.getselectOptions(new ResearcherPortalProject__c(),fieldName);
            //ResearcherExceptionHandlingUtil.createForceExceptionForTestCoverage(isForceExceptionRequired);
            
        }  
        catch(Exception ex)     
        {   
            //Exception handling Error Log captured :: Starts
            ResearcherExceptionHandlingUtil.addExceptionLog(ex,ResearcherExceptionHandlingUtil.getParamMap()); //added by Ali
            //Exception handling Error Log captured :: Ends
            System.debug('Exception occured : '+ex.getMessage());
        } 
        return allSelectOptions;
       
    }
   /************************************************************************************
    // RPORW-47     :  View All project list both RM and Non-RM.
    // SPRINT       :  SPRINT-3
    // Purpose      :  to fetch custom research project list view data for UI Component
    // 			       from getMyResearchProjectListViewData method in ResearchProjectHelper
    // 			       Class
    // Parameters   :  Integer limitValue, Integer offsetValue		   	
    // Developer    :  Subhajit
    // Created Date :  10/03/2018                 
    //***********************************************************************************/
    @AuraEnabled    
    public static ProjectDetailsWrapper.ResearchProjectListView getMyResearchProjectListView()
    {
        
        ProjectDetailsWrapper.ResearchProjectListView myResearchProjectListView = new  ProjectDetailsWrapper.ResearchProjectListView(); 
      
        try{
            
            myResearchProjectListView = ResearchProjectHelper.getMyResearchProjectListViewData();
            system.debug('****Myyyy'+myResearchProjectListView);
            system.debug('@@@Istrue==>'+isForceExceptionRequired);
            
            ResearcherExceptionHandlingUtil.createForceExceptionForTestCoverage(isForceExceptionRequired);
            
        }  
        catch(Exception ex)     
        {   
            //Exception handling Error Log captured :: Starts
            ResearcherExceptionHandlingUtil.addExceptionLog(ex,ResearcherExceptionHandlingUtil.getParamMap()); //added by Ali
            //Exception handling Error Log captured :: Ends
            System.debug('Exception occured : '+ex.getMessage());
        } 
        return myResearchProjectListView;
    }  
    
     /************************************************************************************
    // RPORW-919     :  View All public project list both RM and Non-RM.
    // SPRINT       :  SPRINT-3
    // Purpose      :  to fetch custom research public project list view data for UI Component
    // 			       from getRMITPublicResearcherProject method in ResearcherPublicProjectsHelper
    // 			       Class
    // Parameters   :  String accessType   	
    // Developer    :  Subhajit
    // Created Date :  10/03/2018                 
    //***********************************************************************************/
    @AuraEnabled    
    public static ProjectDetailsWrapper.ResearchProjectPublicProjects getMyResearchProjectPublicListView(String searchText,String tabSelected)
    {
        
        ProjectDetailsWrapper.ResearchProjectPublicProjects rmitPublicProjects = new ProjectDetailsWrapper.ResearchProjectPublicProjects();//Gourav
        try{
            ResearcherPublicProjectsHelper.ResearchProjectPublicListViewConfig configuration = new ResearcherPublicProjectsHelper.ResearchProjectPublicListViewConfig();
            configuration.searchText    =   searchText;
            configuration.tabSelected   =   tabSelected;
            
            rmitPublicProjects = new ResearcherPublicProjectsHelper().getRMITPublicResearcherProject(configuration);
            system.debug('****Myyyy'+rmitPublicProjects);
            system.debug('@@@Istrue==>'+isForceExceptionRequired);
            
            ResearcherExceptionHandlingUtil.createForceExceptionForTestCoverage(isForceExceptionRequired);
            
        }  
        catch(Exception ex)     
        {   
            //Exception handling Error Log captured :: Ends
            System.debug('Exception occured : '+ex.getStackTraceString());
            System.debug('Exception occured : '+ex.getMessage());

            //Exception handling Error Log captured :: Starts
            ResearcherExceptionHandlingUtil.addExceptionLog(ex,ResearcherExceptionHandlingUtil.getParamMap()); //added by Ali
            
        } 
        return rmitPublicProjects;
    }  

    /*@Author : Gourav Bhardwaj
      @Story : 
      @Description :  getRMITpUBLIC

      String searchText,String tabSelected,Integer pageNo
    */ 
     @AuraEnabled    
    public static ProjectDetailsWrapper.ResearchProjectPublicProjects getRMITPublicResearchProjectsFiltered(String params)
    {
        
        ProjectDetailsWrapper.ResearchProjectPublicProjects rmitPublicProjects = new ProjectDetailsWrapper.ResearchProjectPublicProjects();//Gourav
        try{
            system.debug('## params : '+params);

            ResearcherPublicProjectsHelper.ResearchProjectPublicListViewConfig configuration = (ResearcherPublicProjectsHelper.ResearchProjectPublicListViewConfig)JSON.deserialize(params, ResearcherPublicProjectsHelper.ResearchProjectPublicListViewConfig.class);

            system.debug('### configuration : '+configuration);

            rmitPublicProjects = new ResearcherPublicProjectsHelper().getRMITPublicResearcherProjectFiltered(configuration);
            system.debug('****Myyyy'+rmitPublicProjects);
            system.debug('@@@Istrue==>'+isForceExceptionRequired);
            
            ResearcherExceptionHandlingUtil.createForceExceptionForTestCoverage(isForceExceptionRequired);
            
        }  
        catch(Exception ex)     
        {   

            //Exception handling Error Log captured :: Ends
            System.debug('Exception occured : '+ex.getMessage()+' : '+ex.getStackTraceString());

            //Exception handling Error Log captured :: Starts
            ResearcherExceptionHandlingUtil.addExceptionLog(ex,ResearcherExceptionHandlingUtil.getParamMap()); //added by Ali
            
        } 
        return rmitPublicProjects;
    }     
    /*@Author : Gourav Bhardwaj
      @Story : 
      @Description :  Get Researcher Portal record Load more functionality
    */
     @AuraEnabled    
    public static ProjectDetailsWrapper.ResearchProjectPublicProjects getNextSetOfRMITPublicResearchProjects(String params)
    {
        
        ProjectDetailsWrapper.ResearchProjectPublicProjects rmitPublicProjects = new ProjectDetailsWrapper.ResearchProjectPublicProjects();//Gourav
        try{
            
             system.debug('## params : '+params);

            ResearcherPublicProjectsHelper.ResearchProjectPublicListViewConfig configuration = (ResearcherPublicProjectsHelper.ResearchProjectPublicListViewConfig)JSON.deserialize(params, ResearcherPublicProjectsHelper.ResearchProjectPublicListViewConfig.class);

            system.debug('### configuration : '+configuration);

            rmitPublicProjects = new ResearcherPublicProjectsHelper().getRMITPublicResearcherProjectFiltered(configuration);
            //system.debug('****Myyyy'+rmitPublicProjects);
            system.debug('@@@Istrue==>'+rmitPublicProjects);
            
            ResearcherExceptionHandlingUtil.createForceExceptionForTestCoverage(isForceExceptionRequired);
            
        }  
        catch(Exception ex)     
        {   
            //Exception handling Error Log captured :: Starts
            ResearcherExceptionHandlingUtil.addExceptionLog(ex,ResearcherExceptionHandlingUtil.getParamMap()); //added by Ali
            //Exception handling Error Log captured :: Ends
            System.debug('Exception occured : '+ex.getStackTraceString());
        } 
        return rmitPublicProjects;
        //return null;
    } 

    //RPROW-1024
    @AuraEnabled 
    public static List < String > getFORValues() { 
        
        List < String > picklistValues = new List<String>();
        
        try{
            sObject obj = Schema.getGlobalDescribe().get('ResearcherPortalProject__c').newSObject();
            System.debug('obj@@@'+obj);

            picklistValues = SobjectUtils.getselectOptions(obj,'Discipline_area__c');

            system.debug('## picklistValues :'+picklistValues);
            
			
            
            picklistValues.addAll(ResearcherPublicProjectsHelperUtil.getSixDigitForCodes());
            
        } catch(Exception ex){
            //Exception handling Error Log captured :: Ends
            System.debug('@@@Exception occured: '+ex.getMessage());
            System.debug('@@@Exception occured: '+ex.getStackTraceString());


            //Exception handling Error Log captured :: Starts
            ResearcherExceptionHandlingUtil.addExceptionLog(ex,ResearcherExceptionHandlingUtil.getParamMap());
            
            
        }
        return picklistValues;
       
    }

    //RPROW-981
     @AuraEnabled 
    public static List < String > getImpactCategory() { 
        
        List < String > picklistValues = new List<String>();
        
        try{
            sObject obj = Schema.getGlobalDescribe().get('ResearcherPortalProject__c').newSObject();
            System.debug('obj@@@'+obj);

            picklistValues = SobjectUtils.getselectOptions(obj,'Impact_Category__c');

            system.debug('## picklistValues :'+picklistValues);
            
        } catch(Exception ex){
            //Exception handling Error Log captured :: Ends
            System.debug('@@@Exception occured: '+ex.getMessage());
            System.debug('@@@Exception occured: '+ex.getStackTraceString());


            //Exception handling Error Log captured :: Starts
            ResearcherExceptionHandlingUtil.addExceptionLog(ex,ResearcherExceptionHandlingUtil.getParamMap());
            
            
        }
        return picklistValues;
       
    }
    
        //RPORW-1415
        @AuraEnabled 
        public static Integer getRecordSizeLimit() { 
            RSTP_Project_Collaboration__mdt collaborationMetadata = ResearcherPublicProjectsHelperUtil.getCollaborationMetadata();
            if(collaborationMetadata!=null && collaborationMetadata.Records_Size__c!=null){
                return collaborationMetadata.Records_Size__c.intValue();
            }else{
                return 1000;
            }
        }//getRecordSizeLimit
        
        
    /************************************************************************************
    // JIRA         : RPORW-1638
    // Purpose      : To show the CreateProject Button
    // Parameters   :        
    // Developer    : Ankit
    // Created Date : 19/06/2019
     ***********************************************************************************/
    
    @AuraEnabled 
    public static Boolean isCreateProjectApplicable (){
        
        Boolean isCreateProjectApplicable = false;
        // To check the Profile Access of the User as only 9 Profiles should be applicable
        
        Boolean isCurrentUserProfileAccessibleFlag = SobjectUtils.getProfileAccessCheckInfo(userinfo.getProfileId()); 
              System.debug('@@@isCurrentUserProfileAccessibleFlag'+isCurrentUserProfileAccessibleFlag);
              if(isCurrentUserProfileAccessibleFlag)   { 
                  isCreateProjectApplicable =  true ;
                 }  
                  
        return isCreateProjectApplicable;
    }
}