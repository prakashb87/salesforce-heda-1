/*****************************************************************************************************
*** @Class             : CourseIdIntegrationCanvasTest
*** @Author            : Avinash Machineni 
*** @Requirement       : Integration between Salesforce and IPaaS for sending course SIS Id's and record id's
*** @Created date      : 24/05/2018
*** @JIRA ID           : ECB-3251
*****************************************************************************************************/
/*****************************************************************************************************
*** @About Class
*** This class is used integrate the records from Salesforce and IPaas for sending course Id's
*****************************************************************************************************/
@isTest
public class CourseIdIntegrationCanvasTest {
    class EchoHttpMock implements HttpCalloutMock {
        HttpResponse res;
        EchoHttpMock(HttpResponse r) {
            res = r;
        }
       
        // This is the HttpCalloutMock interface method
        public HttpResponse respond(HttpRequest req) {
            return res;
        }
    }
    
    static testMethod void testParse() {
        
        TestDataFactoryUtil.dummycustomsetting();
        Account acc1 = new Account(Name = 'Test Account');
        insert acc1;
        hed__Course__c crs1 = new hed__Course__c(Name = 'Test Course', hed__Account__c = acc1.Id, status__c = 'Active', SIS_Course_Id__c = '123244');
        insert crs1;
        
        HttpResponse res = new HttpResponse();
        res.setBody('{"payload": [{"sf_record_id": "'+crs1.Id+'","sis_course_id": "'+crs1.SIS_Course_Id__c+'"}]}');
        res.setStatusCode(200);
        Test.setMock(HttpCalloutMock.class, new EchoHttpMock(res));

        Test.startTest();
        //system.assert(crs1 !=null, 'true');
        CourseIdIntegrationCanvas.returntoIpass(crs1.id,crs1.SIS_Course_Id__c);
        system.assert(crs1.Id !=null, 'true');
        Test.stopTest();
            
    }
    
    // ..........21CC standard user Profile fix for Retrieve course offering functionality................    
    static testMethod void testBusinessAdminProfile()
    {
         TestDataFactoryUtil.dummycustomsetting();
         TestDataFactoryUtilRefOne.testDataFactory();
         system.runAs(TestDataFactoryUtil.standardUser21CC)
         {
                PermissionSet ps = [SELECT ID From PermissionSet WHERE Name = :ConfigDataMapController.getCustomSettingValue('MarketPlace Permission Set')];
                system.debug('PSSSSS=' +ps);
                insert new PermissionSetAssignment(AssigneeId = TestDataFactoryUtil.standardUser21CC.id, PermissionSetId = ps.Id );
      
                String message1 = CourseIdIntegrationCanvas.returntoIpass(TestDataFactoryUtil.course.Id,''); 
                
                
                system.assert(TestDataFactoryUtil.course.Id != null, 'true');
                
                system.assert(message1=='');
                            
        }
    }
    static testMethod void testStandardUserProfile()
    {
         TestDataFactoryUtil.dummycustomsetting();
         TestDataFactoryUtilRefOne.testDataFactory();
         system.runAs(TestDataFactoryUtil.standardUser21CC)
         {
                
      
                String message1 = CourseIdIntegrationCanvas.returntoIpass(TestDataFactoryUtil.course.Id,''); 
                
                
                system.assert(TestDataFactoryUtil.course.Id != null, 'true');
                
                system.assert(message1=='Error2');
                            
        }
    }
    
    
}