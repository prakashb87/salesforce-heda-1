/**************************************************************
Purpose: Handler class of ResearcherPortalProject Trigger
History:
Created by Subhajit on 10/08/2018
*************************************************************/
public with Sharing class ResearcherPortalProjectTriggerHandler {
    Public static final string DEFAULT_STATUS='Active';
    Public static final string IGNORE_STATUS='Ignore';
    
    /*****************************************************************************************
    // JIRA No      :  RPORW-982
    // SPRINT       :  SPRINT-13
    // Purpose      :  get configuration data from custom metadata for update
    // Parameters   :  list of ResearcherPortalProject__c 
    // Developer    :  Subhajit
    // Created Date :  04/03/2019                 
    //****************************************************************************************/  
    public static List<Group_Project_Application_Status__mdt> getMapSetofStatusConfigData(list<ResearcherPortalProject__c> rPortalProjs) 
    {
        Set<String> grpStatusSet = new Set<String>();
        Set<Boolean> grpClosedOffSet = new Set<Boolean>();
        for(ResearcherPortalProject__c rPortalProj :rPortalProjs)
        {
            grpStatusSet.add(rPortalProj.Status__c);
            grpClosedOffSet.add(rPortalProj.ls_Closed_Off__c);
            
        }
        
		if (!Schema.sObjectType.Group_Project_Application_Status__mdt.fields.backendApplicationStatus__c.isAccessible()){
                system.debug('backendApplicationStatus__c ==> is not accessible');
        }
        List<Group_Project_Application_Status__mdt> groupStatusConfigRecs =[SELECT Portal_Project_Status__c,backendApplicationStatus__c,IsCLosedOff__c 
                                                                            FROM Group_Project_Application_Status__mdt // custom metadata
                                                                            WHERE backendApplicationStatus__c IN : grpStatusSet
                                                                            AND IsCLosedOff__c IN : grpClosedOffSet
                                                                           ];
        return groupStatusConfigRecs;
    }
   /*****************************************************************************************
    // JIRA No      :  RPORW-982
    // SPRINT       :  SPRINT-13
    // Purpose      :  get group status
    // Parameters   :  List<Group_Project_Application_Status__mdt> grpPrjAppStatusConfigRecs,ResearcherPortalProject__c rPortalProj 
    // Developer    :  Subhajit
    // Created Date :  04/03/2019                 
    //****************************************************************************************/   
  /* public static String getGroupStatus(List<Group_Project_Application_Status__mdt> grpPrjAppStatusConfigRecs, ResearcherPortalProject__c rPortalProj) 
    {
        String groupStatus;  
        for(Group_Project_Application_Status__mdt grpPrjAppStatus : grpPrjAppStatusConfigRecs)
                    {
                        if(rPortalProj.Status__c== grpPrjAppStatus.backendApplicationStatus__c 
                           && rPortalProj.ls_Closed_Off__c==grpPrjAppStatus.IsCLosedOff__c)
                        {
                             groupStatus = grpPrjAppStatus.Portal_Project_Status__c;
                        }
                    }
        return groupStatus;
    }*/
    /*****************************************************************************************
    // JIRA No      :  RPORW-264
    // SPRINT       :  SPRINT-4
    // Purpose      :  Changing status of RM project data
    // Parameters   :  list of ResearcherPortalProject__c 
    // Developer    :  Subhajit
    // Created Date :  10/08/2018                 
	//****************************************************************************************/
    public static void changeStatusForRMProj(list<ResearcherPortalProject__c> rPortalProjs)
    {
        Map<String,String> applicationStatusToStatusMap =  new Map<String,String>();
        try{
            List<Group_Project_Application_Status__mdt> grpPrjAppStatusConfigRecs = ResearcherPortalProjectTriggerHandler.getMapSetofStatusConfigData(rPortalProjs);
            system.debug('@@@@grpPrjAppStatusConfigRecs===>'+JSON.serializePretty(grpPrjAppStatusConfigRecs));
            for(Group_Project_Application_Status__mdt confData : grpPrjAppStatusConfigRecs)
            {
                applicationStatusToStatusMap.put(confData.backendApplicationStatus__c+'#'+confData.IsCLosedOff__c,confData.Portal_Project_Status__c);
            }
            
            for(ResearcherPortalProject__c rPortalProj :rPortalProjs)
            {
                // If RM Project Filter condition
                if(rPortalProj.RecordTypeId==Schema.SObjectType.ResearcherPortalProject__c.getRecordTypeInfosByName().get(Label.RSTP_ResearchProjectRMRecordtype).getRecordTypeId())
                {
                    rPortalProj.Portal_Project_Status__c=Label.RSTP_ResearchProjectConductingStatus;
                    rPortalProj.Access__c=Label.RSTP_MembersAccessLevel;
                    //rPortalProj.Group_Status__c = getGroupStatus(grpPrjAppStatusConfigRecs,rPortalProj);
                    if(applicationStatusToStatusMap.get(rPortalProj.Status__c+'#'+rPortalProj.ls_Closed_Off__c)!=IGNORE_STATUS)
                    {
                        rPortalProj.Group_Status__c = applicationStatusToStatusMap.get(rPortalProj.Status__c+'#'+rPortalProj.ls_Closed_Off__c);
                    }
                    else
                    {
                        rPortalProj.Group_Status__c ='';
                    }
                }
                else if(rPortalProj.RecordTypeId==Schema.SObjectType.ResearcherPortalProject__c.getRecordTypeInfosByName().get(Label.RSTP_ResearchProjectRPPRecordtype).getRecordTypeId())
                {
                    rPortalProj.Group_Status__c =DEFAULT_STATUS;
                }
                
            }   
            
            system.debug('@@@@rPortalProjs===>'+JSON.serializePretty(rPortalProjs));
            
        } catch(Exception ex){
            //Exception handling Error Log captured :: Starts
            ResearcherExceptionHandlingUtil.addExceptionLog(ex,ResearcherExceptionHandlingUtil.getParamMap()); //added by Ali
            //Exception handling Error Log captured :: Ends
            System.debug('@@@Exception occured: '+ex.getMessage());
            
        }
    }
    
    
   
    
    /*****************************************************************************************
    // JIRA No      :  RPORW-982
    // SPRINT       :  SPRINT-4
    // Purpose      :  Change application status
    // Parameters   :  list<ResearcherPortalProject__c>
    // Developer    :  Subhajit
    // Created Date :  07/03/2019                
    //****************************************************************************************/
    public static void changeGoupStatusAsPerApplicationStatus(list<ResearcherPortalProject__c> rPortalProjs,Map<Id,ResearcherPortalProject__c> rOldPortalProjsMap)
    {
        Map<String,String> applicationStatusToStatusMap =  new Map<String,String>();
        try{
            List<Group_Project_Application_Status__mdt> grpPrjAppStatusConfigRecs = ResearcherPortalProjectTriggerHandler.getMapSetofStatusConfigData(rPortalProjs);
            system.debug('@@@@grpPrjAppStatusConfigRecs===>'+JSON.serializePretty(grpPrjAppStatusConfigRecs));
            
            for(Group_Project_Application_Status__mdt confData : grpPrjAppStatusConfigRecs)
            {
                applicationStatusToStatusMap.put(confData.backendApplicationStatus__c+'#'+confData.IsCLosedOff__c,confData.Portal_Project_Status__c);
            }
            for(ResearcherPortalProject__c rPortalProj :rPortalProjs)
            {
                
                if( (rPortalProj.Group_Status__c==null && rPortalProj.Status__c !=null)
                   ||(rOldPortalProjsMap!=null && rPortalProj.Status__c != rOldPortalProjsMap.get(rPortalProj.Id).Status__c)
                   || (rOldPortalProjsMap!=null && rPortalProj.ls_Closed_Off__c != rOldPortalProjsMap.get(rPortalProj.Id).ls_Closed_Off__c)
                   
                  ) 
                {
                    //rPortalProj.Group_Status__c =  getGroupStatus(grpPrjAppStatusConfigRecs,rPortalProj);
                    if(applicationStatusToStatusMap.get(rPortalProj.Status__c+'#'+rPortalProj.ls_Closed_Off__c)!=IGNORE_STATUS)
                    {
                        rPortalProj.Group_Status__c = applicationStatusToStatusMap.get(rPortalProj.Status__c+'#'+rPortalProj.ls_Closed_Off__c);
                    }
                    else
                    {
                        rPortalProj.Group_Status__c ='';
                    }
                }
                
            }
            
            
            
        } catch(Exception ex){
            //Exception handling Error Log captured :: Starts
            ResearcherExceptionHandlingUtil.addExceptionLog(ex,ResearcherExceptionHandlingUtil.getParamMap()); //added by Ali
            //Exception handling Error Log captured :: Ends
            System.debug('@@@Exception occured: '+ex.getMessage());
            
        }
    }    
  
    
    /*****************************************************************************************
    // JIRA No      :  RPORW-152
    // SPRINT       :  SPRINT-4
    // Purpose      :  Change project access - permissions for RM
    // Parameters   :  list<ResearcherPortalProject__c>
    // Developer    :  Subhajit
    // Created Date :  10/10/2018                 
    //****************************************************************************************/
    public static void assignProjectAccessProject(list<ResearcherPortalProject__c> rPortalProjs,list<ResearcherPortalProject__c> rOldPortalProjs)
    {
        list<ResearcherPortalProject__c> rmProjs = new list<ResearcherPortalProject__c>();
        Map<Id,String> projectIdToOldAccessMap = prepareIdToOldAccessMap(rOldPortalProjs); //Added for PMD
        
        try{
            if(rPortalProjs!=null && ! rPortalProjs.isEmpty())
            {
                for(ResearcherPortalProject__c rPortalProj :rPortalProjs)
                {
                    // If Access got changed/modified
                    if(projectIdToOldAccessMap.get(rPortalProj.Id) != null && rPortalProj.Access__c!= projectIdToOldAccessMap.get(rPortalProj.Id)) {
                        rmProjs.add(rPortalProj);
                    }
                    
                }
                if(rmProjs!=null && ! rPortalProjs.isEmpty()) {
                    ResearcherMemberSharingUtils.operatingProjectAccess(rmProjs ); // Added by subhajit :: 10.10.18 ::RPORW-152
                }
                
            }     
        } catch(Exception ex){
            //Exception handling Error Log captured :: Starts
            ResearcherExceptionHandlingUtil.addExceptionLog(ex,ResearcherExceptionHandlingUtil.getParamMap()); //added by Ali
            //Exception handling Error Log captured :: Ends
            System.debug('@@@Exception occured: '+ex.getMessage());
            
        }
    }
    
    //Khushman:Created for PMD Fix
    public static Map<Id,String> prepareIdToOldAccessMap(List<ResearcherPortalProject__c> rOldPortalProjs){
        Map<Id,String> projectIdToOldAccessMap = new Map<Id,String>();
        
        try{
            if(rOldPortalProjs!=null && ! rOldPortalProjs.isEmpty())
            {
                for(ResearcherPortalProject__c oldRec : rOldPortalProjs) {
                    projectIdToOldAccessMap.put(oldRec.id,oldRec.Access__c);
                }
            }
        } catch(Exception ex){
            //Exception handling Error Log captured :: Starts
            ResearcherExceptionHandlingUtil.addExceptionLog(ex,ResearcherExceptionHandlingUtil.getParamMap()); //added by Ali
            //Exception handling Error Log captured :: Ends
            System.debug('@@@Exception occured: '+ex.getMessage());
            
        }
        return projectIdToOldAccessMap;
    }
    
    /*****************************************************************************************
    // JIRA No      :  RPORW-998
    // SPRINT       :  SPRINT-12
    // Purpose      :  Changing status of RM project data
    // Parameters   :  list of ResearcherPortalProject__c 
    // Developer    :  Ankit
    // Created Date :  03/03/2019                 
    //****************************************************************************************/
    public static void deleteProjectMemberSharingRecords(list<ResearcherPortalProject__c> rPortalProjs)
    {
        Set<Id> projectIds = new Set<Id>();
        Id projectMemberSharingRecordtypeId= Schema.SObjectType.Researcher_Member_Sharing__c.getRecordTypeInfosByName().get(Label.RSTP_ResearchProjectRecordtype).getRecordTypeId();
        
        try{
            for(ResearcherPortalProject__c rPortalProj :rPortalProjs)
            {
                projectIds.add(rPortalProj.id);                
            } 
            
            if (!Schema.sObjectType.Researcher_Member_Sharing__c.fields.SF_Role__c.isAccessible()){
                system.debug('SF_Role__c==> is not accessible');
                //ResearcherExceptionHandlingUtil.addLog('Field is not accessible',ResearcherExceptionHandlingUtil.getParamMap());
            } 
            list<Researcher_Member_Sharing__c> lstProjectMemberSharing =[SELECT id,User__c, SF_Role__c,Contact__c,
                                                                         Researcher_Portal_Project__c,Ethics__c 
                                                                         FROM Researcher_Member_Sharing__c
                                                                         WHERE Researcher_Portal_Project__c IN : projectIds                                                
                                                                         ANd recordtypeId =:projectMemberSharingRecordtypeId
                                                                         AND Currently_Linked_Flag__c =true
                                                                        ]; 
            
            System.debug('@@@lstProjectMemberSharing'+lstProjectMemberSharing);
            if (!Researcher_Member_Sharing__c.sObjectType.getDescribe().isDeletable()){
                
                System.debug('ResearchProjectContract__Share is not deletable');
            }
            delete lstProjectMemberSharing;
            
        } catch(Exception ex){
            //Exception handling Error Log captured :: Starts
            ResearcherExceptionHandlingUtil.addExceptionLog(ex,ResearcherExceptionHandlingUtil.getParamMap()); //added by Ali
            //Exception handling Error Log captured :: Ends
            System.debug('@@@Exception occured: '+ex.getMessage());
            
        }
    }    
 
    /*****************************************************************************************
    // Purpose : RPORW-1041, To assign RMIT Public Access Type in RM Projects
    // Parameters :List<ResearcherPortalProject__c> 
    // Developer : Ankit
    // Created Date : 10.04.19 
    //****************************************************************************************/
    public static void assignRMITPublicAccessType(List<ResearcherPortalProject__c> projectList, Map<id,ResearcherPortalProject__c> oldProjectMap){
        Set<Id> projectIdSet = new Set<Id>();                                           
        List<ResearcherPortalProject__c> projectUpsertList = new List<ResearcherPortalProject__c>();
        
        List<ResearcherPortalProject__c> rmChangedProjectList = new List<ResearcherPortalProject__c>();
        
        for(ResearcherPortalProject__c eachProject:projectList){
            
             if(eachProject.Project_Type__c != oldProjectMap.get(eachProject.id).Project_Type__c ||
                eachProject.Group_Status__c != oldProjectMap.get(eachProject.id).Group_Status__c ||
                eachProject.ls_Funded__c != oldProjectMap.get(eachProject.id).ls_Funded__c ||
                eachProject.ls_Confidential__c != oldProjectMap.get(eachProject.id).ls_Confidential__c)
               {
                 projectIdSet.add(eachProject.Id);
                 rmChangedProjectList.add(eachProject);
             }
        } 
        
        getProjectListToUpsert(projectIdSet,rmChangedProjectList); 
    
    }    
    
    /*****************************************************************************************
    // Purpose : RPORW-1041, To update/insert the RM Project List with Access Field
    // Parameters :List<ResearcherPortalProject__c> 
    // Developer : Ankit
    // Created Date : 10.04.19 
    //****************************************************************************************/
    public static void getProjectListToUpsert(Set<Id> projectIdSet, List<ResearcherPortalProject__c> rmChangedProjectList ){

        Map<Id,List<Research_Project_Funding__c>>  projectIdAndFundingListMap = new Map<Id,List<Research_Project_Funding__c>>();
        List<Research_Project_Funding__c> fundingList = new List<Research_Project_Funding__c>();

        if(Schema.sObjectType.Research_Project_Funding__c.fields.Organisation_Ecode__c.isAccessible()){
        fundingList = [SELECT id, Organisation_Ecode__c,ResearcherPortalProject__c FROM Research_Project_Funding__c
                                                        WHERE ResearcherPortalProject__c IN:projectIdSet];
        }    
                                            
        projectIdAndFundingListMap =  getprojectIdAndFundingListMap(fundingList);       
        upsertProjectList(rmChangedProjectList,projectIdAndFundingListMap); 
 
    }   
    
    
    
   /*****************************************************************************************
    // Purpose : RPORW-1041, To update/insert the RM Project List with Access Field
    // Parameters :List<ResearcherPortalProject__c> 
    // Developer : Ankit
    // Created Date : 10.04.19 
    //****************************************************************************************/
    public static void upsertProjectList(List<ResearcherPortalProject__c> projectList,Map<Id,List<Research_Project_Funding__c>> projectIdAndFundingListMap){
    Id rmRecordTypeId =  Schema.SObjectType.ResearcherPortalProject__c.getRecordTypeInfosByName().get(Label.ResearchMasterProject_RecorType).getRecordTypeId();
        
		    List<ResearcherPortalProject__c> projectUpsertList = new List<ResearcherPortalProject__c>();
		    for(ResearcherPortalProject__c eachProject: projectList){
		     if(eachProject.recordTypeId == rmRecordTypeId ){
		    	
	            ResearcherPortalProject__c projectToUpdate = new ResearcherPortalProject__c(Id=eachProject.Id);
	            
	            List<Research_Project_Funding__c> tempFundingList = projectIdAndFundingListMap.get(eachProject.id);
	            if(tempFundingList!= null && !tempFundingList.isEmpty()){
	               projectToUpdate= setAccessToRMProject(tempFundingList,projectToUpdate,eachProject); 
	            }else
	            {
	                    projectToUpdate.Access__c = Label.RSTP_MembersAccessLevel;
	            }
	            projectUpsertList.add(projectToUpdate);           
		     }   
	        }  
        
        if(projectUpsertList.size()>0){
            if(!Schema.sObjectType.ResearcherPortalProject__c.fields.Access__c.IsCreateable() || !Schema.sObjectType.ResearcherPortalProject__c.fields.Access__c.isUpdateable())
                {
                    system.debug('ParentId==> is not updateable');
                }
                    
            upsert projectUpsertList;
        }  
          
    }	
    
    public static ResearcherPortalProject__c setAccessToRMProject( List<Research_Project_Funding__c> tempFundingList, ResearcherPortalProject__c projectToUpdate, ResearcherPortalProject__c eachProject )
    {
       for(Research_Project_Funding__c projectFunding : tempFundingList ){ 
                      
                      if( eachProject.Project_Type__c == 'Competitive Research' &&
                          (eachProject.Group_Status__c == 'Active' ||  eachProject.Group_Status__c == 'Closed') &&
                          eachProject.ls_Funded__c == true &&
                          eachProject.ls_Confidential__c == false 
                          &&(projectFunding.Organisation_Ecode__c == 'ARC' || projectFunding.Organisation_Ecode__c == 'NHMRC')
                           ) {
                                system.debug('public');
                                projectToUpdate.Access__c = Label.RSTP_PublicAccessLevel;  
                                break;
                             }
                          else
                             {  
                                 system.debug('Members');
                                 projectToUpdate.Access__c = Label.RSTP_MembersAccessLevel;
                             }
                  }
                System.debug('@@@projectToUpdate'+projectToUpdate);

            return projectToUpdate;    
    }  
 
    /*****************************************************************************************
    // Purpose : RPORW-1041, To get the wrapper of checkRMITPublicWrapper 
    // Parameters :ID projectId
    // Developer : Ankit
    // Created Date : 10.04.19 
    //****************************************************************************************/
    public static Map<Id,List<Research_Project_Funding__c>> getprojectIdAndFundingListMap(List<Research_Project_Funding__c> fundingList){
         
        Map<Id,List<Research_Project_Funding__c>>  projectIdAndFundingListMap = new Map<Id,List<Research_Project_Funding__c>>();
        
        for(Research_Project_Funding__c eachFunding : fundingList) {
            if(projectIdAndFundingListMap.containsKey(eachFunding.ResearcherPortalProject__c)) {
                List<Research_Project_Funding__c> tempFundingList = projectIdAndFundingListMap.get(eachFunding.ResearcherPortalProject__c);
                tempFundingList.add(eachFunding);
                projectIdAndFundingListMap.put(eachFunding.ResearcherPortalProject__c, tempFundingList);
            } else {
                projectIdAndFundingListMap.put(eachFunding.ResearcherPortalProject__c, new List<Research_Project_Funding__c> {eachFunding});
            }
        }
        
        return projectIdAndFundingListMap;
    }      
    
}