/*****************************************************************
Name: EmbeddedSyncBasketIntegrationBatch
Author: Capgemini [Shreya]
Purpose: Handle Canvas Integration  for all the course connection records which are created via embedded batch credential job.
Jira Reference : ECB-3900,ECB-5545,ECB-5549
*****************************************************************/
/*==================================================================
History
--------
Version   Author            Date              Detail
1.0       Shreya           23/06/2018         Initial Version
********************************************************************/
public class EmbeddedSyncBasketIntegrationBatch implements Database.Batchable<sObject>,Database.Stateful,Database.AllowsCallouts {

  public String querystring ;
  public List<hed__Course_Enrollment__c> courseConnList = new List<hed__Course_Enrollment__c>();
  @testVisible public Integer failCount = 0;
  
  public Database.QueryLocator start(Database.BatchableContext bc) {
         //ECB-5545: Added Canvas_Integration_Processing_Complete__c check in the query : Shreya
         querystring = 'Select id, Name,Service__c,Canvas_Integration_Processing_Complete__c FROM hed__Course_Enrollment__c WHERE Source_Type__c = \'MKT-Batch\' AND Canvas_Integration_Processing_Complete__c= false ';       
         querystring += 'AND (hed__Status__c = \'Current\' OR hed__Status__c = \'Enrolled\')';
         return Database.getQueryLocator(querystring);
    }
    
    public void execute(Database.BatchableContext bc, List<hed__Course_Enrollment__c> scope){
       List<Id> serviceIdList = new List<Id>();
       Set<Id> courseConnIdSet = new Set<Id>();       
       system.debug('scope::::'+scope);
       
       if(!scope.isEmpty()){
           system.debug('scope not empty');
           courseConnList.addAll(scope);
           for(hed__Course_Enrollment__c courseconn : scope){
               //serviceIdList.add(courseconn.Service__c);
               courseConnIdSet.add(courseconn.Id);
           }
       }
       try{
           //Canvas Integration
            CanvasEnrollmentRequest enrollReq = new CanvasEnrollmentRequest();
            EmbeddedCanvasEnrollmentService canvasCon = new EmbeddedCanvasEnrollmentService(enrollReq);
            canvasCon.sendCourseConnIds(courseConnIdSet);
            //counting on the integration failure
            failCount = failcount+canvasCon.getFailedCount();
       }catch(Exception ex){
           ApplicationErrorLogger.logError(ex);
       }
          
          
          
    }
    
    public void finish(Database.BatchableContext bc) {
        system.debug('failCount :::'+failCount );
        
        if(failCount > 0){
     
            Messaging.SingleEmailMessage email =new Messaging.SingleEmailMessage();
            String[] toAddresses = new list<string> {ConfigDataMapController.getCustomSettingValue('ECB Support Email')};
            String subject = ConfigDataMapController.getCustomSettingValue('EmbedErrorEmailSubject');
            email.setToAddresses(toAddresses);
            email.setSubject(subject);
            String emailBody = 'Canvas Enrollment Failure on'+ System.Today() + '\n';
            emailBody += 'No of records errored : '+failCount;
            email.setPlainTextBody(emailBody);                     
            
            Messaging.SendEmailResult [] r = Messaging.sendEmail(new Messaging.SingleEmailMessage[] {email}); 
        }    
        
        EmbeddedSamsIntegrationBatch btch= new EmbeddedSamsIntegrationBatch(); 
        Database.executeBatch(btch,10);
       
        
        
    }
    
    
}