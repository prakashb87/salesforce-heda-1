/*****************************************************************************************************
 *** @Class             : ErrorLogCleanUpBatchTest
 *** @Author            : Praneet Vig
 *** @Requirement       : Test Class to cover the coverage of ErrorLogCleanUpBatch
 *** @Created date      : 07/06/2018
 *** @JIRA ID           : ECB-3768
 *****************************************************************************************************/
/*****************************************************************************************************
 *** @About Class
 *** This class is a Test Class to cover the class Coverage of ErrorLogCleanUpBatch.
 *****************************************************************************************************/ 
@isTest
public class ErrorLogCleanUpBatchTest {
 @testSetup
    static void TestData(){
        schedulerTest();
        //method called from testDataFactoryUtil class to retrieve test data
        // ECB-3768
        TestDataFactoryUtilRefOne.ErrorLogCleanUpScheduler();
         
        
    }
        static testmethod void test() {        
        Test.startTest();
        ErrorLogCleanUpBatch cub = new ErrorLogCleanUpBatch();
        Id batchId = Database.executeBatch(cub);
        Test.stopTest();
    }
    public static void schedulerTest() 
    {
        String CRON_EXP = '0 15 17 ? * MON-FRI';
        
        ErrorLogCleanUpScheduler p = new ErrorLogCleanUpScheduler();
    
            String jobId = System.schedule('Product Data sync',  CRON_EXP, p);
            CronTrigger ct = [SELECT Id, state, PreviousFireTime, CronExpression, TimesTriggered, NextFireTime FROM CronTrigger WHERE id = :jobId];
            System.assertEquals(CRON_EXP, ct.CronExpression);
            System.assertEquals(0, ct.TimesTriggered);
            
        
    }
       

}