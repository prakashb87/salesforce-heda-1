/**********************************************************************   
    @Author     : Gourav Bhardwaj
      @Story        : RPORW-294
      @CreatedDate  : 1/4/2019
    @description  : Class Created to Handle Public Researcher Projects Logic Only.
***********************************************************************/
public with Sharing class ResearcherPublicProjectsHelper{
   
   public static Map<id,List<Researcher_Member_Sharing__c>> allProjectMemberMap;
   public static List<ProjectDetailsWrapper.AllResearchProjectPublicWrapper> globalResearcherProjects;//gourav
   public Map<id,List<Research_Project_Classification__c>> forCodesForProjects; //RPORW-979 

   public ResearcherPublicProjectsHelper(){
      List<ResearcherPortalProject__c> globalListPublicProjectList = new List<ResearcherPortalProject__c>(); 

      allProjectMemberMap = new Map<id,List<Researcher_Member_Sharing__c>>();
      forCodesForProjects  = new Map<id,List<Research_Project_Classification__c>>();//RPORW-979
      globalResearcherProjects = new List<ProjectDetailsWrapper.AllResearchProjectPublicWrapper>();//Gourav

      String accessType = Label.RMITPublic;
      String orderByClause = ' ORDER BY LastModifiedDate DESC';                                                                                   
      String filterClause='';
      String queryLimit =' LIMIT 50000';
      String notIncludedProjectStatus = 'Archive';

      String researchPortalProjectQuery = 'Select Id,Impact_Category__c,Title__c,Description__c,Keywords__c,RecordTypeid FROM ResearcherPortalProject__c WHERE Portal_Project_Status__c !=:notIncludedProjectStatus AND Access__c=:accessType';
      researchPortalProjectQuery += filterClause + orderByClause + queryLimit;

      globalListPublicProjectList = Database.query(string.escapeSingleQuotes(researchPortalProjectQuery));//depreciated

      ProjectDetailsWrapper.AllResearchProjectPublicWrapper allResearcherProjectWrapper = new ProjectDetailsWrapper.AllResearchProjectPublicWrapper();
      globalResearcherProjects =  allResearcherProjectWrapper.getAllResearcherProjectWrapperList(globalListPublicProjectList);

      allProjectMemberMap = ResearcherPublicProjectsHelperUtil.getProjectMembersForRMITPublicAccess(globalResearcherProjects);

      forCodesForProjects = ResearcherProjectFORCodeUtil.getFORCodesForProjects(globalResearcherProjects); //RPORW-979 

   }
  
    /*
    @Author       : Gourav Bhardwaj
    @Story        : RPORW-294
    @CreatedDate  : 1/4/2019
    @description  : Class Created to Handle Public Researcher Projects Logic Only. CALLED FROM UI
    @return       : ProjectDetailsWrapper.ResearchProjectPublicProjects wrapper data to display in the UI.
    */
    public ProjectDetailsWrapper.ResearchProjectPublicProjects getRMITPublicResearcherProject(ResearchProjectPublicListViewConfig configuration){
        ProjectDetailsWrapper.ResearchProjectPublicProjects rmitPublicProjects = new ProjectDetailsWrapper.ResearchProjectPublicProjects();//needed
        
        ResearcherPublicProjectsHelperUtil.RMITPublicProjectWrapper rmitPublicProjectWrapper   = new ResearcherPublicProjectsHelperUtil.RMITPublicProjectWrapper();
        rmitPublicProjectWrapper.lstResearcherPortalProject       = globalResearcherProjects;
        rmitPublicProjectWrapper.allProjectMemberMap              = allProjectMemberMap;
        rmitPublicProjectWrapper.configuration                    = configuration;
        rmitPublicProjectWrapper.forCodesForProjects              = forCodesForProjects;

        rmitPublicProjects = ResearcherPublicProjectsHelperUtil.createPublicProjectListView(rmitPublicProjectWrapper);

        system.debug(rmitPublicProjects);

        return rmitPublicProjects;
    }//getRMITPublicResearcherProject() Ends



    /*@author     : Gourav Bhardwaj
    @description  : Method called when a tab (All/ RM Project/ User Created Project) is clicked. CALLED FROM UI
    @story        : RPORW-294
    @createdDate  : 1/4/2019
    @return       : ProjectDetailsWrapper.ResearchProjectPublicProjects
    */
    //Changed from getMyResearchProjectPublicListViewDataForATab to getRMITPublicResearcherProjectForSelectedTab
    public ProjectDetailsWrapper.ResearchProjectPublicProjects getRMITPublicResearcherProjectFiltered(ResearchProjectPublicListViewConfig configuration){
        Id userCreatedProjectRecordType = Schema.SObjectType.ResearcherPortalProject__c.getRecordTypeInfosByName().get('Researcher Portal Project').getRecordTypeId();
        Id rmProjectRecordType = Schema.SObjectType.ResearcherPortalProject__c.getRecordTypeInfosByName().get('Researcher Master Project').getRecordTypeId();

        list<ProjectDetailsWrapper.ResearchProjectPublicProjectListView > lstResearchProjectWrapper= new list<ProjectDetailsWrapper.ResearchProjectPublicProjectListView>();

        ProjectDetailsWrapper.ResearchProjectPublicProjects rmitPublicProjects = new ProjectDetailsWrapper.ResearchProjectPublicProjects();
        ProjectDetailsWrapper.ResearchProjectPublicProjectListView myResearchProjectListView;

        String accessType = Label.RMITPublic;
        String orderByClause = ' ORDER BY LastModifiedDate DESC';                                                                                   
        String filterClause='';
        String queryLimit =' LIMIT 50000';
        String notIncludedProjectStatus = 'Archive';

        String projectRecordTypeSelected='All';

        if(String.isBlank(configuration.tabSelected) || configuration.tabSelected.equals('All')){
            projectRecordTypeSelected='All';
        }
        else if(configuration.tabSelected=='RM')
        {
            filterClause += ' AND RecordTypeid =: rmProjectRecordType';
        }else if(configuration.tabSelected=='UC'){
            filterClause += ' AND RecordTypeid =: userCreatedProjectRecordType';
        }  

        List<ResearcherPortalProject__c> globalListPublicProjectList = new List<ResearcherPortalProject__c>(); //depreciated
        allProjectMemberMap = new Map<id,List<Researcher_Member_Sharing__c>>();
       // globalResearcherProjects = new List<ProjectDetailsWrapper.AllResearchProjectPublicWrapper>();//Gourav

        
        //Query for selected Tab if any
        String researchPortalProjectQuery = 'Select Id,Impact_Category__c,Title__c,Description__c,Keywords__c,RecordTypeid FROM ResearcherPortalProject__c WHERE Portal_Project_Status__c !=:notIncludedProjectStatus AND Access__c=:accessType';
        researchPortalProjectQuery += filterClause + orderByClause + queryLimit;
        system.debug('dynamic query : '+researchPortalProjectQuery);
        globalListPublicProjectList = Database.query(string.escapeSingleQuotes(researchPortalProjectQuery));

        allProjectMemberMap = ResearcherPublicProjectsHelperUtil.getProjectMembersForRMITPublicAccess(globalResearcherProjects);

        //If Search Text, Impact Category and FOR Codes are blank when searched for then show all records
         if(String.isBlank(configuration.searchText) 
          && configuration.impactCodeDetailSearch.isEmpty() 
          && configuration.forCodeDetailSearch.isEmpty())
          {
              //Sort the List based on Title field, then update the Global List
              ProjectDetailsWrapper.AllResearchProjectPublicWrapper allResearcherProjectWrapper = new ProjectDetailsWrapper.AllResearchProjectPublicWrapper();
              globalResearcherProjects =  allResearcherProjectWrapper.getAllResearcherProjectWrapperList(globalListPublicProjectList);

             
          }else{
              //Sort the List properly as per AC, then update the Global List
              globalResearcherProjects = recordsWithSearchText(globalListPublicProjectList,configuration,allProjectMemberMap);
          }
        /*
        if(!String.isBlank(configuration.searchText)){
              //Sort the List properly as per AC, then update the Global List
              globalResearcherProjects = recordsWithSearchText(globalListPublicProjectList,configuration,allProjectMemberMap);
          }else{
              //Sort the List based on Title field, then update the Global List
              ProjectDetailsWrapper.AllResearchProjectPublicWrapper allResearcherProjectWrapper = new ProjectDetailsWrapper.AllResearchProjectPublicWrapper();
              globalResearcherProjects =  allResearcherProjectWrapper.getAllResearcherProjectWrapperList(globalListPublicProjectList);
          }
        */

        system.debug('## globalResearcherProjects sorted : '+globalResearcherProjects);

        

        ResearcherPublicProjectsHelperUtil.RMITPublicProjectWrapper rmitPublicProjectWrapper   = new ResearcherPublicProjectsHelperUtil.RMITPublicProjectWrapper();
        rmitPublicProjectWrapper.lstResearcherPortalProject       = globalResearcherProjects;
        rmitPublicProjectWrapper.allProjectMemberMap              = allProjectMemberMap;
        rmitPublicProjectWrapper.configuration                    = configuration;
        rmitPublicProjectWrapper.forCodesForProjects              = forCodesForProjects;
        
        rmitPublicProjects = ResearcherPublicProjectsHelperUtil.createPublicProjectListView(rmitPublicProjectWrapper);//Only 7 records
        //rmitPublicProjects.totalPublicProjects = globalResearcherProjects.size();
        
        return rmitPublicProjects;
    }//Method getMyResearchProjectPublicListViewData() Ends


/* ------------------------------------------------------------- Helper Methods Below --------------------------------------------------------------- */
    /*@author     : Gourav Bhardwaj
    @description  : Method returns a true if Search Text is present in Project Title, Keywords, Member Name, Summary. Else returns false
                    No Search Text present also return true, so that all records can be displayed with out filter
    @story        : RPORW-294
    @createdDate  : 1/4/2019
    @return       : Boolean
    Move this to util class - gourav
    */
    private List<ProjectDetailsWrapper.AllResearchProjectPublicWrapper> recordsWithSearchText(List<ResearcherPortalProject__c> researchProjects,ResearchProjectPublicListViewConfig configuration,Map<id,List<Researcher_Member_Sharing__c>> allProjectMemberMap){
      
      Boolean isSearchTextPresent = false;

      Boolean isSearchTextPresentInPrj = false;
      Boolean isSearchTextPresentInMem = false;
      Boolean isSearchedFORPresent = false; //RPROW-1302
      Boolean isSearchedImpactCategoryPresent = true;//981

      ResearcherPublicProjectsHelperUtil.RMITPublicProjectWrapper rmitPublicProjectWrapper   = new ResearcherPublicProjectsHelperUtil.RMITPublicProjectWrapper();
      rmitPublicProjectWrapper.lstResearcherPortalProject       = globalResearcherProjects;
      rmitPublicProjectWrapper.allProjectMemberMap              = allProjectMemberMap;
      rmitPublicProjectWrapper.configuration                    = configuration;
      rmitPublicProjectWrapper.forCodesForProjects              = forCodesForProjects;      
        
      List<ProjectDetailsWrapper.AllResearchProjectPublicWrapper> allResearchProjectList = new List<ProjectDetailsWrapper.AllResearchProjectPublicWrapper>();

        for(ResearcherPortalProject__c project : researchProjects){
          Integer searchOrder = 0;
          
            //Check if SearchText is present in Project
            ResearcherPublicProjectsHelperUtil.FilteredPublicProjectRecordWrapper filterRecordWrapperPrj =  ResearcherPublicProjectsHelperUtil.isSearchTextPresentInProject(project,rmitPublicProjectWrapper); 
            isSearchTextPresentInPrj = filterRecordWrapperPrj.isSearchTextPresent;
            
            //Check if SearchText is present in Project Members
            ResearcherPublicProjectsHelperUtil.FilteredPublicProjectRecordWrapper filterRecordWrapperMem = ResearcherPublicProjectsHelperUtil.isSearchTextPresentInMemberName(project,rmitPublicProjectWrapper);
            isSearchTextPresentInMem = filterRecordWrapperMem.isSearchTextPresent;
            
            //Check if Selected FOR is present in Project (FORs) - 1302
            ResearcherPublicProjectsHelperUtil.FilteredPublicProjectRecordWrapper filterRecordWrapperFOR = ResearcherProjectFORCodeUtil.isFORCodePresentInProject(project,rmitPublicProjectWrapper);
            isSearchedFORPresent     = filterRecordWrapperFOR.isSearchTextPresent;
            
            //Check if Selected Impact Category in Project 981
            ResearcherPublicProjectsHelperUtil.FilteredPublicProjectRecordWrapper filterRecordWrapperIC = ResearcherProjectImpactCategoryUtil.isImpactCategoryPresentInProject(project,rmitPublicProjectWrapper);            
            isSearchedImpactCategoryPresent     = filterRecordWrapperIC.isSearchTextPresent;
            
            //Created wrapper of all the filtered records wrappers, to make a seperate method and avoid PMD
            AllFilteredRecordWrappers allFilteredRecords = new AllFilteredRecordWrappers();
            allFilteredRecords.filterRecordWrapperPrj	= filterRecordWrapperPrj;
            allFilteredRecords.filterRecordWrapperMem	= filterRecordWrapperMem;
            allFilteredRecords.filterRecordWrapperFOR	= filterRecordWrapperFOR;
            allFilteredRecords.filterRecordWrapperIC	= filterRecordWrapperIC;
            
            //Deciding the Sort order of the list
            searchOrder = getSearchOrder(allFilteredRecords);
           
			system.debug('isSearchTextPresentInPrj : '+isSearchTextPresentInPrj+' : isSearchTextPresentInMem : '+isSearchTextPresentInMem+' : isSearchedFORPresent : '+isSearchedFORPresent+' isSearchedImpactCategoryPresent '+isSearchedImpactCategoryPresent);

			//RPORW-1403 : AND Operation in search keyword, FOR and Impact Category
			//Update RPORW-1383 : OR Operation between fields
            if( (isSearchTextPresentInPrj || isSearchTextPresentInMem) && isSearchedFORPresent && isSearchedImpactCategoryPresent){
                ProjectDetailsWrapper.AllResearchProjectPublicWrapper allResearchProject = new ProjectDetailsWrapper.AllResearchProjectPublicWrapper(project,searchOrder,rmitPublicProjectWrapper.configuration.searchText);
                allResearchProjectList.add(allResearchProject);
            }
           
        }//for(ResearcherPortalProject__c project Ends

        system.debug('## allResearchProjectList : '+allResearchProjectList);
        allResearchProjectList.sort();
        system.debug('## allResearchProjectList sorted : '+allResearchProjectList);

        

      return allResearchProjectList;
    }
    //isRecordsContainingSearchText Ends

  //Method decides the sortorder of the record when record is filtered based on multiple fields have searched values
   private Integer getSearchOrder(AllFilteredRecordWrappers allFilteredRecordWrapper){
	   	Integer searchOrder = 0;
	   	 //Deciding the Sort order of the list
            if(allFilteredRecordWrapper.filterRecordWrapperPrj.searchOrder!=0){
                searchOrder = allFilteredRecordWrapper.filterRecordWrapperPrj.searchOrder;
            }else 
            if(allFilteredRecordWrapper.filterRecordWrapperMem.searchOrder!=0)
            {
             	searchOrder = allFilteredRecordWrapper.filterRecordWrapperMem.searchOrder;
            }else 
            if(allFilteredRecordWrapper.filterRecordWrapperFOR.searchOrder!=0)
            {
             	searchOrder = allFilteredRecordWrapper.filterRecordWrapperFOR.searchOrder;
            }else 
            if(allFilteredRecordWrapper.filterRecordWrapperIC.searchOrder!=0)
            {
             	searchOrder  = allFilteredRecordWrapper.filterRecordWrapperIC.searchOrder;  
            }
	   	return searchOrder;
   
   }
   //getSearchOrder Ends

/*==============================================================================================================================================
                            WRAPPER CLASSES
================================================================================================================================================
*/

    /*@Author     : Gourav Bhardwaj
    @description  : This wrapper class holds configuration values which will be used for doing seach on Public Projects
    @Story        : RPORW-294
    @CreatedDate  : 1/4/2019
    */

    public class ResearchProjectPublicListViewConfig{
        public String searchText;
        public Set<String> forCodeDetailSearch;//1024
        public String tabSelected;
        public Integer pageNo;
        public Set<String> impactCodeDetailSearch;//981

    }
    //ResearchProjectPublicListViewConfig Ends
    
    public Class AllFilteredRecordWrappers{
    	ResearcherPublicProjectsHelperUtil.FilteredPublicProjectRecordWrapper filterRecordWrapperPrj;
    	ResearcherPublicProjectsHelperUtil.FilteredPublicProjectRecordWrapper filterRecordWrapperMem;
    	ResearcherPublicProjectsHelperUtil.FilteredPublicProjectRecordWrapper filterRecordWrapperFOR;
    	ResearcherPublicProjectsHelperUtil.FilteredPublicProjectRecordWrapper filterRecordWrapperIC;
    	
    	
    }
}