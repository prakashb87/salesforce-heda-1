/*********************************************************************************
 *** @ClassName         : Batch_ActivatorLeadAutoConvert
 *** @Author            : Shubham Singh 
 *** @Requirement       : This Batch Class queries the Lead records of RT Activator Registration and passes into helper class
 *** @Created date      : 2019-05-09
 *** @Modified by       : -
 *** @modified date     : -
 *** @Reference		    : RM-2720
 **********************************************************************************/
public with sharing class Batch_ActivatorLeadAutoConvert implements Database.Batchable < sObject > {
    public Database.QueryLocator start(Database.BatchableContext leadContext) {
        //The record owner should not be activator queue
        List < Group > activatorQueue = [Select Id from Group where Type = 'Queue'
                                         AND DeveloperName = 'Activator_Queue'
                                         Limit 1
                                        ];
        String queryString = 'SELECT Type_of_registration__c,Student_Number__c,FirstName,LastName,CreatedDate,Matched_Contact__c FROM Lead WHERE RecordType.DeveloperName = \'Activator_Registration\' AND isConverted = false AND OwnerID != \'' + activatorQueue[0].ID + '\'';
     
        return Database.getQueryLocator(queryString);
    }
    public void execute(Database.BatchableContext leadContext, List < Lead > leadRecords) {
        //Helper class
        system.debug('-----leadRecords--leadContext----'+leadContext);
        system.debug('-----leadRecords--ActivatorLeads----'+leadRecords);
        Batch_ActivatorLeadAutoConvert_helper.processActivatorLeads(leadRecords);
    }
    public void finish(Database.BatchableContext leadContext) {
        //Message After completion of Batch
        System.debug('Batch_ActivatorLeadAutoConvert is Finished');
    }
}