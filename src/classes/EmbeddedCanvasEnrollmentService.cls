public without sharing class EmbeddedCanvasEnrollmentService {

    private ICanvasEnrollmentRequest canvasRequest;
    @testVisible private List<Course_Connection_Life_Cycle__c> ccLifeCycleList;
    @testVisible private List<hed__Course_Enrollment__c> updatedCourseConnList ;
    @testVisible private Integer failedCount;
    
    public EmbeddedCanvasEnrollmentService(ICanvasEnrollmentRequest canvasRequest) {
        this.canvasRequest = canvasRequest;
        this.failedCount = 0;
        updatedCourseConnList = new List<hed__Course_Enrollment__c>();
        ccLifeCycleList = new List<Course_Connection_Life_Cycle__c>();
    }
    
    public Integer getFailedCount() {
        return this.failedCount;
    }
    
    public void sendCourseConnIds(Set<Id> incomingCourseEnrolments) {
       if (incomingCourseEnrolments == null) {
            throw new QueryException('Missing course enrollment Ids. Value cannot be null.');
        }
       
        List<hed__Course_Enrollment__c> courseConnList =  Database.query('Select hed__Contact__c,Canvas_Integration_Processing_Complete__c from hed__Course_Enrollment__c where Id IN :incomingCourseEnrolments');
        Set<Id> contactIdSet = new Set<Id>();
        
        for(hed__Course_Enrollment__c course : courseConnList ){
           contactIdSet.add(course.hed__Contact__c);
        }
        List<Contact> conList = getContactsForCourseEnrollments(incomingCourseEnrolments, contactIdSet);
        system.debug('conList ::::'+conList);
                                       
        Map<String,List<hed__Course_Enrollment__c>>  contactCourseConnMap = new Map<String,List<hed__Course_Enrollment__c>>();                          
        
        List<hed__Course_Enrollment__c> enrollList = new List<hed__Course_Enrollment__c>();
        
        if (!conList.isEmpty()) {
            for(Contact c : conList){
                List<hed__Course_Enrollment__c> cousrConnList = new List<hed__Course_Enrollment__c>();
                cousrConnList.addAll(c.hed__Student_Course_Enrollments__r); 
                enrollList.addAll(c.hed__Student_Course_Enrollments__r);                
                contactCourseConnMap.put(c.Id,cousrConnList);
            } 
            system.debug('contactCourseConnMap::::'+contactCourseConnMap);
        }                               
        
        //Querying the related life cycle records
       queryCCLifeCycleRec(incomingCourseEnrolments);
       
       CourseConnCanvasRequestWrapper crsconnwrp;
       if(conList != null) {
            for(Contact con : conList){
                crsconnwrp = New CourseConnCanvasRequestWrapper();
                crsconnwrp.studentId = con.Student_ID__c;
                crsconnwrp.studentName = con.Name;
                crsconnwrp.studentEmail = con.hed__UniversityEmail__c;
                crsconnwrp.accountId = ConfigDataMapController.getCustomSettingValue('CourseConnectionCanvasAccountId');
                crsconnwrp.type = ConfigDataMapController.getCustomSettingValue('CourseConnectionCanvastype');
                crsconnwrp.enrollment_state = ConfigDataMapController.getCustomSettingValue('CourseConnectionCanvasenrolmentstate');
                crsconnwrp.notify = ConfigDataMapController.getCustomSettingValue('CourseConnectionCanvasnotify');
                crsconnwrp.enrolmentDetails.addAll(this.createCourseEnrollmentList(contactCourseConnMap.get(con.Id)));
                
                system.debug('crsconnwrp:::'+crsconnwrp);   
                 
                //failRecordcount = sendCourseDetailsToCanvas(crsconnwrp,contactCourseConnMap.get(con.Id),failCount);
                
                try{
                    canvasRequest.sendEnrollDetailToCanvas(crsconnwrp);
                    //In case of response 200 update life cycle status to Initated
                    updateLifeCycleStatusToInitiated();
                    
                }catch(CanvasRequestCalloutException ex){
                    //In case of response other than 200 update life cycle status to Error
                    updateLifeCycleStatusToError();
                    ApplicationErrorLogger.logError(ex);
                    failedCount = failedCount+1;
                }
        
            }   
          
        } 
    }
    
    @testVisible
    private List<Contact> getContactsForCourseEnrollments(Set<Id> courseEnrollmentIds, Set<Id> courseEnrollmentContactIds) {
        if (courseEnrollmentIds == null || courseEnrollmentContactIds == null) {
            throw new QueryException('Missing course enrollment or contact Ids.');
        }
        String conQuery = 'SELECT id,Name,Student_ID__c,hed__UniversityEmail__c,';
        conQuery+='(SELECT ID, hed__Course_Offering__r.Name, hed__Course_Offering__c,hed__Course_Offering__r.hed__Course__r.SIS_Course_Id__c FROM hed__Student_Course_Enrollments__r WHERE Id IN: courseEnrollmentIds) ';
        conQuery+='FROM Contact WHERE id IN :courseEnrollmentContactIds';
        return Database.query(conQuery);
    }
    
    @testVisible
    private List<CourseConnCanvasRequestWrapper.EnrolDetails> createCourseEnrollmentList(List<hed__Course_Enrollment__c> courseEnrollList){
    
        system.debug('courseEnrollList::::'+courseEnrollList);
        List<CourseConnCanvasRequestWrapper.EnrolDetails> enrollDetailList = new List<CourseConnCanvasRequestWrapper.EnrolDetails>();
        for(hed__Course_Enrollment__c crsenr : courseEnrollList) {
            CourseConnCanvasRequestWrapper.EnrolDetails enrdet = new CourseConnCanvasRequestWrapper.EnrolDetails();
            enrdet.courseConnectionId = crsenr.id;
            enrdet.courseOfferingId = crsenr.hed__Course_Offering__r.Name;
            enrdet.sisCourseId = crsenr.hed__Course_Offering__r.hed__Course__r.SIS_Course_Id__c;
            enrollDetailList.add(enrdet);
        }
        
        system.debug('enrollDetailList::::'+enrollDetailList);
        
        return enrollDetailList;
    }
    
     //Method to query related life cycle records
     @testVisible
    private void queryCCLifeCycleRec(Set<Id> incomingCourseEnrolments){
        
        if (incomingCourseEnrolments == null) {
            throw new QueryException('Missing course enrollment Ids.');
        }
        String lifeCycStage = ConfigDataMapController.getCustomSettingValue('CourseConnectionLifeCycleStageCanvas');
        
        String lifecycQuery = 'SELECT Id,Name,Status__c,Course_Connection__c FROM Course_Connection_Life_Cycle__c ';
        lifecycQuery+='WHERE Course_Connection__c in : incomingCourseEnrolments AND Stage__c =: lifeCycStage';
        system.debug('lifecycQuery:::'+lifecycQuery);
        ccLifeCycleList = Database.query(lifecycQuery);        
        
    
    }
    
    //Method to update lifecycle record to initiated,if Ipass returns 200 response code
    @testVisible
    private void updateLifeCycleStatusToInitiated(){
        for(Course_Connection_Life_Cycle__c cclyfcyc : ccLifeCycleList){
                
            cclyfcyc.Status__c = ConfigDataMapController.getCustomSettingValue('CourseConnectionLifeCycleInitiated'); 
               
        }
        
        Map<Id,CourseConnCanvasResponseWrapper.Response> canvasEnrollResByCourseConnIdMap = canvasRequest.getCanvasEnrollResByCourseConnId();
        //Based on Canvas response updating life cycle record to Success or Error
        updateLifeCycleStatToSuccessorError(canvasEnrollResByCourseConnIdMap);
         
    }
    
    //Method to update lifecycle record to error,if Ipass returns any response code other than 200
    private void updateLifeCycleStatusToError(){
        for(Course_Connection_Life_Cycle__c cclyfcyc : ccLifeCycleList){
                
            cclyfcyc.Status__c = ConfigDataMapController.getCustomSettingValue('CourseConnectionLifeCycleError'); 
               
        }
        
        updateCouseConnAndLifeCycRec();
    }
    
    //Method to update lifecycle records to success or error,based on response from canvas
    private void updateLifeCycleStatToSuccessorError(Map<Id, CourseConnCanvasResponseWrapper.Response> canvasEnrollResByCourseConnIdMap){
        
        for(Course_Connection_Life_Cycle__c cclyf : ccLifeCycleList){
                
            if(canvasEnrollResByCourseConnIdMap.get(cclyf.Course_Connection__c).result == ConfigDataMapController.getCustomSettingValue('CourseConnectionLifeCycleSuccess')){
                cclyf.Status__c = ConfigDataMapController.getCustomSettingValue('CourseConnectionLifeCycleSuccess');
                //ECB-5545: If enrollment to Canvas is successful,setting the flag Canvas_Integration_Processing_Complete__c to true : Shreya
                updatedCourseConnList.add(new hed__Course_Enrollment__c(id=cclyf.Course_Connection__c,Canvas_Integration_Processing_Complete__c=true));
            }else{
                failedCount = failedCount+1;
                cclyf.Status__c = ConfigDataMapController.getCustomSettingValue('CourseConnectionLifeCycleError');
                //ECB-5545: If enrollment to Canvas is successful,setting the flag Canvas_Integration_Processing_Complete__c to false : Shreya
                updatedCourseConnList.add(new hed__Course_Enrollment__c(id=cclyf.Course_Connection__c,Enrolment_Status__c='',Canvas_Integration_Processing_Complete__c=false));
                
                ApplicationErrorLogger.logError('Canvas Integration', String.ValueOf(cclyf.Course_Connection__c), 
                    'Status: ' + ConfigDataMapController.getCustomSettingValue('CourseConnectionLifeCycleError') 
                    + ' in EmbeddedCanvasEnrollmentService class, method updateLifeCycleStatToSuccessorError');
                
            }
               
        }
        
        updateCouseConnAndLifeCycRec();
    }
    
     //Method to update database
    private void updateCouseConnAndLifeCycRec(){
        if(!ccLifeCycleList.isEmpty()){             
            Database.update(ccLifeCycleList,false); 
        }     
        system.debug('updatedCourseConnList:::'+updatedCourseConnList);
        if(!updatedCourseConnList.isEmpty()){           
            Database.update(updatedCourseConnList,false);
        }    
                                                    
        
    }

}