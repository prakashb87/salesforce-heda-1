/*****************************************************************************************************
 *** @Class             : CourseConnStatusUpdateTest
 *** @Author            : Shreya Barua
 *** @Requirement       : Test class for apex class CourseConnStatusUpdate
 *** @Created date      : 18/07/2018
 *** @JIRA ID           : ECB-4246
 *** @LastModifiedBy    : Shreya Barua
 *** @LastModified date : 18/07/2018
 
 *****************************************************************************************************/
@isTest
public class CourseConnStatusUpdateTest {
    
    @isTest
    public static void testsetCourseConnStatus1(){
       
        TestDataFactoryUtilRefOne.testDataFactory();
        
        system.runAs(TestDataFactoryUtil.standardUser21CC){
                PermissionSet ps = [SELECT ID From PermissionSet WHERE Name = :ConfigDataMapController.getCustomSettingValue('MarketPlace Permission Set')];
                insert new PermissionSetAssignment(AssigneeId = TestDataFactoryUtil.standardUser21CC.id, PermissionSetId = ps.Id );
      
                String message1 = CourseConnStatusUpdate.setCourseConnStatus(TestDataFactoryUtil.courseEnrolment1.Id); 
                String message2 = CourseConnStatusUpdate.setCourseConnStatus(TestDataFactoryUtil.courseEnrolment2.Id); 
                
                system.assert(TestDataFactoryUtil.courseEnrolment1.Id != null, 'true');
                system.assert(TestDataFactoryUtil.courseEnrolment2.Id != null, 'true');
                system.assert(message1=='Success', 'Success');
                system.assert(message2=='Error1', 'Error');
                            
        }
        
    }
    
    @isTest
    public static void testsetCourseConnStatus2(){
        
         TestDataFactoryUtilRefOne.testDataFactory();
        
        system.runAs(TestDataFactoryUtil.testUser){
                
                String message = CourseConnStatusUpdate.setCourseConnStatus(TestDataFactoryUtil.courseEnrolment1.Id); 
                system.assert(message=='Error2', 'Error');
            
        }
        
        
    }
    
    @isTest
    public static void testsetCourseConnStatus3(){
        
         TestDataFactoryUtilRefOne.testDataFactory();
        
        system.runAs(TestDataFactoryUtil.standardUser21CC){
                
                String message = CourseConnStatusUpdate.setCourseConnStatus(TestDataFactoryUtil.courseEnrolment1.Id); 
                system.assert(message=='Error2', 'Error');
        }
        
        
    }
    
    
    
    

}