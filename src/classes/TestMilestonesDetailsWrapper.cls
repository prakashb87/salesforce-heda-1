/*******************************************
Purpose: Test class MilestonesDetailsWrapper
History:
Created by Ankit Bhagat on 31/10/2018
*******************************************/
@isTest
public class TestMilestonesDetailsWrapper {
    
    /************************************************************************************
    // Purpose      :  Creating Test data for Community User for all test methods 
    // Developer    :  Ankit
    // Created Date :  10/24/2018                 
    //***********************************************************************************/
    @testSetup static void testDataSetup() {
        
        RSTP_TestDataFactoryUtils.createNonCommunityUsersWithProfile(1,'System Administrator','Activator');
        
        User adminUser =[SELECT Id,ContactId FROM User WHERE UserName='test.nonCommunityUser1@test.com.testDev' LIMIT 1];
        System.runAs(adminUser)   
        {
            Map<String,Integer> allnumbersOfDataMap = new Map<String,Integer>();
            allnumbersOfDataMap.put('user',1);
            allnumbersOfDataMap.put('account',1);
            allnumbersOfDataMap.put('contact',1);
            
            Map<String,String> requiredInfoMap = new Map<String,String>();
            requiredInfoMap.put('userType','CommunityUsers');
            requiredInfoMap.put('profile',Label.testProfileName); 
            
            Boolean userCreatedFlag = RSTP_TestDataFactoryUtils.testRSTPDataSetup(allnumbersOfDataMap,requiredInfoMap);
            System.assert(userCreatedFlag==true,'Community User Created'); 
        }
        
    }    
    /************************************************************************************
    // Purpose      :  Test functionality for View MyEthicsList Controller
    // Developer    :  Ankit
    // Created Date :  10/24/2018                 
    //***********************************************************************************/
    
    @isTest
    public static void milestonesDetailsWrapperMethod(){

        User u =[SELECT Id,ContactId FROM User WHERE UserName='test.communityUser1@test.com.testDev' LIMIT 1]; 

        System.runAs(u) {            
            
            Ethics__c ethicObj = new Ethics__c();
            ethicObj.Ethics_Id__c='TestEthics';
            insert ethicObj ;
            List<id> ethicsId = new List<id>();
            ethicsId.add(ethicObj.id);
            system.assert(ethicsId!=null);
            
            Ethics__c ethicTest1 = new Ethics__c();
            ethicTest1.Ethics_Id__c='TestEthics1';
            ethicTest1.Application_Title__c = 'Test';
            ethicTest1.Application_Type__c = 'HUMAN = Human Ethics';
            ethicTest1.Status__c = 'Amended';
            ethicTest1.Approved_Date__c = date.today();
            ethicTest1.Expiry_Date__c   = date.today().addDays(10);
            insert ethicTest1;
            
            List<Researcher_Member_Sharing__c> memberResearchList = new List<Researcher_Member_Sharing__c>();
            Researcher_Member_Sharing__c memberResearch = new Researcher_Member_Sharing__c();
            memberResearch.Ethics__c = ethicTest1.id;
            memberResearch.User__c   = u.id;
            memberResearchList.add(memberResearch);
            
            Researcher_Member_Sharing__c memberResearch1 = new Researcher_Member_Sharing__c();
            memberResearch1.Ethics__c = ethicTest1.id;
            memberResearch1.User__c   = u.id;
            memberResearchList.add(memberResearch1);
            insert memberResearchList;
                        
            ResearcherPortalProject__c researchProject = new ResearcherPortalProject__c();
            researchProject.Title__c = 'Test';
            insert researchProject;
            System.Assert(researchProject.Title__c == 'Test');
            
            Publication__c publication = new Publication__c();
            publication.Publication_Title__c = 'Test';
            publication.Publication_Id__c = 123;
            insert publication;
            System.Assert(publication.Publication_Title__c == 'Test');          
            
            List<SignificantEvent__c> milestonesList = new List<SignificantEvent__c>();
           
                SignificantEvent__c milestoneEthics = new SignificantEvent__c();
                milestoneEthics.DueDate__c = date.today().addDays(15);
                milestoneEthics.IsAction__c = true;
                milestoneEthics.Ethics__c = ethicTest1.id;
                milestoneEthics.EventDescription__c = 'Test';
                milestonesList.add(milestoneEthics);  
                
                SignificantEvent__c milestoneProject = new SignificantEvent__c();
                milestoneProject.DueDate__c = date.today().addDays(45);
                milestoneProject.IsAction__c = true;
                milestoneProject.ResearchProject__c = researchProject.id;
                milestoneProject.EventDescription__c = 'Test';
                milestoneProject.ActualCompletionDate__c  = date.today().addDays(10);
                milestonesList.add(milestoneProject);  
                
                SignificantEvent__c milestonePublication = new SignificantEvent__c();
                milestonePublication.DueDate__c = date.today().addDays(45);
                milestonePublication.IsAction__c = true;
                milestonePublication.Publication__c = publication.id;
                milestonePublication.EventDescription__c = 'Test';
                milestonePublication.ActualCompletionDate__c  = date.today().addDays(10);
                milestonesList.add(milestonePublication);          
                
            insert milestonesList;
            System.Assert(milestonesList.size()== 3);
            
            ViewMyMilestonesHelper.getMilestonesWrapperList();
            
            
            MilestonesDetailsWrapper.milestonesWrapper  milestonesWrapper = new MilestonesDetailsWrapper.milestonesWrapper  ();
            milestonesWrapper.dueDate  = date.today().addDays(45);
            milestonesWrapper.description   ='abc';
            milestonesWrapper.details       ='Amended';  
            milestonesWrapper.dateCompleted = date.today().addDays(40);
            milestonesWrapper.IsDueWithin30Days    = true;
            milestonesWrapper.IsOverDue =  true;
            
            
            MilestonesDetailsWrapper.milestonesWrapper  milestonesWrapper1 = new MilestonesDetailsWrapper.milestonesWrapper();
            MilestonesDetailsWrapper.milestonesWrapper  milestonesWrapper2 = new MilestonesDetailsWrapper.milestonesWrapper(milestonesList[0]);

        }
    }
         
    /************************************************************************************
    // Purpose      :  Test functionality for View MyEthicsList Controller
    // Developer    :  Ankit
    // Created Date :  12/05/2018                 
    //***********************************************************************************/
    
    @isTest
    public static void milestonesDetailsWrapperNegativeMethod(){

        User u =[SELECT Id,ContactId FROM User WHERE UserName='test.communityUser1@test.com.testDev' LIMIT 1]; 

        System.runAs(u) {            
            Boolean result = false;
                          
            Researcher_Member_Sharing__c memberResearch8 = new Researcher_Member_Sharing__c();
             
            try
            {    memberResearch8.User__c = u.id;
                insert memberResearch8 ;    
            }
            catch(DmlException ex)
            {
                result =true;                   
               
            }
            system.assert(memberResearch8.User__c == u.id);
            
            Ethics__c ethicTest1 = new Ethics__c();
            ethicTest1.Ethics_Id__c='TestEthics1';
            ethicTest1.Application_Title__c = 'Test';
            ethicTest1.Application_Type__c = 'HUMAN = Human Ethics';
            ethicTest1.Status__c = 'Amended';
            ethicTest1.Approved_Date__c = date.today();
            ethicTest1.Expiry_Date__c   = date.today().addDays(10);
            insert ethicTest1;
            
            List<Researcher_Member_Sharing__c> memberResearchList = new List<Researcher_Member_Sharing__c>();
            Researcher_Member_Sharing__c memberResearch = new Researcher_Member_Sharing__c();
            memberResearch.Ethics__c = ethicTest1.id;
            memberResearch.User__c   = u.id;
            memberResearchList.add(memberResearch);
            
            Researcher_Member_Sharing__c memberResearch1 = new Researcher_Member_Sharing__c();
            memberResearch1.Ethics__c = ethicTest1.id;
            memberResearch1.User__c   = u.id;
            memberResearchList.add(memberResearch1);
            insert memberResearchList;
                        
            List<SignificantEvent__c> milestonesList = new List<SignificantEvent__c>();
            
            SignificantEvent__c milestoneEthics = new SignificantEvent__c();
            milestoneEthics.DueDate__c = date.today().addDays(15);
            milestoneEthics.IsAction__c = true;
            milestoneEthics.Ethics__c = ethicTest1.id;
            milestoneEthics.EventDescription__c = 'Test';
            milestonesList.add(milestoneEthics);  
            
            insert milestonesList;
            System.Assert(milestonesList.size()==1);
           
            MilestonesDetailsWrapper.milestonesWrapper  milestonesWrapper1 = new MilestonesDetailsWrapper.milestonesWrapper();
            MilestonesDetailsWrapper.milestonesWrapper  milestonesWrapper2 = new MilestonesDetailsWrapper.milestonesWrapper(milestonesList[0]);

        }
    }

}