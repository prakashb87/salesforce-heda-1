/**
 * @description This interface defines the server side methods required
 * for implementing a Captcha
 * @group Captcha
 */
// Global state is required as the page referencing this class
// will be inside an iframe
@SuppressWarnings('PMD.AvoidGlobalModifier')
global interface CaptchaVerificationRequest {

    /**
     * @description Verifies with the captcha provider if the user has 
     * successfully completed the Captcha
     * @param response Response from the Captcha page as a result of the user
     * completing the captcha on the front-end
     * @return Returns true if the user has completed the Captcha successfully
     */
    boolean isCaptchaVerified(String response);
}