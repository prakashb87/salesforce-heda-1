/**
 * @description Use this class to make requests to IDAM (Passport) to get information 
 * about existing users in IDAM
 * @group Identity Management
 */
public class PassportIdentityProviderUserStatusReq implements IdentityProviderUserStatusReq {

    @testVisible private boolean isCalloutLogged;
    private final String EMAIL;
    @testVisible private UserCheckPayload userStatusResponse;


    /**
     * @description 
     * @param email Email address used to check if user exists
     */
    public PassportIdentityProviderUserStatusReq(String email) {
        if (String.isNotBlank(email)) {
            this.EMAIL = email;
        } else {
            throw new ArgumentException('Email has not been set. Email is required.');
        }
        isCalloutLogged = true;  // The callout will log to the Integration log by default
    }


    /**
     * @description This method determines whether the callout should be logged. By default, 
     * all callouts from this class will be logged to the Integration Log. However, when 
     * it is required that logging should not happen, this method allows for logging to be 
     * turned off (e.g. in the case of multiple callouts such that dml operations cannot be 
     * executed before all callouts have been completed)
     * @param isCalloutLogged Set this to false if the callout should not be logged
     */
    public void setIntegrationLogging(boolean isCalloutLogged) {
        this.isCalloutLogged = isCalloutLogged;
    }


    /**
     * @description This method checks if the user exists in IDAM. It will make a callout
     * to IDAM if a callout hasn't been made
     * @return Returns if the user exists in Passport IDAM
     */
    public boolean isExistingUser() {
        if (userStatusResponse == null) {
            this.getUserStatus();
        }
        return userStatusResponse.exists;
    }

    /**
     * @description This method checks if the user exists and is verified in IDAM. It will 
     * make a callout to IDAM if a callout hasn't been made
     * @return Returns if a user exists and is also verified in Passport IDAM
     */
    public boolean isExistingVerifiedUser() {
        if (userStatusResponse == null) {
            this.getUserStatus();
        } 
        if (userStatusResponse.exists) {
            return userStatusResponse.isVerified;
        } else {
            return false;
        }
    }


    private void getUserStatus() {
        Http http = new Http();
        HTTPRequest req = new HTTPRequest();
        HTTPResponse response = new HTTPResponse();
        String endpoint = ConfigDataMapController.getCustomSettingValue('IDAMIpaaSEndPoint') + '?email=' + this.EMAIL.trim();

        req.setHeader('client_id', ConfigDataMapController.getCustomSettingValue('IDAMIpaaSClientID'));
        req.setHeader('client_secret', ConfigDataMapController.getCustomSettingValue('IDAMIpaaSClientSecret'));
        req.setHeader('Content-Type', 'application/json');
        req.setEndpoint(endpoint);
        req.setMethod('GET');
        response = http.send(req);

        System.debug( 'Email Check API Request=' + endpoint);
        System.debug('Email Check API Response=' + response);
        if (this.isCalloutLogged) {
            logIntegrationLog(endpoint,response);
        }

        if (response.getStatusCode() != 200) {
            throw new HttpCalloutException('Request was not successful. Http Status Code: ' + response.getStatusCode());
        }

        UserCheckResponse userCheckResponse = (UserCheckResponse) System.JSON.deserialize(response.getBody(), UserCheckResponse.class);
        if (userCheckResponse.payload != null) {
            this.userStatusResponse = userCheckResponse.payload;
        } else {
            throw new HttpCalloutException('Unexpected response, unable to process payload.');
        }
    }

    private static void logIntegrationLog( String endpoint, HTTPResponse response ) {
        IntegrationLog requestLog = new IntegrationLog();
        requestLog.setRequestBody(endpoint);
        requestLog.setType('IDAM_IPaaS_EmailCheck');
        requestLog.setServiceType('Outbound Service');
        requestLog.log();

        IntegrationLog responseLog = new IntegrationLog();
        responseLog.setRequestBody(response.getBody());
        responseLog.setType('IDAM_IPaaS_EmailCheck');
        responseLog.setServiceType('Acknowledgement');
        responseLog.log();
    }

    private class UserCheckResponse {
        public String id;
        public String result;
        public String code;
        public String application;
        public String provider;
        public UserCheckPayload payload;       
    }

    private class UserCheckPayload {
		public Boolean success;
		public String message;
		public Boolean exists;
		public Boolean isVerified;
	}
}