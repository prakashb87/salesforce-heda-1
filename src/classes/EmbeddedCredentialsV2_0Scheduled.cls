/*****************************************************************
Name: EmbeddedCredentialsV2_0Scheduled 
Author: Capgemini [Parveen Kaushik]
Jira Reference : ECB-5530 (Master), ECB-5794, ECB-5795, ECB-5800, ECB-5801

Purpose: This class provides the scheduling ability for the newly redesigned EmbeddedCredentialsLaunchBatchV2_0 class

History
--------
Version   Author            Date              Detail
1.0       Parveen           07/06/2019        Initial Version
*/

public without sharing class EmbeddedCredentialsV2_0Scheduled implements Schedulable {
    
    public void execute(SchedulableContext ctx) {
        EmbeddedCredentialsLaunchBatchV2_0 bc = new EmbeddedCredentialsLaunchBatchV2_0();  
        Database.executeBatch(bc, 10);
    }
 
}