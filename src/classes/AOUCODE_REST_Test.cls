/*********************************************************************************
*** @TestClassName     : AOUCODE_REST_Test
*** @Author            : Shubham Singh
*** @Requirement       : To Test the AOUCODE_REST web service apex class.
*** @Created date      : 17/09/2018
**********************************************************************************/
@isTest
public class AOUCODE_REST_Test {
    static testMethod void myUnitTest() {
        
        RestRequest req = new RestRequest();
        
        RestResponse res = new RestResponse();
        // adding header to the request
        req.addHeader('httpMethod', 'POST');
        
        req.requestUri = '/services/apexrest/HEDA/v1/AOUCODE';
        
        //creating test data
        String jsonMsg = '{"code": "110","effectiveDate": "2009-01-01","effectiveStatus": "I","description": "Chemical & Metallurgical HE","shortDescription": "ChemMet HE"}';
        
        req.requestBody = Blob.valueof(jsonMsg);
        
        RestContext.request = req;
        
        RestContext.response = res;
        
        Test.startTest();
        //store response
        Map < String, String > check = AOUCODE_REST.upsertAOUCODE();
        List < String > str = check.values();
        //assert condition 
        System.assertEquals(True, str[0] == 'ok');
        
        Test.stopTest();
        
    }
    static testMethod void coverError() {
        Account acc = new Account();
        acc.Name = 'RMITU';
        insert acc;
        
        RestRequest req = new RestRequest();
        
        RestResponse res = new RestResponse();
        // adding header to the request
        req.addHeader('httpMethod', 'POST');
        
        req.requestUri = '/services/apexrest/HEDA/v1/AOUCODE';
        
        //creating test data
        String jsonMsg = '{"code": "","effectiveDate": "2009-01-01","effectiveStatus": "I","description": "Chemical & Metallurgical HE","shortDescription": "ChemMet HE"}';
        
        req.requestBody = Blob.valueof(jsonMsg);
        
        RestContext.request = req;
        
        RestContext.response = res;
        
        Test.startTest();
        //store response
        Map < String, String > check = AOUCODE_REST.upsertAOUCODE();
        List < String > str = check.values();
        //assert condition 
        System.assertEquals(False, str[0] == 'ok');
        
        Test.stopTest();
        
    }
    static testMethod void coverTryCatch() {
        Map < String, String > check;
        Test.startTest();
        try {
            //store response
            check = AOUCODE_REST.upsertAOUCODE();
        } catch (DMLException e) {
            List < String > str = check.values();
            //assert condition 
            System.assertEquals(False, str[0] == 'ok');
        }
        Test.stopTest();
    }
    static testMethod void updateMethod() {
		
		AOU_Code__c aou = new AOU_Code__c();
		aou.Name = '110';
		aou.Effective_date__c =	Date.ValueOf('2009-01-01');
        aou.Effective_status__c = 'A';
		
		insert aou;
        
        RestRequest req = new RestRequest();
        
        RestResponse res = new RestResponse();
        // adding header to the request
        req.addHeader('httpMethod', 'POST');
        
        req.requestUri = '/services/apexrest/HEDA/v1/AOUCODE';
        
        //creating test data
        String jsonMsg = '{"code": "110","effectiveDate": "2009-01-01","effectiveStatus": "I","description": "Chemical & Metallurgical HE","shortDescription": "ChemMet HE"}';
        
        req.requestBody = Blob.valueof(jsonMsg);
        
        RestContext.request = req;
        
        RestContext.response = res;
        
        Test.startTest();
        //store response
        Map < String, String > check = AOUCODE_REST.upsertAOUCODE();
        List < String > str = check.values();
        //assert condition 
        System.assertEquals(True, str[0] == 'ok');
        
        Test.stopTest();
        
    }
}