/*********************************************************************************
*** @Class         : programEnrollmentService
*** @Author		: BJIRA Squad
*** @Requirement   : 
*** @Created date  :
**********************************************************************************/
public with sharing class ProgramEnrollmentService {
    private static set<string> programStatus = new Set<String> {'AD','AC'};
        public Map < Id, hed__Program_Enrollment__c > conIdAndProgEnrAff = new Map < Id, hed__Program_Enrollment__c > ();
    public static Id prospectAffRecordTypeId = Schema.SObjectType.hed__Affiliation__c.getRecordTypeInfosByName().get('Prospect').getRecordTypeId();
    public List < hed__Affiliation__c > affiliationListOnInsert = new List < hed__Affiliation__c > ();
    public Map < Id, hed__Affiliation__c > affIdAndAffiliation = new Map < Id, hed__Affiliation__c > ();
    public  static Opportunity createApplicantforInquiryOpp(Opportunity oppRec, hed__Program_Enrollment__c programEnroll, Id recordTypeId) {
        Opportunity opp = new Opportunity();
        opp.RecordTypeId = recordTypeId;
        opp.StageName = 'Enrolled';
        opp.OwnerId=  oppRec.OwnerId;
        opp.Affiliation__c = programEnroll.hed__Affiliation__c;
        opp.Admit_Term__c = programEnroll.Admit_Term__c;
        opp.Segment__c = oppRec.Segment__c;
        opp.CloseDate = oppRec.CloseDate;
        opp.Name = oppRec.Assign_To_Contact__r.Name + ' - ' + programEnroll.Admit_Term__c;
        opp.Assign_To_Contact__c = programEnroll.hed__Contact__c;
        opp.Inquiry_Date__c = oppRec.Inquiry_Date__c;
        opp.Level_of_Study__c = oppRec.Level_of_Study__c;
        opp.Opportunity_Key__c = programEnroll.hed__Contact__c + '.' + oppRec.Assign_To_Contact__r.AccountId  + '.' + recordTypeId +'.' + programEnroll.Admit_Term__c;
        return opp;
    }
    public void updateExistingContactAffiliationOnInsert(List < hed__Program_Enrollment__c > programEnrollments) {
        
        if(programEnrollments!=null){
            for (hed__Program_Enrollment__c progEnroll: programEnrollments) {                
                conIdAndProgEnrAff.put(progEnroll.hed__Contact__c, progEnroll);                
            }
            affIdAndAffiliation = new  Map < Id, hed__Affiliation__c > ([SELECT Id,
                                                                                 RecordTypeId,
                                                                                 RecordType.DeveloperName,
                                                                                 hed__Contact__c,
                                                                                 hed__Account__c,
                                                                                 hed__Status__c,
                                                                                 hed__Role__c
                                                                                 FROM hed__Affiliation__c
                                                                                 Where hed__Contact__c IN: conIdAndProgEnrAff.keySet() 
                                                                                 And hed__Status__c!= 'Former'
                                                                                 And RecordTypeId =: prospectAffRecordTypeId
                                                                                ]);  
            system.debug('Available Affiliations are :'+json.serializePretty(affIdAndAffiliation.values()));
        }
        if(affIdAndAffiliation.size()>0){
            for(hed__Affiliation__c affRec : affIdAndAffiliation.values()){
                affRec.hed__Status__c = 'Former';
                affiliationListOnInsert.add(affRec);
            }
            methodtoInsert(affiliationListOnInsert);            
        }
    }
    //Applicant opp Return true if Program Status is equal to AD or AC
    public  static boolean checkProgramStatus(hed__Program_Enrollment__c programEnroll, Opportunity oppRec) {
        return programStatus.contains(programEnroll.Program_Status__c) && (programEnroll.Admit_Term__c != oppRec.Admit_Term__c) && oppRec.StageName == 'Enrolled';
    }
    
    //Inquiry opp Return true if Program Status is equal to AD or AC
    public  static integer checkInquiryOppStatus(hed__Program_Enrollment__c programEnroll, String oppAdmitTerm) {
        integer value;
        value = programEnroll.Admit_Term__c == oppAdmitTerm ? 1 : 3;
        if (!(programStatus.contains(programEnroll.Program_Status__c))) {
            value = programEnroll.Admit_Term__c == oppAdmitTerm ? 2 : 4;
        }
        return value;
    }
    public static void methodtoInsert(List < hed__Affiliation__c >  affiliationList) {
        System.debug('affiliates---->' + JSON.serializePretty(affiliationList));
        List<hed__Affiliation__c> affToUpdate = new List<hed__Affiliation__c>(new Set<hed__Affiliation__c>(affiliationList));
        if(affToUpdate.size()>0){
            Database.Update(affToUpdate,false);
        }       
    }
    public static void methodtoUpdate(List < Opportunity > opportunityToUpsert) {
       
        System.debug('opportunate---->' + opportunityToUpsert);
        if(opportunityToUpsert.size()>0){
            Database.Upsert(opportunityToUpsert,false);
        }
    }
}