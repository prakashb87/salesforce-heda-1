/*******************************************
Purpose: To create different objects in the Utility Class
History:
Created by Ankit Bhagat on 05/09/2018
*******************************************/

public with sharing class SobjectUtils {
    
   
    /************************************************************************************
    // Purpose      :  generic picklist option utility
    // Parameters   :  sObject, string	   	
    // Developer    :  Ankit Bhagat
    // Created Date :  05/09/2018             
    //***********************************************************************************/
    public static List < String > getselectOptions(sObject objObject, string fld) {
        
        System.debug('@@@objObject'+objObject);
        System.debug('@@@fld'+fld);
        
        List <String> allOpts = new list <String> ();
        Schema.sObjectType objType = objObject.getSObjectType();
        
        Schema.DescribeSObjectResult objDescribe = objType.getDescribe();
        map < String, Schema.SObjectField > fieldMap = objDescribe.fields.getMap();
        
        // Get the list of picklist values for this field.
        list < Schema.PicklistEntry > values = 
          fieldMap.get(fld).getDescribe().getPickListValues();
        
        // Add these values to the selectoption list.
        for (Schema.PicklistEntry a: values) {
            allOpts.add(a.getValue());
        }
        system.debug('allOpts ---->' + allOpts);
        allOpts.sort();
        return allOpts;
    }
    
    /************************************************************************************
    // Purpose      : Fetching current user information 
    // Parameters   : Void 		   	
    // Developer    : Ankit Bhagat
    // Created Date : 05/09/2018             
    //***********************************************************************************/
    
    public static user fetchUser(){
    	
      //handling Validate CRUD permission before SOQL/DML operation in PMD code review	:: Added by subhajit
      // query current user information : Starts 
       User oUser = new User();
       if (!Schema.sObjectType.User.fields.Name.isAccessible()){
          return oUser;
        }
      // query current user information : Ends
        
       oUser = [SELECT id,Name,Username,Alias,ContactId,SmallPhotoURL,IsPortalEnabled FROM User WHERE id =: userInfo.getUserId() LIMIT 1];
       return oUser;
    }
    
    /************************************************************************************
    // Jira         : RPORW-496/474
    // SPIRINT      : 7
    // Purpose      : Checking profile Access for Community User in add member
    // Parameters   : Id 		   	
    // Developer    : Subhajit
    // Created Date : 11/13/2018             
    //***********************************************************************************/  
    public static Boolean getProfileAccessCheckInfo(Id profileId)
    {
        Boolean isAccessFlag =false;
        CustomPermissionToProfile__c cPTP = CustomPermissionToProfile__c.getInstance(profileId);
        isAccessFlag= cPTP.isAccessible__c;
        system.debug('@@@@@ profileId==>'+profileId);
        system.debug('@@@@@ isAccessFlag==>'+isAccessFlag);
        return isAccessFlag;
    }
    
    /************************************************************************************
    // JIRA         : 245/223
    // SPRINT       : 7
    // Purpose      : checking Organisation is Sandbox
    // Parameters   : Boolean  	   	
    // Developer    : Subhajit  
    // Created Date : 11/14/2018                  
    //***********************************************************************************/ 
    public static Boolean checkOrgType()
    {
    	List<Organization> lstOrganization;
        Boolean isSandbox =false;
        if (Organization.sObjectType.getDescribe().IsAccessible()){ //Added for PMD Fix
        	lstOrganization = [Select isSandbox from Organization LIMIT 1];
        } else {
        	lstOrganization = [Select isSandbox from Organization LIMIT 1];
        }
 
        if(lstOrganization!= null && lstOrganization.size()>0) 
        {
            isSandbox= lstOrganization[0].isSandbox;
        }
        return isSandbox;
    }
   
}