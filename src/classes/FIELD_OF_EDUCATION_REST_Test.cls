/*********************************************************************************
*** @TestClassName     : FIELD_OF_EDUCATION_REST_Test
*** @Author            : Dinesh Kumar
*** @Requirement       : To Test the FIELD_OF_EDUCATION_REST web service apex class.
*** @Created date      : 14/08/2018
**********************************************************************************/

@isTest
public class FIELD_OF_EDUCATION_REST_Test {
    static testMethod void myUnitTest() {
        RestRequest req = new RestRequest();
        
        RestResponse res = new RestResponse();
        // adding header to the request
        req.addHeader('httpMethod', 'POST');
        
        req.requestUri = '/services/apexrest/HEDA/v1/FieldOfEducation';
        
        //creating test data
        String JsonMsg = '{"code": "55","effectiveDate": "2007-01-01","effectiveStatus": "A","description": "Skills Store RPL Assessment -","shortDescription": "Skills Store RPL Assessment - Fee for Service"}';
        
        req.requestBody = Blob.valueof(JsonMsg);
        
        RestContext.request = req;
        
        RestContext.response = res;
        
        Test.startTest();
        //store response
        Map < String, String > check = FIELD_OF_EDUCATION_REST.upsertFieldOfEducation();
        List < String > str = check.values();
        //assert condition 
        System.assertEquals(True, str[0] == 'ok');
        
        Test.stopTest();
        
    }
    static testMethod void coverError() {
        RestRequest req = new RestRequest();
        
        RestResponse res = new RestResponse();
        // adding header to the request
        req.addHeader('httpMethod', 'POST');
        
        req.requestUri = '/services/apexrest/HEDA/v1/FieldOfEducation';
        
        //creating test data
        String JsonMsg = '{"code": "","effectiveDate": "2007-01-01","effectiveStatus": "A","description": "Skills Store RPL Assessment -","shortDescription": "Skills Store RPL Assessment - Fee for Service"}';
        
        req.requestBody = Blob.valueof(JsonMsg);
        
        RestContext.request = req;
        
        RestContext.response = res;
        
        Test.startTest();
        //store response
        Map < String, String > check = FIELD_OF_EDUCATION_REST.upsertFieldOfEducation();
        List < String > str = check.values();
        //assert condition 
        System.assertEquals(False, str[0] == 'ok');
        
        Test.stopTest();
        
    }
    static testMethod void coverTryCatch() {
        Map < String, String > check;
        Test.startTest();
        try {
            //store response
            check = FIELD_OF_EDUCATION_REST.upsertFieldOfEducation();
        } catch (DMLException e) {
            List < String > str = check.values();
            //assert condition 
            System.assertEquals(False, str[0] == 'ok');
        }
        Test.stopTest();
    }
    static testMethod void updateMethod() {
        
        Field_of_education__c fEdu = new Field_of_education__c();
        
        fEdu.Name = '55';
        fEdu.Effective_Date__c = Date.ValueOf('2008-01-01');
        insert fEdu;
        
        RestRequest req = new RestRequest();
        
        RestResponse res = new RestResponse();
        // adding header to the request
        req.addHeader('httpMethod', 'POST');
        
        req.requestUri = '/services/apexrest/HEDA/v1/FieldOfEducation';
        
        //creating test data
        String JsonMsg = '{"code": "55","effectiveDate": "2007-01-01","effectiveStatus": "A","description": "Skills Store RPL Assessment -","shortDescription": "Skills Store RPL Assessment - Fee for Service"}';
        
        req.requestBody = Blob.valueof(JsonMsg);
        
        RestContext.request = req;
        
        RestContext.response = res;
        
        Test.startTest();
        //store response
        Map < String, String > check = FIELD_OF_EDUCATION_REST.upsertFieldOfEducation();
        List < String > str = check.values();
        //assert condition 
        System.assertEquals(True, str[0] == 'ok');
        
        Test.stopTest();
        
    }
}