/*********************************************************************************
 *** @ClassName         : AOUCODE_REST
 *** @Author            : Shubham Singh 
 *** @Requirement       : To insert/update the AOU_Code__c object record
 *** @Created date      : 17/08/2018
 *** @Modified by       : Shubham Singh 
 *** @modified date     : 17/09/2018
 **********************************************************************************/
@RestResource(urlMapping = '/HEDA/v1/AOUCODE')
global with sharing class AOUCODE_REST {
    public String code;
    public String effectiveDate;
    public String effectiveStatus;
    public String description;
    public String shortDescription;

    @HttpPost
    global static Map < String, String > upsertAOUCODE() {
        //to create map of effective date        
        map < String, Date > updateEffectiveDate = new map < String, Date > ();
        //final response to send
        Map < String, String > responsetoSend = new Map < String, String > ();
        //old AOU record
        List < AOU_Code__c > previousAOURecord = new List < AOU_Code__c > ();
        //AOU code List
        List < String > addAouCode = new List < String > ();
        //effective date List
        List < Date > addEffectiveDate = new List < Date > ();
        // record to insert
//        AOU_Code__c aou = new AOU_Code__c();
        //key to match record
        String key;
        
        try {

            //request and response variable
            RestRequest request = RestContext.request;

            RestResponse response = RestContext.response;

            //no need to add [] in JSON
            String reqStr = '[' + request.requestBody.toString() + ']';

            response.addHeader('Content-Type', 'application/json');

            //getting the data from JSON and Deserialize it
            List < AOUCODE_REST > postString = (List < AOUCODE_REST > ) System.JSON.deserialize(reqStr, List < AOUCODE_REST > .class);
            system.debug('postString-->' + postString );

			previousAOURecord=getPreviousAOURecords(postString,addAouCode, updateEffectiveDate);
			if (previousAOURecord==null){
				responsetoSend = throwError();
                return responsetoSend;
			}
        
            //map to update the old Location
            map < String, ID > oldAOUUpdate = new map < String, ID > ();
            map < String, AOU_Code__c > stringAOUCodeMap = new map < String, AOU_Code__c > ();
            for (AOU_Code__c aouRecord: previousAOURecord) {
                if (aouRecord.Effective_date__c != null) {
                    String uniqueKey = aouRecord.Name + '';
                    if (updateEffectiveDate != null && updateEffectiveDate.get(uniqueKey) != null &&
                        updateEffectiveDate.get(uniqueKey) <= System.today())
                    {
                        oldAOUUpdate.put(aouRecord.Name, aouRecord.ID);                        
                        stringAOUCodeMap.put(aouRecord.Name,aouRecord);
                    }
                    else if(aouRecord.Effective_date__c > updateEffectiveDate.get(uniqueKey)){
                        responsetoSend.put('message', 'OK');
                        responsetoSend.put('status', '200');
                        return responsetoSend;
                    }else{
                        responsetoSend.put('message', 'OK');
                        responsetoSend.put('status', '200');
                        return responsetoSend;
                    }
                }
            }


			upsertAOUCodes(postString, stringAOUCodeMap, oldAOUUpdate);      

            responsetoSend.put('message', 'OK');
            responsetoSend.put('status', '200');
            return responsetoSend;
       
        } catch (Exception excpt) {
            //If exception occurs then return this message to IPASS
            responsetoSend = throwError();
            return responsetoSend;
        }
    }
    public static Map < String, String > throwError() {
        //to store the error response
        Map < String, String > errorResponse = new Map < String, String > ();
        errorResponse.put('message', 'Oops! AOU was not well defined, amigo.');
        errorResponse.put('status', '404');
        //If error occurs then return this message to IPASS
        return errorResponse;
    }
    
    private static List < AOU_Code__c > getPreviousAOURecords(List <AOUCODE_REST> postString, List<String> addAouCode, Map<String, Date> updateEffectiveDate){

	        if (postString != null) {
                for (AOUCODE_REST aouCodeRest: postString) {
                    if (aouCodeRest.code != null && (aouCodeRest.code).length() > 0) {
                        addAouCode.add(aouCodeRest.code);
                        //updateEffectiveDate.put(aouCodeRest.code + '' + aouCodeRest.effectiveDate, Date.ValueOf(aouCodeRest.effectiveDate));
                    } else {
						return null; //To indicate caller to return error
                    }
                    
                    if(aouCodeRest.effectiveDate != null && (aouCodeRest.effectiveDate).length() > 0){
	                        updateEffectiveDate.put(aouCodeRest.code,Date.ValueOf(aouCodeRest.effectiveDate));
	                }
                }
            }
            
       		return queryPreviousAOURecords(addAouCode);

    }
    
    private static List < AOU_Code__c > queryPreviousAOURecords(List<String> addAouCode){
    		List < AOU_Code__c > previousAOURecord = new List < AOU_Code__c > ();
            if (addAouCode.size() > 0 && addAouCode != null) {
            	if (Schema.sObjectType.AOU_Code__c.isAccessible()){
                	previousAOURecord = [select id,Name,Effective_date__c,Effective_status__c,Description__c,Short_Description__c from AOU_Code__c where Name =: addAouCode limit 1];
            	}
            }
            return previousAOURecord;
    }

	private static void upsertAOUCodes(List <AOUCODE_REST> postString, map < String, AOU_Code__c > stringAOUCodeMap, map < String, ID > oldAOUUpdate){
	        //insert/update AOU Code
	        List < AOU_Code__c > upsertAOUCode = new List < AOU_Code__c > ();

			AOU_Code__c aou = new AOU_Code__c();
            //insert or update the aouRecordount
            for (AOUCODE_REST aouCode : postString) {
                aou = new AOU_Code__c();
                String key = aouCode.code + '' + aouCode.effectiveDate;
                aou.Name = aouCode.code;
                aou.Effective_date__c = Date.valueOf(aouCode.effectiveDate);
                aou.Effective_status__c = aouCode.effectiveStatus == 'A' ? 'Active': 'Inactive';
                aou.Description__c = aouCode.description;
                aou.Short_Description__c = aouCode.shortDescription;
                if (stringAOUCodeMap.size() > 0 && stringAOUCodeMap.get(aouCode.code) != null){
                    if (stringAOUCodeMap.get(aouCode.code).Effective_Date__c <= aou.Effective_date__c ){
                        aou.ID = oldAOUUpdate.get(aouCode.code);
                    }else{
                        aou =null;
                	}
             	}
            // getting record ID to update the AOU code
            
	            if(aou !=null && aou.Effective_date__c <= System.today()){
	                upsertAOUCode.add(aou);
	            }
            }

			upsertAOUCodes(upsertAOUCode, aou);
	}
	
	private static void upsertAOUCodes(List < AOU_Code__c > upsertAOUCode, AOU_Code__c aou){
            if (upsertAOUCode != null && upsertAOUCode.size() > 0) {
            	if (Schema.sObjectType.AOU_Code__c.isUpdateable()
            		&& Schema.sObjectType.AOU_Code__c.isCreateable()){            
                	upsert upsertAOUCode Name;
            	}
            }
	}
	
}