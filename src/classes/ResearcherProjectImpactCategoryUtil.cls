/*@Author     : Gourav Bhardwaj
@description  : Util class holds generic methods for Impact Category Related Functionality
@Story        : RPROW-981
@CreatedDate  : 18/4/2019
*/
public class ResearcherProjectImpactCategoryUtil {
	public static ResearcherPublicProjectsHelperUtil.FilteredPublicProjectRecordWrapper isImpactCategoryPresentInProject(ResearcherPortalProject__c project,ResearcherPublicProjectsHelperUtil.RMITPublicProjectWrapper rmitPublicProjectWrapper){
		//1383
      	ResearcherPublicProjectsHelperUtil.FilteredPublicProjectRecordWrapper filteredRecordWrapper = new ResearcherPublicProjectsHelperUtil.FilteredPublicProjectRecordWrapper();
      	Boolean isSearchTextPresent = false;
      	
      	system.debug('## impactCodeDetailSearch : '+rmitPublicProjectWrapper.configuration.impactCodeDetailSearch);
      	
      	//This check is necessary for AND condition where filter should include Text Search AND FOR AND Impact Category
      	if(rmitPublicProjectWrapper.configuration.impactCodeDetailSearch.isEmpty()){
      		filteredRecordWrapper.isSearchTextPresent = true;
			filteredRecordWrapper.searchOrder         = 0;
			return filteredRecordWrapper;
      	}
		
		if(!rmitPublicProjectWrapper.configuration.impactCodeDetailSearch.isEmpty() && !String.isBlank(project.Impact_Category__c)){
			isSearchTextPresent = checkImpactCategoryInProject(project.Impact_Category__c.split(';'),rmitPublicProjectWrapper);
		}

		//1383
		filteredRecordWrapper.isSearchTextPresent = isSearchTextPresent;
		filteredRecordWrapper.searchOrder         = 5;
		
		return filteredRecordWrapper;
	}

	private static Boolean checkImpactCategoryInProject(List<String> impactCategories,ResearcherPublicProjectsHelperUtil.RMITPublicProjectWrapper rmitPublicProjectWrapper){
		
		Boolean isSearchTextPresent = false;
		for(String impactCategory : impactCategories){

			system.debug('## searched impact category : '+rmitPublicProjectWrapper.configuration.impactCodeDetailSearch);

			system.debug('## impactCategory : '+impactCategory);

			if(rmitPublicProjectWrapper.configuration.impactCodeDetailSearch.contains(impactCategory.toLowerCase())){
				isSearchTextPresent = true;
				break;
			}
		}//for ends

		system.debug('isSearchTextPresent : '+isSearchTextPresent);

		return isSearchTextPresent;
	}
}