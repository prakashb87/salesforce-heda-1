/********************************************************************
// Purpose              : Student User creation in Salesforce
// Author               : Capgemini [Rishabh]
// Parameters           : List<Contact>
//  Returns             : null
//JIRA Reference        : ECB-3249 Student User creation in Salesforce
//********************************************************************/
public with sharing class UserCreationHelper{
    
    
    public static Boolean runningInASandbox() {
        return [SELECT IsSandbox FROM Organization LIMIT 1].IsSandbox;
    }
    
    public void createUserOnContactCreation(List<Contact> conList){
        
        List<User> userList = new List <User>();
        
        Id profileId = [SELECT Id FROM Profile WHERE Name =:ConfigDataMapController.getCustomSettingValue('ProfileName')].Id; //ECB-4308-Shreya:Replaced Custom Label with Custom Setting
        
       
        Set<id> conIds = new Set<id>();
        
        for(Contact con : conList){
            String aliasFName = con.FirstName.substring(0,1);
            String aliasLName;
            if(con.LastName.length()>4){
                aliasLName = con.LastName.substring(0,4);
            }else{
                aliasLName = con.LastName;
            }    
            system.debug('aliasFName::::'+aliasFName+'aliasLName::::'+aliasLName);
            
            String federationId = con.Student_ID__c;
            if(federationId.charAt(0) == 83){
               federationId =federationId.toLowerCase();
            }else if(federationId.isNumeric()){
                federationId = 's'+federationId;
            }else if(federationId.charAt(0) != 115){
 
                federationId = 's'+federationId.substring(1);
            }
                
            User usr = new User();
            usr.FirstName = con.FirstName;
            usr.LastName = con.LastName;
            usr.Email=con.Email;
            if(!runningInASandbox()){
                usr.Username=con.Email+'.marketplace';
                usr.Title = con.Title;
                //usr.CommunityNickname=con.FirstName+con.LastName+'.marketplace';
                //ECB-4238
                usr.CommunityNickname=con.FirstName.left(1)+con.LastName+'.' + con.Student_ID__c;
            }else{
                usr.Username=con.Email+'.marketplace.'+ConfigDataMapController.getCustomSettingValue('SandboxName'); //ECB-4308-Shreya:Replaced Custom Label with Custom Setting
                usr.Title = con.Title+'.'+ConfigDataMapController.getCustomSettingValue('SandboxName'); //ECB-4308-Shreya:Replaced Custom Label with Custom Setting
                //usr.CommunityNickname=con.FirstName+con.LastName+'.marketplace.'+Label.SandboxName;
                usr.CommunityNickname=con.FirstName.left(1)+con.LastName+'.' + con.Student_ID__c+'.' +ConfigDataMapController.getCustomSettingValue('SandboxName'); //ECB-4308-Shreya:Replaced Custom Label with Custom Setting

            }
            usr.ProfileId = profileId;
            usr.ContactId=con.Id;
            usr.IsActive=true;
            usr.IsPortalSelfRegistered=true;
            usr.FederationIdentifier=federationId;
            usr.Alias = aliasFName+aliasLName;
            usr.TimeZoneSidKey = ConfigDataMapController.getCustomSettingValue('Region'); //ECB-4308-Shreya:Replaced Custom Label with Custom Setting
            usr.EmailEncodingKey = ConfigDataMapController.getCustomSettingValue('EmailFormat'); //ECB-4308-Shreya:Replaced Custom Label with Custom Setting
            usr.LanguageLocaleKey = ConfigDataMapController.getCustomSettingValue('Language'); //ECB-4308-Shreya:Replaced Custom Label with Custom Setting
            usr.LocaleSidKey = ConfigDataMapController.getCustomSettingValue('Locale'); //ECB-4308-Shreya:Replaced Custom Label with Custom Setting
            system.debug('user details: ' + usr);
            userList.add(usr);
            
        }
        Database.SaveResult[] srList = Database.insert(userList, false);
        Set<id> userids = new Set<id>();
        
        if(!srList.isEmpty()){
            for (Database.SaveResult sr : srList) {
                if(sr.isSuccess()){
                    System.debug('Success--->'+sr);
                    userids.add(sr.getId());
                 
                }else{
                    for(Database.Error err : sr.getErrors()) {
                        System.debug('Failed ---> '+sr);
                        System.debug(err.getStatusCode() + ': ' + err.getMessage());
                    }
                    
                }
            }
           List<User> successUserList = [select id, contactId from user where id in:userids ];
            System.debug('User Success List--->'+successUserList);
            for(User u: successUserList){
                conIds.add(u.contactId);
            }
            List<Contact> contacts = [select id,Name,Community_User_Provisioned__c from contact where id in :conIds];
            System.debug('User Contact List--->'+contacts);
            for(Contact c:contacts){
                System.debug(c.name);
                c.Community_User_Provisioned__c = true;
            }try{
                Database.update(contacts);
                if(Test.isRunningTest()){
                    throw new DMLException();
                }
            }catch(DmlException ex){
                system.debug('Exception:::'+ex);
            } 
        }
        
        
    }
}