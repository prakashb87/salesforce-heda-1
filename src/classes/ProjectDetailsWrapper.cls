/*******************************************
Purpose: To create Project wrapper Class for Create, edit Project page and Multiple Use
History:
Created by Ankit Bhagat on 05/09/2018
*******************************************/
public class ProjectDetailsWrapper {

    /*@Author       : Gourav Bhardwaj
      @Description  : Wrapper class to collect the list of Researcher Projects and sort it based on the view 1. Initial Load/ Clear All 2. Search Text
      @Story        : RPORW-294
      @CreatedDate  : 1/4/2019
    */
    public class AllResearchProjectPublicWrapper implements Comparable{
        public String title;
        public String description;
        public String keywords;
        public Id recordId;
        public String recordTypeId;
        public Integer sortOrder;
        public Boolean isSearchTextPresent = false;
        Boolean isFilterRecord = false;

        public List<AllResearchProjectPublicWrapper> getAllResearcherProjectWrapperList(List<ResearcherPortalProject__c> researcherProjects){
            List<AllResearchProjectPublicWrapper> allResearchProjectPublicWrapperList = new List<AllResearchProjectPublicWrapper>();

            for(ResearcherPortalProject__c project : researcherProjects){
                AllResearchProjectPublicWrapper researchProjectWrapper =  new AllResearchProjectPublicWrapper(project);
                allResearchProjectPublicWrapperList.add(researchProjectWrapper);
            }

            allResearchProjectPublicWrapperList.sort();
            return allResearchProjectPublicWrapperList;
        }

        public AllResearchProjectPublicWrapper(){
                system.debug('AllResearchProjectPublicWrapper');
        }

        public AllResearchProjectPublicWrapper(ResearcherPortalProject__c researchProject){
             title          = researchProject.Title__c;
             description    = researchProject.Description__c;
             keywords       = researchProject.Keywords__c;
             recordId       = researchProject.Id;
             recordTypeId   = researchProject.RecordTypeid;
        }

        public AllResearchProjectPublicWrapper(ResearcherPortalProject__c researchProject,Integer sortOrder,String searchedString){
             title          = researchProject.Title__c;
             description    = researchProject.Description__c;
             keywords       = researchProject.Keywords__c;
             recordId       = researchProject.Id;
             recordTypeId   = researchProject.RecordTypeid;
             
             this.sortOrder = sortOrder;
             if(sortOrder!=null){
                isFilterRecord = true;
             }
             
             if(!String.isBlank(searchedString)){
             	isSearchTextPresent = true;
             }
        }

        //Code to sort the list if search test then title, description, keyword
        //If not search then sort by title only
        public Integer compareTo(Object compareTo){
            AllResearchProjectPublicWrapper allResearchProject = (AllResearchProjectPublicWrapper)compareTo;
            system.debug(isFilterRecord);
            system.debug('sortOrder : '+sortOrder+' : '+allResearchProject.sortOrder);

            if(isFilterRecord && isSearchTextPresent){//Sorting as per priority when text is entered in search box and other is filtered
                if(sortOrder > allResearchProject.sortOrder){
                    return 1;
                }else if(sortOrder < allResearchProject.sortOrder){
                    return -1;
                }else{//1383
                    if(title > allResearchProject.title){
                        return 1;
                    }else{
                        return -1;
                    }
                }//1383 Ends
            }
            else //Sorting only if Search Text is not provided but other filter values(FOR or Impact Category) are given. 
            if(!isSearchTextPresent){
            	if(title > allResearchProject.title){
                    return 1;
                }else{
                    return -1;
                }
            }
            else{//Sorting Title when fitler criteria is provided
                if(title > allResearchProject.title){
                    return 1;
                }else{
                    return -1;
                }
            }
           
        }//compareTo Ends
    }


    /*@Author       : Gourav Bhardwaj
      @Description  : Wrapper class to return Public Researcher Project with total count and list of records
      @Story        : RPORW-294
      @CreatedDate  : 1/4/2019
    */
    public class ResearchProjectPublicProjects{
        @AuraEnabled public Integer totalPublicProjects {get;set;}
        @AuraEnabled public List<ResearchProjectPublicProjectListView> publicProjectList {get;set;} 
        @AuraEnabled public Boolean hasMoreRecords {get;set;} 
    }
    
    /*****************************************************************************************
    // Purpose      :  For Research Project Public List View
    // Developer    :  Subhajit
    // Created Date :  14/03/2019                 
    //************************************************************************************/ 
    public class ResearchProjectPublicProjectListView implements Comparable{
        
        @AuraEnabled public String projectTitle {get;set;}
        @AuraEnabled public String projectSummary {get;set;}
        @AuraEnabled public string projectTypeIcon  {get;set;}
        @AuraEnabled public List<Researcher_Member_Sharing__c> projectMembersAndRoles {get;set;}
        @AuraEnabled public string recordId  {get;set;} //RPROW-1353 BUG
        public Integer sortOrder; //RPORW-1284
        @AuraEnabled public List<ProjectDetailsWrapper.FieldOfResearchForProjectsWrapper> fieldOfResearchListSorted {get;set;}//RPROW-979
        
        
         public ResearchProjectPublicProjectListView (){
            system.debug('default-cons');
         }

         //Gourav
          public ResearchProjectPublicProjectListView(ProjectDetailsWrapper.AllResearchProjectPublicWrapper rpp,list<Researcher_Member_Sharing__c> projectMembers,List<ProjectDetailsWrapper.FieldOfResearchForProjectsWrapper> fieldOfResearchListSorted)
         {

            system.debug('## projectMembers : '+projectMembers);

            system.debug('## fieldOfResearchListSorted : '+fieldOfResearchListSorted);

            String recordtypename = Schema.SObjectType.ResearcherPortalProject__c.getRecordTypeInfosById().get(rpp.recordTypeId).getname();
            this.projectTitle     = rpp.title;
            this.recordId		  = rpp.recordId; //RPROW-1353 BUG
            this.projectSummary   = rpp.description;
             if(recordtypename == Label.RSTP_ResearchProjectRMRecordtype){
                this.projectTypeIcon = Label.RSTP_ResearchProjectRMRecordtype;
            }else
            {
                this.projectTypeIcon = Label.RSTP_ResearchProjectRPPRecordtype;
            }
            
            this.projectMembersAndRoles   = projectMembers;

            this.fieldOfResearchListSorted = fieldOfResearchListSorted;//RPROW-979
         }

        /*
        @author       : Gourav Bhardwaj
        @Description  : the below method sorts records based on the field where a searchtext is present.
                        If the searchText is present in title the record sortOrder is 1
                        If the searchText is present in description the record sortOrder is 2
                        If the searchText is present in keyword the record sortOrder is 3
                            else the record sortOrder is 4
                        Based on this sortOrder field the list is sorted, giving the required result.
        @Date         : 8/4/2019
        @story        : RPORW-1284
        */
        public Integer compareTo(Object compareTo){
            ResearchProjectPublicProjectListView researchProject = (ResearchProjectPublicProjectListView)compareTo;

                
            if(sortOrder > researchProject.sortOrder){
                return 1;
            }else{
                return -1;
            }
        }//compareTo Ends
    }

    //RPROW-979 : Wrapper contains the FOR codes and are sorted based on FOR Code details
    public class FieldOfResearchForProjectsWrapper implements Comparable{
        @AuraEnabled public String forCodeDetail {get;set;}

        public List<FieldOfResearchForProjectsWrapper> getSortedListOfFieldOfResearch(List<Research_Project_Classification__c> fieldOfResearchList,Boolean isRMProject){
            List<FieldOfResearchForProjectsWrapper> listOfFieldOfResearch = new List<FieldOfResearchForProjectsWrapper>();
            for(Research_Project_Classification__c fieldOfResearch : fieldOfResearchList){
            	   //RPORW-1602 : Checking is FOR Code is not blank
                    if(isRMProject && !String.isBlank(fieldOfResearch.For_Six_Digit_Detail__c)){
                        listOfFieldOfResearch.add(new FieldOfResearchForProjectsWrapper(fieldOfResearch.For_Six_Digit_Detail__c));
                    }else if(!isRMProject && !String.isBlank(fieldOfResearch.For_Four_Digit_Detail__c)){
                        listOfFieldOfResearch.add(new FieldOfResearchForProjectsWrapper(fieldOfResearch.For_Four_Digit_Detail__c));
                    }
            }

            listOfFieldOfResearch.sort();

            return listOfFieldOfResearch;
            
        }

        public FieldOfResearchForProjectsWrapper(){
            system.debug('FieldOfResearchForProjectsWrapper');
        }

        public FieldOfResearchForProjectsWrapper(String forCodeDetail){
            this.forCodeDetail = forCodeDetail;
        }

        public Integer compareTo(Object compareTo){
            FieldOfResearchForProjectsWrapper fieldOfResearch = (FieldOfResearchForProjectsWrapper)compareTo;
            
            if(forCodeDetail > fieldOfResearch.forCodeDetail){
                return 1;
            }else{
                return -1;
            }
        }

    }//FieldOfResearchForProjectsWrapper Ends
    
    /*****************************************************************************************
    // Purpose      :  For Research Project ListView
    // Developer    :  Ankit Bhagat 
    // Created Date :  05/09/2018                 
    //************************************************************************************/ 
    public class ResearchProjectListView{
        
        @AuraEnabled public list<ResearchProjectListView> lstResearchProject {get;set;}
        @AuraEnabled public string projectName {get;set;}
        @AuraEnabled public string projId {get;set;}
        @AuraEnabled public datetime lastmodifiedDate {get;set;}
        @AuraEnabled public Integer projectCount {get;set;}        
        @AuraEnabled public string projectTitle {get;set;}
        @AuraEnabled public string projectSummary {get;set;}
        @AuraEnabled public Date projectStartDate {get;set;}
        @AuraEnabled public Date projectEndDate {get;set;}
        @AuraEnabled public string status {get;set;}
        @AuraEnabled public string accesss {get;set;}
        @AuraEnabled public string projectType  {get;set;}
        @AuraEnabled public Boolean isRMProject{get;set;}
        @AuraEnabled public string projectCIName  {get;set;} // Added by Subhajit :: 10.04.18 :: RPORW-47  
        @AuraEnabled public string projectPrimaryContact {get;set;} // Added by Subhajit :: 10.04.18 :: RPORW-47 
        @AuraEnabled public string currentStatus {get;set;} // Added by Subhajit :: 10.09.18 :: RPORW-264 
        @AuraEnabled public string wbsNumber {get;set;} // Added by Raj for RPORW-281
        @AuraEnabled public string groupStatus {get;set;} // Added by Subhajit for RPORW-721
        @AuraEnabled public Boolean isClosedOff {get;set;} // Added by Subhajit for RPORW-721
        
        public ResearchProjectListView (){
            system.debug('defaultcons');
        }
        
        public ResearchProjectListView(ResearcherPortalProject__c rpp){
            
            String recordtypename = Schema.SObjectType.ResearcherPortalProject__c.getRecordTypeInfosById().get(rpp.RecordTypeId).getname();
            
            this.projId= rpp.Id;
            this.lastmodifiedDate = rpp.LastModifiedDate;
            this.projectName      = rpp.Title__c;
            this.projectTitle     = rpp.Title__c;
            this.projectSummary   = rpp.Description__c;
            this.projectStartDate = rpp.Start_Date__c;
            this.projectEndDate   = rpp.End_Date__c;
            this.status           = rpp.Status__c;
            this.currentStatus    = rpp.Portal_Project_Status__c;// Added by Subhajit :: 10.09.18 :: RPORW-264 
            this.accesss          = rpp.Access__c;
            this.projectType      = rpp.Project_Type__c;
            this.WBSNumber        = (rpp.SAP_WBS_NUM__c != null) ? rpp.SAP_WBS_NUM__c  : '';
            this.groupStatus      = (rpp.Group_Status__c != null) ? rpp.Group_Status__c  : '';// Added by Subhajit for RPORW-721
            this.isClosedOff      = rpp.ls_Closed_Off__c;// Added by Subhajit for RPORW-721
            
            //this.projectCIName  = rpp.owner.Name; // Added by Subhajit :: 10.04.18 :: RPORW-47 
            
            if(recordtypename == 'Researcher Master Project'){
                this.IsRMProject = true;
            }else
            {
                this.IsRMProject = false;
            }
            
            
        }
        
        
        
    }
    
    /*****************************************************************************************
    // Purpose      :  For Research Project Detail Page
    // Developer    :  Ankit Bhagat 
    // Created Date :  05/09/2018                 
    //************************************************************************************/ 
    public class ResearchProjectDetailData{
        
        @AuraEnabled public list<ResearchProjectDetailData> lstResearchProject {get;set;}
        @AuraEnabled public string projectName {get;set;}
        @AuraEnabled public string projId {get;set;}
        @AuraEnabled public datetime lastmodifiedDate {get;set;}
        @AuraEnabled public Integer projectCount {get;set;}
        @AuraEnabled public String groupStatus{get;set;}//added by Ali RPROW-672
        @AuraEnabled public string projectTitle {get;set;}
        @AuraEnabled public string projectSummary {get;set;}
        @AuraEnabled public Date projectStartDate {get;set;}
        @AuraEnabled public Date projectEndDate {get;set;}
        @AuraEnabled public string status {get;set;}
        @AuraEnabled public string accesss {get;set;}
        @AuraEnabled public string projectType  {get;set;}
        @AuraEnabled public string impactCategories  {get;set;}
        @AuraEnabled public string forCodes  {get;set;}
        @AuraEnabled public string impactSummary  {get;set;}
        @AuraEnabled public string projectCIName  {get;set;}
        @AuraEnabled public string projectRecordType{get;set;}
        @AuraEnabled public Boolean IsRMProject{get;set;}
        @AuraEnabled public string agreementType{get;set;}
        @AuraEnabled public Date agreementStartDate{get;set;}
        @AuraEnabled public string WBSNumber{get;set;}
        @AuraEnabled public Boolean IsEdit{get;set;}
        @AuraEnabled public Boolean IsActive{get;set;}
        @AuraEnabled public Boolean IsConfidential{get;set;}
        @AuraEnabled public string currentStatus {get;set;} // Added by Subhajit :: 10.09.18 :: RPORW-264 
        @AuraEnabled public string projectPrimaryContactSchool {get;set;} // Added for RPORW-281 
        @AuraEnabled public List<ContactDetail> projectMembers  {get;set;}
        //@AuraEnabled public List<Id> projectClassificationIds  {get;set;}
        @AuraEnabled public string keywords {get;set;} // Added by Ankit :: 11.08.18 :: RPORW-229 
        @AuraEnabled public string expectedImpact {get;set;} // Added by Ankit :: 11.08.18 :: RPORW-229 
        @AuraEnabled public string fundingBodyRef {get;set;} // Added by Ankit :: 11.08.18 :: RPORW-451 
        @AuraEnabled public Boolean isProjectMember {get;set;} // Added by Ankit :: 18.03.19 :: RPORW-945 
        @AuraEnabled public Boolean isAccessFieldEditable {get;set;} // Added by Ankit :: 15.04.19 :: RPORW-1041
        @AuraEnabled public string loggedInUser {get;set;} // Added by Ankit :: 17.05.19 :: RPORW-1414

        public ResearchProjectDetailData(){
            // isEdit = false;
            projectTitle ='';
            projectSummary ='';
            status  =''; 
            currentStatus='';// Added by Subhajit :: 10.09.18 :: RPORW-264 
            accesss ='';
            groupStatus='';//added by Ali RPROW-672
            projectType  =''; 
            impactSummary ='';
            impactCategories ='';
            forCodes = '';
            WBSNumber = '';
            keywords = '';  // Added by Ankit :: 11.08.18 :: RPORW-229 
            expectedImpact = '';  // Added by Ankit :: 11.08.18 :: RPORW-229 
            fundingBodyRef = ''; /// Added by Ankit :: 11.08.18 :: RPORW-451
            loggedInUser = ''; // Added by Ankit :: 17.05.19 :: RPORW-1414
        }
        
        
        public ResearchProjectDetailData(ResearcherPortalProject__c rpp){
            
            String startDate ='';
            String endDate ='';
            System.debug('@@@recordtypename'+rpp.RecordTypeId);
            string recordtypename = Schema.SObjectType.ResearcherPortalProject__c.getRecordTypeInfosById().get(rpp.RecordTypeId).getname();
            
            
            this.projId= rpp.Id;
            this.lastmodifiedDate = rpp.LastModifiedDate;
            this.groupStatus      = rpp.Group_Status__c;//added by ali RPROW-672
            this.projectName      =  rpp.Title__c;
            this.projectTitle     = rpp.Title__c;
            this.projectSummary   = rpp.Description__c;
            this.projectStartDate = rpp.Start_Date__c;
            this.projectEndDate   = rpp.End_Date__c;
            this.status           = rpp.Status__c;
            this.currentStatus    = rpp.Portal_Project_Status__c;// Added by Subhajit :: 10.09.18 :: RPORW-264 
            this.accesss          = rpp.Access__c;
            this.projectType      = rpp.Project_Type__c;
            this.impactCategories = rpp.Impact_Category__c;
            //this.forCodes         = rpp.Discipline_area__c;
            this.impactSummary    = rpp.Approach_to_impact__c;
            //this.projectCIName    = rpp.owner.Name; // Need to change to  Primary Owner Name
            this.keywords = rpp.Keywords__c;  // Added by Ankit :: 11.08.18 :: RPORW-229
            this.expectedImpact = rpp.Expected_Impact__c;  // Added by Ankit :: 11.08.18 :: RPORW-229
            this.fundingBodyRef = rpp.Funding_Body_Num__c;  // Added by Ankit :: 11.08.18 :: RPORW-451

            
            this.WBSNumber =  (rpp.SAP_WBS_NUM__c != null) ? rpp.SAP_WBS_NUM__c  : '';
            
            if(recordtypename == 'Researcher Master Project'){
                this.IsRMProject = true;
            }else
            {
                this.IsRMProject = false;
            }
            
            this.agreementType      = rpp.Project_Type__c;// modified by subhajit
            this.agreementStartDate = rpp.Start_Date__c;// modified by subhajit
            this.WBSNumber          = rpp.SAP_WBS_NUM__c;
            
            //    this.IsEdit = isEdit;
            this.IsActive = isActive;
            projectMembers=new List<ContactDetail>();
            if (rpp.Linked_Contacts__r!=null) {
                for (Researcher_Member_Sharing__c rms: rpp.Linked_Contacts__r){
                    //projectMembers.add(new ContactDetail(rms));
                    //If RM Project, dont show Members who have NO Access - RPORW-1574 
					if(ResearcherPublicProjectsHelperUtil.isRMProject(rpp) && rms.AccessLevel__c !='NO Access'){
						projectMembers.add(new ContactDetail(rms));
					}
					//If UC Project show all Members - RPORW-1751 
					else if(!ResearcherPublicProjectsHelperUtil.isRMProject(rpp)){
						projectMembers.add(new ContactDetail(rms));
					}
                }
            }
            
        }
    }
    
    /*****************************************************************************************
    // Purpose      :  For Contact Detail Records
    // Developer    :  Ankit Bhagat 
    // Created Date :  05/09/2018                 
    //************************************************************************************/ 
    public class ContactDetail{
        @AuraEnabled public Id memberId {get;set;}
        @AuraEnabled public String contactName {get;set;}
        @AuraEnabled public String contactId {get;set;}
        @AuraEnabled public String memberRole {get;set;}
        @AuraEnabled public String contactEmail {get;set;}
        @AuraEnabled public String schoolName {get;set;}
        @AuraEnabled public boolean IsExternalContact {get;set;}
        @AuraEnabled public boolean IsRMMember {get;set;}
        @AuraEnabled public string RMPosition {get;set;}
        @AuraEnabled public boolean lisActiveFlag {get;set;}// Added by subhajit for US-198
        //Delete Logic to show or hide for,  // Current Owner should not have delete button,
        // RM member should not have deleted     // Only Primary Contact should not have delete button..
        @AuraEnabled public boolean IsReadOnly  {get;set;}
        
        public ContactDetail(Contact cont, String schoolNameVar){
            setContactParams(cont, schoolNameVar);
        }
        
        
        public ContactDetail(Id contId){
            contactId=contId;
        }        
        public ContactDetail(Researcher_Member_Sharing__c rms){
            system.debug('##'+rms); 
            memberId=rms.id;
            lisActiveFlag = rms.Currently_Linked_Flag__c;// Added by subhajit for US-198
            RMPosition =rms.Position__c; // Showing the RM Position if required in the UI
            if(rms.memberRole__c==label.RSTP_Role_No_Access){
                rms.memberRole__c=label.researcher; 
            } else{
                memberRole=rms.memberRole__c;  
            }          
            IsRMMember=rms.RM_Member__c;
            IsReadOnly = false;
            // if role is CI, delete button should be shown for others. And
            //If Primary Contact, delete button should not visible
            if((rms.User__c!=null && rms.User__c==userinfo.getUserId()) || rms.RM_Member__c == true || rms.Primary_Contact_Flag__c==true) // Current User Logged in, delete button should not shown.
            {
                IsReadOnly = true;
            }
      
            // populate member details for community user and external contact
            if(rms.Contact__r!=null){
                setContactParams(rms.Contact__r, schoolName);
                // populate user details for salesforce users.
                
            }
            else if(rms.User__r!=null){
                contactName=rms.User__r.Name;
                contactEmail=rms.User__r.email;
                IsReadOnly = true; // to avoid the deleting SF non community users.
            }
            system.debug('## rms.User__r'+IsReadOnly);
        }
        
    /*****************************************************************************************
    // Purpose      :  Set Contact Parameters
    // Developer    :  Ankit Bhagat 
    // Created Date :  05/09/2018                 
    //************************************************************************************/ 
        public void setContactParams(Contact cont, String schoolNameVar){
            
            contactName=cont.Name;
            contactId=cont.Id;
            //contactEmail=cont.hed__WorkEmail__c;              
           
            contactEmail = getContactEmail(cont);        
            schoolName=schoolNameVar;
            if (cont.Current_Person_Type__c =='External')
            {
                IsExternalContact =true; 
                IsReadOnly = true; // External contact should not be deleted.
            }else
            {
                IsExternalContact =false;
            } 
        }
        
         /*****************************************************************************************
        // Purpose      :  get Contact Email
        // Developer    :  Ankit Bhagat 
        // Created Date :  05/09/2018                 
        //************************************************************************************/
        public String getContactEmail(Contact cont)
        {
            String[] allDOmains = label.ResearchMemberEmailSearchString.split(',');         
            for (String currentDomain: allDOmains){
                if (cont.hed__WorkEmail__c != null && cont.hed__WorkEmail__c.EndsWithIgnoreCase(currentDomain)){
                    contactEmail = cont.hed__WorkEmail__c;
                }
            }
            System.debug('@@@contactEmail1'+contactEmail);
            
            if (contactEmail==null || String.isEmpty(contactEmail)){
                for (String currentDomain: allDOmains){
                    if (cont.hed__UniversityEmail__c != null && cont.hed__UniversityEmail__c.EndsWithIgnoreCase(currentDomain)){
                        contactEmail = cont.hed__UniversityEmail__c;
                    }
                }
            }       
            System.debug('@@@contactEmail2'+contactEmail); 
            return contactEmail;
        }
        
    }
    
}