/*********************************************************************************
*** @ClassName         : SCHOOL_REST
*** @Requirement       : 
*** @Created date      : 28/08/2018
*** @Modified by       : Dinesh Kumar
*** @modified date     : 21/09/2018
**********************************************************************************/
@RestResource(urlMapping = '/HEDA/v1/School')
global with sharing class SCHOOL_REST {

	public String school;
    public String effectiveDate;
    public String status;
    public String description;
    public String shortDescription;
    public String formalDescription;
    public String academicInstitution;
    public String aouCode;
    public String campus;
    public String parentAcademicOrganisation; 

//Moved from inside method to outside to resolve PMD violation
        
        //to create map of effective date        
        public static Map < String, Date > updateEffectiveDate = new Map < String, Date >(); 

        //get list of AOUCode from JSON
        public static List<String> listAOUCode = new List<String>();
        
        //get list of parentAcademicOrganisation from JSON
        public static List<String> listParAcadOrg = new List<String>();

    @HttpPost
    global static Map<String,String> upsertSchool() {

        //get list of campus from JSON
        List<String> listCampus = new List<String>();

        //get list of Academic Institution from JSON
        List<String> listAcademicInstitution = new List<String>();
        
        //Map to update the old Account
        Map < String, Account > oldAccntUpdate = new Map < String, Account > ();
            
        //final response to send
        Map <String, String> responsetoSend = new Map <String, String>();
        //upsert School
        List <Account> upsertSchool = new List <Account>();
        
        //get Map of unique key and existing Account Record Id to update
        Map <String,Id> updateOldSchool = new Map <String,Id>();
        
        //Account object
        Account school;

        //Account Record-School
        Account accountRecord = new Account();
        
        //uniquKey to get unique values
        String uniqueKey;
        //String key;
	
        try {
        
        //request and response variable
        RestRequest request = RestContext.request;
        
        RestResponse response = RestContext.response;
        
        //no need to add [] in JSON
        String reqStr = '[' + request.requestBody.toString() + ']';
        
        response.addHeader('Content-Type', 'application/json');
        
        //getting the data from JSON and Deserialize it
        List < SCHOOL_REST > postString = (List < SCHOOL_REST > ) System.JSON.deserialize(reqStr, List < SCHOOL_REST > .class);
        system.debug('postString-->'+ postString);
        
        
        //get list of school from JSON
        List<String> lstSchool = setListsAsPerRequest(listCampus, listAcademicInstitution, postString);
        
        List<Account> academicInstitutionRecord = SCHOOL_REST_Helper.queryAcademicInstitutionRecord(listAcademicInstitution);
        List<AOU_Code__c> aouCodeRecord = SCHOOL_REST_Helper.queryAouCodeRecord(listAOUCode);
        List<Campus__c>  campusRecord = SCHOOL_REST_Helper.queryCampusRecord(listCampus);
        
        //check existing Record for Map
        List<Account> checkExisAccountRecord = SCHOOL_REST_Helper.queryCheckExisAccountRecord(lstSchool, uniqueKey);
        List<Account> parentAcadOrgaRecord = SCHOOL_REST_Helper.queryParentAcadOrgaRecord(listParAcadOrg);
        
        responsetoSend = checkEffectiveDates(checkExisAccountRecord, oldAccntUpdate, postString);
        if (!responsetoSend.isEmpty()){
        	return responsetoSend;
        }

		Map<String, List<sObject>> paramstoPass=new Map<String, List<sObject>>();
		paramsToPass.put('academicInstitutionRecord', academicInstitutionRecord);
		paramsToPass.put('aouCodeRecord', aouCodeRecord);
		paramsToPass.put('campusRecord', campusRecord);
		paramsToPass.put('parentAcadOrgaRecord', parentAcadOrgaRecord);
        school = setupSchool(postString, oldAccntUpdate, paramsToPass);
		return SCHOOL_REST_Helper.upsertSchool(school, upsertSchool, postString);

        } catch (Exception excpt) {
        //If exception occurs then return this message to IPASS
        responsetoSend = throwError();
        return responsetoSend;    
        }
        
    }

    private static Account setupSchool(List < SCHOOL_REST > postString, Map < String, Account > oldAccntUpdate, Map<String, List<sObject>> paramsToPass) {    //insert or update the School
		Account school = new Account();
		String key='';
        for (SCHOOL_REST schoolRest: postString) {
            key = schoolRest.school;
            school.Name = schoolRest.description.length() > 0 ? schoolRest.description : schoolRest.school;
            school.RecordTypeId = Schema.SObjectType.Account.getRecordTypeInfosByName().get('Academic Institution').getRecordTypeId();
            school.AccountNumber = schoolRest.school;
            school.AccountNumber__c = schoolRest.school;
            school.Effective_Date__c = Date.valueOf(schoolRest.effectiveDate);
            school.Status__c = schoolRest.status ;
            school.Description = schoolRest.description;
            school.Short_Description__c = schoolRest.shortDescription;
            school.Formal_Description__c = schoolRest.formalDescription;
            school.Academic_Institution__c = QuickSobjectUtils.getFirstIDElseNull(paramsToPass.get('academicInstitutionRecord'));
            school.AOUCode__c = QuickSobjectUtils.getFirstIDElseNull(paramsToPass.get('aouCodeRecord'));
            school.Campus__c = QuickSobjectUtils.getFirstIDElseNull(paramsToPass.get('campusRecord'));
            school.ParentId = QuickSobjectUtils.getFirstIDElseNull(paramsToPass.get('parentAcadOrgaRecord'));
            //school.AOUCodeDescription__c = aouCodeRecord.size() > 0 ? aouCodeRecord[0].Description__c:null;
            school.Level_Name__c = 'School';
        }
		checkEffectiveDateOfOld(oldAccntUpdate, school, key);
        return school;
	}

	private static Account checkEffectiveDateOfOld(Map < String, Account > oldAccntUpdate, Account school, String key){
            if (oldAccntUpdate.size() > 0 && oldAccntUpdate.get(key) != null){
                if(oldAccntUpdate.get(key).Effective_Date__c <= school.Effective_Date__c){
                    school.ID = oldAccntUpdate.get(key).Id;       
                }else{
                    school = null;
                }
            }
            return school;
	}
	
    public static Map <String, String> checkEffectiveDates(List<Account> checkExisAccountRecord, Map < String, Account > oldAccntUpdate, List < SCHOOL_REST > postString){
			Map <String, String> responsetoSend = new Map <String, String>();
            for(Account prevAccount: checkExisAccountRecord){
                String uniqueKey = prevAccount.AccountNumber__c;
                if(updateEffectiveDate != null && updateEffectiveDate.get(uniqueKey) != null){
                    
                    if(updateEffectiveDate.get(uniqueKey) <= System.today() && prevAccount.Effective_Date__c <= updateEffectiveDate.get(uniqueKey) ){
                        oldAccntUpdate.put(uniqueKey, prevAccount);    
                    }else if(updateEffectiveDate.get(uniqueKey) > System.today()){
                        caseMatching(postString,checkExisAccountRecord);
                    }else if(prevAccount.Effective_Date__c > updateEffectiveDate.get(uniqueKey)){
                        responsetoSend.put('message', 'OK');
                        responsetoSend.put('status', '200');
                        return responsetoSend;
                    }           
                }else{
                    responsetoSend.put('message', 'OK');
                    responsetoSend.put('status', '200');
                    return responsetoSend;
                }        
            }
            return responsetoSend;
    }            
    
    private static List<String> setListsAsPerRequest(List<String> listCampus, List<String> listAcademicInstitution, List < SCHOOL_REST > postString){
    	 List<String> lstSchool = new List<String>();
    	 
    	 if (postString != null) {
        
            for (SCHOOL_REST schoolRest: postString) { 
                if(!String.isEmpty(schoolRest.academicInstitution)){
                        listAcademicInstitution.add(schoolRest.academicInstitution);
                }
                
                if(!String.isEmpty(schoolRest.aouCode)){
                        listAOUCode.add(schoolRest.aouCode);
                }

                addCampusAndParAcadOrg(schoolRest, listCampus, listParAcadOrg);
                addSchoolAndEffectiveDate(schoolRest, lstSchool, updateEffectiveDate);
            }
         }
         return lstSchool;

    }

    private static void addCampusAndParAcadOrg(SCHOOL_REST schoolRest, List<String> listCampus, List<String> listParAcadOrg){
                if(!String.isEmpty(schoolRest.campus)){
                        listCampus.add(schoolRest.campus);
                }

                if(!String.isEmpty(schoolRest.parentAcademicOrganisation)){
                        listParAcadOrg.add(schoolRest.parentAcademicOrganisation);    
                }
    }
    
    private static void addSchoolAndEffectiveDate(SCHOOL_REST schoolRest, List<String> lstSchool, Map<String, Date> updateEffectiveDate){
                if(!String.isEmpty(schoolRest.school)){
                        lstSchool.add(schoolRest.school);
                }                

                if(!String.isEmpty(schoolRest.effectiveDate)){
                    updateEffectiveDate.put(schoolRest.school, Date.valueOf(schoolRest.effectiveDate));
                }
    }
    
    public static void caseMatching(List<SCHOOL_REST> postString, List<Account> prevAccounts){

       if(postString[0].school.length() > 0){//check
	       List<Future_Change__c> lstFutureChange = new List<Future_Change__c>();
	       
		   SCHOOL_REST_Util.compareEffDateAndAdd(lstFutureChange, postString, prevAccounts);
		   SCHOOL_REST_Util.compareStatusAndAdd(lstFutureChange, postString, prevAccounts);
		   SCHOOL_REST_Util.compareDescriptionsAndAdd(lstFutureChange, postString, prevAccounts);
           SCHOOL_REST_Util.compareAcademicInstitutionAndAdd(lstFutureChange, postString, prevAccounts);
           SCHOOL_REST_Util.compareAouCodeAndAdd(lstFutureChange, postString, prevAccounts);
           SCHOOL_REST_Util.compareCampusAndAdd(lstFutureChange, postString, prevAccounts);
		   SCHOOL_REST_Util.compareParentAcademicOrganisatioAndAdd(lstFutureChange, postString, prevAccounts);
           
            if(QuickUtils.isNotEmpty(lstFutureChange)){
                Database.insert(lstFutureChange);    
            }

        } 
	}
	
    public static Map < String, String > throwError() {
        //to store the error response
        Map < String, String > errorResponse = new Map < String, String > ();
        errorResponse.put('message', 'Oops! School was not well defined, amigo.');
        errorResponse.put('status', '404');
        //If error occurs then return this message to IPASS
        return errorResponse;
    }
    
    public static Map<String, Schema.DescribeFieldResult> getFieldMetaData(Schema.DescribeSObjectResult dsor, Set<String> fields) {
            
            // the map to be returned with the final data
            Map<String,Schema.DescribeFieldResult> finalMap = 
                new Map<String, Schema.DescribeFieldResult>();
            // map of all fields in the object
            Map<String, Schema.SObjectField> objectFields = dsor.fields.getMap();
            
            // iterate over the requested fields and get the describe info for each one. 
            // add it to a map with field name as key
            for(String field : fields){
                // skip fields that are not part of the object
                if (objectFields.containsKey(field)) {
                    Schema.DescribeFieldResult dr = objectFields.get(field).getDescribe();
                    // add the results to the map to be returned
                    finalMap.put(field, dr); 
                }
            }
            return finalMap;
    }

    public static Future_Change__c createFutChange(String str){
        //request and response variable
        RestRequest request = RestContext.request;
        
        RestResponse response = RestContext.response;
        
        //no need to add [] in JSON
        String reqStr = '[' + request.requestBody.toString() + ']';
        
        response.addHeader('Content-Type', 'application/json');
        List < SCHOOL_REST > postString = (List < SCHOOL_REST > ) System.JSON.deserialize(reqStr, List < SCHOOL_REST > .class);
        Future_Change__c fuChangeSchool = new Future_Change__c();
        Set<String> fields = new Set<String>{str};
            
        Map<String, Schema.DescribeFieldResult> finalMap = 
        SCHOOL_REST.getFieldMetaData(Account.getSObjectType().getDescribe(), fields);
        
        // only print out the 'good' fields
        for (String field : new Set<String>{str}) {
            fuChangeSchool.API_Field_Name__c = finalMap.get(field).getName();
            fuChangeSchool.Field__c = finalMap.get(field).getLabel();
        }
        
        for (SCHOOL_REST checkDate : postString) {
            string jsonDate = checkDate.effectiveDate;
            fuChangeSchool.Effective_Date__c = Date.valueOf(jsonDate);
        }
        
        return fuChangeSchool;
    }

}