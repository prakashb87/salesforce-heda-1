/*********************************************************************************
*** @ClassName         : FutureChange_Generic_Scheduler
*** @Author            : Anupam Singhal
*** @Requirement       : RM-1632
*** @Created date      : 21/09/2018
*** @Modified by       : Shubham Singh
*** @modified date     : 25/09/2018
**********************************************************************************/
public without sharing class FutureChange_Generic_Scheduler implements Schedulable{
    
    public void execute(SchedulableContext sc){

        String accountQuery = 'SELECT Id,API_Field_Name__c,Applied__c,Effective_Date__c,Field__c,Value__c,Parent_Lookup__c FROM Future_Change__c WHERE Applied__c = FALSE AND Effective_Date__c <= TODAY';
        String campusQuery = 'SELECT Id,API_Field_Name__c,Applied__c,Effective_Date__c,Field__c,Value__c,Parent_Lookup__c FROM Campus_Future_Change__c WHERE Applied__c = FALSE AND Effective_Date__c <= TODAY';
        String courseOffQuery = 'SELECT Id,API_Field_Name__c,Applied__c,Effective_Date__c,Field__c,Value__c,Parent_Lookup__c FROM Course_Offering_Future_Change__c WHERE Applied__c = FALSE AND Effective_Date__c <= TODAY';
        String planQuery = 'SELECT Id,API_Field_Name__c,Applied__c,Effective_Date__c,Field__c,Value__c,Parent_Lookup__c FROM Plan_Future_Change__c WHERE Applied__c = FALSE AND Effective_Date__c <= TODAY';
        String programEnrollmentQuery = 'SELECT Id,API_Field_Name__c,Applied__c,Effective_Date__c,Field__c,Value__c,Parent_Lookup__c FROM Program_Enrollment_Future_Change__c WHERE Applied__c = FALSE AND Effective_Date__c <= TODAY';
            
        FutureChange_Generic_Batch accountUpdate = new FutureChange_Generic_Batch(accountQuery,'Account');
        FutureChange_Generic_Batch campusUpdate = new FutureChange_Generic_Batch(campusQuery,'Campus__c');
        FutureChange_Generic_Batch courseUpdate = new FutureChange_Generic_Batch(courseOffQuery,'hed__Course__c');
        FutureChange_Generic_Batch planUpdate = new FutureChange_Generic_Batch(planQuery,'Plan__c');
        FutureChange_Generic_Batch programEnrollmentUpdate = new FutureChange_Generic_Batch(programEnrollmentQuery,'hed__Program_Enrollment__c');
        
        Database.executeBatch(accountUpdate, 50);
        Database.executeBatch(campusUpdate, 50);
        Database.executeBatch(courseUpdate, 50);
        Database.executeBatch(planUpdate, 50);
        Database.executeBatch(programEnrollmentUpdate, 50);
    }

}