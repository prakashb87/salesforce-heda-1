/*****************************************************************************************************
 *** @Class             : CourseConnStatusUpdate
 *** @Author            : Shreya Barua
 *** @Requirement       : On click of Opt Out button, update the status of Course Connection to 'Opt Out'
 *** @Created date      : 17/07/2018
 *** @JIRA ID           : ECB-4246
 *** @LastModifiedBy    : Shreya Barua
 *** @LastModified date : 17/07/2018
 
 *****************************************************************************************************/

public without sharing class CourseConnStatusUpdate
{
    @AuraEnabled
    public static String setCourseConnStatus(String courseConnId){
    
        system.debug('courseConnId::::'+courseConnId);
        system.debug('user::::'+UserInfo.getUserId());
        
        String updateStatusMessage = ''; //variable to store status update suucess or error 
        hed__Course_Enrollment__c courseConnRecord;

        //ECB-4308-Shreya:Replaced Custom Label with Custom Setting
        List<Profile> profileList = [Select Id,Name FROM Profile where Name = 'RMIT 21CC User'];
        
        if(UserInfo.getProfileId() == profileList[0].Id){
            //Querying PermissionSetAssignment to check whether logged in user is 21CC Standard User and contains Marketplace Business Admin permission set 
            List<PermissionSetAssignment> permissionSetAssignmentList = [Select Id FROM PermissionSetAssignment WHERE AssigneeId = :UserInfo.getUserId()  
                                                                        AND PermissionSetId IN (Select Id from PermissionSet where Name = :ConfigDataMapController.getCustomSettingValue('MarketPlace Permission Set'))];
          
        
            system.debug('permissionSetAssignmentList ::::'+permissionSetAssignmentList);    
            
            if(permissionSetAssignmentList.size() > 0)
            {                            
               courseConnRecord = [Select Id,hed__Status__c from hed__Course_Enrollment__c where Id = :courseConnId and (Source_Type__c = 'MKT' OR Source_Type__c = 'MKT-Batch')];
                
                
                if(courseConnRecord!=null)
                {
                    if(courseConnRecord.hed__Status__c != 'Opt Out')
                    {
                        courseConnRecord.hed__Status__c = 'Opt Out' ;
                        updateStatusMessage = ConfigDataMapController.getCustomSettingValue('Success'); //ECB-4308-Shreya:Replaced Custom Label with Custom Setting
                    }
                    else if(courseConnRecord.hed__Status__c == 'Opt Out')
                    {
                        //ECB-4308-Shreya:Replaced Custom Label with Custom Setting
                        updateStatusMessage = ConfigDataMapController.getCustomSettingValue('Status Error1'); // Error1 message to identify that status is already updated to opt out
                    }    
                }
                else
                {
                    //ECB-4308-Shreya:Replaced Custom Label with Custom Setting
                   updateStatusMessage = ConfigDataMapController.getCustomSettingValue('Status Error2'); // Error2 message to identify that the user does not have the required permission 
                }
            }
            else
            {
                //ECB-4308-Shreya:Replaced Custom Label with Custom Setting
                updateStatusMessage = ConfigDataMapController.getCustomSettingValue('Status Error2'); // Error2 message to identify that the user does not have the required permission
            }  
        }
        else
        {
                updateStatusMessage = ConfigDataMapController.getCustomSettingValue('Status Error2'); //ECB-4308-Shreya:Replaced Custom Label with Custom Setting
        }
        
        
        if(updateStatusMessage == ConfigDataMapController.getCustomSettingValue('Success')){ //ECB-4308-Shreya:Replaced Custom Label with Custom Setting
            try{
                update courseConnRecord ;
               
            }catch(DMLException ex){
                system.debug('Exception::::'+ex);
            }
            
        } 
        
        return updateStatusMessage;
        
        
    }
}