/*********************************************************************************
*** @ClassName         : CAMPUS_REST
*** @Author            : Shubham Singh 
*** @Requirement       : 
*** @Created date      : 27/08/2018
*** @Modified by       : Rahul Nasa
*** @modified date     : 10/10/2018
**********************************************************************************/
@RestResource(urlMapping = '/HEDA/v1/Campus')
global with sharing class CAMPUS_REST {
    public String institution;
    public String campus;
    public String effectiveDate;
    public String status;
    public String description;
    public String shortDescription;
    public String location;
    public String validLocationCode;
    
    @HttpPost
    global static Map < String, String > upsertCampus() {
        //final response to send
        Map < String, String > responsetoSend = new Map < String, String > ();
        //upsert Location
        list < Campus__c > upsertCampus = new list < Campus__c > ();
        //get instituion
        list < String > listInstitution = new list < String > ();
        //getting campus code
        list < String > listCampus = new list < String > ();
        //Location object
        Campus__c campus;
        //to create map of effective date        
        map < String,Date > updateEffectiveDate = new map < String,Date >(); 
        //location record
        
        //list location
        list < string > listLocation = new list < string > ();
        //store institution
        Account institution;
        list<Location__c>  location = new list<Location__c>();
        //key to get unique values
        String key;
        try {
            
            //request and response variable
            RestRequest request = RestContext.request;
            
            RestResponse response = RestContext.response;
            
            //no need to add [] in JSON
            String reqStr = '[' + request.requestBody.toString() + ']';
            
            response.addHeader('Content-Type', 'application/json');
            system.debug('PostString From IPASS ------------> ' + reqStr);
            //getting the data from JSON and Deserialize it
            List < CAMPUS_REST > postString = (List < CAMPUS_REST > ) System.JSON.deserialize(reqStr, List < CAMPUS_REST > .class);
            
            if (postString != null) {
                for (CAMPUS_REST camp : postString) {
                    
                    if ((camp.campus != null && (camp.campus).length() > 0) && (camp.institution != null && (camp.institution).length() > 0)) {
                        listCampus.add(camp.campus);
                        listInstitution.add(camp.institution);
                        if (camp.location != null)
                        {
                            listLocation.add(camp.location);
                        }
                            
                        if(camp.effectiveDate != null && (camp.effectiveDate).length() > 0)
                        {
                            updateEffectiveDate.put(camp.institution+''+camp.campus+''+ camp.validLocationCode,Date.ValueOf(camp.effectiveDate));
                        }
                    } 
                    else 
                    {
                        responsetoSend = throwError();
                        return responsetoSend;
                    }
                    
                }
            }
            //Store institution record
            if (listInstitution != null && listInstitution.size() > 0){
                institution = [SELECT id from account where Name =: listInstitution[0] Limit 1];
            }
            //Get location record
            if (listLocation != null && listLocation.size() > 0){
                location = [SELECT id, Description__c FROM Location__c WHERE Name =: postString[0].location LIMIT 1];
            }
            
            //getting the old Location for update
            list < Campus__c > previousLocation = new list < Campus__c > ();
            if (listCampus.size() > 0 && listCampus != null && listInstitution.size() > 0 && listInstitution != null) {
                previousLocation = [SELECT id,Name,Location__r.Name,Institution__r.Name,Institution__c,/*Campus__c*/Effective_date__c,Status__c,Description__c,Descrshort__c,Location__c,Valid_Location_Code__c FROM Campus__c WHERE Name =: listCampus[0] AND Institution__r.Name =: listInstitution AND Valid_Location_Code__c   =: postString[0].validLocationCode LIMIT 1];
            }
            //map to update the old Location
            map < String, ID > oldCampusUpdate = new map < String, ID > ();
            map < String, Campus__c > strCampusUpdate = new map < String, Campus__c> ();
            List<Campus__c> futureEffectiveRecord = new List<Campus__c>();
            for (Campus__c camp : previousLocation) {
                String uniqueKey = camp.Institution__r.Name +''+camp.Name+''+ camp.Valid_Location_Code__c;
                if(updateEffectiveDate != null && updateEffectiveDate.get(uniqueKey) != null)
                {
                    if(updateEffectiveDate.get(uniqueKey) <= System.today() &&  updateEffectiveDate.get(uniqueKey) > camp.Effective_Date__c ){
                        oldCampusUpdate.put(uniqueKey, camp.ID);
                        strCampusUpdate.put(uniqueKey, camp);   
                    }else if(updateEffectiveDate.get(uniqueKey) > System.today()){
                        futureEffectiveRecord.add(camp);
                    }else if(camp.Effective_date__c > updateEffectiveDate.get(uniqueKey)){
                        responsetoSend.put('message', 'OK');
                        responsetoSend.put('status', '200');
                        return responsetoSend;
                    }
                    
                }else{
                    responsetoSend.put('message', 'OK');
                    responsetoSend.put('status', '200');
                    return responsetoSend;
                }
            }
            if(futureEffectiveRecord != null && futureEffectiveRecord.size () > 0){
                greaterEffectiveDate(futureEffectiveRecord,postString);
            }
            
            //insert or update the Location
            if(futureEffectiveRecord.size() <= 0 && futureEffectiveRecord.isEmpty()){
                for (CAMPUS_REST campusRest : postString) {
                    campus = new Campus__c();
                    key = campusRest.institution + '' +campusRest.campus+''+campusRest.validLocationCode;
                    campus.Institution__c   =   institution != null ? institution.id : null;
                    campus.Name    =   campusRest.campus;
                    campus.Effective_date__c    =   campusRest.effectiveDate != null && (campusRest.effectiveDate).length () > 0 ? Date.ValueOf(campusRest.effectiveDate) : null;
                    campus.Status__c    =   campusRest.status == 'A' ? 'Active': 'Inactive';
                    campus.Description__c   =   campusRest.description;
                    campus.Descrshort__c    =   campusRest.shortDescription;
                    campus.Location__c  =  location != null && location.size() > 0 ? location[0].ID : null;
                    campus.Valid_Location_Code__c   =   campusRest.validLocationCode;
                    campus.Campus_key__c = key ;
                    //campus.Location_Description__c = location != null && location.size() > 0 ? location[0].Description__c : null; uncomment if required in future.
                    //assigning the Location id for update
                    if(strCampusUpdate.size() > 0 && strCampusUpdate.get(key) != null){
                        if(strCampusUpdate.get(key).Effective_Date__c <= campus.Effective_Date__c )
                        {
                            campus.Id = oldCampusUpdate.get(key);   
                        }else{
                            campus = null;
                        }
                    }
                    if(campus!=null){
                        //add campus to the list
                        upsertCampus.add(campus);
                    }     
                }
            }
            //upsert campus
            if (upsertCampus != null && upsertCampus.size() > 0 && Date.ValueOf(postString[0].effectiveDate) <= System.today()) {
                upsert upsertCampus Campus_key__c ;
            }
            responsetoSend.put('message', 'OK');
            responsetoSend.put('status', '200');
            return responsetoSend;
        } catch (Exception excpt) {
            //If exception occurs then return this message to IPASS
            responsetoSend = throwError();
            return responsetoSend;
        }
        
    }
    
    public static Map < String, String > throwError() {
        //to store the error response
        Map < String, String > errorResponse = new Map < String, String > ();
        errorResponse.put('message', 'Oops! Campus was not well defined, amigo.');
        errorResponse.put('status', '404');
        //If error occurs then return this message to IPASS
        return errorResponse;
    }
    
    public static void greaterEffectiveDate(List<Campus__c> camp,List < CAMPUS_REST > jsonString){
        List<Campus_Future_Change__c> lstCampusFuChange = new List<Campus_Future_Change__c>();
        Campus_Future_Change__c CampusFuChange ;
        if (jsonString != null) 
        {
        	//Get Location__c records for camp Location name is changed
			Set<String> locationNames=new Set<String>();
        	for (CAMPUS_REST campRest : jsonString) {
                if(camp[0].Location__r.Name != campRest.location && campRest.location!=null){
                    	locationNames.add(campRest.location);
                }
        	}
        	List<Location__c> locationRecs=new List<Location__c>();
        	if (!Schema.sObjectType.Location__c.IsAccessible()){
        		locationRecs=[SELECT Id, Name FROM Location__c WHERE Name IN :locationNames];
        	}
        	Map<String, Id> locationNameToRecordMap=new Map<String, Id>(); 
        	for (Location__c loc: locationRecs){
        		locationNameToRecordMap.put(loc.Name, loc.Id);
        	}
        	
            for (CAMPUS_REST campRest : jsonString)
             {
                /*if (camp[0].Institution__r.Name != campRest.institution){
                    CampusFuChange = new Campus_Future_Change__c();
                    CampusFuChange = CAMPUS_REST.createCampusFutrChange('Institution__c');
                    CampusFuChange.Parent_Lookup__c = camp[0].Id;
                    CampusFuChange.Value__c = campRest.institution;//other fields to add
                    lstCampusFuChange.add(CampusFuChange);                           
                    }
                    if(camp[0].Name != campRest.campus){
                    CampusFuChange = new Campus_Future_Change__c();
                    CampusFuChange = CAMPUS_REST.createCampusFutrChange('Name');
                    CampusFuChange.Parent_Lookup__c = camp[0].Id;
                    CampusFuChange.Value__c = campRest.campus;
                    lstCampusFuChange.add(CampusFuChange);
                }*/
                if(String.valueOf(camp[0].Effective_date__c) != campRest.effectiveDate){
                    CampusFuChange = new Campus_Future_Change__c();
                    CampusFuChange = CAMPUS_REST.createCampusFutrChange('Effective_date__c');
                    CampusFuChange.Parent_Lookup__c = camp[0].Id;
                    CampusFuChange.Value__c = campRest.effectiveDate;
                    lstCampusFuChange.add(CampusFuChange);
                }
                String restStatus = null;
                if(campRest.status == 'A'){
                    restStatus = 'Active';
                }
                if(camp[0].Status__c != restStatus){
                    CampusFuChange = new Campus_Future_Change__c();
                    CampusFuChange = CAMPUS_REST.createCampusFutrChange('Status__c');
                    CampusFuChange.Parent_Lookup__c = camp[0].Id;
                    CampusFuChange.Value__c = campRest.status;
                    lstCampusFuChange.add(CampusFuChange);
                }
                
                if(camp[0].Description__c != campRest.description){
                    CampusFuChange = new Campus_Future_Change__c();
                    CampusFuChange = CAMPUS_REST.createCampusFutrChange('Description__c');
                    CampusFuChange.Parent_Lookup__c = camp[0].Id;
                    CampusFuChange.Value__c = campRest.description;
                    lstCampusFuChange.add(CampusFuChange);
                }
                if(camp[0].Descrshort__c != campRest.shortDescription){
                    CampusFuChange = new Campus_Future_Change__c();
                    CampusFuChange = CAMPUS_REST.createCampusFutrChange('Descrshort__c');
                    CampusFuChange.Parent_Lookup__c = camp[0].Id;
                    CampusFuChange.Value__c = campRest.shortDescription;
                    lstCampusFuChange.add(CampusFuChange);
                }
                if(camp[0].Location__r.Name != campRest.location){
                    CampusFuChange = new Campus_Future_Change__c();
                    CampusFuChange = CAMPUS_REST.createCampusFutrChange('Location__c');
                    CampusFuChange.Parent_Lookup__c = camp[0].Id;
                    CampusFuChange.Value__c = campRest.location!= null?locationNameToRecordMap.get(campRest.location):null;
                    lstCampusFuChange.add(CampusFuChange);
                }
                /* Since Valid location code is a key now - future tracking descoped
                if(camp[0].Valid_Location_Code__c != campRest.validLocationCode){
                    CampusFuChange = new Campus_Future_Change__c();
                    CampusFuChange = CAMPUS_REST.createCampusFutrChange('Valid_Location_Code__c');
                    CampusFuChange.Parent_Lookup__c = camp[0].Id;
                    CampusFuChange.Value__c = campRest.validLocationCode;
                    lstCampusFuChange.add(CampusFuChange);
                }*/
                
            }
        }
        if(lstCampusFuChange != null && lstCampusFuChange.size() > 0)
        {
            insert lstCampusFuChange;
        }
    }
    
    public static Map<String, Schema.DescribeFieldResult> getFieldMetaData(
        Schema.DescribeSObjectResult dsor, Set<String> fields) {
            
            // the map to be returned with the final data
            Map<String,Schema.DescribeFieldResult> finalMap = 
                new Map<String, Schema.DescribeFieldResult>();
            // map of all fields in the object
            Map<String, Schema.SObjectField> objectFields = dsor.fields.getMap();
            
            // iterate over the requested fields and get the describe info for each one. 
            // add it to a map with field name as key
            for(String field : fields){
                // skip fields that are not part of the object
                if (objectFields.containsKey(field)) {
                    Schema.DescribeFieldResult dr = objectFields.get(field).getDescribe();
                    // add the results to the map to be returned
                    finalMap.put(field, dr); 
                }
            }
            return finalMap;
        }
    public static Campus_Future_Change__c createCampusFutrChange(String str){
        //request and response variable
        RestRequest request = RestContext.request;
        
        RestResponse response = RestContext.response;
        
        //no need to add [] in JSON
        String reqStr = '[' + request.requestBody.toString() + ']';
        
        response.addHeader('Content-Type', 'application/json');
        
        //getting the data from JSON and Deserialize it
        List < CAMPUS_REST > postString = (List < CAMPUS_REST > ) System.JSON.deserialize(reqStr, List < CAMPUS_REST > .class);
        
        Campus_Future_Change__c campFu = new Campus_Future_Change__c();
        Set<String> fields = new Set<String>{str};
            
            Map<String, Schema.DescribeFieldResult> finalMap = 
            CAMPUS_REST.getFieldMetaData(Campus__c.getSObjectType().getDescribe(), fields);
        
        // only print out the 'good' fields
        for (String field : new Set<String>{str}) {
            campFu.API_Field_Name__c = finalMap.get(field).getName();
            campFu.Field__c = finalMap.get(field).getLabel();
        }
        
        for (CAMPUS_REST checkDate : postString) {
            string jsonDate = checkDate.effectiveDate;
            campFu.Effective_Date__c = Date.valueOf(jsonDate);
        }
        
        //campFu.Applied__c = true;
        
        return campFu;
    }
}