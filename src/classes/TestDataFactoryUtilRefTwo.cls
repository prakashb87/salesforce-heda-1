@isTest
 public class TestDataFactoryUtilRefTwo
 {
   public static void testfullLoad()
   {
    TestDataFactoryUtil.dummycustomsetting();        
    List<hed__Course_Enrollment__c> lstcrsenr = [SELECT Id,Name FROM hed__Course_Enrollment__c];
    
    Id accountRecordTypeId = Schema.SObjectType.Account.getRecordTypeInfosByName().get('Business Organization').getRecordTypeId();  
    
    Id recordTypeId = Schema.SObjectType.Account.getRecordTypeInfosByName().get('Academic Program').getRecordTypeId();
    
    Id courseEnrollRecordTypeId = Schema.SObjectType.hed__Course_Enrollment__c.getRecordTypeInfosByName().get('Student').getRecordTypeId();
    
    Account pt = new Account(name = 'RMIT Online', recordTypeId = Schema.SObjectType.Account.getRecordTypeInfosByName().get('Academic Institution').getRecordTypeId());
    insert pt; 
    
    Account accountRec = new Account(Name='Test Partner',RecordTypeId=accountRecordTypeId);
    Insert accountRec;
    
    //Creating Address test data
    hed__Address__c addr = new hed__Address__c();
    addr.hed__MailingStreet__c = 'abc';
    addr.hed__MailingStreet2__c = 'xyz';
    addr.hed__MailingCity__c= 'Test';
    addr.hed__MailingState__c= 'Mel';
    addr.hed__MailingPostalCode__c= '0121';
    addr.hed__MailingCountry__c = 'Aus';
    addr.Last_Name__c = 'test';
    Insert addr;
    
    User u = [Select id, ContactId from User where id =: UserInfo.getUserId() LIMIT 1];
    
    Contact cons = new Contact(LastName='TestCon',Student_ID__c='123',Email='mail@mail.com',hed__Current_Address__c=addr.Id);
    insert cons;
    //
    List<hed__Course__c> courseList = testFullLoadCourseList(pt);
	Insert courseList;
	//
    Set<String> cnameSet = new Set<String>();
    for(hed__Course__c c : courseList){
        cnameSet.add(c.Name);
    }
    List<hed__Course__c> allCourseList = [SELECT Id FROM hed__Course__c WHERE Name IN :cnameSet];
    
    hed__Term__c term = new hed__Term__c(name = 'test term', hed__Account__c = pt.id);
    insert term;
    hed__Term__c term1 = new hed__Term__c(name = 'test term1', hed__Account__c = pt.id);
    insert term1;
    //
    List<hed__Course_Offering__c> cooferList = testFullLoadCooferList(term1,allCourseList);
	Insert cooferList;
    //
    List<Course_Testimonial__c> ctList = testFullLoadCTList(allCourseList);
	Insert ctList;
    // 
	
    //Creating Course Industry Partner Test Data
    Course_Industry_Partner__c cpRec = new Course_Industry_Partner__c(Course_Id__c=allCourseList[100].Id,Industry_Partner__c=accountRec.Id);
    Insert cpRec;
    
    //Creating Parent Account
    Account parent = new Account(name = 'RMIT Online');
    insert parent;
    // 
    List<Account> progList = testFullLoadProgramList(parent,recordTypeId);
	Insert progList;
	//
    Set<String> pnameSet = new Set<String>();
    for(Account c : progList){
        pnameSet.add(c.Name);
    }
    List<Account> allProgList = [SELECT Id FROM Account WHERE Name IN :pnameSet];
    
    //Creatin Plan test Data
    Set<String> pplannameSet = testFullLoadPPlanList(allProgList);
	
    testFullLoadAllProgPlanList(pplannameSet,allCourseList);
    
    testFullLoadCreateRecords(cooferList,allProgList,allCourseList);
	
	testFullLoadCreateProgramIndustryPartner(allProgList,accountRec);
  }  
public static  List<hed__Course__c> testFullLoadCourseList(Account pt)
{
 List<hed__Course__c> courseList = new List<hed__Course__c>();
    for(Integer i=0;i<=1000;i++){
        if(i<=350){
            hed__Course__c course = new hed__Course__c();
            course.Name = 'test Name'+i;
            course.hed__Account__c = pt.id;
            course.Status__c='Active';
            course.RecordTypeId=Schema.SObjectType.hed__Course__c.getRecordTypeInfosByName().get('21CC').getRecordTypeId();
            courseList.add(course);
            
        }else if(i>350 && i<=700){
            hed__Course__c course = new hed__Course__c();
            course.Name = 'test Name'+i;
            course.hed__Account__c = pt.id;
            course.Status__c='Active';
            course.RecordTypeId=Schema.SObjectType.hed__Course__c.getRecordTypeInfosByName().get('RMIT Online').getRecordTypeId();
            courseList.add(course);
           
        }else{
            hed__Course__c course = new hed__Course__c();
            course.Name = 'test Name'+i;
            course.hed__Account__c = pt.id;
            course.Status__c='Active';
            course.RecordTypeId=Schema.SObjectType.hed__Course__c.getRecordTypeInfosByName().get('RMIT Training').getRecordTypeId();
            courseList.add(course);
            
        }
    }   
	return courseList;
}
public static List<hed__Course_Offering__c> testFullLoadCooferList(hed__Term__c term1,List<hed__Course__c> allCourseList)
{
    List<hed__Course_Offering__c> cooferList = new List<hed__Course_Offering__c>();
    
    getTillOneHundredCooferList(term1,allCourseList[0],cooferList);
    getTillOneFiftyCooferList(term1,allCourseList[3],cooferList);
    getTillFourFiftyCooferList(term1,allCourseList[8],cooferList);
    getTillSixTwoFiveCooferList(term1,allCourseList[22],cooferList);
    getTillSevenHundredCooferList(term1,allCourseList[40],cooferList);
    
	return cooferList;
}
private static void getTillSevenHundredCooferList(hed__Term__c term1,hed__Course__c coofer,List<hed__Course_Offering__c> cooferList)
{	 
	for(Integer i=625;i<700;i++){                    
            hed__Course_Offering__c crsoffer = new hed__Course_Offering__c();
            crsoffer.name = 'test courseoffer'+i;
            crsoffer.hed__Course__c = coofer.Id;
            crsoffer.hed__Term__c = term1.id;
            crsoffer.hed__Start_Date__c=Date.newInstance(2018,05,03);
            crsoffer.hed__End_Date__c=Date.newInstance(2018,06,03);
            cooferList.add(crsoffer);    
    }
}
private static void getTillSixTwoFiveCooferList(hed__Term__c term1,hed__Course__c coofer,List<hed__Course_Offering__c> cooferList)
{	 
	for(Integer i=450;i<625;i++){                    
            hed__Course_Offering__c crsoffer = new hed__Course_Offering__c();
            crsoffer.name = 'test courseoffer'+i;
            crsoffer.hed__Course__c = coofer.Id;
            crsoffer.hed__Term__c = term1.id;
            crsoffer.hed__Start_Date__c=Date.newInstance(2018,05,03);
            crsoffer.hed__End_Date__c=Date.newInstance(2018,06,03);
            cooferList.add(crsoffer);         
    }
}
private static void getTillFourFiftyCooferList(hed__Term__c term1,hed__Course__c coofer,List<hed__Course_Offering__c> cooferList)
{	 
	for(Integer i=150;i<450;i++){                    
            hed__Course_Offering__c crsoffer = new hed__Course_Offering__c();
            crsoffer.name = 'test courseoffer'+i;
            crsoffer.hed__Course__c = coofer.Id;
            crsoffer.hed__Term__c = term1.id;
            crsoffer.hed__Start_Date__c=Date.newInstance(2018,05,03);
            crsoffer.hed__End_Date__c=Date.newInstance(2018,06,03);
            cooferList.add(crsoffer);         
    }
}
private static void getTillOneFiftyCooferList(hed__Term__c term1,hed__Course__c coofer,List<hed__Course_Offering__c> cooferList)
{
	for(Integer i=100;i<150;i++){                    
            hed__Course_Offering__c crsoffer = new hed__Course_Offering__c();
            crsoffer.name = 'test courseoffer'+i;
            crsoffer.hed__Course__c = coofer.Id;
            crsoffer.hed__Term__c = term1.id;
            crsoffer.hed__Start_Date__c=Date.newInstance(2018,05,03);
            crsoffer.hed__End_Date__c=Date.newInstance(2018,06,03);
            cooferList.add(crsoffer);           
    }
}
private static void getTillOneHundredCooferList(hed__Term__c term1,hed__Course__c coofer,List<hed__Course_Offering__c> cooferList)
{
	for(Integer i=0;i<100;i++){        
            hed__Course_Offering__c crsoffer = new hed__Course_Offering__c();
            crsoffer.name = 'test courseoffer'+i;
            crsoffer.hed__Course__c = coofer.Id;
            crsoffer.hed__Term__c = term1.id;
            crsoffer.hed__Start_Date__c=Date.newInstance(2018,05,03);
            crsoffer.hed__End_Date__c=Date.newInstance(2018,06,03);
            cooferList.add(crsoffer);    
    }
}
private static List<Course_Testimonial__c> testFullLoadCTList(List<hed__Course__c> allCourseList)
{
    List<Course_Testimonial__c> ctList = new List<Course_Testimonial__c>();
    for(Integer i=0;i<=50;i++){
        if(i<=20){
            Course_Testimonial__c ct = new Course_Testimonial__c();
            ct.Name__c = 'test ct'+i;
            ct.Course__c=allCourseList[10].Id;
            ctList.add(ct);
            i++;
        }else{
            Course_Testimonial__c ct = new Course_Testimonial__c();
            ct.Name__c = 'test ct'+i;
            ct.Course__c=allCourseList[30].Id;
            ctList.add(ct);
            i++;
        }
    }
	return ctList;
}
private static List<Account> testFullLoadProgramList(Account parent,Id recordTypeId)
{
List<Account> progList = new List<Account>();
    //Creating program
    for(Integer i=0;i<=10;i++){
        Account acc = new Account();
        acc.Name='Test Program'+i;
        acc.RecordTypeId=recordTypeId;
        acc.ParentId=parent.Id;
        progList.add(acc);
    }
   
	return progList;
}
private static Set<String> testFullLoadPPlanList(List<Account> allProgList)
{
    List<hed__Program_Plan__c> pplanList = new List<hed__Program_Plan__c>();
    for(Integer i=0;i<=10;i++){
        hed__Program_Plan__c plan = new hed__Program_Plan__c();
        plan.Name ='test Plan'+i;
        plan.hed__Account__c=allProgList[i].Id;
        pplanList.add(plan);
    }
    Insert pplanList;
	Set<String> pplannameSet = new Set<String>();
    for(hed__Program_Plan__c pplan : pplanList){
        pplannameSet.add(pplan.Name);
    }
	return pplannameSet;
}
private static void testFullLoadAllProgPlanList(Set<String> pplannameSet,List<hed__Course__c> allCourseList)
{
    List<hed__Program_Plan__c> allProgPlanList = [SELECT Id FROM hed__Program_Plan__c WHERE Name IN :pplannameSet];
    
    List<hed__Plan_Requirement__c> preqList = new List<hed__Plan_Requirement__c>();
    for(Integer i=0;i<=50;i++){
        if(i<=10){
            hed__Plan_Requirement__c planRequirement = new hed__Plan_Requirement__c();
            planRequirement.Name ='test Plan Requirement'+i;
            planRequirement.hed__Program_Plan__c = allProgPlanList[2].Id;
            planRequirement.hed__Course__c = allCourseList[i].Id;
            planRequirement.hed__Sequence__c = i;
            preqList.add(planRequirement);
        }else{
            hed__Plan_Requirement__c planRequirement = new hed__Plan_Requirement__c();
            planRequirement.Name ='test Plan Requirement'+i;
            planRequirement.hed__Program_Plan__c = allProgPlanList[6].Id;
            planRequirement.hed__Course__c = allCourseList[i].Id;
            planRequirement.hed__Sequence__c = i;
            preqList.add(planRequirement);
        }
    }
    Insert preqList;
}
private static void testFullLoadCreateRecords(List<hed__Course_Offering__c> cooferList,List<Account> allProgList, List<hed__Course__c> allCourseList)
{
    //Creating CourseOfferingSession
    Course_Offering_Session__c courseOfferingSession = new Course_Offering_Session__c();
    courseOfferingSession.Course_Offering__c = cooferList[10].Id;
    insert courseOfferingSession;
    
    // Creating Program testimonial
    Program_Testimonial__c programTestimonial = new Program_Testimonial__c();
    programTestimonial.Name = 'programtestimonial';
    programTestimonial.Program__c = allProgList[2].Id;
    insert programTestimonial;
    
    // Creating Course Relationship
    Course_Relationship__c courseRelationship = new Course_Relationship__c();
    courseRelationship.Course__c = allCourseList[50].Id;
    courseRelationship.Related_Course__c = allCourseList[10].Id;
    courseRelationship.Status__c = 'Active';
    insert courseRelationship;
    
    
}
private static void testFullLoadCreateProgramIndustryPartner(List<Account> allProgList,Account accountRec)
{
	// Creating Program Industry Partner
    Program_Industry_Partner__c  programIndustryPartner = new Program_Industry_Partner__c();
    programIndustryPartner.Program__c = allProgList[2].Id;
    programIndustryPartner.Industry_Partner__c = accountRec.Id;
    insert programIndustryPartner;    
}
public static void testMyDashboard()
    {
        Account accAdministrative = TestUtility.createTestAccount(true
                                                                  , 'Administrative Account 001'
                                                                  , Schema.SObjectType.Account.getRecordTypeInfosByName().get('Administrative').getRecordTypeId());
        Profile p = [SELECT Id FROM Profile WHERE Name='Customer Community Login User - RMIT']; 
        // Create contact record.
        List<String> lstParams = new List<String>{'Test', 'Con'};
        Contact conObj = TestUtility.createTestContact(true
                                                       , lstParams
                                                       , accAdministrative.Id);
        
        User u = TestUtility.createUser('Partner Community User', false);
        u.ContactId = conObj.Id;
        u.Username = 'communityuser@unittest.com';
        u.ProfileId = p.Id;
        insert u;
        
        /*User u = [Select id, ContactId from User where id =: UserInfo.getUserId() LIMIT 1];       
Contact conObj = new Contact(LastName='TestCon',Student_ID__c='123',Email='mail@mail.com');
insert conObj;*/
        
        Account a = new Account(name = 'test Account');
        insert a;
        
        Id courseRecordTypeId = Schema.SObjectType.hed__Course__c.getRecordTypeInfosByName().get('RMIT Online').getRecordTypeId();  
        hed__Course__c course = new hed__Course__c(status__c = 'enabled', Name = 'Test RMITOnline Course', hed__Account__c = a.id,RecordTypeId=courseRecordTypeId);
        insert course;
        hed__Course__c course1 = new hed__Course__c(status__c = 'enabled', Name = 'Test RMITOnline Course1', hed__Account__c = a.id,RecordTypeId=courseRecordTypeId);
        insert course1;
        hed__Course__c course2 = new hed__Course__c(status__c = 'enabled', Name = 'Test RMITOnline Course2', hed__Account__c = a.id,RecordTypeId=courseRecordTypeId);
        insert course2;
        hed__Course__c course3 = new hed__Course__c(status__c = 'enabled', Name = 'Test RMITOnline Course3', hed__Account__c = a.id,RecordTypeId=courseRecordTypeId);
        insert course3;
        
        //Creating Parent Account.
        Account parent = new Account(name = 'RMIT Online');
        insert parent;        
        //Creating program
        Id recordTypeId = Schema.SObjectType.Account.getRecordTypeInfosByName().get('Academic Program').getRecordTypeId();
        Account program = new Account(Name='Test Program',RecordTypeId=recordTypeId,ParentId=parent.Id);
        Insert program;
        //Creating Program Plan. 
        hed__Program_Plan__c plan = new hed__Program_Plan__c();
        plan.Name ='test Plan';
        plan.hed__Account__c=program.Id;  
        plan.hed__Is_Primary__c = true;      
        insert plan;
        
        //Creating Plan Requirement.
        List<hed__Plan_Requirement__c> planReqList = new List<hed__Plan_Requirement__c>();
        hed__Plan_Requirement__c planRequirement1 = new hed__Plan_Requirement__c();
        planRequirement1.Name ='test Plan Requirement';
        planRequirement1.hed__Program_Plan__c = plan.Id;
        planRequirement1.hed__Course__c = course.Id;
        planRequirement1.hed__Sequence__c = 1.00;
        planReqList.add(planRequirement1);
        
        hed__Plan_Requirement__c planRequirement2 = new hed__Plan_Requirement__c();
        planRequirement2.Name ='test Plan Requirement2';
        planRequirement2.hed__Program_Plan__c = plan.Id;
        planRequirement2.hed__Course__c = course2.Id;
        planRequirement2.hed__Sequence__c = 2.00;
        planReqList.add(planRequirement2);
        
        hed__Plan_Requirement__c planRequirement3 = new hed__Plan_Requirement__c();
        planRequirement3.Name ='test Plan Requirement3';
        planRequirement3.hed__Program_Plan__c = plan.Id;
        planRequirement3.hed__Course__c = course3.Id;
        planRequirement3.hed__Sequence__c = 3.00;
        planReqList.add(planRequirement3);
        
        hed__Plan_Requirement__c planRequirement4 = new hed__Plan_Requirement__c();
        planRequirement4.Name ='test Plan Requirement';
        planRequirement4.hed__Program_Plan__c = plan.Id;
        planRequirement4.hed__Course__c = course.Id;
        planRequirement4.hed__Sequence__c = 1.00;
        planReqList.add(planRequirement4);
        
        insert planReqList;
        
        //Creating Order test data
        csord__Order_Request__c req= new csord__Order_Request__c(Name='testOrder',csord__Module_Name__c='ModuleTest',csord__Module_Version__c='10',csord__Process_Status__c='requested',csord__Request_DateTime__c=System.now());
        insert req;
        
        //Creating Subscription test data
        csord__Subscription__c sub= new csord__Subscription__c(name= 'SubTest', csord__Identification__c='testIdentification',csord__Order_Request__c=Req.id); 
        insert sub;
        
        //Creating Product Configuration Test Data
        cscfga__Product_Configuration__c prodconfig = new cscfga__Product_Configuration__c();
        prodconfig.Name='ABCD Test';
        prodconfig.cscfga__Quantity__c=1;       
        insert prodconfig;
        
        hed__Term__c term = new hed__Term__c(name = 'test term', hed__Account__c = a.id);
        insert term;
        hed__Term__c term1 = new hed__Term__c(name = 'test term1', hed__Account__c = a.id);
        insert term1;
        
        hed__Course_Offering__c crsoffer = new hed__Course_Offering__c(name = 'test course offering 1', hed__Course__c = course1.id, hed__Term__c = term.id,
                                                                       hed__Start_Date__c=Date.newInstance(2018,05,03),hed__End_Date__c=Date.newInstance(2018,06,03),Last_Day_to_Add__c=Date.newInstance(2018,10,03));
        insert crsoffer;
        
        hed__Course_Offering__c crsoffer1 = new hed__Course_Offering__c(name = 'test course offering 2', hed__Course__c = course.id, hed__Term__c = term.id,
                                                                        hed__Start_Date__c=Date.newInstance(2018,05,03),hed__End_Date__c=Date.newInstance(2018,06,03),Last_Day_to_Add__c=Date.newInstance(2018,10,03));
        insert crsoffer1;
        
        hed__Course_Offering__c crsoffer2 = new hed__Course_Offering__c(name = 'test course offering 3', hed__Course__c = course3.id, hed__Term__c = term.id,
                                                                        hed__Start_Date__c=Date.newInstance(2018,05,03),hed__End_Date__c=Date.newInstance(2018,06,03),Last_Day_to_Add__c=Date.newInstance(2018,10,03));
        insert crsoffer2;
        
        hed__Course_Offering__c crsoffer3 = new hed__Course_Offering__c(name = 'test course offering 4', hed__Course__c = course.id, hed__Term__c = term.id,
                                                                        hed__Start_Date__c=Date.newInstance(2018,05,03),hed__End_Date__c=Date.newInstance(2018,06,03),Last_Day_to_Add__c=Date.newInstance(2018,10,03));
        insert crsoffer3;
        
        //Creating Opportunity
        Opportunity opp = new Opportunity();
        opp.StageName = 'open';
        opp.name = 'Test Opp';
        opp.CloseDate = Date.today();
        Insert opp;
        
        //Creating Product Basket test data
        cscfga__Product_Basket__c prodbasket = new cscfga__Product_Basket__c ();
        prodbasket.Name = 'New Basket';
        prodbasket.cscfga__Total_Price__c = 100.00;
        prodbasket.cscfga__Opportunity__c = opp.Id;
        Insert prodbasket;
        
        CSPOFA__Orchestration_Process_Template__c tempRec = new CSPOFA__Orchestration_Process_Template__c();
        tempRec.Name = 'Course Enrollment Workflow';
        Insert tempRec;
        
        csord__Service__c servic= new csord__Service__c();
        servic.Name='service1';
        servic.csord__Identification__c='TestIdentify';
        servic.csordtelcoa__Main_Contact__c = conObj.id;
        servic.csord__Subscription__c= sub.id;
        servic.csord__Order_Request__c=Req.id;
        servic.csord__Status__c='StatusRequest';
        servic.csordtelcoa__Product_Configuration__c=prodconfig.id;
        servic.csordtelcoa__Product_Basket__c = prodbasket.Id;
        servic.Plan__c = plan.Id;
        servic.Course_Program__c = 'Program';
        servic.Course_Offering__c = crsoffer.Id;
        insert servic;
        
        //Creating Program Enrollment record
        hed__Program_Enrollment__c pgnEnrollObj = new hed__Program_Enrollment__c();
        pgnEnrollObj.hed__Contact__c = conObj.id;
        pgnEnrollObj.hed__Account__c = program.id;
        pgnEnrollObj.hed__Program_Plan__c = plan.id;
        pgnEnrollObj.Service__c = servic.id;
        insert pgnEnrollObj;
        
        //Creating Course conneciton record
        List<hed__Course_Enrollment__c> ceList = new List<hed__Course_Enrollment__c>();
        hed__Course_Enrollment__c cnr1 = new hed__Course_Enrollment__c();
        cnr1.hed__Contact__c = conObj.id;
        cnr1.hed__Course_Offering__c=crsoffer2.id;
        cnr1.Enrolment_Status__c = 'E';
        cnr1.hed__Status__c = 'Former';
        cnr1.Source_Type__c = 'MKT';
        cnr1.Service__c = servic.Id;
        ceList.add(cnr1);
        
        hed__Course_Enrollment__c cnr2 = new hed__Course_Enrollment__c();
        cnr2.hed__Contact__c=conObj.id;
        cnr2.hed__Course_Offering__c=crsoffer1.id; 
        cnr2.hed__Program_Enrollment__c=pgnEnrollObj.id;      
        cnr2.Enrolment_Status__c = 'E'; 
        cnr2.hed__Status__c = 'Current';
        cnr2.Source_Type__c = 'MKT';
        cnr2.Service__c = servic.Id;
        ceList.add(cnr2);
        
        hed__Course_Enrollment__c cnr4 = new hed__Course_Enrollment__c();
        cnr4.hed__Contact__c=conObj.id;
        cnr4.hed__Course_Offering__c=crsoffer3.id; 
        cnr4.hed__Program_Enrollment__c=pgnEnrollObj.id;     
        cnr4.Enrolment_Status__c = 'E';
        cnr4.hed__Status__c = 'Assessment Complete';
        cnr4.Source_Type__c = 'MKT';
        cnr4.Service__c = servic.Id;
        ceList.add(cnr4);
        
        Insert ceList;
        
    }
 
}