/*
  ******************************************************************************************************
  * @author       :Resmi Ramakrishnan
  * @date         : 08/06/2018 
  * @description  : TestClass for IntegrationLogWrapper class
  ******************************************************************************************************
*/

@isTest
public class IntegrationLogWrapperTest
{
     static testMethod void testCourseConnectionLifeCycleObjCreation()
     {
         Integration_Logs__c testIntLogObj = new Integration_Logs__c();
         testIntLogObj = IntegrationLogWrapper.createIntegrationLog('SAMS_IPaaS','JSONRequestString', 'Outbound Service','',false);
         system.assert(testIntLogObj != null);
     }

}