@isTest
public with sharing class TestResearcherPortalNotificationLoader {
    private static final String CONTENT_TYPE = 'TestContentType';
    private static final String CONTENT_NAME = 'TestContent';
    private static final String SITE_NAME = 'TestSite';
    
    static cms__Content__c[] testContents;
    
    static void buildTestContent() {
        // Normally we wouldn't touch any cms__ objects, even in a test
        // Here we need a valid cms__Content__c ID to insert an OCMS_Search__c record
        cms__Content__c testContent = new cms__Content__c(
                cms__Name__c = CONTENT_NAME,
                cms__Site_Name__c = SITE_NAME
        );

        insert testContent;

        testContents = new cms__Content__c[] { testContent };

        OCMS_Search__c ocmsSearch = new OCMS_Search__c(
                Content__c = testContent.Id,
                Importance__c = 50
        );

        insert ocmsSearch;
    }

    static RenderResultBundle.RenderedContent buildRenderedContent(Id originId) {
        RenderResultBundle.RenderedContent renderedContent = new RenderResultBundle.RenderedContent();
        renderedContent.originId = originId;
        renderedContent.renderMap = new Map<String, String> {
                'JsonDataTemplate' => JSON.serialize(buildContentBundle(originId))
        };
        renderedContent.socialBundle = new SocialBundle();
        renderedContent.socialBundle.socialActivity = new SocialActivity();
        renderedContent.socialBundle.socialActivity.viewedByMe = false;

        return renderedContent;
    }

    static ContentBundle buildContentBundle(Id originId) {
        // ResearcherPortalOcmsSearchLoader only uses bundle.originId
        ContentBundle bundle = new ContentBundle();
        bundle.originId = originId;
        bundle.contentAttributes = new Map<String, List<AttributeBundle.ContentAttribute>>();
        bundle.contentAttributes.put('en_US', new List<AttributeBundle.ContentAttribute>());
        return bundle;
    }

    static RenderResultBundle buildRenderResultBundle() {
        RenderResultBundle bundle = new RenderResultBundle();
        bundle.renderings = new RenderResultBundle.RenderedContent[] {};
        for (cms__Content__c testContent : testContents) {
            bundle.renderings.add(buildRenderedContent(testContent.Id));
        }
        return bundle;
    }

    static JsonMessage.ApiResponse buildApiResponse(Object responseObject) {
        JsonMessage.ApiResponse response = new JsonMessage.ApiResponse();
        response.isSuccess = true;
        response.responseObject = Json.serialize(responseObject);
        return response;
    }

    static testmethod void testGetContent() {
        buildTestContent();

        ApiRequest req = new ApiRequest();
        JsonMessage.ApiResponse response = buildApiResponse(buildRenderResultBundle());
        cms.ServiceEndPoint.addMockResponse('OrchestraRenderingAPI', Json.serialize(response));

        ResearcherPortalNotificationLoader loader = new ResearcherPortalNotificationLoader();

        String bundleListJson = loader.getContent(JSON.serialize(req));
        ContentBundleList bundleList = (ContentBundleList)JSON.deserialize(bundleListJson, ContentBundleList.class);

        System.assertEquals(testContents.size(), bundleList.contentBundles.size());
    }
}