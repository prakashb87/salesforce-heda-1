/*****************************************************************
Name: CreateTransactionFileController 
Author: Capgemini 
Purpose: To create daily transaction file 
JIRA Reference : ECB-4356 :  Daily transaction text file consumable by SAP integration
                 ECB-4418: Build: SAP file for posting revenue from Course Transaction object
                 ECB-4493: Include only Revenue transactions                 
*****************************************************************/
/*==================================================================
History 
--------
Version   Author            Date              Detail
1.0       Shreya            09/08/2018         Initial Version
********************************************************************/
public without sharing class CreateTransactionFileController {

    public String batchInputInterfaceType {get;set;}
    public String interfaceKey {get;set;}
    public String client {get;set;}
    public Integer currentBatchNumber {get;set;}
    public String fileHeader {get;set;}
    public String fileTrailor {get;set;}
    public List<documentLineItemWrapper> documentWrapperList{get;set;}
    // ECB-4493 doNotPost
    public String doNotPost {get;set;}
    public Integer fileLineCount;
    
    public void getFileParameters(){
        
        // Note: Modifications will be needed later on to include other Record Types when the courses will be available in the market.
        Id rmitOnlineCourseRecordTypeId = Schema.SObjectType.hed__Course__c.getRecordTypeInfosByName().get('RMIT Online').getRecordTypeId();
        Id rmitTrainingCourseRecordTypeId = Schema.SObjectType.hed__Course__c.getRecordTypeInfosByName().get('RMIT Training').getRecordTypeId();
        
        // ECB-4493 Get Revenue RecordTypeID for Course Transaction. Get doNotPost value from Custom Setting
        Id revenueRecordTypeId = Schema.SObjectType.Course_Transaction__c.getRecordTypeInfosByName().get('Revenue').getRecordTypeId();
        doNotPost = ConfigDataMapController.getCustomSettingValue('DoNotPost'); 
        
        batchInputInterfaceType = ConfigDataMapController.getCustomSettingValue('BatchInputInterfaceRecordType'); 
        
        interfaceKey = ConfigDataMapController.getCustomSettingValue('InterfaceKey'); 
        
        client = ConfigDataMapController.getCustomSettingValue('Client'); 
        
        Config_Data_Map__c configRec = Config_Data_Map__c.getInstance('BatchNumber');
        currentBatchNumber = Integer.valueOf(configRec.Config_Value__c)+1;
        configRec.Config_Value__c = String.valueOf(currentBatchNumber);
        
        try{
            update configRec;
        }catch(DMLException ex){
            system.debug('Exception:::'+ex);
        }
       
       String recordEnd = '/'; 
       
      // Creating the file header string 
       fileHeader = batchInputInterfaceType+interfaceKey+client+String.valueOf(currentBatchNumber).leftpad(5,'0')+recordEnd.leftpad(22,' ')+'\r\n';
       
       //Querying the course transaction records
       // ECB-4493 Added conditions for Record Type and Post Action
       //ECB-4676 Fixed:Pick up all unposted transaction that can be recognised for revenue
       List<Course_Transaction__c> courseTransacList = [SELECT Census_Date__c,Company_Code__c,Contact__c,Contact__r.Name,Course_Connection__c,Discount_Amount__c,GL_Code__c,GST_Amount__c,IO_Code__c,List_Price__c,
                                                       Net_Amount__c,Post_To_SAP__c,Receipt_Number__c,SAP_File_Name__c,Transaction_Date__c,Course_Connection__r.hed__Course_Offering__r.hed__Course__r.RecordTypeId,  
                                                       CreatedDate 
                                                       FROM Course_Transaction__c 
                                                       WHERE Post_To_SAP__c = false 
                                                       AND (Census_Date__c = null OR Census_Date__c <= TODAY) 
                                                       AND RecordTypeId = :revenueRecordTypeId AND Post_Action__c != :doNotPost
                                                       AND (Net_Amount__c!= 0.00 AND Net_Amount__c!=null)
                                                       AND (GL_Code__c!=null AND GL_Code__c!='')
                                                       AND (IO_Code__c!=null AND IO_Code__c!='')
                                                       AND (Company_Code__c!=null AND Company_Code__c!='')];
       
       system.debug('courseTransacList ::::'+courseTransacList);                                      
   
       fileLineCount = 1;
       documentWrapperList = new List<documentLineItemWrapper>();
       
       if(!courseTransacList.isEmpty()){
           for(Course_Transaction__c ct : courseTransacList){
               
               /* String monthVal;
                String dayVal;  
                String yearVal;              
                
                if(ct.Census_Date__c == null){
                   monthVal = ct.CreatedDate.month() + 1 > 10 ? String.valueOf(ct.CreatedDate.month()) : '0'+ct.CreatedDate.month() ;
                   dayVal = ct.CreatedDate.day() + 1 > 10 ? String.valueOf(ct.CreatedDate.day()) : '0'+ct.CreatedDate.day() ;
                   yearVal = String.valueOf(ct.CreatedDate.year());
                }else{
                   monthVal = ct.Census_Date__c.month() + 1 > 10 ? String.valueOf(ct.Census_Date__c.month()) : '0'+ct.Census_Date__c.month() ;
                   dayVal = ct.Census_Date__c.day() + 1 > 10 ? String.valueOf(ct.Census_Date__c.day()) : '0'+ct.Census_Date__c.day() ;
                   yearVal = String.valueOf(ct.Census_Date__c.year());
                }*/
                
                //ECB-4676: Fixed date in document header.All transactions in the file should have document date = posting date:Shreya
               /* String monthVal = (System.TODAY()-1).month() + 1 > 10 ? String.valueOf((System.TODAY()-1).month()) : '0'+(System.TODAY()-1).month() ;
                String dayVal = (System.TODAY()-1).day() + 1 > 10 ? String.valueOf((System.TODAY()-1).day()) : '0'+(System.TODAY()-1).day() ;             
               
               String dateVal = dayVal+monthVal+(System.TODAY()-1).YEAR();*/
               //ECB-4743
               String monthVal = (System.TODAY()).month() + 1 > 10 ? String.valueOf((System.TODAY()).month()) : '0'+(System.TODAY()).month() ;
               String dayVal = (System.TODAY()).day() + 1 > 10 ? String.valueOf((System.TODAY()).day()) : '0'+(System.TODAY()).day() ;             
               
               String dateVal = (System.TODAY()).YEAR()+monthVal+dayVal;
               String dateValHeader = dayVal+monthVal+(System.TODAY()).YEAR();
               
               String receiptNo;
               if(String.isNotBlank(ct.Receipt_Number__c)){
                   receiptNo = ct.Receipt_Number__c;
               }else{
                   receiptNo = ' ';
               }
               
               String conName ;
               if(String.isNotBlank(ct.Contact__r.Name)){
                  conName = ct.Contact__r.Name ;
               }else{
                  conName = ' ';
               }
               //freeText = freeText+prductBasketReceiptNoMap.get(ser.csordtelcoa__Product_Basket__c);
               String freeText = (receiptNo +' '+conName).trim();
               
               String glCode;
               if(String.isNotBlank(ct.GL_Code__c)){
                   glCode = ct.GL_Code__c;
               }else{
                   glCode = ' ';
               }
   
               String ioCode;
               if(String.isNotBlank(ct.IO_Code__c)){
                   ioCode = ct.IO_Code__c;
               }else{
                   ioCode = ' ';
               }
               
               String netAmount;
               if(ct.List_Price__c!=null && ct.GST_Amount__c!=null && ct.GST_Amount__c!=0){
                  netAmount = getNetAmount(ct.List_Price__c.setScale(2));
               }else if(ct.List_Price__c!=null && (ct.GST_Amount__c==null || ct.GST_Amount__c==0)){
                  netAmount = String.valueOf(ct.List_Price__c.setScale(2)); 
               }else{
                 netAmount = ' ';
               }
   
               String netDiscountAmount;
               if(ct.Discount_Amount__c!=null && ct.GST_Amount__c!=null && ct.GST_Amount__c!=0){
                   netDiscountAmount = getNetAmount(ct.Discount_Amount__c.setScale(2));
               }else if(ct.Discount_Amount__c!=null && (ct.GST_Amount__c==null || ct.GST_Amount__c==0)){
                  netDiscountAmount = String.valueOf(ct.Discount_Amount__c.setScale(2)); 
               }else{
                   netDiscountAmount = ' ';
               }
                              
               String gstAmount;
               if(ct.List_Price__c!=null && ct.GST_Amount__c!=null && ct.GST_Amount__c!= 0){
                   gstAmount = getGSTAmount(ct.List_Price__c.setScale(2));
               }else{
                   gstAmount = ' ';
               }
   
               String discountGstAmount;
               if(ct.Discount_Amount__c!=null && ct.GST_Amount__c!=null && ct.GST_Amount__c!=0){
                   discountGstAmount = getGSTAmount(ct.Discount_Amount__c.setScale(2));
               }else{
                   discountGstAmount = ' ';
               }
       
               String taxCode;
               if(ct.GST_Amount__c!=null && ct.GST_Amount__c > 0){
                    taxCode = ConfigDataMapController.getCustomSettingValue('GST10%TaxCode');
               }else{
                    taxCode = ConfigDataMapController.getCustomSettingValue('GSTFreeTaxCode');
               }
   
              String totalAmount;
              if(ct.List_Price__c!=null && ct.Discount_Amount__c!=null){
                    totalAmount = getTotalAmount(ct.List_Price__c.setScale(2),ct.Discount_Amount__c.setScale(2));
              }else if(ct.List_Price__c!=null && ct.Discount_Amount__c==null){
                    totalAmount = String.valueOf(ct.List_Price__c.setScale(2));
              }else{
                    totalAmount = ' ';
              }
               
              Boolean rmitOCourse = false; 
              Boolean rmitTrainCourse = false;
              if(ct.Course_Connection__r.hed__Course_Offering__r.hed__Course__r.RecordTypeId == rmitOnlineCourseRecordTypeId){
                    rmitOCourse = true;                                
                    fileLineCount++;
               }else if(ct.Course_Connection__r.hed__Course_Offering__r.hed__Course__r.RecordTypeId == rmitTrainingCourseRecordTypeId){
                    rmitTrainCourse = true;                                
                    fileLineCount++;
               }
            
              documentLineItemWrapper lineItemObj = new documentLineItemWrapper(); 
             //calling a function to create the document line item wrapper
             lineItemObj = createDocLineItemWrapper(receiptNo,dateVal,freeText,glCode,ioCode,netAmount,netDiscountAmount,dateValHeader,gstAmount,discountGstAmount,taxCode,totalAmount,rmitOCourse,rmitTrainCourse,conName,recordEnd);
             documentWrapperList.add(lineItemObj);
              
              fileLineCount = fileLineCount+4;      
                                            
              ct.Post_To_SAP__c = true;
              //ECB-4676:Fixed-File Name
              //ct.SAP_File_Name__c = ConfigDataMapController.getCustomSettingValue('SAPFileName')+(System.TODAY()-1).YEAR()+monthVal+dayVal+'.txt';
              ct.SAP_File_Name__c = ConfigDataMapController.getCustomSettingValue('SAPFileName')+dateVal+'.txt';                                
           } 
        }
        
       fileLineCount = fileLineCount+1;
       //creating the file trailor string
       fileTrailor = ConfigDataMapController.getCustomSettingValue('FileTrailorBatchInputRType')+String.valueOf(fileLineCount).leftpad(5,'0')+recordEnd+'\r\n';
       
       try{
           Database.Update(courseTransacList,false);
       }catch(DMLException ex){
           system.debug('Exception:::'+ex);
       }
    
    }
    
    /********************************************************************
    // Purpose              : Get the net List pice/Discount amount after gst split                                   
    // Author               : Capgemini [Shreya]
    // Parameters           : Decimal listPrice
    //  Returns             : String 
   
    //********************************************************************/
    public String getNetAmount(Decimal priceVal){
        String netAmount;
        try{
            Decimal netAmt = (priceVal/1.1).setScale(2);
            netAmount = String.valueOf(netAmt);
        }catch(Exception ex){
            system.debug('Exception:::'+ex);
        }
        return netAmount;
    }
    
    /********************************************************************
    // Purpose              : Get the gst split amount on List price/Discount amount                                  
    // Author               : Capgemini [Shreya]
    // Parameters           : Decimal listPrice
    //  Returns             : String 
   
    //********************************************************************/
    public String getGSTAmount(Decimal priceVal){
        String gstAmount;
        try{
            Decimal gstAmt = (priceVal - (priceVal/1.1)).setScale(2);
            gstAmount = String.valueOf(gstAmt);
        }catch(Exception ex){
            system.debug('Exception:::'+ex);
        }
        return gstAmount;
    }
    
     /********************************************************************
    // Purpose              : Get the total amount                                    
    // Author               : Capgemini [Shreya]
    // Parameters           : Decimal listPrice,Decimal discountAmount
    //  Returns             : String 
   
    //********************************************************************/
    
    public String getTotalAmount(Decimal listPrice,Decimal discountAmount){
        String totalAmount;
        try{
            Decimal totalAmt = (listPrice - discountAmount).setScale(2);
            totalAmount = String.valueOf(totalAmt);
        }catch(Exception ex){
            system.debug('Exception:::'+ex);
        }
        return totalAmount;
    }
    
     /********************************************************************
    // Purpose              : Create the Document LineItem Wrapper                                    
    // Author               : Capgemini [Shreya]
    // Parameters           : String receiptNo,String dateVal,String freeText,String glCode,String ioCode,String netAmount,String netDiscountAmount,
                              String gstAmount,String discountGstAmount,String taxCode,String totalAmount,Boolean rmitOCourse
    //  Returns             : documentLineItemWrapper 
   
    //********************************************************************/
    
      public documentLineItemWrapper createDocLineItemWrapper(String receiptNo,String dateVal,String freeText,String glCode,String ioCode,String netAmount,String netDiscountAmount,String dateValHeader,
                                                             String gstAmount,String discountGstAmount,String taxCode,String totalAmount,Boolean rmitOCourse,Boolean rmitTrainCourse,String conName,String recordEnd){
        
        documentLineItemWrapper lineItemObj = new documentLineItemWrapper();  
         //Creating the document header
        lineItemObj.documentHeader = ConfigDataMapController.getCustomSettingValue('DHeaderBatchInputRType')
                                    +ConfigDataMapController.getCustomSettingValue('TransactionCode').rightpad(20,' ')
                                    +dateValHeader+ConfigDataMapController.getCustomSettingValue('DocumentType');
        if(rmitOCourse){
            lineItemObj.documentHeader = lineItemObj.documentHeader + ConfigDataMapController.getCustomSettingValue('RMITOCompanyCode');
        }else if(rmitTrainCourse){
            lineItemObj.documentHeader = lineItemObj.documentHeader + ConfigDataMapController.getCustomSettingValue('RMITTrainingCompanyCode');
        }else{
            lineItemObj.documentHeader = lineItemObj.documentHeader + ConfigDataMapController.getCustomSettingValue('RMITOCompanyCode');
        }
        lineItemObj.documentHeader = lineItemObj.documentHeader+dateValHeader
                                     +ConfigDataMapController.getCustomSettingValue('CurrencyKey').rightpad(15,' ')+receiptNo.rightpad(16,' ')
                                     +ConfigDataMapController.getCustomSettingValue('DocumentHeaderText').rightpad(25,' ')+recordEnd+'\r\n';                      
        if(rmitOCourse || rmitTrainCourse){
            lineItemObj.documentHeader='';
            fileLineCount=fileLineCount-1;
        }  
        String blankSpace = '';                              

         //creating the credit document line item
        lineItemObj.creditDocumentLineItem = ConfigDataMapController.getCustomSettingValue('DLineItemBatchInterfaceRType')
                                            +ConfigDataMapController.getCustomSettingValue('TableName').rightpad(30, ' ')
                                            +ConfigDataMapController.getCustomSettingValue('PostingKeyCredit')+glCode.rightpad(17,' ')                                                 
                                            +netAmount.leftpad(16,' ')+((gstAmount+taxCode).leftpad(18,' '))+blankSpace.rightpad(18,' ')+freeText.rightpad(50,' ')
                                            +ioCode.rightpad(40,' ')
                                            +recordEnd+'\r\n';
        
        if(rmitOCourse || rmitTrainCourse){
              lineItemObj.creditDocumentLineItem= ConfigDataMapController.getCustomSettingValue('DLineItemBatchInterfaceRType')
                                                  +ConfigDataMapController.getCustomSettingValue('TableName').rightpad(30, ' ')
                                                  +ConfigDataMapController.getCustomSettingValue('PostingKeyCredit')+glCode.rightpad(17,' ')                                                 
                                                  +netAmount.leftpad(16,' ')+((gstAmount+taxCode).leftpad(18,' '))+blankSpace.rightpad(18,' ')+freeText.rightpad(50,' ')
                                                  +ioCode.rightpad(40,' ')
                                                //  +ConfigDataMapController.getCustomSettingValue('RMITCompanyCode')
                                                  +recordEnd+'\r\n';
        } 

         if(netAmount==' '|| String.valueOf(netAmount)=='0.00'){
              lineItemObj.debitDocumentLineItem = '';
              fileLineCount=fileLineCount-1;

        }                                   
                                           
        //creating the discount document line item                                    
        lineItemObj.debitDocumentLineItem = ConfigDataMapController.getCustomSettingValue('DLineItemBatchInterfaceRType')
                                            +ConfigDataMapController.getCustomSettingValue('TableName').rightpad(30, ' ')
                                            +ConfigDataMapController.getCustomSettingValue('PostingKeyDebit')
                                            +ConfigDataMapController.getCustomSettingValue('DiscountAccountGL').rightpad(17,' ')                                                 
                                            +netDiscountAmount.leftpad(16,' ')+((discountGstAmount+taxCode).leftpad(18,' '))+blankSpace.rightpad(18,' ')
                                            +freeText.rightpad(50,' ');
        if(rmitOCourse || rmitTrainCourse){    
              lineItemObj.debitDocumentLineItem = lineItemObj.debitDocumentLineItem 
                                                  +ConfigDataMapController.getCustomSettingValue('RMITODiscountAccountIO').rightpad(40,' ');
                                                 // +ConfigDataMapController.getCustomSettingValue('RMITCompanyCode'); 
              lineItemObj.debitDocumentLineItem = lineItemObj.debitDocumentLineItem +recordEnd+'\r\n'; 

              

        }else{
            lineItemObj.debitDocumentLineItem = lineItemObj.debitDocumentLineItem + ioCode.rightpad(40,' ');
            lineItemObj.debitDocumentLineItem = lineItemObj.debitDocumentLineItem +recordEnd+'\r\n';         
        }
        
        if(netDiscountAmount==' '|| String.valueOf(netDiscountAmount)=='0.00'){
            lineItemObj.debitDocumentLineItem = '';
            fileLineCount=fileLineCount-1;

        }
        
        if(rmitOCourse){
            //creating the document header for RMITO course
            lineItemObj.liabilitydocumentHeader = ConfigDataMapController.getCustomSettingValue('DHeaderBatchInputRType')
                                                 +ConfigDataMapController.getCustomSettingValue('TransactionCode').rightpad(20,' ')
                                                 +dateValHeader+ConfigDataMapController.getCustomSettingValue('DocumentType')
                                                 +ConfigDataMapController.getCustomSettingValue('RMITOCompanyCode')
                                                 +dateValHeader
                                                 +ConfigDataMapController.getCustomSettingValue('CurrencyKey').rightpad(15,' ')+receiptNo.rightpad(16,' ')
                                                 +ConfigDataMapController.getCustomSettingValue('DocumentHeaderText').rightpad(25,' ')+recordEnd+'\r\n';   
        }else if(rmitTrainCourse){
            lineItemObj.liabilitydocumentHeader = ConfigDataMapController.getCustomSettingValue('DHeaderBatchInputRType')
                                                 +ConfigDataMapController.getCustomSettingValue('TransactionCode').rightpad(20,' ')
                                                 +dateValHeader+ConfigDataMapController.getCustomSettingValue('DocumentType')
                                                 +ConfigDataMapController.getCustomSettingValue('RMITTrainingCompanyCode')
                                                 +dateValHeader
                                                 +ConfigDataMapController.getCustomSettingValue('CurrencyKey').rightpad(15,' ')+receiptNo.rightpad(16,' ')
                                                 +ConfigDataMapController.getCustomSettingValue('DocumentHeaderText').rightpad(25,' ')+recordEnd+'\r\n';  
        }
        //creating the debit document line item
       
         /*lineItemObj.debitDiscountLineItem = ConfigDataMapController.getCustomSettingValue('DLineItemBatchInterfaceRType')
                                            +ConfigDataMapController.getCustomSettingValue('TableName').rightpad(30, ' ')
                                            +ConfigDataMapController.getCustomSettingValue('PostingKeyDebit')
                                            +ConfigDataMapController.getCustomSettingValue('ClearingAccount').rightpad(27,' ')
                                            +totalAmount.leftpad(16,' ')+receiptNo.leftpad(40,' ')+' '+conName.rightpad(75,' ')
                                            +ConfigDataMapController.getCustomSettingValue('RMITCompanyCode')+recordEnd+'\r\n';*/
                                            
         lineItemObj.debitDiscountLineItem = ConfigDataMapController.getCustomSettingValue('DLineItemBatchInterfaceRType')
                                            +ConfigDataMapController.getCustomSettingValue('TableName').rightpad(30, ' ')
                                            +ConfigDataMapController.getCustomSettingValue('PostingKeyDebit')
                                            +ConfigDataMapController.getCustomSettingValue('ClearingAccount').rightpad(17,' ')
                                            +totalAmount.leftpad(16,' ')+blankSpace.rightpad(36,' ')+(receiptNo+' '+conName).rightpad(86,' ')
                                            +ConfigDataMapController.getCustomSettingValue('RMITCompanyCode')+recordEnd+'\r\n';
                                            
        if(totalAmount==' '|| String.valueOf(totalAmount)=='0.00'){
            lineItemObj.debitDocumentLineItem = '';
            fileLineCount=fileLineCount-1;

        }                                  
      System.debug('lineItemObj '+lineItemObj);                                    
      return lineItemObj;
                              
    }
       
    
    //File Wrapper
    public class documentLineItemWrapper{
        public String creditDocumentLineItem {get;set;}
        public String debitDocumentLineItem {get;set;}
        public String debitDiscountLineItem {get;set;}
        public String liabilitydocumentHeader {get;set;}
        public String documentHeader {get;set;}
    }
}