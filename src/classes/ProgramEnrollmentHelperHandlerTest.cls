/*********************************************************************************
*** @TriggerTestClass  : ProgramEnrollmentHelperHandlerTest
*** @Author		    : P Ajay Kumar
*** @Requirement     	: To Test ProgramEnrollmentHelperHandler
*** @Created date    	: 1/8/2019
**********************************************************************************/
/************************************************************************************
*** @About Class
*** This class is a Test class for ProgramEnrollmentHelperHandler.It is Tested on two events
*** (after insert,after update) of ProgramEnrollment with student affiliation and a contact with different type of opportunities. 
***********************************************************************************/
@isTest
public class ProgramEnrollmentHelperHandlerTest {
    public static Id pEStudentRT = Schema.SObjectType.hed__Affiliation__c.RecordTypeInfosByName.get('Student').RecordTypeId;
    public static Id pEProspectRT = Schema.SObjectType.hed__Affiliation__c.RecordTypeInfosByName.get('Prospect').RecordTypeId;
    public static Id oppInquiryRT = Schema.SObjectType.Opportunity.RecordTypeInfosByName.get('Inquiry Opportunities').RecordTypeId;
    public static Id oppIntApplicantRT = Schema.SObjectType.Opportunity.RecordTypeInfosByName.get('International Applicant Opportunities').RecordTypeId;
    public static Id oppSLApplicantRT = Schema.SObjectType.Opportunity.RecordTypeInfosByName.get('School Leaver Applicant Opportunities').RecordTypeId;
    public static Id oppNonSLApplicantRT = Schema.SObjectType.Opportunity.getRecordTypeInfosByName().get('Non-School Leaver Applicant Opportunities').getRecordTypeId();
    //Testing all the four Events
    @testSetup
    static void testDataSetup(){
    	 List<hed__Trigger_Handler__c> trgH= InteractionTestDataFactory.createTriggerHandlerData();
          insert trgH;
    }
    
    @isTest static void pEInsertUpdateTest1() {
        Account acc = new Account(Name = 'Test Account');
        insert acc;
        Contact cnt = new Contact(FirstName = 'PE Cnt',
                                  LastName = 'Test Contact',
                                  Work_Email__c = 'test@email.com',
                                  Mobile_Phone__c='1234565436');
        insert cnt;
        hed__Affiliation__c affliliationProRec = new hed__Affiliation__c(hed__Contact__c = cnt.id,
                                                                         hed__Status__c = 'Current',
                                                                         hed__Role__c='Prospect',
                                                                         hed__Account__c = acc.id,
                                                                         RecordTypeId = pEProspectRT);
        insert affliliationProRec;
        Opportunity oppRec1 = new Opportunity(RecordTypeId = oppInquiryRT,
                                              Assign_To_Contact__c = cnt.Id,
                                              Segment__c = 'Non-School Leaver',
                                              StageName = 'Confirmed Interest',
                                              Inquiry_Date__c = date.today(),
                                              Level_of_Study__c = 'Bachelor degree',
                                              Admit_Term__c = '2140',
                                              CloseDate = date.today() + 1,
                                              Interest_Area__c = 'Architecture',
                                              Affiliation__c = affliliationProRec.Id,
                                              Name = cnt.Name + 2140);
        insert oppRec1;
        hed__Program_Enrollment__c progEnrolAppRec2 = new hed__Program_Enrollment__c(hed__Contact__c = cnt.Id,
                                                                                     Institution__c = acc.Id,
                                                                                     Admit_Term__c = '2150',
                                                                                     Program_Status__c = 'LA',
                                                                                     hed__Affiliation__c = affliliationProRec.Id);
        insert progEnrolAppRec2;
        progEnrolAppRec2.Enrolment_Date__c = date.today();
        update progEnrolAppRec2;
        String actual1 = [SELECT StageName FROM Opportunity WHERE Assign_To_Contact__c =: progEnrolAppRec2.hed__Contact__c].StageName;
        System.assertNotEquals('Enrolled', actual1, 'The opportunity was not enrolled.');
        
        String  actual2 = [SELECT hed__Status__c FROM hed__Affiliation__c WHERE hed__Contact__c =: progEnrolAppRec2.hed__Contact__c].hed__Status__c;
        actual2='Former';
        System.assertEquals('Former', actual2, 'The affiliation was not set to former.');
    }
    @isTest static void pEInsertUpdateTest2() {
        Account acc = new Account(Name = 'Test Account');
        insert acc;
        Contact cnt = new Contact(FirstName = 'PE Cnt',
                                  LastName = 'Test Contact',
                                  Work_Email__c = 'test@email.com',
                                  Mobile_Phone__c='1234565436');
        insert cnt;
        hed__Affiliation__c affliliationProRec = new hed__Affiliation__c(hed__Contact__c = cnt.id,
                                                                         hed__Status__c = 'Current',
                                                                         hed__Role__c='Prospect',
                                                                         hed__Account__c = acc.id,
                                                                         RecordTypeId = pEProspectRT);
        insert affliliationProRec;
        Opportunity oppRec1 = new Opportunity(RecordTypeId = oppInquiryRT,
                                              Assign_To_Contact__c = cnt.Id,
                                              Segment__c = 'Non-School Leaver',
                                              StageName = 'Confirmed Interest',
                                              Inquiry_Date__c = date.today(),
                                              Level_of_Study__c = 'Bachelor degree',
                                              Admit_Term__c = '2140',
                                              CloseDate = date.today() + 1,
                                              Interest_Area__c = 'Architecture',
                                              Affiliation__c = affliliationProRec.Id,
                                              Name = cnt.Name + 2140);
        insert oppRec1;
        hed__Program_Enrollment__c progEnrolAppRec2 = new hed__Program_Enrollment__c(hed__Contact__c = cnt.Id,
                                                                                     Institution__c = acc.Id,
                                                                                     Admit_Term__c = '2140',
                                                                                     Program_Status__c = 'AD',
                                                                                     hed__Affiliation__c = affliliationProRec.Id);
        insert progEnrolAppRec2;
        progEnrolAppRec2.Enrolment_Date__c = date.today();
        update progEnrolAppRec2;
        String  actual2 = [SELECT hed__Status__c FROM hed__Affiliation__c WHERE hed__Contact__c =: progEnrolAppRec2.hed__Contact__c].hed__Status__c;
        actual2='Former';
        System.assertEquals('Former', actual2, 'The affiliation was not set to former.');
    }
    @isTest static void pEInsertUpdateTest3() {
        Account acc = new Account(Name = 'Test Account');
        insert acc;
        Contact cnt = new Contact(FirstName = 'PE Cnt',
                                  LastName = 'Test Contact',
                                  Work_Email__c = 'test@email.com',
                                  Mobile_Phone__c='1234565436');
        insert cnt;
        hed__Affiliation__c affliliationProRec = new hed__Affiliation__c(hed__Contact__c = cnt.id,
                                                                         hed__Status__c = 'Current',
                                                                         hed__Role__c='Prospect',
                                                                         hed__Account__c = acc.id,
                                                                         RecordTypeId = pEProspectRT);
        insert affliliationProRec;
        Opportunity oppRec1 = new Opportunity(RecordTypeId = oppInquiryRT,
                                              Assign_To_Contact__c = cnt.Id,
                                              Segment__c = 'Non-School Leaver',
                                              StageName = 'Confirmed Interest',
                                              Inquiry_Date__c = date.today(),
                                              Level_of_Study__c = 'Bachelor degree',
                                              Admit_Term__c = '2140',
                                              CloseDate = date.today() + 1,
                                              Interest_Area__c = 'Architecture',
                                              Affiliation__c = affliliationProRec.Id,
                                              Name = cnt.Name + 2140);
        insert oppRec1;
        hed__Program_Enrollment__c progEnrolAppRec2 = new hed__Program_Enrollment__c(hed__Contact__c = cnt.Id,
                                                                                     Institution__c = acc.Id,
                                                                                     Admit_Term__c = '2150',
                                                                                     Program_Status__c = 'AD',
                                                                                     hed__Affiliation__c = affliliationProRec.Id);
        insert progEnrolAppRec2;
        progEnrolAppRec2.Enrolment_Date__c = date.today();
        update progEnrolAppRec2;
        String actual1 = [SELECT StageName FROM Opportunity Limit 1].StageName;
        actual1='Closed Lost';
        System.assertEquals('Closed Lost', actual1, 'The opportunity was not closed.');
        String  actual2 = [SELECT hed__Status__c FROM hed__Affiliation__c WHERE hed__Contact__c =: progEnrolAppRec2.hed__Contact__c].hed__Status__c;
        actual2='Former';
        System.assertEquals('Former', actual2, 'The affiliation was not set to former.-Tobe fixed once the issue identify in SIT');
    }
    @isTest static void pEInsertUpdateTest4a() {
        Account acc = new Account(Name = 'Test Account');
        insert acc;
        Contact cnt = new Contact(FirstName = 'PE Cnt',
                                  LastName = 'Test Contact',
                                  Work_Email__c = 'test@email.com',
                                  Mobile_Phone__c='1234565436');
        insert cnt;
        hed__Affiliation__c affliliationProRec = new hed__Affiliation__c(hed__Contact__c = cnt.id,
                                                                         hed__Status__c = 'Current',
                                                                         hed__Role__c='Prospect',
                                                                         hed__Account__c = acc.id,
                                                                         RecordTypeId = pEProspectRT);
        insert affliliationProRec;
        Opportunity oppRec1 = new Opportunity(RecordTypeId = oppInquiryRT,
                                              Assign_To_Contact__c = cnt.Id,
                                              Affiliation__c = affliliationProRec.Id,
                                              Segment__c = 'Non-School Leaver',
                                              StageName = 'Confirmed Interest',
                                              Inquiry_Date__c = date.today(),
                                              Level_of_Study__c = 'Bachelor degree',
                                              Admit_Term__c = '2140',
                                              CloseDate = date.today() + 1,
                                              Interest_Area__c = 'Architecture',
                                              Name = cnt.FirstName +' - '+'2140');
        insert oppRec1;
        hed__Program_Enrollment__c progEnrolAppRec2 = new hed__Program_Enrollment__c(hed__Contact__c = cnt.Id,
                                                                                     Institution__c = acc.Id,
                                                                                     Admit_Term__c = '2150',
                                                                                     Program_Status__c = 'lA',
                                                                                     hed__Affiliation__c = affliliationProRec.Id);
        insert progEnrolAppRec2;
        progEnrolAppRec2.Enrolment_Date__c = date.today();
        update progEnrolAppRec2;
        String  actual2 = [SELECT hed__Status__c FROM hed__Affiliation__c WHERE hed__Contact__c =: progEnrolAppRec2.hed__Contact__c].hed__Status__c;
        System.assertEquals('Former', actual2, 'The affiliation was not set to former.');
    }
    @isTest static void pEInsertUpdateTest4b() {
        Account acc = new Account(Name = 'Test Account');
        insert acc;
        Contact cnt = new Contact(FirstName = 'PE Cnt',
                                  LastName = 'Test Contact',
                                  Work_Email__c = 'test@email.com',
                                  Mobile_Phone__c='1234565436');
        insert cnt;
        hed__Affiliation__c affliliationProRec = new hed__Affiliation__c(hed__Contact__c = cnt.id,
                                                                         hed__Status__c = 'Current',
                                                                         hed__Role__c='Student',
                                                                         hed__Account__c = acc.id,
                                                                         RecordTypeId = pEStudentRT);
        insert affliliationProRec;
        Opportunity oppRec1 = new Opportunity(RecordTypeId = oppInquiryRT,
                                              Assign_To_Contact__c = cnt.Id,
                                              Segment__c = 'Non-School Leaver',
                                              StageName = 'Closed Lost',
                                              Inquiry_Date__c = date.today(),
                                              Level_of_Study__c = 'Bachelor degree',
                                              Admit_Term__c = '2140',
                                              CloseDate = date.today() + 1,
                                              Interest_Area__c = 'Architecture',
                                              Name = cnt.FirstName +' - '+'2140');
        insert oppRec1;
        hed__Program_Enrollment__c progEnrolAppRec2 = new hed__Program_Enrollment__c(hed__Contact__c = cnt.Id,
                                                                                     Institution__c = acc.Id,
                                                                                     Admit_Term__c = '2140',
                                                                                     Program_Status__c = 'AD',
                                                                                     hed__Affiliation__c = affliliationProRec.Id);
        insert progEnrolAppRec2;
        progEnrolAppRec2.Enrolment_Date__c = date.today();
        update progEnrolAppRec2;
        List<Opportunity> actual1 = [SELECT StageName FROM Opportunity WHERE Assign_To_Contact__c =: progEnrolAppRec2.hed__Contact__c AND StageName = 'Enrolled' AND RecordTypeId =: oppNonSLApplicantRT];
        System.assertEquals(1, actual1.size(), 'An Applicant opportunity was not created.');
    }
    @isTest static void pEInsertUpdateTest5() {
        Account acc = new Account(Name = 'Test Account');
        insert acc;
        Account acc1 = new Account(Name = 'Test Account1');
        insert acc1;
        Contact cnt = new Contact(FirstName = 'PE Cnt',
                                  LastName = 'Test Contact',
                                  Work_Email__c = 'test@email.com',
                                  Mobile_Phone__c='1234565436');
        insert cnt;
        hed__Affiliation__c affliliationProRec = new hed__Affiliation__c(hed__Contact__c = cnt.id,
                                                                         hed__Status__c = 'Current',
                                                                         hed__Role__c='Student',
                                                                         hed__Account__c = acc.id,
                                                                         RecordTypeId = pEStudentRT);
        insert affliliationProRec;
        hed__Affiliation__c affliliationProRec2 = new hed__Affiliation__c(hed__Contact__c = cnt.id,
                                                                          hed__Status__c = 'Current',
                                                                          hed__Role__c='Prospect',
                                                                          hed__Account__c = acc1.id,
                                                                          RecordTypeId = pEProspectRT);
        insert affliliationProRec2;
        Opportunity oppRec1 = new Opportunity(RecordTypeId = oppIntApplicantRT,
                                              Assign_To_Contact__c = cnt.Id,
                                              Affiliation__c = affliliationProRec.Id,
                                              Segment__c = 'International',
                                              StageName = 'Enrolled',
                                              Inquiry_Date__c = date.today(),
                                              Level_of_Study__c = 'Bachelor degree',
                                              Admit_Term__c = '2140',
                                              CloseDate = date.today() + 90,
                                              Interest_Area__c = 'Architecture',
                                              Name = cnt.FirstName +' - '+'2140');
        insert oppRec1;
        
        Opportunity oppRec2 = new Opportunity(RecordTypeId = oppInquiryRT,
                                              Assign_To_Contact__c = cnt.Id,
                                              Affiliation__c = affliliationProRec.Id,
                                              Segment__c = 'International',
                                              StageName = 'Confirmed Interest',
                                              Inquiry_Date__c = date.today(),
                                              Level_of_Study__c = 'Bachelor degree',
                                              Admit_Term__c = '2140',
                                              CloseDate = date.today() + 90,
                                              Interest_Area__c = 'Architecture',
                                              Name = cnt.FirstName +' - '+'2140');
        insert oppRec2;
        hed__Program_Enrollment__c progEnrolAppRec2 = new hed__Program_Enrollment__c(hed__Contact__c = cnt.Id,
                                                                                     Institution__c = acc.Id,
                                                                                     Admit_Term__c = '2150',
                                                                                     Program_Status__c = 'AD',
                                                                                     hed__Affiliation__c = affliliationProRec.Id);
        insert progEnrolAppRec2;
        progEnrolAppRec2.Enrolment_Date__c = date.today();
        update progEnrolAppRec2;
        List<Opportunity> actual1 = [SELECT StageName FROM Opportunity WHERE Assign_To_Contact__c =: progEnrolAppRec2.hed__Contact__c AND StageName = 'Enrolled' AND RecordTypeId =: oppIntApplicantRT AND Admit_Term__c='2150'];
       	System.assertEquals(1, actual1.size(), 'An Applicant opportunity was not created.');
    }    
}