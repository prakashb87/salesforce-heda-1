public without sharing class CreateAdministrativeAccount {
    public static void createAccount(List<Contact> newcon){
        Set<id> setAccntIds = new Set<Id>();
        
		String accountAdministrativeRecord = Schema.SObjectType.Account.RecordTypeInfosByName.get('Administrative').RecordTypeId;        
        
        map<string, Account> mapAccount = new map<string,Account>();
        map<string, Contact> mapContact = new map<string,Contact>();
        
        /*--------Start-------------------------------*/
        //RM - 658 - Author - Binish Kumar
        
        Integer contMapIndex = 0;//Added as part of RM-658
        
        for(Contact c: newcon) {
            //if((c.RecordTypeId  == studentRecordTypeId) || (c.RecordTypeId  == staffRecordTypeId)){Commented as part of RM-451
            contMapIndex++; //Added as part of RM-658
            Account a  = new Account();
            String firstname;
            if(c.firstname != null){
            	firstname = c.firstname+' ';
            }else {
            	firstname = '';
            }
            if(c.EXT_ID_Contact__c != null){//Added as part of RM-658                
                a.Contact_Id__c = c.EXT_ID_Contact__c   ;
                a.Name = firstname+c.LastName+' '+Label.Administrative_Account;
                a.RecordTypeId = accountAdministrativeRecord;
                mapAccount.put(c.EXT_ID_Contact__c  , a);
                mapContact.put(c.EXT_ID_Contact__c  , c);
            }else if(c.Unique_Reference_ID__c != null){//Condition Added as part of RM-658 , rest functionality is existing
                a.Contact_Id__c = c.Unique_Reference_ID__c;
                a.Name = firstname+c.LastName+' '+Label.Administrative_Account;
                a.RecordTypeId = accountAdministrativeRecord;
                mapAccount.put(c.Unique_Reference_ID__c, a);
                mapContact.put(c.Unique_Reference_ID__c, c);
            }else if(c.OpenEdx_Student_Id__c != null){//Added as part of RM-658
                a.Contact_Id__c = c.OpenEdx_Student_Id__c ;
                a.Name = firstname+c.LastName+' '+Label.Administrative_Account;
                a.RecordTypeId = accountAdministrativeRecord;
                mapAccount.put(c.OpenEdx_Student_Id__c , a);
                mapContact.put(c.OpenEdx_Student_Id__c, c);
                //setAccntIds.add(c.AccountId);
            }else{//Added as part of RM-658
                a.Contact_Id__c = String.valueOf(contMapIndex);
                a.Name = firstname+c.LastName+' '+Label.Administrative_Account;
                a.RecordTypeId = accountAdministrativeRecord;
                mapAccount.put(a.Contact_Id__c  , a);
                mapContact.put(a.Contact_Id__c  , c);
            }
            /*-------------------------RM-658-End-------------------------------------*/     
            
            // }Commented as part of RM-451
        }
        // Insert Administrative Accounts
        if(!(mapAccount.isEmpty())){       
            Database.SaveResult[] accList = database.insert(mapAccount.values(),false);
            for (Database.SaveResult sr : accList) {
                if (sr.isSuccess()) {
                    setAccntIds.add(sr.getId());
                }
            }      
        }
        
        if(!(setAccntIds.isEmpty())) {
            
            For(Account acc : [Select Id, Contact_Id__c from Account where Id IN : setAccntIds]){
                if(mapContact.containsKey(acc.Contact_Id__c)){
                    Contact con = mapContact.get(acc.Contact_Id__c);
                    con.accountId = acc.Id;
                    mapContact.put(acc.Contact_Id__c, con);
                    
                }
                
            }
        }
        
    }
    //new method to update administrative account when contact is updated
    public static void updateAdministrativeAccount(List<Contact> contactList){
    	Set<Id> accountIdSet = new Set<Id>(); 
    	List<Account> accountList = new List<Account>();
    	List<Account> accountAdministrativeListToUpdate = new List<Account>();
    	Id administrativeRecordTypeId = Schema.SObjectType.Account.RecordTypeInfosByName.get('Administrative').RecordTypeId; 
    	Map<Id,Contact> idAccountContactMap = new Map<Id,Contact>();
    	String administrativeAccountName;
    	String firstname;
    	
    	for(Contact contact : contactList){
    		accountIdSet.add(contact.accountId);
    		idAccountContactMap.put(contact.accountId,contact);
    	}
    	
    	accountList = [SELECT id,recordtypeId,name FROM Account WHERE Id IN :accountIdSet ];
    	
    	for(Account account : accountList){
    		if(account.recordtypeid == administrativeRecordTypeId){
    			if(idAccountContactMap.get(account.id).firstname != null){
	            	firstname = idAccountContactMap.get(account.id).firstname+' ';
	            }else {
	            	firstname = '';
	            }
	            
	            administrativeAccountName = firstname+idAccountContactMap.get(account.id).LastName+' '+Label.Administrative_Account; 
	            
            	if(account.name != administrativeAccountName){
            		account.name = administrativeAccountName;
    				accountAdministrativeListToUpdate.add(account);
    			}
    		}
    	}
    	
    	try{
    		if(!accountAdministrativeListToUpdate.isempty()){
    			update accountAdministrativeListToUpdate;
    		}
    		
    	}catch (Exception e){
    		throw e;
    	}
    }
}