/*****************************************************************
Name: GeneratePDFControllerTest
Author: Capgemini[Shreya] 
Purpose: Test Class For GeneratePDFController
*****************************************************************/
/*==================================================================
History
--------
Version   Author                     Date              Detail
1.0       Shreya Barua            16/05/2018         Initial Version
********************************************************************/

@isTest
public class GeneratePDFControllerTest 
{
    static testmethod void testGeneratePDF()
    {
      
        TestDataFactoryUtilRefOne.setUpTestDataForInvoicePdfGeneration();
        //Querying data from test datasetup method
        List<csord__Service__c> servicesList = [Select Id from csord__Service__c];
        system.assert(servicesList.size()>0,true);
        
        List<id> serviceIDList = new List<Id>();
        
        for(csord__Service__c ser : servicesList)
        {
            serviceIDList.add(ser.id);  
        }
        
        Test.startTest();
        
        PDFGenerationObserver.generatePDF(serviceIDList); 


        Test.stopTest();   
         system.assert(servicesList.size()>0,true);
       
    } 

    static testmethod void testCaseExisting()
    {
      
        TestDataFactoryUtilRefOne.setUpTestDataForInvoicePdfGeneration();


        //Querying data from test datasetup method
        List<csord__Service__c> servicesList = [Select Id,csordtelcoa__Main_Contact__r.Id from csord__Service__c];
        system.assert(servicesList.size()>0,true);
        Case c = new Case();
        c.RecordTypeId = Schema.SObjectType.Case.getRecordTypeInfosByName().get('RMIT Support Request Case').getRecordTypeId();
        c.Origin = ConfigDataMapController.getCustomSettingValue('PaymentCaseEmail'); //ECB-4308 - :Shreya -Replaced Custom Label with Custom Setting
        c.Status = ConfigDataMapController.getCustomSettingValue('PaymentCaseStatus'); //ECB-4308 - :Shreya -Replaced Custom Label with Custom Setting
        c.description = 'test'; 
        c.subject = 'test';
        c.ContactId = servicesList[0].csordtelcoa__Main_Contact__r.Id;
        c.Type = 'MarketPlace'; 
        insert c;
        
        
        List<id> serviceIDList = new List<Id>();
        
        for(csord__Service__c ser : servicesList)
        {
            serviceIDList.add(ser.id);  
        }
        
        Test.startTest();
        
        PDFGenerationObserver.generatePDF(serviceIDList); 


        Test.stopTest();   
         system.assert(servicesList.size()>0,true);
       
    } 
  static testmethod  void getPDFContentTest()
  {
        
        Test.startTest();
        
        TestDataFactoryUtil.test();
        //Querying data from test datasetup method
        cscfga__Product_Configuration__c prodconfig = [Select Id from cscfga__Product_Configuration__c where Name = 'ABCD Test'];
        system.assert(prodconfig!=null,true);
               
        PageReference pdf = Page.InvoicePDF;
        pdf.getParameters().put('prodConfigIds', prodconfig.Id);
        
        GeneratePDFController obj = new GeneratePDFController();
        obj.prodConfigIds = prodconfig.Id;
        List<GeneratePDFCOntroller.CourseDetailWrapper> result= new List<GeneratePDFCOntroller.CourseDetailWrapper>();
        result = obj.getPDFContent();
 
        system.assert(result.size()>0,true);
        
        Test.stopTest();
    }

    

    
     
}