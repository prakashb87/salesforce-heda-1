public without sharing class AllOpportunityTriggersHandler {
        
    public static void handleBeforeUpdate(Map<Id, Opportunity> newMap, Map<Id, Opportunity> oldMap) {
        updateOpportunityOwnerOnUpdate(newMap, oldMap);
    }
    
    public static void handleBeforeInsert(List<Opportunity> newOppList) {
        updateOpportunityOwnerOnInsert(newOppList);
    }

    public static void updateOpportunityOwnerOnUpdate(Map<Id, Opportunity> newMap, Map<Id, Opportunity> oldMap) {
        
        system.debug('newMap::::'+newMap);
        system.debug('oldMap::::'+oldMap);
        
        map<Id, Id> mapContactIdOpptyId = new map<Id, Id>();
        for (Id opptyId: newMap.keySet()) {
            if (newMap.get(opptyId).Assign_To_Contact__c != null && 
                newMap.get(opptyId).Assign_To_Contact__c != oldMap.get(opptyId).Assign_To_Contact__c) {
                mapContactIdOpptyId.put(newMap.get(opptyId).Assign_To_Contact__c, opptyId);
            }
        }
        
        system.debug('mapContactIdOpptyId::::'+mapContactIdOpptyId);
        map<Id, Id> mapContactIdUserId = new map<Id, Id>();
        mapContactIdUserId=mapContactToUser(mapContactIdOpptyId.keySet());

        list<cscfga__Product_Basket__c> toUpdate = new list<cscfga__Product_Basket__c>();
        for (cscfga__Product_Basket__c basket: [select id, OwnerId, cscfga__Opportunity__c, cscfga__Opportunity__r.Assign_To_Contact__c 
            from cscfga__Product_Basket__c 
            where cscfga__Opportunity__c in: newMap.keySet()]) {
            if (mapContactIdUserId.containsKey(newMap.get(basket.cscfga__Opportunity__c).Assign_To_Contact__c)) {
                basket.OwnerId = mapContactIdUserId.get(newMap.get(basket.cscfga__Opportunity__c).Assign_To_Contact__c);
                toUpdate.add(basket);
            }
        }
        
        for (Opportunity oppty: newMap.values()) {
            if (mapContactIdUserId.containsKey(oppty.Assign_To_Contact__c)) { 
                oppty.OwnerId = mapContactIdUserId.get(oppty.Assign_To_Contact__c);
            }
        }
        system.debug('toUpdate::::'+toUpdate);
        if (!toUpdate.isEmpty()) {
            Database.update(toUpdate);
        }
    }
    private static  map<Id, Id> mapContactToUser(set<ID> contactIds){
    	 map<Id, Id> mapContactIdUserId = new map<Id, Id>();
    	 for (User user: [select Id, ContactId 
            from User 
            where IsActive = true 
                and ContactId <> ''
                and ContactId in: contactIds]) {
            mapContactIdUserId.put(user.ContactId, user.id);                        
        }
        return mapContactIdUserId;
    }
    public static void updateOpportunityOwnerOnInsert(List<Opportunity> newList) {
        Id oppRecordtypeID = Schema.SObjectType.Opportunity.getRecordTypeInfosByName().get('Inquiry Opportunities').getRecordTypeId();
        system.debug('newList::::'+newList);
        Map<Id, Opportunity> newOpptyMap = new Map<Id, Opportunity>();
        for(Opportunity opp : newList){
            if(oppRecordtypeID!=opp.RecordTypeId){
                 newOpptyMap.put(opp.Id, opp);
            }
            
        }
           
        
        map<Id, Id> mapContactIdOpptyId = new map<Id, Id>();
        for (Id opptyId: newOpptyMap.keySet()) {
            if (newOpptyMap.get(opptyId).Assign_To_Contact__c != null) {
                mapContactIdOpptyId.put(newOpptyMap.get(opptyId).Assign_To_Contact__c, opptyId);
            }
        }
        
        system.debug('mapContactIdOpptyId::::'+mapContactIdOpptyId);
        map<Id, Id> mapContactIdUserId = new map<Id, Id>();
        for (User user: [select Id, ContactId 
            from User 
            where IsActive = true 
                and ContactId <> ''
                and ContactId in: mapContactIdOpptyId.keySet()]) {
            mapContactIdUserId.put(user.ContactId, user.id);                        
        }
        for (Opportunity oppty: newOpptyMap.values()) {
            if (mapContactIdUserId.containsKey(oppty.Assign_To_Contact__c)) { 
                oppty.OwnerId = mapContactIdUserId.get(oppty.Assign_To_Contact__c);
            }
        }
    }
}