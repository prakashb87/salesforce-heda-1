/*********************************************************************************
*** @TestClassName     : FUND_SOURCE_REST_Test
*** @Author            : Shubham Singh
*** @Requirement       : To Test the FUND_SOURCE_REST web service apex class.
*** @Created date      : 14/08/2018
**********************************************************************************/
@isTest
public class FUND_SOURCE_REST_Test {
    static testMethod void myUnitTest() {
        RestRequest req = new RestRequest();
        
        RestResponse res = new RestResponse();
        // adding header to the request
        req.addHeader('httpMethod', 'POST');
        
        req.requestUri = '/services/apexrest/HEDA/v1/FundSource';
        
        //creating test data
        String jsonMsg = '{"fundSourceCode": "55","effectiveDate": "2007-01-01","status": "A","description": "Skills Store RPL Assessment -","longDescription": "Skills Store RPL Assessment - Fee for Service","fundSourceState": "Z36","fundSourceNational": "20","fundSourceBroad": "AFPENT","fundSourceDEEWR": "7","state": "VIC","apprenticeTraineeWaiver": "Y","apprenticeTraineeIndicator": "N","subjectToEsos": "N"}';
        
        req.requestBody = Blob.valueof(jsonMsg);
        
        RestContext.request = req;
        
        RestContext.response = res;
        
        Test.startTest();
        //store response
        Map < String, String > check = FUND_SOURCE_REST.upsertFundSource();
        List < String > str = check.values();
        //assert condition 
        System.assertEquals(True, str[0] == 'ok');
        
        Test.stopTest();
        
    }
    static testMethod void coverError() {
        RestRequest req = new RestRequest();
        
        RestResponse res = new RestResponse();
        // adding header to the request
        req.addHeader('httpMethod', 'POST');
        
        req.requestUri = '/services/apexrest/HEDA/v1/FundSource';
        
        //creating test data
        String jsonMsg = '{"fundSourceCode": "","effectiveDate": "2007-01-01","status": "A","description": "Skills Store RPL Assessment -","longDescription": "Skills Store RPL Assessment - Fee for Service","fundSourceState": "Z36","fundSourceNational": "20","fundSourceBroad": "AFPENT","fundSourceDEEWR": "7","state": "VIC","apprenticeTraineeWaiver": "Y","apprenticeTraineeIndicator": "N","subjectToEsos": "N"}';
        
        req.requestBody = Blob.valueof(jsonMsg);
        
        RestContext.request = req;
        
        RestContext.response = res;
        
        Test.startTest();
        //store response
        Map < String, String > check = FUND_SOURCE_REST.upsertFundSource();
        List < String > str = check.values();
        //assert condition 
        System.assertEquals(False, str[0] == 'ok');
        
        Test.stopTest();
        
    }
    static testMethod void coverTryCatch() {
        Map < String, String > check;
        Test.startTest();
        try {
            //store response
            check = FUND_SOURCE_REST.upsertFundSource();
        } catch (DMLException e) {
            List < String > str = check.values();
            //assert condition 
            System.assertEquals(False, str[0] == 'ok');
        }
        Test.stopTest();
    }
    static testMethod void updateMethod() {
        
        Fund_Source__c fs = new Fund_Source__c();
        
        fs.Name = '55';
        fs.Effective_Date__c = Date.ValueOf('2008-01-01');
        insert fs;
        
        RestRequest req = new RestRequest();
        
        RestResponse res = new RestResponse();
        // adding header to the request
        req.addHeader('httpMethod', 'POST');
        
        req.requestUri = '/services/apexrest/HEDA/v1/FundSource';
        
        //creating test data
        String jsonMsg = '{"fundSourceCode": "55","effectiveDate": "2007-01-01","status": "A","description": "Skills Store RPL Assessment -","longDescription": "Skills Store RPL Assessment - Fee for Service","fundSourceState": "Z36","fundSourceNational": "20","fundSourceBroad": "AFPENT","fundSourceDEEWR": "7","state": "VIC","apprenticeTraineeWaiver": "Y","apprenticeTraineeIndicator": "N","subjectToEsos": "N"}';
        
        req.requestBody = Blob.valueof(jsonMsg);
        
        RestContext.request = req;
        
        RestContext.response = res;
        
        Test.startTest();
        //store response
        Map < String, String > check = FUND_SOURCE_REST.upsertFundSource();
        List < String > str = check.values();
        //assert condition 
        System.assertEquals(True, str[0] == 'ok');
        
        Test.stopTest();
        
    }
}