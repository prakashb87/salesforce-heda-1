/********************************************************************************************************/
/**Author : Capgemini                                  Date : 14/02/2019     ****************************/ 
/* This class contains all the logic for the trigger ****************************************************/              
public with sharing class CourseTransactionTriggerHandler implements TriggerHandler {
    
    public void beforeInsert(List<SObject> newItems) {
       system.debug('Inside before insert');
    }
 
    public void beforeUpdate(Map<Id, SObject> newItems, Map<Id, SObject> oldItems) {
       system.debug('Inside before update');
    }
 
    public void beforeDelete(Map<Id, SObject> oldItems) {
       system.debug('Inside before delete');
    }
 
    public void afterInsert(Map<Id, SObject> newItems) {
       CourseTransactionUpdateTriggerHelper.updateCourseTransactionRecord(newItems.keySet());
    }
 
    public void afterUpdate(Map<Id, SObject> newItems, Map<Id, SObject> oldItems) {
       system.debug('Inside after update');
    }
 
    public void afterDelete(Map<Id, SObject> oldItems) {
       system.debug('Inside after delete');
    }
 
    public void afterUndelete(Map<Id, SObject> oldItems) {
       system.debug('Inside after undelete');
    }
    
}