/**
 * @author Resmi Ramakrishnan
 * @date 8/06/2018 
 * @description This class is a generic utility class for creating Integration Log Objects
 */
public without sharing class IntegrationLogWrapper {
    
    /**
	 * @description Method to Create Integration Logs
	 * @return Returns integration logs record with field values populated
	 */
    public static Integration_Logs__c createIntegrationLog(String inputType, String inputRequestResponse, 
            string inputServiceType, String objectId, Boolean isException) {        
        System.debug('RRRR: Entering  -createIntegrationLog: Service Type=' + inputServiceType);
        Integration_Logs__c intLogObj = new Integration_Logs__c();
       
        intLogObj.Type__c = inputType;
        intLogObj.Request_Response__c = inputRequestResponse;
        intLogObj.Service_Type__c = inputServiceType;
        intLogObj.SObject_ID__c = objectId;
        intLogObj.Is_Exception__c = isException;
        intLogObj.Sent_Received_Date_Time__c = System.now();
            
        System.debug('RRRR: Leaving  - createIntegrationLog method' );       
        return intLogObj;
    }    
}