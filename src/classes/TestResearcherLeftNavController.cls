/*******************************************
Purpose: Test class ResearcherLeftNavController
History:
Created by Ankit Bhagat on 11/10/2018
*******************************************/
@isTest
public class TestResearcherLeftNavController {
    
    /************************************************************************************
    // Purpose      :  Creating Test data for Community User for all test methods 
    // Developer    :  Ankit
    // Created Date :  10/24/2018                 
    //***********************************************************************************/
    @testSetup static void testDataSetup() {
        
        RSTP_TestDataFactoryUtils.createNonCommunityUsersWithProfile(1,'System Administrator','Activator');
        
        User adminUser =[SELECT Id,ContactId FROM User WHERE UserName='test.nonCommunityUser1@test.com.testDev' LIMIT 1];
        System.runAs(adminUser)   
        {
            Map<String,Integer> allnumbersOfDataMap = new Map<String,Integer>();
            allnumbersOfDataMap.put('user',1);
            allnumbersOfDataMap.put('account',1);
            allnumbersOfDataMap.put('contact',1);
            
            Map<String,String> requiredInfoMap = new Map<String,String>();
            requiredInfoMap.put('userType','CommunityUsers');
            requiredInfoMap.put('profile',Label.testProfileName);
            
            Boolean userCreatedFlag = RSTP_TestDataFactoryUtils.testRSTPDataSetup(allnumbersOfDataMap,requiredInfoMap);
            System.assert(userCreatedFlag==true,'Community User Created'); 
        }
        
    }    
    /************************************************************************************
    // Purpose      :  Test functionality for View MyEthicsList Controller
    // Developer    :  Ankit
    // Created Date :  10/24/2018                 
    //***********************************************************************************/
    
    @isTest
   public static void testResearcherLeftNavWrapperMethod(){

        User u =[SELECT Id,ContactId FROM User WHERE UserName='test.communityUser1@test.com.testDev' LIMIT 1]; 

        System.runAs(u) {            
            
            Ethics__c ethicObj = new Ethics__c();
            ethicObj.Ethics_Id__c = 'test4';
            insert ethicObj ;
            List<id> ethicsId = new List<id>();
            ethicsId.add(ethicObj.id);
            system.assert(ethicsId!=null);
            
            Ethics__c ethicTest1 = new Ethics__c();
            ethicTest1 .Ethics_Id__c = 'Test1';
            ethicTest1.Application_Title__c = 'Test';
            ethicTest1.Application_Type__c = 'HUMAN = Human Ethics';
            ethicTest1.Status__c = 'Amended';
            ethicTest1.Approved_Date__c = date.today();
            ethicTest1.Expiry_Date__c   = date.today().addDays(10);
            insert ethicTest1;
            
                  
            ResearcherPortalProject__c researchProject = new ResearcherPortalProject__c();
            researchProject.Title__c = 'Test';
            insert researchProject;
            System.Assert(researchProject.Title__c == 'Test');
            
            Publication__c publication = new Publication__c();
            publication.Publication_Id__c=231234;
            publication.Publication_Title__c = 'Test';
            insert publication;
            System.Assert(publication.Publication_Title__c == 'Test');  
            
             Id projectRecordTypeId = Schema.SObjectType.Researcher_Member_Sharing__c.getRecordTypeInfosByName().get(Label.RSTP_ResearchProjectRecordtype).getRecordTypeId();
             Id ethicsRecordTypeId  = Schema.SObjectType.Researcher_Member_Sharing__c.getRecordTypeInfosByName().get(Label.RSTP_EthicsRecordtype).getRecordTypeId();
             Id contractRecordTypeId = Schema.SObjectType.Researcher_Member_Sharing__c.getRecordTypeInfosByName().get(Label.RSTP_ContractRecordtype).getRecordTypeId();
             Id publicationRecordTypeId = Schema.SObjectType.Researcher_Member_Sharing__c.getRecordTypeInfosByName().get(Label.RSTP_PublicationRecordtype).getRecordTypeId();
        
            
            List<Researcher_Member_Sharing__c> memberResearchList = new List<Researcher_Member_Sharing__c>();
            Researcher_Member_Sharing__c memberResearch = new Researcher_Member_Sharing__c();
            memberResearch.Ethics__c = ethicTest1.id;
            memberResearch.User__c   = u.id;
            memberResearch.RecordTypeId = ethicsRecordTypeId ;
            memberResearchList.add(memberResearch);
            
            Researcher_Member_Sharing__c memberResearch1 = new Researcher_Member_Sharing__c();
            memberResearch1.Researcher_Portal_Project__c= researchProject.id;
            memberResearch1.User__c   = u.id;
            memberResearch.RecordTypeId = projectRecordTypeId;
            memberResearchList.add(memberResearch1);
            
            Researcher_Member_Sharing__c memberResearch2 = new Researcher_Member_Sharing__c();
            memberResearch2.Publication__c= publication.id;
            memberResearch2.User__c   = u.id;
            memberResearch2.RecordTypeId = publicationRecordTypeId ;
            memberResearchList.add(memberResearch2);
   
            
            insert memberResearchList;
               
            
            List<SignificantEvent__c> milestonesList = new List<SignificantEvent__c>();
           
                SignificantEvent__c milestoneEthics = new SignificantEvent__c();
                milestoneEthics.DueDate__c = date.today().addDays(-15);
                milestoneEthics.IsAction__c = true;
                milestoneEthics.Ethics__c = ethicTest1.id;
                milestoneEthics.EventDescription__c = 'Test';
                milestonesList.add(milestoneEthics);  
                
                SignificantEvent__c milestoneProject = new SignificantEvent__c();
                milestoneProject.DueDate__c = date.today().addDays(10);
                milestoneProject.IsAction__c = true;
                milestoneProject.ResearchProject__c = researchProject.id;
                milestoneProject.EventDescription__c = 'Test';
                milestoneProject.ActualCompletionDate__c  = date.today().addDays(10);
                milestonesList.add(milestoneProject);  
                
                SignificantEvent__c milestonePublication = new SignificantEvent__c();
                milestonePublication.DueDate__c = date.today().addDays(45);
                milestonePublication.IsAction__c = true;
                milestonePublication.Publication__c = publication.id;
                milestonePublication.EventDescription__c = 'Test';
                milestonePublication.ActualCompletionDate__c  = date.today().addDays(10);
                milestonesList.add(milestonePublication);          
                
            insert milestonesList;
            System.Assert(milestonesList.size()== 3);
            
            
            ResearcherLeftNavWrapper.leftNavWrapper  wrapper = new ResearcherLeftNavWrapper.leftNavWrapper();
            wrapper.milestoneCount  = 3;
            
            ResearcherLeftNavController.getLeftNavWrapper();
            Boolean isSandbox =ResearcherLeftNavController.checkIsSandbox();
            if(isSandbox)
            {
                System.assert(isSandbox== true,'This is a Sandbox');  
            }
            else
            {
                System.assert(isSandbox== false,'This is a Production org');  
            }
        }
    }
    
        
    /************************************************************************************
    // Purpose      :  Test functionality for View MyEthicsList Controller
    // Developer    :  Ankit
    // Created Date :  12/05/2018                 
    //***********************************************************************************/
    
    @isTest
   public static void testResearcherLeftNavWrapperNegativeMethod(){
       
       User u =[SELECT Id,ContactId FROM User WHERE UserName='test.communityUser1@test.com.testDev' LIMIT 1]; 
       
       System.runAs(u) {            
           
           ResearcherLeftNavController.getLeftNavWrapper();
           Boolean isSandbox =ResearcherLeftNavController.checkIsSandbox();
           
           if(isSandbox)
           {
               System.assert(isSandbox== true,'This is a Sandbox');  
           }
           else
           {
               System.assert(isSandbox== false,'This is a Production org');  
           }
       }
    }
    @isTest
    public static void testExceptionHandlingMethod(){
        
        User u =[SELECT Id,ContactId FROM User WHERE UserName='test.communityUser1@test.com.testDev' LIMIT 1]; 
        
        System.runAs(u) {      
            
            ResearcherLeftNavController.isForceExceptionRequired=true; 
            ResearcherLeftNavController.getLeftNavWrapper();
            Boolean isSandbox =ResearcherLeftNavController.checkIsSandbox();
             if(isSandbox)
           {
               System.assert(isSandbox== true,'This is a Sandbox');  
           }
           else
           {
               System.assert(isSandbox== false,'This is a Production org');  
           }
            
        }
    }
    
}