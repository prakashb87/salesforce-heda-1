public without sharing class MockSuccessSingleCanvasEnrollReq implements ICanvasEnrollmentRequest {

    public void sendEnrollDetailToCanvas(CourseConnCanvasRequestWrapper canvasEnrollmentRequestBody) {
        System.debug('Mock request assumed to be successful!');
    }
    
    public Map<Id, CourseConnCanvasResponseWrapper.Response> getCanvasEnrollResByCourseConnId() {
        Id courseConnectionId;
        // Assume test data is already setup
        List<hed__Course_Enrollment__c> courseConnections = [SELECT Id FROM hed__Course_Enrollment__c LIMIT 1];
        if (courseConnections.isEmpty()) {
            Account acc = new Account(name = 'RMIT Online', recordTypeId = Schema.SObjectType.Account.getRecordTypeInfosByName().get('Academic Institution').getRecordTypeId());
            Database.insert(acc,false);
            
            Contact con = new Contact();
            con.LastName='TestCon';
            con.Student_ID__c='1235';
            con.Email='mail@mail.com';
            con.hed__UniversityEmail__c = 'mail@mail.com';
            con.hed__Preferred_Email__c = 'SAMS/SAP University Email';
            Database.Insert(con,false);
            
            hed__Course__c course = new hed__Course__c();
            course.Name = 'test Course';
            course.hed__Account__c = acc.id;
            course.Status__c='Active';
            course.SIS_Course_Id__c='1254';
            course.RecordTypeId=Schema.SObjectType.hed__Course__c.getRecordTypeInfosByName().get('21CC').getRecordTypeId();
            Database.Insert(course,false);
            
            hed__Term__c term = new hed__Term__c(name = 'test term', hed__Account__c = acc.id);
            Database.Insert(term,false);
            
            hed__Course_Offering__c crsoffer = new hed__Course_Offering__c();
            crsoffer.name = 'test courseoffer';
            crsoffer.hed__Course__c = course.Id;
            crsoffer.hed__Term__c = term.id;
            crsoffer.hed__Start_Date__c=Date.newInstance(2018,05,03);
            crsoffer.hed__End_Date__c=Date.newInstance(2018,06,03);
            Database.Insert(crsoffer,false); 
            
            hed__Course_Enrollment__c ce = new hed__Course_Enrollment__c();
            ce.hed__Contact__c = con.Id;
            ce.hed__Course_Offering__c = crsoffer.Id;
            ce.hed__Status__c = 'Current';
            ce.Enrolment_Status__c = 'E';
            ce.Canvas_Integration_Processing_Complete__c = false;
            ce.SAMS_Integration_Processing_Complete__c = true;
            ce.Source_Type__c = 'MKT-Batch';
            Database.Insert(ce,false);
            
            courseConnectionId = ce.Id;
        } else {
            courseConnectionId = courseConnections[0].Id;
        }
           
        CourseConnCanvasResponseWrapper.Response successResponse = new CourseConnCanvasResponseWrapper.Response();
        successResponse.courseConnectionId = courseConnectionId;
        successResponse.result = 'Success';
        successResponse.html_url = '';
        successResponse.errorMessage = '';
        
        Map<Id, CourseConnCanvasResponseWrapper.Response> result = new Map<Id, CourseConnCanvasResponseWrapper.Response>();
        result.put(courseConnectionId, successResponse);
        
        return result;
    }
    
    public Integer getTotalNumberOfFailedEnrollments() {
        return 0;
    }
}