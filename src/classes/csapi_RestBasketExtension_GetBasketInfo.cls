@RestResource(urlMapping='/csapi_BasketExtension/GetBasketInfo')
global class csapi_RestBasketExtension_GetBasketInfo {
    
    @HttpPost
    global static csapi_BasketRequestWrapper.BasketInfoResponse doPost(csapi_BasketRequestWrapper.BasketInfoRequest csapi_request) {
        
        RestRequest req = RestContext.request;
        RestResponse res = RestContext.response;
        
        Exception ee = null;
        csapi_BasketRequestWrapper.BasketInfoResponse csapi_response;
        try {
            csapi_response = csapi_BasketExtension.getBasketInfo(csapi_request);
        } catch(Exception e ) {
            ee = e;
        } finally {
            if(ee != null) 
            {
                system.debug('ee >> ' + ee.getMessage());
            }
        }
                
        return csapi_response;
    }
    
}