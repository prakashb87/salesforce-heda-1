/*****************************************************************************************************
*** @Class             : CourseConnCanvasResponseWrapperTest
*** @Author            : Avinash Machineni 
*** @Requirement       : Integration between Salesforce and IPaaS for sending course SIS Id, Course Offering Id's
*** @Created date      : 13/06/2018
*** @JIRA ID           : ECB-367
*****************************************************************************************************/
/*****************************************************************************************************
*** @About Class
*** This class is used to test the clas wriiten for receiving the response structure that is received from Ipaas
*************************************************************************************/
@IsTest
public class CourseConnCanvasResponseWrapperTest {
	
	static testMethod void testParse() {
		String json = '{  '+
		'   \"id\":\"ID-dev-6r87t87587\",'+
		'   \"result\":\"success\",'+
		'   \"code\":\"200\",'+
		'   \"application\":\"rmit-system-api-sfdc\",'+
		'   \"provider\":\"canvas\",'+
		'   \"payload\":{  '+
		'      \"studentId\":\"34\",'+
		'      \"enrolmentResponse\":[  '+
		'         {  '+
		'            \"courseConnectionId\":\"\",'+
		'            \"result\":\"\",'+
		'            \"html_url\":\"\",'+
		'            \"errorMessage\":\"\"'+
		'         },'+
		'         {  '+
		'            \"courseConnectionId\":\"\",'+
		'            \"result\":\"\",'+
		'            \"html_url\":\"\",'+
		'            \"errorMessage\":\"\"'+
		'         }'+
		'      ]'+
		'   }'+
		'}';
		CourseConnCanvasResponseWrapper obj = CourseConnCanvasResponseWrapper.parse(json);
		System.assert(obj != null);
	}
}