/*
 * @description This interface defines methods to create a new user in 
 * the identity provider
 * @group Identity Management
 */
// Global state is required as the page referencing this class
// will be inside an iframe
@SuppressWarnings('PMD.AvoidGlobalModifier')
global interface IdentityProviderUserRequest {

    /**
     * @description Sets the user's first name for this request
     * @param firstName The user's first name
     */
    void setFirstName(String firstName);

    /**
     * @description Sets the user's last name for this request
     * @param lastName The user's last name
     */
    void setLastName(String lastName);

    /**
     * @description Sets the user's email for this request
     * @param email The user's email
     */
    void setEmail(String email);

    /**
     * @description Sets the user's mobile for this request
     * @param mobile The user's mobile
     */
    void setMobile(String mobile);

    /**
     * @description Sets the user's password for this request
     * @param password The user's password in plain text
     */
    void setPassword(String password);

    /**
     * @description Creates a user in the Identity Provider based on the values set.
     */
    void createUser();

    /**
     * @description Retrieves the Federation Id for the user created
     */
    String getFederationId();
}