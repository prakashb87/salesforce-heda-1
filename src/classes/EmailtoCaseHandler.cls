/***************************************************************************************
 *** @Class             : EmailtoCaseHandler
 *** @Author            : Shubham Singh
 *** @Requirement       : To handle the incoming mails and avoid spam mails
 *** @Created date      : 08/01/2018
 ***********************************************************************************************/
/************************************************************************************************
 *** @About Class
 *** This class is used in Email Services to handle the incoming mails and create or update 
 *** the cases based on them. Also, it will not allow spam emails from domain specified as aspam
 ***********************************************************************************************/
@SuppressWarnings('PMD.AvoidGlobalModifier')
global without sharing class EmailtoCaseHandler implements Messaging.InboundEmailHandler {
    global Messaging.InboundEmailResult handleInboundEmail(Messaging.inboundEmail email,
        Messaging.InboundEnvelope env) {
        Messaging.InboundEmailResult result = new Messaging.InboundEmailResult();
        try {
            //Identify the spam mails 
            String emailAddress = email.fromAddress;
            EmailToCaseHandlerUtility etc = new EmailToCaseHandlerUtility();
            //use contains
            List < Spam_Mails__c > mcs = Spam_Mails__c.getall().values();
                for (Integer i = 0; i < mcs.size(); i++) {
                    if (emailAddress.contains(mcs[i].Spam_Mails__c)) {
                        result.success = false;
                        return result;
                    }
                }
            List < Contact > vCon = new List < Contact > ([SELECT Id, Name, Email FROM Contact
                WHERE Email =: email.fromAddress OR hed__UniversityEmail__c =: email.fromAddress OR hed__WorkEmail__c =: email.fromAddress OR Home_Email__c =: email.fromAddress OR Latest_Email__c =: email.fromAddress LIMIT 1
            ]);

            //If there are any contacts then find if any previous case realted to that contact with same subject
            if (vCon.size() > 0 && vCon != null) {
                List <case > caseCon = new List <case >([select id, ContactId, CaseNumber, Subject, Description from
                        case where ContactId =: vCon.isEmpty() ? ' ': vCon[0].id
                    ]);
                List<case> replyCase = new List<case>();
                for(case allCase: caseCon){
                    String emailSubject = email.subject;                                 
                    if(emailSubject.containsIgnoreCase(allCase.Subject) && ((emailSubject.startsWithIgnoreCase('Re: ')) || (emailSubject.startsWithIgnoreCase('Fwd: ')))){
                        replyCase.add(allCase);
                    }
                }
                //if no case found then create new case and relate it to the respective contact
                if (replyCase.size() == 0) {
                     List<case> lcase = new List<case>();//to pass null case
                     etc.createCaseOrTask(vCon,lcase,email);
                } else if (replyCase.size() > 0) {
                     etc.createCaseOrTask(vCon,replyCase,email);
                } 
            }else if (vCon.size() == 0) {
                //if there are no contacts with mail as email from address try to find any related previous case with same subject and email address
                List <case > lstCase = new List <case >([select id,subject,ContactId from
                        case where (ContactId = null AND SuppliedEmail =: email.fromAddress)
                 ]);
                List<case> replyCase = new List<case>();
                for(case allCase: lstCase){
                    String emailSubject = email.subject;
                    if(emailSubject.containsIgnoreCase(allCase.Subject) && ((emailSubject.startsWithIgnoreCase('Re: ')) || (emailSubject.startsWithIgnoreCase('Fwd: ')))){
                        replyCase.add(allCase);
                    }
                }
                if (replyCase.size() == 0) {
                    //if there are no case found then find the contact which matches with the first name & last name of incoming mail
                    //null check
                    if(email.fromName != null){
                        String str = email.fromName;//singh,shubham shubham singh
                        String[] name = str.replaceAll(',', '').split(' ');
                        List < contact > newCon = new List < Contact > ([select id, Name, Email from contact
                            where((LastName =: name[1] AND FirstName =: name[0]) OR(LastName =: name[0] AND FirstName =: name[1])) OR Name =: email.fromName Limit 1
                        ]);
                    if (newCon.size() > 0) {
                        // If contact is found then create a new case and update the latest email of contact
                        Contact con = new Contact(Id = newCon.isempty() ? null : newCon[0].id);
                        con.Latest_Email__c = email.fromAddress;
                        update con;
                        List<case> lcase = new List<case>();//to pass null casse
                        etc.createCaseOrTask(newCon,lcase,email);
                        //seprate method for case creation
                        }else {
                        //if no contact found then create a case
                        List<Contact> lcon = new List<Contact>();//to pass null contact
                        List<case> lcase = new List<case>(); //to pass null case
                        etc.createCaseOrTask(lcon,lcase,email);
                        }
                    }else {
                        List<Contact> lcon = new List<Contact>();//to pass null contact
                        List<case> lcase = new List<case>(); //to pass null case
                        etc.createCaseOrTask(lcon,lcase,email);
                    }
                }else{
                    List<Contact> lcon = new List<Contact>();//to pass null contact
                    etc.createCaseOrTask(lcon,replyCase,email);
                }
            }
        }
        // If an exception occurs when the query accesses 
        // the contact record, a QueryException is called.
        // The exception is written to the Apex debug log.
        catch (QueryException e) {
            result.success = false;

        }
        //if everything is fine then return result as success
        result.success = true;
        return result;

    }
}