/*********************************************************************************
 *** @ClassName         : FutureChange_Generic_Batch_Helper_New
 *** @Author            : Shubham Singh
 *** @Requirement       : RM-1632
 *** @Created date      : 24/09/2018
 *** @Modified by       : Shubham Singh
 *** @modified date     : 25/09/2018
 **********************************************************************************/
public without sharing class FutureChange_Generic_Batch_Helper_New {
    public void updateObjectRecords(List < SObject > objectRecords, string ObjectName) {
        //Map to store field api name
        Map < String, String > finalMap = new Map < String, String > ();
        //to get data type of fields
        Schema.DisplayType fielddataType;
        //to store data type of fields
        map < string, Schema.DisplayType > mapDataType = new map < string, Schema.DisplayType > ();
        //schema map objects
        Map < String, Schema.SObjectType > schemaMap = Schema.getGlobalDescribe();
        //Schema for an Object
        Schema.SObjectType objectSchema = schemaMap.get(ObjectName);
        //field map of an object to store its fields
        Map < String, Schema.SObjectField > fieldMap = objectSchema.getDescribe().fields.getMap();
        //new object
        sObject sObj;
        //Assining future change obejct
        sobject object_instance;

        //Loop to iteratre through field names
        for (String fieldName: fieldMap.keySet()) {
            fielddataType = fieldMap.get(fieldName).getDescribe().getType();
            finalMap.put(fieldName.toUpperCase(), fieldName);
            //SYSTEM.DEBUG('fieldName'+fieldName);
            mapDataType.put(fieldName.toUpperCase(), fielddataType);
        }

        //creation and assignment of new object        
        Schema.SObjectType sObjectType = objectRecords.getSObjectType();

        //to store records for update of applied value
        List < SObject > futureUpdate = new List < SObject > ();


        //to update the parent from future object
        Map < Id, SObject > futureMap = new Map < Id, SObject > ();
        Map < Id, SObject > checkSuccessMap = new Map < Id, SObject > ();

        //iterate through records
        for (SObject record: objectRecords) {
            //create new sobject instance
            object_instance = sObjectType.newSObject();
            //assigning the record to object_instance for udpate
            object_instance = record;
            //store apiname
            string apiName = (string) object_instance.get('API_Field_Name__c');
            //store value
            string value = (string) object_instance.get('Value__c');
            //looping in future change records
            //check if finalMap is null and the api of field and future object record mathches
            if (finalMap.get(apiName.toUpperCase()) != null && (apiName.toUpperCase()) == finalMap.get(apiName.toUpperCase())) {
                
                if (ObjectName != null && ObjectName.length() > 0 && (!(futureMap.containsKey((ID) object_instance.get('Parent_Lookup__c'))))) {
                    sObj = Schema.getGlobalDescribe().get(ObjectName).newSObject();
                    sObj.put(finalMap.get('ID'), object_instance.get('Parent_Lookup__c'));
                }

                //setObjectId.add((ID)object_instance.get('Parent_Lookup__c'));

                //Creation of object instance starts
                object_instance.put('Applied__c', TRUE);

                //if the data type of field is not date
                if (String.valueOf(mapDataType.get(apiName.toUpperCase())) != 'DATE') {
                    sObj.put(finalMap.get(apiName.toUpperCase()), value);
                }else {
                    sObj.put(finalMap.get(apiName.toUpperCase()), Date.Valueof(value));
                }
            }
            
            //add the parent record for update
            futureMap.put((ID) object_instance.get('Parent_Lookup__c'), sObj);
            
            checkSuccessMap.put((ID) object_instance.get('Parent_Lookup__c'), object_instance);
            
            //add the future object record for update
            futureUpdate.add(object_instance);
        }

        try {
            List < SObject > ListFutureRecord = new List < SObject > ();
            List < Database.SaveResult > srList = new List < Database.SaveResult > ();
            if (futureMap != null && futureMap.size() > 0) {
                System.debug('Future Map-------------> ' + futureMap);
                srList = Database.update(futureMap.values(), False);
            }
            System.debug('checkSuccessMap-------------> ' + checkSuccessMap);
            for (Database.SaveResult srrec: srList) {
                if (srrec.isSuccess()) {
                    ListFutureRecord.add(checkSuccessMap.get(srrec.getId()));
                } else {
                    System.debug('Errors caused---> ' + srrec.errors);
                }
            }
            System.debug('ListFutureRecord-------------> ' + ListFutureRecord);
            if (ListFutureRecord != null && ListFutureRecord.size() > 0) {
                Database.update(ListFutureRecord, False);
            }

        } catch (Exception e) {
            System.debug('Exception------->' + e.getMessage());
        }
    }
}