/*******************************************
 Purpose: Helper class for ViewMyEthicsListController
 History:
 Created by Ankit Bhagat on 12/09/2018
 *******************************************/

public with sharing class ViewMyEthicsHelper {

	public static final string NO_ACCESS_ROLE = label.RSTP_Role_No_Access;

     //@AuraEnabled public static List<Researcher_Member_Sharing__c> reseacherMemberSharingList{get;set;}
 
    /************************************************************************************
    // Purpose      :  To get the list of Ethics
    // Parameters   :  Null     
    // Developer    :  Ankit Bhagat 
    // Created Date :  12/09/2018                 
    //***********************************************************************************/
    @AuraEnabled    
    public static List<EthicsDetailWrapper.ethicsWrapper> getEthicsWrapperList(){
        
        List<EthicsDetailWrapper.ethicsWrapper> ethicsWrapperList = new List<EthicsDetailWrapper.ethicsWrapper>();
        Set<Id> ethicsIds = new Set<Id>();
        Set<Id> projectIds = new Set<Id>(); 
        
        List<Ethics__c> ethicsList = new List<Ethics__c>();
        List<Ethics_Researcher_Portal_Project__c> ethicsProjectList = new List<Ethics_Researcher_Portal_Project__c>();
        
        ethicsIds = getEthicsIds(userinfo.getuserid()); 
   
        projectIds = getProjectIds(); 
        
        ethicsList = getEthicsList(ethicsIds);
        
        ethicsProjectList = getEthicsProjectList(ethicsIds, projectIds);
         
        ethicsWrapperList = getEthicsWrapperList(ethicsProjectList, ethicsList); 
        
        return ethicsWrapperList;
    }
    
    
    /************************************************************************************
    // Purpose      :  To get the Search list of Ethics
    // Parameters   :  searchText    
    // Developer    :  Ankit Bhagat
    // Created Date :  12/09/2018                   
    //***********************************************************************************/
    @AuraEnabled    
    public static List<EthicsDetailWrapper.ethicsWrapper> getEthicsSearchWrapperList(String searchText){

        List<EthicsDetailWrapper.ethicsWrapper> ethicsWrapperList = new List<EthicsDetailWrapper.ethicsWrapper>();
        
        Set<Id> ethicsIds = new Set<Id>();
        Set<Id> masterEthicsIds = new Set<Id>();
        Set<Id> ethicProjectsIds = new Set<Id>();
        Set<Id> projectsIds = new Set<Id>();
        Set<Id> masterProjectIds = new Set<Id>();
       
        ethicsIds = getEthicsIds(userinfo.getuserid());
        
        projectsIds = getProjectIds();
        
        List<Ethics__c> ethicsList = new List<Ethics__c>();
        List<Ethics__c> masterEthicsList = new List<Ethics__c>();
        
        List<Ethics__c> ethicsListSearch = new List<Ethics__c>();
        List<ResearcherPortalProject__c>  projectListSearch =new List<ResearcherPortalProject__c>();
        List<Ethics_Researcher_Portal_Project__c> ethicsProjectList = new List<Ethics_Researcher_Portal_Project__c>();
        
        ethicsList = getEthicsList(ethicsIds);
        ethicsProjectList = getEthicsProjectList(ethicsIds, projectsIds);
        
             
 
        String searchEthicsProjectString = '*'+searchText+'*';
        String searchQuery = 'FIND \'' + searchEthicsProjectString + '\' IN All FIELDS  RETURNING  Ethics__c (Id,name,Application_Title__c,Application_Type__c,Status__c,Approved_Date__c,Expiry_Date__c where Id IN : ethicsIds) , ResearcherPortalProject__c(Id,Title__c where Id In: projectsIds) ';
        List<List <sObject>> searchList = search.query(searchQuery);
        
        ethicsListSearch  = ((List<Ethics__c>)searchList[0]);
        projectListSearch = ((List<ResearcherPortalProject__c>)searchList[1]);
         
        for(Ethics__c eachEthics : ethicsListSearch){
                
            if( (eachEthics.Application_Title__c.containsIgnoreCase(searchText)) ) {
                    masterEthicsList.add(eachEthics);
                    masterEthicsIds.add(eachEthics.Id);
                }
            }
                
        for(ResearcherPortalProject__c eachproject : projectListSearch){
                
            if(eachproject.Title__c.containsIgnoreCase(searchText)){
                    masterProjectIds.add(eachproject.Id);
                }  
            }
                        
        List<Ethics_Researcher_Portal_Project__c> masterEthicsProjectList = new List<Ethics_Researcher_Portal_Project__c>();
        if((Schema.sObjectType.Ethics_Researcher_Portal_Project__c.isAccessible()) && (Schema.sObjectType.Ethics_Researcher_Portal_Project__c.fields.Ethics__c.isAccessible())){   //added by ali PMD voilation
        masterEthicsProjectList = [Select id, name, Ethics__c, Ethics__r.Application_Title__c , Ethics__r.Application_Type__c,
                                    Ethics__r.Status__c, Ethics__r.Approved_Date__c, Ethics__r.Expiry_Date__c,
                                    Researcher_Portal_Project__r.Title__c, Researcher_Portal_Project__r.Id
                                    From  Ethics_Researcher_Portal_Project__c
                                    where  Ethics__c IN : ethicsIds
                                    AND Researcher_Portal_Project__c IN : projectsIds
                                    AND (Ethics__c IN : masterEthicsIds OR Researcher_Portal_Project__c IN : masterProjectIds)];    
        
        ethicsWrapperList = getEthicsWrapperList(masterEthicsProjectList, masterEthicsList);
        }
        System.debug('@@@ethicsWrapperList'+ethicsWrapperList);
        return ethicsWrapperList;
    }    
    
 
    /************************************************************************************
    // Purpose      :  To get the list of Ethics as per Logged In User
    // Parameters   :  Set of Ethics Ids   
    // Developer    :  Ankit Bhagat
    // Created Date :  12/09/2018                   
    //***********************************************************************************/
    public static List<Ethics__c> getEthicsList( Set<Id> ethicsIds){
        
        List<Ethics__c> ethicsList = new List<Ethics__c>();
        if((Schema.sObjectType.Ethics__c.isAccessible()) && (Schema.sObjectType.Ethics__c.fields.Application_Title__c.isAccessible())){  //added by ali PMD voilation
            ethicsList = [Select id, Application_Title__c, Application_Type__c, Status__c, Approved_Date__c, Expiry_Date__c
                          From Ethics__c where Id IN: ethicsIds ];
        }
        return ethicsList;
        
        
    }
    /************************************************************************************
    // Purpose      :  To get the list of Ethics Projects as per Logged In User
    // Parameters   :  Set of Ethics Ids   
    // Developer    :  Ankit Bhagat
    // Created Date :  12/09/2018                   
    //***********************************************************************************/
    public static List<Ethics_Researcher_Portal_Project__c> getEthicsProjectList(Set<Id> ethicsIds , Set<Id> projectIds){
    
        List<Ethics_Researcher_Portal_Project__c> ethicsProjectList = new List<Ethics_Researcher_Portal_Project__c>();
        if((Schema.sObjectType.Ethics_Researcher_Portal_Project__c.isAccessible()) && (Schema.sObjectType.Ethics_Researcher_Portal_Project__c.fields.Ethics__c.isAccessible())){  //added by ali PMD voilation
        ethicsProjectList = [Select id, name, Ethics__c, Ethics__r.Application_Title__c , Ethics__r.Application_Type__c,
                                    Ethics__r.Status__c, Ethics__r.Approved_Date__c, Ethics__r.Expiry_Date__c,
                                    Researcher_Portal_Project__r.Title__c, Researcher_Portal_Project__r.Id
                                    From  Ethics_Researcher_Portal_Project__c
                                    where Ethics__c IN : ethicsIds
                                    AND Researcher_Portal_Project__c IN : projectIds]; 
        }    
        return ethicsProjectList;
    }   
             
    
    /************************************************************************************
    // Purpose      :  To get the Wrapper list of Ethics
    // Parameters   :  EthicsProjects List, Ethics List   
    // Developer    :  Ankit Bhagat
    // Created Date :  12/09/2018                   
    //***********************************************************************************/
    @AuraEnabled    
    public static List<EthicsDetailWrapper.ethicsWrapper> getEthicsWrapperList(List<Ethics_Researcher_Portal_Project__c> masterEthicsProjectList, List<Ethics__c> masterEthicsList){
        
        Set<Id> ethicsIds = new Set<Id>();
        Map<Id,String> ethicsidAndPositionMap = new  Map<Id,String>();
        List<Researcher_Member_Sharing__c> reseacherMemberSharingList = new List<Researcher_Member_Sharing__c>();
        
        ethicsIds = getEthicsIds(userinfo.getuserid());
        Id ethicsRecordTypeId = Schema.SObjectType.Researcher_Member_Sharing__c.getRecordTypeInfosByName().get('Ethics').getRecordTypeId();
        reseacherMemberSharingList = [SELECT Id,Name,Ethics__c,Position__c FROM Researcher_Member_Sharing__c 
                                      WHERE RecordTypeId =: ethicsRecordTypeId 
									  AND User__c =: userinfo.getuserid()
									  AND Ethics__c IN: ethicsIds 
                                      AND RM_Member__c=true 
									  AND AccessLevel__c !=: NO_ACCESS_ROLE];
  
        for(Researcher_Member_Sharing__c  rmsobj : reseacherMemberSharingList){
            
            ethicsidAndPositionMap.put(rmsobj.Ethics__c, rmsobj.Position__c);
        }
        system.debug('@@@ethicsidAndPositionMapvalue'+ethicsidAndPositionMap);                          
        
        List<EthicsDetailWrapper.ethicsWrapper> ethicsWrapperList = new List<EthicsDetailWrapper.ethicsWrapper>();
        Set<Id> addedEthicIds = new Set<Id>();
        
        for(Ethics_Researcher_Portal_Project__c  ethicsproject : masterEthicsProjectList) {
            
            EthicsDetailWrapper.ethicsWrapper wrapperObj = new EthicsDetailWrapper.ethicsWrapper(ethicsproject);
            
            wrapperObj.position=ethicsidAndPositionMap.get(ethicsproject.Ethics__c);
            
            ethicsWrapperList.add(wrapperObj);
            addedEthicIds.add(ethicsproject.Ethics__c);
        }
        
        
        for(Ethics__c ethics: masterEthicsList){
            
            if(!addedEthicIds.contains(ethics.id)){
                
                EthicsDetailWrapper.ethicsWrapper wrapperObj = new EthicsDetailWrapper.ethicsWrapper(ethics);
                wrapperObj.position=ethicsidAndPositionMap.get(ethics.id);
                
                ethicsWrapperList.add(wrapperObj);
            }
        }         
        System.debug('@@@ethicsWrapperList'+ethicsWrapperList);
        return ethicsWrapperList;
    }
  
    /*****************************************************************************************
    // <JIRA NO?>   :  
    // SPRINT       :  
    // Purpose      :  
    // Parameters   :    
    // Developer    :  
    // Created Date :                  
   //****************************************************************************************/ 
    public static Set<Id> getEthicsIds(Id userId){ 

        Set<Id> ethicsIds = new Set<Id>();
        Set<Id> projectIds = new Set<Id>();
        
        List<Researcher_Member_Sharing__c> reseacherMemberSharingList = new List<Researcher_Member_Sharing__c>();
        reseacherMemberSharingList = getEthicsMemberSharing(userId);
        /*
        reseacherMemberSharingList = [SELECT id, Contact__c, Ethics__c, Account__c, Publication__c,Researcher_Portal_Project__c
                                      FROM Researcher_Member_Sharing__c 
                                      WHERE User__c =: userId
                                      AND Ethics__c!=null
                                      AND RM_Member__c=true 
                                      AND Currently_Linked_Flag__c =true  // Added by Subhajit for US-198 [They should also not be able to view the ethics records related to this project from the Ethics tab anymore]
                                      ]; */
         
        System.debug('@@@reseacherMemberSharingList'+reseacherMemberSharingList);
        
        if(! reseacherMemberSharingList.isEmpty()){
             for(Researcher_Member_Sharing__c rms:reseacherMemberSharingList){
               ethicsIds.add(rms.Ethics__c);
             }
        }
  
        System.debug('@@@ethicsIds'+ethicsIds);

        return ethicsIds;
    }
    
    
    
    /*****************************************************************************************
    // <JIRA NO?>   :  
    // SPRINT       :  
    // Purpose      :  
    // Parameters   :    
    // Developer    :  
    // Created Date :                  
   //****************************************************************************************/ 
    public static List<Researcher_Member_Sharing__c>  getEthicsMemberSharing(Id userId){ 

        List<Researcher_Member_Sharing__c> reseacherMemberSharingList = new List<Researcher_Member_Sharing__c>();
        Id ethicsRecordTypeId = Schema.SObjectType.Researcher_Member_Sharing__c.getRecordTypeInfosByName().get('Ethics').getRecordTypeId();
        if((Schema.sObjectType.Researcher_Member_Sharing__c.isAccessible()) && (Schema.sObjectType.Researcher_Member_Sharing__c.fields.Contact__c.isAccessible())){  //added by ali PMD voilation
        reseacherMemberSharingList = [SELECT id, Contact__c, Ethics__c, Account__c, Publication__c,Researcher_Portal_Project__c
                                      FROM Researcher_Member_Sharing__c 
                                      WHERE user__c =: userinfo.getUserid()
                                      AND RecordTypeId =: ethicsRecordTypeId
                                      AND RM_Member__c=true 
                                      AND Currently_Linked_Flag__c =true  // Added by Subhajit for US-198 [They should also not be able to view the ethics records related to this project from the Ethics tab anymore]
                                      AND AccessLevel__c !=: NO_ACCESS_ROLE];
         
        }
        return reseacherMemberSharingList;
    }
    
    
   /*****************************************************************************************
    // <JIRA NO?>   :  
    // SPRINT       :  
    // Purpose      :  
    // Parameters   :    
    // Developer    :  
    // Created Date :                  
   //****************************************************************************************/ 
    public static Set<Id> getProjectIds(){ 

        Set<Id> projectIds = new Set<Id>();
        List<Researcher_Member_Sharing__c> reseacherMemberSharingList = new List<Researcher_Member_Sharing__c>();
        
        Id projectRecordTypeId = Schema.SObjectType.Researcher_Member_Sharing__c.getRecordTypeInfosByName().get(Label.RSTP_ResearchProjectRecordtype).getRecordTypeId();
        Set<Id> recordTypeIdSet = new Set<Id>{projectRecordTypeId};
        reseacherMemberSharingList = ResearcherMemberSharingUtils.geResearcherMemberSharingList(recordTypeIdSet, userinfo.getUserId());
        System.debug('@@@reseacherMemberSharingList'+reseacherMemberSharingList);
         
         for(Researcher_Member_Sharing__c rms:reseacherMemberSharingList){
            if(rms.Researcher_Portal_Project__c!=null){
                projectIds.add(rms.Researcher_Portal_Project__c);
            }
         }
      
  
        System.debug('@@@projectIds'+projectIds);

        return projectIds;
    }    

    /*****************************************************************************************
    // <JIRA NO?>   :  
    // SPRINT       :  
    // Purpose      :  
    // Parameters   :    
    // Developer    :  
    // Created Date :                  
   //****************************************************************************************/ 
   public static List<EthicsDetailWrapper.ethicsWrapper> getProjectEthicsWrapperHelper(Id projectId ){
        
        Set<Id> ethicsIds = new Set<Id>();
        List<EthicsDetailWrapper.ethicsWrapper> projectEthicsWrapperList = 
            new List<EthicsDetailWrapper.ethicsWrapper>();
        
        List<Ethics_Researcher_Portal_Project__c> ethicsProjectList = new List<Ethics_Researcher_Portal_Project__c>();
        ethicsProjectList = ApexWithoutSharingUtils.getResearcherEthicsRecords(ProjectId);
        /*ethicsProjectList = [Select id, name, Researcher_Portal_Project__c,Ethics__c
                             From  Ethics_Researcher_Portal_Project__c
                             where Researcher_Portal_Project__c  =: ProjectId];*/
        System.debug('@@@ethicsProjectList'+ethicsProjectList);
        
        for(Ethics_Researcher_Portal_Project__c eachEthicsProject : ethicsProjectList){
            ethicsIds.add(eachEthicsProject.Ethics__c);
        }
        
        List<Ethics__c> ethicsList = new List<Ethics__c>();
       
        ///ethicsList = getEthicsList(ethicsIds);
       /* ethicsList = [Select id, Application_Title__c, Application_Type__c, Status__c, Approved_Date__c, Expiry_Date__c
                      From Ethics__c where Id IN: ethicsIds]; */
        ethicsList = ApexWithoutSharingUtils.getEthicsList(ethicsIds);
        
        for(Ethics__c eachethics : ethicsList){
            
            EthicsDetailWrapper.ethicsWrapper wrapperObj = new EthicsDetailWrapper.ethicsWrapper(eachethics);
            
            
            projectEthicsWrapperList.add(wrapperObj); 
        }
        
        System.debug('@@@ProjectEthicsWrapperList'+projectEthicsWrapperList);
        return projectEthicsWrapperList;
        
    }
    
  
    /*****************************************************************************************
    // Purpose      :  Searching Ethics from project detail Page
    // Parameters   :  String  
    // Developer    :  Ankit
    // Created Date :  09/05/2018   
    // Modified By  :  Subhajit  
    // Modified Date: 11/06/2018         
    //****************************************************************************************/
        public static List<EthicsDetailWrapper.ethicsWrapper> searchEthicswithTitleHelper(String searchText, Boolean isAddEthicsPopup ){
        
        System.debug('@@@searchText==>'+searchText);
        Set<Id> ethicsIds = new Set<Id>();
        Set<Id> searchEthicsIds = new Set<Id>();
        Set<Id> masterEthicsIds  = new Set<Id>();
        List<Researcher_Member_Sharing__c> reseacherMemberSharingList = new List<Researcher_Member_Sharing__c>();
        
        List<Ethics__c> masterEthicsList = new List<Ethics__c>();
        
        //Id ethicsRecordTypeId = Schema.SObjectType.Researcher_Member_Sharing__c.getRecordTypeInfosByName().get('Ethics').getRecordTypeId();
         
        reseacherMemberSharingList = getEthicsMemberSharing(userinfo.getuserid());
        /*    
        reseacherMemberSharingList = [SELECT id, Contact__c, Ethics__c, Account__c, Publication__c,Researcher_Portal_Project__c
                                      FROM Researcher_Member_Sharing__c 
                                      WHERE user__c =: userinfo.getUserid()
                                      AND RecordTypeId =: ethicsRecordTypeId
                                      AND RM_Member__c=true 
                                      AND Currently_Linked_Flag__c =true
                                     ]; */
            
        // Add RECORD TYPR OF ETHICS
        system.debug('@@@reseacherMemberSharingList ' + JSON.serializePretty(reseacherMemberSharingList));                                      
        
        if(! reseacherMemberSharingList.isEmpty()){
            for(Researcher_Member_Sharing__c rms:reseacherMemberSharingList){
                ethicsIds.add(rms.Ethics__c);
            }
        }
        System.debug('@@@ethicsIds'+ethicsIds);
        
        List<EthicsDetailWrapper.ethicsWrapper> projectEthicsWrapperList = 
            new List<EthicsDetailWrapper.ethicsWrapper>();
        List<Ethics__c> searchEthicsList = New List<Ethics__c>();
        
        if(isAddEthicsPopup == false){
            String searchStr1 = '*'+searchText+'*';
            String searchQuery = 'FIND \'' + searchStr1 + '\' IN ALL FIELDS  RETURNING  Ethics_Researcher_Portal_Project__c , Ethics__c (Id,name,Application_Title__c,Application_Type__c,Status__c,Approved_Date__c,Expiry_Date__c) , ResearcherPortalProject__c(Id,Title__c) ';
            List<List <sObject>> searchList = search.query(searchQuery);
            
            System.debug('@@@searchList'+JSON.serializePretty(searchList));
            searchEthicsList  = ((List<Ethics__c>)searchList[1]);   
                
            System.debug('@@@searchEthicsList'+JSON.serializePretty(searchEthicsList));
            
            for(Ethics__c ethics: searchEthicsList){
                if(ethics.Application_Title__c!=null && ethics.Application_Title__c.containsIgnoreCase(searchText)){
                    searchEthicsIds.add(ethics.Id);
                }
            } 
            
            System.debug('@@@searchEthicsIds'+JSON.serializePretty(searchEthicsIds));
            
            for(Id eachethicsId : searchEthicsIds){
                if(ethicsIds.contains(eachethicsId)){
                    masterEthicsIds.add(eachethicsId);
                }
            }
            System.debug('@@@masterEthicsIds'+masterEthicsIds);
            
            
            masterEthicsList = getEthicsList(masterEthicsIds);
        }
        else
        {
            masterEthicsList = getEthicsList(ethicsIds);
        }
       
        
        /*masterEthicsList = [Select Id,name,Application_Title__c,Application_Type__c,Status__c,Approved_Date__c,Expiry_Date__c
                            From Ethics__c where ID IN : masterEthicsIds]; */
        System.debug('@@@masterEthicsList'+masterEthicsList);
        
        for(Ethics__c ethics: masterEthicsList){
            
            
            EthicsDetailWrapper.ethicsWrapper wrapperObj = new EthicsDetailWrapper.ethicsWrapper(ethics);
            
            
            projectEthicsWrapperList.add(wrapperObj);
            
        } 
        System.debug('@@@ProjectEthicsWrapperList'+projectEthicsWrapperList);
        return projectEthicsWrapperList;
        
    }
    
     /*****************************************************************************************
    // <JIRA NO?>   :  
    // SPRINT       :  
    // Purpose      :  
    // Parameters   :    
    // Developer    :  
    // Created Date :                  
    //****************************************************************************************/
    
    public static List<EthicsDetailWrapper.ethicsWrapper> addEthicsProjectRecord(List<Id> ehicsIdList, Id projectId ){
        List<EthicsDetailWrapper.ethicsWrapper> projectEthicsWrapperList = new List<EthicsDetailWrapper.ethicsWrapper>();
        List<Ethics_Researcher_Portal_Project__c> ethicsProjectList = new List<Ethics_Researcher_Portal_Project__c>();
      
        
        //Preparing Ethics_Researcher_Portal_Project__c junction object records for Ethics to projects
        if(ehicsIdList!= null && !ehicsIdList.isEmpty())
        {
            for (Id ethics : ehicsIdList)
            {
            
                Ethics_Researcher_Portal_Project__c ethicsProject = new Ethics_Researcher_Portal_Project__c();
                ethicsProject.Ethics__c = ethics;
                ethicsProject.Researcher_Portal_Project__c = projectId;
                ethicsProjectList.add(ethicsProject);
                
                // No need for ethicsshare  ,Instead of Ethics__Share, add RMS redords with Record types as ethics and RM Member as false - Done
                // need to check if some ethics already given acess then no need to again add RMS records. - Need to Do 
            }  
        }
        
        // Inserting Ethics_Researcher_Portal_Project__c junction object records
        if(ethicsProjectList!=null && ethicsProjectList.size() > 0){
			if (!Schema.sObjectType.Ethics_Researcher_Portal_Project__c.isCreateable()){
				System.debug('Insufficient Access');
				//return null;
			}
            insert ethicsProjectList;
        }
         projectEthicsWrapperList= getProjectEthicsWrapperHelper(projectId);      
        System.debug('@@@ProjectEthicsWrapperList'+JSON.serializePretty(projectEthicsWrapperList));
        return projectEthicsWrapperList;
    }
   
     /*****************************************************************************************
    // <JIRA NO?>   :  
    // SPRINT       :  
    // Purpose      :  
    // Parameters   :    
    // Developer    :  
    // Created Date :                  
    //****************************************************************************************/
    public static void deleteEthicsProjectHelper(Id ethicsId, Id projectId ){
        
         
        List<Ethics_Researcher_Portal_Project__c> ethicsProjectList = new List<Ethics_Researcher_Portal_Project__c>();
         
         
        Set<Id> ethicsIdSet = new Set<Id>{ethicsId};
        Set<Id> projectIdSet = new Set<Id>{projectId};
        
        //Calling common class for PMD, Ankit 
        ethicsProjectList = getEthicsProjectList(ethicsIdSet , projectIdSet);
        /*  
        ethicsProjectList = [Select id, name, Researcher_Portal_Project__c,Ethics__c
                             From  Ethics_Researcher_Portal_Project__c
                             where Researcher_Portal_Project__c  =: projectId
                             And Ethics__c =: EthicsId]; */
                             
        System.debug('@@@ethicsProjectList'+ethicsProjectList);
        
        if(ethicsProjectList!=null && ethicsProjectList.size() > 0){
                ApexWithoutSharingUtils.deleteEthicsProjectRecords(ethicsProjectList);
        }
    
    } 
   
  
}