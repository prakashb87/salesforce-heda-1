/*********************************************************************************
*** @TestClassName     : HECS_REST_Test
*** @Author            : Shubham Singh
*** @Requirement       : To Test the HECS_REST web service apex class.
*** @Created date      : 14/08/2018
**********************************************************************************/
@isTest
public class HECS_REST_Test {
    static testMethod void myUnitTest() {
        RestRequest req = new RestRequest();
        
        RestResponse res = new RestResponse();
        // adding header to the request
        req.addHeader('httpMethod', 'POST');
        
        req.requestUri = '/services/apexrest/HEDA/v1/HECS';
        
        //creating test data
        String JsonMsg = '{"LoanType": "F","effectiveDate": "2018-01-01","effectiveStatus": "A","Description": "Dummy HECS CHANGED","CommonwealthAssisted": "N","HecsExemptStatusCode": "07","ShortDescription": "Dummy CHANGED","HecsCodeType": "N","LongDescription": "CHANGED HECS code for correcting incorrect stduent accounts.  Can be deleted after all updates have been made ie around 6/6/2002."}';
        
        req.requestBody = Blob.valueof(JsonMsg);
        
        RestContext.request = req;
        
        RestContext.response = res;
        
        Test.startTest();
        //store response
        Map < String, String > check = HECS_REST.upsertHECS();
        List < String > str = check.values();
        //assert condition 
        System.assertEquals(True, str[0] == 'ok');
        
        Test.stopTest();
        
    }
    static testMethod void coverError() {
        RestRequest req = new RestRequest();
        
        RestResponse res = new RestResponse();
        // adding header to the request
        req.addHeader('httpMethod', 'POST');
        
        req.requestUri = '/services/apexrest/HEDA/v1/Location';
        
        //creating test data
        String JsonMsg = '{"LoanType": "F","effectiveDate": "2018-01-01","effectiveStatus": "A","Description": "Dummy HECS CHANGED","CommonwealthAssisted": "N","HecsExemptStatusCode": "","ShortDescription": "Dummy CHANGED","HecsCodeType": "N","LongDescription": "CHANGED HECS code for correcting incorrect stduent accounts.  Can be deleted after all updates have been made ie around 6/6/2002."}';
        
        req.requestBody = Blob.valueof(JsonMsg);
        
        RestContext.request = req;
        
        RestContext.response = res;
        
        Test.startTest();
        //store response
        Map < String, String > check = HECS_REST.upsertHECS();
        List < String > str = check.values();
        //assert condition 
        System.assertEquals(False, str[0] == 'ok');
        
        Test.stopTest();
        
    }
    static testMethod void coverTryCatch() {
        Map < String, String > check;
        Test.startTest();
        try {
            //store response
            check = HECS_REST.upsertHECS();
        } catch (DMLException e) {
            List < String > str = check.values();
            //assert condition 
            System.assertEquals(False, str[0] == 'ok');
        }
        Test.stopTest();
    }
    static testMethod void updateMethod() {
        
        HECS__c hcs = new HECS__c();
        
        hcs.Name = '07';
        
        hcs.Effective_Date__c = Date.ValueOf('2017-01-01');
       
        insert hcs;
        
        RestRequest req = new RestRequest();
        
        RestResponse res = new RestResponse();
        // adding header to the request
        req.addHeader('httpMethod', 'POST');
        
        req.requestUri = '/services/apexrest/HEDA/v1/HECS';
        
        //creating test data
        String JsonMsg = '{"LoanType": "F","effectiveDate": "2018-01-01","effectiveStatus": "A","Description": "Dummy HECS CHANGED","CommonwealthAssisted": "N","HecsExemptStatusCode": "07","ShortDescription": "Dummy CHANGED","HecsCodeType": "N","LongDescription": "CHANGED HECS code for correcting incorrect stduent accounts.  Can be deleted after all updates have been made ie around 6/6/2002."}';
        
        req.requestBody = Blob.valueof(JsonMsg);
        
        RestContext.request = req;
        
        RestContext.response = res;
        
        Test.startTest();
        //store response
        Map < String, String > check = HECS_REST.upsertHECS();
        List < String > str = check.values();
        //assert condition 
        System.assertEquals(True, str[0] == 'ok');
        
        Test.stopTest();
        
    }
     static testMethod void updateErrorMethod() {
        
        HECS__c hcs = new HECS__c();
        
        hcs.Name = '07';
        
        hcs.Effective_Date__c = Date.ValueOf('2017-01-01');
       
        insert hcs;
        
        RestRequest req = new RestRequest();
        
        RestResponse res = new RestResponse();
        // adding header to the request
        req.addHeader('httpMethod', 'POST');
        
        req.requestUri = '/services/apexrest/HEDA/v1/HECS';
        
        //creating test data
        String JsonMsg = '{"LoanType": "F","effectiveDate": "2019-01-01","effectiveStatus": "A","Description": "Dummy HECS CHANGED","CommonwealthAssisted": "N","HecsExemptStatusCode": "07","ShortDescription": "Dummy CHANGED","HecsCodeType": "N","LongDescription": "CHANGED HECS code for correcting incorrect stduent accounts.  Can be deleted after all updates have been made ie around 6/6/2002."}';
        
        req.requestBody = Blob.valueof(JsonMsg);
        
        RestContext.request = req;
        
        RestContext.response = res;
        
        Test.startTest();
        //store response
        Map < String, String > check = HECS_REST.upsertHECS();
        List < String > str = check.values();
        //assert condition 
        System.assertEquals(True, str[0] == 'ok');
        
        Test.stopTest();
        
    }
}