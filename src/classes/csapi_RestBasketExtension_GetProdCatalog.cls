/*****************************************************************
Name: CS_CustomLookupPriceItemTest
Author: Pawan [CloudSense]
Purpose: custom API in Salesforce that queries the CloudSense Commercial Product Table
*****************************************************************/
/*==================================================================
History
--------
Version   Author           Date              Detail
1.0       Pawan            10/09/2018         Initial Version
********************************************************************/
@RestResource(urlMapping='/csapi_BasketExtension/GetProductCatalog')
global with sharing class csapi_RestBasketExtension_GetProdCatalog
{  
    @HttpGet
    global static csapi_BasketRequestWrapper.ProductCatalogResponse doGet() {
    
        RestRequest req = RestContext.request;
        RestResponse res = RestContext.response;
        Exception ee = null;
        
        csapi_BasketRequestWrapper.ProductCatalogRequest request;
        csapi_BasketRequestWrapper.ProductCatalogResponse csapiresponse;
        try {
            csapiresponse = csapi_BasketExtension.getProductCatalog(request);
            system.debug('csapiresponse >> ' + csapiresponse );
        } catch(Exception e ) {
            ee = e;
        } finally {
            if(ee != null) 
            {
                system.debug('ee >> ' + ee.getMessage());
            }
        }
        return csapiresponse;
    }
  
  
}