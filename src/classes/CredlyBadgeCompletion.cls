/*****************************************************************************************************
 *** @Class             : CredlyBadgeCompletion
 *** @Author            : Avinash Machineni / Rishabh Anand
 *** @Requirement       : scheduler to send Course Connections associated with the contact
 *** @Created date      : 13/06/2018
 *** @JIRA ID           : ECB-2446
 *** @LastModifiedBy    : Avinash Machineni
 *** @LastModified date : 19/06/2018
 
 *****************************************************************************************************/
/*****************************************************************************************************
 *** @About Class
 *** This class is used as a scheduler to schedule Course ID to Ipass API. It works when we schedule using schedule apex.
 *****************************************************************************************************/ 
public without sharing class CredlyBadgeCompletion 
{
    public static void updateCourseConn(Set<Id> contactIdSet)
    {
        List<hed__Course_Enrollment__c> courseConnList;
        if (Schema.sObjectType.hed__Course_Enrollment__c.isAccessible()){
	        courseConnList  = [SELECT Id , 
	                       											  hed__Contact__c,
	                       										      hed__Contact__r.hed__UniversityEmail__c, 
	                       										      hed__Course_Offering__r.hed__Course__r.Credly_Badge_Id__c,
	                       											  Grade_Date__c,
	                       											  hed__Status__c                                    
	                                                                  FROM hed__Course_Enrollment__c
	                                                                  WHERE hed__Contact__c IN :contactIdSet AND hed__Status__c =: ConfigDataMapController.getCustomSettingValue('CredlyCourseConnectionStatus') AND 
	                                                                  Enrolment_Status__c =: ConfigDataMapController.getCustomSettingValue('CredlyCourseConnectionEnrolmentStatus') AND 
	                                                                  hed__Course_Offering__r.hed__Course__r.RecordType.DeveloperName =: ConfigDataMapController.getCustomSettingValue('CredlyCourseConnectionType')];     
        }        
        set<Id> crsconnids = new set<Id>();
        if(courseConnList.size() > 0)
        {  
            Map<String, List<hed__Course_Enrollment__c>> contactCourseEnrollMap = new  Map<String, List<hed__Course_Enrollment__c>>();
            Map<String,hed__Course_Enrollment__c> courseEnrollMap = new Map<String,hed__Course_Enrollment__c>();
            
            for(hed__Course_Enrollment__c cenroll : courseConnList){
                crsconnids.add(cenroll.Id);
                courseEnrollMap.put(cenroll.Id,cenroll);
                if(contactCourseEnrollMap.containsKey(String.valueOf(cenroll.hed__Contact__r.hed__UniversityEmail__c))){
                    contactCourseEnrollMap.get(String.valueOf(cenroll.hed__Contact__r.hed__UniversityEmail__c)).add(cenroll); 
                }else{
                    List<hed__Course_Enrollment__c> courseenrollList = new List<hed__Course_Enrollment__c>(); 
                    courseenrollList.add(cenroll); 
                    contactCourseEnrollMap.put(String.valueOf(cenroll.hed__Contact__r.hed__UniversityEmail__c),courseenrollList);
                }
                
            }
            
            List<Course_Connection_Life_Cycle__c> courseConnectionLifeCycleList;
            if (Schema.sObjectType.Course_Connection_Life_Cycle__c.isAccessible()){
            	courseConnectionLifeCycleList = [SELECT Id,
                                                                                   Name,
                                                                                   Status__c,
                                                                                   Course_Connection__c,																			  
                                                                                   Course_Connection__r.Enrolment_Status__c,
                                                                                   Course_Connection__r.Name,
                                                                                   Course_Connection__r.Grade_Date__c
                                                                                   FROM Course_Connection_Life_Cycle__c
                                                                                   WHERE Course_Connection__c in : crsconnids AND Stage__c =: ConfigDataMapController.getCustomSettingValue('CredlyCCLifeCycleStage') 
                                                                                   AND (Status__c =: ConfigDataMapController.getCustomSettingValue('CourseConnectionLifeCycleNotStarted') OR Status__c =: ConfigDataMapController.getCustomSettingValue('CourseConnectionLifeCycleError'))];
            }
            
			Map<String, Course_Connection_Life_Cycle__c> courseConnLifeCycleMap = new  Map<String, Course_Connection_Life_Cycle__c>();

                for(Course_Connection_Life_Cycle__c cclyfcycle : courseConnectionLifeCycleList){
                    if(cclyfcycle.Course_Connection__c!=null){
                        courseConnLifeCycleMap.put(cclyfcycle.Course_Connection__c, cclyfcycle);
                    }
                }   
            
            
			if(!contactCourseEnrollMap.isEmpty()){
				callCredlyWSAndUpsert(contactCourseEnrollMap, courseEnrollMap, courseConnLifeCycleMap);
        	}
        }
    }
    
    public static void callCredlyWSAndUpsert(Map<String, List<hed__Course_Enrollment__c>> contactCourseEnrollMap, Map<String,hed__Course_Enrollment__c> courseEnrollMap, Map<String, Course_Connection_Life_Cycle__c> courseConnLifeCycleMap){
    			List<Course_Connection_Life_Cycle__c> listlifecycleinitiated = new List<Course_Connection_Life_Cycle__c>();
    			List<CredlyJsonResponseWrapper> listcredlyjreswrp = new List<CredlyJsonResponseWrapper>();
    			List<Integration_Logs__c> requestLogObjectToInsertList = new List<Integration_Logs__c>();
		        List<Integration_Logs__c> responseLogObjectToInsertList = new List<Integration_Logs__c>(); 
    			
                for(String email : contactCourseEnrollMap.keySet()){
                    CredlyJsonRequestWrapper credlyjswrp =  new CredlyJsonRequestWrapper();
                    credlyjswrp.email = email;
                    credlyjswrp.badgeDetails = getBadgeDetailList(contactCourseEnrollMap.get(email));
                    
                    String requestJSONString = JSON.serialize(credlyjswrp);
                    System.debug('Serialized list of Contacts and Course Connections into JSON format: ' + requestJSONString);
                    String endpoint = ConfigDataMapController.getCustomSettingValue('CourseConnectionCredlyEndPoint');
                    HttpRequest req = new HttpRequest();
                    req.setEndpoint(endpoint);
                    req.setHeader('client_id', ConfigDataMapController.getCustomSettingValue('CourseConnectionCredlyClientId'));
                    req.setHeader('client_secret', ConfigDataMapController.getCustomSettingValue('CourseConnectionCredlyClientSecretId'));
                    req.setHeader('Content-Type', 'application/json');
                    req.setMethod('POST');
                    req.setbody(requestJSONString);
                    Http http = new Http();
                    HTTPResponse response;
                    try
                    {

                        requestLogObjectToInsertList.add(IntegrationLogWrapper.createIntegrationLog('Credly_IPaaS',requestJSONString, 'Outbound Service','',false));
                        
                        response = http.send(req); 
                        
                        System.debug('Response Code ---------->'+response.getStatusCode()+'\nResponse Code ---------->'+response.getStatus());
                        
                        responseLogObjectToInsertList.add(IntegrationLogWrapper.createIntegrationLog('Credly_IPaaS',response.getBody(), 'Acknowledgement','',false));
                        
                        
                        //Response from IPaas
                        System.debug('response body ---->' + response.getBody());
                        
                        listcredlyjreswrp.add((CredlyJsonResponseWrapper) JSON.deserializeStrict(response.getBody(), CredlyJsonResponseWrapper.class)); 
                        
                        if(response.getStatusCode() == 200){
                            listlifecycleinitiated.addAll(updateStausToInitiated(contactCourseEnrollMap.get(email),courseConnLifeCycleMap));
                        }
                        
                    }
                    catch(Exception e) 
                    {
                        ApplicationErrorLogger.logError(e);
                    } 
                    
                    system.debug('listcredlyjreswrp:::'+listcredlyjreswrp);
                }	
                
                updateListlifecycleinitiatedRecs(listlifecycleinitiated);
                processAndUpsert(courseEnrollMap, listcredlyjreswrp, courseConnLifeCycleMap);    	

                if(Schema.sObjectType.Integration_Logs__c.isCreateable()){
                    insert requestLogObjectToInsertList;        
                    insert responseLogObjectToInsertList;
                }

    }
    
    
    public static void updateListlifecycleinitiatedRecs(List<Course_Connection_Life_Cycle__c> listlifecycleinitiated){
   	            if(!listlifecycleinitiated.isEmpty() && Schema.sObjectType.Course_Connection_Life_Cycle__c.isUpdateable()){
                    try{
                        	update listlifecycleinitiated;
                    }catch(DMLException ex){
                        system.debug('Exception:::::'+ex);
                    }
                }
    }
    
    public static void processAndUpsert(Map<String,hed__Course_Enrollment__c> courseEnrollMap, List<CredlyJsonResponseWrapper> listcredlyjreswrp, Map<String, Course_Connection_Life_Cycle__c> courseConnLifeCycleMap){
               
                List<CredlyJsonResponseWrapper.BadgeResponse> crsBadgeResponseList = new  List<CredlyJsonResponseWrapper.BadgeResponse>();
                
                for(CredlyJsonResponseWrapper credlyjsonreswrp : listcredlyjreswrp)
                {
                    crsBadgeResponseList.addAll(credlyjsonreswrp.badgeResponse);
                }
                
                for (CredlyJsonResponseWrapper.BadgeResponse jsoncrswrp : crsBadgeResponseList) 
                {
                    if (jsoncrswrp.issueDate!=null){
                        if(courseEnrollMap.get(jsoncrswrp.courseConnectionId)!=null){
                            courseEnrollMap.get(jsoncrswrp.courseConnectionId).Grade_Date__c = jsoncrswrp.issueDate;  
                            courseEnrollMap.get(jsoncrswrp.courseConnectionId).hed__Status__c = ConfigDataMapController.getCustomSettingValue('CredlyCourseConnectionStatusAC');
                        }
                        
                        if(courseConnLifeCycleMap.get(jsoncrswrp.courseConnectionId)!=null){
                            courseConnLifeCycleMap.get(jsoncrswrp.courseConnectionId).Status__c = ConfigDataMapController.getCustomSettingValue('CourseConnectionLifeCycleSuccess');
                        }
						if(jsoncrswrp.message!=null && courseConnLifeCycleMap.get(jsoncrswrp.courseConnectionId)!=null){
                            courseConnLifeCycleMap.get(jsoncrswrp.courseConnectionId).Status__c = ConfigDataMapController.getCustomSettingValue('CourseConnectionLifeCycleError');
                        }
                    }
                }

                          
                try{
                    update courseEnrollMap.values();
                    update courseConnLifeCycleMap.values();
                }catch(DMLException ex){
                    system.debug('exception:::'+ex);
                }
                
    }
    
    public static List<CredlyJsonRequestWrapper.BadgeDetails> getBadgeDetailList(List<hed__Course_Enrollment__c> courseEnrollList){
        List<CredlyJsonRequestWrapper.BadgeDetails> badgeDetailList = new List<CredlyJsonRequestWrapper.BadgeDetails>();
        
        if(!courseEnrollList.isEmpty()){
            for(hed__Course_Enrollment__c cenroll : courseEnrollList){
                CredlyJsonRequestWrapper.BadgeDetails badgeObj = new CredlyJsonRequestWrapper.BadgeDetails();
                badgeObj.courseConnectionId = cenroll.Id;
                badgeObj.badgeId = cenroll.hed__Course_Offering__r.hed__Course__r.Credly_Badge_Id__c;
                badgeDetailList.add(badgeObj);
            }
        }	
        return badgeDetailList;
    }
    
    public static List<Course_Connection_Life_Cycle__c> updateStausToInitiated(List<hed__Course_Enrollment__c> courseEnrollList,Map<String, Course_Connection_Life_Cycle__c> courseConnLifeCycleMap){
        
        List<Course_Connection_Life_Cycle__c> cLifeCycleList = new List<Course_Connection_Life_Cycle__c>();
        
        for(hed__Course_Enrollment__c cenroll : courseEnrollList){
            cLifeCycleList.add(courseConnLifeCycleMap.get(cenroll.Id));
        }
        system.debug('cLifeCycleList:::::'+cLifeCycleList);
        if(!cLifeCycleList.isEmpty()){
            for(Course_Connection_Life_Cycle__c clifecycle : cLifeCycleList){
                system.debug('clifecycle:::'+clifecycle);
                clifecycle.Status__c = ConfigDataMapController.getCustomSettingValue('CourseConnectionLifeCycleInitiated'); 
            }
        } 
        
        if(!cLifeCycleList.isEmpty()){
            return cLifeCycleList;
        }else{
            return null;
        }
    }	
 
}