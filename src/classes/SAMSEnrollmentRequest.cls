/*****************************************************************
Name: SAMSEnrollmentRequest
Author: Capgemini [Shreya]
Purpose: Handle SAMS Integration  for all the course connection records which are created via embedded batch credential job.
Jira Reference : ECB-3900,ECB-5545ECB-5549
*****************************************************************/
/*==================================================================
History
--------
Version   Author            Date              Detail
1.0       Shreya           02/04/2019         Initial Version
********************************************************************/

public without sharing class SAMSEnrollmentRequest implements ISAMSEnrollmentRequest  {
    
    private JsonCourseWrapper calloutResponse;
    private String jSONString;
    private HTTPResponse response;
       
    public void sendEnrollDetailToSAMS( List<CourseConnectionWrapper> samsEnrollmentRequestBody){
        String endpoint = ConfigDataMapController.getCustomSettingValue('CourseConnectionIPaasEndPoint');
        HttpRequest req = new HttpRequest();
        req.setEndpoint(endpoint);
        req.setHeader('client_id', ConfigDataMapController.getCustomSettingValue('CourseConnectionIPaasClientId'));
        req.setHeader('client_secret', ConfigDataMapController.getCustomSettingValue('CourseConnectionIPaasClientSecretId'));
        req.setHeader('Content-Type', 'application/json');
        req.setMethod('POST');
        jSONString = JSON.serialize(samsEnrollmentRequestBody);
        req.setbody(jSONString);
        req.setTimeout(120000);
        Http http = new Http();
        response = http.send(req);

        insertIntegrationLogs(jSONString,response.getBody());
        
        if(response.getStatusCode()!= 200){
            throw new SAMSRequestCalloutException('SAMS Response not successful. Status Code: ' + response.getStatus() + ' Response: ' + response.getBody());
        }
           
        System.debug('Response Code ---------->'+response.getStatusCode()+'\nResponse Code ---------->'+response.getStatus());
        try{
            this.calloutResponse = (JsonCourseWrapper) JSON.deserializeStrict(response.getBody(), JsonCourseWrapper.class);
        }catch(Exception ex){
            throw new SAMSRequestCalloutException('SAMS Response not successful. Status Code: ' + response.getStatus() + ' Response: ' + response.getBody());
        }
        
    }
         
    public Map<Id, JsonCourseWrapper.IpaasResponseWrapper> getSAMSEnrollResByCourseConnId() {
        if (calloutResponse == null) {
            throw new ArgumentException('HTTP callout request for SAMS enrollment needs to be executed first.');
        }
        Map<Id, JsonCourseWrapper.IpaasResponseWrapper> samsEnrollResByCourseConnId = new Map<Id, JsonCourseWrapper.IpaasResponseWrapper>();
        for(JsonCourseWrapper.IpaasResponseWrapper calloutResponse : calloutResponse.payload) {
           samsEnrollResByCourseConnId.put(calloutResponse.courseConnectionId, calloutResponse);
        }
        return samsEnrollResByCourseConnId;
    }

    private void insertIntegrationLogs(String reqBody, String responseBody){
        IntegrationLog requestLog = new IntegrationLog();
        requestLog.setRequestBody(reqBody);
        requestLog.setType('SAMS_IPaaS');
        requestLog.setResponseBody(responseBody);
        requestLog.log();
    }
    
}