/**
* -------------------------------------------------------------------------------------------------+
* This class will schedule the batch after each hour
* --------------------------------------------------------------------------------------------------
* @author         Sajitha Deivasikamani
* @version        0.1
* @created        2019-03-12
* @modified       2019-03-14
* @modified by    Anupam Singhal
* @Reference      RM-2539 
* -------------------------------------------------------------------------------------------------+
*/

public with sharing class Batch_CloseOfEnrolment_Scheduler implements Schedulable {
    public void execute(SchedulableContext ctx){
    	RMITO_Lead_Auto_Convert__c csRmito = RMITO_Lead_Auto_Convert__c.getValues('COEBatchConfiguration');
        Batch_CloseOfEnrolment batchCloseOfEnrolment = new Batch_CloseOfEnrolment();
        Database.executeBatch(batchCloseOfEnrolment, 200);
        Batch_CloseOfEnrolment_Scheduler scheduleBatch = new Batch_CloseOfEnrolment_Scheduler();
        DateTime sysTime = System.now();
        Integer addMoreDays = Integer.valueOf(csRmito.BatchRunFrequency__c);
        sysTime = sysTime.addDays(addMoreDays);
        String scheduleInterval = '' + sysTime.second() + ' ' + sysTime.minute() + ' ' + sysTime.hour() + ' ' + sysTime.day() + ' ' + sysTime.month() + ' ? ' + sysTime.year();
        String scheduledJobId = System.schedule('Close Of Enrolment'+scheduleInterval, scheduleInterval, scheduleBatch);
    }
}