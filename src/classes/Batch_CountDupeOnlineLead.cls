/* -----------------------------------------------------------------------------------------------+
* @description
* This batchable class is used to update the count of potential duplicates on Prospective RMIT
* Online leads
* ------------------------------------------------------------------------------------------------
* @author    Anupam Singhal 10/05/2019
* @Reference RM-2872
* -----------------------------------------------------------------------------------------------+
*/  
public class Batch_CountDupeOnlineLead implements Database.Batchable<sObject>,Schedulable {
    private static Id leadRMITORecTypeId=Schema.SObjectType.Lead.getRecordTypeInfosByName().get('Prospective Student - RMIT Online').getRecordTypeId();
    public Database.QueryLocator start(Database.BatchableContext driContext){
        String rtId = '\''+leadRMITORecTypeId+'\'';
        String queryString = 'SELECT Id,Number_Of_Potential_Duplicate__c,RecordType.DeveloperName FROM Lead WHERE isConverted = false AND (MobilePhone !=null OR Phone != null OR Email != null) AND RecordTypeId = '+rtId;
        return Database.getQueryLocator(queryString);       
    }    
    public void execute(Database.BatchableContext driContext,List <Lead> scope ){
        countDuplicates(scope);   
    }
    
    public void finish(Database.BatchableContext driContext){
        AsyncApexJob a = [SELECT Id, Status,ExtendedStatus,NumberOfErrors, JobItemsProcessed,TotalJobItems FROM AsyncApexJob WHERE Id =:driContext.getJobId()];
        SendEmailOnBatchError.sendEmailOnBatch(a,'CountDuplicateOnlineLead');    
    }
    
    public void execute(SchedulableContext schcontext){
        Database.executeBatch(new Batch_CountDupeOnlineLead(),200);
    }
    
    private void countDuplicates(List<Lead> scope){
        set<ID> result= new set<ID>();
        List<Lead> idlist;          
        List<Lead> updateDupCount = new List<Lead>();
        for(Lead leadRec: scope){
            idlist = new List<Lead>{leadRec};
                Datacloud.FindDuplicatesResult[] results = Datacloud.FindDuplicates.findDuplicates(idlist);
            for (Datacloud.FindDuplicatesResult findDupeResult : results) {
                for (Datacloud.DuplicateResult dupeResult : findDupeResult.getDuplicateResults()) {
                    for (Datacloud.MatchResult matchResult : dupeResult.getMatchResults()) {  
                        leadRec.Number_Of_Potential_Duplicate__c = matchResult.getSize();
                        updateDupCount.add(leadRec);
                    }
                }
            }
        }
        try{
            if(!updateDupCount.isEmpty()){
                Database.DMLOptions dml = new Database.DMLOptions();
                dml.DuplicateRuleHeader.allowSave = true;
                Database.update(updateDupCount,dml);
            }
        }catch(DmlException e){
            SendEmailOnBatchError.sendEmailOnException(e,'CountDuplicateOnlineLead');
        }
        
    }    
}