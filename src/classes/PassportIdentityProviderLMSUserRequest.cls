/**
 * @description This class allows Salesforce to make requests to IDAM (Passport) to
 * create users in the respective Learning Management Systems
 * @group Identity Management
 */
public without sharing class PassportIdentityProviderLMSUserRequest {
    
    @testVisible
    private UserValidator validator;
    
    /**
     * @description Creates a new instance of the PassportIdentityProviderLMSUserRequest class 
     * with the PassportIdentityUserValidator dependency. 
     * Using Bastard dependency injection as there is no dependency injection framework.
     */
    public PassportIdentityProviderLMSUserRequest() {
        this.validator = new PassportIdentityUserValidator();
    }


    /**
     * @description Creates a new instance of the PassportIdentityProviderLMSUserRequest class
     * @param validator The validator to be used to check user fields
     */
    public PassportIdentityProviderLMSUserRequest(UserValidator validator) {
        this.validator = validator;
    }
    
    
    /**
     * @description Creates the user in the Learning Management System (LMS). 
     * This method requires Email and Learning Systems to be set. IDAM will not provide
     * a success or fail response. Salesforce will only receive an acknowledge that the
     * request has been received.
     * @param email The user's email. This email will be used to check if the user already exists in the LMS
     * @param learningSystems A set of Learning Systems enum where the user should be created in 
     */
    public void createLearningMgmtSystemUser(String email, Set<LearningManagementSystem> learningSystems) {
        
        if(!validator.isEmailValid(email) || learningSystems == null || learningSystems.size() < 1) {
            throw new ArgumentException('Invalid email or learning systems parameters.');
        }
        
        List<LearningSystem> systems = new List<LearningSystem>();
        for (LearningManagementSystem systemRequested : learningSystems) {
            LearningSystem sys = new LearningSystem();
            sys.add = learningManagementSystemToString(systemRequested);
            systems.add(sys);
        }
        
        ProvisionRequest req = new ProvisionRequest();
        req.email = email;
        req.systems = systems;
        
        String requestBody = JSON.serialize(req);

        Http http = new Http();
        HTTPRequest request = new HTTPRequest();
        HTTPResponse response = new HTTPResponse();
        String  endpoint = ConfigDataMapController.getCustomSettingValue('IDAMProvisonEndPoint'); 
        request.setHeader('client_id', ConfigDataMapController.getCustomSettingValue('IDAMIpaaSClientID')); 
        request.setHeader('client_secret', ConfigDataMapController.getCustomSettingValue('IDAMIpaaSClientSecret')); 
        request.setHeader('Content-Type', 'application/json');
        request.setEndpoint(endpoint);
        request.setMethod('PUT');
        request.setBody(requestBody);
        response = http.send(request); 

        if (response.getStatusCode() != 200) {
            throw new HttpCalloutException('Request was not successful. Http Status Code: ' + response.getStatusCode());
        }

        IntegrationLog requestLog = new IntegrationLog();
        requestLog.setRequestBody(requestBody);
        requestLog.setType('IDAM_IPaaS_Provision');
        requestLog.setServiceType('Outbound Service');
        requestLog.log();

        IntegrationLog responseLog = new IntegrationLog();
        responseLog.setRequestBody(response.getBody());
        responseLog.setType('IDAM_IPaaS_Provision');
        responseLog.setServiceType('Acknowledgement');
        responseLog.log();
    }

    private String learningManagementSystemToString(LearningManagementSystem lms) {
        if (lms == LearningManagementSystem.CANVAS) {
            return 'canvas';
        } else if (lms == LearningManagementSystem.OPENEDX) {
            return 'openedx';
        } else {
            throw new ArgumentException('Invalid parameter value. String conversion not defined for the parameter');
        }
    }

    private class ProvisionRequest {
        public String email;
        public List<LearningSystem> systems;
    }
    

    private class LearningSystem {
        public String add;
    }
}