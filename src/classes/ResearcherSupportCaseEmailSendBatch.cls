/*******************************************
Name: ResearcherSupportCaseEmailSendBatch 

Description : Batch class to used to send the email notification for the researcher support case. The user who doesnt have permission to access the case
fields wont be sent email from case create process. 
Using the Case field "IsResearcherSupportEmailToBeSent__c" to send the emails.

10/06/2019- 

*******************************************/ 


public class ResearcherSupportCaseEmailSendBatch implements Database.batchable<sObject>{


    public Database.QueryLocator start(Database.BatchableContext batchContext) {
        
        
        String query = 'SELECT ID,IsResearcherSupportEmailToBeSent__c FROM Case WHERE IsResearcherSupportEmailToBeSent__c =true LIMIT 10' ;
        
        system.debug('###'+ query);
        
        return Database.getQueryLocator(query);
    }

    public void execute(Database.BatchableContext batchContext, List<case> scopeCaseList) {
          
        system.debug('### caseId'+scopeCaseList);
        for(case theCase: scopeCaseList){
            ResearcherSupportCaseEmailTemplateHelper.sendResearchSupportEmailNotification(theCase.id);
            theCase.IsResearcherSupportEmailToBeSent__c=false;
        }
        update scopeCaseList;
    }   

    public void finish(Database.BatchableContext batchContext) {
        
        system.debug('### finished');
        
    }
}