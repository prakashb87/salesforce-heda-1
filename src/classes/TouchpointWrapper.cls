/*Wrapper Class for touchpoints to be refrenced in Application Trigger Handler*/
public class TouchpointWrapper{
        public string strTouchpoint{get;set;}
        public string strSubject{get;set;}
	    public DateTime dtTouchpoint{get;set;}
    
        public TouchpointWrapper(){
            strTouchpoint = '';
            strSubject = '';
            dtTouchpoint = null;
        }
    }