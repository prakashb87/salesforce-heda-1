/*****************************************************************
Name: EmbeddedSAMSEnrollmentServiceTest 
Author: Capgemini [Shreya]
Purpose: Test Class of EmbeddedSAMSEnrollmentService
*****************************************************************/
@isTest
public class EmbeddedSAMSEnrollmentServiceTest {
         
    @isTest
    static void nullReqTest() {
        EmbedSamsDataFactoryUtil.testEmbeds();
        MockSuccessSingleSamsEnrollReq samsReq = new MockSuccessSingleSamsEnrollReq();
        EmbeddedSAMSEnrollmentService embeddedSamsEnrolSer = new EmbeddedSAMSEnrollmentService(samsReq);
        
        try {
            embeddedSamsEnrolSer.sendCourseConn(null);
            System.assert(false);
        } catch (QueryException e) {
            System.assert(true);    // no exception expected!
        } catch (Exception e) {
            System.assert(false);        // nothing else should fail
        }
    }
    
    @isTest
    static void successEnrollReqTest() {
        EmbedSamsDataFactoryUtil.testEmbeds();
        MockSuccessSingleSamsEnrollReq samsReq = new MockSuccessSingleSamsEnrollReq();
        EmbeddedSAMSEnrollmentService embeddedSamsEnrolSer = new EmbeddedSAMSEnrollmentService(samsReq);
               
        Test.startTest();
        try {
            List<hed__Course_Enrollment__c> enrollmentList = [SELECT Id,SAMS_Integration_Processing_Complete__c FROM hed__Course_Enrollment__c];           
            Set<Id> courseConnections = new Set<Id> {};
            for(hed__Course_Enrollment__c ce : enrollmentList){
                courseConnections.add(ce.Id);
            }
            embeddedSamsEnrolSer.sendCourseConn(courseConnections);
            System.assertEquals(embeddedSamsEnrolSer.failedCount,0);
            System.assertEquals(embeddedSamsEnrolSer.updatedCourseConnList[0].SAMS_Integration_Processing_Complete__c,true);
            System.assertEquals(embeddedSamsEnrolSer.ccLifeCycleList[0].Status__c,'Success');
            
            
        } catch (Exception e) {
            system.debug('Exception:::'+e);
            System.assert(false);        // nothing else should fail
        }
        Test.StopTest();
    }
    
    @isTest
    static void failedSingleEnrollReqTest() {
        EmbedSamsDataFactoryUtil.testEmbeds();
        MockFailedSingleSAMSEnrollReq samsReq = new MockFailedSingleSAMSEnrollReq();
        EmbeddedSAMSEnrollmentService embeddedSamsEnrolSer = new EmbeddedSAMSEnrollmentService(samsReq);
              
        Test.startTest();
        try {
            List<hed__Course_Enrollment__c> enrollmentList = [SELECT Id,Canvas_Integration_Processing_Complete__c FROM hed__Course_Enrollment__c];           
            Set<Id> courseConnections = new Set<Id> {};
            for(hed__Course_Enrollment__c ce : enrollmentList){
                courseConnections.add(ce.Id);
            }
            embeddedSamsEnrolSer.sendCourseConn(courseConnections);
            
        } catch (CanvasRequestCalloutException e) {
            // E.g. integration is not working, so expecting an callout exception
            System.assert(true);
            System.assertEquals(embeddedSamsEnrolSer.failedCount,1);
            System.assertEquals(embeddedSamsEnrolSer.ccLifeCycleList[0].Status__c,'Error');
        } catch (Exception e) {
            System.assert(false);
        }
        Test.StopTest();
    }
    
    @isTest
    static void samsErrorResponseTest() {
        EmbedSamsDataFactoryUtil.testEmbeds();
        MockFailedSingleSAMSEnrollReq samsReq = new MockFailedSingleSAMSEnrollReq();
        EmbeddedSAMSEnrollmentService embeddedSamsEnrolSer = new EmbeddedSAMSEnrollmentService(samsReq);
              
        Test.startTest();
        try {
            List<Course_Connection_Life_Cycle__c> lifeCycleList = [SELECT Id,Course_Connection__c,Course_Connection__r.id,Stage__c,Status__c FROM Course_Connection_Life_Cycle__c];
            embeddedSamsEnrolSer.ccLifeCycleList = new List<Course_Connection_Life_Cycle__c>();
            embeddedSamsEnrolSer.ccLifeCycleList.addAll(lifeCycleList);
            embeddedSamsEnrolSer.updateLifeCycleStatusToInitiated();
            System.assertEquals(embeddedSamsEnrolSer.failedCount,1);
            
        } catch (CanvasRequestCalloutException e) {
            // E.g. integration is not working, so expecting an callout exception
            System.assert(false);
        }catch(Exception ex){
            system.debug('Excpeiton::'+ex);
        } 
        Test.StopTest();
    }
    
    @isTest
    static void nullCourseEnrollIdsTest1() {
        EmbedSamsDataFactoryUtil.testEmbeds();
        MockSuccessSingleSamsEnrollReq samsReq = new MockSuccessSingleSamsEnrollReq();
        EmbeddedSAMSEnrollmentService embeddedSamsEnrolSer = new EmbeddedSAMSEnrollmentService(samsReq);  
        
        try {
            embeddedSamsEnrolSer.queryCCLifeCycleRec(null);
            embeddedSamsEnrolSer.getCourseEnrollmentDetails(null);
            System.assert(false);
        } catch (QueryException e) {
            System.assert(true);    // no exception expected!
        } catch (Exception e) {
            System.assert(false);        // nothing else should fail
        }
    }
    
    @isTest
    static void nullCourseEnrollIdsTest2() {
        EmbedSamsDataFactoryUtil.testEmbeds();
        MockSuccessSingleSamsEnrollReq samsReq = new MockSuccessSingleSamsEnrollReq();
        EmbeddedSAMSEnrollmentService embeddedSamsEnrolSer = new EmbeddedSAMSEnrollmentService(samsReq);  
        
        try {
            
            embeddedSamsEnrolSer.getCourseEnrollmentDetails(null);
            System.assert(false);
        } catch (QueryException e) {
            System.assert(true);    // no exception expected!
        } catch (Exception e) {
            System.assert(false);        // nothing else should fail
        }
    }
    
    @isTest
    static void failCountTest() {
        EmbedSamsDataFactoryUtil.testEmbeds();
        MockSuccessSingleSamsEnrollReq samsReq = new MockSuccessSingleSamsEnrollReq();
        EmbeddedSAMSEnrollmentService embeddedSamsEnrolSer = new EmbeddedSAMSEnrollmentService(samsReq);  
        
        try {
            embeddedSamsEnrolSer.failedCount = 5;
            embeddedSamsEnrolSer.getFailedCount();
            System.assertEquals(embeddedSamsEnrolSer.failedCount,5);
        } catch (QueryException e) {
            System.assert(true);    // no exception expected!
        } catch (Exception e) {
            System.assert(false);        // nothing else should fail
        }
    }
}