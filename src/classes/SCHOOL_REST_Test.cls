@isTEST
public class SCHOOL_REST_Test {
    static testMethod void myUnitTest() {
        
        RestRequest req = new RestRequest();
        RestResponse res = new RestResponse();
        // adding header to the request
        req.addHeader('httpMethod', 'POST');
        req.requestUri = '/services/apexrest/HEDA/v1/School';
        
        //creating test data
        String jsonMsg = ' { "school": "155T", "effectiveDate": "2005-01-01", "status": "Active","description": "Life & Physical Sciences","shortDescription": "Lif & Phy","formalDescription": "Life","academicInstitution": "RMITU","aouCode": "S10","campus": "RMITU", "parentAcademicOrganisation" : "RMITU"} ';
       	req.requestBody = Blob.valueof(jsonMsg);
        RestContext.request = req;
        RestContext.response = res;
        
      	
        Account schoolRecord = new Account();
        schoolRecord.Name='';
        schoolRecord.Effective_Date__c= System.today();
        schoolRecord.Status__c='';
        schoolRecord.Description='';
        schoolRecord.Short_Description__c='';
        schoolRecord.Formal_Description__c='';
        
        
        List<Account> accs=[Select id from Account where AccountNumber__c='155T'];
        
        System.assertEquals(accs.size(), 0);
        Test.startTest();
       		SCHOOL_REST.upsertSchool();
        
        Test.stopTest();
        
        accs=[Select id from Account where AccountNumber__c='155T'];
        System.assertEquals(1, accs.size());
        
    }
    
    static testMethod void coverTryCatch() {
        Map < String, String > check;
        Test.startTest();
        try {
            //store response
            check = SCHOOL_REST.upsertSchool();
        } catch (DMLException e) {
            List < String > str = check.values();
            //assert condition 
            System.assertEquals(False, str[0] == 'ok');
        }
        Test.stopTest();
    }

}