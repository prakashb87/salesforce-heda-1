/*********************************************************************************
*** @TestClassName     : PLAN_REST_Test
*** @Author            : Dinesh Kumar
*** @Requirement       : To Test the PLAN_REST web service apex class.
*** @Created Date      : 03/09/2018
*** @Modified Date 	   : 18/09/2018
**********************************************************************************/

@isTest
public class PLAN_REST_Test {
    
	static testMethod void myUnitTest() {
        
        RestRequest req = new RestRequest();
        
        RestResponse res = new RestResponse();
        // adding header to the request
        req.addHeader('httpMethod', 'POST');
        
        req.requestUri = '/services/apexrest/HEDA/v1/Plan';
        
        //creating test data
        String JsonMsg = '{"institution": "TOM","academicPlan": "t","academicProgram": "EL000","effectiveDate": "2017-01-01","status": "A","academicCareer": "PREP","academicPlanType": "Default","description": "English Tourism & Hospitality","shortDescription": "ENGTOURISM","firstTermValid": "0005","degree": "ELICOS","transcriptLevel": "20","ownership.academicOrganisation": "","ownership.percentageOwned": 100,"fieldOfStudyCode": "","fieldOfEducationCode": "","specialisationCode": "","cricosCode": "","aqfCode": "","fieldOfStudyGroup": ""}';
        
        req.requestBody = Blob.valueof(JsonMsg);
        
        RestContext.request = req;
        
        RestContext.response = res;
        
        Account acc = new Account();
        acc.Name = 'RMITU';
        insert acc;
        
        //Fine Test
        
        Account accR = new Account();
        accR.Name = 'Fine Test';
        insert accR;
        
        Field_of_Study__c fieldStudy = new Field_of_Study__c();
        fieldStudy.Name = '1223';
        fieldStudy.Field_of_Study_Code__c = 'Test';
        insert fieldStudy;
        
        Field_of_education__c fieldofEdu = new Field_of_education__c();
        fieldofEdu.Name = '081100';
        fieldofEdu.Effective_date__c = Date.newInstance(2016, 12, 9);
        insert fieldofEdu;
            
        
        Plan__c planRecord = new Plan__c();
        planRecord.Institution__c = acc.Id;
        planRecord.Academic_Plan__c = 'ETH';
        planRecord.Academic_Program__c = acc.Id;
        planRecord.Academic_Organization__c = acc.Id;
        planRecord.Field_of_Study_Code__c = fieldStudy.Id;
        planRecord.Field_of_Education_Code__c = fieldofEdu.Id;
        insert planRecord;
        
        Test.startTest();
        //store response
        Map < String, String > check = PLAN_REST.upsertPlan();
        List < String > str = check.values();
        //assert condition 
        System.assertEquals(True, str[0] == 'ok');
        
        Test.stopTest();
        
    }
    
    
    static testMethod void myUnitTestwithDates() {
        RestRequest req = new RestRequest();
        RestResponse res = new RestResponse();
        // adding header to the request
        req.addHeader('httpMethod', 'POST');
        
        req.requestUri = '/services/apexrest/HEDA/v1/Plan';
        
        //creating test data
        String JsonMsg = ' {"institution": "RMITU", "academicPlan": "ETH", "academicProgram": "Fine Test","effectiveDate": "2000-01-01","status": "A","academicCareer": "PREP","academicPlanType": "Default","description": "English Tourism & Hospitality","shortDescription": "ENGTOURISM1","firstTermValid": "0005","degree": "ELICOS","transcriptLevel": "20","ownership.academicOrganisation": "Fine Test","ownership.percentageOwned": 100,"fieldOfStudyCode": "1223","fieldOfEducationCode": "081100","specialisationCode": "","cricosCode": "","aqfCode": "","fieldOfStudyGroup": ""} ';
        
        req.requestBody = Blob.valueof(JsonMsg);
        
        RestContext.request = req;
        
        RestContext.response = res;
        
        Account acc = new Account();
        acc.Name = 'RMITU';
        insert acc;
        
        //Fine Test
        
        Account accR = new Account();
        accR.Name = 'Fine Test';
        insert accR;
        
        Field_of_Study__c fieldStudy = new Field_of_Study__c();
        fieldStudy.Name = '1223';
        fieldStudy.Field_of_Study_Code__c = 'Test';
        insert fieldStudy;
        
        Field_of_education__c fieldofEdu = new Field_of_education__c();
        fieldofEdu.Name = '081100';
        fieldofEdu.Effective_date__c = Date.newInstance(2016, 12, 9);
        insert fieldofEdu;
            
        
        Plan__c planRecord = new Plan__c();
        planRecord.Institution__c = acc.Id;
        planRecord.Academic_Plan__c = 'ETH';
        planRecord.Effective_Date__c = System.today();
        planRecord.Academic_Program__c = acc.Id;
        planRecord.Academic_Organization__c = acc.Id;
        planRecord.Field_of_Study_Code__c = fieldStudy.Id;
        planRecord.Field_of_Education_Code__c = fieldofEdu.Id;
        insert planRecord;
        
        
        Plan_Future_Change__c futurePlan = new Plan_Future_Change__c();
        
        Test.startTest();
        //store response
        Map < String, String > check = PLAN_REST.upsertPlan();
        List < String > str = check.values();
        //assert condition 
        System.assertEquals(True, str[0] == 'ok');
        
        Test.stopTest();
        
    }
    
    static testMethod void unitTestPlanFutureChange() {
        RestRequest req = new RestRequest();
        RestResponse res = new RestResponse();
        // adding header to the request
        req.addHeader('httpMethod', 'POST');
        
        req.requestUri = '/services/apexrest/HEDA/v1/Plan';
        
        //creating test data
        String JsonMsg = ' {"institution": "RMITU", "academicPlan": "ETH", "academicProgram": "Fine Test","effectiveDate": "2023-01-01","status": "A","academicCareer": "PREP","academicPlanType": "Default","description": "English Tourism & Hospitality","shortDescription": "ENGTOURISM1","firstTermValid": "0005","degree": "ELICOS","transcriptLevel": "20","ownership.academicOrganisation": "Fine Test","ownership.percentageOwned": 100,"fieldOfStudyCode": "","fieldOfEducationCode": "","specialisationCode": "123","cricosCode": "12","aqfCode": "23","fieldOfStudyGroup": "123"} ';
        
        req.requestBody = Blob.valueof(JsonMsg);
        
        RestContext.request = req;
        
        RestContext.response = res;
        
        Account acc = new Account();
        acc.Name = 'RMITU';
        insert acc;
        
        //Fine Test
        
        Account accR = new Account();
        accR.Name = 'Fine Test';
        accR.AccountNumber = 'Fine Test';
        insert accR;
        
        Field_of_Study__c fieldStudy = new Field_of_Study__c();
        fieldStudy.Name = '1223';
        fieldStudy.Field_of_Study_Code__c = 'Test';
        insert fieldStudy;
        
        Field_of_education__c fieldofEdu = new Field_of_education__c();
        fieldofEdu.Name = '081100';
        fieldofEdu.Effective_date__c = Date.newInstance(2016, 12, 9);
        insert fieldofEdu;
            
        
        Plan__c planRecord = new Plan__c();
        planRecord.Institution__c = acc.Id;
        planRecord.Academic_Plan__c = 'ETH';
        planRecord.Effective_Date__c = System.today();
        planRecord.Academic_Program__c = acc.Id;
        planRecord.Academic_Organization__c = acc.Id;
        planRecord.Field_of_Study_Code__c = fieldStudy.Id;
        planRecord.Field_of_Education_Code__c = fieldofEdu.Id;
        insert planRecord;
        
        
        Plan_Future_Change__c futurePlan = new Plan_Future_Change__c();
        
        Test.startTest();
        //store response
        Map < String, String > check = PLAN_REST.upsertPlan();
        List < String > str = check.values();
        //assert condition 
        System.assertEquals(True, str[0] == 'ok');
        
        Test.stopTest();
        
    }
    
    static testMethod void coverTryCatch() {
        Map < String, String > check;
        Test.startTest();
        try {
            //store response
            check = PLAN_REST.upsertPlan();
        } catch (DMLException e) {
            List < String > str = check.values();
            //assert condition 
            System.assertEquals(False, str[0] == 'ok');
        }
        Test.stopTest();
    }
    
}