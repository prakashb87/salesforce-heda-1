@isTest
private class ApplicationTriggerHandlerTest{
    public static List< Lead > insertLeadRecords(){
        List< lead > insertLead = new List< lead >();
        Lead leadRec;
        for(Integer i =0; i < 20; i++){
            leadRec = new Lead(LastName='Test Lead' + i,Company='Test company' + i,status = 'Requested',Email = 'test@test.com');
            insertLead.add(leadRec);
        }
        insert insertLead;
        Campaign cam = new Campaign();
        cam.Name = 'Test';
        insert cam;
        List<CampaignMember> listCampaignMember = new List<CampaignMember>();
        for(Integer i = 0 ; i < 10 ; i++){
            CampaignMember cm = New CampaignMember(CampaignId=cam.Id,Status='Sent',LeadId = insertLead[i].ID);
            listCampaignMember.add(cm);
        }
        insert listCampaignMember;
        List< Task > listTask = new List< Task >();
        for(Integer i = 10; i < 20 ;i++){
            Task task = new Task();
            task.WhoId = insertLead[i].ID;
            task.Status = 'Completed';
            task.Priority = 'Normal';
            if(i < 10){
            	task.TaskSubtype = 'Call';
                task.Subject = 'call subject';
            }else if( i == 10 && i < 20){
                task.TaskSubtype = 'Email';
                task.Subject = 'Email subject';
            }
            listTask.add(task);
        }
        insert listTask; 
        return insertLead;
    }

    @isTest 
    public static void insertAppRecords(){
        List < String > listOfPriorityStatus = new List < String > ();
        //picklist values for priority
        listOfPriorityStatus.add('Enrolment');
        listOfPriorityStatus.add('Acceptance');
        listOfPriorityStatus.add('Offer');
        listOfPriorityStatus.add('Conditional offer');
        listOfPriorityStatus.add('Application');
        listOfPriorityStatus.add('Registration');
        listOfPriorityStatus.add('Deny');
        listOfPriorityStatus.add('Deferment');
        listOfPriorityStatus.add('Leave');
        listOfPriorityStatus.add('Withdrawal');
        List< lead > recordInserted = new List< lead >();
        //get lead insertedd records
        recordInserted = insertLeadRecords();
        List< Application__c > listApplication = new List< Application__c >();
        Application__c appRecord;
        //associate application with lead
        for(Integer i = 0; i < 20 ; i++ ){
            for(Integer j = 0; j < 10 ; j++){
                appRecord = new Application__c();
                appRecord.Name = 'Test_Application_Lead_DND' + j;
                appRecord.Program_Application_Status__c = listOfPriorityStatus[j];
                appRecord.Applicant_Name__c = recordInserted[i].ID;
                listApplication.add(appRecord);
            }
        }
        insert listApplication;
        list<Lead> queryLead = [select Current_Status__c from Lead where Current_Status__c = 'Enrolment'];
        system.assertEquals(20,queryLead.size());
        //delete application record
        
        List<Id> deleteAppListId = new List<Id>();
        List<Application__c> deleteAppList = new List<Application__c>();
        List< Application__c > deleteApp = new List< Application__c >();
        for(Integer i = 0; i < listApplication.size();i++){
            if(i/2 == 0){
            	deleteApp.add(listApplication[i]);
                deleteAppListId.add(listApplication[i].Id);
            }
        }
        Delete deleteApp;
        
        deleteAppList = [Select Id, Name From Application__c where Id IN: deleteAppListId];
        ApplicationTouchPointsUpdate appTouchObject = new ApplicationTouchPointsUpdate();
        appTouchObject.beforeUpdateApplication(deleteAppList, deleteApp);
        
    }
    
    @isTest 
    public static void updateApplicationRecordsElse(){
          
        ApplicationTouchPointsUpdate appTouchObject = new ApplicationTouchPointsUpdate();
        Lead leadRec = new Lead(LastName='Test Lead', Company='Test company', status = 'Requested', Email = 'test@test.com');
        insert leadRec;
        
        System.assertEquals(leadRec.LastName, 'Test Lead');
        
        Task taskRec = new Task();
        taskRec.WhoId = leadRec.Id;
        taskRec.Status = 'Completed';
        taskRec.Priority = 'Normal';
        insert taskRec;
        
        Account acct = new Account (Name = 'Acme, Inc.');
        insert acct;
        system.debug('Inserted Account, ID: ' + acct.id);
    
        Contact con = new Contact(
                          FirstName = 'Robin',
                          LastName = 'Koehler',
                          AccountId = acct.Id
                          );
        insert con;   
        system.debug('Inserted Contact, ID: ' + con.id);
    
        Campaign camp = new Campaign(
                            Name = 'Test',
                            IsActive = TRUE
                            );            
        insert camp;
        system.debug('Inserted Campaign, ID: ' + camp.id);
    
        CampaignMember member = new CampaignMember(
            ContactId = con.Id,
            Status = 'Completed',
            CampaignId = camp.Id
            ); 
        insert member;
        
        Application__c appRec = new Application__c();
        appRec.Name = 'Test';
        insert appRec;
        appTouchObject.updateApplicationRecords(taskRec, member, appRec);
        
    }

}