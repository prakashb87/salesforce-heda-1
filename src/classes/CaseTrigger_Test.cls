@isTest
public class CaseTrigger_Test {
    @isTest static void testAll(){
        contact con = new contact(LastName='Testcon');
        con.First_in_family_at_university__c = true;
        insert con;
        Contact cont = [select LastName,First_in_family_at_university__c from contact where id=:con.id];
        System.assertEquals(cont.LastName,'Testcon');
        System.assertEquals(cont.First_in_family_at_university__c,true);
        case c = new case();
        c.Status = 'In Progress';
        c.ContactId = con.Id;
        insert c;
        Case ce = [select Status from case where id=:c.id];
        System.assertEquals(ce.Status,'In Progress');
        c.Status = 'Closed';
        c.First_in_family_at_university__c = false;
        update c;
        ce = [select Status,First_in_family_at_university__c from case where id=:c.id];
        System.assertEquals(ce.Status,'Closed');
        System.assertEquals(ce.First_in_family_at_university__c,false);
    }
	
	@isTest static void testmethodToUpdateContactNameInCase(){
            
        RSTP_TestDataFactoryUtils.createNonCommunityUsersWithProfile(1,'System Administrator','Activator');
        
        User adminUser =[SELECT Id,ContactId FROM User WHERE UserName='test.nonCommunityUser1@test.com.testDev' LIMIT 1];
        adminUser.FederationIdentifier = 'e123';
        
        System.runAs(adminUser)   
        {       
        
            Id rmitStaffRecordTypeId = Schema.SObjectType.hed__Affiliation__c.getRecordTypeInfosByName().get('RMIT Staff').getRecordTypeId();
            Id studentRecordTypeId = Schema.SObjectType.hed__Affiliation__c.getRecordTypeInfosByName().get('Student').getRecordTypeId();
            
            contact con = new contact(LastName='Testcon', Enumber__c = 'e123');
            con.First_in_family_at_university__c = true;
            insert con;            
            
            contact con2 = new contact(LastName='Testcon2', Enumber__c = 'e1234');
            con2.First_in_family_at_university__c = true;
            insert con2;
            
            hed__Affiliation__c affiliation1 = new hed__Affiliation__c() ;
            affiliation1.RecordTypeId = rmitStaffRecordTypeId ;
            affiliation1.hed__Contact__c = con.id;
            insert affiliation1 ;
            
            hed__Affiliation__c affiliation2 = new hed__Affiliation__c() ;
            affiliation2.RecordTypeId = studentRecordTypeId ;
            affiliation2.hed__Contact__c = con2.id;
            insert affiliation2 ;
            
            contact con3 = new contact(LastName='Testcon3', Student_ID__c = 's123');
            con3.First_in_family_at_university__c = true;
            insert con3;
            
            contact con4 = new contact(LastName='Testcon4', Student_ID__c = 's1234');
            con4.First_in_family_at_university__c = true;
            insert con4;
            
            hed__Affiliation__c affiliation3 = new hed__Affiliation__c() ;
            affiliation3.RecordTypeId = rmitStaffRecordTypeId ;
            affiliation3.hed__Contact__c = con3.id;
            insert affiliation3 ;
            
            hed__Affiliation__c affiliation4 = new hed__Affiliation__c() ;
            affiliation4.RecordTypeId = studentRecordTypeId ;
            affiliation4.hed__Contact__c = con4.id;
            insert affiliation4 ;
                                     
            Contact cont = [select LastName,First_in_family_at_university__c from contact where id=:con.id];
            System.assertEquals(cont.LastName,'Testcon');
            System.assertEquals(cont.First_in_family_at_university__c,true);
                
            Id caseAdminRecordTypeId = Schema.SObjectType.Case.getRecordTypeInfosByName().get('Researcher Portal Feedback').getRecordTypeId();

            case c = new case();
            c.Status = 'In Progress';
            c.RecordTypeId = caseAdminRecordTypeId ;
            insert c;
            

            //System.assertEquals(ce.Status,'Closed');
            //System.assertEquals(ce.First_in_family_at_university__c,false);
        
        }
    }
}