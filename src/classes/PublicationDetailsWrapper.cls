/*******************************************
Purpose: To create wrapper class for Publication Details
History:
Created by Ankit Bhagat on 30/10/2018
*******************************************/

public class PublicationDetailsWrapper {
    
    /*****************************************************************************************
// Purpose      :  Publications wrapper with total Notifications and milestoneWrapperList
// Developer    :  Ankit Bhagat 
// Created Date :  30/10/2018                 
//************************************************************************************/ 
    public class PublicationDetailsWithTotalsWrapper{
        
        @AuraEnabled public integer totalPublications;
        @AuraEnabled public List<publicationWrapper> publicationWrapperList;
    }
    
    /*****************************************************************************************
// Purpose      :  Publications wrapper
// Developer    :  Ankit Bhagat 
// Created Date :  30/10/2018                 
//************************************************************************************/ 
    public class publicationWrapper{
        
        @AuraEnabled public id publicationId;//added by Ali,RPROW-894
        @AuraEnabled public decimal year;
        @AuraEnabled public string category;
        @AuraEnabled public string title;
        @AuraEnabled public string outlet;
        @AuraEnabled public string edition;
        @AuraEnabled public string status;
        @AuraEnabled public String description;//added by Ali,RPROW-894
        @AuraEnabled public String keywords;//added by Ali,RPROW-894
        @AuraEnabled public String forcodes;//added by Ali,RPROW-894
        @AuraEnabled public String publicationEcode;
        @AuraEnabled public String subCategory;
        @AuraEnabled public String medium;
        @AuraEnabled public String iSSN;
        @AuraEnabled public String publisher;
        @AuraEnabled public String placeofPublication;
        @AuraEnabled public String conferenceName;
        @AuraEnabled public String conferenceLocation;
        @AuraEnabled public String eventDate;
        @AuraEnabled public String editors;
        @AuraEnabled public String volume;
        @AuraEnabled public String issueNumber;
        @AuraEnabled public String startPage;
        @AuraEnabled public String endPage;
        @AuraEnabled public decimal totalPages;
        @AuraEnabled public String creativeWorkExtent;
        @AuraEnabled public String sjrQuartile;
        @AuraEnabled public String sjrPercentile;
        @AuraEnabled public String electronicLocation;
        @AuraEnabled public String scopusID;
        @AuraEnabled public String webofScienceID;
        @AuraEnabled public date dateDepositedtoRepository;
        @AuraEnabled public String copyright;
        @AuraEnabled public String authors;
		//@AuraEnabled public String positions;
		@AuraEnabled public List<conDetailsWrapper> rmsMembers;
        
        
        
        public publicationWrapper(){
            
            year         = 0.0;
            category     = '';
            title        = '';
            outlet       = '';
            edition      = '';
            status       = '';
            authors      = '';
        }
        
        public publicationWrapper(Publication__c publication){
            
            publicationId = publication.Id;//added by Ali,RPROW-894
            year       = publication.Year_Published__c;
            category   = publication.Publication_Category__c;
            title      = publication.Publication_Title__c;
            outlet     = publication.Outlet__c;
            edition    = publication.Edition__c;
            status     = publication.Audit_Result__c;//added by Ali,RPROW-894
            description = publication.Description__c;//added by Ali,RPROW-894
            keywords = publication.Keywords__c;//added by Ali,RPROW-894
            publicationEcode = publication.Publication_Ecode__c;
            subCategory = publication.Creative_Work_Type__c;
            medium = publication.Medium__c;
            iSSN = publication.ISSN__c;
            publisher = publication.Publisher__c;
            placeofPublication = publication.Place_Published__c;
            conferenceName = publication.Conference_Name__c;
            conferenceLocation = publication.Conference_Location__c;
            eventDate = publication.Event_Date__c;
            editors = publication.Editors__c;
            volume = publication.Volume__c;
            issueNumber = publication.Issue_Number__c;
            startPage = publication.Start_Page__c;
            endPage = publication.End_Page__c;
            totalPages = publication.Page_Total__c;
            creativeWorkExtent = publication.Creative_Work_Extent__c;
            sjrQuartile = publication.Scimago_Journal_Quartile__c;
            sjrPercentile = publication.SJR_Percentile__c;
            electronicLocation = publication.Digital_Object_Identifier__c;
            scopusID = publication.Scopus_Identifier__c;
            webofScienceID = publication.Web_of_Science_Identifier__c;
            dateDepositedtoRepository=publication.Repository_Deposited_Date__c;
            copyright=publication.Copyright__c;
			
			  rmsMembers=new List<conDetailsWrapper>();
            //if (rpp.Linked_Contacts__r!=null) {
                for (Researcher_Member_Sharing__c eachRms: publication.Researcher_Portal_Member_Sharings__r){
                    conDetailsWrapper conObj = new conDetailsWrapper();
                    conObj.authors=eachRms.User__r.name;
                    conObj.positions=eachRms.Position__c;
                    conObj.personnelType=eachRms.Personnel_Type__c;
                    
                    rmsMembers.add(conObj);
                    system.debug('@@@rmsMembers'+rmsMembers);
                }
            //}
            
        }//publicationWrapper constructor Ends
        
        
	    
        
    }//publicationWrapper Ends
    
 public class conDetailsWrapper{
            
            @AuraEnabled public String authors;
            @AuraEnabled public String positions;
     		@AuraEnabled public String personnelType;
            
        }//conDetailsWrapper Ends
    
}