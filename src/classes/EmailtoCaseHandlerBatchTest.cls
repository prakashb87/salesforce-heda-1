/*********************************************************************************
 *** @Class			    : EmailtoCaseHandlerBatchTest
 *** @Author		    : Shubham Singh
 *** @Requirement     	: Test class for EmailtoCaseHandle
 *** @Created date    	: 10/01/2018
 **********************************************************************************/
/*****************************************************************************************
 *** @About Class
 *** This class is a test class for EmailtoCaseHandlerBatch class
 *****************************************************************************************/
@isTest
private class EmailtoCaseHandlerBatchTest {
    static testmethod void test() {
        //Insert contact
        contact con = new contact();
        con.FirstName = 'Test';
        con.LastName = 'Mail';
        con.Email = 'cooltricks@gmail.com';
        insert con;
        //Insert Spam_Mails__c
        Spam_Mails__c em = new Spam_Mails__c();
        em.Name = 'Test Setting';
        em.Spam_Mails__c = 'formsite.com';
        insert em;
        List<case> lcase = new List<case>();
        //Adding cases without spam email and with attachment 
        
        for(Integer i=0;i < 10;i++){
            case c = new case();
            c.ContactId = con.Id;
            c.SuppliedEmail = 'cooltricks@gmail.com';
            c.Description = 'Hello i am a test mail to test the cases';
            if(i > 5){
                if(i == 6)
                {
                	c.Subject = 'MailTest';
                }
                //Adding case with subject as reply
                if(i >= 7)
                {
                	c.Subject = 'Re: MailTest';
                }
            }else{
                c.Subject =  'Mail '+i;
            }
            c.Priority = 'Normal';
            c.Status = 'New';
            c.Origin = 'Email';
            lcase.add(c);        
        }
        insert lcase;
        //Adding Attachment with parent as case
        Attachment attach=new Attachment();   	
    	attach.Name='Unit Test Attachment';
    	Blob bodyBlob=Blob.valueOf('Unit Test Attachment Body');
    	attach.body=bodyBlob;
        attach.parentId=lcase[7].id;
        insert attach;

        List<case> lstcase = new List<case>();
        //Adding cases with spam email
        for(Integer i=0;i < 10;i++){
            case c = new case();
            c.SuppliedEmail = 'testmail@formsite.com';
            c.Description = 'Hello i am a test mail to test the cases';
            c.Priority = 'Normal';
            c.Status = 'New';
            c.Subject =  'Spam Mail '+i;
            c.Origin = 'Email';
            lstcase.add(c);        
        }
        insert lstcase;
        Test.startTest();
            EmailtoCaseHandlerBatch upCon = new EmailtoCaseHandlerBatch();
            DataBase.executeBatch(upCon);
        Test.stopTest();
        List<case> lsttest = [select id,suppliedEmail from case where ID IN : lstcase];
      	System.assertEquals(0, lsttest.size());
        List<case> lst = [select id,suppliedEmail from case where Id =: lcase[7].Id limit 1];
      	System.assertEquals(0, lst.size()); 
    	
    }
    
}