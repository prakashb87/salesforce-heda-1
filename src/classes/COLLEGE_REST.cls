/*********************************************************************************
*** @ClassName         : COLLEGE_REST
*** @Author            : Shubham Singh 
*** @Requirement       : 
*** @Created date      : 28/08/2018
*** @Modified by       : Anupam Singhal
*** @modified date     : 20/09/2018
**********************************************************************************/
@RestResource(urlMapping = '/HEDA/v1/College')
global with sharing class COLLEGE_REST {
    public String institution;
    public String college;
    public String effectiveDate;
    public String status;
    public String description;
    public String shortDescription;
    
    @HttpPost
    global static Map < String, String > upsertCollege() {
        //to create map of effective date        
        map < String,Date > updateEffectiveDate = new map < String,Date >(); 
        //final response to send
        Map < String, String > responsetoSend = new Map < String, String > ();
        //upsert Account
        list < Account > upsertCollegeList = new list < Account > ();
        //get instituion
        list < String > listInstitution = new list < String > ();
        //getting college code from account
        list < String > listCollegeCode = new list < String > ();
        //getting the old Location for update
        list < Account > previousAccount = new list < Account > ();
        //Account object
        Account accnt;
        
        Account collegeExisRecord;
        //store institution
        Account institution;
        //key to get unique values
        String uniqueKey;
        
        String key;
        try {
            
            //request and response variable
            RestRequest request = RestContext.request;
            
            RestResponse response = RestContext.response;
            
            //no need to add [] in JSON
            String reqStr = '[' + request.requestBody.toString() + ']';
            
            response.addHeader('Content-Type', 'application/json');
            system.debug('reqStr-->' + reqStr);
            //getting the data from JSON and Deserialize it
            List < COLLEGE_REST > postString = (List < COLLEGE_REST > ) System.JSON.deserialize(reqStr, List < COLLEGE_REST > .class);
            
            if (postString != null) {
                for (COLLEGE_REST restColl : postString) {
                    
                    if ((restColl.college != null && (restColl.college).length() > 0)) {
                        listCollegeCode.add(restColl.college);
                        System.debug('listCollegeCode--------------'+listCollegeCode);
                        if (restColl.institution != null && (restColl.institution).length() >0)
                        {
                            listInstitution.add(restColl.institution);
                        }
                        if(restColl.effectiveDate != null && (restColl.effectiveDate).length() > 0)
                        {
                            updateEffectiveDate.put(restColl.college,Date.ValueOf(restColl.effectiveDate));
                        }
                    } else {
                        responsetoSend = throwError();
                        return responsetoSend;
                    }
                    
                }
            }
            //Store institution record
            if (listInstitution != null && listInstitution.size() > 0)
            {
                institution = [select id from account where Name =: listInstitution[0] Limit 1];
            }
            if (listCollegeCode.size() > 0 && listCollegeCode != null){// && listInstitution.size() > 0 && listInstitution != null) {
                previousAccount = [select id,Name,College_code__c,Academic_Institution__c,Academic_Institution__r.Name,Effective_Date__c,Status__c,Description,Short_Description__c  from Account where College_Code__c =: listCollegeCode[0] limit 1];
            }
            //map to update the old Location
            map < String, Account > oldAccntUpdate = new map < String, Account > ();
            
            if(previousAccount.size() > 0){
                for(Account prevAccount: previousAccount){
                    uniqueKey = prevAccount.College_Code__c;
                    if(updateEffectiveDate != null && updateEffectiveDate.get(uniqueKey) != null){
                        if(updateEffectiveDate.get(uniqueKey) <= System.today() && prevAccount.Effective_Date__c < updateEffectiveDate.get(uniqueKey) ){
                            oldAccntUpdate.put(uniqueKey, prevAccount);    
                        }else if(updateEffectiveDate.get(uniqueKey) > System.today()){
                            caseMatching(postString,previousAccount);
                        }else if(prevAccount.Effective_Date__c > updateEffectiveDate.get(uniqueKey)){
                            responsetoSend.put('message', 'OK');
                            responsetoSend.put('status', '200');
                            return responsetoSend;
                        }
                        
                    }else{
                        responsetoSend.put('message', 'OK');
                        responsetoSend.put('status', '200');
                        return responsetoSend;
                    }        
                }
            }
            
            
            //insert or update the account
            for (COLLEGE_REST accntRest : postString) {            
                accnt = new Account();
                key = accntRest.college;
                accnt.RecordTypeId = Schema.SObjectType.Account.getRecordTypeInfosByName().get('Academic Institution').getRecordTypeId();
                accnt.Name = accntRest.description != null && (accntRest.description).length() > 0? accntRest.description : accntRest.college ;
                accnt.Academic_Institution__c = institution != null ? institution.ID : null;
                accnt.AccountNumber = accntRest.college;
                accnt.AccountNumber__c = accntRest.college;
                accnt.Effective_Date__c = accntRest.effectiveDate != null && (accntRest.effectiveDate).length() > 0 ? Date.valueOf(accntRest.effectiveDate) : null;
                System.debug('accnt.Effective_Date__c ------------------> '+ accnt.Effective_Date__c);
                accnt.Status__c = accntRest.status;
                accnt.Description = accntRest.description;
                accnt.Short_Description__c = accntRest.shortDescription;
                accnt.College_code__c = accntRest.college;
                accnt.Level_Name__c = 'College';
                //assigning the account id for update
                if (oldAccntUpdate.size() > 0 && oldAccntUpdate.get(key) != null){
                    if(oldAccntUpdate.get(key).Effective_Date__c <= Date.ValueOf(accntRest.effectiveDate)){
                        accnt.ID = oldAccntUpdate.get(key).Id;
                    }else{
                        accnt = null;
                    }
                    
                }
                //add account to the list
                if(accnt !=null){
                    upsertCollegeList.add(accnt);    
                }
                
            }
            system.debug('upsertCollegeList-->' + upsertCollegeList);
            // add account to upsert
            if (upsertCollegeList != null && upsertCollegeList.size() > 0 && Date.valueOf(postString[0].effectiveDate) <= system.today()) {
                upsert upsertCollegeList AccountNumber__c;
            }
            
            responsetoSend.put('message', 'OK');
            responsetoSend.put('status', '200');
            return responsetoSend;
        
        } catch (Exception excpt) {
            //If exception occurs then return this message to IPASS
            responsetoSend = throwError();
            return responsetoSend;
        }
        
    }
    public static void caseMatching(List<COLLEGE_REST> postString, List<Account> prevAccounts){
        
        List<Future_Change__c> ListFutureChange = new List<Future_Change__c>();
        Future_Change__c FutureChangeRec = new Future_Change__c();
        
        if(postString[0].college.length() > 0){//check
            
            string effDateDB = null;
            if (prevAccounts[0].Effective_Date__c != Null){
                effDateDB = String.valueOf(prevAccounts[0].Effective_Date__c);
            }
            
            string effDateJSON = null;
            if (postString[0].effectiveDate.length() > 0){
                effDateJSON = postString[0].effectiveDate;                           
            }
            
            if(effDateDB != effDateJSON){
                FutureChangeRec = COLLEGE_REST.createFutChange('Effective_Date__c');
                FutureChangeRec.Parent_Lookup__c = prevAccounts[0].Id;
                FutureChangeRec.Value__c = postString[0].effectiveDate;
                ListFutureChange.add(FutureChangeRec);
            }
            
            string StatusDB = null;
            if(prevAccounts[0].Status__c != null){
                StatusDB = prevAccounts[0].Status__c;
            }
            
            string StatusJSON = null;
            if(postString[0].status.length() > 0 ){
                if(postString[0].status == 'A'){
                    StatusJSON = 'A';
                }else if(postString[0].status == 'I'){
                    StatusJSON = 'I';
                }
            }
            if(StatusDB != StatusJSON){
                FutureChangeRec = COLLEGE_REST.createFutChange('Status__c');
                FutureChangeRec.Parent_Lookup__c = prevAccounts[0].Id;
                FutureChangeRec.Value__c = StatusJSON;
                ListFutureChange.add(FutureChangeRec);
            }
            
            string acadInstDB=null;
            if(prevAccounts[0].Academic_Institution__c != null){    //Error
                acadInstDB = prevAccounts[0].Academic_Institution__r.Name;
            }
            
            string acadInstJSON = null;
            if(postString[0].institution.length() > 0){
                acadInstJSON = postString[0].institution;
            }
            
            if(acadInstDB != acadInstJSON){
                FutureChangeRec = COLLEGE_REST.createFutChange('Academic_Institution__c');
                FutureChangeRec.Parent_Lookup__c = prevAccounts[0].Id ;
                FutureChangeRec.Value__c = acadInstJSON != null ?[SELECT Id FROM ACCOUNT WHERE Name =: acadInstJSON LIMIT 1].size()>0?[SELECT Id FROM ACCOUNT WHERE Name =: acadInstJSON LIMIT 1].Id:acadInstJSON:acadInstJSON;
                ListFutureChange.add(FutureChangeRec);
            }
            
            
            string DescriptionDB = null;
            if(prevAccounts[0].Description != Null){
                DescriptionDB = prevAccounts[0].Description;
            }
            
            string DescriptionJSON = null;
            if(postString[0].Description.length() >0 ){
                DescriptionJSON = postString[0].Description;
            }  
            
            if(DescriptionDB != DescriptionJSON){
                FutureChangeRec = COLLEGE_REST.createFutChange('Description');
                FutureChangeRec.Parent_Lookup__c = prevAccounts[0].Id;
                FutureChangeRec.Value__c = postString[0].Description;
                ListFutureChange.add(FutureChangeRec);
            }    
            
            string shortDescriptionDB = null;
            if(prevAccounts[0].Short_Description__c != Null){
                shortDescriptionDB = prevAccounts[0].Short_Description__c;
            }
            
            string shortDescriptionJSON = null;
            if(postString[0].shortDescription.length() >0 ){
                shortDescriptionJSON = postString[0].shortDescription;
            }  
            
            if(shortDescriptionDB != shortDescriptionJSON){
                FutureChangeRec = COLLEGE_REST.createFutChange('Short_Description__c');
                FutureChangeRec.Parent_Lookup__c = prevAccounts[0].Id;
                FutureChangeRec.Value__c = postString[0].shortDescription;
                ListFutureChange.add(FutureChangeRec);
            }
            if(ListFutureChange != null && ListFutureChange.size() > 0){
                Database.insert(ListFutureChange);    
            }     
        } 
    }
    public static Map < String, String > throwError() {
        //to store the error response
        Map < String, String > errorResponse = new Map < String, String > ();
        errorResponse.put('message', 'Oops! College was not well defined, amigo.');
        errorResponse.put('status', '404');
        //If error occurs then return this message to IPASS
        return errorResponse;
    }
    
    public static Map<String, Schema.DescribeFieldResult> getFieldMetaData(Schema.DescribeSObjectResult dsor, Set<String> fields) {
        
        // the map to be returned with the final data
        Map<String,Schema.DescribeFieldResult> finalMap = 
            new Map<String, Schema.DescribeFieldResult>();
        // map of all fields in the object
        Map<String, Schema.SObjectField> objectFields = dsor.fields.getMap();
        
        // iterate over the requested fields and get the describe info for each one. 
        // add it to a map with field name as key
        for(String field : fields){
            // skip fields that are not part of the object
            if (objectFields.containsKey(field)) {
                Schema.DescribeFieldResult dr = objectFields.get(field).getDescribe();
                // add the results to the map to be returned
                finalMap.put(field, dr); 
            }
        }
        return finalMap;
    }
    
    public static Future_Change__c createFutChange(String str){
        //request and response variable
        RestRequest request = RestContext.request;
        
        RestResponse response = RestContext.response;
        
        //no need to add [] in JSON
        String reqStr = '[' + request.requestBody.toString() + ']';
        
        response.addHeader('Content-Type', 'application/json');
        List < COLLEGE_REST > postString = (List < COLLEGE_REST > ) System.JSON.deserialize(reqStr, List < COLLEGE_REST > .class);
        Future_Change__c planFu = new Future_Change__c();
        Set<String> fields = new Set<String>{str};
            
            Map<String, Schema.DescribeFieldResult> finalMap = 
            COLLEGE_REST.getFieldMetaData(Account.getSObjectType().getDescribe(), fields);
        
        // only print out the 'good' fields
        for (String field : new Set<String>{str}) {
            planFu.API_Field_Name__c = finalMap.get(field).getName();
            planFu.Field__c = finalMap.get(field).getLabel();
        }
        
        for (COLLEGE_REST checkDate : postString) {
            string jsonDate = checkDate.effectiveDate;
            planFu.Effective_Date__c = Date.valueOf(jsonDate);
        }
        
        //planFu.Applied__c = true;
        
        return planFu;
    }
}