/****************************************************
Purpose: Apex Sharing Utility class for System mode
History:
Created by Esther on 10/26/2018
Modified by Subhajit for code comment 27.10.18
****************************************************/
public without Sharing class ApexWithoutSharingUtils {
    
    /*****************************************************************************************
    // Global Declaration Block
    *****************************************************************************************/ 
    public static final string KEY1 ='queueId';
    public static final string KEY2 ='routedEmailAddress';
    
     /************************************************************************************
    // Purpose      : To check current login user is community user or salesforce user
                   
    //***********************************************************************************/    
	    
    public static Boolean checkInternalDualAccessUser(){
            Boolean isInternalDualAccessUser = false;
             if(userInfo.getUserType()=='Standard'){
            	isInternalDualAccessUser = true;
            }
            return isInternalDualAccessUser;
    } 
    
    // Refer the business rules:RPORW-352    
    public static map<id, string> getSchoolNamebyContactId(set<id> contactIdSet){
        
        map<id, string> mapContactIdSchoolName = new map<id,string>();
         /* Get the staff School Name from Affiliation..
            1) get the current affliation for the staff. There will be only one affiliation with current status.
        */
        if((Schema.sObjectType.hed__Affiliation__c.isAccessible()) && (Schema.sObjectType.hed__Affiliation__c.fields.hed__Contact__c.isAccessible())){   //added by ali PMD voilation
        list<hed__Affiliation__c> lstAffiliation =[SELECT id, hed__Contact__c,hed__Account__c,hed__Account__r.name FROM hed__Affiliation__c
                                                     WHERE hed__Status__c ='Current' and hed__Contact__c = :contactIdSet
                                                        and hed__Contact__r.Enumber__c !=null and hed__Account__r.name!=null
                                                        and recordtypeid=: Schema.SObjectType.hed__Affiliation__c.getRecordTypeInfosByName().get(label.ResearchMemberAffilationStaff).getRecordTypeId()];
        system.debug('***&&affils'+lstAffiliation);
       
        for(hed__Affiliation__c aAffiliation:lstAffiliation){
            mapContactIdSchoolName.put(aAffiliation.hed__Contact__c,aAffiliation.hed__Account__r.name); 
            system.debug('***&&mapContactIdSchoolName'+mapContactIdSchoolName);
        }
        
        }
        /* Get the current school Name for Student
         1) find the program enrollment for the all students
         2) get the latest program enrollment based on effective date
         3) find out the program(Account) from program enrollment
         4) from Program value, find out the parent account(school)        
        this logic is to fetch account name if the contact is Student */
         if((Schema.sObjectType.hed__Program_Enrollment__c.isAccessible()) && (Schema.sObjectType.hed__Program_Enrollment__c.fields.hed__Contact__c.isAccessible())){   //added by ali PMD voilation
        list<hed__Program_Enrollment__c> progEnrollmentList= [SELECT id,hed__Contact__c,hed__Account__r.ParentId,
                                        hed__Account__r.Parent.Name FROM hed__Program_Enrollment__c 
                                                    WHERE hed__Contact__c = :contactIdSet And hed__Contact__r.Student_ID__c !=null 
                                                    order by Effective_Date__c desc];
  
        for(hed__Program_Enrollment__c aProgEnroll:progEnrollmentList){
            if(!(mapContactIdSchoolName!=null && mapContactIdSchoolName.containskey(aProgEnroll.hed__Contact__c))&& aProgEnroll.hed__Account__c!=null&& aProgEnroll.hed__Account__r.ParentId!=null){
                             mapContactIdSchoolName.put(aProgEnroll.hed__Contact__c,aProgEnroll.hed__Account__r.Parent.Name); 
           
        }
        }   
         } 
        return mapContactIdSchoolName;
        
    }
    /*This method to update the RMS record where current login user
    does not have permission to change permission
    This method needs to run in without sharing mode to allow the perform deletion*/
    public static void updateRMSrecords(list<Researcher_Member_Sharing__c> updateRMS){     
            update updateRMS;     
            system.debug('@@@@@updateLog==>'+JSON.serializePretty(updateRMS));
    }
    /*This method to delete the Ethics Project records where current login user
    does not have permission to change permission
    This method needs to run in without sharing mode to allow the perform deletion*/
    
    public static void deleteEthicsProjectRecords(list<Ethics_Researcher_Portal_Project__c> deleteResearchPortalEthics){     
            delete deleteResearchPortalEthics;     
    }
    
    /*****************************************************************************************
    // SPRINT       :  SPRINT-4
    // Purpose      :  Sharing Researcher_Member_Sharing__c records manualy to project members
    // Parameters   :  Researcher_Member_Sharing__c 
    // Developer    :  Ankit/Subhajit
    // Created Date :  10/10/2018                 
    //****************************************************************************************/    
    public static Researcher_Member_Sharing__Share shareRMSrecords(Researcher_Member_Sharing__c eachRMS,String access){
  
            Researcher_Member_Sharing__Share rmsShare = new Researcher_Member_Sharing__Share();
            rmsShare.ParentId = eachRMS.Id;
            rmsShare.UserOrGroupId = UserInfo.getUserId();
            rmsShare.AccessLevel = access;
            rmsShare.RowCause = Label.RSTP_ManualApexSharing;
      
            return rmsShare;     
    }

   /*****************************************************************************************
    // JIRA NO      :  470/314/469
    // SPRINT       :  7
    // Purpose      :  get queueid and routed emailAddress
    // Parameters   :  string,String
    // Developer    :  Subhajit
    // Created Date :  11/10/2018                 
    //****************************************************************************************/
    public static Map<String,String> getQueueIdAndEmailAddress (string topic,String subTopic,String recordtypeName)
    {
        // Declaration and initialisation Block  
        String queueId='';
        Map<String,String> getQueueAndEmailMap = new Map<String,String>();
        TopicSubtopicQueueMapping__mdt allTopicSubtopicQueueMappings;
        
        //Fetch queue name and routed email from TopicSubtopicQueueMapping__mdt by filtering topic and subtopic from frontend
        if(SobjectUtils.checkOrgType()&&(Schema.sObjectType.TopicSubtopicQueueMapping__mdt.isAccessible())
            &&(Schema.sObjectType.TopicSubtopicQueueMapping__mdt.fields.QueueName__c.isAccessible()))
        {
            System.debug('@@@1');
            allTopicSubtopicQueueMappings = [
                SELECT QueueName__c,Test_Email_Address__c
                FROM TopicSubtopicQueueMapping__mdt
                WHERE Topic__c =: topic
                AND Subtopic__c =: subTopic
                AND Case_Record_Type__c =: recordtypeName
                LIMIT 1
            ]; 
            getQueueAndEmailMap.put(KEY2,allTopicSubtopicQueueMappings.Test_Email_Address__c);
        }
        else
        {           System.debug('@@@2');
            
            allTopicSubtopicQueueMappings = [
                SELECT QueueName__c,EmailAddress__c 
                FROM TopicSubtopicQueueMapping__mdt
                WHERE Topic__c =: topic
                AND Subtopic__c =: subTopic
                AND Case_Record_Type__c =: recordtypeName
                LIMIT 1
            ]; 
            getQueueAndEmailMap.put(KEY2,allTopicSubtopicQueueMappings.EmailAddress__c);
        }
        
        system.debug('@@@@allTopicSubtopicQueueMappings'+JSON.serializePretty(allTopicSubtopicQueueMappings));  
        
        //getting queueId from QueueSobject
        if(!String.isEmpty(allTopicSubtopicQueueMappings.QueueName__c)) 
        {
            queueId =[SELECT QueueId FROM QueueSobject WHERE Queue.name=:allTopicSubtopicQueueMappings.QueueName__c].QueueId;
        }
        
        getQueueAndEmailMap.put(KEY1,queueId);
        
        
        return getQueueAndEmailMap;
    } 
    
    
    /*****************************************************************************************
    // SPRINT       :  SPRINT-11
    // Purpose      :  Querying Project Contracts for without Sharing for 251
    // Parameters   :  projectId 
    // Developer    :  Ankit
    // Created Date :  19/02/2019                 
    //****************************************************************************************/    
    public static List<ProjectContract__c> getProjectContractsList(Id projectId){
  
            List <ProjectContract__c> projectContracts = new  List <ProjectContract__c>();
            if((Schema.sObjectType.ProjectContract__c.isAccessible()) && (Schema.sObjectType.ProjectContract__c.fields.Research_Project_Contract__c.isAccessible())){   //added by ali PMD voilation
                System.debug('Check PMD');
            }
            projectContracts = [SELECT Id,Research_Project_Contract__c,Researcher_Portal_Project__c 
                                                                    FROM ProjectContract__c
                                                                    WHERE Researcher_Portal_Project__c =: projectId];  
                                                                    
            return projectContracts;                                                      
    }
    
    /*****************************************************************************************
    // SPRINT       :  SPRINT-11
    // Purpose      :  Querying Project Ethics for without Sharing
    // Parameters   :  projectId 
    // Developer    :  Prakash
    // Created Date :  21/03/2019                 
    //****************************************************************************************/ 
    
    public static List<Ethics_Researcher_Portal_Project__c> getResearcherEthicsRecords(Id projectId){
  
            
            List<Ethics_Researcher_Portal_Project__c> ethicsProjectList = new List<Ethics_Researcher_Portal_Project__c>();
            if((Schema.sObjectType.Ethics_Researcher_Portal_Project__c.isAccessible()) && (Schema.sObjectType.Ethics_Researcher_Portal_Project__c.fields.Ethics__c.isAccessible())){   //added by ali PMD voilation
            ethicsProjectList = [Select id, name, Researcher_Portal_Project__c,Ethics__c
                             From  Ethics_Researcher_Portal_Project__c
                             where Researcher_Portal_Project__c  =: ProjectId];
            }                                                        
            return ethicsProjectList ;                                                      
    }
    
    /*************************************************************************************************
    // SPRINT       :  SPRINT-11
    // Purpose      :  Fetch user details for chatter
    // Parameters   :  userIds
    // Developer    :  Prakash
    // Created Date :  28/03/2019     
    
    ***************************************************************************************************/
    
    public static Map<Id, User> getUserMap(Set<Id> userIds){
        
        Map<Id, User> userMaps = new Map<Id, User>();
        for ( User usr : [SELECT Id, Name, Email, SmallPhotoUrl, FullPhotoUrl FROM User WHERE ID IN: userIds]){
            userMaps.put(usr.Id, usr);
        }
        return userMaps;
    }
    
    /*************************************************************************************************
    // SPRINT       :  SPRINT-4
    // Purpose      :  Query Ethics List using Without Sharing to display Ethics List in Project Detail Page
    // Parameters   :  ethicsIds
    // Developer    :  Ankit
    // Created Date :  02/04/2019     
    ***************************************************************************************************/
        public static List<Ethics__c> getEthicsList( Set<Id> ethicsIds){
        
        List<Ethics__c> ethicsList = new List<Ethics__c>();
        if((Schema.sObjectType.Ethics__c.isAccessible()) && (Schema.sObjectType.Ethics__c.fields.Application_Title__c.isAccessible())){  //added by ali PMD voilation
            ethicsList = [Select id, Application_Title__c, Application_Type__c, Status__c, Approved_Date__c, Expiry_Date__c
                          From Ethics__c where Id IN: ethicsIds ];
        }
        return ethicsList;
        
        
    }
    
    /*************************************************************************************************
    // SPRINT       :  SPRINT-4
    // Purpose      :  Query Contract List using Without Sharing to display Contract List in Project Detail Page
    // Parameters   :  contractIds
    // Developer    :  Ankit
    // Created Date :  02/04/2019     
    ***************************************************************************************************/
        public static List<ResearchProjectContract__c> getContractList( Set<Id> contractIds){
        
        List<ResearchProjectContract__c> researchProjectContracts = new List<ResearchProjectContract__c>();
        if((Schema.sObjectType.ResearchProjectContract__c.isAccessible()) && (Schema.sObjectType.ResearchProjectContract__c.fields.ContractTitle__c.isAccessible())){ 
        
        researchProjectContracts = [SELECT Id, Ecode__c, ContractTitle__c, Status__c, RIReviewer__c,
                                                         Contract_Logged_Date__c, Current_Active_Action__c, StorageReferenceNumber__c,
                                                         Fully_Executed_Date__c, Contact_Preferred_Full_Name__c, LInkedPrimaryOrganisationName__c,
                                                         Activity_Type__c, Contract_Type__c, PreExecutionManager__c,
                                                         (Select id, MemberRole__c, AccessLevel__c, SF_Role__c,RM_Member__c, // added by Ankit, 1512
                                                         Currently_Linked_Flag__c, User__c,User__r.name, Position__c From  Researcher_Portal_Member_Sharings__r)
                                                         FROM ResearchProjectContract__c
                                                         WHERE Id IN : contractIds];
        }
        return researchProjectContracts;
                                                         
        }
        
    /*************************************************************************************************
    // SPRINT       :  SPRINT-6
    // Purpose      :  Query Milestones related to Contract to display in Contract Detail Page
    // Parameters   :  contractId
    // Developer    :  Prakash
    // Created Date :  30/04/2019     
    ***************************************************************************************************/
        public static List<SignificantEvent__c> getContractMilestoneList(Id contractId){
        
            Id contractMilestoneRecordTypeId = Schema.SObjectType.SignificantEvent__c.getRecordTypeInfosByName().get(Label.RSTP_MilestoneContractRecordtype).getRecordTypeId();
            List<SignificantEvent__c> milestonesList = new List<SignificantEvent__c>();
            if((Schema.sObjectType.SignificantEvent__c.isAccessible()) && (Schema.sObjectType.SignificantEvent__c.fields.ActualCompletionDate__c.isAccessible())){  //added by ali PMD voilation
                milestonesList = [Select id, ActualCompletionDate__c, DueDate__c, Ethics__c, Ethics__r.Application_Title__c,  EventDescription__c,IsAction__c,
                                              IsActionDue__c, Publication__c, Publication__r.Publication_Title__c, ResearchProject__c, ResearchProject__r.Title__c,
                                                  ResearchProjectContract__c, ResearchProjectContract__r.ContractTitle__c,
                            SignificantEventType__c, Status__c , RecordTypeId
                                                  From SignificantEvent__c 
                                                  where ResearchProjectContract__c =:contractId
                                                  AND RecordTypeId =: contractMilestoneRecordTypeId
                            ORDER BY DueDate__c DESC  NULLS LAST ];                                              
            }
            return milestonesList;
        }
        
    /*************************************************************************************************
    // SPRINT       :  SPRINT-6
    // Purpose      :  To return list of milestone count on the left nav bar
    // Parameters   :  List of RMS
    // Developer    :  Prakash
    // Created Date :  30/04/2019     
    ***************************************************************************************************/
    public static List<SignificantEvent__c> getMilestonesList(List<Researcher_Member_Sharing__c> reseacherMemberSharingList){
        Set<Id> ethicsIdSet      = new Set<Id>();
        Set<Id> projectIdSet     = new Set<Id>();
        Set<Id> publicationIdSet = new Set<Id>();
        Set<Id> contractIdSet    = new Set<Id>();

        System.debug('@@@reseacherMemberSharingList'+reseacherMemberSharingList);
        // 1) Ethics - Count the CI role only see the notifications
        // 2) Project- CI and CI delegate (Member role=CI) only see the notifications
        // 3) Publication and contracts - all active member can see the notifications
        for(Researcher_Member_Sharing__c eachmemberSharing :reseacherMemberSharingList){
            if(!String.isBlank(eachmemberSharing.Ethics__c) && eachMembersharing.MemberRole__c== system.label.CI){
                ethicsIdSet.add(eachmemberSharing.Ethics__c);
            } 
            else if(!String.isBlank(eachmemberSharing.Researcher_Portal_Project__c) && eachMembersharing.MemberRole__c== system.label.CI)
            {
                projectIdSet.add(eachmemberSharing.Researcher_Portal_Project__c);
            }  
            else if(!String.isBlank(eachmemberSharing.Publication__c)) {
                publicationIdSet.add(eachmemberSharing.Publication__c);
            }
            else if(!String.isBlank(eachmemberSharing.ResearchProjectContract__c)) {
                contractIdSet.add(eachmemberSharing.ResearchProjectContract__c);
            }  

        }  
    
        
        
        return getMileStonesListQuery(contractIdSet, ethicsIdSet, projectIdSet, publicationIdSet);
    }
    
    public static List<SignificantEvent__c> getMileStonesListQuery(Set<Id> contractIdSet, Set<Id> ethicsIdSet, Set<Id> projectIdSet , Set<Id> publicationIdSet){
        
        checkAccessToSignificantEvent();
        List<SignificantEvent__c> milestonesList = [SELECT id, ActualCompletionDate__c, DueDate__c, Ethics__c, Ethics__r.Application_Title__c,  EventDescription__c,IsAction__c,
                                              IsActionDue__c, Publication__c, Publication__r.Publication_Title__c, ResearchProject__c, ResearchProject__r.Title__c,
                                              ResearchProjectContract__c, ResearchProjectContract__r.ContractTitle__c,
                                              SignificantEventType__c, Status__c , RecordTypeId
                                              FROM SignificantEvent__c 
                                              WHERE Ethics__c IN:ethicsIdSet
                                              OR ResearchProject__c IN:projectIdSet
                                              OR Publication__c IN:publicationIdSet
                                              OR ResearchProjectContract__c IN:contractIdSet
                                              ORDER BY DueDate__c DESC];
         return milestonesList;                                    
    }

    public static void checkAccessToSignificantEvent(){
        if(!Schema.sObjectType.SignificantEvent__c.isAccessible()){   
            System.debug('Check PMD');
            throw new DMLException();
        }
    }
	
	public static List<Case> getCaseList(Set<Id> caseIds){
        
        if(Schema.sObjectType.Case.fields.Id.isAccessible()) {
            System.debug('Is not accessible');
        }
           
        return [Select id,CreatedById,ContactId From Case where Id In: caseIds];
    
    }
    
    public static void updateCases(List<Case> caseList){
        
        if (!Schema.sObjectType.Case.fields.Id.isUpdateable()){
            System.debug('Not Updateable');
        }
        Update caseList;
    
    }

}