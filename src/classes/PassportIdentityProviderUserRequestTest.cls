/*
 * @description :  Test class for PassportIdentityProviderUserRequest class
 */
 @isTest
public class PassportIdentityProviderUserRequestTest
{
	 @TestSetup
    static void makeData()
    {
        List<Config_Data_Map__c> configurations = new List<Config_Data_Map__c>();
        Config_Data_Map__c configDataMapObj1 = new Config_Data_Map__c();
        configDataMapObj1.Name = 'IDAM_ENCRYPT_IV';
        configDataMapObj1.Config_Value__c = '2kmlr43eCkTe#rki';
        configurations.add(configDataMapObj1);
        
        Config_Data_Map__c configDataMapObj2 = new Config_Data_Map__c();
        configDataMapObj2.Name = 'IDAM_ENCRYPT_KEY';
        configDataMapObj2.Config_Value__c = 'Zs8#X8sQ4a#KV5ap';
        configurations.add(configDataMapObj2);
        
        Config_Data_Map__c configDataMapObj3 = new Config_Data_Map__c();
        configDataMapObj3.Name = 'IDAM_ENCRYPT_ALGORITHM';
        configDataMapObj3.Config_Value__c = 'AES128';
        configurations.add(configDataMapObj3);

         Config_Data_Map__c configDataMapObj4 = new Config_Data_Map__c();
        configDataMapObj4.Name = 'IDAMIpaaSEndPoint';
        configDataMapObj4.Config_Value__c = 'http://ecb-mock.getsandbox.com/api/users?email=test@test.com';
        configurations.add(configDataMapObj4);

         Config_Data_Map__c configDataMapObj5 = new Config_Data_Map__c();
        configDataMapObj5.Name = 'IDAMIpaaSClientID';
        configDataMapObj5.Config_Value__c = '123';
        configurations.add(configDataMapObj5);

         Config_Data_Map__c configDataMapObj6 = new Config_Data_Map__c();
        configDataMapObj6.Name = 'IDAMIpaaSClientSecret';
        configDataMapObj6.Config_Value__c = '123';
        configurations.add(configDataMapObj6);

        insert configurations;
    }

	 @isTest
    static void checkInValidConstructorTest()
     {
       try
       {
	     	PassportIdentityProviderUserRequest passport1 = new PassportIdentityProviderUserRequest( null );

       }
       catch( Exception ex)
       {
       	system.assert(true);
       }
        
    }
	 @isTest
    static void checkValidFirstNameTest()
     {
       try
       {
	       	PassportIdentityProviderUserRequest passport = new PassportIdentityProviderUserRequest();
	        passport.setFirstName('TestFirstName');    
       }
       catch( Exception ex)
       {
       	system.assert(false);
       }
        
    }

	 @isTest
    static void checkInValidFirstNameTest()
     {
       try
       {
	       PassportIdentityProviderUserRequest passport = new PassportIdentityProviderUserRequest();
	        passport.setFirstName('');
       }
       catch(Exception ex)
       {
       	system.assert( true );
       }
             
    }
     @isTest
    static void checkValidLastNameTest()
     {
       try
       {
	       	PassportIdentityProviderUserRequest passport = new PassportIdentityProviderUserRequest();
	        passport.setLastName('TestLastName');    
       }
       catch( Exception ex)
       {
       	system.assert(false);
       }
        
    }

	 @isTest
    static void checkInValidlastNameTest()
     {
       try
       {
	       PassportIdentityProviderUserRequest passport = new PassportIdentityProviderUserRequest();
	        passport.setLastName('');    
       }
       catch(Exception ex)
       {
       	system.assert( true );
       }
             
    }
    @isTest
    static void checkValidEmailTest()
     {
       try
       {
	       	PassportIdentityProviderUserRequest passport = new PassportIdentityProviderUserRequest();
	        passport.setEmail('TestEmail');    
       }
       catch( Exception ex)
       {
       	system.assert(false);
       }
        
    }
    @isTest
    static void checkInValidEmailTest()
     {
       try
       {
	       	PassportIdentityProviderUserRequest passport = new PassportIdentityProviderUserRequest();
	        passport.setEmail('');    
       }
       catch( Exception ex)
       {
       	system.assert(true);
       }
        
    }
    @isTest
    static void checkValidMobileTest()
     {
       try
       {
	       	PassportIdentityProviderUserRequest passport = new PassportIdentityProviderUserRequest();
	        passport.setMobile('1234');    
       }
       catch( Exception ex)
       {
       	system.assert(false);
       }
        
    }
     @isTest
    static void checkInValidMobileTest()
     {
       try
       {
	       	PassportIdentityProviderUserRequest passport = new PassportIdentityProviderUserRequest();
	        passport.setMobile('');    
       }
       catch( Exception ex)
       {
       	system.assert(true);
       }
        
    }
     @isTest
    static void checkValidPassword()
     {
       try
       {
	       	PassportIdentityProviderUserRequest passport = new PassportIdentityProviderUserRequest();
	        passport.setPassword('abcd'); 	       
       }
       catch( Exception ex)
       {
       	system.assert(false);
       }       
    }
     @isTest
    static void checkinValidPassword()
     {
       try
       {
          PassportIdentityProviderUserRequest passport = new PassportIdentityProviderUserRequest();
          passport.setPassword('');          
       }
       catch( Exception ex)
       {
        system.assert(true);
       }       
    }
     @isTest
    static void checkEncryptPassword()
     {
       try
       {

	       	PassportIdentityProviderUserRequest passport = new PassportIdentityProviderUserRequest();
	        String encrypted = passport.encryptUserPassword('1234');
	        system.assert(String.isNotBlank(encrypted)); 	       
       }
       catch( Exception ex)
       {
       	system.assert(false);
       }
        
    }
     @isTest
    static void checkCreateUserSuccessTest()
     {
       try
       {

       	 	String json = '{"id": "ID-devqa-b040144b-4d9a-4ac0-9c70-895ec71f5e13","result": "SUCCESS","code": "200","application": "rmit-system-api-idm","provider": "idm","payload": {"success": true,"message": "","federationID": "O909908317"}}';
			Test.setMock(HttpCalloutMock.class, new IDAMUserCreationCheckMock());
        
            Test.startTest();
        	PassportIdentityProviderUserRequest passport = new PassportIdentityProviderUserRequest();
        	passport.setFirstName('AAA');
        	passport.setLastName('BBB');
        	passport.setEmail('aa@gmail.com');
        	passport.setMobile('12345');
        	passport.setPassword('abcd');

	        passport.createUser();	
	        string fedId = passport.getFederationId();
            Test.stopTest();
            System.assert(String.isNotBlank(fedId));
	              
       }
       catch( Exception ex)
       {
       	system.assert(false);
       }
        
    }

     @isTest
    static void checkCreateUserpayloadSuccessFalseTest()
     {
       try
       {

       	 	String json = '{"id": "ID-devqa-b040144b-4d9a-4ac0-9c70-895ec71f5e13","result": "SUCCESS","code": "200","application": "rmit-system-api-idm","provider": "idm","payload": {"success": false,"message": "","federationID": "O909908317"}}';
			Test.setMock(HttpCalloutMock.class, new IDAMUserCreationPayloadSuccessIsFalseMock());
        
            Test.startTest();
        	PassportIdentityProviderUserRequest passport = new PassportIdentityProviderUserRequest();
        	passport.setFirstName('AAA');
        	passport.setLastName('BBB');
        	passport.setEmail('aa@gmail.com');
        	passport.setMobile('12345');
        	passport.setPassword('abcd');

	        passport.createUser();	
            Test.stopTest();	              
       }
       catch( Exception ex)
       {
       	system.assert(true);
       }
        
    }

     @isTest
    static void checkCreateUserFailureTest()
     {
       try
       {

       	 	String json = '{"id": "ID-dev-6r87t87587","result": "Error","code": "400","application": "rmit-system-api-idam","provider": "idam","payload":{"success": false,"message": ""}}';
			Test.setMock(HttpCalloutMock.class, new IDAMUserCreationCheckFailureMock());
        
            Test.startTest();
        	PassportIdentityProviderUserRequest passport = new PassportIdentityProviderUserRequest();
        	passport.setFirstName('AAA');
        	passport.setLastName('BBB');
        	passport.setEmail('aa@gmail.com');
        	passport.setMobile('12345');
        	passport.setPassword('abcd');

	        passport.createUser();	
            Test.stopTest();	              
       }
       catch( Exception ex)
       {
       	system.assert(true);
       }
        
    }

     @isTest
    static void checkCreateUserValidationTest()
     {
       try
       {
            Test.startTest();
        	PassportIdentityProviderUserRequest passport = new PassportIdentityProviderUserRequest();
        	passport.firstName = '' ;
        	passport.lastName = '';
        	passport.email = '';
        	passport.mobile = '';
        	passport.password = '';
       
	        passport.checkCreateUserRequiredState();	
            Test.stopTest();	              
       }
       catch( Exception ex)
       {
       	system.assert(true);
       }
        
    }



    //---------------------------------Mock Responses-------------------------------------------//
    public class IDAMUserCreationCheckMock implements HttpCalloutMock 
    {

         public HTTPResponse respond(HTTPRequest request)
         {
            String jsonBody = '{"id": "ID-devqa-b040144b-4d9a-4ac0-9c70-895ec71f5e13","result": "SUCCESS","code": "200","application": "rmit-system-api-idm","provider": "idm","payload": {"success": true,"message": "","federationID": "O909908317"}}';
            HttpResponse response = new HttpResponse();
            response.setHeader('Content-Type', 'application/json');
            response.setBody(jsonBody);
            response.setStatusCode(200);
            return response;
        }
    }
    public class IDAMUserCreationPayloadSuccessIsFalseMock implements HttpCalloutMock 
    {

         public HTTPResponse respond(HTTPRequest request)
         {
            String jsonBody = '{"id": "ID-devqa-b040144b-4d9a-4ac0-9c70-895ec71f5e13","result": "SUCCESS","code": "200","application": "rmit-system-api-idm","provider": "idm","payload": {"success": false,"message": "","federationID": "O909908317"}}';
            HttpResponse response = new HttpResponse();
            response.setHeader('Content-Type', 'application/json');
            response.setBody(jsonBody);
            response.setStatusCode(200);
            return response;
        }
    }

     public class IDAMUserCreationCheckFailureMock implements HttpCalloutMock 
    {

         public HTTPResponse respond(HTTPRequest request)
         {
            String jsonBody = '{"id": "ID-dev-6r87t87587","result": "Error","code": "400","application": "rmit-system-api-idam","provider": "idam","payload":{"success": false,"message": ""}}';
            HttpResponse response = new HttpResponse();
            response.setHeader('Content-Type', 'application/json');
            response.setBody(jsonBody);
            response.setStatusCode(400);
            return response;
        }
    }

}