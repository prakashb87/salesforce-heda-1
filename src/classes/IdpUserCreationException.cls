/**
 * @description Exception for any problems with the Identity Provide user creation
 */
public class IdpUserCreationException extends Exception {}