/*********************************************************************************
*** @TestClassName     : COLLEGE_REST_Test
*** @Author            : Shubham Singh
*** @Requirement       : To Test the Location_Rest web service apex class.
*** @Created date      : 3/09/2018
**********************************************************************************/
@isTest
public class COLLEGE_REST_Test {
    static testMethod void myUnitTest() {
        Account acc = new Account();
        acc.Name = 'RMITU';
        insert acc;
        
        RestRequest req = new RestRequest();
        
        RestResponse res = new RestResponse();
        // adding header to the request
        req.addHeader('httpMethod', 'POST');
        
        req.requestUri = '/services/apexrest/HEDA/v1/College';
        
        //creating test data
        String JsonMsg = '{"institution": "RMITU","college": "110P","effectiveDate": "2005-01-01","status": "A","description": "SET Portfolio Office","shortDescription": "SET Portfo"}';
        
        req.requestBody = Blob.valueof(JsonMsg);
        
        RestContext.request = req;
        
        RestContext.response = res;
        
        Test.startTest();
        //store response
        Map < String, String > check = COLLEGE_REST.upsertCollege();
        List < String > str = check.values();
        //assert condition 
        System.assertEquals(True, str[0] == 'ok');
        
        Test.stopTest();
        
    }
    static testMethod void coverError() {
        Account acc = new Account();
        acc.Name = 'RMITU';
        insert acc;
        
        RestRequest req = new RestRequest();
        
        RestResponse res = new RestResponse();
        // adding header to the request
        req.addHeader('httpMethod', 'POST');
        
        req.requestUri = '/services/apexrest/HEDA/v1/College';
        
        //creating test data
        String JsonMsg = '{"institution": "","college": "","effectiveDate": "2005-01-01","status": "A","description": "SET Portfolio Office","shortDescription": "SET Portfo"}';
        
        req.requestBody = Blob.valueof(JsonMsg);
        
        RestContext.request = req;
        
        RestContext.response = res;
        
        Test.startTest();
        //store response
        Map < String, String > check = COLLEGE_REST.upsertCollege();
        List < String > str = check.values();
        //assert condition 
        System.assertEquals(False, str[0] == 'ok');
        
        Test.stopTest();
        
    }
    static testMethod void coverTryCatch() {
        Map < String, String > check;
        Test.startTest();
        try {
            //store response
            check = COLLEGE_REST.upsertCollege();
        } catch (DMLException e) {
            List < String > str = check.values();
            //assert condition 
            System.assertEquals(False, str[0] == 'ok');
        }
        Test.stopTest();
    }
    static testMethod void updateMethod() {
        Account acc = new Account();
        acc.Name = 'RMITU';
        insert acc;
        
        Account college = new Account();
        //need to comment if changed to auto number;
        college.Name = 'Test';
        college.RecordTypeId = Schema.SObjectType.Account.getRecordTypeInfosByName().get('Academic Institution').getRecordTypeId();
        college.Academic_Institution__c = acc.ID;
        college.College_Code__c = '110P';
        insert college;
        
        RestRequest req = new RestRequest();
        
        RestResponse res = new RestResponse();
        // adding header to the request
        req.addHeader('httpMethod', 'POST');
        
        req.requestUri = '/services/apexrest/HEDA/v1/College';
        
        //creating test data
        String JsonMsg = '{"institution": "RMITU","college": "110P","effectiveDate": "2005-01-01","status": "I","description": "SET Portfolio Office","shortDescription": "SET Portfo"}';
        
        req.requestBody = Blob.valueof(JsonMsg);
        
        RestContext.request = req;
        
        RestContext.response = res;
        
        Test.startTest();
        //store response
        Map < String, String > check = COLLEGE_REST.upsertCollege();
        List < String > str = check.values();
        //assert condition 
        System.assertEquals(True, str[0] == 'ok');
        
        Test.stopTest();
        
    }
        
    static testMethod void effectiveDateGreater() {
        
        
       	Account institution = new Account();
        institution.Name = 'RMITU';
        institution.AccountNumber = '110P';
        institution.College_code__c = '110P';
        insert institution;
        /*
        Account organisation = new Account();
        organisation.Name = '821';
        insert organisation;
        
        Future_Change__c fuchangeRec = new Future_Change__c();
        fuchangeRec.API_Field_Name__c = 'TEST API TEST';
        fuchangeRec.Value__c = 'Test';
        insert fuchangeRec;
        
        Account program = new Account(Name='FS001', Academic_Institution__c=institution.Id, Academic_Organisation__c = organisation.Id, Academic_Program__c = 'FS001');
        insert program;
        Date todaysDate = Date.today();
        Date futureDate = todaysDate.addDays(1);
        

        */
        RestRequest req = new RestRequest();
        
        RestResponse res = new RestResponse();
        // adding header to the request
        req.addHeader('httpMethod', 'POST');
        
        req.requestUri = '/services/apexrest/HEDA/v1/Class';
        
        //creating test data
        String JsonMsg = '{"institution": "RMITU","college": "110P","effectiveDate": "2022-01-01","status": "I","description": "SET Portfolio Office","shortDescription": "SET Portfo"}';
        
        req.requestBody = Blob.valueof(JsonMsg);
        
        RestContext.request = req;
        
        RestContext.response = res;
        
        Test.startTest();
        //store response
        Map < String, String > check = COLLEGE_REST.upsertCollege();
        system.debug('------Test class check---------'+check);
        List < String > str = check.values();
        system.debug('------Test class str---------'+str);
        //assert condition 
        System.assertEquals(True, str[0] == 'ok');
        
        Test.stopTest();
        
    }
}