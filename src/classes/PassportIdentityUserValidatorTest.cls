/**
 * This class contains unit tests for validating the behavior of Apex classes
 * and triggers.
 *
 * Unit tests are class methods that verify whether a particular piece
 * of code is working properly. Unit test methods take no arguments,
 * commit no data to the database, and are flagged with the testMethod
 * keyword in the method definition.
 *
 * All test methods in an organization are executed whenever Apex code is deployed
 * to a production organization to confirm correctness, ensure code
 * coverage, and prevent regressions. All Apex classes are
 * required to have at least 75% code coverage in order to be deployed
 * to a production organization. In addition, all triggers must have some code coverage.
 * 
 * The @isTest class annotation indicates this class only contains test
 * methods. Classes defined with the @isTest annotation do not count against
 * the organization size limit for all Apex scripts.
 *
 * See the Apex Language Reference for more information about Testing and Code Coverage.
 */
@isTest
private class PassportIdentityUserValidatorTest {

    @isTest
    static void blankEmailTest() {
        PassportIdentityUserValidator validator = new PassportIdentityUserValidator();
        System.assertEquals(false, validator.isEmailValid(''));
    }
    
    
    @isTest
    static void nullEmailTest() {
        PassportIdentityUserValidator validator = new PassportIdentityUserValidator();
        System.assertEquals(false, validator.isEmailValid(null));
    }


    @isTest
    static void validEmailTest() {
        PassportIdentityUserValidator validator = new PassportIdentityUserValidator();
        System.assertEquals(true, validator.isEmailValid('test.test1@example.com'));
    }
    
    
    @isTest
    static void invalidTooLongEmailTest() {
        PassportIdentityUserValidator validator = new PassportIdentityUserValidator();
        System.assertEquals(false, validator.isEmailValid('test123456789example123456salesforce@example.com'));
    }
    
    
    @isTest
    static void validFirstNameTest() {
        PassportIdentityUserValidator validator = new PassportIdentityUserValidator();
        System.assertEquals(true, validator.isFirstNameValid('John'));
    }
    
    
    @isTest
    static void invalidFirstNameTest() {
        PassportIdentityUserValidator validator = new PassportIdentityUserValidator();
        System.assertEquals(false, validator.isFirstNameValid('Adolph Blaine Charles David Earl Frederick Gerald Hubert'));
    }
    
    
    @isTest
    static void invalidBlankFirstNameTest() {
        PassportIdentityUserValidator validator = new PassportIdentityUserValidator();
        System.assertEquals(false, validator.isFirstNameValid(''));
    }
    
    
    @isTest
    static void nullBlankFirstNameTest() {
        PassportIdentityUserValidator validator = new PassportIdentityUserValidator();
        System.assertEquals(false, validator.isFirstNameValid(null));
    }
    
    
    @isTest
    static void validLastNameTest() {
        PassportIdentityUserValidator validator = new PassportIdentityUserValidator();
        System.assertEquals(true, validator.isLastNameValid('Smith'));
    }
    
    
    @isTest
    static void invalidLastNameTest() {
        PassportIdentityUserValidator validator = new PassportIdentityUserValidator();
        System.assertEquals(false, validator.isLastNameValid('Blaine Charles David Earl Frederick Xerxes Yancy Zeus'));
    }
    
    
    @isTest
    static void invalidBlankLastNameTest() {
        PassportIdentityUserValidator validator = new PassportIdentityUserValidator();
        System.assertEquals(false, validator.isLastNameValid(''));
    }
    
    
    @isTest
    static void nullBlankLastNameTest() {
        PassportIdentityUserValidator validator = new PassportIdentityUserValidator();
        System.assertEquals(false, validator.isLastNameValid(null));
    }
    
    
    @isTest
    static void validPhoneNumberTest() {
        PassportIdentityUserValidator validator = new PassportIdentityUserValidator();
        System.assertEquals(true, validator.isPhoneNumberValid('+61412936258'));
    }
    
    
    @isTest
    static void invalidPhoneNumberTooLongTest() {
        PassportIdentityUserValidator validator = new PassportIdentityUserValidator();
        System.assertEquals(false, validator.isPhoneNumberValid('+61412936258787854475215522458'));
    }
    
    
    @isTest
    static void invalidBlankPhoneNumberTest() {
        PassportIdentityUserValidator validator = new PassportIdentityUserValidator();
        System.assertEquals(false, validator.isPhoneNumberValid(''));
    }
    
    
    @isTest
    static void invalidNullPhoneNumberTest() {
        PassportIdentityUserValidator validator = new PassportIdentityUserValidator();
        System.assertEquals(false, validator.isPhoneNumberValid(null));
    }
}