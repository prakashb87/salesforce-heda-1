/*****************************************************************************************************
 *** @Class             : CheckBusinessAdminUser
 *** @Author            : Resmi Ramakrishnan
 *** @Requirement       : Apex class for check whether logged in user is 21CC standard user or Business admin user
 *** @Created date      : 19/07/2018
 *** @JIRA ID           : ECB-4201
 *** @LastModifiedBy    : Resmi Ramakrishnan
 *** @LastModified date : 19/07/2018
 
 *****************************************************************************************************/
public without sharing class CheckBusinessAdminUser 
{
    @AuraEnabled
    public static String getUserProfileName()
    {
        String userType = '';
        //ECB-5260 Addded RMIt Training Condition
        List<Profile> profileList = [Select Id,Name FROM Profile where (Name = :ConfigDataMapController.getCustomSettingValue('21CC Profile Name') OR Name = :ConfigDataMapController.getCustomSettingValue('RMIT Training Profile Name'))];        
        if(UserInfo.getProfileId() == profileList[0].Id)
        {
            //Querying PermissionSetAssignment to check whether logged in user is 21CC Standard User and contains Marketplace Business Admin permission set 
            List<PermissionSetAssignment> permissionSetAssignmentList = [Select Id FROM PermissionSetAssignment WHERE AssigneeId = :UserInfo.getUserId()  
                                                                        AND PermissionSetId IN (Select Id from PermissionSet where Name = :ConfigDataMapController.getCustomSettingValue('MarketPlace Permission Set'))];
          
        
            system.debug('permissionSetAssignmentList ::::'+permissionSetAssignmentList);    
            
            if(permissionSetAssignmentList.size() > 0)
            {
                userType = 'Business Admin';
            }
            else
            {
                userType = '21CC Standard User';
            }
        }
         system.debug('GGGGGGG=' + userType);
         return userType;
         
    }

}