/*****************************************************************
Name: DailyTransactionFileScheduler 
Author: Capgemini 
Purpose: To schedule the daily transaction file creation
JIRA Reference : ECB-4356 :  Daily transaction text file consumable by SAP integration
                 
*****************************************************************/
/*==================================================================
History 
--------
Version   Author            Date              Detail
1.0       Shreya            24/08/2018         Initial Version
********************************************************************/
@SuppressWarnings('PMD.AvoidGlobalModifier')
global class DailyTransactionFileScheduler implements Schedulable {
   global void execute(SchedulableContext sc) {  
    
        try{
            DailyTransactionFileController.getDailyTransactionFile();
        }catch(Exception ex){
            system.debug('Exception:::'+ex);
        }    

   }
}