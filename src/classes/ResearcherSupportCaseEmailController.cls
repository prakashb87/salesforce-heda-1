/*******************************************
Purpose: Controller class for Support Case Email Notification Functionalities: 
Due to limiation of email template component, custom controller and variables created to show/hide the section for child objects.
History:
Created by RSTP Team on 05/06/2019
*******************************************/

public without sharing class ResearcherSupportCaseEmailController{

    // Variables to set the parameter for Email template.
    
    // This variable to show the date time value in local timezone.
    public String caseSubmittedDatetime {get; set;}  
    
    // Display the respecitve section in the templates 
    public Boolean isProjectAssociated {get;set;}
    public Boolean isEthicsAssociated {get;set;}
    public Boolean isContractAssociated {get;set;}
    public Boolean isPublicationAssociated {get;set;}
    public Boolean isMilestoneAssociated {get;set;}
    
    // Get Set methods to get the component variables 
    public case researcherCase;    
    public case getResearcherCase(){ return researcherCase; }
    
    // Set the associated object variables for the email template sections.
    
    public void setResearcherCase(case aCase){
        system.debug('## setContIdChosen : '+aCase);
        researcherCase= aCase;
        caseSubmittedDatetime= researcherCase.CreatedDate.format();     
        isProjectAssociated = researcherCase.Researcher_Project_Cases__r.size()>0? true : false; 
        isEthicsAssociated  = researcherCase.Ethics_Cases__r.size()>0? true : false;
        isContractAssociated = researcherCase.Contracts_Cases__r.size()>0? true : false;
        isPublicationAssociated = researcherCase.Publications_Cases__r .size()>0? true : false;
        isMilestoneAssociated = researcherCase.Milestones_Cases__r.size()>0? true : false;
        
    }
    
   
}