public class ResearcherCalculateRMSShareBatch implements Database.batchable<sObject>{

    Set<Id> projectIds = new Set<Id>();
    Set<Id> ethicsIds = new Set<Id>();
    Set<Id> pubIds = new Set<Id>();
    Set<Id> contractIds = new Set<Id>();
        
    Set<String> projectUserIds = new Set<String>();
    Set<String> ethicsUserIds = new Set<String>();
    Set<String> pubUserIds = new Set<String>();
    Set<String> contUserIds = new Set<String>();    
        
    List<ResearcherPortalProject__Share> projectShareUpdate = new List<ResearcherPortalProject__Share>();
    List<Ethics__Share> ethicsShareUpdate = new List<Ethics__Share>();
    List<Publication__Share> pubShareUpdate = new List<Publication__Share>();
    List<ResearchProjectContract__Share> contShareUpdate = new List<ResearchProjectContract__Share>();
    
    public Database.QueryLocator start(Database.BatchableContext bc){
        
        String rmsList;
        
        rmsList = 'SELECT Researcher_Portal_Project__c,Publication__c,Ethics__c,ResearchProjectContract__c ,AccessLevel__c,User__c FROM Researcher_Member_Sharing__c WHERE AccessLevel__c != \'No Access\' AND (Researcher_Portal_Project__c != null OR ResearchProjectContract__c != null OR Publication__c != null OR Ethics__c != null) AND User__c != null';
        return Database.getQueryLocator(rmsList);
    }
    
    public void execute(Database.BatchableContext bc, List<Researcher_Member_Sharing__c> scope){ 
      
        
        for(Researcher_Member_Sharing__c rmsScope : scope){
            getRelatedRecordIds(rmsScope);
               
        }

        getRelatedShareRecords();
                        
        for(Researcher_Member_Sharing__c rmsScope : scope){
        
            if(rmsScope.Researcher_Portal_Project__c != null && !(projectUserIds.contains(String.valueof(rmsScope.Researcher_Portal_Project__c)+String.valueof(rmsScope.User__c)))){
                createProjectShare(rmsScope);   
            }else if(rmsScope.Publication__c != null){
                createPublicationShare(rmsScope);
            }else if(rmsScope.Ethics__c != null){
                createEthicShare(rmsScope);
            }else if(rmsScope.ResearchProjectContract__c  != null){
                createContractShare(rmsScope); 
            }    
        }
        
        insertAllShares();
        
    }
    
    public void finish(Database.BatchableContext bc) { 
        System.debug('Finish');
    }
    
    public void getRelatedRecordIds(Researcher_Member_Sharing__c rmsScope){
        if(rmsScope.Researcher_Portal_Project__c != null){
            projectIds.add(rmsScope.Researcher_Portal_Project__c);
        }else if(rmsScope.Publication__c != null){
            pubIds.add(rmsScope.Publication__c);
        }else if(rmsScope.Ethics__c != null){
            ethicsIds.add(rmsScope.Ethics__c);
        }else if(rmsScope.ResearchProjectContract__c  != null){
            contractIds.add(rmsScope.ResearchProjectContract__c);
        } 
    }
    
    public void getRelatedShareRecords(){
        
        for(ResearcherPortalProject__Share projectShare : [SELECT UserORGroupId, ParentId FROM ResearcherPortalProject__Share WHERE ParentID IN: projectIds]){
            projectUserIds.add(String.valueof(projectShare.ParentID)+String.valueof(projectShare.UserorGroupID));
        }
        
        for(Ethics__Share ethicsShare : [SELECT UserORGroupId, ParentId FROM Ethics__Share WHERE ParentID IN: ethicsIds]){
            ethicsUserIds.add(String.valueof(ethicsShare.ParentID)+String.valueof(ethicsShare.UserorGroupID));
        }
        
        for(Publication__Share pubShare : [SELECT UserORGroupId, ParentId FROM Publication__Share WHERE ParentID IN: pubIds]){
            pubUserIds.add(String.valueof(pubShare.ParentID)+String.valueof(pubShare.UserorGroupID));
        }
        
        for(ResearchProjectContract__Share contShare : [SELECT UserORGroupId, ParentId FROM ResearchProjectContract__Share WHERE ParentID IN: contractIds]){
            contUserIds.add(String.valueof(contShare.ParentID)+String.valueof(contShare.UserorGroupID));
        }
    
    }
    
    public void createProjectShare(Researcher_Member_Sharing__c rmsScope){
        ResearcherPortalProject__Share projShare = new ResearcherPortalProject__Share();
        projShare.UserORGroupID = rmsScope.User__c;
        projShare.ParentID = rmsScope.Researcher_Portal_Project__c;
        projShare.AccessLevel = rmsScope.AccessLevel__c;
        projShare.RowCause = 'Manual';
        projectShareUpdate.add(projShare);
    }
    
    public void createEthicShare(Researcher_Member_Sharing__c rmsScope){
        Ethics__Share ethicShare = new Ethics__Share();
        ethicShare.UserORGroupID = rmsScope.User__c;
        ethicShare.ParentID = rmsScope.Ethics__c;
        ethicShare.AccessLevel = 'Read';
        ethicShare.RowCause = 'Manual';
        ethicsShareUpdate.add(ethicShare);
    }
    
    public void createPublicationShare(Researcher_Member_Sharing__c rmsScope){
        Publication__Share pubShare = new Publication__Share();
        pubShare.UserORGroupID = rmsScope.User__c;
        pubShare.ParentID = rmsScope.Publication__c;
        pubShare.AccessLevel = 'Read';
        pubShare.RowCause = 'Manual';
        pubShareUpdate.add(pubShare);
    }
    
    public void createContractShare(Researcher_Member_Sharing__c rmsScope){
        ResearchProjectContract__Share contShare = new ResearchProjectContract__Share();
        contShare.UserORGroupID = rmsScope.User__c;
        contShare.ParentID = rmsScope.ResearchProjectContract__c;
        contShare.AccessLevel = 'Read';
        contShare.RowCause = 'Manual';
        contShareUpdate.add(contShare);
    }
    
    public void insertAllShares(){
    	
    Database.SaveResult[] shareResultsList = new List<Database.SaveResult>();//RPORW-1716

        if(projectShareUpdate.size() > 0){
           shareResultsList.addAll(Database.insert(projectShareUpdate, false));
        }
        
        if(pubShareUpdate.size() > 0){
        	shareResultsList.addAll(Database.insert(pubShareUpdate, false));
        }
        
        if(ethicsShareUpdate.size() > 0){
        	shareResultsList.addAll(Database.insert(ethicsShareUpdate, false));
        }
        
        if(contShareUpdate.size() > 0){
        	shareResultsList.addAll(Database.insert(contShareUpdate, false));
        }
        
        checkInsertResults(shareResultsList);
    }
    
    
    public static void checkInsertResults(Database.SaveResult[] srList){
    	
      // Iterate through each returned result
           for (Database.SaveResult sr : srList) {
		
		    if (sr.isSuccess()) {
		        // Operation was successful, so get the ID of the record that was processed
		        System.debug('Successfully inserted pub share ' + sr.getId());
		    }
		
		    else {
		        // Operation failed, so get all errors                
		        for(Database.Error err : sr.getErrors()) {                   
		
		            System.debug(err.getStatusCode() + ': ' + err.getMessage()); 
		         }
		      }
           }        
    	
    	
    	
    }
    
    
    
}