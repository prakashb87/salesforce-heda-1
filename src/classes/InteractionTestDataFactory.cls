/*****************************************
* File: INT_TestDataFactory
* Author: Sierra-Cedar
* Description: Test data factory for Interactions
******************************************/
public with sharing class InteractionTestDataFactory {
      /**
     * @description Creates and returns a List of Interaction_Mapping__c test data
     * @return testMappingsToInsert, the List of test data to insert in your unit test
     */
     
    public static List < Interaction_Mapping__c > createInteractionmappingsTriggerHandlers() {
        // Load the test hed__Trigger_Handler__c from the static resource Trigger_Handler_Data
        List < Interaction_Mapping__c > listOfInteractionMapping = Test.loadData(Interaction_Mapping__c.sObjectType, 'InteractionMapping');
        //return list of trigger handler
        return listOfInteractionMapping;
    }
   public static List<hed__Trigger_Handler__c> createTriggerHandlerData(){
   	  List<hed__Trigger_Handler__c> triggerHandlers= new List<hed__Trigger_Handler__c>();
        hed__Trigger_Handler__c tHdlr1= new hed__Trigger_Handler__c(hed__Active__c=false,
                                                                   hed__Class__c='ACCT_IndividualAccounts_TDTM',
                                                                  hed__Object__c='Contact',
                                                                  hed__Trigger_Action__c='AfterInsert;AfterUpdate;AfterDelete',
                                                                  hed__Load_Order__c=1);
        triggerHandlers.add(tHdlr1);
        hed__Trigger_Handler__c tHdlr2= new hed__Trigger_Handler__c(hed__Active__c=false,
                                                                   hed__Class__c='REL_Relationships_Con_TDTM',
                                                                  hed__Object__c='Contact',
                                                                  hed__Trigger_Action__c='AfterInsert;AfterUpdate;AfterDelete',
                                                                  hed__Load_Order__c=3);
        triggerHandlers.add(tHdlr2);
        hed__Trigger_Handler__c tHdlr3= new hed__Trigger_Handler__c(hed__Active__c=false,
                                                                   hed__Class__c='ADDR_Contact_TDTM',
                                                                  hed__Object__c='Contact',
                                                                  hed__Trigger_Action__c='BeforeInsert;BeforeUpdate;AfterInsert;AfterUpdate',
                                                                  hed__Load_Order__c=2);
        triggerHandlers.add(tHdlr3);
        hed__Trigger_Handler__c tHdlr4= new hed__Trigger_Handler__c(hed__Active__c=false,
                                                                   hed__Class__c='CON_DoNotContact_TDTM',
                                                                  hed__Object__c='Contact',
                                                                  hed__Trigger_Action__c='BeforeInsert;BeforeUpdate',
                                                                  hed__Load_Order__c=2);
        triggerHandlers.add(tHdlr4);
        hed__Trigger_Handler__c tHdlr5= new hed__Trigger_Handler__c(hed__Active__c=false,
                                                                   hed__Class__c='CON_Preferred_TDTM',
                                                                  hed__Object__c='Contact',
                                                                  hed__Trigger_Action__c='BeforeInsert;BeforeUpdate',
                                                                  hed__Load_Order__c=3);
        triggerHandlers.add(tHdlr5);
        
        hed__Trigger_Handler__c tHdlr6= new hed__Trigger_Handler__c(hed__Active__c=false,
                                                                   hed__Class__c='CON_PrimaryAffls_TDTM',
                                                                  hed__Object__c='Contact',
                                                                  hed__Trigger_Action__c='AfterInsert;AfterUpdate',
                                                                  hed__Load_Order__c=4);
       
		triggerHandlers.add(tHdlr6);
        
        hed__Trigger_Handler__c tHdlr7= new hed__Trigger_Handler__c(hed__Active__c=false,
                                                                   hed__Class__c='CON_CannotDelete_TDTM',
                                                                  hed__Object__c='Contact',
                                                                  hed__Trigger_Action__c='BeforeDelete',
                                                                  hed__Load_Order__c=1);
       
		triggerHandlers.add(tHdlr7);
        hed__Trigger_Handler__c tHdlr8= new hed__Trigger_Handler__c(hed__Active__c=false,
                                                                   hed__Class__c='CON_PrimaryLanguage_TDTM',
                                                                  hed__Object__c='Contact',
                                                                  hed__Trigger_Action__c='AfterInsert;AfterUpdate',
                                                                  hed__Load_Order__c=5);
       
		triggerHandlers.add(tHdlr8);
		return triggerHandlers;
   }
   
  
}