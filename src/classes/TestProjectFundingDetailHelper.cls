/*****************************************************
Purpose: Test class for ProjectFundingDetailHelper
History:
Created by Subhajit on 10/24/2018
******************************************************/
@isTest
public class TestProjectFundingDetailHelper {

    /************************************************************************************
    // Purpose      :  Creating Test data setup for RSTP for this test class
    // Developer    :  Subhajit
    // Created Date :  10/24/2018                 
    //***********************************************************************************/
    @testSetup static void testDataSetup() {
        
        RSTP_TestDataFactoryUtils.createNonCommunityUsersWithProfile(1,'System Administrator','Activator');
        
        User adminUser =[SELECT Id,ContactId FROM User WHERE UserName='test.nonCommunityUser1@test.com.testDev' LIMIT 1];
        System.runAs(adminUser)   
        {
            Map<String,Integer> allnumbersOfDataMap = new Map<String,Integer>();
            allnumbersOfDataMap.put('user',1);
            allnumbersOfDataMap.put('account',1);
            allnumbersOfDataMap.put('contact',1);
            
            Map<String,String> requiredInfoMap = new Map<String,String>();
            requiredInfoMap.put('userType','CommunityUsers');
            requiredInfoMap.put('profile',Label.testProfileName);
            
            Boolean userCreatedFlag = RSTP_TestDataFactoryUtils.testRSTPDataSetup(allnumbersOfDataMap,requiredInfoMap);
            System.assert(userCreatedFlag==true,'Community User Created'); 
        }
        
    }    
    
    /************************************************************************************
    // Purpose      :  Testing unit cases for Project funding Expense Utility
    // Developer    :  Subhajit
    // Created Date :  10/24/2018                 
    //***********************************************************************************/
    @isTest
    public static void testProjectFundingTableMethod()
    {
       User usr =[SELECT Id,ContactId FROM User WHERE UserName='test.communityUser1@test.com.testDev' LIMIT 1];
         System.runAs(usr)
          {
            ResearcherPortalProject__c researchProject = new ResearcherPortalProject__c();
            researchProject.Status__c='Idea';
            researchProject.Access__c='Private';
            researchProject.Approach_to_impact__c='test1';
            researchProject.Discipline_area__c='';
            researchProject.Start_Date__c= date.newInstance(2018, 10, 08) ;
            researchProject.End_Date__c=date.newInstance(2018, 12, 14);
            researchProject.Impact_Category__c='Social';
            researchProject.Title__c='projectTest1';
            //researchProject.RecordTypeId = Schema.SObjectType.ResearcherPortalProject__c.getRecordTypeInfosByName().get(Label.RSTP_ResearchProjectRMRecordtype).getRecordTypeId(); 
            insert researchProject;
            system.assert(researchProject!=null);
            List<Contact> cont =  [SELECT Id FROM Contact LIMIT 1];
            List<ProjectSAPFunding__c> projSapFundingList =  new List<ProjectSAPFunding__c>();
            for(integer i=1; i<=2; i++)
            {
                ProjectSAPFunding__c eachFundingRec = new ProjectSAPFunding__c(); 
                eachFundingRec.CostElementDescription__c='test Description'+i;
                eachFundingRec.ValueTypeCode__c  =10;
                eachFundingRec.ValueTypeDetailCode__c=1;
                eachFundingRec.DrCrCode__c='S';
                eachFundingRec.CostExpenseLevel1__c='RMIT_ALL.RPT';
                eachFundingRec.CostExpenseLevel2__c='RMIT_ALL05.RPT';
                //eachFundingRec.Expenditure__c=0.0;
                eachFundingRec.CostExpenseLevel3__c='RMIT_ALL01.RPT'; 
                eachFundingRec.CostExpenseLevel4__c='RMIT_ALL21.RPT';
                eachFundingRec.Amount__c= 3000 + i;
                eachFundingRec.ProjectCode__c=researchProject.Id;
                eachFundingRec.CELevel8Description__c='Casuals Academic';
                //eachFundingRec.Commitment__c=0.0;
                eachFundingRec.CostElementCode__c='511300';
                eachFundingRec.PCLevel5Description__c='School of Science';
                eachFundingRec.ProjectSystemStatusDescription__c='SETC'; 
                eachFundingRec.ResearchType__c='N';
                eachFundingRec.WBSCode__c='RE-00203-011';
                eachFundingRec.WBSDescription__c='Opex - Development of enzyme technology';
                eachFundingRec.WBSHierarchialCode__c='RE-00203-011';
                eachFundingRec.WBSLevelHierarchy__c=3.0;
                eachFundingRec.WBSSequenceNumber__c='PR00015556';
                eachFundingRec.LeadChiefInvestigatorCode__c= cont[0].Id;
                eachFundingRec.Lead_Chief_Investigator_ENumber__c='';
                eachFundingRec.WBSPersonResponsibleCode__c=cont[0].Id;  
                eachFundingRec.WBSPersonResponsibleENumber__c='1502';
                eachFundingRec.FiscalPeriod__c='';
                eachFundingRec.ProjectCodeSAPWBSNum__c='';
                
                projSapFundingList.add(eachFundingRec);
            }        
            insert projSapFundingList;
            system.assert(projSapFundingList!=null);
            Integer year = Date.Today().Year();
            //Positive test case  
           // ProjectFundingDetailHelper.getProjectFundingTable(researchProject.Id,String.valueof(system.today()));
             ProjectFundingDetailHelper.getProjectFundingTable(researchProject.Id,String.valueof(system.today().year()));
        }
        
       
    }
    
    /************************************************************************************
    // Purpose      :  Testing unit cases for Project funding Budget Utility
    // Developer    :  Subhajit
    // Created Date :  10/24/2018                 
    //***********************************************************************************/
    @isTest
    public static void testProjectFundingBudgetDetails()
    {
       User usr =[SELECT Id,ContactId FROM User WHERE UserName='test.communityUser1@test.com.testDev' LIMIT 1];
         System.runAs(usr)
          {
            ResearcherPortalProject__c researchProject = new ResearcherPortalProject__c();
            researchProject.Status__c='Idea';
            researchProject.Access__c='Private';
            researchProject.Approach_to_impact__c='test1';
            researchProject.Discipline_area__c='';
            researchProject.Start_Date__c= date.newInstance(2018, 10, 08) ;
            researchProject.End_Date__c=date.newInstance(2018, 12, 14);
            researchProject.Impact_Category__c='Social';
            researchProject.Title__c='projectTest1';
            //researchProject.RecordTypeId = Schema.SObjectType.ResearcherPortalProject__c.getRecordTypeInfosByName().get(Label.RSTP_ResearchProjectRMRecordtype).getRecordTypeId(); 
            insert researchProject;
            system.assert(researchProject!=null);
              
            List<Research_Project_Funding__c> projSapFundingList =  new List<Research_Project_Funding__c>();
            for(integer i=1; i<=2; i++)
            {
                Research_Project_Funding__c eachFundingRec = new Research_Project_Funding__c(); 
                eachFundingRec.Amount_Applied__c=100+i;
                eachFundingRec.Amount_Received__c=1000+i;
                eachFundingRec.Budget_Year__c='1985'; 
                eachFundingRec.Scheme_Description__c='Test Scheme'+i; 
                eachFundingRec.Project_Scheme__c=i; 
                eachFundingRec.ResearcherPortalProject__c=researchProject.Id;
                //insert eachFundingRec ;
                projSapFundingList.add(eachFundingRec);
           
            }        
            insert projSapFundingList;
            system.assert(projSapFundingList!=null);
            
            //Positive test case  
            ProjectFundingDetailHelper.getProjectFundingBudgetDetails(researchProject.Id);
         
        }
        
       
    }
  /************************************************************************************
// Purpose      :  Testing unit cases of Negative for Project funding Expense Utility
// Developer    :  Subhajit
// Created Date :  12/05/2018                 
//***********************************************************************************/
@isTest
public static void testProjectFundingNegativeMethod()
{
   User usr =[SELECT Id,ContactId FROM User WHERE UserName='test.communityUser1@test.com.testDev' LIMIT 1];
     System.runAs(usr)
      {
        ResearcherPortalProject__c researchProject = new ResearcherPortalProject__c();
        researchProject.Status__c='Idea';
        researchProject.Access__c='Private';
        researchProject.Approach_to_impact__c='test1';
        researchProject.Discipline_area__c='';
        researchProject.Start_Date__c= date.newInstance(2018, 10, 08) ;
        researchProject.End_Date__c=date.newInstance(2018, 12, 14);
        researchProject.Impact_Category__c='Social';
        researchProject.Title__c='projectTest1';
        //researchProject.RecordTypeId = Schema.SObjectType.ResearcherPortalProject__c.getRecordTypeInfosByName().get(Label.RSTP_ResearchProjectRMRecordtype).getRecordTypeId(); 
        insert researchProject;
        system.assert(researchProject!=null);
        List<Contact> cont =  [SELECT Id FROM Contact LIMIT 1];
        List<ProjectSAPFunding__c> projSapFundingList =  new List<ProjectSAPFunding__c>();
        for(integer i=1; i<=2; i++)
        {
            ProjectSAPFunding__c eachFundingRec = new ProjectSAPFunding__c(); 
            eachFundingRec.CostElementDescription__c='test Description'+i;
            eachFundingRec.ValueTypeCode__c  =10;
            eachFundingRec.ValueTypeDetailCode__c=1;
            eachFundingRec.DrCrCode__c='S';
            eachFundingRec.CostExpenseLevel1__c='RMIT_ALL.RPT';
            eachFundingRec.CostExpenseLevel2__c='RMIT_ALL05.RPT';
            //eachFundingRec.Expenditure__c=0.0;
            eachFundingRec.CostExpenseLevel3__c='RMIT_ALL01.RPT'; 
            eachFundingRec.CostExpenseLevel4__c='RMIT_ALL21.RPT';
            eachFundingRec.Amount__c= 3000 + i;
            eachFundingRec.ProjectCode__c=researchProject.Id;
            eachFundingRec.CELevel8Description__c='Casuals Academic';
            //eachFundingRec.Commitment__c=0.0;
            eachFundingRec.CostElementCode__c='511300';
            eachFundingRec.PCLevel5Description__c='School of Science';
            eachFundingRec.ProjectSystemStatusDescription__c='SETC'; 
            eachFundingRec.ResearchType__c='N';
            eachFundingRec.WBSCode__c='RE-00203-011';
            eachFundingRec.WBSDescription__c='Opex - Development of enzyme technology';
            eachFundingRec.WBSHierarchialCode__c='RE-00203-011';
            eachFundingRec.WBSLevelHierarchy__c=3.0;
            eachFundingRec.WBSSequenceNumber__c='PR00015556';
            eachFundingRec.LeadChiefInvestigatorCode__c= cont[0].Id;
            eachFundingRec.Lead_Chief_Investigator_ENumber__c='';
            eachFundingRec.WBSPersonResponsibleCode__c=cont[0].Id;  
            eachFundingRec.WBSPersonResponsibleENumber__c='1502';
            eachFundingRec.FiscalPeriod__c='';
            eachFundingRec.ProjectCodeSAPWBSNum__c='';
            
            projSapFundingList.add(eachFundingRec);
        }        
        insert projSapFundingList;
        system.assert(projSapFundingList!=null);
        Integer year = Date.Today().Year();
      
         ProjectFundingDetailHelper.getProjectFundingBudgetDetails(researchProject.Id);
         ProjectFundingDetailHelper.getProjectFundingTable(researchProject.Id,String.valueof(system.today().year()));
    }

  }
}