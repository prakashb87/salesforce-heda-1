public without sharing class MockFailedSingleSAMSEnrollReq implements ISAMSEnrollmentRequest {

    public void sendEnrollDetailToSAMS(List<CourseConnectionWrapper> samsEnrollmentRequestBody) {
        System.debug('Mock request assumed to have failed!');
        throw new SAMSRequestCalloutException('SAMS Response not successful. Something went wrong!');
    }
    
    public Map<Id, JsonCourseWrapper.IpaasResponseWrapper> getSAMSEnrollResByCourseConnId(){
 
        // Assume test data is already setup
        List<hed__Course_Enrollment__c> courseConnections = [SELECT Id FROM hed__Course_Enrollment__c LIMIT 1];
        Map<Id, JsonCourseWrapper.IpaasResponseWrapper> result = new Map<Id, JsonCourseWrapper.IpaasResponseWrapper>();     
        JsonCourseWrapper.IpaasResponseWrapper successResponse = new JsonCourseWrapper.IpaasResponseWrapper();
        successResponse.courseConnectionId = courseConnections[0].Id;
        successResponse.result = 'Error';
        successResponse.errorCode = '108';
        successResponse.errorText = 'Badge exists';
        result.put(courseConnections[0].Id,successResponse);
         

        return result;
    }
    
}