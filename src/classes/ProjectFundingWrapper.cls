/*************************************************************************************
Purpose: Wrapper class for Viewing Project Funding Detail- expenses view
History:
Created by Ankit Bhagat	 on 10/10/2018 for RPORW-135 
Modified by Subhajit on on 10/10/2018 for RPORW-282
*************************************************************************************/
@SuppressWarnings('PMD.ExcessivePublicCount')
public class ProjectFundingWrapper {

	/*****************************************************************************************
	// JIRA No      :  RPORW-135
	// SPRINT       :  SPRINT-4
	// Purpose      :  Viewing Project Funding Detail- expenses view
	// Developer    :  Ankit Bhagat	
	// Created Date :  10/10/2018                 
	//************************************************************************************/ 
    public class ProjectFundingExpensesTable{
       
        @AuraEnabled public list<ProjectFundingExpensesDetails> projectFundingList {get;set;}
        @AuraEnabled public decimal totalIncome {get;set;}
        @AuraEnabled public decimal totalSalariesOncosts {get;set;}
        @AuraEnabled public decimal salariesOncosts {get;set;}
        @AuraEnabled public decimal totalOtherOperatingExpenses {get;set;}
        @AuraEnabled public decimal totalcapital {get;set;}
        @AuraEnabled public decimal totalFundingExpense {get;set;}
        @AuraEnabled public decimal remainingBalance {get;set;}
		@AuraEnabled public datetime lastRefreshedDate {get;set;}
        public ProjectFundingExpensesTable(){
			
            totalIncome = 0.0;
            totalSalariesOncosts = 0.0;
            salariesOncosts  = 0.0; 
            totalOtherOperatingExpenses =0.0;
            totalcapital = 0.0;
            totalFundingExpense  = 0.0; 
            remainingBalance = 0.0;
        }
        
    }
	
	/*****************************************************************************************
	// JIRA No      :  RPORW-135
	// SPRINT       :  SPRINT-4
	// Purpose      :  Viewing Project Funding Detail- expenses view
	// Developer    :  Ankit Bhagat	
	// Created Date :  10/10/2018                 
	//************************************************************************************/ 
	public class ProjectFundingExpensesDetails{
       
        @AuraEnabled public string costElememtDescription {get;set;}
        @AuraEnabled public decimal income {get;set;}
        @AuraEnabled public decimal salariesOncosts {get;set;}
        @AuraEnabled public decimal otherOperatingExpenses {get;set;}
        @AuraEnabled public decimal capital {get;set;}
        @AuraEnabled public decimal totalExpense {get;set;}
        @AuraEnabled public decimal balance {get;set;}
				
		public ProjectFundingExpensesDetails(){
            
            costElememtDescription = '';
            income = 0.0;
            salariesOncosts  = 0.0; 
            otherOperatingExpenses = 0.0;
            capital  = 0.0; 
            totalExpense = 0.0;
            balance = 0.0;
        }
		
     	public ProjectFundingExpensesDetails(ProjectSAPFunding__c projectFunding){
            
            costElememtDescription = projectFunding.CostElementDescription__c;
            income = projectFunding.Income__c;
            salariesOncosts  = projectFunding.Salaries_and_Oncosts__c; 
            otherOperatingExpenses = projectFunding.Other_Operating_Expenses__c;
            capital  = projectFunding.Capital__c; 
            totalExpense = projectFunding.Total_Expense__c;
            balance = projectFunding.Amount__c;
        }
        
        public ProjectFundingExpensesDetails(AggregateResult aggFundingDetail){
           
            costElememtDescription = string.valueof(aggFundingDetail.get('CostElementDescription__c'));
            income =decimal.valueOf(aggFundingDetail.get('Income__c')+'');
            salariesOncosts  = decimal.valueOf(aggFundingDetail.get('Salaries_and_Oncosts__c')+''); 
            otherOperatingExpenses = decimal.valueOf(aggFundingDetail.get('Other_Operating_Expenses__c')+'');
            capital  = decimal.valueOf(aggFundingDetail.get('Capital__c')+''); 
            totalExpense = decimal.valueOf(aggFundingDetail.get('Total_Expense__c')+'');
            if(income!=null && income<>0){
            	balance = income;
            }else {
            	balance = decimal.valueOf(aggFundingDetail.get('Amount__c')+'');
            }
        }

	}
    
    /*****************************************************************************************
	// JIRA No      :  RPORW-282
	// SPRINT       :  SPRINT-4
	// Purpose      :  Viewing Project Funding Detail- Budget view
	// Developer    :  Subhajit	
	// Created Date :  10/10/2018                 
	//************************************************************************************/ 
    public class ProjectFundingBudgetDetails{ 
        @AuraEnabled public list<projectFundingBudgetList> projectFundingBudgetList {get;set;}        
        @AuraEnabled public string fundingSchemeDescription {get;set;}
        @AuraEnabled public integer totalFundingAppliedBudget {get;set;}
        @AuraEnabled public integer totalFundingReceivedBudget {get;set;}
        @AuraEnabled public string projectId {get;set;}
        
        public ProjectFundingBudgetDetails(){
            totalFundingAppliedBudget =0;
            totalFundingReceivedBudget =0;           
            fundingSchemeDescription='';
            projectId='';
        }
        
       
    }
    
    /*****************************************************************************************
	// JIRA No      :  RPORW-282
	// SPRINT       :  SPRINT-4
	// Purpose      :  Viewing Project Funding Detail- Budget view
	// Developer    :  Subhajit	
	// Created Date :  10/10/2018                 
	//************************************************************************************/ 
    public class projectFundingBudgetList{
        @AuraEnabled public string year {get;set;}
        @AuraEnabled public integer fundingAppliedBudget {get;set;}
        @AuraEnabled public integer fundingReceivedBudget {get;set;}
        public projectFundingBudgetList(){
            fundingAppliedBudget = 0;
            fundingReceivedBudget  =0;
            year = ''; 
        }
        public projectFundingBudgetList(Research_Project_Funding__c projectBudgetFunding){
            fundingAppliedBudget = integer.valueOf(projectBudgetFunding.Amount_Applied__c!=null ? projectBudgetFunding.Amount_Applied__c:0);
            fundingReceivedBudget  = integer.valueOf(projectBudgetFunding.Amount_Received__c!=null ? projectBudgetFunding.Amount_Received__c:0);
            year = projectBudgetFunding.Budget_Year__c;
        }
        
    }
}