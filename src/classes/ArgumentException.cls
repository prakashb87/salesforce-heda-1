/**
 * @description Exception for any problems with arguments
 */
public class ArgumentException extends Exception {}