/*****************************************************************************************************
 *** @Class             : CredlyCourseConnectionIntControllerTest
 *** @Author            : Avinash Machineni
 *** @Purpose 	        : Test class for a Controller to retry quick action lightning component
 *** @Created date      : 18/05/2018
 *** @JIRA ID           : ECB-2466
 *****************************************************************************************************/

@isTest
public class CredlyCourseConnectionIntControllerTest {
    @isTest
    public static void updateCourseConnlyfcycle(){

        TestDataFactoryUtilRefOne.testCreateDataforCredlySuccess();
        
        hed__Course_Enrollment__c crsenrlmnt = [ Select id from hed__Course_Enrollment__c];
        
        Test.startTest();
        CredlyCourseConnectionIntController.updateCourseConn(crsenrlmnt.id);
        System.assert(crsenrlmnt != null);
        Test.stopTest();
    }
}