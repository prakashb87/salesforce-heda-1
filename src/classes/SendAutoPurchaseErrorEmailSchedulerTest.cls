/*
  *****************************************************************************************************************
  * @author       :Resmi Ramakrishnan
  * @date         : 16/06/2018 
  * @description  : Test Class for sending Embedded Credential AutoPurchase Error records to 21CC Support team
  ******************************************************************************************************************
*/

@isTest
public class SendAutoPurchaseErrorEmailSchedulerTest  
{
	static testMethod void testAutoPurchaseEmailSendPositiveScenario()
    {
         RMErrLog__c rmErrorLogTestObj = TestDataFactoryUtil.createAutoPurchaseErrorRecords();
         insert rmErrorLogTestObj;
        
         Config_Data_Map__c csDataMapEmailObj  = TestDataFactoryUtil.createCongigDataMapRecordsFromNameValue('AutoPurchaseSupportEmail', 'resmi.ramakrishnan@rmit.edu.au');
         insert csDataMapEmailObj;
         Config_Data_Map__c csDataMapSubjectObj  = TestDataFactoryUtil.createCongigDataMapRecordsFromNameValue('AutoPurchaseErrorEmailSubject', 'Embedded Credential Auto Purchase Error Report');
         insert csDataMapSubjectObj;
         Config_Data_Map__c csDataMapFileNameObj  = TestDataFactoryUtil.createCongigDataMapRecordsFromNameValue('AutoPurchaseErrorFileName', 'Embedded Credential Error Report.csv');
         insert csDataMapFileNameObj;
        
        
        
         //Test.startTest();
         //SendAutoPurchaseErrorEmailScheduler  sch1 = new SendAutoPurchaseErrorEmailScheduler ();
         //Datetime futureTime1 = Datetime.now().addMinutes(1);
         //String futureTimeCronString1 = '0 ' + futureTime1.minute()  + ' '+ futureTime1.hour()+ ' '  + futureTime1.day() + ' ' + futureTime1.month() + ' ? '+ futureTime1.year();
         //String jobId = system.schedule('RESMI-TESTING-STATEMENT -Batch1',futureTimeCronString1,sch1);  
         //CronTrigger ct = [SELECT Id, CronExpression, TimesTriggered, NextFireTime FROM CronTrigger WHERE id = :jobId];
         //System.assertEquals(futureTimeCronString1, ct.CronExpression);
         //System.assertEquals(0, ct.TimesTriggered);
         //Test.stopTest();  
        
    }
}