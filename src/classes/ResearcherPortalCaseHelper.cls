/*******************************************
Purpose: Controller class of Feedback and Support Case Pages. Functionalities: Case Creation, assignment to queue, sending email, providing list of Topics and Sub Topics
History:
Created by Subhajit on 11/10/18
*******************************************/
public without sharing class ResearcherPortalCaseHelper {

    /*****************************************************************************************
    // Global Declaration Block
    *****************************************************************************************/ 
    public static final string NONE_VALUE ='--None--';
    public static final string KEY1 ='queueId';
    public static final string KEY2 ='routedEmailAddress';
    
    /*****************************************************************************************
    // JIRA NO      :  470/314/469
    // SPRINT       :  7
    // Purpose      :  Preparing topic to Subtopic list in a map
    // Parameters   :  ResearcherCaseDetailsWrapper
    // Developer    :  Subhajit
    // Created Date :  11/10/2018                 
    //****************************************************************************************/
    public static Map<String, list<String>> getAllRecsFromTopicSubtopicQueueMapping()
    {
            Map<String, list<String>> topicSubtopicQueueMapping = new Map<String, list<String>>();
            List<TopicSubtopicQueueMapping__mdt> allTopicSubtopicQueueMappings = [SELECT id, Topic__c, Subtopic__c FROM TopicSubtopicQueueMapping__mdt WHERE Case_Record_Type__c =: label.RMITSupportRequestCase];
            for (TopicSubtopicQueueMapping__mdt currMapping:allTopicSubtopicQueueMappings)
            {
                if(topicSubtopicQueueMapping.containsKey(currMapping.Topic__c)) {
                    List<String> subtopicList = topicSubtopicQueueMapping.get(currMapping.Topic__c);
                    subtopicList.add(currMapping.Subtopic__c);
                    topicSubtopicQueueMapping.put(currMapping.Topic__c, subtopicList);
                } else {
                    
                    topicSubtopicQueueMapping.put(currMapping.Topic__c, new list<String> {currMapping.Subtopic__c});
                }
            }
        
           return topicSubtopicQueueMapping;
    }
    
    /*****************************************************************************************
    // JIRA NO      :  470/314/469
    // SPRINT       :  7
    // Purpose      :  Preparing generic picklist values
    // Parameters   :  String,String
    // Developer    :  Subhajit
    // Created Date :  11/10/2018                 
    //****************************************************************************************/
    public static List<String> getPicklistValues(String objectApiName,String fieldName){ 
        
        List<String> lstPickvals=new List<String>();
        schema.SObjectType targetType = Schema.getGlobalDescribe().get(objectApiName);//From the Object Api name retrieving the SObject
        sobject objectName = targetType.newSObject();
        Schema.sObjectType sobjectType = objectName.getSObjectType(); //grab the sobject that was passed
        Schema.DescribeSObjectResult sobjectDescribe = sobjectType.getDescribe(); //describe the sobject
        Map<String, Schema.SObjectField> fieldMap = sobjectDescribe.fields.getMap(); //get a map of fields for the passed sobject
        List<Schema.PicklistEntry> pickListValues = fieldMap.get(fieldName).getDescribe().getPickListValues(); //grab the list of picklist values for the passed field on the sobject
        for (Schema.PicklistEntry a : pickListValues) { //for all values in the picklist list
            lstPickvals.add(a.getValue());//add the value  to our final list
        }
        
        return lstPickvals;
    }
    
    /*****************************************************************************************
    // JIRA NO      :  470/314/469
    // SPRINT       :  7
    // Purpose      :  Create portalfeedback and research support case
    // Parameters   :  ResearcherCaseDetailsWrapper
    // Developer    :  Subhajit
    // Created Date :  11/10/2018                 
    //****************************************************************************************/
    public static Id createNewCaseRecord(ResearcherPortalCaseWrapper caseWrapper){    
        
        System.debug('@@@caseWrapper'+caseWrapper);
        // Declaration and initialisation Block  
        String recordtype='';
        Case theCase; 
        
        //get current user contact
        Id conId =[SELECT contactId FROM User WHERE Id =:userInfo.getUserId() LIMIT 1].contactId;
        
        // Recordtype selection as per records from frontEnd and Set in record   
        /*if(!String.isEmpty(caseWrapper.recordType) && caseWrapper.recordType.equalsIgnoreCase(label.RMITSupportRequestCase))
        {
            //Fetch queue name and routed email from TopicSubtopicQueueMapping__mdt by filtering topic and subtopic from frontend
            Map<String,String> getQueueAndEmailMap = ApexWithoutSharingUtils.getQueueIdAndEmailAddress(caseWrapper.researcherCaseTopic,caseWrapper.researcherCaseSubTopic,label.RMITSupportRequestCase);
            recordtype =Schema.SObjectType.Case.getRecordTypeInfosByName().get(label.RMITSupportRequestCase).getRecordTypeId();
            theCase = new Case(
                subject=caseWrapper.subject,
                description=caseWrapper.description,
                status=caseWrapper.status,
                researcherCaseTopic__c= caseWrapper.researcherCaseTopic ,
                researcherCaseSubTopic__c= caseWrapper.researcherCaseSubTopic,
                researcherCaseRoutedToEmail__c= getQueueAndEmailMap.get(KEY2),

                contactId=ResearcherUserContactMapHelper.getContactId(userinfo.getUserId()),
                recordTypeId= recordtype,
                OwnerId=  getQueueAndEmailMap.get(KEY1)  
            );  
        }
        else */
        if(!String.isEmpty(caseWrapper.recordType) && caseWrapper.recordType.equalsIgnoreCase(label.ResearcherPortalFeedback))
        {
            //Fetch queue name and routed email from TopicSubtopicQueueMapping__mdt for feedback Case
            Map<String,String> getQueueAndEmailMap = ApexWithoutSharingUtils.getQueueIdAndEmailAddress(NONE_VALUE,NONE_VALUE,label.ResearcherPortalFeedback);
            recordtype =Schema.SObjectType.Case.getRecordTypeInfosByName().get(label.ResearcherPortalFeedback).getRecordTypeId();
            theCase = new Case(
                subject=caseWrapper.subject,
                description=caseWrapper.description,
                status=caseWrapper.status,
                researcherCaseRoutedToEmail__c=getQueueAndEmailMap.get(KEY2),
                contactId=ResearcherUserContactMapHelper.getContactId(userinfo.getUserId()),
                portalFeedback__c=caseWrapper.portalFeedback,  
                recordTypeId= recordtype,
                OwnerId=  getQueueAndEmailMap.get(KEY1),
                Satisfaction_Rating__c = caseWrapper.satisfactionRating,
                Origin = 'Webform'
            );   
        }
        
    
        System.debug('@@@theCase'+theCase);
        // Insert the new case records from front end.  
        if(theCase != null)
        {
            if (!Schema.sObjectType.Case.isCreateable()){
				System.debug('Error: Insufficient Access');
            //return null;
            }
            insert theCase;
            System.debug('@@@theCase inserted : '+theCase);
            
           // ResearcherSupportCaseEmailTemplateHelper.sendResearchSupportEmailNotification(theCase.Id);            
            //RPORW-912 : Insert Attachments for Feedback Case type
            if(!String.isEmpty(caseWrapper.recordType) && caseWrapper.recordType.equalsIgnoreCase(label.ResearcherPortalFeedback)){
            	createCaseAttachment(caseWrapper.caseAttachments,theCase);
            }
            
        } 
        System.debug('@@@theCase inserted : '+theCase.Id);
           
        return theCase.Id;  
    }
  

     /*@Description : Helper class to upload Attachments for the Case created. 
      @Story : RPORW-912
      @Author : Gourav Bhardwaj
    */
    public static void createCaseAttachment(List<ResearcherPortalCaseWrapper.CaseAttachmentWrapper> caseAttachments,Case newCaseRecord){
        system.debug('### caseAttachments : '+caseAttachments);
        List<ContentVersion> contentVersions = new List<ContentVersion>();
        Map<String,String> contentVersionToDocumentMap = new Map<String,String>();
        List<ContentDocumentLink> contentDocuments = new List<ContentDocumentLink>();
            
        if(caseAttachments != null && !caseAttachments.isEmpty()){
            for(ResearcherPortalCaseWrapper.CaseAttachmentWrapper caseAttachment : caseAttachments){
                //caseAttachment.fileContent = EncodingUtil.urlDecode(caseAttachment.fileContent, 'UTF-8');
                ContentVersion conVer = new ContentVersion();
                conVer.ContentLocation = 'S'; // S specify this document is in SF, use E for external files
                conVer.PathOnClient = caseAttachment.fileName; // The files name, extension is very important here which will help the file in preview.
                conVer.Title = caseAttachment.fileName; // Display name of the files
                conVer.VersionData = EncodingUtil.base64Decode(caseAttachment.fileContent); // converting your binary string to Blob
                contentVersions.add(conVer);
                
                
            }
            
            System.debug('@@@IS CREATABLE VERSION : '+Schema.sObjectType.ContentVersion.isCreateable());
            
            if(!contentVersions.isEmpty()&& Schema.sObjectType.ContentVersion.isCreateable()){
                insert contentVersions;
                
                System.debug('@@@contentVersions : '+contentVersions);
            }
            
            
            for(ContentVersion contentVer : [SELECT ContentDocumentId,Id FROM ContentVersion WHERE Id =:contentVersions]){
                contentVersionToDocumentMap.put(contentVer.Id,contentVer.ContentDocumentId);
            }
            
            System.debug('@@@contentVersionToDocumentMap : '+contentVersionToDocumentMap);
            
            for(String contentVersionId : contentVersionToDocumentMap.keySet()){
                ContentDocumentLink cDe = new ContentDocumentLink();
                cDe.ContentDocumentId = contentVersionToDocumentMap.get(contentVersionId);
                cDe.LinkedEntityId = newCaseRecord.Id; // you can use objectId,GroupId etc
                cDe.ShareType = 'V'; // Inferred permission, checkout description of ContentDocumentLink object for more details
                cDe.Visibility = 'AllUsers';
                
                contentDocuments.add(cDe);
                
            }
            
            System.debug('@@@IS DOCUMENT LINK : '+Schema.sObjectType.ContentDocumentLink.isCreateable());
            
            if(!contentDocuments.isEmpty() && Schema.sObjectType.ContentDocumentLink.isCreateable() ){
                insert contentDocuments;    
            }
            
            System.debug('@@@contentDocuments : '+contentDocuments);
        }
        
   }
   

}