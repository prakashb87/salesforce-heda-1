/*******************************************
Purpose: Test class for ProjectFundingDetailController
History:
Created by Ankit Bhagat on 22/10/2018
Modified By Subhajit on 27/10/2018 
*******************************************/
@isTest
public class TestProjectFundingDetail {
    
    /************************************************************************************
    // Purpose      :  Creating Test data setup for RSTP for this test class
    // Developer    :  Subhajit
    // Created Date :  10/27/2018                 
    //***********************************************************************************/
    @testSetup static void testDataSetup() {
        
        RSTP_TestDataFactoryUtils.createNonCommunityUsersWithProfile(1,'System Administrator','Activator');
        
        User adminUser =[SELECT Id,ContactId FROM User WHERE UserName='test.nonCommunityUser1@test.com.testDev' LIMIT 1];
        System.runAs(adminUser)   
        {
            Map<String,Integer> allnumbersOfDataMap = new Map<String,Integer>();
            allnumbersOfDataMap.put('user',1);
            allnumbersOfDataMap.put('account',1);
            allnumbersOfDataMap.put('contact',1);
            
            Map<String,String> requiredInfoMap = new Map<String,String>();
            requiredInfoMap.put('userType','CommunityUsers');
            requiredInfoMap.put('profile',Label.testProfileName);
            
            Boolean userCreatedFlag = RSTP_TestDataFactoryUtils.testRSTPDataSetup(allnumbersOfDataMap,requiredInfoMap);
            System.assert(userCreatedFlag==true,'Community User Created'); 
        }
        
    }    
    /**************************************************************************************************
    // Purpose      :  Testing Class All positive Use Cases for ProjectFundingDetailController Methods
    // Developer    :  Subhajit
    // Created Date :  10/27/2018                 
    //*************************************************************************************************/
    @isTest 
    public static void testPositiveUseCaseForProjectFundingDetailControllerMethods(){
        
        User usr =[SELECT Id,ContactId FROM User WHERE UserName='test.communityUser1@test.com.testDev' LIMIT 1];   
       
        system.runAs(usr)
        {
            ResearcherPortalProject__c researchProject = new ResearcherPortalProject__c();
            researchProject.Status__c='Idea';
            researchProject.Access__c='Private';
            researchProject.Approach_to_impact__c='test1';
            researchProject.Discipline_area__c='';
            researchProject.Start_Date__c= date.newInstance(2018, 10, 08) ;
            researchProject.End_Date__c=date.newInstance(2018, 12, 14);
            researchProject.Impact_Category__c='Social';
            researchProject.Title__c='projectTest1';
            researchProject.RecordTypeId = Schema.SObjectType.ResearcherPortalProject__c.getRecordTypeInfosByName().get(Label.RSTP_ResearchProjectRMRecordtype).getRecordTypeId(); 
            insert researchProject;
            system.assert(researchProject!=null);
            
            Researcher_Member_Sharing__c memberSharing = new Researcher_Member_Sharing__c();
            memberSharing.MemberRole__c = 'CI';
            memberSharing.Contact__c = usr.ContactId;
            memberSharing.Researcher_Portal_Project__c = researchProject.Id;
            memberSharing.Primary_Contact_Flag__c = true  ;
            memberSharing.RecordTypeId = Schema.SObjectType.Researcher_Member_Sharing__c.getRecordTypeInfosByName().get(Label.RSTP_ResearchProjectRecordtype).getRecordTypeId();
            //insert member_sharing;
            system.assert(memberSharing!=null);
            
           ProjectSAPFunding__c projectSAP = new ProjectSAPFunding__c(); 
           projectSAP.CostElementDescription__c='test Description';
           projectSAP.ValueTypeCode__c  =10;
           projectSAP.ValueTypeDetailCode__c=1;
           projectSAP.DrCrCode__c='S';
           projectSAP.CostExpenseLevel1__c='RMIT_ALL.RPT';
           projectSAP.CostExpenseLevel2__c='RMIT_ALL05.RPT';
           projectSAP.CostExpenseLevel3__c='RMIT_ALL01.RPT'; 
           projectSAP.CostExpenseLevel4__c='RMIT_ALL21.RPT';
           projectSAP.Amount__c= 3000;
           projectSAP.ProjectCode__c=researchProject.Id;
           projectSAP.CELevel8Description__c='Casuals Academic';
           projectSAP.CostElementCode__c='511300';
           projectSAP.PCLevel5Description__c='School of Science';
           projectSAP.ProjectSystemStatusDescription__c='SETC'; 
           projectSAP.ResearchType__c='N';
           projectSAP.WBSCode__c='RE-00203-011';
           projectSAP.WBSDescription__c='Opex - Development of enzyme technology';
           projectSAP.WBSHierarchialCode__c='RE-00203-011';
           projectSAP.WBSLevelHierarchy__c=3.0;
           projectSAP.WBSSequenceNumber__c='PR00015556';
           projectSAP.LeadChiefInvestigatorCode__c=[SELECT Id FROM Contact LIMIT 1].Id;
           projectSAP.Lead_Chief_Investigator_ENumber__c='';
           projectSAP.WBSPersonResponsibleCode__c=[SELECT Id FROM Contact LIMIT 1].Id;  
           projectSAP.WBSPersonResponsibleENumber__c='1502';
           projectSAP.FiscalPeriod__c='';
           projectSAP.ProjectCodeSAPWBSNum__c='';
           insert projectSAP;
           system.assert(projectSAP!=null);
            
            Research_Project_Funding__c projectFunding = new Research_Project_Funding__c();
            projectFunding.Amount_Applied__c = 4512.25;
            projectFunding.Amount_Received__c = 4754.85;
            projectFunding.Budget_Year__c='1993';
            projectFunding.Funding_Category_Description__c='None-HERDC research income';
            projectFunding.ResearcherPortalProject__c = researchProject.Id;
            projectFunding.Scheme_Description__c='testschema';
            insert projectFunding;
            system.assert(projectFunding!=null);
            
            Test.StartTest();
            
            ProjectFundingWrapper.ProjectFundingExpensesDetails projectExpense = new ProjectFundingWrapper.ProjectFundingExpensesDetails();
            ProjectFundingWrapper.ProjectFundingExpensesDetails projectExpense1 = new ProjectFundingWrapper.ProjectFundingExpensesDetails(projectSAP);
            ProjectFundingWrapper.ProjectFundingBudgetDetails projectBudget = new ProjectFundingWrapper.ProjectFundingBudgetDetails();
            ProjectFundingWrapper.ProjectFundingExpensesTable projectTable = new ProjectFundingWrapper.ProjectFundingExpensesTable();
            ProjectFundingWrapper.projectFundingBudgetList projectList = new ProjectFundingWrapper.projectFundingBudgetList();
            ProjectFundingWrapper.projectFundingBudgetList projectList1 = new ProjectFundingWrapper.projectFundingBudgetList(projectFunding);
            
            //All Positive Use Cases: Starts
            String jsonString = ProjectFundingDetailController.getProjectFundingExpenseDetails(projectSAP.Id,String.valueof(system.today()));
            system.assert(jsonString!=null);
            
            jsonString = ProjectFundingDetailController.getProjectFundingBudgetDetails(researchProject.Id);
            system.assert(jsonString!=null);
            
            ProjectDetailsWrapper.ResearchProjectListView myResearchProjectListView = ProjectFundingDetailController.getMyResearchProjectListsForFunding();
            system.assert(myResearchProjectListView!=null);
            
            jsonString =ProjectFundingDetailController.getProjectDetailViewByProjectId(researchProject.Id);
            system.assert(jsonString!=null);
                   
			          
            //All Positive Use Cases: Ends
            Test.StopTest();
        }    
    }
    
     /**************************************************************************************************
    // Purpose      :  Testing Class All negative Use Cases for ProjectFundingDetailController Methods
    // Developer    :  Subhajit
    // Created Date :  10/27/2018                 
    //*************************************************************************************************/
    @isTest 
    public static void testNegativeUseCaseForProjectFundingDetailControllerMethods(){
        
        User usr =[SELECT Id,ContactId FROM User WHERE UserName='test.communityUser1@test.com.testDev' LIMIT 1];   
       
        system.runAs(usr)
        {
             Boolean result = false;
        Contact con =[SELECT Id,Firstname,lastname FROM contact WHERE LastName='Contact1' LIMIT 1 ];
        con.hed__UniversityEmail__c='raj@rmit.edu.au';
        con.lastname='';
        //update con; 
         try
            {
                update con;    
            }
            catch(DmlException ex)
            {
                result =true;                   
               
            }
            system.assert(result);
            
            ResearcherPortalProject__c researchProject = new ResearcherPortalProject__c();
            researchProject.Status__c='Idea';
            researchProject.Access__c='Private';
            researchProject.Approach_to_impact__c='test1';
            researchProject.Discipline_area__c='';
            researchProject.Start_Date__c= date.newInstance(2018, 10, 08) ;
            researchProject.End_Date__c=date.newInstance(2018, 12, 14);
            researchProject.Impact_Category__c='Social';
            researchProject.Title__c='projectTest1';
            researchProject.RecordTypeId = Schema.SObjectType.ResearcherPortalProject__c.getRecordTypeInfosByName().get(Label.RSTP_ResearchProjectRMRecordtype).getRecordTypeId(); 
            insert researchProject;
            system.assert(researchProject!=null);
            
            Researcher_Member_Sharing__c memberSharing = new Researcher_Member_Sharing__c();
            memberSharing.MemberRole__c = 'CI';
            memberSharing.Contact__c = usr.ContactId;
            memberSharing.Researcher_Portal_Project__c = researchProject.Id;
            memberSharing.Primary_Contact_Flag__c = true  ;
            memberSharing.RecordTypeId = Schema.SObjectType.Researcher_Member_Sharing__c.getRecordTypeInfosByName().get(Label.RSTP_ResearchProjectRecordtype).getRecordTypeId();
            //insert member_sharing;
            system.assert(memberSharing!=null);
              Researcher_Member_Sharing__c memberResearch = new Researcher_Member_Sharing__c();
       
            try
           {
                memberResearch .Contact__c = con.Id;
                memberresearch.User__c = usr.Id;
            insert memberresearch;   
            }
                catch(Exception ex)
            {
                result =true;                   
           
            }  
            system.assert(result);
            
           ProjectSAPFunding__c projectSAP = new ProjectSAPFunding__c(); 
           projectSAP.CostElementDescription__c='test Description';
           projectSAP.ValueTypeCode__c  =10;
           projectSAP.ValueTypeDetailCode__c=1;
           projectSAP.DrCrCode__c='S';
           projectSAP.CostExpenseLevel1__c='RMIT_ALL.RPT';
           projectSAP.CostExpenseLevel2__c='RMIT_ALL05.RPT';
           projectSAP.CostExpenseLevel3__c='RMIT_ALL01.RPT'; 
           projectSAP.CostExpenseLevel4__c='RMIT_ALL21.RPT';
           projectSAP.Amount__c= 3000;
           projectSAP.ProjectCode__c=researchProject.Id;
           projectSAP.CELevel8Description__c='Casuals Academic';
           projectSAP.CostElementCode__c='511300';
           projectSAP.PCLevel5Description__c='School of Science';
           projectSAP.ProjectSystemStatusDescription__c='SETC'; 
           projectSAP.ResearchType__c='N';
           projectSAP.WBSCode__c='RE-00203-011';
           projectSAP.WBSDescription__c='Opex - Development of enzyme technology';
           projectSAP.WBSHierarchialCode__c='RE-00203-011';
           projectSAP.WBSLevelHierarchy__c=3.0;
           projectSAP.WBSSequenceNumber__c='PR00015556';
           projectSAP.LeadChiefInvestigatorCode__c=[SELECT Id FROM Contact LIMIT 1].Id;
           projectSAP.Lead_Chief_Investigator_ENumber__c='';
           projectSAP.WBSPersonResponsibleCode__c=[SELECT Id FROM Contact LIMIT 1].Id;  
           projectSAP.WBSPersonResponsibleENumber__c='1502';
           projectSAP.FiscalPeriod__c='';
           projectSAP.ProjectCodeSAPWBSNum__c='';
           insert projectSAP;
           system.assert(projectSAP!=null);
            
            Research_Project_Funding__c projectFunding = new Research_Project_Funding__c();
            projectFunding.Amount_Applied__c = 4512.25;
            projectFunding.Amount_Received__c = 4754.85;
            projectFunding.Budget_Year__c='1993';
            projectFunding.Funding_Category_Description__c='None-HERDC research income';
            projectFunding.ResearcherPortalProject__c = researchProject.Id;
            projectFunding.Scheme_Description__c='testschema';
            insert projectFunding;
            system.assert(projectFunding!=null);
            
            Test.StartTest();
            
            ProjectFundingWrapper.ProjectFundingExpensesDetails projectExpense = new ProjectFundingWrapper.ProjectFundingExpensesDetails();
            ProjectFundingWrapper.ProjectFundingExpensesDetails projectExpense1 = new ProjectFundingWrapper.ProjectFundingExpensesDetails(projectSAP);
            ProjectFundingWrapper.ProjectFundingBudgetDetails projectBudget = new ProjectFundingWrapper.ProjectFundingBudgetDetails();
            ProjectFundingWrapper.ProjectFundingExpensesTable projectTable = new ProjectFundingWrapper.ProjectFundingExpensesTable();
            ProjectFundingWrapper.projectFundingBudgetList projectList = new ProjectFundingWrapper.projectFundingBudgetList();
            ProjectFundingWrapper.projectFundingBudgetList projectList1 = new ProjectFundingWrapper.projectFundingBudgetList(projectFunding);
           
            
            //All Negative Use Cases: Starts
            
            //All Negative Use Cases: Ends
            
            Test.StopTest();
        }    
    }
    
    @isTest
    public static void testExceptionHandlingMethod(){
        
        User u =[SELECT Id,ContactId FROM User WHERE UserName='test.communityUser1@test.com.testDev' LIMIT 1]; 
        
        System.runAs(u) {      
            
            ResearcherPortalProject__c researchProject = new ResearcherPortalProject__c();
            researchProject.Status__c='Idea';
            researchProject.Access__c='Private';
            researchProject.Approach_to_impact__c='test1';
            researchProject.Discipline_area__c='';
            researchProject.Start_Date__c= date.newInstance(2018, 10, 08) ;
            researchProject.End_Date__c=date.newInstance(2018, 12, 14);
            researchProject.Impact_Category__c='Social';
            researchProject.Title__c='projectTest1';
            researchProject.RecordTypeId = Schema.SObjectType.ResearcherPortalProject__c.getRecordTypeInfosByName().get(Label.RSTP_ResearchProjectRMRecordtype).getRecordTypeId(); 
            insert researchProject;
            system.assert(researchProject!=null);
            
              ProjectSAPFunding__c projectSAP = new ProjectSAPFunding__c(); 
           projectSAP.CostElementDescription__c='test Description';
           projectSAP.ValueTypeCode__c  =10;
           projectSAP.ValueTypeDetailCode__c=1;
           projectSAP.DrCrCode__c='S';
           projectSAP.CostExpenseLevel1__c='RMIT_ALL.RPT';
           projectSAP.CostExpenseLevel2__c='RMIT_ALL05.RPT';
           projectSAP.CostExpenseLevel3__c='RMIT_ALL01.RPT'; 
           projectSAP.CostExpenseLevel4__c='RMIT_ALL21.RPT';
           projectSAP.Amount__c= 3000;
           projectSAP.ProjectCode__c=researchProject.Id;
           projectSAP.CELevel8Description__c='Casuals Academic';
           projectSAP.CostElementCode__c='511300';
           projectSAP.PCLevel5Description__c='School of Science';
           projectSAP.ProjectSystemStatusDescription__c='SETC'; 
           projectSAP.ResearchType__c='N';
           projectSAP.WBSCode__c='RE-00203-011';
           projectSAP.WBSDescription__c='Opex - Development of enzyme technology';
           projectSAP.WBSHierarchialCode__c='RE-00203-011';
           projectSAP.WBSLevelHierarchy__c=3.0;
           projectSAP.WBSSequenceNumber__c='PR00015556';
           projectSAP.LeadChiefInvestigatorCode__c=[SELECT Id FROM Contact LIMIT 1].Id;
           projectSAP.Lead_Chief_Investigator_ENumber__c='';
           projectSAP.WBSPersonResponsibleCode__c=[SELECT Id FROM Contact LIMIT 1].Id;  
           projectSAP.WBSPersonResponsibleENumber__c='1502';
           projectSAP.FiscalPeriod__c='';
           projectSAP.ProjectCodeSAPWBSNum__c='';
           insert projectSAP;
           system.assert(projectSAP!=null);
            
            ProjectFundingDetailController.isForceExceptionRequired=true; 
            ProjectFundingDetailController.getProjectFundingExpenseDetails(projectSAP.Id,String.valueof(system.today()));
            ProjectFundingDetailController.getMyResearchProjectListsForFunding();
            ProjectFundingDetailController.getProjectFundingBudgetDetails(researchProject.Id);
            ProjectFundingDetailController.getProjectDetailViewByProjectId(researchProject.Id);
            
           
        }
    }
}