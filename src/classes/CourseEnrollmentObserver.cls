@SuppressWarnings('PMD.AvoidGlobalModifier')
global without sharing class CourseEnrollmentObserver implements csordcb.ObserverApi.IObserver {  
    
    global void execute(csordcb.ObserverApi.Observable o, Object arg) {   
        csordtelcoa.OrderGenerationObservable observable = (csordtelcoa.OrderGenerationObservable)o; 
        list<Id> serviceIds = observable.getServiceIds();
        //do the work
        enrollContacts(serviceIds);
    } 
    
    @TestVisible
    public static void enrollContacts(list<String> serviceIds) {
        // Created to Call the AffiliationRecCreationHelper Class related to ECB-3748 
        
        User usr;
        if(Schema.sObjectType.User.isAccessible()){    
            usr = [Select id, ContactId from User where id =: UserInfo.getUserId() LIMIT 1];
        }
        //ECB-4294 - Shreya: Added to get the Student RecordType Id.
        Id courseEnrollRecordTypeId = Schema.SObjectType.hed__Course_Enrollment__c.getRecordTypeInfosByName().get('Student').getRecordTypeId();   
        Id recordTypeId = Schema.SObjectType.Account.getRecordTypeInfosByName().get('Academic Program').getRecordTypeId();
        Id course21CCRecordTypeId = Schema.SObjectType.hed__Course__c.getRecordTypeInfosByName().get('21CC').getRecordTypeId();
        System.debug(recordTypeId);
        
        if(Test.isRunningTest()){
            usr = TestDataFactoryUtil.testusr;     
        }//
        
        // Added as per ECB-5038 -- Starts
        
        List<csord__Service__c> serviceList;
        if(Schema.sObjectType.csord__Service__c.isAccessible()){ 
            serviceList = [select id,Course__c,Course__r.RecordTypeId
                           , csordtelcoa__Main_Contact__c
                           , Course__r.hed__Account__r.Name
                           , Course_Offering__r.hed__Course__r.hed__Account__r.Name
                           , Course__r.hed__Account__c
                           , Course_Offering__c
                           , Plan__c
                           , Plan__r.hed__Account__c,Is_Upgrade__c,Course_Start_Date__c,
                           Course_Program__c
                           from 
                           csord__Service__c 
                           where 
                           id in :serviceIds 
                           and csordtelcoa__Main_Contact__c <> ''];
            
        }
        
        Set<String> courseType	= new Set<String>();
        
        System.debug('slist-->'+serviceList);
        if(!serviceList.isEmpty()){
            for(csord__Service__c ser : serviceList){
                courseType.add(ser.Course_Offering__r.hed__Course__r.hed__Account__r.Name);
            }
        }
        System.debug('ct-->'+courseType);
        
        // Original - list<contact> conlist = [select id, Student_ID__c from contact where  Account.Name = '21CC' AND id =: usr.ContactId];
        
        List<Contact> conlist = new List<Contact>();
        
        if(Schema.sObjectType.Contact.isAccessible()){    
            conlist  = [select id, Student_ID__c from contact where  id =: usr.ContactId];
        }
        
        list<string> studentIds = new list<string>();
        if(conlist.size() >0){
            for(Contact con :conlist){
                studentIds.add(con.Student_ID__c);
            }
        }
        
        // Added courseType parameter for the AffiliationRecCreationHelper Class createaffilaitions method
        Map<String,Id> affIds  = new Map<String,Id> ();
        if(!courseType.isEmpty()){
            affIds = AffiliationRecCreationHelper.createaffilaitions(studentIds,courseType); 
        }
      
        System.debug('affcreated-->'+affIds);
        //ECB-5038 Ends
        
        Map<Id,csord__Service__c> progPlanServiceMap = new Map<Id,csord__Service__c>();
        Map<Id,Date> progPlanCourseStartMap = new Map<Id,Date>();
        Set<Id> servicePlanIdSet = new Set<Id>();
        Set<Id> upgradeServicePlanIdet = new Set<Id>();
        /*-------------------------------------------*/
        list<hed__Course_Enrollment__c> courseEnrollments = new list<hed__Course_Enrollment__c>();
        List<hed__Program_Enrollment__c> programEnrollmentList = new List<hed__Program_Enrollment__c>();
        for(csord__Service__c service: serviceList) 
        {   
            
            if(service.Course_Program__c== ConfigDataMapController.getCustomSettingValue('Program')){
                hed__Program_Enrollment__c programeEnrollment = new hed__Program_Enrollment__c();
                programeEnrollment.hed__Contact__c = service.csordtelcoa__Main_Contact__c;
                programeEnrollment.Service__c = service.Id;
                programeEnrollment.hed__Account__c = service.Plan__r.hed__Account__c;
                programeEnrollment.hed__Program_Plan__c = service.Plan__c;
                programEnrollmentList.add(programeEnrollment);
                if(service.Is_Upgrade__c==false){
                    servicePlanIdSet.add(service.Plan__c);
                    progPlanServiceMap.put(service.Plan__c,service);
                    progPlanCourseStartMap.put(service.Plan__c,service.Course_Start_Date__c);              
                }else{
                    upgradeServicePlanIdet.add(service.Plan__c);
                    
                }             
            }else if(service.Course_Program__c== ConfigDataMapController.getCustomSettingValue('Course') || service.Course__r.RecordTypeId == course21CCRecordTypeId ){
                hed__Course_Enrollment__c cenr = new hed__Course_Enrollment__c();
                cenr.RecordTypeId = courseEnrollRecordTypeId ; //ECB-4294:Shreya-Set the Course Connection record Type to Student
                cenr.hed__Contact__c = service.csordtelcoa__Main_Contact__c; 
                cenr.hed__Course_Offering__c = service.Course_Offering__c;
                cenr.Service__c= service.Id; // ECB-3900 :Shreya - Course Connection must be related to a service
                if(System.isBatch()){ // ECB-3900 :Shreya - When run in batch context,set the source type as MKT Batch
                    cenr.Source_Type__c = ConfigDataMapController.getCustomSettingValue('MKTBatch');  //ECB-4308 - :Shreya -Replaced Custom Label with Custom Setting
                }else{
                    cenr.Source_Type__c = ConfigDataMapController.getCustomSettingValue('MKT');
                }    
                cenr.Admission_Type__c = ConfigDataMapController.getCustomSettingValue('AdmissionType');
                cenr.hed__Status__c = ConfigDataMapController.getCustomSettingValue('EnrollmentStatus');
                cenr.Batch_Processed__c = false; // ECB-3900 :Shreya - When course connection is cretaed,keep batch processed false 
                //ECB-4549:Sprint Testing- The course connection is created but the Enrollment status field is blank : Shreya:7/10/2018
                cenr.Enrolment_Status__c = ConfigDataMapController.getCustomSettingValue('CourseConnectionEnrolmentStatusE');
                
                //Created to link Course Connection to Affiliation   
				String v = service.csordtelcoa__Main_Contact__c+ service.Course_Offering__r.hed__Course__r.hed__Account__r.Name;
                if(affIds.containsKey(v))
                {
                    cenr.hed__Affiliation__c = affIds.get(v);
                }
                /*-----------------*/    
                courseEnrollments.add(cenr);
            }   
            
        }
        
        
        Set<Id> progEnrollIdSet = new Set<Id>();   
        try{
            System.debug('Size--->'+programEnrollmentList.size());
            if(programEnrollmentList.size() > 0){
                Database.SaveResult[] srList = Database.insert(programEnrollmentList ,false);
                
                
                for(Database.SaveResult sr : srList)
                {
                    if (sr.isSuccess()) 
                    {                                    
                        progEnrollIdSet.add(sr.getId());                                                           
                    }
                }
            }   
        }catch(DmlException ex)
        {
            system.debug('Exception:::'+ex);
        }
        
        List<hed__Program_Enrollment__c> progEnrollList;
        if(Schema.sObjectType.hed__Program_Enrollment__c.isAccessible()){
            progEnrollList = [SELECT Id,hed__Program_Plan__c,Service__c,Service__r.Is_Upgrade__c FROM hed__Program_Enrollment__c WHERE Id IN :progEnrollIdSet];
        }
        
        Map<Id,Id> planProgEnrollMap = new Map<Id,Id>();
        Set<String> upgradeProgEnrollIdSet = new Set<String>();
        
        if(!progEnrollList.isEmpty()){
            for(hed__Program_Enrollment__c enroll : progEnrollList){
                planProgEnrollMap.put(enroll.hed__Program_Plan__c,enroll.Id);
                if(enroll.Service__r.Is_Upgrade__c == true){
                    upgradeProgEnrollIdSet.add(enroll.Id);
                }
            }
        }   
        
        if(!servicePlanIdSet.isEmpty()){
            Set<Id> planId = new Set<Id>();
            
            List<hed__Program_Plan__c> planProgramId;
            if(Schema.sObjectType.hed__Program_Plan__c.isAccessible()){
                planProgramId = [SELECT Id,hed__Account__c,hed__Account__r.ParentId,hed__Account__r.Parent.Name,hed__Account__r.RecordTypeId From hed__Program_Plan__c WHERE Id IN: servicePlanIdSet];
            }
            if(planProgramId.size() >0){
                for(hed__Program_Plan__c ppl : planProgramId){
                    if(ppl.hed__Account__r.Parent.Name == ConfigDataMapController.getCustomSettingValue('ParentAccountName') && ppl.hed__Account__r.RecordTypeId == recordTypeId){
                        planId.add(ppl.Id);
                        System.debug('Id-->'+planId);
                    }
                }
            }
            Set<Id> courseId = new Set<Id>();
            
            List<hed__Plan_Requirement__c> planRequirementList;
            if(Schema.sObjectType.hed__Plan_Requirement__c.isAccessible()){
                planRequirementList = [SELECT hed__Course__c,Id,hed__Program_Plan__c FROM hed__Plan_Requirement__c WHERE  hed__Sequence__c = 1.00 AND hed__Program_Plan__r.Id IN: planId];
            }
            
            if(planRequirementList.size()>0){
                for(hed__Plan_Requirement__c plan:planRequirementList){
                    hed__Course_Offering__c courseOfferingRec = getCourseOffering(plan.hed__Course__c,progPlanCourseStartMap.get(plan.hed__Program_Plan__c));
                    if(courseOfferingRec!=null){
                        courseEnrollments.add(createCourseConnection(courseOfferingRec,progPlanServiceMap.get(plan.hed__Program_Plan__c),planProgEnrollMap.get(plan.hed__Program_Plan__c)));
                    }
                }
                
            }
            
        }
        if(!upgradeServicePlanIdet.isEmpty()){
            
            List<hed__Plan_Requirement__c> planRequirementList;
            if(Schema.sObjectType.hed__Plan_Requirement__c.isAccessible()){
                planRequirementList = [SELECT hed__Course__c,Id,hed__Program_Plan__c FROM      hed__Plan_Requirement__c WHERE hed__Program_Plan__c IN : upgradeServicePlanIdet AND hed__Course__c!=null];           
            }
            Set<Id> courseIdSet = new Set<Id>();
            Map<Id,Id> courseProgPlanMap = new Map<Id,Id>();
            
            for(hed__Plan_Requirement__c preq : planRequirementList){
                
                courseIdSet.add(preq.hed__Course__c);           
                courseProgPlanMap.put(preq.hed__Course__c,preq.hed__Program_Plan__c);
                
            }
            
            
            
            List<hed__Course_Enrollment__c> cenrollList = [SELECT hed__Course_Offering__c,hed__Course_Offering__r.hed__Course__c,hed__Contact__c,hed__Program_Enrollment__c FROM hed__Course_Enrollment__c WHERE 
                                                           hed__Course_Offering__r.hed__Course__c IN :courseIdSet AND hed__Contact__c = :usr.ContactId AND hed__Program_Enrollment__c=null];
            
            if(!cenrollList.isEmpty()){
                for(hed__Course_Enrollment__c cenroll : cenrollList){
                    
                    Id progPlanId = courseProgPlanMap.get(cenroll.hed__Course_Offering__r.hed__Course__c);
                    cenroll.hed__Program_Enrollment__c = planProgEnrollMap.get(progPlanId);                            
                    
                }                          
                
                Database.update(cenrollList,false);
            }
            
        }
        
        /* if(!upgradeProgEnrollIdSet.isEmpty()){
CourseTransactionController.courseTransactionPartialProgram(upgradeProgEnrollIdSet);        
}*/
        
        system.debug('courseEnrollments >> ' + courseEnrollments);
        if (courseEnrollments.size() > 0)
        {
            Database.SaveResult[] srList = Database.insert(courseEnrollments ,false);
            
            System.debug('Calling My Method+++++'+courseEnrollments);
            
            System.debug('Save Result'+srList);
            Set<Id> incomingCourseEnrolments = new Set<Id>();   
            for(Database.SaveResult sr : srList)
            {
                if (sr.isSuccess()) 
                {                                    
                    incomingCourseEnrolments.add(sr.getId());                                                           
                }
            }
            
            //ECB-4438 - The course transaction record in Course Transaction object should be created only for course that have a price. i.e do not create a record for 21CC type 
            
            //Set to store the course conneciton ids of courses other than 21CC
            Set<Id> courseTransacCourseEnrollIdList = new Set<Id>();         
            if( !incomingCourseEnrolments.isEmpty()){
                
                for(hed__Course_Enrollment__c cenroll : [SELECT Id,hed__Course_Offering__r.hed__Course__r.RecordTypeId FROM hed__Course_Enrollment__c 
                                                         WHERE Id IN :incomingCourseEnrolments
                                                         AND hed__Course_Offering__r.hed__Course__r.RecordTypeId != :course21CCRecordTypeId]){
                                                             
                                                             courseTransacCourseEnrollIdList.add(cenroll.Id);
                                                         }
            }
            
            //ECB-4416: Added to call the function that creates course transaction record for each of the course connections that got created : Shreya
            if (!incomingCourseEnrolments.isEmpty()) {
                
                CourseTransactionController.createCourseTransactionRecord( courseTransacCourseEnrollIdList );
                
            }
            
            if (!incomingCourseEnrolments.isEmpty()) {
                System.debug('Courselifecycle--->'+IncomingCourseEnrolments);
                //......Below method is for automatically create stage records on CourseConnection Life Cycle Object--
                CreateRecordsOnCourseConnectionLifeCycle.createCourseConnection(incomingCourseEnrolments);
                //CCDetailsIpaaS.sendCourseConn(IncomingCourseEnrolments);                   
            }
            
            
        }
        
    }
    
    public static hed__Course_Offering__c getCourseOffering(Id courseId,Date courseStartDate){
        hed__Course_Offering__c courseOfferingRecord;
        try{
            if(Schema.sObjectType.hed__Course_Offering__c.isAccessible()){
                courseOfferingRecord = [SELECT Id,Name,hed__Course__c FROM hed__Course_Offering__c WHERE hed__Course__c =:courseId AND hed__Start_Date__c = :courseStartDate LIMIT 1];                                             
            }
        }catch(QueryException ex){
            system.debug('exception:::'+ex);
        }                              
        
        if(courseOfferingRecord!=null){
            return  courseOfferingRecord;                                 
        }else{
            return null;
            
        }             
        
    }
    
    
    
    public static hed__Course_Enrollment__c createCourseConnection(hed__Course_Offering__c cOfferRec,csord__Service__c serviceRec,Id progEnrollId){
        
        hed__Course_Enrollment__c cenr = new hed__Course_Enrollment__c();
        cenr.RecordTypeId = Schema.SObjectType.hed__Course_Enrollment__c.getRecordTypeInfosByName().get('Student').getRecordTypeId() ;
        cenr.hed__Contact__c = serviceRec.csordtelcoa__Main_Contact__c;
        cenr.hed__Course_Offering__c = cOfferRec.Id;
        cenr.hed__Program_Enrollment__c = progEnrollId;
        cenr.Service__c= serviceRec.Id;
        if(System.isBatch()){
            cenr.Source_Type__c = ConfigDataMapController.getCustomSettingValue('MKTBatch');  
            
        }else{
            cenr.Source_Type__c = ConfigDataMapController.getCustomSettingValue('MKT');
            
        }   
        
        cenr.Admission_Type__c = ConfigDataMapController.getCustomSettingValue('AdmissionType');
        cenr.hed__Status__c = ConfigDataMapController.getCustomSettingValue('EnrollmentStatus');
        cenr.Batch_Processed__c = false;
        //ECB-4549:Sprint Testing- The course connection is created but the Enrollment status field is blank : Shreya:7/10/2018
        cenr.Enrolment_Status__c = ConfigDataMapController.getCustomSettingValue('CourseConnectionEnrolmentStatusE');
        //ECB-4780 - Code to populate Academic Program field
        cenr.hed__Account__c = serviceRec.Plan__r.hed__Account__c;
        
        return cenr;
        
    }
    
    
}