/*********************************************************************************
*** @ClassName         : FUND_SOURCE_REST
*** @Author            : Shubham Singh 
*** @Requirement       : 
*** @Created date      : 13/08/2018
*** @Modified by       : Shubham Singh 
*** @modified date     : 13/08/2018
**********************************************************************************/
//Warning:  PMD Team: Cyclomatic Complexity Reached for this class. If any method to be added then create a helper class and call that class method instead.

@RestResource(urlMapping = '/HEDA/v1/FundSource')
global with sharing class FUND_SOURCE_REST
{
    public String fundSourceCode;
    public String effectiveDate;
    public String status;
    public String description;
    public String longDescription;
    public String fundSourceState;
    public String fundSourceNational;
    public String fundSourceBroad;
    public String fundSourceDEEWR;
    public String state;
    public String apprenticeTraineeWaiver;
    public String apprenticeTraineeIndicator;
    public String subjectToEsos;
    
    @HttpPost
    global static Map < String, String > upsertFundSource() {
        //final response to send
        Map < String, String > responsetoSend = new Map < String, String > ();
        
        try {
            //request and response variable
            RestRequest request = RestContext.request;
            
            RestResponse response = RestContext.response;
            
            //no need to add [] in JSON
            String reqStr = '[' + request.requestBody.toString() + ']';
            
            response.addHeader('Content-Type', 'application/json');
            
            //getting the data from JSON and Deserialize it
            List < FUND_SOURCE_REST > postString = (List < FUND_SOURCE_REST > ) System.JSON.deserialize(reqStr, List < FUND_SOURCE_REST > .class);
            system.debug('postString -->' + postString );
			
			
            //to store fundsource code
            list < String > updateFundSourceCode = new list < String > ();
            //to create map of effective date
            map < String,Date > mapEffectiveDate = new map < String,Date >();
			 //getting the old fundsource for update
            list < Fund_Source__c > listFSource = new list < Fund_Source__c > ();
            //check key values should not be null && getting the old fundsource for update
            listFSource = checkKeyValuesNotNull(postString,mapEffectiveDate,updateFundSourceCode);
            if(listFSource == null)
			{
				responsetoSend = throwError();
                return responsetoSend;
			}
           
            
            //map to update the old fundsource
            map < String, ID > oldFundSOurce = new map < String, ID > ();
            map < String, Fund_Source__c > stringFundSurceMap = new map < String, Fund_Source__c > ();
			for (Fund_Source__c fs : listFSource) {
                if(fs.Name != null){
                    String getEffDate = fs.Name+'';
                    if(mapEffectiveDate.size() > 0 && mapEffectiveDate.get(getEffDate) != null
					&& mapEffectiveDate.get(getEffDate) <= System.today()){
						oldFundSOurce.put(fs.Name,fs.ID);
                        stringFundSurceMap.put(fs.Name,fs);
					}
					else if(fs.Effective_Date__c > mapEffectiveDate.get(getEffDate)){
						responsetoSend.put('message', 'OK');
						responsetoSend.put('status', '200');
						return responsetoSend;
					}
                    else{
                        responsetoSend.put('message', 'OK');
                        responsetoSend.put('status', '200');
                        return responsetoSend;
                    }
                }
            }
            
			
            //insert or update the fund source
            upsertLstFundSource(postString,oldFundSOurce,stringFundSurceMap);
			
            responsetoSend.put('message', 'OK');
            responsetoSend.put('status', '200');
            return responsetoSend;
        } catch (Exception excpt) {
            //If exception occurs then return this message to IPASS
            responsetoSend = throwError();
            return responsetoSend;
        }
    }
	
    private static list < Fund_Source__c > checkKeyValuesNotNull(List < FUND_SOURCE_REST > postString,map < String,Date > mapEffectiveDate,list < String > updateFundSourceCode)
    {		
	   
    	if (postString != null) 
		{
                for (FUND_SOURCE_REST fSource: postString)
			   {
                    if (!String.isBlank(fSource.fundSourceCode))
                    {                   
                        updateFundSourceCode.add(fSource.fundSourceCode);
                        
                    }
                    else
                    {
                        return null;
                        
                    }
					if(!String.isBlank(fSource.effectiveDate))
                    {
                       mapEffectiveDate.put(fSource.fundSourceCode,Date.ValueOf(fSource.effectiveDate));
                    }
					
                    
                }
				
        }
    	
    	return getOldFundSource(updateFundSourceCode);
    }
	
	private static list < Fund_Source__c > getOldFundSource(list < String > updateFundSourceCode)
    {    
	        list < Fund_Source__c > listFSource = new list < Fund_Source__c > ();
            if ( updateFundSourceCode.size() > 0 && Schema.sObjectType.Fund_Source__c.isAccessible() ) {
                
                   listFSource = [select id, Effective_Date__c, Name/*, Apprentice_Trainee_Flag__c, AT_Waiver__c*/, Description__c, Fund_Source_Bro__c, Fund_Source_DEEWR__c, Fund_Source_National__c, Fund_Source_State__c, Long_Description__c, State__c, Status__c/*, Subject_to_ESOS__c*/ from Fund_Source__c where Name IN: updateFundSourceCode]; 
               
               
            }

        return listFSource;

    }
	
	private static void upsertLstFundSource(List < FUND_SOURCE_REST > postString,map<string,ID>oldFundSOurce,map<string,Fund_Source__c>stringFundSurceMap)
	{
		//upsert fund source
        list < Fund_Source__c > listUpsertFSource = new list < Fund_Source__c > ();
        //fund source
        Fund_Source__c upsertFSource;
	       for (FUND_SOURCE_REST fsr: postString)
		   {
                
                upsertFSource = new Fund_Source__c();
                upsertFSource.Name = fsr.fundSourceCode;
                upsertFSource.Effective_Date__c = setFSEffectiveDateField(fsr.effectiveDate);//fsr.effectiveDate != null ? Date.valueOf(fsr.effectiveDate) : null;
                upsertFSource.Status__c = setSFStatusField(fsr.status);// fsr.status != null && fsr.status.equalsIgnoreCase('A') ? 'Active' : 'Inactive';
                upsertFSource.Description__c = fsr.description;
                upsertFSource.Long_Description__c = fsr.longDescription;
                upsertFSource.Fund_Source_State__c = fsr.fundSourceState;
                upsertFSource.Fund_Source_National__c = fsr.fundSourceNational;
                upsertFSource.Fund_Source_Bro__c = fsr.fundSourceBroad;
                upsertFSource.Fund_Source_DEEWR__c = fsr.fundSourceDEEWR;
                upsertFSource.State__c = fsr.state;                
                //upsertFSource.AT_Waiver__c = fsr.apprenticeTraineeWaiver != null && ((fsr.apprenticeTraineeWaiver).equals('Y') || (fsr.apprenticeTraineeWaiver).equals('y')) ? true : false;
                //upsertFSource.Apprentice_Trainee_Flag__c = fsr.apprenticeTraineeIndicator != null && ((fsr.apprenticeTraineeIndicator).equals('Y') || (fsr.apprenticeTraineeIndicator).equals('y')) ? true : false;
                upsertFSource.Subject_to_ESOS__c = setFSSubjectESOSFields(fsr.subjectToEsos);//fsr.subjectToEsos != null && ((fsr.subjectToEsos).equals('Y') || (fsr.subjectToEsos).equals('y')) ? true : false;
                
                //assigning the fund source id for update
               
				 if (oldfundsource.size() > 0 && oldfundsource.get(fsr.fundsourcecode) != null)
				 {
					 //assignFundSourceIdToUpdate(fsr,upsertFSource,stringFundSurceMap);
					 if (stringfundsurcemap.get(fsr.fundsourcecode).effective_date__c <= upsertFSource.effective_date__c)
		             {
                        upsertFSource.id = oldfundsource.get(fsr.fundsourcecode);
                     }
					else
					{
			            upsertFSource=null;
			        }
					 
				 }
				 if(upsertFSource != null && Date.ValueOf(fsr.effectiveDate) <= System.today())
				 {
					 listupsertfsource.add(upsertFSource);
			     }
				       
            }
			upsertListOfFundSource(listUpsertFSource,postString);
           
	}
	private static Boolean setFSSubjectESOSFields(string subjectEsosCode)
    {
    	Boolean sTE = (subjectEsosCode != null && (subjectEsosCode).equalsIgnoreCase('Y'))? true : false;
    	return 	sTE;
    }
    private static Date setFSEffectiveDateField(string eFFD)
    {
    	
		Date eD = eFFD != null ? Date.valueOf(eFFD) : null;
        return eD;
    }
    private static string setSFStatusField(string fStatus)
    {
    	String statusVal= (fStatus != null && fStatus.equalsIgnoreCase('A'))? 'Active' : 'Inactive';
    	return statusVal;
    }
    private static void upsertListOfFundSource(list < Fund_Source__c > listUpsertFSource,List < FUND_SOURCE_REST > postString)
	{
		
		if (listUpsertFSource!=null && listUpsertFSource.size()>0 && (Schema.sObjectType.Fund_Source__c.isUpdateable()
            		&& Schema.sObjectType.Fund_Source__c.isCreateable()))
		{
		
                      upsert listUpsertFSource Name;
				
		}
		
	}
    public static Map < String, String > throwError() {
        //to store the error response
        Map < String, String > errorResponse = new Map < String, String > ();
        errorResponse.put('message', 'Oops! Student was not well defined, amigo.');
        errorResponse.put('status', '404');
        //If error occurs then return this message to IPASS
        return errorResponse;
    }
    
}