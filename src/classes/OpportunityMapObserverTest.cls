@isTest(seeAllData=false)
public class OpportunityMapObserverTest {
	@testSetup 
    static void setupTestData() {
   
  		Account accDepartment = TestUtility.createTestAccount(true
            , 'Department Account 001'
            , Schema.SObjectType.Account.getRecordTypeInfosByName().get('Academic Institution').getRecordTypeId());
 		Account acc = TestUtility.createTestAccount(true, 'Test Account',
 			Schema.SObjectType.Account.getRecordTypeInfosByName().get('Academic Institution').getRecordTypeId());
        List<String> lstParams = new List<String>{'Test First', 'Test Last'};
        Contact con = TestUtility.createTestContact(true,lstParams, acc.Id);
        Contact con2 = TestUtility.createTestContact(true,lstParams, acc.Id);
        
        
        Opportunity opp = TestUtility.createTestOpportunity(true, 'Test Opportunity', acc.Id);
        
        Custom_Opportunity__c custOpp=new Custom_Opportunity__c();
        custOpp.Opportunity__c=opp.Id;
        insert custOpp;
        
 	 	List<String> lstCreateProduct =  new List<String>{'Test Product','code1', 'Subscription'};
	 
  	    Product2 prod = TestUtility.createProduct(true,lstCreateProduct);
       
        PriceBookEntry pb = TestUtility.createPriceBookEntry(true, prod.Id);
        
        cscfga__Product_Definition__c prodDefintion = new cscfga__Product_Definition__c (Name = 'Test definition'
            , cscfga__Description__c = 'Test definition 1');
        insert prodDefintion;

        cscfga__Attribute_Definition__c attrDef = new cscfga__Attribute_Definition__c(Name = 'Test Attribute'
            , cscfga__Product_Definition__c = prodDefintion.Id
            , cscfga__Is_Line_Item__c = true
            , cscfga__Line_Item_Description__c = 'Sample Attr'
            , cscfga__Line_Item_Sequence__c = 0);
        insert attrDef;
       	
       	cscfga__Product_Basket__c basket = new cscfga__Product_Basket__c(cscfga__Opportunity__c = opp.Id);
        insert basket;
        
        cscfga__Product_Configuration__c config = new cscfga__Product_Configuration__c(Name = 'Test config '
            , cscfga__Product_Definition__c = prodDefintion.Id
            , cscfga__Product_Basket__c = basket.Id
            , cscfga__Product_Family__c = 'Test Family');  
        insert config;
       
        cscfga__Attribute__c attr = new cscfga__Attribute__c(Name = 'Test Attribute1'
            , cscfga__Product_Configuration__c = config.Id
            , cscfga__Attribute_Definition__c = attrDef.Id
            , cscfga__Is_Line_Item__c = true
            , cscfga__Line_Item_Description__c = 'Sample Attr'
            , cscfga__is_active__c = true);
        insert attr;

        hed__Term__c term = new hed__Term__c(hed__Account__c = accDepartment.Id);
        insert term;

        hed__Course__c course = new hed__Course__c(Name = 'Course 001'
            , recordTypeId = Schema.SObjectType.hed__Course__c.getRecordTypeInfosByName().get('21CC').getRecordTypeId()
            , hed__Account__c = accDepartment.Id);
        insert course;
        
        hed__Course_Offering__c courseOffering = new hed__Course_Offering__c(Name = 'Course Offering 001'
            , hed__Course__c = course.Id
            , hed__Term__c = term.Id 
            , hed__Start_Date__c = system.today()
            , hed__End_Date__c  = system.today());
        insert courseOffering;
        
        OpportunityLineItem oli = TestUtility.createOpportunityLineItem(false, opp.Id, pb.Id);
        oli.cscfga__Attribute__c = attr.Id;
  		insert oli;
        
        csord__Order__c ord = new csord__Order__c(csordtelcoa__Opportunity__c = opp.Id, csord__Account__c = acc.Id, csord__Identification__c = 'Order-' + opp.Id);
        insert ord;
        
  		csord__Subscription__c subscription =new csord__Subscription__c(csord__Account__c = acc.Id,csord__Identification__c = 'Subscription-' + config.Id) ;
        subscription.csord__Order__c = ord.Id;
        insert subscription;
        csord__Service__c service = new csord__Service__c(Name = 'Test Service'
           // , csordtelcoa__Main_Contact__c=con.Id
            , Course_Offering__c=courseOffering.ID
            , csordtelcoa__Product_Configuration__c = config.Id
            , csordtelcoa__Product_Basket__c = basket.Id
            , csord__Identification__c = 'Service_' + config.Id
            , csord__Subscription__c = subscription.Id
            , csord__Order__c = ord.Id);
        insert service;

        
    }
    
	static testmethod void  testMapOpportunityFieldValues() {
       
		setupTestData();
		Test.startTest();
		List<Id> servicesId=new List<Id>();
	    for(csord__Service__c service:[SELECT Id from csord__Service__c]){
	      servicesId.add(service.Id);
	    }
    	OpportunityMapObserver.mapOpportunityFieldValues(servicesId);
    	System.assert(true);
		Test.stopTest();
	}
}