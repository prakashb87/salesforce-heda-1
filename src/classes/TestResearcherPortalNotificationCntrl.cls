@isTest
public with sharing class TestResearcherPortalNotificationCntrl {
    static testmethod void testSetViewed() {
        JSONMessage.APIResponse socialResponse = new JSONMessage.APIResponse();
        socialResponse.responseObject = JSON.serialize(new Map<String, SocialBundle> {
            'contentId' => new SocialBundle()
        });
        
        cms.ServiceEndPoint.addMockResponse('SocialAPI', Json.serialize(socialResponse));
        
        String response = ResearcherPortalNotificationController.setViewed('contentId');
        
        System.assert(response.contains('contentId'));
    }
}