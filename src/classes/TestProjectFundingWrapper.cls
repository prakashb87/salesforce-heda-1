/*******************************************
Purpose: Test class ProjectFundingWrapper
History:
Created By Subhajit on 27/10/2018
*******************************************/
@isTest
public class TestProjectFundingWrapper {
    
    /************************************************************************************
    // Purpose      :  Creating Test data for Community User for all test methods 
    // Developer    :  Ankit
    // Created Date :  10/24/2018                 
    //***********************************************************************************/
    @testSetup static void testDataSetup() {
        
              
        RSTP_TestDataFactoryUtils.createNonCommunityUsersWithProfile(1,'System Administrator','Activator');
        
        User adminUser =[SELECT Id,ContactId FROM User WHERE UserName='test.nonCommunityUser1@test.com.testDev' LIMIT 1];
        System.runAs(adminUser)   
        {
            Map<String,Integer> allnumbersOfDataMap = new Map<String,Integer>();
            allnumbersOfDataMap.put('user',1);
            allnumbersOfDataMap.put('account',1);
            allnumbersOfDataMap.put('contact',1);
            
            Map<String,String> requiredInfoMap = new Map<String,String>();
            requiredInfoMap.put('userType','CommunityUsers');
            requiredInfoMap.put('profile','RMIT Researcher Portal Plus Community User');
            
            Boolean userCreatedFlag = RSTP_TestDataFactoryUtils.testRSTPDataSetup(allnumbersOfDataMap,requiredInfoMap);
            System.assert(userCreatedFlag==true,'Community User Created'); 
        }
        
    }    
    
    /************************************************************************************
    // Purpose      :  test method for code coverage of ProjectFundingWrapper
    // Developer    :  Subhajit
    // Created Date :  10/27/2018                 
    //***********************************************************************************/
    @isTest
    public static void testProjectFundingWrapperUtilsMethods(){ 
        
        User usr =[SELECT Id,ContactId FROM User WHERE UserName='test.communityUser1@test.com.testDev' LIMIT 1];
        
        system.runAs(usr)
        {
            ProjectFundingWrapper.ProjectFundingExpensesTable pfeTable = new ProjectFundingWrapper.ProjectFundingExpensesTable();
            pfeTable.projectFundingList = new list<ProjectFundingWrapper.ProjectFundingExpensesDetails>();
            pfeTable.lastRefreshedDate = system.now();
            System.assert(pfeTable!=null,'pfeTable not null');
            
            ProjectFundingWrapper.ProjectFundingExpensesDetails pfeDetails = new ProjectFundingWrapper.ProjectFundingExpensesDetails();
            System.assert(pfeDetails != null,'pfeDetails not null');
            
            ProjectSAPFunding__c eachFundingRec = new ProjectSAPFunding__c(); 
            eachFundingRec.CostElementDescription__c='test Description';
            eachFundingRec.ValueTypeCode__c  =10;
            eachFundingRec.ValueTypeDetailCode__c=1;
            eachFundingRec.DrCrCode__c='S';
            eachFundingRec.CostExpenseLevel1__c='RMIT_ALL.RPT';
            eachFundingRec.CostExpenseLevel2__c='RMIT_ALL05.RPT';
            eachFundingRec.CostExpenseLevel3__c='RMIT_ALL01.RPT'; 
            eachFundingRec.CostExpenseLevel4__c='RMIT_ALL21.RPT';
            eachFundingRec.Amount__c= 3000;
            eachFundingRec.CELevel8Description__c='Casuals Academic';
            eachFundingRec.CostElementCode__c='511300';
            eachFundingRec.PCLevel5Description__c='School of Science';
            eachFundingRec.ProjectSystemStatusDescription__c='SETC'; 
            eachFundingRec.ResearchType__c='N';
            eachFundingRec.WBSCode__c='RE-00203-011';
            eachFundingRec.WBSDescription__c='Opex - Development of enzyme technology';
            eachFundingRec.WBSHierarchialCode__c='RE-00203-011';
            eachFundingRec.WBSLevelHierarchy__c=3.0;
            eachFundingRec.WBSSequenceNumber__c='PR00015556';
            
            
            ProjectFundingWrapper.ProjectFundingExpensesDetails  pfe1Details1 = new ProjectFundingWrapper.ProjectFundingExpensesDetails(eachFundingRec);
            System.assert(pfe1Details1!=null,'pfe1Details1 not null');
            
            ProjectFundingWrapper.ProjectFundingBudgetDetails  pfb1Details1 = new ProjectFundingWrapper.ProjectFundingBudgetDetails();
            pfb1Details1.projectFundingBudgetList = new list<ProjectFundingWrapper.projectFundingBudgetList>();
            System.assert(pfb1Details1!=null,'pfb1Details1 not null');
            
            Research_Project_Funding__c eachFundingRec1 = new Research_Project_Funding__c(); 
            eachFundingRec1.Amount_Applied__c=100;
            eachFundingRec1.Amount_Received__c=1000;
            eachFundingRec1.Budget_Year__c='1985'; 
            eachFundingRec1.Scheme_Description__c='Test Scheme';  
            
            ProjectFundingWrapper.projectFundingBudgetList pfeList = new ProjectFundingWrapper.projectFundingBudgetList();
            System.assert(pfeList!=null,'pfeList not null');
            pfeList = new ProjectFundingWrapper.projectFundingBudgetList(eachFundingRec1);
            System.assert(pfeList!=null,'pfeList not null');
        }
    }   
     /************************************************************************************
    // Purpose      :  Negative test method for code coverage of ProjectFundingWrapper
    // Developer    :  Shalu
    // Created Date :  11/22/2018                 
    //***********************************************************************************/
    
    @isTest
    public static void testProjectFundingWrapperUtilsMethodsNegative(){ 
        
        User usr =[SELECT Id,ContactId FROM User WHERE UserName='test.communityUser1@test.com.testDev' LIMIT 1];
        
        system.runAs(usr)
        {
            
            ProjectFundingWrapper.ProjectFundingExpensesTable pfeTable = new ProjectFundingWrapper.ProjectFundingExpensesTable();
            pfeTable.projectFundingList = new list<ProjectFundingWrapper.ProjectFundingExpensesDetails>();
            pfeTable.lastRefreshedDate = null;
            System.assert(pfeTable.lastRefreshedDate==null);
            
            ProjectFundingWrapper.ProjectFundingExpensesDetails pfeDetails = new ProjectFundingWrapper.ProjectFundingExpensesDetails();
            pfeDetails = null;
            System.assert(pfeDetails == null);
            
            ProjectSAPFunding__c eachFundingRec = new ProjectSAPFunding__c(); 
            eachFundingRec.CostElementDescription__c='';
            eachFundingRec.ValueTypeCode__c  =0;
            eachFundingRec.ValueTypeDetailCode__c=0;
            eachFundingRec.DrCrCode__c='';
            eachFundingRec.CostExpenseLevel1__c='';
            eachFundingRec.CostExpenseLevel2__c='';
            eachFundingRec.CostExpenseLevel3__c=''; 
            eachFundingRec.CostExpenseLevel4__c='';
            eachFundingRec.Amount__c= 0;
            eachFundingRec.CELevel8Description__c='';
            eachFundingRec.CostElementCode__c='';
            eachFundingRec.PCLevel5Description__c='';
            eachFundingRec.ProjectSystemStatusDescription__c=''; 
            eachFundingRec.ResearchType__c='';
            eachFundingRec.WBSCode__c='';
            eachFundingRec.WBSDescription__c='';
            eachFundingRec.WBSHierarchialCode__c='';
            eachFundingRec.WBSLevelHierarchy__c=0;
            eachFundingRec.WBSSequenceNumber__c='';
            
            
            ProjectFundingWrapper.ProjectFundingExpensesDetails  pfe1Details1 = new ProjectFundingWrapper.ProjectFundingExpensesDetails(eachFundingRec);
            System.assert(pfe1Details1!=null,'pfe1Details1 not null');
            
            ProjectFundingWrapper.ProjectFundingBudgetDetails  pfb1Details1 = new ProjectFundingWrapper.ProjectFundingBudgetDetails();
            pfb1Details1.projectFundingBudgetList = new list<ProjectFundingWrapper.projectFundingBudgetList>();
            System.assert(pfb1Details1!=null,'pfb1Details1 not null');
            
            Research_Project_Funding__c eachFundingRec1 = new Research_Project_Funding__c(); 
            eachFundingRec1.Amount_Applied__c=0;
            eachFundingRec1.Amount_Received__c=0;
            eachFundingRec1.Budget_Year__c=''; 
            eachFundingRec1.Scheme_Description__c='';  
            
            ProjectFundingWrapper.projectFundingBudgetList pfeList = new ProjectFundingWrapper.projectFundingBudgetList();
            pfeList =null;
            System.assert(pfeList==null);
            pfeList = new ProjectFundingWrapper.projectFundingBudgetList(eachFundingRec1);
            System.assert(pfeList!=null,'pfeList not null');
        }
    }
}