public without sharing class MockSuccessSingleSamsEnrollReq implements ISAMSEnrollmentRequest {

    public void sendEnrollDetailToSAMS(List<CourseConnectionWrapper> samsEnrollmentRequestBody) {
        System.debug('Mock request assumed to be successful!');
    }
    
    public Map<Id, JsonCourseWrapper.IpaasResponseWrapper> getSAMSEnrollResByCourseConnId() {
        Id courseConnectionId;
        // Assume test data is already setup
        List<hed__Course_Enrollment__c> courseConnections = [SELECT Id FROM hed__Course_Enrollment__c LIMIT 10];
        Map<Id, JsonCourseWrapper.IpaasResponseWrapper> result = new Map<Id, JsonCourseWrapper.IpaasResponseWrapper>();
        for(hed__Course_Enrollment__c enroll : courseConnections){   
            JsonCourseWrapper.IpaasResponseWrapper successResponse = new JsonCourseWrapper.IpaasResponseWrapper();
            successResponse.courseConnectionId = enroll.Id;
            successResponse.result = 'Success';
            successResponse.errorCode = '';
            successResponse.errorText = '';
            result.put(enroll.Id,successResponse);
        }     

        return result;
    }
    
 
}