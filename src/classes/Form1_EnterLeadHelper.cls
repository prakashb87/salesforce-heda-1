public with sharing class Form1_EnterLeadHelper {

	public static Lead setPicklistValuesIfNotNone(Lead createNewLead, Lead newLead){
            if(newLead.Subject_Expert_SE_Consulted__c!='--None--')
            {
                createNewLead.Subject_Expert_SE_Consulted__c = newLead.Subject_Expert_SE_Consulted__c;
            }
            if(newLead.HOS_Senior_Executive_informed__c!='--None--')
            {
                createNewLead.HOS_Senior_Executive_informed__c = newLead.HOS_Senior_Executive_informed__c;
            }
            if(newLead.Current_Partner__c!='--None--')
            {
                createNewLead.Current_Partner__c = newLead.Current_Partner__c;
            }
            /*if(newLead.RMIT_Staff_Title__c!='--None--')
            {
                createNewLead.RMIT_Staff_Title__c = newLead.RMIT_Staff_Title__c;
            }*/
            if(newLead.Type_of_Planned_Activity__c!='--None--')
            {
                createNewLead.Type_of_Planned_Activity__c = newLead.Type_of_Planned_Activity__c;
            }
            if(newLead.SE_Supports_proposal__c!='--None--')
            {
                createNewLead.SE_Supports_proposal__c = newLead.SE_Supports_proposal__c;
            }
            if(newLead.HoS_Contacted__c!='--None--')
            {
                createNewLead.HoS_Contacted__c = newLead.HoS_Contacted__c;
            }
            if(newLead.SE_Supports_proposal__c!='--None--')
            {
                createNewLead.SE_Supports_proposal__c = newLead.SE_Supports_proposal__c;
            }

			return createNewLead;
	}

}