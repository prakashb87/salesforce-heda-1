/*******************************************
Purpose: To create Helper class for Publication Details
History:
Created by Ankit Bhagat on 30/10/2018
*******************************************/
@SuppressWarnings('PMD.ApexCRUDViolation')
public with Sharing class ViewMyPublicationHelper {
    
    /*****************************************************************************************
// Purpose      :  Viewing Publication Wrapper List method
// Developer    :  Ankit Bhagat 
// Created Date :  30/10/2018                 
//************************************************************************************/ 
    
    public static final string NO_ACCESS_ROLE = label.RSTP_Role_No_Access;
    
    
    public static PublicationDetailsWrapper.PublicationDetailsWithTotalsWrapper getPublicationWrapperList(){ 
        
        Set<Id> publicationIdSet = new Set<Id>();
        
        List<Researcher_Member_Sharing__c> reseacherMemberSharingList = new List<Researcher_Member_Sharing__c>();
        PublicationDetailsWrapper.PublicationDetailsWithTotalsWrapper publicationwrapper= new PublicationDetailsWrapper.PublicationDetailsWithTotalsWrapper();
        List<Publication__c> publicationList = new List<Publication__c>();
        List<PublicationDetailsWrapper.publicationWrapper> publicationwrapperList =  new List<PublicationDetailsWrapper.publicationWrapper>();
        
        Id publicationRecordTypeId = Schema.SObjectType.Researcher_Member_Sharing__c.getRecordTypeInfosByName().get(Label.RSTP_PublicationRecordtype).getRecordTypeId();
        
        User userdeatails = SobjectUtils.fetchUser();  
        
        
        Set<Id> recordTypeIdSet = new Set<Id> {publicationRecordTypeId}; //Changed variable name from RecordTypeIdSet to recordTypeIdSet for PMD Fix
            reseacherMemberSharingList = ResearcherMemberSharingUtils.geResearcherMemberSharingList(recordTypeIdSet, userdeatails.Id); //Changed variable name from RecordTypeIdSet to recordTypeIdSet for PMD Fix
        	System.debug('@@@reseacherMemberSharingList'+reseacherMemberSharingList);
        for(Researcher_Member_Sharing__c eachmemberSharing :reseacherMemberSharingList){
            publicationIdSet.add(eachmemberSharing.Publication__c);
        }
        System.debug('@@@publicationIdSet'+publicationIdSet);
        
        publicationList = [Select id, Year_Published__c, Publication_Category__c, Publication_Title__c,
                           Outlet__c, Editors__c, Audit_Result__c ,Description__c,Keywords__c,Publication_Ecode__c,Edition__c,
                           Creative_Work_Type__c,Medium__c,ISSN__c,Publisher__c,Place_Published__c,Conference_Name__c,Copyright__c,Repository_Deposited_Date__c,
                           Conference_Location__c,Event_Date__c,SJR_Percentile__c,Volume__c,Issue_Number__c,Start_Page__c,End_Page__c,Page_Total__c,
                           Creative_Work_Extent__c,Scimago_Journal_Quartile__c,Digital_Object_Identifier__c,Scopus_Identifier__c,Web_of_Science_Identifier__c,
                           (Select id, User__c, User__r.name,DisplayOrder__c,Position__c,Personnel_Type__c From Researcher_Portal_Member_Sharings__r ORDER BY DisplayOrder__c asc)
                           From Publication__c 
                           where ID IN:publicationIdSet
                           ORDER BY Year_Published__c DESC , Publication_Category__c ASC]; 
        
        System.debug('@@@publicationList'+publicationList);
        
        integer countpublication = publicationList.size();
        
        //if(publicationList!= null && !publicationList.isEmpty())
        //{
        //publicationwrapper = new PublicationDetailsWrapper.PublicationDetailsWithTotalsWrapper();
        for(Publication__c eachPublication : publicationList){
            
            String authors = '';
            PublicationDetailsWrapper.publicationWrapper wrapper = new PublicationDetailsWrapper.publicationWrapper(eachPublication);
            
            for(Researcher_Member_Sharing__c memberSharing : eachPublication.Researcher_Portal_Member_Sharings__r){
                if(memberSharing.User__c != null){
                    authors+= memberSharing.User__r.name + ', ';
                }
            }
            wrapper.authors = authors.removeEnd(', '); 
            publicationwrapperList.add(wrapper);                
        }
        //}         
        publicationwrapper.totalPublications = countpublication;
        publicationwrapper.publicationWrapperList = publicationwrapperList; 
		
        
        System.debug('@@@publicationwrapper'+publicationwrapper);
        
        return publicationwrapper;
    }//getPublicationWrapperList ends
    
    
    /*****************************************************************************************
// Purpose      :  Viewing Publication details page
// Developer    :  Md Ali   
// Created Date :  19/02/2019                 
//************************************************************************************/ 
    
    
    public static PublicationDetailsWrapper.publicationWrapper getPublicationDetailsHelper(id publicationId){
        
        List<publication__c> pubList = new List<publication__c>();
        List<PublicationDetailsWrapper.publicationWrapper> publicationwrapperList =  new List<PublicationDetailsWrapper.publicationWrapper>();
        
        PublicationDetailsWrapper.publicationWrapper publicationData = new PublicationDetailsWrapper.publicationWrapper();
        if(!Schema.sObjectType.Publication__c.fields.Publication_Title__c.isAccessible() && !Schema.sObjectType.Publication_Classification__c.fields.Publication__c.isAccessible()){
            return null;
        }
        pubList=[SELECT id,Publication_Title__c,Description__c,Keywords__c,Year_Published__c,Publication_Category__c,Outlet__c,Audit_Result__c,Edition__c,
                 Publication_Ecode__c,Creative_Work_Type__c,Medium__c,ISSN__c,Publisher__c,Place_Published__c,Conference_Name__c,Copyright__c,
                 Conference_Location__c,Event_Date__c,Editors__c,Volume__c,SJR_Percentile__c,Repository_Deposited_Date__c,Issue_Number__c,Start_Page__c,End_Page__c,Page_Total__c,
                 Creative_Work_Extent__c,Scimago_Journal_Quartile__c,Digital_Object_Identifier__c,Scopus_Identifier__c,Web_of_Science_Identifier__c,
                 (SELECT id,For_Six_Digit_Detail__c FROM Publication_Classifications__r ),(Select id, User__c, User__r.name , DisplayOrder__c,Position__c,Personnel_Type__c From Researcher_Portal_Member_Sharings__r ORDER BY DisplayOrder__c asc)
				 FROM Publication__c WHERE id=: PublicationId ];
        system.debug('@@pubList'+pubList);
        
        for(Publication__c eachrecord :pubList){
            PublicationDetailsWrapper.publicationWrapper wrapper = new PublicationDetailsWrapper.publicationWrapper(eachrecord);
            
            publicationwrapperList.add(wrapper);
        }
        
        publicationData = new PublicationDetailsWrapper.publicationWrapper(pubList[0]);
        publicationData.forcodes = fetchFORCodes(pubList[0]);
 
               
        system.debug('@@@publicationData'+publicationData);
        return publicationData;
        
    }//getPublicationDetailsHelper ends
    
    /*****************************************************************************************
// Purpose      :  Getting FORcodes from publication classification 
// Developer    :  Md Ali   
// Created Date :  19/02/2019                 
//************************************************************************************/
    public static String fetchFORCodes(Publication__c pub){
        
        String forcodesData='';
        
        for(Publication_Classification__c publicationclass :pub.Publication_Classifications__r){
            if(publicationclass.For_Six_Digit_Detail__c !=null){
                forcodesData +=  publicationclass.For_Six_Digit_Detail__c + ';' ;
            }    
        }
        
        forcodesData = forcodesData.removeEnd(';');
        
        system.debug('@@@forcodesData'+forcodesData);
        return forcodesData;
        
    }//fetchFORCodes ends

    
}