/*****************************************************************
Name: CreateAddressWrapperForInvoice
Author: Capgemini [Shreya]
Purpose: Handles creating the address wrapper to be refereed while generating the invoice pdf
Jira Reference : ECB-3900,ECB-5545
*****************************************************************/
/*==================================================================
History
--------
Version   Author            Date              Detail
1.0       Shreya           08/05/2019         Initial Version
********************************************************************/
public without sharing class CreateAddressWrapperForInvoice{

    //Creating the address wrapper to be referred in invoice pdf
    public static AddressWrapper createAddressWrapper(List<csord__Service__c> serviceList){
        
        AddressWrapper addWrap = new AddressWrapper();
        if(String.isNotBlank(serviceList[0].csordtelcoa__Main_Contact__r.hed__Current_Address__r.hed__MailingStreet__c)){
            addWrap.street = serviceList[0].csordtelcoa__Main_Contact__r.hed__Current_Address__r.hed__MailingStreet__c + ' ';
        }
        if(String.isNotBlank(serviceList[0].csordtelcoa__Main_Contact__r.hed__Current_Address__r.hed__MailingStreet2__c)){
            addWrap.street += serviceList[0].csordtelcoa__Main_Contact__r.hed__Current_Address__r.hed__MailingStreet2__c + ' ';
        }
        if(String.isNotBlank(serviceList[0].csordtelcoa__Main_Contact__r.hed__Current_Address__r.hed__MailingCity__c)){
            addWrap.cityStatePOCode = serviceList[0].csordtelcoa__Main_Contact__r.hed__Current_Address__r.hed__MailingCity__c + ' ';
        }   
        if(String.isNotBlank(serviceList[0].csordtelcoa__Main_Contact__r.hed__Current_Address__r.hed__MailingState__c)){
            addWrap.cityStatePOCode += serviceList[0].csordtelcoa__Main_Contact__r.hed__Current_Address__r.hed__MailingState__c + ' ';
        }   
        if(String.isNotBlank(serviceList[0].csordtelcoa__Main_Contact__r.hed__Current_Address__r.hed__MailingPostalCode__c)){
            addWrap.cityStatePOCode += serviceList[0].csordtelcoa__Main_Contact__r.hed__Current_Address__r.hed__MailingPostalCode__c + ' ';
        }   
        if(String.isNotBlank(serviceList[0].csordtelcoa__Main_Contact__r.hed__Current_Address__r.hed__MailingCountry__c)){
            addWrap.country = serviceList[0].csordtelcoa__Main_Contact__r.hed__Current_Address__r.hed__MailingCountry__c + ' ';
        } 
               
        addWrap.prodConfigIdString = createProductConfigString(serviceList,addWrap);
        return  addWrap;    
    }
    
        
   private static String createProductConfigString(List<csord__Service__c> serviceList,  AddressWrapper addWrap){
        addWrap.prodConfigIdString = '';
        for (csord__Service__c ser: serviceList) {

            if (ser.csordtelcoa__Product_Configuration__r.Id != null) {
                addWrap.prodConfigIdString = addWrap.prodConfigIdString + ser.csordtelcoa__Product_Configuration__r.Id + ',';

            }
        }
        addWrap.prodConfigIdString.removeEnd(',');
        return addWrap.prodConfigIdString;
    
   }
   
    public class AddressWrapper {
        public String prodConfigIdString;      
        public String street;        
        public String cityStatePOCode;
        public String country;
        
    }
    
}