/*********************************************************************************
*** @ClassName         : ActivatorLeadAutoConvert_Scheduler
*** @Author            : Shubham Singh 
*** @Requirement       : Schedule this class to run Batch_ActivatorLeadAutoConvert class and update
						 or convert the lead
*** @Created date      : 2019-05-12
*** @Reference		   : RM-2720
*** @Modified by       : Shubham Singh
*** @Modified Date     : -
**********************************************************************************/
public with sharing class ActivatorLeadAutoConvert_Scheduler implements Schedulable  {
	 public void execute(SchedulableContext ctx){
        Batch_ActivatorLeadAutoConvert batchLeadAutoConvert = new Batch_ActivatorLeadAutoConvert();
        Database.executeBatch(batchLeadAutoConvert,200);
    }
}