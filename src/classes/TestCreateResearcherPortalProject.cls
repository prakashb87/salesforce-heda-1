/*******************************************
Purpose: Test class for CreateResearcherPortalProject
History:
Created by Ankit Bhagat on 21/09/2018
*******************************************/
@isTest
public class TestCreateResearcherPortalProject {

    
    /************************************************************************************
    // Purpose      :  Creating Test data for Community User for all test methods 
    // Developer    :  Ankit
    // Created Date :  10/24/2018                 
    //***********************************************************************************/
    @testSetup static void testDataSetup() {
        
        RSTP_TestDataFactoryUtils.createNonCommunityUsersWithProfile(1,'System Administrator','Activator');
        
        User adminUser =[SELECT Id,ContactId FROM User WHERE UserName='test.nonCommunityUser1@test.com.testDev' LIMIT 1];
        System.runAs(adminUser)   
        {
            Map<String,Integer> allnumbersOfDataMap = new Map<String,Integer>();
            allnumbersOfDataMap.put('user',1);
            allnumbersOfDataMap.put('account',1);
            allnumbersOfDataMap.put('contact',1);
            
            Map<String,String> requiredInfoMap = new Map<String,String>();
            requiredInfoMap.put('userType','CommunityUsers');
            requiredInfoMap.put('profile','RMIT Researcher Portal Plus Community User');
            
            Boolean userCreatedFlag = RSTP_TestDataFactoryUtils.testRSTPDataSetup(allnumbersOfDataMap,requiredInfoMap);
            System.assert(userCreatedFlag==true,'Community User Created'); 
        }
        
    }    
    
    /************************************************************************************
    // Purpose      :  Test functionality for Create ResearcherPortal Project Helper
    // Developer    :  Ankit
    // Created Date :  10/24/2018                 
    //***********************************************************************************/
    @isTest    
    public static void testcreateResearcherProjectHelperPositiveMethod(){

        User usr =[SELECT Id,ContactId FROM User WHERE UserName='test.communityUser1@test.com.testDev' LIMIT 1];

        system.runAs(usr)
        {
        CreateProjectHelper.projectDetailsWrapper  wrapperDetails = new  CreateProjectHelper.projectDetailsWrapper();
        wrapperDetails.projectTitle = 'newtest';
        wrapperDetails.status       = 'Idea';
        wrapperDetails.summary      = 'abc' ;
        wrapperDetails.startDate    = date.newInstance(2018, 09, 21) ;
        wrapperDetails.endDate      = date.newInstance(2018, 12, 14);
        wrapperDetails.access       ='Private';
        system.assert( wrapperDetails.status == 'Idea');
        
        List<String> impactCategorytest = new List<String>();
        impactCategorytest.add('Social');
        impactCategorytest.add('Environmental');
        impactCategorytest.add('Cultural');
        wrapperDetails.impactCategory = impactCategorytest ;
        system.assert(impactCategorytest.size() > 2);
        
        List<String> forCodestest     = new List<String>();
        forCodestest.add('0101 - PURE MATHEMATICS');
        forCodestest.add('0102 - APPLIED MATHEMATICS');
        forCodestest.add('0104 - STATISTICS');
        wrapperDetails.forCodes         = forCodestest;
        system.assert(forCodestest.size() > 2);
        
        wrapperDetails.approachedImpact = 'new2'; 
        system.assert( wrapperDetails.approachedImpact == 'new2');
            
        String projectRecordTypeId=Schema.SObjectType.ResearcherPortalProject__c.getRecordTypeInfosByName().get(Label.RSTP_ResearchProjectRPPRecordtype).getRecordTypeId();
        String access = 'Private';
        List<ResearcherPortalProject__c> projectObjList = RSTP_TestDataFactoryUtils.createResearchProj(1, projectRecordTypeId, access);
        system.assert(projectObjList.size() > 0);
            
        String serailizedString = JSON.serialize(wrapperDetails); 
        
        CreateProjectHelper.createResearcherProject(serailizedString,projectObjList[0].id);
        CreateResearcherPortalProjectController.createProject(serailizedString, projectObjList[0].id);
        Map<String,String> fieldsApiNamesMap = CreateResearcherPortalProjectController.getfieldsAPINames();
        system.assert(fieldsApiNamesMap!=null,'Records Created');
    
        Map<String,String> fieldsObjectNamesMap = CreateResearcherPortalProjectController.getObjectsAPINames();
        system.assert(fieldsObjectNamesMap!=null,'Records Created');
            
        List <String> selectOptionList = CreateResearcherPortalProjectController.getselectOptions('ResearcherPortalProject','Category');
        system.assert(selectOptionList!=null,'Records Created');
        CreateResearcherPortalProjectController.getProjectDetails(projectObjList[0].id); 
        }
    }
    
      /************************************************************************************
    // Purpose      :  Test functionality of negative case for Create ResearcherPortal Project Helper
    // Developer    :  Ali
    // Created Date :  11/23/2018                 
    //***********************************************************************************/
    @isTest    
    public static void testcreateResearcherProjectHelperNegativeMethod(){

        User usr =[SELECT Id,ContactId FROM User WHERE UserName='test.communityUser1@test.com.testDev' LIMIT 1];

        system.runAs(usr)
        {
      /*  Map<String,String> parameterList = new Map<String,String>();
        parameterlist.put('businessFunctionName','create Project');
        parameterlist.put('isIntegation','No');*/
        Boolean result = false;
        Contact con =[SELECT Id,Firstname,lastname FROM contact WHERE LastName='Contact1' LIMIT 1 ];
        con.hed__UniversityEmail__c='raj@rmit.edu.au';
        con.lastname='';
        //update con; 
         try
            {
                update con;    
            }
            catch(DmlException ex)
            {
                result =true;   
                //ResearcherExceptionHandlingUtil.addExceptionLog(ex,parameterList);
        }
            system.assert(result);
        CreateProjectHelper.projectDetailsWrapper  wrapperDetails = new  CreateProjectHelper.projectDetailsWrapper();
        wrapperDetails.projectTitle = 'newtest';
     
        List<String> impactCategorytest = new List<String>();
        impactCategorytest.add('');
         try
            {
                wrapperDetails.impactCategory = impactCategorytest ;   
            }
            catch(DmlException ex)
            {
                result =true;                   
               
            }
            system.assert(result);
    
        List<String> forCodestest     = new List<String>();
        forCodestest.add('');
    
        wrapperDetails.approachedImpact = 'new2'; 
        system.assert( wrapperDetails.approachedImpact == 'new2');
            
        String projectRecordTypeId=Schema.SObjectType.ResearcherPortalProject__c.getRecordTypeInfosByName().get(Label.RSTP_ResearchProjectRMRecordtype).getRecordTypeId();
        String access = 'Private';
        List<ResearcherPortalProject__c> projectObjList = RSTP_TestDataFactoryUtils.createResearchProj(1, projectRecordTypeId, access);
        system.assert(projectObjList.size() > 0);
            
        String serailizedString = JSON.serialize(wrapperDetails); 
        
        //use case covering exception block
        CreateResearcherPortalProjectController.createProject(null, null);
        
        Map<String,String> fieldsApiNamesMap = CreateResearcherPortalProjectController.getfieldsAPINames();
        system.assert(fieldsApiNamesMap!=null,'Records Created');
    
        Map<String,String> fieldsObjectNamesMap = CreateResearcherPortalProjectController.getObjectsAPINames();
        system.assert(fieldsObjectNamesMap!=null,'Records Created');
            
        List <String> selectOptionList = CreateResearcherPortalProjectController.getselectOptions('Account','industry');
        system.assert(selectOptionList!=null,'Records Created');
        //use case covering exception block
        CreateResearcherPortalProjectController.getProjectDetails(projectObjList[0].id); 
        }
    }
   @isTest
    public static void testExceptionHandlingMethod(){
        
        User u =[SELECT Id,ContactId FROM User WHERE UserName='test.communityUser1@test.com.testDev' LIMIT 1]; 
        
        System.runAs(u) {      
            
            String projectRecordTypeId=Schema.SObjectType.ResearcherPortalProject__c.getRecordTypeInfosByName().get(Label.RSTP_ResearchProjectRMRecordtype).getRecordTypeId();
            String access = 'Private';
            List<ResearcherPortalProject__c> projectObjList = RSTP_TestDataFactoryUtils.createResearchProj(1, projectRecordTypeId, access);
            system.assert(projectObjList.size() > 0);
            
            CreateResearcherPortalProjectController.isForceExceptionRequired=true;
            CreateResearcherPortalProjectController.getfieldsAPINames();
            CreateResearcherPortalProjectController.getObjectsAPINames();
            CreateResearcherPortalProjectController.getselectOptions('Account','industry');
            CreateResearcherPortalProjectController.getProjectDetails(projectObjList[0].id); 
            CreateResearcherPortalProjectController.createProject(null, null);
            
        }
    }
}