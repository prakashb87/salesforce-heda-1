@isTest
public class SearchAndReplace_TEST {
	static testMethod void testMethod1() 
    {	
        Account testAcc = new Account();
        testAcc.Name = 'TestClass_Batch';
        testAcc.RecordTypeId = Schema.SObjectType.Account.getRecordTypeInfosByName().get('Educational Institution').getRecordTypeId();
        Database.insert(testAcc);
        List<hed__Affiliation__c> affList= new List<hed__Affiliation__c>();
        for(Integer i=0 ;i <50;i++)
        {
            hed__Affiliation__c aff = new hed__Affiliation__c();
            aff.hed__Account__c = testAcc.Id;
            affList.add(aff);
        }
         
        insert affList;
        
        Test.startTest();
            SearchAndReplace obj = new SearchAndReplace(); 
            DataBase.executeBatch(obj); 
        Test.stopTest();
        /*List <hed__Affiliation__c> aff23 = [SELECT hed__Role__c FROM hed__Affiliation__c];
        for(hed__Affiliation__c aff45: aff23){
        	System.assertEquals('Staff', aff45.hed__Role__c);
        }*/
        
    }
}