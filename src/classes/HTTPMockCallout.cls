/*****************************************************************
Name: HTTPMockCallout
Author: Capgemini[Shreya]
Purpose: Test Class For CourseFullDataLoad
*****************************************************************/
/*==================================================================
History
--------
Version   Author                     Date              Detail
1.0       Shreya Barua            29/05/2018         Initial Version
********************************************************************/
@isTest
@SuppressWarnings('PMD.AvoidGlobalModifier')
global class HTTPMockCallout implements HttpCalloutMock {
    
    /********************************************************************
    // Purpose              : To create mock response for HTTP Callout                            
    // Author               : Capgemini [Shreya]
    // Parameters           : HTTPRequest req
    //  Returns             : HTTPResponse
    //JIRA Reference        : ECB-3247 - The Full load for the Event trigger not catered to
    //********************************************************************/ 
    global HTTPResponse respond(HTTPRequest req) {
        HttpResponse res = new HttpResponse();
        res.setHeader('Content-Type', 'application/json');
        string jsonResBody = '{ "Status": "Fail",  "StatusCode": "404"}';
        res.setBody(jsonResBody);
        res.setStatusCode(404);
        res.setStatus('Fail');
        return res;
    }
}