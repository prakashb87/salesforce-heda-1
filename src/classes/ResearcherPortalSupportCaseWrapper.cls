/*@Description : Wrapper class to case Support Case records 
  @Story : RPORW-1423
  @Author : Gourav Bhardwaj
*/
public with sharing class ResearcherPortalSupportCaseWrapper {
  
	    @AuraEnabled public String recordType;
	    @AuraEnabled public String subject;
	    @AuraEnabled public String description;
	    @AuraEnabled public String status;
	    @AuraEnabled public String researcherCaseTopic;
	    @AuraEnabled public String researcherCaseSubTopic;
	    @AuraEnabled public String researcherLocation;
	    @AuraEnabled public String projectQuery;
	   	@AuraEnabled public CaseJunctionObjects caseJunction;
   
    public class CaseJunctionObjects{
    	@AuraEnabled public List<Id> projectSelected;    
	    @AuraEnabled public List<Id> contractsSelected;   
	    @AuraEnabled public List<Id> ethicsSelected;      
	    @AuraEnabled public List<Id> milestonesSelected;  
	    @AuraEnabled public List<Id> publicationsSelected;
    }
    //CaseJunctionObjects Ends
}
//ResearcherPortalSupportCaseWrapper Ends