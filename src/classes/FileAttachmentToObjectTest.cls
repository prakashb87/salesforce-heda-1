/*********************************************************************************
*** @TestClassName     : FileAttachmentToObjectTest
*** @Author            : Dinesh Kumar 
*** @Requirement       : To Test the FileAttachmentToObject apex class.
*** @Created date      : 01/03/2019
*** @Modified by       : Dinesh Kumar 
*** @modified date     : 01/03/2019
**********************************************************************************/

@isTest
public class FileAttachmentToObjectTest {
    
    public static testMethod void testCreateFileForSObject() {
        
        Account acc = new Account();
        acc.Name = 'Test_Account_Name';
        insert acc;
        
        string fileName = 'Test_File_Name';
        Id parentId = acc.Id;
        Blob fileContent = Blob.valueOf('This is test blob content');
        

   		System.assertEquals(acc.Name, 'Test_Account_Name');
        
        FileAttachmentToObject.createFileForSObject(fileName, parentId, fileContent);
    }
    
    public static testMethod void testCreateFileForSObjectNegative() {
        
        Account acc = new Account();
        acc.Name = 'Test_Account_Name';
        insert acc;
        
        string fileName = '';
        Id parentId = acc.Id;
        Blob fileContent = Blob.valueOf('This is test blob content');
        
        System.assertEquals(acc.Name, 'Test_Account_Name');
        
        FileAttachmentToObject.createFileForSObject(fileName, parentId, fileContent);
    }
}