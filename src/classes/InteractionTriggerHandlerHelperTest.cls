@isTest
private class InteractionTriggerHandlerHelperTest {
    @testSetup
    static void testdataSetup(){
        list<Interaction_Mapping__c> intMapping = InteractionTestDataFactory.createInteractionmappingsTriggerHandlers();
         //creating TDTM Records
          List<hed__Trigger_Handler__c> trgH= InteractionTestDataFactory.createTriggerHandlerData();
          insert trgH;
      
        
    }
	private static testMethod void createInteractioOnlyBulkTest() {
	    Id acadRT = Schema.SObjectType.Account.RecordTypeInfosByName.get('Academic Institution').RecordTypeId;
        Id agentRT = Schema.SObjectType.Account.RecordTypeInfosByName.get('Agent').RecordTypeId;
        Id secondRT = Schema.SObjectType.Account.RecordTypeInfosByName.get('Secondary School').RecordTypeId;
        Id adminRT = Schema.SObjectType.Account.RecordTypeInfosByName.get('Administrative').RecordTypeId;
        Id intRT = Schema.SObjectType.Interaction__c.RecordTypeInfosByName.get('Prospective Student').RecordTypeId;
        Account acc1 = new Account(Name = 'Academic Inst Test', RecordTypeId = acadRT);
        insert acc1;
        Account acc2 = new Account(Name = 'Agent Test', RecordTypeId = agentRT);
        insert acc2;
        Account acc3 = new Account(Name = 'Secondary Inst Test', RecordTypeId = secondRT);
        insert acc3;
        List<Interaction__c> interactions = new List<Interaction__c> ();
	    for(integer i=0;i<10;i++){
	        Interaction__c intRec= new Interaction__c(RecordTypeId=intRT,
	            First_Name__c='JKLGHS' +i,
	            Last_Name__c='LASGTXVS' +i,
	            Email__c='JKLGHS' +i + '.'  + 'LASGTXVS' +i + '@emaildomain.com' ,
	            Mobile_Phone__c='04012233' + i,
	            Admit_Term__c='2050',
	            Start_Semester__c='Semester 1, 2021',
	            Education_Institution__c=acc1.Id,
	            AgentSponsor__c=acc2.Id,
	            Secondary_School__c=acc3.Id);
	            interactions.add(intRec);
	            
	    }
	    Test.startTest();
	    insert interactions;
	    Test.stopTest();
	    List<Contact> contactList= [SELECT ID FROM Contact WHERE FirstName like 'JKLGHS%'];
	    System.assert(contactList.size()==10,'The number of expected contact that should exist is ' + contactList.size());

	}
	private static testMethod void createInteractionWithDuplicateLeadsTest() {
		Id acadRT = Schema.SObjectType.Account.RecordTypeInfosByName.get('Academic Institution').RecordTypeId;
        Id agentRT = Schema.SObjectType.Account.RecordTypeInfosByName.get('Agent').RecordTypeId;
        Id secondRT = Schema.SObjectType.Account.RecordTypeInfosByName.get('Secondary School').RecordTypeId;
        Id adminRT = Schema.SObjectType.Account.RecordTypeInfosByName.get('Administrative').RecordTypeId;
        Id intRT = Schema.SObjectType.Interaction__c.RecordTypeInfosByName.get('Prospective Student').RecordTypeId;
        Id leadRT = Schema.SObjectType.Lead.RecordTypeInfosByName.get('Prospective Student').RecordTypeId;
        Account acc1 = new Account(Name = 'Academic Inst Test', RecordTypeId = acadRT);
        insert acc1;
        Account acc2 = new Account(Name = 'Agent Test', RecordTypeId = agentRT);
        insert acc2;
        Account acc3 = new Account(Name = 'Secondary Inst Test', RecordTypeId = secondRT);
        insert acc3;
        List<Interaction__c> interactions = new List<Interaction__c> ();
        Lead leadRecord= new Lead(FirstName='FirstNameXXX',
                                    LastName='LastNameDefault1',
                                    Email='LastNameDefault1.FirstNameXXX@emaildomain.com',
                                    MobilePhone='040122431',
                                    RecordTypeId=leadRT,
                                    Company='.');
             insert LeadRecord;
        
        
	    for(integer i=0;i<10;i++){
	        Interaction__c intRec= new Interaction__c(RecordTypeId=intRT,
	            First_Name__c='FirstNameXXX' +i,
	            Last_Name__c='LastNameDefault' +i,
	            Email__c='LastNameDefault' +i + '.'  + 'FirstNameXXX' +i + '@emaildomain.com' ,
	            Mobile_Phone__c='04012243' + i,
	            Admit_Term__c='2050',
	            Start_Semester__c='Semester 1, 2021',
	            Education_Institution__c=acc1.Id,
	            AgentSponsor__c=acc2.Id,
	            Secondary_School__c=acc3.Id);
	            interactions.add(intRec);
	            
	            
	    }
	    Test.startTest();
	    insert interactions;
	    Test.stopTest();
	     List<Contact> contactList= [SELECT ID FROM Contact WHERE FirstName like '%FirstNameXXX%'];
	    System.assert(contactList.size()==10,'The number of expected contact that should exist is ' + contactList.size());
	}
		private static testMethod void createInteractionWithDuplicateContactTest() {
		Id acadRT = Schema.SObjectType.Account.RecordTypeInfosByName.get('Academic Institution').RecordTypeId;
        Id agentRT = Schema.SObjectType.Account.RecordTypeInfosByName.get('Agent').RecordTypeId;
        Id secondRT = Schema.SObjectType.Account.RecordTypeInfosByName.get('Secondary School').RecordTypeId;
        Id adminRT = Schema.SObjectType.Account.RecordTypeInfosByName.get('Administrative').RecordTypeId;
        Id intRT = Schema.SObjectType.Interaction__c.RecordTypeInfosByName.get('Prospective Student').RecordTypeId;
        Id leadRT = Schema.SObjectType.Lead.RecordTypeInfosByName.get('Prospective Student').RecordTypeId;
        Account acc1 = new Account(Name = 'Academic Inst Test', RecordTypeId = acadRT);
        insert acc1;
        Account acc2 = new Account(Name = 'Agent Test', RecordTypeId = agentRT);
        insert acc2;
        Account acc3 = new Account(Name = 'Secondary Inst Test', RecordTypeId = secondRT);
        insert acc3;
        List<Interaction__c> interactions = new List<Interaction__c> ();
        Contact con= new Contact(FirstName='FirstNameDefaultx1',
                                    LastName='LastNameDefaultx1',
                                    hed__WorkEmail__c='LastNameDefaultx1.FirstNameDefault1x@emaildomain.com',
                                    MobilePhone='040122531');
        insert con;
        
        
	    for(integer i=0;i<10;i++){
	        Interaction__c intRec= new Interaction__c(RecordTypeId=intRT,
	            First_Name__c='FirstNameDefaultx' +i,
	            Last_Name__c='LastNameDefaultx' +i,
	            Email__c='LastNameDefault' +i + '.'  + 'FirstNameDefault' +i + '@emaildomain.com' ,
	            Mobile_Phone__c='04012253' + i,
	            Admit_Term__c='2050',
	            Start_Semester__c='Semester 1, 2021',
	            Education_Institution__c=acc1.Id,
	            AgentSponsor__c=acc2.Id,
	            Secondary_School__c=acc3.Id);
	            interactions.add(intRec);
	         	            
	    }
	    test.startTest();
	    insert interactions;
	    test.stopTest();
	    List<Contact> contactList= [SELECT ID FROM Contact WHERE FirstName like '%FirstNameDefaultx%'];
	    System.assert(contactList.size()==10,'The number of expected contact that should exist is ' + contactList.size());
	    map<Id,Contact> mapCon= new map<Id,Contact>(contactList);
	    List<Opportunity> opplist=[SELECT ID FROM Opportunity WHERE Assign_to_Contact__c IN: mapCon.KeySet()];
	    System.assert(opplist.size()==10,'The number of expected opportunity that should exist is ' + oppList.size());
	    
	}
	private static testMethod void createInteractionWithDuplicateContactLeadsTest() {
		Id acadRT = Schema.SObjectType.Account.RecordTypeInfosByName.get('Academic Institution').RecordTypeId;
        Id agentRT = Schema.SObjectType.Account.RecordTypeInfosByName.get('Agent').RecordTypeId;
        Id secondRT = Schema.SObjectType.Account.RecordTypeInfosByName.get('Secondary School').RecordTypeId;
        Id adminRT = Schema.SObjectType.Account.RecordTypeInfosByName.get('Administrative').RecordTypeId;
        Id intRT = Schema.SObjectType.Interaction__c.RecordTypeInfosByName.get('Prospective Student').RecordTypeId;
        Id leadRT = Schema.SObjectType.Lead.RecordTypeInfosByName.get('Prospective Student').RecordTypeId;
		
		 Account acc1 = new Account(Name = 'Academic Inst Test', RecordTypeId = acadRT);
         insert acc1;
         Account acc2 = new Account(Name = 'Agent Test', RecordTypeId = agentRT);
	        insert acc2;
	        Account acc3 = new Account(Name = 'Secondary Inst Test', RecordTypeId = secondRT);
	        insert acc3;
		  Lead leadRecord= new Lead(FirstName='dupleadTest1',
                                    LastName='dupleadTest1',
                                    Email='dupleadTest1.dupleadTest1@emaildomain.com',
                                    MobilePhone='040122439',
                                    RecordTypeId=leadRT,
                                    Company='.');
             insert LeadRecord;
           Contact con= new Contact(FirstName='dupleadTest1',
                                    LastName='dupleadTest1',
                                    hed__WorkEmail__c='dupleadTest1.dupleadTest1@emaildomain.com',
                                    MobilePhone='0401224391');
        insert con;
         List<Interaction__c> interactions = new List<Interaction__c> ();
        for(integer i=0;i<10;i++){
	        Interaction__c intRec= new Interaction__c(RecordTypeId=intRT,
	            First_Name__c='dupleadTest' +i,
	            Last_Name__c='dupleadTest' +i,
	            Email__c='dupleadTest' +i + '.'  + 'dupleadTest' +i + '@emaildomain.com' ,
	            Mobile_Phone__c='040122439' + i,
	            Admit_Term__c='2050',
	            Start_Semester__c='Semester 1, 2021',
	            Education_Institution__c=acc1.Id,
	            AgentSponsor__c=acc2.Id,
	            Secondary_School__c=acc3.Id);
	            interactions.add(intRec);
	           
	    }
	    test.startTest();
	    insert interactions;
	    test.stopTest();
	    List<Contact> contactList= [SELECT ID FROM Contact WHERE FirstName like '%dupleadTest%'];
	    System.assert(contactList.size()==10,'The number of expected contact that should exist is ' + contactList.size());
	    map<Id,Contact> mapCon= new map<Id,Contact>(contactList);
	    List<Opportunity> opplist=[SELECT ID FROM Opportunity WHERE Assign_to_Contact__c IN: mapCon.KeySet()];
	    System.assert(opplist.size()==10,'The number of expected opportunity that should exist is ' + oppList.size());
	}
	private static testMethod void multipleLeadsTestGMSR(){
	 Profile profileGMSRId = [SELECT Id FROM Profile WHERE Name = 'RMIT GMSR User' LIMIT 1];
	 User usr = new User(LastName = 'Test RMITGMSRUser',
                            FirstName='User',
                            Alias = 'asdfg',
                            Email = 'test.rmitGMSR@rmittest.com',
                            Username = 'test.rmitGMSR@rmittest.com.sandbox',
                            ProfileId = profileGMSRId.id,
                            TimeZoneSidKey = 'GMT',
                            LanguageLocaleKey = 'en_US',
                            EmailEncodingKey = 'UTF-8',
                            LocaleSidKey = 'en_US',
                            isActive=true
                           );
     insert usr;
     	id acadRT = Schema.SObjectType.Account.RecordTypeInfosByName.get('Academic Institution').RecordTypeId;
        Id agentRT = Schema.SObjectType.Account.RecordTypeInfosByName.get('Agent').RecordTypeId;
        Id secondRT = Schema.SObjectType.Account.RecordTypeInfosByName.get('Secondary School').RecordTypeId;
        Id adminRT = Schema.SObjectType.Account.RecordTypeInfosByName.get('Administrative').RecordTypeId;
        Id intRT = Schema.SObjectType.Interaction__c.RecordTypeInfosByName.get('Prospective Student').RecordTypeId;
        Id leadRT = Schema.SObjectType.Lead.RecordTypeInfosByName.get('Prospective Student').RecordTypeId;
		
		 	Account acc1 = new Account(Name = 'Academic Inst Test', RecordTypeId = acadRT);
         	insert acc1;
         	Account acc2 = new Account(Name = 'Agent Test', RecordTypeId = agentRT);
	        insert acc2;
	        Account acc3 = new Account(Name = 'Secondary Inst Test', RecordTypeId = secondRT);
	        insert acc3;
	        Account acc4 = new Account(Name = 'dupleadTest21 dupleadTest21 Administrative Account', RecordTypeId = adminRT);
	        insert acc4;
		  	Lead leadRecord1= new Lead(FirstName='dupleadTest21',
                                    LastName='dupleadTest21',
                                    Email='dupleadTest21.dupleadTest21@emaildomain.com',
                                    MobilePhone='04012243967',
                                    LeadSource='Phone',
                                    RecordTypeId=leadRT,
                                    Company='.',
                                    OwnerId=usr.id);
             insert LeadRecord1;
             Lead leadRecord2 = new Lead(FirstName='dupleadTest21',
                                    LastName='dupleadTest21',
                                    Email='dupleadTest21.dupleadTest21@emaildomain.com',
                                    MobilePhone='04012243967',
                                    LeadSource='Phone',
                                    RecordTypeId=leadRT,
                                    Company='.',
                                    OwnerId=usr.id);
            insert LeadRecord2;

        	test.startTest();
           	Interaction__c intRec= new Interaction__c(RecordTypeId=intRT,
	            First_Name__c='dupleadTest21',
	            Last_Name__c='dupleadTest21',
	            Email__c='dupleadTest21.dupleadTest21@emaildomain.com' ,
	            Mobile_Phone__c='04012243967',
	            Admit_Term__c='2050',
	            Start_Semester__c='Semester 1, 2021',
	            Education_Institution__c=acc1.Id,
	             AgentSponsor__c=acc2.Id,
	            Secondary_School__c=acc3.Id);
	            System.runAs(usr){
	            insert intRec;
	            }
	        test.stopTest();
	        List<Contact> contactList= [SELECT ID FROM Contact WHERE FirstName like '%dupleadTest%'];
	    	System.assert(contactList.size()==1,'The number of expected contact that should exist is ' + contactList.size());
	    	List<Lead> convertedLeads=[SELECT ID FROM Lead WHERE MobilePhone='04012243967' AND isConverted=true];
	    	System.assert(convertedLeads.size()==2,'The number of expected converted leads that should exist is ' + convertedLeads.size());
	
	}

}