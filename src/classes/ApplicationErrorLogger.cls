/**
 * @author Resmi Ramakrishnan                     
 * @date 04/06/2018                  
 * @description This Error handling utility class contains different methods for adding 
 * error records on Application Error Log Object                                         
 */
@SuppressWarnings('PMD.ApexCRUDViolation')
public without sharing class ApplicationErrorLogger {
       
    public static void logError(Course_Connection_Life_Cycle__c cclyf,String errorType,Map<String, JsonCourseWrapper.IpaasResponseWrapper> ipaasResWrp) {
    	
    	Id recordID = cclyf.Course_Connection__r.id;
    	String errorPayload = string.valueof(ipaasResWrp.get(cclyf.Course_Connection__c));
    	String errorCode = ipaasResWrp.get(cclyf.Course_Connection__c).errorCode;
    	String errorMessage = ipaasResWrp.get(cclyf.Course_Connection__c).errorText;
    	system.debug('recordID--'+recordID+'errorPayload--'+errorPayload+'errorCode--'+errorCode+'errorMessage--'+errorMessage);
    	
        system.debug('ErrorLogger.logError entered');
        RMErrLog__c newError = New RMErrLog__c();
        newError.Record_Id__c = recordID;
        newError.Error_Type__c = errorType;
        newError.Error_Payload__c = errorPayload;
        newError.Error_Code__c = errorCode;
        newError.Error_Message__c = errorMessage;    

        try {
        			    			
            Insert newError;
            system.debug(newError);

        } 
        catch (Exception e) {
            system.debug('ErrorLogger.logError exception occured: ' + e.getMessage());
        }
        system.debug('ErrorLogger.logError exited');
    }
    
    public static void logError(Course_Connection_Life_Cycle__c cclyf,String errorType,Map<Id, CourseConnCanvasResponseWrapper.Response> canvasres) {
    	
    	Id recordID = cclyf.Course_Connection__r.id;
    	String errorPayload = string.valueof(canvasres.get(cclyf.Course_Connection__c));
    	String errorCode = canvasres.get(cclyf.Course_Connection__c).result;
    	String errorMessage = canvasres.get(cclyf.Course_Connection__c).errorMessage;
    	
    	system.debug('recordID--'+recordID+'errorPayload--'+errorPayload+'errorCode--'+errorCode+'errorMessage--'+errorMessage);
    	
        system.debug('ErrorLogger.logError entered');
        RMErrLog__c newError = New RMErrLog__c();
        newError.Record_Id__c = recordID;
        newError.Error_Type__c = errorType;
        newError.Error_Payload__c = errorPayload;
        newError.Error_Code__c = errorCode;
        newError.Error_Message__c = errorMessage;    

        try {
        						
            Insert newError;
            system.debug(newError);

        } 
        catch (Exception e) {
            system.debug('ErrorLogger.logError exception occured: ' + e.getMessage());
        }
        system.debug('ErrorLogger.logError exited');
    }
/*	//PMD: To Delete:
    public static RMErrLog__c logError(String userid, Exception ex, Exception errReason, 
            Integer errNum, String errMessage, Datetime errTime, String errType) {
    
        RMErrLog__c err = new RMErrLog__c();
        err.Error_Cause__c = String.valueOf(errReason);
        err.Error_Line_Number__c = errNum;
        err.Error_Message__c = errMessage;
        err.Error_Time__c = errTime;
        err.Error_Type__c = errType;
        try {
            insert err;
            if(Test.isRunningTest()) {
                throw new DMLException();
            }
        } catch(exception e) {
            System.debug('The following exception has occurred: ' + e.getMessage());
        }
        return err;
    }
*/

/*Commented Method as it is not used anywhere:
    public static void logError( String businessFunctionName, string className, string errorCause, 
            string errorCode, Integer errLineNumber, String errMessage, string errPayload, 
            string errType, Id recordId, Boolean isAdminnotified, Boolean isCaseCreated, 
            Boolean isIntegrationError,string methodName ) {
        RMErrLog__c errorObj = New RMErrLog__c();
        errorObj.Business_Function_Name__c = businessFunctionName;
        errorObj.Class_Name__c = className;
        errorObj.Error_Cause__c = errorCause;
        errorObj.Error_Code__c = errorCode;
        errorObj.Error_Line_Number__c = errLineNumber;
        errorObj.Error_Message__c = errMessage;
        errorObj.Error_Payload__c = errPayload;
        errorobj.Error_Type__c = errType;
        errorobj.Record_Id__c = recordId;
        errorobj.Is_Admin_Notified__c = isAdminnotified;
        errorobj.Is_Case_Created_c__c = isCaseCreated; 
        errorobj.Is_Integration_Error__c = isIntegrationError;
        errorobj.Logged_In_User__c = UserInfo.getuserid();
        errorobj.Method_Name__c = methodName;    
        try {
        	if (!Schema.sObjectType.RMErrLog__c.isCreateable()) {
				throw new DMLException();
			}
            Insert errorObj;
            system.debug(errorObj);
        } catch (Exception e) {
            system.debug('ErrorLogger.logError exception occured: ' + e.getMessage());
        }       
    }*/
     
    public static RMErrLog__c logError(String businessFunctionName, 
            String failedRecordId,String errorMessage ){
    
        RMErrLog__c err = new RMErrLog__c();
        err.Error_Type__c = 'Application';
        err.Business_Function_Name__c = businessFunctionName;
        err.Record_Id__c = failedRecordId;
        err.Error_Message__c = errorMessage;
        
        try {
        							
            insert err;            
            if(Test.isRunningTest()) {
                throw new DMLException();
            }
        } catch(exception e) {
            System.debug('The following exception has occurred: ' + e.getMessage());
        }
        return err;
    }

      public static RMErrLog__c logError(Exception ex)
      {
      	
      	//userinfo.getuserid(), e, e.getCause(), e.getLineNumber(), e.getMessage(), DateTime.now(), e.getTypeName()       
            RMErrLog__c err = new RMErrLog__c();
            err.Error_Cause__c = String.valueOf(ex.getCause());
            err.Error_Line_Number__c = ex.getLineNumber();
            err.Error_Message__c = ex.getMessage();
            err.Error_Time__c = DateTime.now();
            err.Error_Type__c = ex.getTypeName();
            err.Logged_In_User__c = UserInfo.getuserid();
            try
            {	        								
                insert err;
                if(Test.isRunningTest())
                {
                    throw new DMLException();
                }
            }
            catch(exception e)
            {
               System.debug('The following exception has occurred: ' + e.getMessage());
            }
            return err;
    }

}