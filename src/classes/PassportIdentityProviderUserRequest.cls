/*
 * @description Use this class to make requests to IDAM (Passport) for functionality 
 * related to user creation and existing IDAM users.
 * @group Identity Management
 */
 
/***************************************************************************************************
 * IMPORTANT: THIS API CALL CONTAINS SENSTIVE DATA
 * THE REQUEST OR PASSWORD SHOULD NOT BE LOGGED IN ANY CIRCUMSTANCES
 * DO NOT LOG REQUEST OR PASSWORD USING System.Debug OR TO ANY CUSTOM
 * OBJECT
 ***************************************************************************************************/

public without sharing class PassportIdentityProviderUserRequest implements IdentityProviderUserRequest
{
    
    private UserValidator validator;
    private String federationId;
    @TestVisible private String firstName;
    @TestVisible private String lastName;
    @TestVisible private String email;
    @TestVisible private String mobile;
    @TestVisible private String password;
    
    /**
     * @description Creates a new instance of the PassportIdentityProviderUserRequest class 
     * with the PassportIdentityUserValidator validator
     */
    public PassportIdentityProviderUserRequest() 
    {
       this.validator = new PassportIdentityUserValidator();
       this.mobile = '';
    }


    /**
     * @description Creates a new instance of the PassportIdentityProviderUserRequest class
     * @param validator The validator to be used to check user fields
     */
    public PassportIdentityProviderUserRequest(UserValidator validator) 
    {
        this.validator = validator;
        this.mobile = '';
    }
    
    
    /**
     * @description Sets the user's first name for this request
     * @param firstName The user's first name
     */
    public void setFirstName(String firstName) 
    {
        if (validator.isFirstNameValid(firstName)) 
        {
            this.firstName = firstName;
        } 
        else 
        {
            throw new ArgumentException('Invalid first name parameter.');
        }
    }
    
    
    /**
     * @description Sets the user's last name for this request
     * @param lastName The user's last name
     */
    public void setLastName(String lastName) 
    {
        if (validator.isLastNameValid(lastName)) 
        {
            this.lastName = lastName;
        } 
        else 
        {
            throw new ArgumentException('Invalid last name parameter.');
        }
    }
    
    
    /**
     * @description Sets the user's email for this request
     * @param email The user's email
     */
    public void setEmail(String email) 
    {
        if (validator.isEmailValid(email)) 
        {
            this.email = email;
        } 
        else 
        {
            throw new ArgumentException('Invalid email parameter.');
        }
    }
    
    
    /**
     * @description Sets the user's mobile for this request
     * @param mobile The user's mobile
     */
    public void setMobile(String mobile) 
    {
        if (validator.isPhoneNumberValid(mobile)) 
        {
            this.mobile = mobile;
        } 
        else 
        {
            throw new ArgumentException('Invalid mobile parameter.');
        }
    }
    

    /**
     * @description Sets the user's password for this request
     * @param password The user's password in plain text
     */
    public void setPassword(String password) 
    {
        //this.password = ''; // Blank for now due to API issues
        
        if (String.isNotBlank(password))
        {
            this.password = this.encryptUserPassword(password);
        }
        else 
        {
            throw new ArgumentException('Password not set.');
        }
        
    }

    
    /**
     * @description Creates a user in IDAM based on email address as a unique identifier.
     * Requires first name, last name and email to be set before this method is called
     */
    public void createUser() 
    {
        checkCreateUserRequiredState();
        
        UserCreationRequest requestStructure = new UserCreationRequest();
        requestStructure.firstName = this.firstName;
        requestStructure.lastName = this.lastName;
        requestStructure.email = this.email;
        requestStructure.mobile = this.mobile;
        requestStructure.password = this.password;
        
        String userCreationJSON = JSON.serialize(requestStructure);
        
        String endpoint = ConfigDataMapController.getCustomSettingValue('IDAMIpaaSEndPoint');
        HttpRequest req = new HttpRequest();
        req.setEndpoint(endpoint);
        req.setHeader('client_id', ConfigDataMapController.getCustomSettingValue('IDAMIpaaSClientID'));
        req.setHeader('client_secret', ConfigDataMapController.getCustomSettingValue('IDAMIpaaSClientSecret'));
        req.setHeader('Content-Type', 'application/json');
        req.setMethod('POST');
        req.setbody(userCreationJSON);
    
        Http http = new Http();
        HTTPResponse response = http.send(req);
        system.debug('user creation response=' + response.getBody());
        IDAMResponseWrapperForUserCreation userCreationResponse = IDAMResponseWrapperForUserCreation.parse(response.getBody());
        
        logIntegrationLog('*** SENSTIVE DATA LOG OMITTED ***',response);
        // IDAM uses a HTTP 500 Status if an attribute is missing or if a user already exists
        if (response.getStatusCode() != 200 && response.getStatusCode() != 500)
        {
            throw new HttpCalloutException('Request was not successful. Http Status Code: ' + response.getStatusCode());
        }
        if( userCreationResponse.payload != null )
        {
            if (userCreationResponse.payload.success && String.isNotBlank(userCreationResponse.payload.federationID))
            {
                this.federationId = userCreationResponse.payload.federationID;
            }
            if (userCreationResponse.payload.success == false)
            {
                throw new IdpUserCreationException('Unable to create user in IDAM.');
            }
        }
    }

    //Log integration Log 
    @testVisible
    private static void logIntegrationLog( String userCreationJSON, HTTPResponse response)
    {
        IntegrationLog requestLog = new IntegrationLog();
        requestLog.setRequestBody(userCreationJSON);
        requestLog.setType('IDAM_IPaaS_CreateUser');
        requestLog.setServiceType('Outbound Service');
        requestLog.log();

        IntegrationLog responseLog = new IntegrationLog();
        responseLog.setRequestBody(response.getBody());
        responseLog.setType('IDAM_IPaaS_CreateUser');
        responseLog.setServiceType('Acknowledgement');
        responseLog.log();
    }
    
    /**
     * @description Retrieves the Federation Id for the user created
     */
    public String getFederationId()
    {
        return this.federationId;
    }

    
    @testVisible
    private String encryptUserPassword(String password) 
    {
        /**********************************************************
         * IMPORTANT: DO NOT PUT ANY DEBUG LOGS OR RECORD PASSWORD
         * IN CUSTOM OBJECTS
         *********************************************************/
        String encryptedPassword = '';
        // encrypt the password as per IDAM requirements
        if( String.IsNotBlank(password))
        {
            Blob encryptIv = Blob.valueOf(ConfigDataMapController.getCustomSettingValue('IDAM_ENCRYPT_IV'));
            //Blob key = Crypto.generateAesKey(Integer.valueof(ConfigDataMapController.getCustomSettingValue('IDAM_ENCRYPT_KEY').trim()));
            Blob key = Blob.valueOf(ConfigDataMapController.getCustomSettingValue('IDAM_ENCRYPT_KEY'));
            Blob data = Blob.valueOf(password);
            String algorithm = ConfigDataMapController.getCustomSettingValue('IDAM_ENCRYPT_ALGORITHM');
            Blob encrypted = Crypto.encrypt(algorithm, key, encryptIv, data);
            encryptedPassword = EncodingUtil.base64Encode(encrypted );
        }
        return encryptedPassword;
    }

    
    @testVisible
    private void checkCreateUserRequiredState() 
    {
        if (String.isBlank(this.email) || String.isBlank(this.firstName) || 
                String.isBlank(this.lastName)) 
        {
            throw new IllegalStateException('First Name, Last Name, email has not been set.');
        }
    }


    private class UserCreationRequest 
    {
        public String firstname;
        public String lastname;
        public String email;
        public String mobile;
        public String password;
    }

}