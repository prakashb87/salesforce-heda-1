/*****************************************************************
Name: CustomCommunitesLoginControllerTest
Author: Capgemini [Rishabh]
Purpose: Test Class For CustomCommunitesLoginController
*****************************************************************/
/*==================================================================
History
--------
Version   Author                     Date              Detail
1.0       Rishabh Anand           27/07/2018         Initial Version
********************************************************************/
@isTest
public class CustomCommunitesLoginControllerTest {
    
    @isTest
    public static void redirectURLTest(){
        
        Test.startTest();
        
        TestDataFactoryUtil.dummycustomsetting();
        
        PageReference pg = Page.CustomCommunitesLogin;
		
        pg = CustomCommunitesLoginController.redirectURL();
      	
        System.assertNotEquals(null, pg);
        
        Test.stopTest();
        
    }
    
    
}