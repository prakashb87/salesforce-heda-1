/*******************************************
Purpose: Test class ApexWithoutSharingUtils
History:
Created by Subhajit on 27/10/2018
*******************************************/
@isTest
public class TestApexWithoutSharingUtils {
    
    
    /************************************************************************************
  // Purpose      :  Creating Test data for Community User for all test methods 
  // Developer    :  Subhajit
  // Created Date :  10/24/2018                 
  //***********************************************************************************/
    @testSetup static void testDataSetup() {
        
        RSTP_TestDataFactoryUtils.createNonCommunityUsersWithProfile(1,'System Administrator','Activator');
        
        User adminUser =[SELECT Id,ContactId FROM User WHERE UserName='test.nonCommunityUser1@test.com.testDev' LIMIT 1];
        System.runAs(adminUser)   
        {
            Map<String,Integer> allnumbersOfDataMap = new Map<String,Integer>();
            allnumbersOfDataMap.put('user',1);
            allnumbersOfDataMap.put('account',1);
            allnumbersOfDataMap.put('contact',1);
            
            Map<String,String> requiredInfoMap = new Map<String,String>();
            requiredInfoMap.put('userType','CommunityUsers');
            requiredInfoMap.put('profile',Label.testProfileName);
            
            Boolean userCreatedFlag = RSTP_TestDataFactoryUtils.testRSTPDataSetup(allnumbersOfDataMap,requiredInfoMap);
            System.assert(userCreatedFlag==true,'Community User Created'); 
        }
        
    }    
    /************************************************************************************
    // Purpose      :  Test functionality for searchContactResultsHelper
    // Developer    :  Subhajit
    // Created Date :  10/24/2018                 
    //***********************************************************************************/
    @isTest
    public static void testGSetSchoolNamebyContactIdMethod()
    {
        User usr =[SELECT Id,ContactId FROM User WHERE UserName='test.nonCommunityUser1@test.com.testDev' LIMIT 1];   
        
        system.runAs(usr)
        {
            
            Account academicAcct = new Account();
            academicAcct.Name ='Program school';
            insert academicAcct;
            System.assert(academicAcct!= null,'Record Created'); 
            
            
            Account acct =[SELECT Id, Name FROM Account WHERE Name ='TestAccount1' Limit 1];
            /* List<Account> accts =[SELECT Id, Name FROM Account];
            System.debug('@@@@accts==>'+JSON.serializePretty(accts));*/
            
            acct.Name='Program school';
            acct.Academic_Institution__c=academicAcct.id;
            update acct;
            System.assert(acct!= null,'Record Updated');
            contact testcon = new contact();
            testcon.accountid=acct.id;
            testcon.lastname= 'testlastname';
            insert testcon;
            
            /*Contact con = [SELECT Name,Contact_Roles__c, Current_Person_Type__c,hed__WorkEmail__c,
                           hed__Primary_Organization__c,Enumber__c,Student_ID__c
                           FROM Contact
                           WHERE AccountId =:acct.id Limit 1];*/
            
            testcon.hed__WorkEmail__c='test@rmit.edu.au'; 
            testcon.hed__Primary_Organization__c=acct.id; 
            testcon.Enumber__c='E502'; 
            testcon.Student_ID__c='3330112';
            update testcon;
            System.assert(testcon!= null,'Record Updated'); 
            
            hed__Affiliation__c affli = new hed__Affiliation__c();
            affli.hed__Contact__c=testcon.Id;       
            affli.hed__Account__c=acct.Id;
            affli.hed__Status__c='Current';
            affli.recordtypeid=Schema.SObjectType.hed__Affiliation__c.getRecordTypeInfosByName().get('Academic Staff').getRecordTypeId();
            insert affli;
            System.assert(affli!= null,'Record Created'); 
            
            hed__Program_Enrollment__c progEnroll= new hed__Program_Enrollment__c();
            progEnroll.hed__Contact__c=testcon.Id; 
            progEnroll.hed__Account__c=acct.Id;
            insert progEnroll;
            System.assert(progEnroll!= null,'Record Created'); 
            
            Set<Id> contactIdSet = new Set<Id>();
            contactIdSet.add(testcon.Id);
            map<id, string> mapContactIdSchoolName= ApexWithoutSharingUtils.getSchoolNamebyContactId(contactIdSet);
            System.assert(mapContactIdSchoolName!=null,'Record returned'); 
            
            
        }    
    }  
    /************************************************************************************
    // Purpose      :  Testing unit cases for PshareRMSrecords Utility
    // Developer    :  Subhajit
    // Created Date :  10/26/2018                 
    //***********************************************************************************/
    @isTest 
    public static void shareRMSrecordsTest(){
        
        
        List<Account> accts = RSTP_TestDataFactoryUtils.createAccounts(1);
        System.assert(accts!=null,'Account Created');
        List<Contact> cons = RSTP_TestDataFactoryUtils.createContactWithAccount(1,accts);
        System.assert(cons!=null,'Contacts Created');
        List<ResearcherPortalProject__c> projects=RSTP_TestDataFactoryUtils.createResearchProj(1, 
                                                                                               Schema.SObjectType.ResearcherPortalProject__c.getRecordTypeInfosByName().get(Label.RSTP_ResearchProjectRMRecordtype).getRecordTypeId(),
                                                                                               'Private');
        System.assert(projects!=null,'Projects Created');
        
        Researcher_Member_Sharing__c membersharing = new Researcher_Member_Sharing__c();
        membersharing.MemberRole__c = 'CI';
        membersharing.Contact__c = cons[0].Id;
        membersharing.Researcher_Portal_Project__c = projects[0].Id;
        membersharing.Primary_Contact_Flag__c = true  ;
        membersharing.RecordTypeId = Schema.SObjectType.Researcher_Member_Sharing__c.getRecordTypeInfosByName().get(Label.RSTP_ResearchProjectRecordtype).getRecordTypeId();  
        
        list<Researcher_Member_Sharing__c> memberSharingList = new list<Researcher_Member_Sharing__c>();
        memberSharingList.add(membersharing);
        
        
        list<Ethics_Researcher_Portal_Project__c> ethicsResearchList = new list<Ethics_Researcher_Portal_Project__c>();
        
        Researcher_Member_Sharing__Share rshare= ApexWithoutSharingUtils.shareRMSrecords(membersharing,Label.RSTP_Edit);
        System.assert(rshare!=null,'rshare returned');
        
        
        ApexWithoutSharingUtils.deleteEthicsProjectRecords(ethicsResearchList);
        Map<String,String> getQueueAndEmailMap = ApexWithoutSharingUtils.getQueueIdAndEmailAddress ('Finding Funding','Internal ECP schemes offered by RMIT University','Researcher Portal Support Case');
        System.assert(getQueueAndEmailMap!=null,'Map Has data');
        
        insert memberSharingList;
        membersharing.MemberRole__c = 'No Access';
        ApexWithoutSharingUtils.updateRMSrecords(memberSharingList);
    }
      /************************************************************************************
    // Purpose      :  Testing unit cases for PshareRMSrecords Utility
    // Developer    :  Ali
    // Created Date :  12/05/2018                 
    //***********************************************************************************/
    @isTest 
    public static void shareRMSrecordsNegativeTest(){
    User usr =[SELECT Id,ContactId FROM User WHERE UserName='test.nonCommunityUser1@test.com.testDev' LIMIT 1];   
        
        system.runAs(usr)
        {
        Boolean result = false;
        Contact con =[SELECT Id,Firstname,lastname FROM contact WHERE LastName='Contact1' LIMIT 1 ];
        con.hed__UniversityEmail__c='raj@rmit.edu.au';
        con.lastname='';
        //update con; 
         try
            {
                update con;    
            }
            catch(DmlException ex)
            {
                result =true;                   
               
            }
            system.assert(result);
        List<ResearcherPortalProject__c> projects=RSTP_TestDataFactoryUtils.createResearchProj(1, 
                                                                                               Schema.SObjectType.ResearcherPortalProject__c.getRecordTypeInfosByName().get(Label.RSTP_ResearchProjectRMRecordtype).getRecordTypeId(),
                                                                                               'Private');
        System.assert(projects!=null,'Projects Created');
           Researcher_Member_Sharing__c memberResearch = new Researcher_Member_Sharing__c();
       
        try
        {
            memberResearch .Contact__c = con.Id;
            memberresearch.User__c = usr.Id;
            insert memberresearch;   
        }
        catch(Exception ex)
        {
            result =true;                   
           
        }  
        system.assert(result);
       
        Researcher_Member_Sharing__c membersharing = new Researcher_Member_Sharing__c();
        membersharing.MemberRole__c = 'CI';
        membersharing.Researcher_Portal_Project__c = projects[0].Id;
        membersharing.Primary_Contact_Flag__c = true  ;
        membersharing.RecordTypeId = Schema.SObjectType.Researcher_Member_Sharing__c.getRecordTypeInfosByName().get(Label.RSTP_ResearchProjectRecordtype).getRecordTypeId(); 
        
        list<Researcher_Member_Sharing__c> memberSharingList = new list<Researcher_Member_Sharing__c>();
        memberSharingList.add(membersharing);
        
        
        list<Ethics_Researcher_Portal_Project__c> ethicsResearchList = new list<Ethics_Researcher_Portal_Project__c>();
        
        Researcher_Member_Sharing__Share rshare= ApexWithoutSharingUtils.shareRMSrecords(membersharing,Label.RSTP_Edit);
        System.assert(rshare!=null,'rshare returned');
        
        
        ApexWithoutSharingUtils.deleteEthicsProjectRecords(ethicsResearchList);
        Map<String,String> getQueueAndEmailMap = ApexWithoutSharingUtils.getQueueIdAndEmailAddress ('Finding Funding','Internal ECP schemes offered by RMIT University','Researcher Portal Support Case');
        System.assert(getQueueAndEmailMap!=null,'Map Has data');
        
        insert memberSharingList;
        membersharing.MemberRole__c = 'No Access';
        ApexWithoutSharingUtils.updateRMSrecords(memberSharingList);
    }
  }
}