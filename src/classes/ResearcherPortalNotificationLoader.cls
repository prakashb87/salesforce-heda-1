@SuppressWarnings('PMD.AvoidGlobalModifier')
global with sharing class ResearcherPortalNotificationLoader implements cms.IContentBundlerOverride {

    /**
     * Gets all published ResearcherPortalNotification content
     *
     * @param jsonRequest
     *
     * @return ContentBundleList
     */
    global String getContent(String jsonRequest) {
        System.debug('ResearcherPortalNotificationLoader');

        APIRequest request = (APIRequest) JSON.deserialize(jsonRequest, APIRequest.class);

        List<String> contentTypes = request.listParameters.get('contentTypes');
        Integer limitCount = getValueOrDefault(request.parameters.get('limitCount'), 5);
        Integer offset = getValueOrDefault(request.parameters.get('offset'), 0);
        String order = getValueOrDefault(request.parameters.get('order'), 'date');

        RenderingAPIRequest renderingRequest = new RenderingAPIRequest();

        renderingRequest.parameters.put('renderType', 'contentType');
//        renderingRequest.listParameters.put('contentLayouts', new List<String> {
//                'JsonDataTemplate'
//        });
        renderingRequest.listParameters.put('contentTypes', contentTypes);
        renderingRequest.parameters.put('limit', String.valueOf(limitCount));
        renderingRequest.parameters.put('offset', String.valueOf(offset));
        renderingRequest.parameters.put('order', order);

        renderingRequest.requestFlags.put(RenderingAPIRequest.WITH_SOCIAL_BUNDLE, true);

        Map<String, String> apiParams = buildRequest(renderingRequest);
        ContentBundleList bundleList = makeRequest(apiParams);

        //TODO:

        return JSON.serialize(bundleList);
    }

    /**
     * Build the OrchestraCMS RenderingAPI request
     *
     * @param renderingRequest
     *
     * @return api request map
     */
    private Map<String, String> buildRequest(RenderingAPIRequest renderingRequest) {
        Map<String, String> apiParams = new Map<String, String>();
        apiParams.put('service', 'OrchestraRenderingAPI');
        apiParams.put('action', 'getRenderedContent');
        apiParams.put('apiVersion', '5.1');
        apiParams.put('runtime', 'Sites');
        apiParams.put('renderingRequest', JSON.serialize(renderingRequest));

        return apiParams;
    }

    /**
     * Perform an OrchestraCMS RenderingAPI request
     *
     * @param apiParams api request map
     *
     * @return contentBundleList containing contentBundles and optionally socialBundles (depending on request parameters)
     */
    private ContentBundleList makeRequest(Map<String, String> apiParams) {
        String response = cms.ServiceEndPoint.doActionApex(apiParams);
        JSONMessage.APIResponse apiResponse = (JSONMessage.APIResponse) JSON.deserialize(response, JSONMessage.APIResponse.class);
        RenderResultBundle resultBundle = (RenderResultBundle) JSON.deserialize(apiResponse.responseObject, RenderResultBundle.class);

        List<ContentBundle> contentBundles = new List<ContentBundle>();
        Map<String, SocialBundle> socialBundles = new Map<String, SocialBundle>();
        System.debug('### Renderings: ' + resultBundle.renderings);
        for (RenderResultBundle.RenderedContent renderedContent : resultBundle.renderings) {
            try {

                String serializedContentBundle = renderedContent.renderMap.get('JsonDataTemplate');
                ContentBundle cb = (ContentBundle) JSON.deserialize(serializedContentBundle, ContentBundle.class);
                if (renderedContent.socialBundle != null && renderedContent.socialBundle.socialActivity != null && !renderedContent.socialBundle.socialActivity.viewedByMe) {
                    System.debug('### Not Viewed By Me');
                    contentBundles.add(cb);
                    socialBundles.put(cb.originId, renderedContent.socialBundle);
                } else if (renderedContent.socialBundle == null || renderedContent.socialBundle.socialActivity == null) {
                    System.debug('### No Social Bundle');
                    contentBundles.add(cb);
                }
            } catch (Exception e) {
                System.debug('### makeRequest: ' + e);
            }
        }

        return new ContentBundleList(contentBundles, socialBundles, resultBundle.contentRemaining);
    }

    /**
     * Return the provided value in Integer form if valid, otherwise return the default value.
     *
     * @param stringValue a user provided integer value
     * @param defaultValue the default value to use when no valid value is provided
     */
    private Integer getValueOrDefault(String stringValue, Integer defaultValue) {
        Integer value = defaultValue;
        try {
            value = Integer.valueOf(stringValue);
        } catch(Exception e) {
            value = defaultValue;
        }

        return value;
    }

    /**
     * Return the provided value, unless it is blank in which case the default value is returned
     *
     * @param stringValue the user provided value to use
     * @param defaultValue the default value to use when no valid value is provided
     */
    private String getValueOrDefault(String stringValue, String defaultValue) {
        return String.isNotBlank(stringValue) ? stringValue : defaultValue;
    }

//    @AuraEnabled public static String getNotifications() {
//        List<String> contentTypes = new List<String>{'ResearcherPortalNotification'};
//        List<String> contentLayouts = new List<String>{'ResearcherPortalNotification'};
//
//        RenderingAPIRequest renderingRequest = new RenderingAPIRequest();
//        renderingRequest.parameters.put('renderType', 'contentType');
//        renderingRequest.parameters.put('status', 'published');
//        renderingRequest.parameters.put('order', 'publish_date');
//
//        renderingRequest.listParameters.put('contentTypes', contentTypes);
//        renderingRequest.listParameters.put('contentLayouts', contentLayouts);
//
//        renderingRequest.requestFlags.put('withSocialBundle', true);
//
//        String response = cms.ServiceEndpoint.doActionApex(new Map<String, String> {
//            'service' => 'OrchestraRenderingApi',
//            'action' => 'getRenderedContent',
//            'renderingRequest' => Json.serialize(renderingRequest),
//            'apiVersion' => '5.1'
//        });
//        JSONMessage.APIResponse apiResponse = (JSONMessage.APIResponse) json.deserialize(response, JSONMessage.APIResponse.class);
//
//
//
//        if (!apiResponse.isSuccess)
////            throw new APIException('Could not retrieve renderings for this node');
//        if (apiResponse.type != 'RenderResultBundle')
////            throw new APIException('Unexpected result from Rendering API');
//
//        System.Debug(apiResponse.responseObject);
//    }
}