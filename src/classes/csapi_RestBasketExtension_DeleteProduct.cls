@RestResource(urlMapping='/csapi_BasketExtension/DeleteProduct')
global class csapi_RestBasketExtension_DeleteProduct {
    
    @HttpPost
    global static csapi_BasketRequestWrapper.DeleteProductResponse doPost(csapi_BasketRequestWrapper.DeleteProductRequest csapi_request) {
        
        RestRequest req = RestContext.request;
        RestResponse res = RestContext.response;
        
        Exception ee = null;
        csapi_BasketRequestWrapper.DeleteProductResponse csapi_response;
        try {
            csapi_response = csapi_BasketExtension.deleteProduct(csapi_request);
        } catch(Exception e ) {
            ee = e;
        } finally {
            if(ee != null) 
            {
                system.debug('ee >> ' + ee.getMessage());
            }
        }
                
        return csapi_response;
    }
    
}