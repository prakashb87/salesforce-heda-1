//Suppressing Warnings for this class as it is used by Cloud Sense managed package which can break references through managed package 
@SuppressWarnings('PMD')
public without sharing class ProductUtility 
{

    public static void CreateOLIs(set<string> setProductBasketId)
    {
        /*
        --select all attributes which are line items
        --check if combinations of Product Family and Line Item Description exist in Product2
        --for the ones which do exist - get Product2.Id
        --for the others - insert and get Product2.Id
        
        --select standard price book
        
        --check if there is an existing combination of PriceBook and Product2 in PriceBookEntry
        --if it doesn't exist - create and get PriceBookEntry.Id
        --if it does exist - get PriceBookEntry.Id
        
        -- enter all PriceBookEntries into OLIs
        */
        
        map<string,string> mapProductBasketIdPriceBookId = new AssignPriceBookToProductBasketImpl().AssignPriceBook(setProductBasketId);
        
        system.debug('****assigned mapProductBasketIdPriceBookId=' + mapProductBasketIdPriceBookId);
        
        list<cscfga__Attribute__c> lstAttribute = [select Id, cscfga__is_active__c, cscfga__Is_Line_Item__c, cscfga__Line_Item_Description__c, cscfga__Line_Item_Sequence__c, 
            cscfga__Price__c, cscfga__List_Price__c, cscfga__Product_Configuration__c, cscfga__Product_Configuration__r.cscfga__Product_Basket__c, 
            cscfga__Product_Configuration__r.cscfga__Product_Family__c, Name , cscfga__Recurring__c, cscfga__Attribute_Definition__r.cscfga__Line_Item_Sequence__c,
            cscfga__Product_Configuration__r.cscfga__Quantity__c
            from cscfga__Attribute__c
            where cscfga__Is_Line_Item__c=true and cscfga__is_active__c=true and 
            cscfga__Product_Configuration__r.cscfga__Product_Basket__c in : mapProductBasketIdPriceBookId.keyset()];
            
        system.debug('****lstAttribute=' + lstAttribute);
        
        //this is the map where the keys are: ProductFamily and ProductName (LineItemDescription) 
        map<string,map<string,Product>> mapProductFamilymapProduct = CreateProducts2(lstAttribute);
        //system.debug('****mapProductFamilymapProduct=' + mapProductFamilymapProduct);
        
        //this is the map where the keys are: PriceBookId and Product2Id
        map<string,map<string,PBEntry>> mapPriceBookIdmapPBEntry = MakePriceBookPBEntriesMap(mapProductBasketIdPriceBookId,lstAttribute,mapProductFamilymapProduct);
        system.debug('****MakePriceBookPBEntriesMap mapPriceBookIdmapPBEntry=' + mapPriceBookIdmapPBEntry);
        
        //this function is void because it just modifies mapPriceBookIdmapPBEntry, so no need for return
        CreatePriceBookEntries(mapPriceBookIdmapPBEntry);
        //system.debug('****CreatePriceBookEntries mapPriceBookIdmapPBEntry=' + mapPriceBookIdmapPBEntry);
        
        //this function takes all structures created before and generates OLIs
        CreateOLIs(mapProductBasketIdPriceBookId,lstAttribute,mapProductFamilymapProduct,mapPriceBookIdmapPBEntry);
        
    }
    
    private static void CreateOLIs(map<string,string> mapProductBasketIdPriceBookId, list<cscfga__Attribute__c> lstAttribute,
        map<string,map<string,Product>> mapProductFamilymapProduct, map<string,map<string,PBEntry>> mapPriceBookIdmapPBEntry)
    {
        list<OpportunityLineItem> lstOLI = new list<OpportunityLineItem>(); 
        
        set<string> setProductBasketId = new set<string>();
        
        for (cscfga__Attribute__c tmpAttribute : lstAttribute)
        {
            setProductBasketId.add(tmpAttribute.cscfga__Product_Configuration__r.cscfga__Product_Basket__c);
        }
        
        map<Id,cscfga__Product_Basket__c> mapProductBasket = new map<Id,cscfga__Product_Basket__c>([select Id,cscfga__Opportunity__c 
            from cscfga__Product_Basket__c where Id in : setProductBasketId]); 
        
        for (cscfga__Attribute__c tmpAttribute : lstAttribute)
        {
            
            string tmpProductFamily = tmpAttribute.cscfga__Product_Configuration__r.cscfga__Product_Family__c;
            //system.debug('****tmpProductFamily=' + tmpProductFamily);
            
            string tmpLineItemDescription = ProductUtility.RemoveAmpresand(tmpAttribute.cscfga__Line_Item_Description__c);
            //system.debug('****tmpLineItemDescription=' + tmpLineItemDescription);
            
            string tmpProductBasketId = tmpAttribute.cscfga__Product_Configuration__r.cscfga__Product_Basket__c;
            system.debug('****private CreateOLIs: tmpProductBasketId=' + tmpProductBasketId);
            
            string tmpPriceBookId = mapProductBasketIdPriceBookId.get(tmpProductBasketId);
            system.debug('****private CreateOLIs: tmpPriceBookId=' + tmpPriceBookId);           
            
            double tmpUnitPrice = tmpAttribute.cscfga__Price__c;
            //system.debug('****tmpUnitPrice=' + tmpUnitPrice);
            
            map<string,Product> mapProduct = mapProductFamilymapProduct.get(tmpProductFamily);
            //system.debug('****mapProduct=' + mapProduct);
            
            Product tmpProduct = mapProduct.get(tmpLineItemDescription);
            //system.debug('****tmpProduct=' + tmpProduct);
            
            string tmpProduct2Id = tmpProduct.Prod2.Id;
            //system.debug('****tmpProduct2Id=' + tmpProduct2Id);
            
            map<string,PBEntry> mapPBEntry = mapPriceBookIdmapPBEntry.get(tmpPriceBookId);
            //system.debug('****mapPBEntry=' + mapPBEntry);
            
            PBEntry tmpPBEntry = mapPBEntry.get(tmpProduct2Id);
            //system.debug('****tmpPBEntry=' + tmpPBEntry);
            
            string tmpPriceBookEntryId = tmpPBEntry.PBE.Id;
            //system.debug('****tmpPriceBookEntryId=' + tmpPriceBookEntryId);
            
            string tmpOpportunityId=mapProductBasket.get(tmpProductBasketId).cscfga__Opportunity__c;
            system.debug('****tmpOpportunityId=' + tmpOpportunityId);
            
            OpportunityLineItem tmpOpportunityLineItem = new OpportunityLineItem();
            tmpOpportunityLineItem.cscfga__Attribute__c=tmpAttribute.Id;
            tmpOpportunityLineItem.OpportunityId=tmpOpportunityId;
            tmpOpportunityLineItem.PricebookEntryId=tmpPriceBookEntryId;
            tmpOpportunityLineItem.Quantity=tmpAttribute.cscfga__Product_Configuration__r.cscfga__Quantity__c;
            tmpOpportunityLineItem.Description=tmpAttribute.cscfga__Line_Item_Description__c;
            //tmpOpportunityLineItem.TotalPrice=tmpAttribute.cscfga__Price__c;
            double UnitPrice;
            if (tmpAttribute.cscfga__Price__c==null)
            {
                tmpOpportunityLineItem.UnitPrice=0;
            }
            else
            {
                tmpOpportunityLineItem.UnitPrice=tmpAttribute.cscfga__Price__c; 
            }    
            
            lstOLI.add(tmpOpportunityLineItem);
        }
        
        system.debug('****beforelstOLI=' + lstOLI); 
        
        if (lstOLI.size()>0) {insert lstOLI;}
        //system.debug('****lstOLI=' + lstOLI);
    }
    
    
    private static map<string,map<string,PBEntry>> MakePriceBookPBEntriesMap(map<string,string> mapProductBasketIdPriceBookId,list<cscfga__Attribute__c> lstAttribute, 
        map<string,map<string,Product>> mapProductFamilymapProduct)
    {
        list<PBEntry> lstPBEntry = new list<PBEntry>();
        map<string,map<string,PBEntry>> mapPriceBookIdmapPBEntry = new map<string,map<string,PBEntry>>(); 
        
        system.debug('****mapProductBasketIdPriceBookId=' + mapProductBasketIdPriceBookId);
        
        for (cscfga__Attribute__c tmpAttribute : lstAttribute)
        {
            
            string tmpProductFamily = tmpAttribute.cscfga__Product_Configuration__r.cscfga__Product_Family__c;
            system.debug('****tmpProductFamily=' + tmpProductFamily);
            
            string tmpLineItemDescription = ProductUtility.RemoveAmpresand(tmpAttribute.cscfga__Line_Item_Description__c);
            system.debug('****tmpLineItemDescription=' + tmpLineItemDescription);
            
            string tmpProductBasketId = tmpAttribute.cscfga__Product_Configuration__r.cscfga__Product_Basket__c;
            system.debug('****tmpProductBasketId=' + tmpProductBasketId);
            
            
            
            if (mapProductFamilymapProduct.containsKey(tmpProductFamily))
            {
                map<string,Product> mapProduct = mapProductFamilymapProduct.get(tmpProductFamily);
                
                if (mapProduct.containsKey(tmpLineItemDescription))
                {
                    Product tmpProduct = mapProduct.get(tmpLineItemDescription);
                    //system.debug('****tmpProduct=' + tmpProduct);
                    
                    string tmpProduct2Id = tmpProduct.Prod2.Id;
                    //system.debug('****tmpProduct2Id=' + tmpProduct2Id);
                    
                    string tmpPriceBookId = mapProductBasketIdPriceBookId.get(tmpProductBasketId);
                    system.debug('****tmpPriceBookId=' + tmpPriceBookId);
                    
                    PBEntry tmpPBEntry = new PBEntry();
                    tmpPBEntry.PriceBookId=tmpPriceBookId;
                    tmpPBEntry.Product2Id=tmpProduct2Id;
                    
                    if (tmpAttribute.cscfga__Price__c==null)
                    {
                        tmpPBEntry.UnitPrice=0;
                    }
                    else
                    {
                        tmpPBEntry.UnitPrice=tmpAttribute.cscfga__Price__c;
                    }
                    tmpPBEntry.Name = tmpLineItemDescription;
                    
                    if (mapPriceBookIdmapPBEntry.containsKey(tmpPriceBookId))
                    {
                        map<string,PBEntry> mapPBEntry = mapPriceBookIdmapPBEntry.get(tmpPriceBookId);
                        if (!mapPBEntry.containsKey(tmpProduct2Id))
                        {
                            mapPBEntry.put(tmpProduct2Id,tmpPBEntry);
                        }
                    }
                    else
                    {
                        map<string,PBEntry> mapPBEntry = new map<string,PBEntry>();
                        mapPBEntry.put(tmpProduct2Id,tmpPBEntry);
                        mapPriceBookIdmapPBEntry.put(tmpPriceBookId,mapPBEntry);
                    }
                    
                }
                
            }
        }
        
        system.debug('****mapPriceBookIdmapPBEntry=' + mapPriceBookIdmapPBEntry);
        return mapPriceBookIdmapPBEntry;
    }
    
    
    
    private static void CreatePriceBookEntries(map<string,map<string,PBEntry>> mapPriceBookIdmapPBEntry)
    {
        //system.debug('***mapPriceBookIdmapPBEntry=' + mapPriceBookIdmapPBEntry);
        
        set<string> setProduct2Id = new set<string>();
        
        list<PricebookEntry> lstPricebookEntryInsert = new list<PricebookEntry>(); 
        
        for (map<string,PBEntry> mapPBEntry : mapPriceBookIdmapPBEntry.values())
        {
            for (PBEntry tmpPBEntry : mapPBEntry.values())
            {
                setProduct2Id.add(tmpPBEntry.Product2Id);
            }
        }
        
        system.debug('*** setProduct2Id=' + setProduct2Id);
        
        if (setProduct2Id.size()>0)
        {
            map<Id,PricebookEntry> mapPricebookEntry = new map<Id,PricebookEntry> ([select Id, IsActive, Name, Pricebook2Id, Product2Id, UnitPrice 
                from PricebookEntry
                where Product2Id in : setProduct2Id]);
                
            
            //system.debug('***mapPricebookEntry=' + mapPricebookEntry);
            
            for (PricebookEntry tmpPricebookEntry : mapPricebookEntry.values())
            {
                string tmpPricebook2Id = tmpPricebookEntry.Pricebook2Id;
                string tmpProduct2Id = tmpPricebookEntry.Product2Id;
                
                system.debug('***private CreatePriceBookEntries: tmpPricebook2Id=' + tmpPricebook2Id);
                system.debug('***private CreatePriceBookEntries: tmpProduct2Id=' + tmpProduct2Id);
                
                if (mapPriceBookIdmapPBEntry.containsKey(tmpPricebook2Id))
                {
                    map<string,PBEntry> mapPBEntry = mapPriceBookIdmapPBEntry.get(tmpPricebook2Id);
                    if (mapPBEntry.containsKey(tmpProduct2Id))
                    {
                        PBEntry tmpPBEntry = mapPBEntry.get(tmpProduct2Id);
                        tmpPBEntry.PBEntryId=tmpPricebookEntry.Id;
                    }
                }
            }
            
            for (map<string,PBEntry> mapPBEntry : mapPriceBookIdmapPBEntry.values())
            {
                for (PBEntry tmpPBEntry : mapPBEntry.values())
                {
                    
                    //system.debug('***tmpPBEntry=' + tmpPBEntry);
                    
                    if ((tmpPBEntry.PBEntryId=='') || (tmpPBEntry.PBEntryId==null)) 
                    {
                        PricebookEntry tmpPricebookEntry = new PricebookEntry();
                        tmpPricebookEntry.IsActive=true;
                        //tmpPricebookEntry.Name=tmpPBEntry.Name;
                        tmpPricebookEntry.Pricebook2Id=tmpPBEntry.PriceBookId;
                        tmpPricebookEntry.Product2Id=tmpPBEntry.Product2Id;
                        tmpPricebookEntry.UnitPrice=tmpPBEntry.UnitPrice;
                        
                        tmpPBEntry.PBE=tmpPricebookEntry;
                        
                        lstPricebookEntryInsert.add(tmpPricebookEntry);
                    }   
                    else
                    {
                        PricebookEntry tmpPricebookEntry = mapPricebookEntry.get(tmpPBEntry.PBEntryId);
                        tmpPBEntry.PBE=tmpPricebookEntry;
                    }   
                }
            }
            
        }
        
        if (lstPricebookEntryInsert.size()>0) {insert lstPricebookEntryInsert;}
        
    }
    
    private static string RemoveAmpresand(string pLineItemDescription)
    {
        //example: Connect ZZP Internet Start &amp; Play Start
        
        if (pLineItemDescription!=null)
        {
            if (pLineItemDescription.contains('&amp;'))
            {
                pLineItemDescription=pLineItemDescription.replace('&amp;','&');
            }
        }
        else
        {
            pLineItemDescription='';
        }
                
        return pLineItemDescription;
    }   
    
    private static map<string,map<string,Product>> CreateProducts2(list<cscfga__Attribute__c> lstAttribute)
    {
        
        //system.debug('***lstAttribute=' + lstAttribute);
        
        map<string,map<string,Product>> mapProductFamilymapProduct = new map<string,map<string,Product>>(); 
        
        for (cscfga__Attribute__c tmpAttribute : lstAttribute)
        {
            string tmpProductFamily = tmpAttribute.cscfga__Product_Configuration__r.cscfga__Product_Family__c;
            string tmpLineItemDescription = ProductUtility.RemoveAmpresand(tmpAttribute.cscfga__Line_Item_Description__c);
        
            //system.debug('***tmpAttribute.cscfga__Line_Item_Description__c=' + tmpAttribute.cscfga__Line_Item_Description__c);
            //system.debug('***tmpProductFamily=' + tmpProductFamily);
            //system.debug('***tmpLineItemDescription=' + tmpLineItemDescription);
            
        
            if (mapProductFamilymapProduct.containsKey(tmpProductFamily))
            {
                //system.debug('***contains tmpProductFamily=' + tmpProductFamily);
                map<string,Product> mapProduct = mapProductFamilymapProduct.get(tmpProductFamily);
                //system.debug('***mapProduct=' + mapProduct);
                if (!mapProduct.containsKey(tmpLineItemDescription))
                {
                    //system.debug('***does not contain tmpLineItemDescription=' + tmpLineItemDescription);
                    Product tmpProduct = new Product();
                    tmpProduct.ProductFamily=tmpProductFamily;
                    tmpProduct.LiniItemDescription=tmpLineItemDescription;
                    mapProduct.put(tmpLineItemDescription,tmpProduct);
                }
            }
            else
            {
                //system.debug('***does not contain tmpProductFamily=' + tmpProductFamily);
                map<string,Product> mapProduct = new map<string,Product>();
                Product tmpProduct = new Product();
                tmpProduct.ProductFamily=tmpProductFamily;
                tmpProduct.LiniItemDescription=tmpLineItemDescription;
                mapProduct.put(tmpLineItemDescription,tmpProduct);
                mapProductFamilymapProduct.put(tmpProductFamily,mapProduct);
            }
            
        }
        
        //system.debug('***mapProductFamilymapProduct=' + mapProductFamilymapProduct);
        
        if (mapProductFamilymapProduct.size()>0)
        {
            map<Id,Product2> mapProduct2 = new map<Id,Product2>([select Family, Id, IsActive, Name 
                from Product2
                where IsActive=true and Family in : mapProductFamilymapProduct.keySet()]);
            
            for (Product2 tmpProduct2 : mapProduct2.values())
            {
                if (mapProductFamilymapProduct.containsKey(tmpProduct2.Family))
                {
                    map<string,Product> mapProduct = mapProductFamilymapProduct.get(tmpProduct2.Family);
                    if (mapProduct.containsKey(tmpProduct2.Name))
                    {
                        Product tmpProduct = mapProduct.get(tmpProduct2.Name);
                        tmpProduct.Product2Id = tmpProduct2.Id;
                    }
                }
            }
            
            list<Product2> lstProduct2Insert = new list<Product2>();
            
            for (string tmpProductFamily : mapProductFamilymapProduct.keySet())
            {
                //system.debug('****tmpProductFamily=' + tmpProductFamily);
                
                map<string,Product> mapProduct = mapProductFamilymapProduct.get(tmpProductFamily);
                
                //system.debug('****mapProduct=' + mapProduct);
                
                for (Product tmpProduct : mapProduct.values())
                {
                    //system.debug('****tmpProduct=' + tmpProduct);
                    if ((tmpProduct.Product2Id=='') || (tmpProduct.Product2Id==null)) 
                    {
                        Product2 tmpProduct2 = new Product2();
                        tmpProduct2.Family=tmpProductFamily;
                        tmpProduct2.Name = tmpProduct.LiniItemDescription;
                        tmpProduct2.IsActive = true;
                        tmpProduct.Prod2 = tmpProduct2;
                        
                        lstProduct2Insert.add(tmpProduct2);
                    }
                    else
                    {
                        Product2 tmpProduct2 = mapProduct2.get(tmpProduct.Product2Id);
                        tmpProduct.Prod2 = tmpProduct2;
                    }
                }
            }
            
            //system.debug('****before lstProduct2Insert=' + lstProduct2Insert);
            if (lstProduct2Insert.size()>0){ insert lstProduct2Insert;}
            system.debug('****lstProduct2Insert=' + lstProduct2Insert);
        }       
        
        
        //system.debug('****mapProductFamilymapProduct=' + mapProductFamilymapProduct);
        return mapProductFamilymapProduct;
    }
    
    /*
    public static void DeleteOLIs(set<string> setProductBasketId)
    {
        map<Id,cscfga__Attribute__c> mapAttribute = new map<Id,cscfga__Attribute__c>([select Id,cscfga__Product_Configuration__r.cscfga__Product_Basket__c 
            from cscfga__Attribute__c where cscfga__Product_Configuration__r.cscfga__Product_Basket__c in : setProductBasketId]); 
        
        set<string> setAttributeId = new set<string>();
        
        for (cscfga__Attribute__c tmpAttribute : mapAttribute.values())
        {
            setAttributeId.add(tmpAttribute.Id);
        }
        
        if (setAttributeId.size()>0)
        {
            list<OpportunityLineItem> lstOLI = [select Id from OpportunityLineItem where cscfga__Attribute__c in : setAttributeId];
            if (lstOLI.size()>0) delete lstOLI;
        }
    }
    */
     
    
    public static void DeleteHardOLIs(set<string> setProductBasketId)
    {
        list<cscfga__Product_Basket__c> lstPB = [select Id, cscfga__Opportunity__c from cscfga__Product_Basket__c 
            where Id in : setProductBasketId];
            
        set<Id> setOpportunityId = new set<Id>();
        
        for(cscfga__Product_Basket__c tmpPB : lstPB)
        {
            setOpportunityId.add(tmpPB.cscfga__Opportunity__c);
        }
        
        if (setOpportunityId.size()>0)
        {
            list<OpportunityLineItem> lstOLI = [select Id from OpportunityLineItem where OpportunityId in : setOpportunityId];
            if (lstOLI.size()>0){ delete lstOLI;}
        }
    }
    
    
    private class Product
    {
        public string ProductFamily {get;set;}
        public string LiniItemDescription {get;set;}
        public string Product2Id {get;set;}
        public Product2 Prod2 {get;set;}
    }
    
    
    private class PBEntry
    {
        public string PBEntryId {get;set;}
        public string PriceBookId {get;set;}
        public string Product2Id {get;set;}
        public double UnitPrice {get;set;}
        public string Name {get;set;}
        public PriceBookEntry PBE {get;set;}
    }


}