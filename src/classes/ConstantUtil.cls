/********************************************************************************************************/
/**    Author : Resmi Ramakrishnan                     Date : 18/05/2018     ****************************/               
/***  This class is using store Constant values                                                 *********/ 
/********************************************************************************************************/
public class ConstantUtil 
{
    public static final String COURSECONNECTION_LIFECYCLE_CREATED= 'CourseConnectionLifeCycleCreated'; 
    public static final String COURSECONNECTION_LIFECYCLE_SUCCESS= 'CourseConnectionLifeCycleSuccess';
    public static final String COURSECONNECTION_LIFECYCLE_NOTSTARTED = 'CourseConnectionLifeCycleNotStarted';
    public static final String COURSECONNECTION_LIFECYCLE_POSTTOSAMS = 'Post To SAMS';
    public static final String COURSECONNECTION_LIFECYCLE_POSTTOCANVAS = 'Post To Canvas';
    public static final String COURSECONNECTION_LIFECYCLE_STAGE_CREATED = 'Created';

    public static final String IDAM_TRUE_RESPONSE = 'True Response From IDAM';
    public static final String IDAM_FALSE_RESPONSE = 'False Response From IDAM';

    public static final String IDAM_IPAAS_SUCCESS_RESPONSE = 'Success Response From IPaaS';
    public static final String IDAM_IPAAS_FAILURE_RESPONSE ='Failure Response From IPaaS';
    

    public static final String IDAM_EMAIL_CHECK_RESPONSE_ERROR = 'IDAM IPaaS Email Check Response Error';
    public static final String IDAM_USER_CREATION_RESPONSE_ERROR = 'IDAM IPaaS User Creation Response Error';


    public static final String IDAM_FIRST_NAME_VALIDATION = 'First Name Validation Error';
    public static final String IDAM_LAST_NAME_VALIDATION = 'Last Name Validation Error';
    public static final String IDAM_EMAIL_VALIDATION_ERROR = 'Email Address Validation Error';
    public static final String IDAM_MOBILE_VALIDATION_ERROR = 'Mobile Number Validation Error';
    
}