@SuppressWarnings('PMD.VariableNamingConventions') 
public without sharing class Form2_EnterLeadController 
{
    private Lead newLead;
    private static Id leadRecordType = Schema.SObjectType.Lead.RecordTypeInfosByName.get('Business Lead').RecordTypeId;
    
    public Lead createNewLead {get; set;}
    public Form2_EnterLeadWrapper leadWrapper {get;set;}
    
    public Form2_EnterLeadController(ApexPages.StandardController stdController)
    {
    	leadWrapper = new Form2_EnterLeadWrapper();
        this.newLead = (Lead) stdController.getRecord();
        this.newLead.RecordTypeId = leadRecordType;       
        String message = String.escapeSingleQuotes('' + ApexPages.CurrentPage().GetParameters().Get('message'));
        String Status_Message = String.escapeSingleQuotes('' + ApexPages.CurrentPage().GetParameters().Get('status'));
        String pageHeaderReferer = ApexPages.currentPage().getHeaders().get('Referer'); 

    }
    
    public PageReference createNewStudent()
    {
        PageReference OpenNewPage = new PageReference('/apex/IntvisitThankPage');
        try
        {
        	Group grpQueue = new Group();
        	if(Schema.sObjectType.Group.isAccessible())
            {
              grpQueue = [select Id from Group where developerName = 'GDP_Queue' and Type = 'Queue'];
            }
            createNewLead = new Lead();
            createNewLead.RecordTypeId = leadRecordType;
            createNewLead.FirstName = leadWrapper.standredFieldsWrapper.FirstName;
            createNewLead.LastName = leadWrapper.standredFieldsWrapper.LastName;
            createNewLead.Company = leadWrapper.standredFieldsWrapper.Company;
            createNewLead.Email = leadWrapper.standredFieldsWrapper.Email;
            createNewLead.LeadSource = 'International Visit Form';
            createNewLead.RMIT_Relationship_Alignment__c  = 'Global_Development_and_Portfolio';
            createNewLead.OwnerId = grpQueue.Id;
            createNewLead.Past_visits_to_RMIT_When_What_purpose__c = leadWrapper.Past_visits_to_RMIT_When_What_purpose;            
            createNewLead.Proposed_Outcomes__c = leadWrapper.Proposed_Outcomes;
            createNewLead.Number_of_visitors__c = leadWrapper.Number_of_visitors;
            createNewLead.Specific_RMIT_divisions_to_meet__c = leadWrapper.Specific_RMIT_divisions_to_meet;
            createNewLead.Names_Roles_of_visitors__c = leadWrapper.Names_Roles_of_visitors;
            createNewLead.Informal_Links_with_RMIT__c = leadWrapper.Informal_Links_with_RMIT;
            createNewLead.Position__c = leadWrapper.Position;
            createNewLead.Country__c = leadWrapper.standredFieldsWrapper.Country;
            validateStandardFields(this.newLead,this.createNewLead);
            System.debug('>>>>> createNewLead: '+createNewLead);
            if(Schema.sObjectType.Lead.isCreateable())
            {
              insert createNewLead;
            }
            System.debug('>>>>> createNewLead ID: '+createNewLead.Id);
            
            OpenNewPage.getParameters().put('message', 'Success! New student has been created.');
            OpenNewPage.getParameters().put('status', 'Success');
            OpenNewPage.setRedirect(true);
            
            //this.newLead = new Lead();
            
            /*
            if((AttachedFileBody!=null)&&(AttachedFileName!=null))
            {
                Attachment attachedFile = new Attachment();
                attachedFile.Body = AttachedFileBody;
                attachedFile.Name = AttachedFileName;
                attachedFile.ParentId = createNewLead.id;
                attachedFile.IsPrivate = false;
                insert attachedFile;
            }
	    */
	    
	         uploadAttachments(leadWrapper.standredFieldsWrapper.AttachedFileBody,leadWrapper.standredFieldsWrapper.AttachedFileName);
             
             return OpenNewPage;
        }
        catch(System.Exception ex)
        {
            OpenNewPage.getParameters().put('message', 'Failed! Cannot create a new student.');
            OpenNewPage.getParameters().put('status', 'Failed');
            OpenNewPage.setRedirect(false);
            System.debug('>>>>> Create New Student Error Message: '+ex.getMessage());
            return null;
        }
        
    }
    
    public void validateStandardFields(Lead newLead,Lead createNewLead)
    {
    	    if(this.newLead.proposed_Date__c != null)
            {
               createNewLead.proposed_Date__c = this.newLead.proposed_Date__c;
            }
            if(this.newLead.Proposed_Time__c !='--None--')
            {
               createNewLead.Proposed_Time__c = this.newLead.Proposed_Time__c;
            }
            if(this.newLead.Current_RMIT_partner__c !='--None--')
            {
               createNewLead.Current_RMIT_partner__c = this.newLead.Current_RMIT_partner__c;
            }
            if(this.newLead.Invitation_Letter_required__c !='--None--')
            {
               createNewLead.Invitation_Letter_required__c = this.newLead.Invitation_Letter_required__c;
            }
            if(this.newLead.Salutation !='--None--')
            {
               createNewLead.Salutation = this.newLead.Salutation;
            }
            if(this.newLead.Purpose_Main_Areas_of_Interest__c !='--None--')
            {
               createNewLead.Purpose_Main_Areas_of_Interest__c = this.newLead.Purpose_Main_Areas_of_Interest__c;
            }
            if(this.newLead.Academic_Focus__c !='--None--')
            {
               createNewLead.Academic_Focus__c = this.newLead.Academic_Focus__c;
            }
    }
    
    public void uploadAttachments(blob fileBody,String fileName)
    {
             if((fileBody!=null)&&(fileName!=null)){
                ContentVersion cv = new ContentVersion();
                cv.versionData = fileBody;
                cv.title = fileName;
                cv.pathOnClient ='/fakePath/'+fileName;
                if(Schema.sObjectType.ContentVersion.isCreateable())
                {
                   insert cv;
                }
                
                ContentDocumentLink cdl = new ContentDocumentLink();
                cdl.ContentDocumentId = [SELECT Id, ContentDocumentId FROM ContentVersion WHERE Id =: cv.Id].ContentDocumentId;
                cdl.LinkedEntityId = createNewLead.Id;
                cdl.ShareType = 'V';
                if(Schema.sObjectType.ContentDocumentLink.isCreateable())
                {
                   insert cdl;
                }
            } 
    }
    public List<SelectOption> getProposedTime()
	{
	    List<SelectOption> options = new List<SelectOption>();

	    /*Schema.DescribeFieldResult mSelectProposed_TimeSchema = Lead.Proposed_Time__c.getDescribe();  Changes done for request RITM0331805
	    List<Schema.PicklistEntry> mSelectProposed_TimeValueLIST  = mSelectProposed_TimeSchema.getPicklistValues();   */
	    options.add(new SelectOption('--None--', '--None--'));
	    options.add(new SelectOption('Morning', 'Morning'));
	    options.add(new SelectOption('Afternoon', 'Afternoon'));
	    options.add(new SelectOption('Full Day', 'Full Day'));
	    /*for( Schema.PicklistEntry mPicklistItem : mSelectProposed_TimeValueLIST)            Changes done for request RITM0331805
	    {
	       options.add(new SelectOption(mPicklistItem.getLabel(), mPicklistItem.getValue()));
	    }*/       
	    return options;
    }
    
    public List<SelectOption> getCurrentRMITpartner()
    {
        List<SelectOption> options = new List<SelectOption>();
            
        Schema.DescribeFieldResult mSelectCurrent_RMIT_partnerSchema = Lead.Current_RMIT_partner__c.getDescribe();
        List<Schema.PicklistEntry> mSelectCurrent_RMIT_partnerValueLIST  = mSelectCurrent_RMIT_partnerSchema.getPicklistValues();   
        options.add(new SelectOption('--None--', '--None--'));
        for( Schema.PicklistEntry mPicklistItem : mSelectCurrent_RMIT_partnerValueLIST)
        {
           options.add(new SelectOption(mPicklistItem.getLabel(), mPicklistItem.getValue()));
        }       
        return options;
    }
    
    
    public List<SelectOption> getInvitationLetterrequired()
    {
        List<SelectOption> options = new List<SelectOption>();
            
        Schema.DescribeFieldResult mSelectInvitation_Letter_requiredSchema = Lead.Invitation_Letter_required__c.getDescribe();
        List<Schema.PicklistEntry> mSelectInvitation_Letter_requiredValueLIST  = mSelectInvitation_Letter_requiredSchema.getPicklistValues();   
        options.add(new SelectOption('--None--', '--None--'));
        for( Schema.PicklistEntry mPicklistItem : mSelectInvitation_Letter_requiredValueLIST)
        {
           options.add(new SelectOption(mPicklistItem.getLabel(), mPicklistItem.getValue()));
        }       
        return options;
    }
    
    public List<SelectOption> getSalutation()
    {
        List<SelectOption> options = new List<SelectOption>();
            
        Schema.DescribeFieldResult mSelectSalutationSchema = Lead.Salutation.getDescribe();
        List<Schema.PicklistEntry> mSelectSalutationValueLIST  = mSelectSalutationSchema.getPicklistValues();   
        options.add(new SelectOption('--None--', '--None--'));
        for( Schema.PicklistEntry mPicklistItem : mSelectSalutationValueLIST)
        {
           options.add(new SelectOption(mPicklistItem.getLabel(), mPicklistItem.getValue()));
        }       
        return options;
    }
    
    public List<SelectOption> getPurposeMainAreasofInterest()
    {
        List<SelectOption> options = new List<SelectOption>();
            
        Schema.DescribeFieldResult mSelectPurpose_Main_Areas_of_InterestSchema = Lead.Purpose_Main_Areas_of_Interest__c.getDescribe();
        List<Schema.PicklistEntry> mSelectPurpose_Main_Areas_of_InterestValueLIST  = mSelectPurpose_Main_Areas_of_InterestSchema.getPicklistValues();   
        options.add(new SelectOption('--None--', '--None--'));
        for( Schema.PicklistEntry mPicklistItem : mSelectPurpose_Main_Areas_of_InterestValueLIST)
        {
           options.add(new SelectOption(mPicklistItem.getLabel(), mPicklistItem.getValue()));
        }       
        return options;
    }
    
    public List<SelectOption> getAcademicFocus()
    {
        List<SelectOption> options = new List<SelectOption>();
            
        Schema.DescribeFieldResult mSelectAcademic_FocusSchema = Lead.Academic_Focus__c.getDescribe();
        List<Schema.PicklistEntry> mSelectAcademic_FocusValueLIST  = mSelectAcademic_FocusSchema.getPicklistValues();   
        options.add(new SelectOption('--None--', '--None--'));
        for( Schema.PicklistEntry mPicklistItem : mSelectAcademic_FocusValueLIST)
        {
           options.add(new SelectOption(mPicklistItem.getLabel(), mPicklistItem.getValue()));
        }       
        return options;
    }
    
    public List<SelectOption> getCountryOption()
    {
        List<SelectOption> options = new List<SelectOption>();
            
        Schema.DescribeFieldResult mSelectCountrySchema = Lead.Country__c.getDescribe();
        List<Schema.PicklistEntry> mSelectCountryValueLIST  = mSelectCountrySchema.getPicklistValues();   
        options.add(new SelectOption('--None--', '--None--'));
        for( Schema.PicklistEntry mPicklistItem : mSelectCountryValueLIST)
        {
           options.add(new SelectOption(mPicklistItem.getLabel(), mPicklistItem.getValue()));
        }       
        return options;
    }
}