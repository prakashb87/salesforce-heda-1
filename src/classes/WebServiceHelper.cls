/**
 * Created by marcelowork on 7/03/19.
 */

public with sharing class WebServiceHelper {


    public static boolean isEffectiveDateEarlierorEqualToday(String aDate){
        Date effectiveDate;
        try{
            effectiveDate = Date.valueOf(aDate);
        }catch (Exception e){
            WebserviceException except = new WebserviceException(e);
            except.setMessage('Invalid Date Format');
            //throw except;
            return false;
        }
        if(effectiveDate <= Date.today()){
            return true;
        }
        return false;
    }


    public class StrategicIntegrationResponse{ 
        public Integer statusCode;
        public String code;
        public String message;
        public String errorId;
        public String value;
    }

    public static Map<String,String> generateResponseFromWrapper(StrategicIntegrationResponse resp){

        String jsonString = JSON.serialize(resp,true);
        Map<String,Object> valuesByKeys =(Map<String,Object>) JSON.deserializeUntyped(jsonString);
        map<String,String> stringValuesByKeys = new Map<String,String>();
        for(String key : valuesByKeys.keySet()){
            if(valuesByKeys.get(key) instanceof String) {
                stringValuesByKeys.put(key, valuesByKeys.get(key).toString());
            }
        }
        if(resp.statusCode!=null){
            RestContext.response.statusCode=resp.statusCode;
        }
        return(stringValuesByKeys);

    }

    public static void validateListNotEmpty(List<Object> alist){
        if(alist.isEmpty()){

            WebserviceException e= new WebserviceException('Empty List');
            e.payload = RestContext.request.requestBody;
            throw e;
        }
    }


    public static Map<String, String>returnError(Integer statusCode, String code, String message) {
        RestContext.response.statusCode = statusCode;
        Map<String, String> errorByCode = new Map <String, String>();
        errorByCode.put(code,message);

        return errorByCode;

    }



    public static Map<String, String>returnErrorForWebServiceException(WebserviceException e,RMErrLog__c log) {
        RestContext.response.statusCode = 400;
        Map<String, String> errorByCode = new Map <String, String>();
        errorByCode.put('errorId',log.Id);

        if (e.errorCode == '01') {
            errorByCode.put('code', '01');
            errorByCode.put('message','Invalid payload on request');
            errorByCode.put('errorId',log.Id);

        } else {
            errorByCode.put('code', '02');
            errorByCode.put('message','Missing Mandatory Parameter');
            errorByCode.put('value',e.parameter);
            errorByCode.put('errorId',log.Id);


        }

        return errorByCode;

    }

    public static Map<String, String>setReturnOKStatus() {
        RestContext.response.statusCode = 200;
        return new Map<String, String>{
                'message' => 'OK',
                'code'=>'00'
        };


    }


}