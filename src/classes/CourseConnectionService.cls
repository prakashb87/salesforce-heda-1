/*********************************************************************************
 *** @ClassName         : CourseConnectionService
 *** @Author            : Shubham Singh 
 *** @Requirement       : CourseConnectionService class for methods related to web service version 1.1
 *** @Created date      : 17/03/2019
 *** @Modified by       : Shubham Singh 
 *** @modified date     : 01/04/2019
 **********************************************************************************/
public with sharing class CourseConnectionService {
    //wrapper class to create variables to store the respective values 
    public class CourseConnectionServiceWrapper {
        public Map < String, Id > mapOfCourseConnectionToUpdate = new Map < String, Id > ();
    }
    /**
     * -----------------------------------------------------------------------------------------------+
     * Getting course connection records based on unique key from hed__Course_Enrollment__c and create map
     * ------------------------------------------------------------------------------------------------
     * @author	  Shubham Singh 
     * @method    getCourseConnectionByUniqueKey
     * @param     Set < String > listUniqueKey
     * @return    CourseConnectionServiceWrapper
     * @Reference RM-2580
     * -----------------------------------------------------------------------------------------------+
     */
    public static CourseConnectionServiceWrapper getCourseConnectionByUniqueKey(Set < String > listUniqueKey) {
        CourseConnectionServiceWrapper wrapper = new CourseConnectionServiceWrapper();

        List < hed__Course_Enrollment__c > courseConnectionList = new List < hed__Course_Enrollment__c > ();
        courseConnectionList = queryCourseConnectionByUniqueKey(listUniqueKey);
        //wrapper.courseConnection = courseConnectionList;
        //Map<String, Id > mapCourseConnectionUpdate = new Map<String,Id> ();
        for (hed__Course_Enrollment__c loopCourseConnection: courseConnectionList) {
            wrapper.mapOfCourseConnectionToUpdate.put(loopCourseConnection.Course_Connection_Unique_Key__c, loopCourseConnection.ID);
        }
        return wrapper;
    }
    /**
     * -----------------------------------------------------------------------------------------------+
     * Query course connection records based on unique key from hed__Course_Enrollment__c
     * ------------------------------------------------------------------------------------------------
     * @author	  Shubham Singh 
     * @method    queryCourseConnectionByUniqueKey
     * @param     Set < String > listUniqueKey
     * @return    List<hed__Course_Enrollment__c>
     * @Reference RM-2580
     * -----------------------------------------------------------------------------------------------+
     */
    public static List < hed__Course_Enrollment__c > queryCourseConnectionByUniqueKey(Set < String > listUniqueKey) {


        if (Schema.sObjectType.hed__Course_Enrollment__c.isAccessible()) {
            //we can use field sets in place of this
            return [Select Institution__r.Name, hed__Contact__r.Student_ID__c, hed__Course_Enrollment__c.Effective_Date__c, RecordTypeId, hed__Account__c, hed__Contact__c, Institution__c, Class_Number__c, Session__c, Academic_Career__c, hed__Status__c, Status_Reason__c, Last_Enrolment_Action__c,
                Last_Action_Reason__c, Enrolment_Add_Date__c, Enrolment_Drop_Date__c, Units_Taken__c, Progress_Units__c, Billing_Units__c, Grading_Basis__c, Grading_Basis_Date__c, Official_Grade__c,
                Grade_Input__c, Course_Connection_Unique_Key__c, hed__Program_Enrollment__c, Fund_Source_Description__c, hed__Course_Offering__c, Career__c, Term__c, Fund_Source__c, HECS_Exempt_Status_Code__c, Status_Date__c from hed__Course_Enrollment__c where Course_Connection_Unique_Key__c In: listUniqueKey
            ];
        }

        return null;

    }

}