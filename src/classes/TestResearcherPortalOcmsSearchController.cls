@isTest
public with sharing class TestResearcherPortalOcmsSearchController {
    private static final String CONTENT_TYPE = 'TestContentType';
    private static final String CONTENT_NAME = 'TestContent';
    private static final String SITE_NAME = 'TestSite';
    private static final String TAXONOMY_PATH = 'Test/Taxonomy/Path';
    private static final String SEARCH_TERM = 'term';

    static cms__Content__c[] testContents;

    static void buildTestContent() {
        // Normally we wouldn't touch any cms__ objects, even in a test
        // Here we need a valid cms__Content__c ID to insert an OCMS_Search__c record
        cms__Content__c testContent = new cms__Content__c(
                cms__Name__c = CONTENT_NAME,
                cms__Site_Name__c = SITE_NAME
        );

        insert testContent;

        testContents = new cms__Content__c[] { testContent };

        OCMS_Search__c ocmsSearch = new OCMS_Search__c(
                Content__c = testContent.Id,
                Importance__c = 50
        );

        insert ocmsSearch;
    }

    static ContentBundle buildContentBundle(Id originId) {
        // ResearcherPortalOcmsSearchLoader only uses bundle.originId
        ContentBundle bundle = new ContentBundle();
        bundle.originId = originId;
        return bundle;
    }

    static FilteringBundle buildFilteringBundle() {
        FilteringBundle bundle = new FilteringBundle();
        bundle.relevance = new Map<Id, Integer> {
                testContents[0].Id => 10
        };

        return bundle;
    }

    static JsonMessage.ApiResponse buildApiResponse(Object responseObject) {
        JsonMessage.ApiResponse response = new JsonMessage.ApiResponse();
        response.isSuccess = true;
        response.responseObject = Json.serialize(responseObject);
        return response;
    }

    static testmethod void testGetResults() {
        buildTestContent();

        FilteringBundle filterBundle = buildFilteringBundle();
        JsonMessage.ApiResponse filterResponse = buildApiResponse(filterBundle);

        cms.ServiceEndPoint.addMockResponse('FilteringApi', Json.serialize(filterResponse));

        String resultsJson = ResearcherPortalOcmsSearchController.getResults(SEARCH_TERM);
        ResearcherPortalOcmsSearchController.SearchResults results =
                (ResearcherPortalOcmsSearchController.SearchResults)Json.deserialize(resultsJson, ResearcherPortalOcmsSearchController.SearchResults.class);

        System.assertEquals(1, results.searchData.size(), 'Should return 1 result');
        System.assertEquals(testContents[0].Id, results.searchData[0].Content__c, 'Should return the existing content');
    }

    static testmethod void testGetKeywords() {
        RenderResultBundle renderResultBundle = new RenderResultBundle();

        renderResultBundle.renderings = new RenderResultBundle.RenderedContent[] {};
        renderResultBundle.renderings.add(new RenderResultBundle.RenderedContent());
        renderResultBundle.renderings[0].renderMap = new Map<String, String>();
        renderResultBundle.renderings[0].renderMap.put('TaxonomyMenu', '<div class="taxonomy" ><ul class="list root-level-list"><li class="listitem root-level-item first last branch"><span>Site Areas</span><ul class="list level-1-list"><li class="listitem level-1-mapitem first"><span>Home Page</span></li><li class="listitem level-1-mapitem"><span>Idea</span></li><li class="listitem level-1-mapitem"><span>Plan</span></li><li class="listitem level-1-mapitem"><span>Fund</span></li><li class="listitem level-1-mapitem"><span>Conduct</span></li><li class="listitem level-1-mapitem"><span>Communicate</span></li><li class="listitem level-1-mapitem last"><span>Impact</span></li></ul></li></ul></div><div class="taxonomy-end"></div>');

        JsonMessage.ApiResponse renderResponse = buildApiResponse(renderResultBundle);
        cms.ServiceEndPoint.addMockResponse('OrchestraRenderingApi', Json.serialize(renderResponse));

        String[] keywords = ResearcherPortalOcmsSearchController.getKeywords();

        System.assertEquals(7, keywords.size(), 'Should return 7 keywords');
    }

    static testmethod void testGetResultPage() {
        buildTestContent();

        RenderResultBundle renderResultBundle = new RenderResultBundle();
        renderResultBundle.renderings = new RenderResultBundle.RenderedContent[] {};
        renderResultBundle.renderings.add(new RenderResultBundle.RenderedContent());
        renderResultBundle.renderings[0].contentBundle = buildContentBundle(testContents[0].Id);

        JsonMessage.ApiResponse renderResponse = buildApiResponse(renderResultBundle);
        cms.ServiceEndPoint.addMockResponse('OrchestraRenderingApi', Json.serialize(renderResponse));

        String contentBundlesJson = ResearcherPortalOcmsSearchController.getResultPage(new Id[] { testContents[0].Id });
        ContentBundle[] contentBundles = (ContentBundle[])Json.deserialize(contentBundlesJson, List<ContentBundle>.class);

        System.assertEquals(1, contentBundles.size(), 'Should return 1 bundle');
        System.assertEquals(testContents[0].Id, contentBundles[0].originId, 'Should return the requested origin ID');
    }
}