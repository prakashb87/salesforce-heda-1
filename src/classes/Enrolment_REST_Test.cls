/*********************************************************************************
*** @TestClassName     : Enrolment_REST_Test
*** @Author            : Shubham Singh
*** @Requirement       : To Test the Admission_REST web service apex class.
*** @Created date      : 06/06/2018
*** @modified date     : 06/06/2018
**********************************************************************************/
@isTest
public class Enrolment_REST_Test {
    static testMethod void myUnitTest() {
        //creating test data
        //insert contacts
        Contact con1 = new Contact(FirstName = 'TestCon', LastName = 'Test', Student_ID__c = '88002P');
        insert con1;
        Contact con2 = new Contact(FirstName = 'Testing', LastName = 'TestNew', Student_ID__c = '98765D');
        insert con2;
        Contact con3 = new Contact(FirstName = 'Testing', LastName = 'TestNew', Student_ID__c = '987656D');
        insert con3;
        Contact con4 = new Contact(FirstName = 'Testing', LastName = 'TestNew', Student_ID__c = '987657D');
        insert con4;
        //insert rmitu account
        Account rmit = new Account(Name = 'RMITU');
        insert rmit;
        //insert accounts Effective_Date__c(YY-MM-DD)
        Account acc1 = new Account(Name = 'Program Test 1', Academic_Institution__c = rmit.ID, AccountNumber = 'AN0001', Effective_Date__c = Date.ValueOf('1996-03-04'), Academic_Career__c = 'PGAG');
        insert acc1;
        Account acc2 = new Account(Name = 'Program Test 2', Academic_Institution__c = rmit.ID, AccountNumber = 'AN0001', Effective_Date__c = Date.ValueOf('1996-04-04'), Academic_Career__c = 'PGAG');
        insert acc2;
        Account acc3 = new Account(Name = 'Program Test 3', Academic_Institution__c = rmit.ID, AccountNumber = 'AN0001', Effective_Date__c = Date.ValueOf('1996-05-04'),Academic_Career__c = 'PGAG');
        insert acc3;
        //insert pe for enrolment
        hed__Program_Enrollment__c pe = new hed__Program_Enrollment__c(Institution__c = rmit.ID,hed__Contact__c = con2.ID,Academic_Program__c = '9874B',Career__c = 'PGAG');
        insert pe;
        //insert term
        Term__c trm = new Term__c();
        trm.Institution__c = rmit.Id;
        trm.Term_Code__c = '1210';
        insert trm;
        //insert term session
        hed__Term__c term = new hed__Term__c(Term_Code__c = trm.ID ,Academic_Career__c = 'PGAG',hed__Account__c = rmit.ID,hed__Start_Date__c = Date.ValueOf('1996-05-05'));
        insert term;
        //insert find source
        Fund_Source__c fundsource = new Fund_Source__c(Name = 'AAB',Effective_Date__c = Date.ValueOf('1995-03-04'),Description__c = 'this is the fund source description');
        insert fundsource;
        //insert course
        hed__Course__c course = new hed__Course__c(Name = 'Engineering',hed__Account__c = acc2.ID);
        insert course;
        //insert course offering                                               
        hed__Course_Offering__c courseOffrng = new hed__Course_Offering__c(hed__Course__c = course.ID,Class_Number__c = '15867',hed__Term__c = term.ID);
        insert courseOffrng;
        
        RestRequest req = new RestRequest();
        
        RestResponse res = new RestResponse();
        
        // adding header to the request
        req.addHeader('httpMethod', 'POST');
        
        req.requestUri = '/services/apexrest/HEDA/v1/StudentEnrolment';
        
        String JsonMsg = '[{"studentId": "88002P","academicCareer": "PGRD","institution": "RMITU","term": "1310","classNumber": 1554,"session": "RAS","status": "D","statusReason": "DROP","lastAction": "D","lastActionReason": "WDRW","statusDate": "2013-01-15","addDate": "2013-01-15","dropDate": "2013-03-13","unitsTaken": 12,"progressUnits": 12,"billingUnits": 12,"gradingBasis": "G00","gradingBasisDate": "2013-01-15","officialGrade": "","gradeInput": "","gradeDate": "","academicProgram": "GD110","hecsExemptionStatusCode": "202","fundingSourceCode": "AA","studentCareerNumber": 5},{"studentId": "98765D","academicCareer": "PGRD","institution": "RMITU","term": "1310","classNumber": 1554,"session": "RAS","status": "D","statusReason": "DROP","lastAction": "D","lastActionReason": "WDRW","statusDate": "2013-01-15","addDate": "2013-01-15","dropDate": "2013-03-13","unitsTaken": 12,"progressUnits": 12,"billingUnits": 12,"gradingBasis": "G00","gradingBasisDate": "2013-01-15","officialGrade": "","gradeInput": "","gradeDate": "","academicProgram": "GD110","hecsExemptionStatusCode": "202","fundingSourceCode": "AA","studentCareerNumber": 5},{"studentId": "987656D","academicCareer": "PGRD","institution": "RMITU","term": "1310","classNumber": 1554,"session": "RAS","status": "D","statusReason": "DROP","lastAction": "D","lastActionReason": "WDRW","statusDate": "2013-01-15","addDate": "2013-01-15","dropDate": "2013-03-13","unitsTaken": 12,"progressUnits": 12,"billingUnits": 12,"gradingBasis": "G00","gradingBasisDate": "2013-01-15","officialGrade": "","gradeInput": "","gradeDate": "","academicProgram": "GD110","hecsExemptionStatusCode": "202","fundingSourceCode": "AA","studentCareerNumber": 5},{"studentId": "987657D","academicCareer": "PGRD","institution": "RMITU","term": "1310","classNumber": 1554,"session": "RAS","status": "D","statusReason": "DROP","lastAction": "D","lastActionReason": "WDRW","statusDate": "2013-01-15","addDate": "2013-01-15","dropDate": "2013-03-13","unitsTaken": 12,"progressUnits": 12,"billingUnits": 12,"gradingBasis": "G00","gradingBasisDate": "2013-01-15","officialGrade": "","gradeInput": "","gradeDate": "","academicProgram": "GD110","hecsExemptionStatusCode": "202","fundingSourceCode": "AA","studentCareerNumber": 5},{"studentId": "0009007","academicCareer": "PGRD","institution": "RMITU","term": "1310","classNumber": 1554,"session": "RAS","status": "D","statusReason": "DROP","lastAction": "D","lastActionReason": "WDRW","statusDate": "2013-01-15","addDate": "2013-01-15","dropDate": "2013-03-13","unitsTaken": 12,"progressUnits": 500000,"billingUnits": 12,"gradingBasis": "G00","gradingBasisDate": "2013-01-15","officialGrade": "","gradeInput": "","gradeDate": "","academicProgram": "GD110","hecsExemptionStatusCode": "202","fundingSourceCode": "AA","studentCareerNumber": 5}] ';        
        
        req.requestBody = Blob.valueof(JsonMsg);
        
        RestContext.request = req;
        
        RestContext.response = res;
        
        Test.startTest();
        //store response
        Map < String, String > check = Enrolment_REST.upsertEnrolment();
        List < String > str = check.values();
        //assert condition 
        System.assertEquals(true, str[0] == 'ok');
        
        Test.stopTest();
    }
    static testMethod void myUnitTest2 (){
        //creating test data
        //insert contacts
        Contact con1 = new Contact(FirstName = 'Enroll', LastName = 'Rest Test', Student_ID__c = '88002P');
        insert con1;
        Contact con2 = new Contact(FirstName = 'Enroll1', LastName = 'Rest Test1', Student_ID__c = '98765D');
        insert con2;
        //insert rmitu account
        Account rmit = new Account(Name = 'RMITU');
        insert rmit;
        //insert accounts Effective_Date__c(YY-MM-DD)
        Account acc1 = new Account(Name = 'Program Test 3', Academic_Institution__c = rmit.ID, AccountNumber = 'GD110', Effective_Date__c = Date.ValueOf('1996-05-04'),Academic_Career__c = 'PGRD');
        insert acc1;
        //insert program enrollment
        hed__Program_Enrollment__c pe = new hed__Program_Enrollment__c(Institution__c = rmit.ID,hed__Contact__c = con1.ID,Academic_Program__c = 'GD110',Career__c = 'PGRD',Student_Career_Number__c = '1');
        insert pe;
        //insert HECS
        HECS__c hcs = new HECS__c(Name = '202');
        insert hcs;
        //insert term for program enrolment
        Term__c trm = new Term__c();
        trm.Institution__c = rmit.Id;
        trm.Term_Code__c = '1310';
        insert trm;
        //insert term session
        hed__Term__c term = new hed__Term__c(Name = 'Test Term',Term_Code__c = trm.ID,Academic_Career__c = 'PGAG',hed__Account__c = rmit.ID,hed__Start_Date__c = Date.ValueOf('1996-05-05'));
        insert term;
        //insert fund source
        Fund_Source__c fundsource = new Fund_Source__c(Name = 'AA',Effective_Date__c = Date.ValueOf('1995-03-04'),Description__c = 'this is the fund source description');
        insert fundsource;
        //insert course for course offering
        /*hed__Course__c course = new hed__Course__c();
        course.hed__Account__c = acc1.id;
        course.Name = 'Test Course';
        insert course;
        //insert coourse offering
        hed__Course_Offering__c hedCourseOffr = new hed__Course_Offering__c();
        hedCourseOffr.hed__Course__c = course.ID;
        hedCourseOffr.Name = 'Test Course Offering';
        hedCourseOffr.hed__Term__c = term.ID;
        hedCourseOffr.Class_Number__c = '1554';
        insert hedCourseOffr;
        */
        hed__Course_Enrollment__c hedCourseEnroll = new hed__Course_Enrollment__c(Official_Grade__c = 'AB',Academic_Career__c = 'PGRD',Institution__c = rmit.ID,hed__Account__c = acc1.Id,Class_Number__c = '1554',Term__c = '1310',hed__Contact__c = con1.ID,Course_Connection_Unique_Key__c = 'PGRD1554131088002PRMITU');
        insert hedCourseEnroll;
        //request and response variables
        RestRequest req = new RestRequest();
        
        RestResponse res = new RestResponse();
        
        // adding header to the request
        req.addHeader('httpMethod', 'POST');
        
        req.requestUri = '/services/apexrest/HEDA/v1/StudentEnrolment';
        
        String JsonMsg = '[{"studentId": "88002P","academicCareer": "PGRD","institution": "RMITU","term": "1310","classNumber": 1554,"session": "RAS","status": "D","statusReason": "DROP","lastAction": "D","lastActionReason": "WDRW","statusDate": "2013-01-15","addDate": "2013-01-15","dropDate": "2013-03-13","unitsTaken": 12,"progressUnits": 12,"billingUnits": 12,"gradingBasis": "G00","gradingBasisDate": "2013-01-15","officialGrade": "AA","gradeInput": "","gradeDate": "2013-01-15","academicProgram": "GD110","hecsExemptionStatusCode": "202","fundingSourceCode": "AA","studentCareerNumber": 1},{"studentId": "98765D","academicCareer": "PGRD","institution": "RMITU","term": "1310","classNumber": 1554,"session": "RAS","status": "D","statusReason": "DROP","lastAction": "D","lastActionReason": "WDRW","statusDate": "2013-01-15","addDate": "2013-01-15","dropDate": "2013-03-13","unitsTaken": 12,"progressUnits": 12,"billingUnits": 12,"gradingBasis": "G00","gradingBasisDate": "2013-01-15","officialGrade": "","gradeInput": "","gradeDate": "","academicProgram": "GD110","hecsExemptionStatusCode": "202","fundingSourceCode": "AA","studentCareerNumber": 5},{"studentId": "987656D","academicCareer": "PGRD","institution": "RMITU","term": "1310","classNumber": 1554,"session": "RAS","status": "D","statusReason": "DROP","lastAction": "D","lastActionReason": "WDRW","statusDate": "2013-01-15","addDate": "2013-01-15","dropDate": "2013-03-13","unitsTaken": 12,"progressUnits": 12,"billingUnits": 12,"gradingBasis": "G00","gradingBasisDate": "2013-01-15","officialGrade": "","gradeInput": "","gradeDate": "","academicProgram": "GD110","hecsExemptionStatusCode": "202","fundingSourceCode": "AA","studentCareerNumber": 5},{"studentId": "987657D","academicCareer": "PGRD","institution": "RMITU","term": "1310","classNumber": 1554,"session": "RAS","status": "D","statusReason": "DROP","lastAction": "D","lastActionReason": "WDRW","statusDate": "2013-01-15","addDate": "2013-01-15","dropDate": "2013-03-13","unitsTaken": 12,"progressUnits": 12,"billingUnits": 12,"gradingBasis": "G00","gradingBasisDate": "2013-01-15","officialGrade": "","gradeInput": "","gradeDate": "","academicProgram": "GD110","hecsExemptionStatusCode": "202","fundingSourceCode": "AA","studentCareerNumber": 5},{"studentId": "0009007","academicCareer": "PGRD","institution": "RMITU","term": "1310","classNumber": 1554,"session": "RAS","status": "D","statusReason": "DROP","lastAction": "D","lastActionReason": "WDRW","statusDate": "2013-01-15","addDate": "2013-01-15","dropDate": "2013-03-13","unitsTaken": 12,"progressUnits": 500000,"billingUnits": 12,"gradingBasis": "G00","gradingBasisDate": "2013-01-15","officialGrade": "","gradeInput": "","gradeDate": "","academicProgram": "GD110","hecsExemptionStatusCode": "202","fundingSourceCode": "AA","studentCareerNumber": 5}] ';        
        
        req.requestBody = Blob.valueof(JsonMsg);
        
        RestContext.request = req;
        
        RestContext.response = res;
        
        Test.startTest();
        //store response
        Map < String, String > check = Enrolment_REST.upsertEnrolment();
        List < String > str = check.values();
        //assert condition 
        //System.assertEquals(true, str[0] == 'ok');
        
        Test.stopTest();
    } 
    static testMethod void coverTryCatch() {
        Map < String, String > check;
        Test.startTest();
        try {
            //store response
            check = Enrolment_REST.upsertEnrolment();
        } catch (DMLException e) {
            List < String > str = check.values();
            //assert condition 
            System.assertEquals(False, str[0] == 'ok');
        }
        Test.stopTest();
    }
    
}