/*****************************************************************************************************
*** @Class             : CourseConnectionIntegrationtoCanvasTest
*** @Author            : Avinash Machineni 
*** @Requirement       : Integration between Salesforce and IPaaS for sending Contact details
*** @Created date      : 14/06/2018
*** @Updated date      : 03/08/2018 - Rishabh
*** @JIRA ID           : ECB-367,ECB-3900
*****************************************************************************************************/
/*****************************************************************************************************
*** @About Class
*** This class is used to integrate between Salesforce and IPaaS for sending course SIS Id, Course Offering Id's
*****************************************************************************************************/

@isTest
public class CourseConnectionIntegrationtoCanvasTest {
    
    Static testmethod void testCanvasSuccess(){
        
        TestDataFactoryUtilRefOne.testCreateDataforCanvasSuccess();
        Test.setMock(HttpCalloutMock.class, new canvasCalloutMockPositive());
        
        String jsonBody = '{"payload":{"studentId":"sis_user_id:12356","enrolmentResponse":[{"courseConnectionId":"'+TestDataFactoryUtil.cern2.id+'","result":"Success","html_url":"https://rmit.test.instructure.com/courses/20445/users/12356","errorMessage":""},{"courseConnectionId":"'+TestDataFactoryUtil.cern3.id+'","result":"Error","html_url":null,"errorMessage":"The specified resource does not exist."}]}}';
        
        Contact con = [SELECT Id,
                       Name,
                       Student_ID__c,
                       hed__UniversityEmail__c,
                       (SELECT ID, hed__Course_Offering__r.Name, hed__Course_Offering__c FROM hed__Student_Course_Enrollments__r) FROM Contact ];
        
        
        Set<id> crsenrId =new Set<id>();
        for(hed__Course_Enrollment__c cenr : con.hed__Student_Course_Enrollments__r){
            crsenrId.add(cenr.id);
        }
        
        System.runAs(TestDataFactoryUtil.usr4){
            Test.startTest();
            CourseConnectionIntegrationtoCanvas.sendCourseConnIds(crsenrId);
            EmbeddedPdfAndIntegrationController.sendCourseConnIds(crsenrId);
            Test.stopTest();
        }
        
        Course_Connection_Life_Cycle__c lifeCycle = [SELECT  Id, Stage__c, Status__c FROM Course_Connection_Life_Cycle__c ];
        System.assertEquals('Success',lifeCycle.Status__c);
        
        list<Integration_Logs__c> dbResults = [select id, Service_Type__c, Type__c,Request_Response__c from Integration_Logs__c where Service_Type__c = 'Acknowledgement'];
        System.assertEquals(2, dbResults.size());
        System.assertEquals(jsonBody, dbResults[0].Request_Response__c);
        
        list<Integration_Logs__c> dbResults2 = [select id, Service_Type__c, Type__c,Request_Response__c from Integration_Logs__c where Service_Type__c = 'Outbound Service'];
        System.assertEquals(2, dbResults2.size());
        
    }  
    
    Static testmethod void testCanvasError(){
        
        TestDataFactoryUtilRefOne.testCreateDataforCanvasError();
        Test.setMock(HttpCalloutMock.class, new canvasCalloutMockNegative());
        String jsonBody ='{"payload":{"studentId":"sis_user_id:12356","enrolmentResponse":[{"courseConnectionId":"'+TestDataFactoryUtil.cern2.id+'","result":"Error","html_url":"https://rmit.test.instructure.com/courses/20445/users/12356","errorMessage":""},{"courseConnectionId":"'+TestDataFactoryUtil.cern3.id+'","result":"Error","html_url":null,"errorMessage":"The specified resource does not exist."}]}}';
        
        Contact con = [SELECT Id,
                       Name,
                       Student_ID__c,
                       hed__UniversityEmail__c,
                       (SELECT ID, hed__Course_Offering__r.Name, hed__Course_Offering__c FROM hed__Student_Course_Enrollments__r) FROM Contact ];
        
        
        Set<id> crsenrId =new Set<id>();
        for(hed__Course_Enrollment__c cenr : con.hed__Student_Course_Enrollments__r){
            crsenrId.add(cenr.id);
        }
        
        System.runAs(TestDataFactoryUtil.usr4){  
            Test.startTest();
            CourseConnectionIntegrationtoCanvas.sendCourseConnIds(crsenrId);
            Course_Connection_Life_Cycle__c lifeCycle = [SELECT  Id, Stage__c, Status__c FROM Course_Connection_Life_Cycle__c ];
            System.assertEquals('Initiated',lifeCycle.Status__c);
            EmbeddedPdfAndIntegrationController.sendCourseConnIds(crsenrId);
            Test.stopTest();
        }
        
        Course_Connection_Life_Cycle__c courselifeCycle = [SELECT  Id, Stage__c, Status__c FROM Course_Connection_Life_Cycle__c ];
        System.assertEquals('Error',courselifeCycle.Status__c);
        
        List<Integration_Logs__c> intloglist = [select id, Service_Type__c, Type__c,Request_Response__c from Integration_Logs__c where Service_Type__c = 'Acknowledgement'];
        system.assertEquals(2, intloglist.size());
        
        system.assertEquals(jsonBody, intloglist[0].Request_Response__c);
        
        list<Integration_Logs__c> listinteglog = [select id, Service_Type__c, Type__c,Request_Response__c from Integration_Logs__c where Service_Type__c = 'Outbound Service'];
        system.assertEquals(2, listinteglog.size());
        
    }  
    
    
    public class canvasCalloutMockPositive implements HttpCalloutMock {
        
        public HTTPResponse respond(HTTPRequest request) {
            
            String jsonBody ='{"payload":{"studentId":"sis_user_id:12356","enrolmentResponse":[{"courseConnectionId":"'+TestDataFactoryUtil.cern2.id+'","result":"Success","html_url":"https://rmit.test.instructure.com/courses/20445/users/12356","errorMessage":""},{"courseConnectionId":"'+TestDataFactoryUtil.cern3.id+'","result":"Error","html_url":null,"errorMessage":"The specified resource does not exist."}]}}';
            HttpResponse response = new HttpResponse();
            response.setHeader('Content-Type', 'application/json');
            response.setBody(jsonBody);
            response.setStatusCode(200);
            return response;
        }
        
    }
    
    public class canvasCalloutMockNegative implements HttpCalloutMock {
        
        public HTTPResponse respond(HTTPRequest request) {
            
            String jsonBody ='{"payload":{"studentId":"sis_user_id:12356","enrolmentResponse":[{"courseConnectionId":"'+TestDataFactoryUtil.cern2.id+'","result":"Error","html_url":"https://rmit.test.instructure.com/courses/20445/users/12356","errorMessage":""},{"courseConnectionId":"'+TestDataFactoryUtil.cern3.id+'","result":"Error","html_url":null,"errorMessage":"The specified resource does not exist."}]}}';
            HttpResponse response = new HttpResponse();
            response.setHeader('Content-Type', 'application/json');
            response.setBody(jsonBody);
            response.setStatusCode(400);
            return response;
        }
        
    }
    
}