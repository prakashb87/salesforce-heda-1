/*****************************************************************
Name: ProductBasketControllerTest
Author: Capgemini 
Purpose: Test Class For ProductBasketController
*****************************************************************/
/*==================================================================
History
--------
Version   Author                     Date              Detail
1.0       Rishabh Anand             07/05/2018         Initial Version
********************************************************************/

@isTest
public class ProductBasketControllerTest {
    
    /**
     * @description Creates test data required for unit tests. This includes 
     * custom setting data creation.
     * Future test data should be added here instead of TestDataFactoryUtil.
     */
    @testSetup
    static void makeData(){

        /**
         * Most test methods in this class are creating the CloudSense messages
         * using TestDataFactoryUtil. However, as the new code shouldn't be added
         * to TestDataFactoryUtil (code issues, PMD violations etc...), the one 
         * message that is missing is added here. 
         *
         * The use of the @testSetup also reduces the test class execution time
         * Future changes should be added here instead of TestDataFactoryUtil.
         */
        List<csapi_Message__c> cloudSenseMessages = new List<csapi_Message__c>();
        csapi_Message__c deleteProductFailMessage = new csapi_Message__c();
        deleteProductFailMessage.name = 'CSAPI_PROD_DELETE_FAIL';
        deleteProductFailMessage.Status__c = 'Fail';
        deleteProductFailMessage.Message__c = 'CSAPI_PROD_DELETE_FAIL';
        deleteProductFailMessage.Code__c = 'CSAPI_FAIL_0004';
        cloudSenseMessages.add(deleteProductFailMessage);

        insert cloudSenseMessages;
    }

    
    /********************************************************************
// Purpose              : Test Class for the getresult() method 
// Author               : Capgemini 
// Parameters           : null
// Returns              : void 
//JIRA Reference        : ECB-330 : Communities - Building basket page
//********************************************************************/
    @isTest
    public static void getresultTest(){
        
        Test.startTest();
        TestDataFactoryUtil.testCreateData();
        
        csapi_BasketRequestWrapper.csapi_CourseDetails details = new csapi_BasketRequestWrapper.csapi_CourseDetails();
        details.CourseName = TestDataFactoryUtil.course.Name;
        details.CourseType = 'Course';
        details.ProductReference = 'Test definition';
        details.Fees = '100.0';
        details.CourseOfferingId = TestDataFactoryUtil.courseOffering.Id;
        details.SessionStartDate = string.valueOf(TestDataFactoryUtil.courseOffering.hed__Start_Date__c);
        details.SessionEndDate = string.valueOf(TestDataFactoryUtil.courseOffering.hed__End_Date__c);
        
        csapi_BasketRequestWrapper.AddProductRequest request = new csapi_BasketRequestWrapper.AddProductRequest();
        request.Course = details;
        request.IsBatchProcessed = false;
        
        csapi_BasketRequestWrapper.AddProductResponse response = csapi_RestBasketExtension_AddProduct.doPost(request);
        system.debug('response >> ' + response);
        
        system.assertEquals(response.Status, 'Pass');
        
        //get basket info
        csapi_BasketRequestWrapper.BasketInfoRequest request2 = new csapi_BasketRequestWrapper.BasketInfoRequest();
        csapi_BasketRequestWrapper.BasketInfoResponse response2 = ProductBasketController.getresult(); 
        
        //assert its created
        system.assertEquals(response2.Status, 'Pass');
        
        Test.stopTest();
        
    }
    
    /********************************************************************
// Purpose              : Test Class for the deleteProduct() method 
// Author               : Capgemini 
// Parameters           : null
// Returns              : void 
//JIRA Reference        : ECB-2463 : Delete from basket and refresh
//********************************************************************/   
    @isTest
    public static void deleteProductTest(){
        
        Test.startTest();
        TestDataFactoryUtil.testCreateData();
        csapi_BasketRequestWrapper.csapi_CourseDetails details = new csapi_BasketRequestWrapper.csapi_CourseDetails();
        details.CourseName = TestDataFactoryUtil.course.Name;
        details.CourseType = 'Course';
        details.ProductReference = 'Test definition';
        details.Fees = '100.0';
        details.CourseOfferingId = TestDataFactoryUtil.courseOffering.Id;
        details.SessionStartDate = string.valueOf(TestDataFactoryUtil.courseOffering.hed__Start_Date__c);
        details.SessionEndDate = string.valueOf(TestDataFactoryUtil.courseOffering.hed__End_Date__c);
        
        csapi_BasketRequestWrapper.AddProductRequest request = new csapi_BasketRequestWrapper.AddProductRequest();
        request.Course = details;
        request.IsBatchProcessed = false;
        
        csapi_BasketRequestWrapper.AddProductResponse response = csapi_RestBasketExtension_AddProduct.doPost(request);
        system.debug('response >> ' + response);
        
        system.assertEquals(response.Status, 'Pass');
        
        //delete product
        csapi_BasketRequestWrapper.csapi_CourseDetails details2 =new csapi_BasketRequestWrapper.csapi_CourseDetails();
        details2.CourseName='Test definition'; 
        
        csapi_BasketRequestWrapper.DeleteProductRequest request2 = new csapi_BasketRequestWrapper.DeleteProductRequest();
        request2.Course = details2;
        
        csapi_BasketRequestWrapper.BasketInfoResponse response2 = ProductBasketController.deleteProduct(details2.CourseName); 
        
        //assert its deleted
        system.assertEquals(response2.Status, 'Pass');
        Test.stopTest();    
    } 
    
    /********************************************************************
// Purpose              : Test Class for the getEmail() method 
// Author               : Capgemini 
// Parameters           : null
// Returns              : void 
//JIRA Reference        : ECB-142 : Delete from basket and refresh
//********************************************************************/ 
    @isTest
    public static void getEmailTest(){
        
        Test.startTest();
        String useremail=UserInfo.getUserEmail();
        String email = ProductBasketController.getEmail();
        system.assertEquals(email, useremail);
        Test.stopTest();
    }
    
    
    /********************************************************************
// Purpose              : Test Class for the addProducttoBasket() method 
// Author               : Capgemini 
// Parameters           : null
// Returns              : void 
//JIRA Reference        : ECB-2491 : Communities - Building basket page
//********************************************************************/ 
    @isTest
    public static void testaddProducttoBasket(){
        
        Test.startTest();
        TestDataFactoryUtil.testCreateData();
        
        csapi_BasketRequestWrapper.csapi_CourseDetails details = new csapi_BasketRequestWrapper.csapi_CourseDetails();
        details.CourseName = TestDataFactoryUtil.course.Name;
        details.CourseType = 'Course';
        details.ProductReference = 'Test definition';
        details.CourseOfferingId = TestDataFactoryUtil.courseOffering.Id;
        details.SessionStartDate = string.valueOf(TestDataFactoryUtil.courseOffering.hed__Start_Date__c);
        details.SessionEndDate = string.valueOf(TestDataFactoryUtil.courseOffering.hed__End_Date__c);
        
        csapi_BasketRequestWrapper.AddProductRequest request = new csapi_BasketRequestWrapper.AddProductRequest();
        request.Course = details;
        request.IsBatchProcessed = false;
        
        csapi_BasketRequestWrapper.AddProductResponse response = ProductBasketController.addProducttoBasket(details.CourseName,details.CourseOfferingId, details.ProductReference,details.SessionEndDate,details.SessionStartDate,details.deliveryMode,details.productLine,details.CourseType,'');
        system.debug('response >> ' + response);
        
        system.assertEquals(response.Status, 'Pass');
        Test.stopTest();
        
    }
    
    /********************************************************************
// Purpose              : Test Class for the confirmEnroll() method 
// Author               : Capgemini 
// Parameters           : null
// Returns              : void 
//JIRA Reference        : ECB-2638 : Processing free carts directly from communities
//********************************************************************/    
    @isTest
    public static void testconfirmEnroll(){
        
        Test.startTest();
        TestDataFactoryUtil.test();
        
        TestDataFactoryUtil.testCreateData();
        
        cscfga__Product_Basket__c prodBasket = [Select Id,Basket_Number__c from cscfga__Product_Basket__c where Name = 'New Basket'];
        
        csapi_BasketRequestWrapper.SyncBasketResponse response = ProductBasketController.confirmEnroll('R1234', prodBasket.Basket_Number__c);
        system.assertEquals(response.Status, 'Pass');
        
        Test.stopTest();
    }
    
    /********************************************************************
// Purpose              : Test Class for the getProgressDetails() method 
// Author               : Capgemini 
// Parameters           : null
// Returns              : void 
//JIRA Reference        : ECB-2685:  My progress page
//********************************************************************/        
    @isTest
    public static void testgetProgressDetails(){
        
        Test.startTest();
        
        TestDataFactoryUtilRefOne.dataFactory();
        
        TestDataFactoryUtilRefOne.testDataFactory();
        
        system.runAs(TestDataFactoryUtil.testUser){
            List<hed__Course_Enrollment__c> enrollmentList = new  List<hed__Course_Enrollment__c> ();
            enrollmentList = ProductBasketController.getProgressDetails();
            System.debug('List---->'+enrollmentList);
            System.assertEquals(enrollmentList.size()>0,true);
        }
        
        
        Test.stopTest();
    }
    /********************************************************************
// Purpose              : Test Class for the getMyDetails() method 
// Author               : Capgemini 
// Parameters           : null
// Returns             : void 
//JIRA Reference        : ECB-2687:  Personal Details page
//********************************************************************/     
    @isTest
    public static void testgetMyDetails(){
        
        Account acc = new Account(name='test');
        insert acc;
        Contact con = new Contact(LastName='TestCon',AccountId=acc.id);
        insert con;
        
        User u = new User(
            ProfileId = [SELECT Id FROM Profile WHERE Name = 'Customer Community Login User - RMIT'].Id,
            LastName = 'last',
            Email = 'puser000@amamama.com',
            Username = 'puser000@amamama.com' + System.currentTimeMillis(),
            CompanyName = 'TEST',
            Title = 'title',
            Alias = 'alias',
            TimeZoneSidKey = 'America/Los_Angeles',
            EmailEncodingKey = 'UTF-8',
            LanguageLocaleKey = 'en_US',
            LocaleSidKey = 'en_US',
            ContactId = con.id
        );
        insert u; 
        
        Test.startTest();
        system.runAs(u){
            Contact contactList = new  Contact ();
            contactList = ProductBasketController.getMyDetails();
            
            System.assertEquals(u.ContactId,con.id);}
        Test.stopTest();
    } 
    /********************************************************************
// Purpose              : Test Class for the getMyPurchaseDetails() method 
// Author               : Capgemini 
// Parameters           : null
// Returns              : void 
//JIRA Reference        : ECB-2686:Purchase history page
//********************************************************************/   
    @isTest
    public static void testgetMyPurchaseDetails(){
        
        Test.startTest();
        
        TestDataFactoryUtilRefOne.dataFactory();
        TestDataFactoryUtilRefOne.testDataFactory();             
        
        system.runAs(TestDataFactoryUtil.testUser){
            List<PaymentWrapper> wrapperList = new List<PaymentWrapper>();   
            wrapperList = ProductBasketController.getMyPurchaseDetails();
            
            System.assertEquals(wrapperList.size()>0, true);
        }
        Test.stopTest();
    }
    
    /********************************************************************
// Purpose              : Test Class for the getShoppingURL(),getViewCartErrorURL(),geCanvasRedirectURL() method 
// Author               : Capgemini 
// Parameters           : null
// Returns              : void 
//JIRA Reference        : ECB-4308
********************************************************************/   
    @isTest
    public static void testgetCustomSettingValues(){
        
        Test.startTest();
        
        TestDataFactoryUtil.dummycustomsetting();           
        
        String shoppingUrl = ProductBasketController.getShoppingURL();
        System.assertEquals(shoppingUrl!=null, true);
        
        String cartFailUrl = ProductBasketController.getViewCartErrorURL();
        System.assertEquals(cartFailUrl!=null, true);
        
        String canvasUrl = ProductBasketController.geCanvasRedirectURL();
        System.assertEquals(canvasUrl!=null, true);
        
        String tCURL = ProductBasketController.getTCURL();
        System.assertEquals(tCURL!=null, true);        
        
        String onestopURL = ProductBasketController.getOnestopURL();
        System.assertEquals(onestopURL!=null, true);  
        
        String communityUrl = ProductBasketController.getCommunityURL();
        System.assertEquals(communityUrl!=null, true);  
        
        String successpageUrl = ProductBasketController.getSuccessPageURL();
        System.assertEquals(successpageUrl!=null, true);  
        
        
        String height = ProductBasketController.getHeight();
        System.assertEquals(height!=null, true);
        
        // ECB-4962 Changes 
        Config_Data_Map__c cs= new Config_Data_Map__c();
        cs.Name = 'ServiceAndSupportURL';
        cs.Config_Value__c = 'https://rmit.service-now.com/connect/?id=sc_home';
        insert cs;
        
        String systemURL = ProductBasketController.getServiceAndSupportURL();
        System.assertEquals(systemURL!=null, true);
        
        
        Test.stopTest();
    } 
    
    /********************************************************************
// Purpose              : Test Class for the updatedProduct method 
// Author               : Capgemini 
// Parameters           : null
// Returns              : void 
//JIRA Reference        : ECB-4505 : As a student, I should be able to select a delivery mode in 'My cart' and the pricing automatically update to reflecting the new list price of the product
//********************************************************************/   
    @isTest
    public static void testupdatedProduct(){
        
        Test.startTest();
        TestDataFactoryUtil.setupUpdateProductData();
        
        cscfga__Product_Configuration__c testProdConfig=[select Id, Name from cscfga__Product_Configuration__c limit 1];
        csapi_BasketRequestWrapper.csapi_CourseDetails details = new csapi_BasketRequestWrapper.csapi_CourseDetails();
        details.CourseName = TestDataFactoryUtil.course.Name;
        details.CourseType = 'Program';
        details.DeliveryMode='All';
        details.ProductId=testProdConfig.Id;
        
        csapi_BasketRequestWrapper.UpdateProductRequest request=new csapi_BasketRequestWrapper.UpdateProductRequest();
        request.Course=details;
        request.IsBatchProcessed = false;
        
        csapi_BasketRequestWrapper.UpdateProductResponse response =csapi_BasketExtension.updateProduct(request);
        
        csapi_BasketRequestWrapper.BasketInfoRequest request2 = new csapi_BasketRequestWrapper.BasketInfoRequest();
        csapi_BasketRequestWrapper.BasketInfoResponse response2 = ProductBasketController.updatedProduct(TestDataFactoryUtil.course.Name,'All'); 
        
        //assert its created
        system.assertEquals(response2.Status, 'Pass');
        
        
    }
    /********************************************************************
// Purpose              : Test Class for the getDeliveryMode() method 
// Author               : Capgemini 
// Parameters           : null
// Returns              : void 
//JIRA Reference        : ECB-4505 : As a student, I should be able to select a delivery mode in 'My cart' and the pricing automatically update to reflecting the new list price of the product
//********************************************************************/      
    
    @isTest
    public static void testgetDeliveryMode(){
        Test.startTest();
        
        TestDataFactoryUtil.setupUpdateProductData();
        List<DeliveryModeWrapper> wrapperList = new List<DeliveryModeWrapper>();
        wrapperList =  ProductBasketController.getDeliveryMode(); 
        Test.stopTest();
        System.assert(true);
        //System.assertEquals(wrapperList.size()>0,true);
        
    }
    /********************************************************************
// Purpose              : To get the Dynamic  T&C
// Author               : Capgemini [Rishabh]
// Parameters           : String productType
//  Returns             : ProductTC__c
//JIRA Reference        : ECB-4375 : Mapping/Integration and enhancement of existing code
//********************************************************************/ 
    @isTest
    public static void testgetTermsAndConditions(){
        
        String productType = 'test Name';
        Test.startTest();
        
        TestDataFactoryUtil.setupTermsAndConditionData();
        ProductTC__c terms = [SELECT AccountId__c,CourseId__c,Terms_and_ConditionId__r.Long_Description__c,Terms_and_ConditionId__r.Active__c,Type__c FROM ProductTC__c WHERE (CourseId__r.Name =: productType OR AccountId__r.Name =: productType)];
        ProductTC__c product = new ProductTC__c();
        product = ProductBasketController.getTermsAndConditions(productType);
        System.assertEquals(product.Terms_and_ConditionId__r.Active__c, terms.Terms_and_ConditionId__r.Active__c);
        
        Test.stopTest();
    }
    
    /********************************************************************
// Purpose              : To apply promo code
// Author               : Capgemini [String]
// Parameters           : String productType
//  Returns             : ProductTC__c
//JIRA Reference        : ECB-4506 : Communities - As a student, I need to be able to enter in a promo code so that it can be applied to my purchase
//********************************************************************/ 
    @isTest
    public static void testgetAppliedPromocodePrice(){
        
        
        Test.startTest();
        
        TestDataFactoryUtil.setupUpdateProductData();
        
        csapi_BasketRequestWrapper.BasketPromoRequest request = new csapi_BasketRequestWrapper.BasketPromoRequest();
        request.PromoCode='TestPromoCode';
        
        csapi_BasketRequestWrapper.BasketPromoResponse response = ProductBasketController.getAppliedPromocodePrice('TestPromoCode');
        
        System.assertEquals(response!=null, true);
        
        
        Test.stopTest();
    }
    
            
    /********************************************************************
// Purpose              : To validate promo code
// Author               : Capgemini [Maumita]
// Parameters           : String productType
//  Returns             : ProductTC__c
//JIRA Reference        : ECB-5764 : Show the validation error on checkout if the pronocode is not valid
//********************************************************************/ 
    @isTest
    public static void testvalidatePromocode(){
        
        
        Test.startTest();
        
        TestDataFactoryUtil.setupUpdateProductData();
        
        csapi_BasketRequestWrapper.ValidatePromoRequest request = new csapi_BasketRequestWrapper.ValidatePromoRequest();
        request.PromoCode='TestPromoCode';
        
        csapi_BasketRequestWrapper.ValidatePromoResponse response = ProductBasketController.validatePromoCode('TestPromoCode');
        
        System.assertEquals(response!=null, true);
        
        
        Test.stopTest();
    }
    
    @isTest
    public static void getCitizenshipTest(){
        AdditionalInformationController.getCitizenship();
        TestDataFactoryUtilRefOne.deltaTestSuccess();
        //ProductBasketController.getCitizenshipStatus();
        System.assertEquals(true, true);
    }
    
    @isTest
    public static void testSendHashingDetails(){
        
        TestDataFactoryUtil.dummycustomsetting();
        String urlStringParametersWithoutHash='Ref=&ReceiptNo=WP01000521&TotAmt=380.00&MPB=MPB-002766&Status=Successy7iJbfr45621Svl01d5G2i5f';
        String expectedHash ='4f7b6931cc5525d831efe99a0e89c19e';
        
        Test.startTest();
        Boolean result = ProductBasketController.sendHashingDetails(urlStringParametersWithoutHash, expectedHash);
        Test.stopTest();
        
        System.assertEquals(result, true);
        
    }
    
    @isTest
    public static void testgetCitizenshipStatus(){
        
        Test.startTest();
        TestDataFactoryUtil.testCreateData();
        
        csapi_BasketRequestWrapper.csapi_CourseDetails details = new csapi_BasketRequestWrapper.csapi_CourseDetails();
        details.CourseName = TestDataFactoryUtil.course.Name;
        details.CourseType = 'Course';
        details.ProductReference = 'Test definition';
        details.Fees = '100.0';
        details.CourseOfferingId = TestDataFactoryUtil.courseOffering.Id;
        details.SessionStartDate = string.valueOf(TestDataFactoryUtil.courseOffering.hed__Start_Date__c);
        details.SessionEndDate = string.valueOf(TestDataFactoryUtil.courseOffering.hed__End_Date__c);
        
        csapi_BasketRequestWrapper.AddProductRequest request = new csapi_BasketRequestWrapper.AddProductRequest();
        request.Course = details;
        request.IsBatchProcessed = false;
        
        csapi_BasketRequestWrapper.AddProductResponse response = csapi_RestBasketExtension_AddProduct.doPost(request);
        system.debug('response >> ' + response);
        
        system.assertEquals(response.Status, 'Pass');
        
        //get basket info
        csapi_BasketRequestWrapper.BasketInfoRequest request2 = new csapi_BasketRequestWrapper.BasketInfoRequest();
        csapi_BasketRequestWrapper.BasketInfoResponse response2 = ProductBasketController.getresult(); 
        
        //assert its created
        system.assertEquals(response2.Status, 'Pass');
        
        Boolean result = ProductBasketController.getCitizenshipStatus();
        System.assertEquals(result, false);
        Test.stopTest();
        
    }
    
    @isTest
    public static void userLoginCheckTest(){
        
        Test.startTest();
        String userType = UserInfo.getUserType();
        String uType = ProductBasketController.userLoginCheck();
        system.assertEquals(uType, userType);
        Test.stopTest();
    }
    
    @isTest
    public static void getcartCreationForPendingTransactionTest(){
        
         Account accProgram = TestUtility.createTestAccount(true
            , 'Program Account 001'
            , Schema.SObjectType.Account.getRecordTypeInfosByName().get('Academic Program').getRecordTypeId());
            
        TestDataFactoryUtil.testCreateData();
        
        csapi_BasketRequestWrapper.csapi_CourseDetails details = new csapi_BasketRequestWrapper.csapi_CourseDetails();
        details.CourseName = TestDataFactoryUtil.course.Name;
        details.CourseType = 'Course';
        details.ProductReference = 'Test definition';
        details.CourseOfferingId = TestDataFactoryUtil.courseOffering.Id;
        details.SessionStartDate = string.valueOf(TestDataFactoryUtil.courseOffering.hed__Start_Date__c);
        details.SessionEndDate = string.valueOf(TestDataFactoryUtil.courseOffering.hed__End_Date__c);
        
        Profile p = [SELECT Id FROM Profile WHERE Name='High Volume Customer Portal User']; 
        Contact portalCon = new Contact(AccountID = accProgram.id
                , FirstName = 'User', LastName = 'test',email = 'test55user@xyzddf.com');
        insert portalCon;       
        
        User u = new User(Alias = 'standt', Email='test55user@xyzddf.com', 
            EmailEncodingKey='UTF-8', LastName='Testing', LanguageLocaleKey='en_US', 
            LocaleSidKey='en_US', ProfileId = p.Id, 
            TimeZoneSidKey='America/Los_Angeles', UserName='test55user@xyzddf.com', ContactId= portalCon.Id);
            
        Pending_Purchase__c pendingPurchase = new Pending_Purchase__c(Contact__c = portalCon.Id
               ,Status__c  = 'Pending'
               ,Course_Name__c  = TestDataFactoryUtil.course.Name
               ,Course_Offering_Id__c  = details.CourseOfferingId
               ,Delivery_Mode__c = details.deliveryMode
               ,Product_Line__c = details.productLine
               ,Product_Reference__c = details.ProductReference
               ,Session_End_Date__c = details.SessionEndDate
               ,Session_Start_Date__c = details.SessionStartDate
               ,Course_Type__c = details.CourseType);
        insert pendingPurchase;
        
        Test.startTest();
         System.runAs(u) {
            System.Assert(u.ContactId != null , 'Invalid Data');
            ProductBasketController.cartCreationForPendingTransaction();
        }  
        Test.stopTest();
        
        
    }
    
    
}
