/**
 * @description This is a mock class for UserValidator
 * This class is only used for unit testing
 */
public class MockUserValidator implements UserValidator {
    
    /**
     * @description Validates the email address
     * @param email The user's email address
     * @return Returns if the email is valid
     */
    public boolean isEmailValid(String email) {
        if (String.isNotBlank(email)) {
            return true;
        } else {
            return false;
        }
    }
    
    
    /**
     * @description Validates if the first name
     * @param firstName The user's first name
     * @return Returns if the name is valid
     */
    public boolean isFirstNameValid(String firstName) {
        return isNameValid(firstName);
	}
	
	
    /**
     * @description Validates if the last name
     * @param lastName The user's last name
     * @return Returns if the name is valid
     */
    public boolean isLastNameValid(String lastName) {
        return isNameValid(lastName);
    }
	
    
    /**
     * @description Performs basic phone number validation
     * @param phoneNumber The user's phone number
     * @return Returns if the phone number is in a valid format
     */
    public boolean isPhoneNumberValid(String phoneNumber) {
        if (String.isNotBlank(phoneNumber) ) {
            return true;
        } else {
            return false;
        }
    }

    
    private boolean isNameValid(String name) {
        if (String.isNotBlank(name)) {
            return true;
        } else {
            return false;
        }
    }
    
}