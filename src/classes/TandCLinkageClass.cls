public without sharing class TandCLinkageClass {
    
    public static void updateTandC()
    {
        
        List<hed__Course__c> courseList = new List<hed__Course__c>();
        
        courseList = [SELECT Id,
                      		 Name
                      		 FROM hed__Course__c 
                      		 WHERE RecordTypeId = : ConfigDataMapController.getCustomSettingValue('21CCRecordId')];
        
        Set<Id> courseId = new Set<Id>();
        for(hed__Course__c course :courseList)
        {
            courseId.add(course.Id);        
        }
        
        List<ProductTC__c> pdlist = [SELECT Id, Name From ProductTC__c Where CourseId__c IN: courseId];
            
        System.debug('Course List--->'+courseList);
        System.debug('PD List--->'+pdlist);
        
        List<Terms_and_Conditions__c>	termsCondObjList = [SELECT Id,Long_Description__c FROM Terms_and_Conditions__c WHERE Product_Type__c = : ConfigDataMapController.getCustomSettingValue('ProductType')];
        System.debug('Terms and Conditions--->'+termsCondObjList);

        List<ProductTC__c> prodTermsCondList = new  List<ProductTC__c>();
        
        for(hed__Course__c course : courseList)
        {
            ProductTC__c pd = new ProductTC__c();
            pd.Terms_and_ConditionId__c = termsCondObjList[0].Id;
            pd.CourseId__c = course.Id;
            prodTermsCondList.add(pd);
            //Database.insert(pd);
            //System.debug('product--->'+pd);
        }

        if( !prodTermsCondList.isEmpty())
        {
            insert prodTermsCondList;
        }
       
    }
    
}