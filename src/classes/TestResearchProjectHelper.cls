/*******************************************
Purpose: Test class ResearchProjectHelper
History:
Created by Ankit Bhagat on 15/10/2018
*******************************************/
@isTest
public class TestResearchProjectHelper {


/************************************************************************************
// Purpose      :  Creating Test data for Community User for all test methods 
// Developer    :  Ankit
// Created Date :  10/24/2018                 
//***********************************************************************************/
@testSetup static void testDataSetup() {

    RSTP_TestDataFactoryUtils.createNonCommunityUsersWithProfile(1,'System Administrator','Activator');
    
    User adminUser =[SELECT Id,ContactId FROM User WHERE UserName='test.nonCommunityUser1@test.com.testDev' LIMIT 1];
    System.runAs(adminUser)   
    {
        Map<String,Integer> allnumbersOfDataMap = new Map<String,Integer>();
        allnumbersOfDataMap.put('user',1);
        allnumbersOfDataMap.put('account',1);
        allnumbersOfDataMap.put('contact',1);
        
        Map<String,String> requiredInfoMap = new Map<String,String>();
        requiredInfoMap.put('userType','CommunityUsers');
        requiredInfoMap.put('profile',Label.testProfileName);
        
        Boolean userCreatedFlag = RSTP_TestDataFactoryUtils.testRSTPDataSetup(allnumbersOfDataMap,requiredInfoMap);
        System.assert(userCreatedFlag==true,'Community User Created'); 
        // Custom setting data preparation//
        CustomPermissionToProfile__c cPTP = new CustomPermissionToProfile__c(
            isAccessible__c=true,
            SetupOwnerId= [SELECT id FROM Profile WHERE NAME =:Label.testProfileName].Id    
        );
        insert cPTP;
        System.assert(cPTP!= null,'Custom setting records created');
	}	    

}
/************************************************************************************
// Purpose      :  Test functionality of positive method for Create ResearcherPortal Project functionality
// Developer    :  Ankit
// Created Date :  10/24/2018                 
//***********************************************************************************/

@isTest
public static void researchProjectHelperPositiveMethod(){
   
    Contact con =[SELECT Id,Firstname FROM contact WHERE LastName='Contact1' LIMIT 1 ];
    con.hed__UniversityEmail__c='raj@rmit.edu.au';
    update con;
            
    User usr =[SELECT Id,ContactId FROM User WHERE UserName='test.communityUser1@test.com.testDev' LIMIT 1];           
    system.runAs(usr)
    {
        ResearcherPortalProject__c researchProject = new ResearcherPortalProject__c();
        researchProject.Status__c='Idea';
        researchProject.Access__c='Private';
        researchProject.Approach_to_impact__c='test1';
        researchProject.Discipline_area__c='';
        researchProject.Start_Date__c= date.newInstance(2018, 10, 08) ;
        researchProject.End_Date__c=date.newInstance(2018, 12, 14);
        researchProject.Impact_Category__c='Social';
        researchProject.Title__c='projectTest1';
        researchProject.Keywords__c='Others';
        researchProject.RecordTypeId =Schema.SObjectType.ResearcherPortalProject__c.getRecordTypeInfosByName().get(Label.ResearchMasterProject_RecorType).getRecordTypeId();
        //researchProject.RecordTypeId = Schema.SObjectType.ResearcherPortalProject__c.getRecordTypeInfosByName().get(Label.RSTP_ResearchProjectRMRecordtype).getRecordTypeId(); 
        insert researchProject;
        system.assert(researchProject!=null); 
         List<id> researchId = new List<id>();
        researchId.add(researchProject.id);
        
        String projectRecordTypeId=Schema.SObjectType.ResearcherPortalProject__c.getRecordTypeInfosByName().get('Researcher Portal Project').getRecordTypeId();
        String access = 'Private';
        List<ResearcherPortalProject__c> projectObjList = RSTP_TestDataFactoryUtils.createResearchProj(1, projectRecordTypeId, access);

        Researcher_Member_Sharing__c memberSharing = new Researcher_Member_Sharing__c();
        memberSharing.MemberRole__c = 'CI';
        memberSharing.Contact__c = con.Id;
        memberSharing.User__c=usr.id;
        memberSharing.Researcher_Portal_Project__c = researchProject.Id;
        memberSharing.Primary_Contact_Flag__c = true  ;
        memberSharing.AccessLevel__c='Edit';
        memberSharing.Position__c='CI';
        memberSharing.Currently_Linked_Flag__c=true;
        memberSharing.RecordTypeId = Schema.SObjectType.Researcher_Member_Sharing__c.getRecordTypeInfosByName().get(Label.RSTP_ResearchProjectRecordtype).getRecordTypeId();
        insert memberSharing;
        system.assert(memberSharing!=null);
        
        
        
        projectRecordTypeId=Schema.SObjectType.ResearcherPortalProject__c.getRecordTypeInfosByName().get(Label.RSTP_ResearchProjectRMRecordtype).getRecordTypeId();
        access = 'RMIT Public';
        projectObjList = RSTP_TestDataFactoryUtils.createResearchProj(1, projectRecordTypeId, access);
        projectObjList[0].Portal_Project_Status__c='Conducting';
        update projectObjList;
        system.assert(projectObjList!=null);
        
        Researcher_Member_Sharing__c rmMemberSharing = new Researcher_Member_Sharing__c();
        rmMemberSharing.MemberRole__c = 'CI';
        rmMemberSharing.Contact__c = con.Id;
        rmMemberSharing.User__c=usr.id;
        rmMemberSharing.Researcher_Portal_Project__c = projectObjList[0].Id;
        rmMemberSharing.Primary_Contact_Flag__c = true  ;
        rmMemberSharing.RecordTypeId = Schema.SObjectType.Researcher_Member_Sharing__c.getRecordTypeInfosByName().get(Label.RSTP_ResearchProjectRecordtype).getRecordTypeId();
        insert rmMemberSharing;
        system.assert(rmMemberSharing!=null);
        
        ProjectDetailsWrapper.ResearchProjectDetailData wrapperListView = new ProjectDetailsWrapper.ResearchProjectDetailData();
        wrapperListView.groupStatus='Active';
        wrapperListView.projectStartDate=Date.newInstance(2011, 3, 22);
        wrapperListView.projectEndDate=Date.newInstance(2019, 05, 10);
   		String str = JSON.serialize(wrapperListView);  
        
        Research_Project_Classification__c  projectClassification = new Research_Project_Classification__c();
        projectClassification.ResearcherPortalProject__c = researchProject.id;
        projectClassification.For_Four_Digit_Detail__c      =  '0101 - Pure Mathematics'; 
        projectClassification.For_Four_Digit_Description__c = 'Test';
        insert projectClassification;
        system.assert(projectClassification!=null);
        
                
        List<ProjectDetailsWrapper.ContactDetail> conDetailList = new List<ProjectDetailsWrapper.ContactDetail>();
        
        ProjectDetailsWrapper.ContactDetail conDetail = new ProjectDetailsWrapper.ContactDetail(con,'test');
        ProjectDetailsWrapper.ContactDetail conExtendDetail = new ProjectDetailsWrapper.ContactDetail(con.id);
        ProjectDetailsWrapper.ContactDetail conExtend= new ProjectDetailsWrapper.ContactDetail(memberSharing);
        conDetailList.add(conDetail);
        conDetailList.add(conExtendDetail);
        conDetailList.add(conExtend);
        system.assert(conDetailList!=null);
        
        
        Test.StartTest();
        ResearchProjectHelper.getMyResearchProjectListViewData();
        ResearchProjectHelper.userCheckAccessToResearchProjData(usr.id);
        ResearchProjectHelper.getProjectIdsByUserRoleAccess(usr.id, 'ALL');
        ResearchProjectHelper.getProjectIdsByUserRoleAccess(usr.id, 'CI');
        ResearchProjectHelper.getProjectIdsByUserRoleAccess(usr.id, 'Admin'); 
        ResearchProjectHelper.getProjectIdsByUserRoleAccess(NULL, 'Admin'); 
        ResearchProjectHelper.getMyResearchProjectListViewByRole(usr.id, 'CI');         
        ResearchProjectHelper.getMyResearchProjectLists();
        ResearchProjectHelper.getProjectDetailsHelper(researchProject.id);
        ResearchProjectHelper.getPrimaryContactNameofProject(researchId);
        ResearchProjectHelper.searchContactResults('raj');
        ResearchProjectHelper.checkProjectMember(researchProject.id); 
        ResearchProjectHelper.updateStatusValueHelper(wrapperListView, researchProject.id);
       
        Test.StopTest();
        
        
    }
}
 /************************************************************************************
    // Purpose      :  Test functionality of Negative method  for Create ResearcherPortal Project functionality
    // Developer    :  Ankit
    // Created Date :  12/05/2018                 
    //***********************************************************************************/

@isTest
public static void researchProjectHelperNegativeMethod(){
  
    User usr =[SELECT Id,ContactId FROM User WHERE UserName='test.communityUser1@test.com.testDev' LIMIT 1];           
    system.runAs(usr)
    {
    
        Boolean result = false;
        Contact con =[SELECT Id,Firstname,lastname FROM contact WHERE LastName='Contact1' LIMIT 1 ];
        con.hed__UniversityEmail__c='raj@rmit.edu.au';
        con.lastname='';
        //update con; 
         try
            {
                update con;    
            }
            catch(DmlException ex)
            {
                result =true;                   
               
            }
            system.assert(result);
        ResearcherPortalProject__c researchProject = new ResearcherPortalProject__c();
        researchProject.Status__c='Idea';
        researchProject.Access__c='Private';
        insert researchProject;
        system.assert(researchProject!=null); 
        List<id> researchId = new List<id>();
        researchId.add(researchProject.id);
    
        Researcher_Member_Sharing__c memberResearch = new Researcher_Member_Sharing__c();
       
        try
        {
            memberResearch .Contact__c = con.Id;
            memberresearch.User__c = usr.Id;
            insert memberresearch;   
        }
        catch(Exception ex)
        {
            result =true;                   
           
        }  
        system.assert(result);
        Research_Project_Classification__c  projectClassification = new Research_Project_Classification__c();
        projectClassification.ResearcherPortalProject__c = researchProject.id;
        insert projectClassification;
        system.assert(projectClassification!=null);
        
        
        List<ProjectDetailsWrapper.ContactDetail> conDetailList = new List<ProjectDetailsWrapper.ContactDetail>();
        
        ProjectDetailsWrapper.ContactDetail conDetail = new ProjectDetailsWrapper.ContactDetail(con,'test');
        conDetailList.add(conDetail);
        system.assert(conDetailList!=null);
        
        
        Test.StartTest();
        ResearchProjectHelper.getMyResearchProjectListViewData();
        //ResearchProjectHelper.getMyResearchProjectPublicListViewData(null);
        ResearchProjectHelper.userCheckAccessToResearchProjData(usr.id);
        ResearchProjectHelper.getProjectIdsByUserRoleAccess(usr.id, 'ALL');
        ResearchProjectHelper.getProjectIdsByUserRoleAccess(usr.id, 'CI');
        ResearchProjectHelper.getProjectIdsByUserRoleAccess(usr.id, 'Admin'); 
        ResearchProjectHelper.getProjectIdsByUserRoleAccess(NULL, 'Admin'); 
        ResearchProjectHelper.getMyResearchProjectListViewByRole(usr.id, 'CI');        
        ResearchProjectHelper.getMyResearchProjectLists();
        ResearchProjectHelper.getProjectDetailsHelper(researchProject.id);
        ResearchProjectHelper.getPrimaryContactNameofProject(researchId);
        ResearchProjectHelper.searchContactResults('raj');
        Test.StopTest();
        
       
    }
  }
}