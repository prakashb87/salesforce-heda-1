/********************************************************************
// Purpose              : Register a new user
// Author               : Capgemini 
//JIRA Reference        : ECB-4578 : HTML Log In/Registration Form
//********************************************************************/

/**********************************************************
 * IMPORTANT: THIS CONTROLLER CAPTURES CUSTOMER PASSWORDS
 * THE PASSWORD SHOULD NOT BE LOGGED IN ANY CIRCUMSTANCES
 * DO NOT LOG PASSWORD USING System.Debug OR TO ANY CUSTOM
 * OBJECT
 *********************************************************/
@SuppressWarnings('PMD.AvoidGlobalModifier')
global without sharing class ECBRegisterPageController {
    // Global state is required as the page will be inside an iframe
    
    public Boolean captchaStatus{get;set;}
    public String captchaSiteKey{get;set;}
    public String resetpwdurl{set; get;} //ECB - 5832
    public static List<Contact> conList;
    // public String emailConfirmPageLink{get;set;}
    
    public static Contact con;
    public static Contact conToUpdate;
    // public Contact countries{get;set;}
    
    
    public ECBRegisterPageController()
    {
        captchaStatus = Boolean.valueOf(ConfigDataMapController.getCustomSettingValue('Captcha'));
        captchaSiteKey = ConfigDataMapController.getCustomSettingValue('CaptchaSiteKey'); //ECB-4782 : Referring captcha site key from custom setting
        //emailConfirmPageLink = ConfigDataMapController.getCustomSettingValue('EmailConfirmationLink');
        resetpwdurl = ConfigDataMapController.getCustomSettingValue('PasswordReset');  //ECB - 5832
    }
    
    
    @remoteAction
    global static RegistrationResponse register(String response,String robj, RegistrationAbstractFactory registrationAbstractFactory ) 
    {
        // Global state is required as the page will be inside an iframe
        
        Boolean googleResponse = false;
        String federationID = '';
        RegistrationResponse registrationResult = new RegistrationResponse();
        registrationResult.success = false;
        registrationResult.errorMessage = '';
        
        try {
            Boolean isCaptchaOn = Boolean.valueOf(ConfigDataMapController.getCustomSettingValue('Captcha'));
            
            RegisterFormDetails registerWrap = (RegisterFormDetails) System.JSON.deserialize(robj, RegisterFormDetails.class);
            system.debug('@@@registerWrap : '+registerWrap);
            
            //Ensure the date is in yyyy-mm-dd format irrespective of whats displayed in UI
            if(registerWrap.bday != null)
            {
                List<String> dateParts = registerWrap.bday.split('-'); 
                registerWrap.bday = dateParts[2] + '-' + dateParts[1] + '-' + dateParts[0];
            } 
            
            CaptchaVerificationRequest google = registrationAbstractFactory.createCaptcha();
            system.debug('Before Exception');
            if (isCaptchaOn) {
                googleResponse = google.isCaptchaVerified(response);
            }
            
            System.debug('>>> googleResponse::::'+ googleResponse);
            if (isCaptchaOn && !googleResponse) {
                throw new ECBRegistrationException('Captcha failed');
            }
            
            //if ((!isCaptchaOn && !googleResponse) ||(isCaptchaOn && googleResponse)) {                         
            List<Contact> conListUpdate;   
            boolean isContactFound = verifyContact(registerWrap.email);
            System.debug('>>>verifycon?>>> '+isContactFound);
            if(isContactFound){
                /*ECB-5832: Error message Update */
                registrationResult.errorMessage = 'Error_userExists';
                throw new ECBRegistrationException('The user is already registered.');
            }
            //if(!verifycon) {// if there no contact with email match
            //System.debug('>>>inside if verifycon?>>> '+verifycon);
            
            IdentityProviderUserStatusReq emailCheckApibj = registrationAbstractFactory.createIdpUserStatusReq(registerWrap.email);
            emailCheckApibj.setIntegrationLogging(false);// cannot log due to multiple callouts - Salesforce limitation
            
            if( ! emailCheckApibj.isExistingUser()) {
                // User not existing in IDAM. We are creating an unverified user in IDAM
                federationID = invokePassportUserCreation(registerWrap, registrationAbstractFactory);
                
            } else if( emailCheckApibj.isExistingVerifiedUser()) {
                // Verified user existing in IDAM. Show error message in UI on email field.
                registrationResult.success = false;
                registrationResult.errorMessage = 'This RMIT account already exists. To continue, please login or use a different email to register.';
            } else if( !emailCheckApibj.isExistingVerifiedUser()) {
                // Unverified user existing in IDAM - handle delete & create scenarios here.
                
                IdentityProviderUserDeleteRequest idpDelete = registrationAbstractFactory.createIdpUserDeleteReq();
                idpDelete.deleteUser(registerWrap.email);
                federationID = invokePassportUserCreation(registerWrap, registrationAbstractFactory);
                
            }
            
            if (!string.IsBlank(federationID)) {
                Contact conObj = new Contact();
                
                Boolean contactCreateFlag =verifyAllOtherEmailFieldsForCreatingContact(registerWrap,federationID);
                if (contactCreateFlag) {//create contact 
                    conObj = createContactAndAddress( registerWrap, federationID );                                                       
                } else {// update contact 
                    conObj = updateContactAndAddress(registerWrap,federationID,conToUpdate);
                }
                //if (conObj != null) {
                UserRegistrationEmail userReg = new UserRegistrationEmail(conObj.id);
                userReg.userRegistrationEmail();
                registrationResult = new RegistrationResponse();
                registrationResult.contactId = conObj.id;
                registrationResult.email = conObj.Email;
                registrationResult.success = true;
               // registrationResult.resetpwdurl = Config_Data_Map__c.getInstance('PasswordReset').Config_Value__c; //ECB - 5832 
                
                //New Code here
                if(String.isNotBlank(registerWrap.url)){
                    try {
                        PendingPurchaseUtils.recordPendingTransaction(conObj.id, registerWrap.url); // ECB-4614  
                    } catch(Exception e) {
                        System.debug('Pending purchase failed.');   // just ignore the error, registration shouldn't fail because of this
                    }
                }                                                       
                //}
            } else {
                //Show error message near to email field to say contact with email is existing.
                registrationResult.success = false;
                registrationResult.errorMessage = 'This RMIT account already exists. To continue, please login or use a different email to register.';
                system.debug('This account already exists. Please log in or enter a different email address.');
            }            
            //} 
            //registrationResult.errorMessage ='Failure Message';
            // }
        } catch(Exception ex) {
            system.debug('Exception:::'+ex);
            registrationResult.success = false;
            if(registrationResult.errorMessage.length()>0){
                registrationResult.errorMessage = registrationResult.errorMessage;
            }else{ /*ECB-5832: Error message Update */
                registrationResult.errorMessage = 'Error_userExists';
            }
            
            logException(ex);// log error log
            return registrationResult;
        }
        system.debug('registrationResult::::'+registrationResult);
        return registrationResult;
        
    }
    
    @testVisible
    private class RegisterFormDetails
    {
        public String givenName;
        public String familyName;
        public String email;
        public String confirmEmail;
        public String password;
        public String mobilenumber;
        public String bday;
        public String country;
        public String url; // ECB - 4614
    }
    
    
    global class RegistrationResponse{
        public String contactId;
        public String email;
        public boolean success;
        public String errorMessage;
      //  public String resetpwdurl; //ECB-5832
    }
    
    @testVisible
    private static string invokePassportUserCreation( RegisterFormDetails registerWrap, RegistrationAbstractFactory registrationAbstractFactory)
    {
        String federationID = '';
        IdentityProviderUserRequest passObj = registrationAbstractFactory.createIdpUserCreateReq(); 
        passObj.setFirstName(registerWrap.givenName);
        passObj.setLastName(registerWrap.familyName);
        passObj.setEmail(registerWrap.email);
        
        // Mobile is optional
        if(String.isNotBlank(registerWrap.mobilenumber) && !((registerWrap.mobilenumber).equalsIgnoreCase('countrycode')) ) { 
            passObj.setMobile(registerWrap.mobilenumber);
        }
        passObj.setPassword(registerWrap.password);
        passObj.createUser();
        federationID = passObj.getFederationId();
        return federationID;
    }
    
    
    @testVisible
    private static boolean verifyContact(String email)
    {
        Boolean conExists = false;
        conList = [SELECT Id,AccountId,hed__Country_of_Origin__c, Email,FirstName, LastName, MobilePhone, Birthdate FROM Contact WHERE (Email =: email)];
        
        if(!conList.IsEmpty() && conList.size()> 0)
        {
            conExists = true;
        }
        return conExists;
    }
    
    
    @testVisible
    private static boolean verifyAllOtherEmailFieldsForCreatingContact( RegisterFormDetails registerWrap, String fedID)
    {
        List <Contact> conListToupdate = new List<Contact>();
        conToUpdate = new Contact();
        Boolean createFlag = true;
        
        List<Contact> checkContactList = new List<Contact> ();
        if(Schema.sObjectType.Contact.isAccessible()){
        checkContactList = [SELECT id,email,Latest_Email__c,Home_Email__c,hed__WorkEmail__c,hed__UniversityEmail__c,hed__AlternateEmail__c from Contact where (Latest_Email__c =: registerWrap.email) OR (Home_Email__c =: registerWrap.email) OR (hed__WorkEmail__c =: registerWrap.email) OR (hed__UniversityEmail__c =: registerWrap.email) OR (hed__AlternateEmail__c =: registerWrap.email)];
        }
        for( Contact cObj : checkContactList)
        {
            if( !String.IsBlank(cObj.Email))
            {
                createFlag = true;// create contact
            }
            else
            {
                if( cObj.FirstName == registerWrap.givenName && cObj.LastName == registerWrap.familyName && cObj.Birthdate == Date.Valueof(registerWrap.bday) && (string.IsBlank(cObj.IDAM_ECB_Federated_Id__c)  || cObj.IDAM_ECB_Federated_Id__c == fedID) )
                {
                    conListToupdate.add(cObj);
                    createFlag = false; //update contact                   
                }
            }
        }
        if( !conListToupdate.IsEmpty())
        {
            conToUpdate = conListToupdate[0];
        }    
        return createFlag;
    }
    
    @testVisible
    private static Contact createContactAndAddress( RegisterFormDetails registerWrap, String federationID )
    {
        con = new Contact();
        con.FirstName = registerWrap.givenName;
        con.LastName = registerWrap.familyName;
        con.Email = registerWrap.email;
        con.MobilePhone = registerWrap.mobilenumber;
        con.Birthdate = Date.valueOf(registerWrap.bday);
        con.IDAM_ECB_Federated_Id__c = federationID;
        con.Student_ID__c = federationID;
        con.HasOptedOutOfEmail = false; // Added For ECB-5616
        System.debug('>>> con>> '+con);
        
        Database.insert(con);
        System.debug('>>> contact id>> '+con.id);
        
        Id contactAdminAccountId = [SELECT AccountId FROM Contact WHERE Id = :con.Id].AccountId;
        
        Account adminAccount = [SELECT Id, OwnerId FROM Account WHERE Id = :contactAdminAccountId LIMIT 1];
           
        if( adminAccount != null )
        {
            adminAccount.OwnerId = ConfigDataMapController.getCustomSettingValue('ECBNewCustomerAccountOwner');
            Database.update(adminAccount); 
        }
        
        hed__Address__c add = new hed__Address__c();                    
        add.hed__Address_Type__c = 'Home';
        add.hed__MailingCountry__c = registerWrap.country;
        add.First_Name__c = registerWrap.givenName;
        add.Last_Name__c = registerWrap.familyName;
        add.Effective_Date__c = System.today();
        add.hed__Parent_Contact__c = con.Id;
        Database.insert(add);
        
        return con;
    }
    @testVisible
    private static Contact updateContactAndAddress(RegisterFormDetails registerWrap, String federationID,Contact con)
    {	
        Contact conSelect = new Contact();
        if(Schema.sObjectType.Contact.isAccessible()){ 
            conSelect = [SELECT id, MobilePhone from Contact where id =:con.id LIMIT 1];
        }
        conSelect.MobilePhone = registerWrap.mobilenumber;
        conSelect.IDAM_ECB_Federated_Id__c = federationID;
        Database.update(conSelect);
        
        List<hed__Address__c> addObjList = new List<hed__Address__c>();
        if(Schema.sObjectType.hed__Address__c.isAccessible()){ 
            addObjList = [ Select id,hed__Parent_Contact__c,hed__MailingCountry__c from hed__Address__c where hed__Parent_Contact__c=:conSelect.id LIMIT 1];
        }
        
        if(!addObjList.isEmpty())
        {
            hed__Address__c addObj = addObjList[0];
            addObj.hed__MailingCountry__c = registerWrap.country;
            Database.update(addObj);
        }
        return conSelect;
    } 
    
    public List<SelectOption> getCountries()
    {
        List<SelectOption> options = new List<SelectOption>();        
        Schema.DescribeFieldResult fieldResult = Contact.hed__Country_of_Origin__c.getDescribe();
        List<Schema.PicklistEntry> ple = fieldResult.getPicklistValues();
        
        for( Schema.PicklistEntry f : ple)
        {
            options.add(new SelectOption(f.getLabel(), f.getValue()));
        }       
        return options;
    }
    
   /* //ECB-4581 : Instructions After Registration Details have been Submitted - Rishabh
    @AuraEnabled
    public static boolean resendConfirmationEmail(String contactId) 
    {
        User_Registration__c userRegistrationObject = new User_Registration__c();
        
        if(Schema.sObjectType.User_Registration__c.isAccessible()){  
            userRegistrationObject  = [SELECT Contact__c,Expiry__c,Id FROM User_Registration__c WHERE Contact__r.Id =: contactId];
        }
        
        Boolean result = false;
        
        if(String.isNotBlank(contactId) && String.isBlank(userRegistrationObject.Id) )
        {
            UserRegistrationEmail userReg = new UserRegistrationEmail(contactId); 
            result=userReg.userRegistrationEmail();
        }
        else{
            UserRegistrationEmail userReg = new UserRegistrationEmail(contactId); 
            result=userReg.sendActivationEmail(userRegistrationObject.Id);
        }
        return result;
    } */
    
    @testVisible
    private static void logException(Exception ex)
    {
        RMErrLog__c errorObj = New RMErrLog__c();
        errorObj.Business_Function_Name__c = 'User Registration';
        errorObj.Class_Name__c = 'ECBRegisterPageController';
        errorObj.Error_Cause__c = 'Exception During Registration';      
        errorObj.Error_Message__c = ex.getmessage();       
        errorobj.Method_Name__c = 'register()';
        Database.insert(errorobj);
    }
}