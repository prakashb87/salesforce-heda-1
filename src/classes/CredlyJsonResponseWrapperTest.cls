@IsTest
public class CredlyJsonResponseWrapperTest {
    
    static testMethod void testParse() {
        
        Account accAdministrative = TestUtility.createTestAccount(true
                                                                  , 'Administrative Account 001'
                                                                  , Schema.SObjectType.Account.getRecordTypeInfosByName().get('Administrative').getRecordTypeId());
    
    Contact con = new Contact(FirstName = 'Test', LastName = 'Contact');
    insert con;
    
    hed__Course__c crs10 = new hed__Course__c(Name = 'Test Course10', hed__Account__c = accAdministrative.Id, status__c = 'Active', Credly_Badge_Id__c = '123');
    insert crs10;
    
    hed__term__c trm1 = new hed__term__c(Name = 'Test Term', hed__Account__c = accAdministrative.Id);
    insert trm1;
        
    hed__Course_Offering__c cofr = new hed__Course_Offering__c(hed__Course__c = crs10.id, Name = 'Test Course Offering', hed__term__c = trm1.id);
    insert cofr;
    
    hed__Course_Enrollment__c cern = new hed__Course_Enrollment__c(hed__Contact__c = con.id, hed__Course_Offering__c = cofr.id, hed__Status__c = 'Current', Enrolment_Status__c = 'E'  );
    insert cern;
    
        
		String json = '{  '+
		'   \"email\":\"34\",'+
		'   \"result\":\"OK\",'+
		'   \"message\":\"Success\",'+
		'   \"badgeResponse\":[  '+
		'      {  '+
		'         \"courseConnectionId\":\"'+cern.Id+'\",'+
		'         \"badgeId\":\"'+crs10.Credly_Badge_Id__c+'\",'+
		'         \"issueDate\":\"2018-01-01\",'+
		'         \"message\":\"Success\"'+
		'      },'+
		'      {  '+
		'         \"courseConnectionId\":\"'+cern.Id+'\",'+
		'         \"badgeId\":\"'+crs10.Credly_Badge_Id__c+'\",'+
		'         \"issueDate\":\"2018-02-02\",'+
		'         \"message\":\"Success\"'+
		'      }'+
		'   ]'+
		'}';
		CredlyJsonResponseWrapper obj = CredlyJsonResponseWrapper.parse(json);
		System.assert(obj != null);
	}
}