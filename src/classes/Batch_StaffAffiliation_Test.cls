@isTest
private class Batch_StaffAffiliation_Test {
    public Static String rmitStaffRtId = Schema.SObjectType.hed__Affiliation__c.getRecordTypeInfosByName().get('Business').getRecordTypeId();
    static testMethod void testMethod_Aff(){
        List<hed__Affiliation__c> affList= new List<hed__Affiliation__c>();
        
        Account newAccount = new Account();
        newAccount.name = 'Test Batch Account';
        insert newAccount;
        Account newAccount1 = new Account();
        newAccount1.name = '1 Test Batch Account';
        insert newAccount1;
        Contact newContact = new Contact();
        newContact.FirstName = 'Test Contact';
        newContact.LastName = 'Contact Test';
        newContact.Enumber__c = '1341532';
        newContact.hed__Primary_Organization__c = newAccount.Id;
        insert newContact;
        for(Integer i = 0; i<= 10; i++){
            hed__Affiliation__c affrt = new hed__Affiliation__c();
            affrt.hed__Contact__c = newContact.Id;
            affrt.hed__Account__c = newAccount.Id;
            affrt.RecordTypeId = rmitStaffRtId;
            affList.add(affrt);
        }
        Database.insert(affList);
        Test.startTest();
            Batch_StaffAffiliation obj = new Batch_StaffAffiliation();
            DataBase.executeBatch(obj);
        Test.stopTest();
        
           
    }
}