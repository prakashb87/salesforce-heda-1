/*******************************************
Purpose: Test class ResearcherPortalCaseHelper
History:
Created by Md Ali on 12/11/2018
*******************************************/
@istest
public class TestResearcherPortalCaseHelper {
    
    /************************************************************************************
// Purpose      :  Creating Test data for Community User for all test methods 
// Developer    :  Md Ali
// Created Date :  12/01/2018                 
//***********************************************************************************/
    @testSetup static void testDataSetup() {
        
        RSTP_TestDataFactoryUtils.createNonCommunityUsersWithProfile(1,'System Administrator','Activator');
        
        User adminUser =[SELECT Id,ContactId FROM User WHERE UserName='test.nonCommunityUser1@test.com.testDev' LIMIT 1];
        System.runAs(adminUser)   
        {
            Map<String,Integer> allnumbersOfDataMap = new Map<String,Integer>();
            allnumbersOfDataMap.put('user',1);
            allnumbersOfDataMap.put('account',1);
            allnumbersOfDataMap.put('contact',1);
            
            Map<String,String> requiredInfoMap = new Map<String,String>();
            requiredInfoMap.put('userType','CommunityUsers');
            requiredInfoMap.put('profile',Label.testProfileName);
            
            Boolean userCreatedFlag = RSTP_TestDataFactoryUtils.testRSTPDataSetup(allnumbersOfDataMap,requiredInfoMap);
            System.assert(userCreatedFlag==true,'Community User Created'); 
        }
       
    }    
    /************************************************************************************
// Purpose      :  Test functionality for ResearcherPortalSupportController
// Developer    :  Md Ali
// Created Date :  11/12/2018                 
//***********************************************************************************/
    

    @isTest
    public static void testPositiveUseCaseForResearcherPortalCaseHelperMethods(){
        User u =[SELECT Id,ContactId FROM User WHERE UserName='test.communityUser1@test.com.testDev' LIMIT 1]; 
        
        ResearcherPortalCaseWrapper portalWrapper = new ResearcherPortalCaseWrapper();
        portalWrapper.caseId='casetest';
        portalWrapper.caseNumber='Testnumber';
        portalWrapper.description='caseCreation';
        portalWrapper.portalFeedback='good';
        portalWrapper.recordType='case';
        portalWrapper.researcherCaseRoutedToEmail='abc';
        portalWrapper.researcherCaseSubTopic='Applying for Animal Ethics approval';
        portalWrapper.researcherCaseTopic='Animal Ethics and Welfare';
        portalWrapper.status='cool';
        portalWrapper.subject='case';
        //insert portalWrapper;
        system.assert(portalWrapper!=null);
        
        //QueueSobject queObj = new QueueSobject();
         String serailizedString = '{"researcherCaseTopic":"Enabling Capability Platform (ECPs)","researcherCaseSubTopic":"Funding options, application, deadlines and management","Subject":"test subject","Description":"test description","recordType":"Researcher Portal Support Case","status":"Closed"}';
        //JSON.serialize(portalWrapper); 
        String portalSerailizedString='{"portalFeedback":"Articles & FAQs","description":"dasdas","satisfactionRating":"4","recordType":"Researcher Portal Feedback","caseAttachments":[{"fileName":"download.png","fileContent":"iVBORw0KGgoAAAANSUhEUgAAANcAAADrCAMAAADNG/","contentType":"png"}]}';     
        
        ResearcherPortalCaseController.getAllTopicSubtopicInfo();
        ResearcherPortalCaseController.createCase(serailizedString);
        ResearcherPortalCaseController.createCase(portalSerailizedString);
        ResearcherPortalCaseController.getAllfeedbackTopicInfo();
        System.runAs(u) {
            
            ResearcherPortalCaseHelper.getAllRecsFromTopicSubtopicQueueMapping();
            ResearcherPortalCaseHelper.getAllRecsFromTopicSubtopicQueueMapping();
            ResearcherPortalCaseHelper.getPicklistValues('case','PortalFeedback__c');            
        } 
    }    
        /************************************************************************************
// Purpose      :  Test functionality for ResearcherPortalSupportController
// Developer    :  Md Ali
// Created Date :  12/05/2018                 
//***********************************************************************************/
    
    @isTest
    public static void testNegativeUseCaseForResearcherPortalCaseHelperMethods(){
        User user1 =[SELECT Id,ContactId FROM User WHERE UserName='test.communityUser1@test.com.testDev' LIMIT 1]; 
        System.assert(user1!= null);
        
        System.runAs(user1) {
            
            Map<String, list<String>> topicSubtopicQueueMapping= ResearcherPortalCaseHelper.getAllRecsFromTopicSubtopicQueueMapping();
            System.assert(topicSubtopicQueueMapping!= null);
            //ResearcherPortalCaseHelper.getAllRecsFromTopicSubtopicQueueMapping();
            List<String> lstPickvals = ResearcherPortalCaseHelper.getPicklistValues('case','PortalFeedback__c');
            System.assert(topicSubtopicQueueMapping!= null);
            
        } 
    }    
}