/*****************************************************************
Name: ProductCatalogRefreshBatchApiTest
Author: Pawan [CloudSense]
Purpose: Test Class For ProductCatalogRefreshBatchApi 
*****************************************************************/
/*==================================================================
History
--------
Version   Author           Date              Detail
1.0       Pawan            27/09/2018         Initial Version
********************************************************************/
@isTest
public class ProductCatalogRefreshBatchApiTest {
    
    @testSetup 
    static void setupTestData() {
        
        Account accDepartment = TestUtility.createTestAccount(true
            , 'Department Account 001'
            , Schema.SObjectType.Account.getRecordTypeInfosByName().get('Academic Institution').getRecordTypeId());
        
        Account accAdministrative = TestUtility.createTestAccount(true
            , 'Administrative Account 001'
            , Schema.SObjectType.Account.getRecordTypeInfosByName().get('Administrative').getRecordTypeId());

        Account accProgram = TestUtility.createTestAccount(true
            , 'Program Account 001'
            , Schema.SObjectType.Account.getRecordTypeInfosByName().get('Academic Program').getRecordTypeId());
            
        Opportunity opp = TestUtility.createTestOpportunity(true, 'Test Opportunity', accDepartment.Id);
        List<String> lstParams = new List<String>{'Test', 'Con'};
        Contact con = TestUtility.createTestContact(true
            , lstParams
            , accAdministrative.Id);
        
        User u = TestUtility.createUser('Partner Community User', false);
        u.ContactId = con.Id;
        insert u;
        
        cscfga__Product_Category__c productCategory = new cscfga__Product_Category__c(Name = 'Test category');
        insert productCategory;
        
        cscfga__Product_Definition__c prodDefintion = new cscfga__Product_Definition__c (Name = 'Test definition'
            , cscfga__Product_Category__c = productCategory.Id
            , cscfga__Description__c = 'Test definition 1');
        insert prodDefintion;
        
        cscfga__Attribute_Definition__c attrDef = new cscfga__Attribute_Definition__c(Name = 'Test Attribute Definition'
            , cscfga__Product_Definition__c = prodDefintion.Id
            , cscfga__Is_Line_Item__c = true
            , cscfga__Line_Item_Description__c = 'Sample Attr'
            , cscfga__Line_Item_Sequence__c = 0 
            , cscfga__is_significant__c = true);
        insert attrDef;
        
        hed__Term__c term = new hed__Term__c(hed__Account__c = accDepartment.Id);
        insert term;

        hed__Course__c course = new hed__Course__c(Name = 'Course 001'
            , recordTypeId = Schema.SObjectType.hed__Course__c.getRecordTypeInfosByName().get('21CC').getRecordTypeId()
            , hed__Account__c = accDepartment.Id);
        insert course;
       
        hed__Course_Offering__c courseOffering = new hed__Course_Offering__c(Name = 'Course Offering 001'
            , hed__Course__c = course.Id
            , hed__Term__c = term.Id 
            , hed__Start_Date__c = system.today()
            , hed__End_Date__c  = system.today());
        insert courseOffering;

        hed__Course_Enrollment__c courseEnrolment = new hed__Course_Enrollment__c(hed__Status__c = 'Enrolled'
            , hed__Course_Offering__c = courseOffering.id
            , hed__Contact__c = con.Id);
        insert courseEnrolment;
        
        cspmb__Price_Item__c priceItem = new cspmb__Price_Item__c(Name = course.Name
            , Course_Offering_ID__c = courseOffering.Id
            , cspmb__Product_Definition_Name__c = 'Test definition'
            , cspmb__Effective_Start_Date__c = courseOffering.hed__Start_Date__c
            , cspmb__Effective_End_Date__c = courseOffering.hed__End_Date__c
            , cspmb__Is_Active__c = true
            , Course_Program__c  = 'Course'
            , Product_Line__c = 'Sample Product'
            , Program__c = accProgram.Id
            , cspmb__One_Off_Cost__c = 2
            , cspmb__One_Off_Charge__c = 4);
        insert priceItem;
        
        cspmb__Add_On_Price_Item__c addonPriceItem = new cspmb__Add_On_Price_Item__c(name = 'All');
        insert addonPriceItem;
        
        cspmb__Price_Item_Add_On_Price_Item_Association__c priceItemAssociation = new cspmb__Price_Item_Add_On_Price_Item_Association__c(cspmb__Add_On_Price_Item__c=addonPriceItem.Id,cspmb__Price_Item__c=priceItem.Id,cspmb__One_Off_Charge__c = 4);
        insert priceItemAssociation; 
        
        CS_Commercial_Product_Refresh_Setting__c productRefreshSetting = new CS_Commercial_Product_Refresh_Setting__c(Last_Modified_Date_Before__c = 24);
        insert productRefreshSetting;
        
        Config_Data_Map__c configDataMapObject = new Config_Data_Map__c(Name = 'ProductRefreshObjectName',Config_Value__c= 'price');
        insert configDataMapObject;
        
        Config_Data_Map__c configDataMapDelta = new Config_Data_Map__c(Name = 'ProductRefreshDeltaFlag',Config_Value__c= 'true');
        insert configDataMapDelta;       
        
    }
   
   /* @isTest
    public static void testProductCatalogRefreshBatchApi(){                
        
        Test.startTest();  
        schedulerTest(); 
        TestDataFactoryUtil.deltaTestSuccess();  
        List<cspmb__Price_Item__c> priceItems = [Select Id from cspmb__Price_Item__c where cspmb__Is_Active__c = true];
        System.Assert(priceItems.size() > 0 , 'Invalid Data');    
        ProductCatalogRefreshBatchApi productCatalogbatch = new ProductCatalogRefreshBatchApi();    
        Database.executeBatch(productCatalogbatch);           
        Test.setMock(HttpCalloutMock.class, new iPaasDeltaLoadCalloutServiceMock2()); 
        Test.stopTest();
        
    } */       
    
    public class iPaasDeltaLoadCalloutServiceMock2 implements HttpCalloutMock {        
        public HTTPResponse respond(HTTPRequest request) {           
            String jsonBody = '{"id": "ID-dev-20180718T035647828","result": "ERROR","code": "400","application": "rmit-publisher-api-sfdc","provider": "iPaaS","payload": "Message Sending Failed"}';
            HttpResponse response = new HttpResponse();
            response.setHeader('Content-Type', 'application/json');
            response.setBody(jsonBody);
            response.setStatusCode(400);
            return response;
     }
        
    }
    
    /********************************************************************
    // Purpose              : Creating Scheduled Job for test class                             
    // Author               : CloudSense [Pawan]
    // Parameters           : null
    // Returns              : void
    // JIRA Reference       : ECB-4489 - Test Method for creating a scheduled job  
    //********************************************************************/ 
    public static void schedulerTest() 
    {
        String cronexp = '0 15 17 ? * MON-FRI';        
        ProductCatalogRefreshBatchApi productCatalogbatch = new ProductCatalogRefreshBatchApi();        
        String jobId = System.schedule('Product Data sync',  cronexp, productCatalogbatch);
        CronTrigger ct = [SELECT Id, state, PreviousFireTime, CronExpression, TimesTriggered, NextFireTime FROM CronTrigger WHERE id = :jobId];
        System.assertEquals(cronexp, ct.CronExpression);
        System.assertEquals(0, ct.TimesTriggered);        
    }    
}