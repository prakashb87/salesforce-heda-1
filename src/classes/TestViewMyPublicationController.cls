/*******************************************
Purpose: Test class ViewMyPublicationController
History:
Created by Shalu Chaudhary on 01/11/2018
*******************************************/
@isTest
public class TestViewMyPublicationController {
    
    
    /************************************************************************************
// Purpose      :  Creating Test data for Community User for all test methods 
// Developer    :  Shalu
// Created Date :  11/01/2018                 
//***********************************************************************************/
    @testSetup static void testDataSetup() {
        
        RSTP_TestDataFactoryUtils.createNonCommunityUsersWithProfile(1,'System Administrator','Activator');
        
        User adminUser =[SELECT Id,ContactId FROM User WHERE UserName='test.nonCommunityUser1@test.com.testDev' LIMIT 1];
        System.runAs(adminUser)   
        {
            Map<String,Integer> allnumbersOfDataMap = new Map<String,Integer>();
            allnumbersOfDataMap.put('user',1);
            allnumbersOfDataMap.put('account',1);
            allnumbersOfDataMap.put('contact',1);
            
            Map<String,String> requiredInfoMap = new Map<String,String>();
            requiredInfoMap.put('userType','CommunityUsers');
            requiredInfoMap.put('profile','RMIT Researcher Portal Plus Community User');
            
            Boolean userCreatedFlag = RSTP_TestDataFactoryUtils.testRSTPDataSetup(allnumbersOfDataMap,requiredInfoMap);
            System.assert(userCreatedFlag==true,'Community User Created'); 
        }
        
    }    
    
    /************************************************************************************
// Purpose      :  Test functionality for View MyPublication List Controller
// Developer    :  Ankit
// Created Date :  10/24/2018                 
//***********************************************************************************/
    
    @isTest
    public static void getPublicationListWrapperTestMethod(){
        User u =[SELECT Id,ContactId FROM User WHERE UserName='test.communityUser1@test.com.testDev' LIMIT 1]; 
        
        System.runAs(u) {
            
            List<Publication__c> publicationList = new List<Publication__c>();
            for(integer i = 0; i < 5; i++){
                
                Publication__c publication = new Publication__c();
                publication.Publication_Id__c=123456+i;
                publication.Year_Published__c = 2017;
                publication.Publication_Category__c = ' A1 - Book';
                publication.Publication_Title__c = 'Test Title' + i;
                publication.Outlet__c = 'Test';
                publication.Audit_Result__c   = '39 active';
                publication.Description__c='Testing';
                publication.Keywords__c='publication key';
                publicationList.add(publication);  
            }
            insert publicationList;
            System.Assert(publicationList.size()== 5);
            
            Ethics__c ethicObj = new Ethics__c();
            ethicObj.Ethics_Id__c='TestEthics';
            ethicObj.Application_Title__c = 'Test';
            ethicObj.Application_Type__c = 'HUMAN = Human Ethics';
            ethicObj.Status__c = 'Amended';
            ethicObj.Approved_Date__c = date.today();
            ethicObj.Expiry_Date__c   = date.today().addDays(10);
            insert ethicObj;
            system.assert(ethicObj!=null);
            
            Ethics__c ethicTest1 = new Ethics__c();
            ethicTest1.Ethics_Id__c='TestEthics1';
            ethicTest1.Application_Title__c = 'Test';
            ethicTest1.Application_Type__c = 'HUMAN = Human Ethics';
            ethicTest1.Status__c = 'Amended';
            ethicTest1.Approved_Date__c = date.today();
            ethicTest1.Expiry_Date__c   = date.today().addDays(10);
            insert ethicTest1;
            system.assert(ethicTest1!=null);
            
            ResearcherPortalProject__c researchProject = new ResearcherPortalProject__c();
            researchProject.Title__c = 'Test';
            insert researchProject;
            System.Assert(researchProject.Title__c == 'Test');
            
            List<Researcher_Member_Sharing__c> memberResearchList = new List<Researcher_Member_Sharing__c>();
            Researcher_Member_Sharing__c ethicsMemberSharing = new Researcher_Member_Sharing__c();
            ethicsMemberSharing.Ethics__c = ethicTest1.id;
            ethicsMemberSharing.User__c   = u.id;
            memberResearchList.add(ethicsMemberSharing);
            
            Researcher_Member_Sharing__c projectMemberSharing = new Researcher_Member_Sharing__c();
            projectMemberSharing.Researcher_Portal_Project__c = researchProject.id;
            projectMemberSharing.User__c   = u.id;
            memberResearchList.add(projectMemberSharing);
            
            Researcher_Member_Sharing__c publicationMemberSharing = new Researcher_Member_Sharing__c();
            publicationMemberSharing.Publication__c = publicationList[0].id;
            publicationMemberSharing.User__c   = u.id;
            memberResearchList.add(publicationMemberSharing);
            insert memberResearchList;
            System.Assert(memberResearchList.size( )> 0 );
            
            Publication_Classification__c pubclass = new Publication_Classification__c();
            pubclass.For_Six_Digit_Detail__c='Test six digit';
            pubclass.Publication__c=publicationList[0].id;
            insert pubclass;
            
            List<PublicationDetailsWrapper.publicationWrapper> publicationWrapperList = new List<PublicationDetailsWrapper.publicationWrapper>();
            PublicationDetailsWrapper.publicationWrapper  publicationWrapper = new PublicationDetailsWrapper.publicationWrapper();
            publicationWrapper.year       = 2016;
            publicationWrapper.category   = 'TestData';
            publicationWrapper.title      = 'Test';
            publicationWrapper.outlet     = 'Test';
            publicationWrapper.status     = 'Test';
            publicationWrapperList.add(publicationWrapper);
            
            PublicationDetailsWrapper.PublicationDetailsWithTotalsWrapper totalwrapper = new PublicationDetailsWrapper.PublicationDetailsWithTotalsWrapper();
            totalwrapper.totalPublications = 1;
            totalwrapper.publicationWrapperList = publicationWrapperList;
            
            ViewMyPublicationHelper.getPublicationWrapperList();
            ViewMyPublicationController.getPublicationDetails(publicationList[0].id);
        }
        ViewMyPublicationController.getPublicationListWrapper();
 
        
        //Added for covering picklist returning methods
        List<String> statusPickList = ViewMyPublicationController.getPublicationStatusValue();
        system.assert(statusPickList!=null && !statusPickList.isEmpty());
        List<String> categoryPickList = ViewMyPublicationController.getPublicationCategoryValue();
        system.assert(categoryPickList!=null && !categoryPickList.isEmpty());
    }
    
    /************************************************************************************
// Purpose      :  Test functionality for View MyPublication List Controller
// Developer    :  Ankit
// Created Date :  12/05/2018                 
//***********************************************************************************/
    
    @isTest
    public static void getPublicationListWrapperNegativeMethod(){
        User u =[SELECT Id,ContactId FROM User WHERE UserName='test.communityUser1@test.com.testDev' LIMIT 1]; 
        
        System.runAs(u) {
            String returnValue= ViewMyPublicationController.getPublicationListWrapper();
            system.assert(returnValue!=null);   
            
            //Added for covering picklist returning methods
            List<String> statusPickList = ViewMyPublicationController.getPublicationStatusValue();
            system.assert(statusPickList!=null && !statusPickList.isEmpty());
            List<String> categoryPickList = ViewMyPublicationController.getPublicationCategoryValue();
            system.assert(categoryPickList!=null && !categoryPickList.isEmpty());
            
        }    
        
    }
    
    @isTest
    public static void testExceptionHandlingMethod(){
        
        User u =[SELECT Id,ContactId FROM User WHERE UserName='test.communityUser1@test.com.testDev' LIMIT 1]; 
        System.assert(u!=null);
        
        System.runAs(u) {      
            
            ViewMyPublicationController.isForceExceptionRequired=true;
            ViewMyPublicationController.getPublicationListWrapper();
            
        }
    }

}