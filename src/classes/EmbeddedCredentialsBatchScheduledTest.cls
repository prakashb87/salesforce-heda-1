@isTest
private class EmbeddedCredentialsBatchScheduledTest {
	
	@isTest static void testEmbeddedCredentialsBatchScheduled() {
		Test.startTest();
        EmbeddedCredentialsBatchScheduled embCrSch =new EmbeddedCredentialsBatchScheduled(); 
        String sch = '0 0 23 * * ?';
        system.schedule('Test schedule', sch, embCrSch);
        Test.stopTest();
	}
}