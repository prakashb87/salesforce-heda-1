/*********************************************************************************
 *** @TestClassName     : ContactTrigger_Test
 *** @Author		    : SF HEDA Team
 *** @Requirement     	: To Test the ContactTrigger_Test
 *** @Created date    	: 22/11/2017
 **********************************************************************************/
@isTest
public class ContactTrigger_Test {
    @isTest static void testInsert() {
        Contact con = new Contact(LastName='Test');
        con.First_in_family_at_university__c = true;
        con.Early_highschool_leaver__c = true;
        con.Previous_low_GPA__c = true;
        con.Employed_fulltime_or_time_poor__c = true;
        con.Unemployed_or_unskilled__c = true;
        con.Course_engagement_level__c = true;
        con.Lives_20km_or_further_from_RMIT__c = true;
        insert con;
        Contact cont = [select LastName from contact where id=:con.id];
        System.assertEquals(cont.LastName,'Test');
       
    }
    @isTest static void testUpdate(){
        Contact con = new Contact(LastName='Test name');
        con.First_in_family_at_university__c = false;
        con.Early_highschool_leaver__c = false;
        con.High_Risk__c = False;
        con.Previous_low_GPA__c = false;
        con.Employed_fulltime_or_time_poor__c = false;
        con.Unemployed_or_unskilled__c = false;
        con.Course_engagement_level__c = false;
        con.Lives_20km_or_further_from_RMIT__c = false;
        con.Assessed_risk_level__c = 'Low';
        insert con;
        case c = new case(Status = 'In Progress',Origin='Email',ContactId= con.Id);
        insert c;
        Contact cont = [select LastName from contact where id=:con.id];
        System.assertEquals(cont.LastName,'Test name');
        //con.RecordTypeId = Schema.SObjectType.Contact.RecordTypeInfosByName.get('Student').RecordTypeId;
        con.First_in_family_at_university__c = true;
        con.Early_highschool_leaver__c = true;
        con.Previous_low_GPA__c = true;
        con.Employed_fulltime_or_time_poor__c = true;
        con.Unemployed_or_unskilled__c = true;
        con.Course_engagement_level__c = true;
        con.Lives_20km_or_further_from_RMIT__c = true;
        con.High_Risk__c = True;
        con.Assessed_risk_level__c = 'Medium';
        update con;
        con.High_Risk__c = false;
        con.Assessed_risk_level__c = 'High';
        update con;
        ContactTriggerHandler cth = new ContactTriggerHandler();
        cont = [select Course_engagement_level__c from contact where id=:con.id];
        System.assertEquals(cont.Course_engagement_level__c,true);
    }
    //To test the prefferredEmail 
    @isTest static void emailTest(){
        Contact con4 = new Contact(lastname = 'Test name3', hed__Preferred_Email__c = 'Alternate', Work_Email__c = 'testUniversity2@gmail.com',
        							email = 'testUniversity2@gmail.com', Home_Email__c = 'testUniversity2@gmail.com',
        							Other_Email__c = 'testUniversity2@gmail.com',
        							Latest_Email__c = 'testUniversity2@gmail.com',hed__UniversityEmail__c = 'testUniversity2@gmail.com', 
        							hed__WorkEmail__c = 'testwork2@gmail.com', hed__AlternateEmail__c = 'testalternate2@gmail.com',
        							Home_Email_manual__c= 'testalternate2@gmail.com');
        insert con4;
        System.assertEquals('testUniversity2@gmail.com', con4.Email);
    }
}