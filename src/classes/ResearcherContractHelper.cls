/*******************************************
Purpose: 
History:
Created by Subhajit on 12/10/18
*******************************************/
@SuppressWarnings('PMD.ApexCRUDViolation')
public with Sharing class ResearcherContractHelper {

    
    public static final string NO_ACCESS_ROLE =Label.RSTP_Role_No_Access; // 891

    /*****************************************************************************************
    // JIRA NO      :  470/314/469
    // SPRINT       :  7
    // Purpose      :  Preparing topic to Subtopic list in a map
    // Parameters   :  ResearcherCaseDetailsWrapper
    // Developer    :  Subhajit
    // Created Date :  12/10/2018                 
    //****************************************************************************************/
           
    public static List<ResearcherContractWrapper> getResearcherContractRecords(){
                     
        List<Researcher_Member_Sharing__c> reseacherMemberSharingList = new List<Researcher_Member_Sharing__c>();
        Id contractRecordTypId = Schema.SObjectType.Researcher_Member_Sharing__c.getRecordTypeInfosByName().get('Research Project Contract').getRecordTypeId();  // Ankit,391
        Set<Id> contractIdSet = new Set<Id>();
                
        Set<Id> recordTypeIdSet = new Set<Id> {contractRecordTypId}; //Changed variable name from RecordTypeIdSet to recordTypeIdSet for PMD Fix
        
        reseacherMemberSharingList = geResearcherMemberSharingList(recordTypeIdSet, userInfo.getUserId()); //Changed variable name from RecordTypeIdSet to recordTypeIdSet for PMD Fix
       
        for(Researcher_Member_Sharing__c eachmemberSharing :reseacherMemberSharingList){
          contractIdSet.add(eachmemberSharing.ResearchProjectContract__c);
        }
        System.debug('@@@contractIdSet'+contractIdSet);

        List<ResearcherContractWrapper> contractRecsWrapper = new List<ResearcherContractWrapper>();
        if((Schema.sObjectType.ResearchProjectContract__c.isAccessible()) && (Schema.sObjectType.ResearchProjectContract__c.fields.ContractTitle__c.isAccessible())){  //added by ali PMD voilation
        List<ResearchProjectContract__c> contractRecs= [SELECT Id, Ecode__c, ContractTitle__c, Status__c, RIReviewer__c,
                                                         Contract_Logged_Date__c, Current_Active_Action__c, StorageReferenceNumber__c,
                                                         Fully_Executed_Date__c, Contact_Preferred_Full_Name__c, LInkedPrimaryOrganisationName__c,
                                                         Activity_Type__c, Contract_Type__c, PreExecutionManager__c 
                                                         FROM ResearchProjectContract__c
                                                        WHERE  ID IN:contractIdSet   // Ankit Changes for 391, Contrcat Permissions
                                                        ORDER BY Status__c asc];
        
        System.debug('@@@@@contractRecs==>'+JSON.serializePretty(contractRecs));
        
        for(ResearchProjectContract__c eachContract : contractRecs)
        {
            ResearcherContractWrapper eachContractWrapper = new ResearcherContractWrapper(eachContract);
            contractRecsWrapper.add(eachContractWrapper);
        }
        }
        return contractRecsWrapper;
    }
    
    /*****************************************************************************************
    // JIRA NO      :  251
    // SPRINT       :  11
    // Purpose      :  Preparing topic to Subtopic list in a map
    // Parameters   :  projectId
    // Developer    :  Ankit 
    // Created Date :  12/02/2019                 
    //****************************************************************************************/
    public static List<ResearcherContractWrapper> getProjectContractsWrapper(Id projectId)
        {
            List<ResearcherContractWrapper> contractWrapperList = new List<ResearcherContractWrapper>();
                          
            List<ResearchProjectContract__c> researchProjectContracts = new List<ResearchProjectContract__c>();
            Set<Id> contractIds = new Set<Id>();
            
            List <ProjectContract__c> projectContracts = ApexWithoutSharingUtils.getProjectContractsList(projectId); 
            
           /* [SELECT Id,Research_Project_Contract__c,Researcher_Portal_Project__c 
                                                                    FROM ProjectContract__c
                                                                    WHERE Researcher_Portal_Project__c =: projectId]; */
            
            for (ProjectContract__c eachContract : projectContracts)
            {
                contractIds.add(eachContract.Research_Project_Contract__c);
            }
             if((Schema.sObjectType.ResearchProjectContract__c.isAccessible()) && (Schema.sObjectType.ResearchProjectContract__c.fields.ContractTitle__c.isAccessible())){  //added by ali PMD voilation
          
          
            researchProjectContracts = ApexWithoutSharingUtils.getContractList(contractIds);
            System.debug('@@@researchProjectContracts'+researchProjectContracts);
            /*researchProjectContracts = [SELECT Id, Ecode__c, ContractTitle__c, Status__c, RIReviewer__c,
                                                         Contract_Logged_Date__c, Current_Active_Action__c, StorageReferenceNumber__c,
                                                         Fully_Executed_Date__c, Contact_Preferred_Full_Name__c, LInkedPrimaryOrganisationName__c,
                                                         Activity_Type__c, Contract_Type__c, PreExecutionManager__c 
                                                         FROM ResearchProjectContract__c
                                                         WHERE Id IN : contractIds]; */
            
            for(ResearchProjectContract__c eachContract :researchProjectContracts){
                ResearcherContractWrapper wrapper = new ResearcherContractWrapper(eachContract);
                contractWrapperList.add(wrapper);
            }
           System.debug('@@@contractWrapperList'+json.serializePretty(contractWrapperList));
             }
            return contractWrapperList;
        }
        
        
    /*****************************************************************************************
    // Purpose      :  This is a method to return list of Reasercher member Sharing based on Set of RecordtypeIds and UserId for ISRMMember = true ,891
    // Parameters   :  Set<Id>,Id
    // Developer    :  Ankit
    // Created Date :  17/02/2019                 
    //****************************************************************************************/
    public static List<Researcher_Member_Sharing__c> geResearcherMemberSharingList(Set<Id> recordTypeIdSet, Id userId){ 

        System.debug('@@@userIdCheck'+userId+'@@@RecordTyperId'+recordTypeIdSet);
        List<Researcher_Member_Sharing__c> reseacherMemberSharingList = new List<Researcher_Member_Sharing__c>();
         if((Schema.sObjectType.Researcher_Member_Sharing__c.isAccessible()) && (Schema.sObjectType.Researcher_Member_Sharing__c.fields.Ethics__c.isAccessible())){  //added by ali PMD voilation
        reseacherMemberSharingList = [SELECT id, Contact__c, Ethics__c, Account__c, Publication__c,User__c,
                                              Researcher_Portal_Project__c, ResearchProjectContract__c,MemberRole__c
                                              FROM Researcher_Member_Sharing__c 
                                              WHERE User__c =: UserId
                                              AND RecordTypeId IN : RecordTypeIdSet
                                              AND AccessLevel__c != : NO_ACCESS_ROLE
                                              AND Currently_Linked_Flag__c  = true
                                              AND RM_Member__c = true ];
                                                                   
        System.debug('@@@reseacherMemberSharingList1'+reseacherMemberSharingList);
         }
        return reseacherMemberSharingList;
    }
    
/*****************************************************************************************
    // Purpose      :  This method is return the researchProjectContracts wrapper to display the researchProjectContracts details
    // Parameters   :  contractId
    // Developer    :  Md Ali
    // Created Date :  20/02/2019                 
    //****************************************************************************************/
    
    public static List<ResearcherContractWrapper> getContractsHelper(Id contractId)
    {
        List<ResearcherContractWrapper> contractWrapperList = new List<ResearcherContractWrapper>();
         if((Schema.sObjectType.ResearchProjectContract__c.isAccessible()) && (Schema.sObjectType.ResearchProjectContract__c.fields.ContractTitle__c.isAccessible())){  //added by ali PMD voilation
        List<ResearchProjectContract__c> contractsList = [SELECT Id, Ecode__c, ContractTitle__c, Status__c, RIReviewer__c,
                                                         Contract_Logged_Date__c, Current_Active_Action__c, StorageReferenceNumber__c,
                                                         Fully_Executed_Date__c, Contact_Preferred_Full_Name__c, LInkedPrimaryOrganisationName__c,
                                                         Activity_Type__c, Contract_Type__c, PreExecutionManager__c,
                                                         (Select id, User__c, User__r.name ,Position__c , AccessLevel__c From Researcher_Portal_Member_Sharings__r ) //890:Ali:get RMS member role
                                                         FROM ResearchProjectContract__c 
                                                         WHERE Id = :  contractId];
        for(ResearchProjectContract__c eachContract :contractsList){
            ResearcherContractWrapper wrapper = new ResearcherContractWrapper(eachContract);
            contractWrapperList.add(wrapper);
        }
         }
        System.debug('###contractWrapperList'+contractWrapperList);
        return contractWrapperList;
    }
    
    /*****************************************************************************************
    // Purpose      :  This is a method to return the list of Milestones for a Contract in the Contract Detail page,889
    // Parameters   :  Id (Contract Id)
    // Developer    :  Ankit
    // Created Date :  26/02/2019                 
    //****************************************************************************************/
    public static List<MilestonesDetailsWrapper.milestonesWrapper> getContractMilestonesList(Id contractId){ 

       // Id contractMilestoneRecordTypeId = Schema.SObjectType.SignificantEvent__c.getRecordTypeInfosByName().get(Label.RSTP_MilestoneContractRecordtype).getRecordTypeId();
        List<MilestonesDetailsWrapper.milestonesWrapper> milestonesWrapperList = new List<MilestonesDetailsWrapper.milestonesWrapper>();
        List<SignificantEvent__c> milestonesList = new List<SignificantEvent__c>();
      //   if((Schema.sObjectType.SignificantEvent__c.isAccessible()) && (Schema.sObjectType.SignificantEvent__c.fields.ActualCompletionDate__c.isAccessible())){  //added by ali PMD voilation
      /*  milestonesList = [Select id, ActualCompletionDate__c, DueDate__c, Ethics__c, Ethics__r.Application_Title__c,  EventDescription__c,IsAction__c,
                                              IsActionDue__c, Publication__c, Publication__r.Publication_Title__c, ResearchProject__c, ResearchProject__r.Title__c,
                                              ResearchProjectContract__c, ResearchProjectContract__r.ContractTitle__c,
                                              SignificantEventType__c, Status__c , RecordTypeId
                                              From SignificantEvent__c 
                                              where ResearchProjectContract__c =:contractId
                                              AND RecordTypeId =: contractMilestoneRecordTypeId
                                              ORDER BY DueDate__c DESC  NULLS LAST ]; */
                                              
        milestonesList = ApexWithoutSharingUtils.getContractMilestoneList(contractId);                                      
        
        System.debug('@@@milestonesList'+milestonesList);
   
        for(SignificantEvent__c eachMilestone : milestonesList){
            
            MilestonesDetailsWrapper.milestonesWrapper wrapper = new MilestonesDetailsWrapper.milestonesWrapper(eachMilestone);
            milestonesWrapperList.add(wrapper);
                    
        }
        System.debug('@@@milestonesWrapperList'+json.serializePretty(milestonesWrapperList));
        return milestonesWrapperList;
    }    
}