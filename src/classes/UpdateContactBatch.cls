/*********************************************************************************
 *** @Class			    : UpdateContactBatch
 *** @Author		    : Shubham Singh
 *** @Requirement     	: Batch for Updading values Of ProgramEnrollment on Contact
 *** @Created date    	: 14/11/2017
 **********************************************************************************/
/*****************************************************************************************
 *** @About Class
 *** This class is a Batch class that is used to update the previous records of contact
 *** according to the related ProgramEnrollment.
 *****************************************************************************************/
@SuppressWarnings('PMD.AvoidGlobalModifier')
global class UpdateContactBatch implements Database.Batchable < sObject > , Database.Stateful {
    // Method to query and pass the records of ProgramEnrollment to the execute Method.
    global Database.QueryLocator start(Database.BatchableContext bc) {
        return Database.getQueryLocator(
            'SELECT ID,Program_Action_Value__c,Action_Date__c,Program_Value__c,Program_Action__c,hed__Graduation_Year__c,hed__Contact__c FROM hed__Program_Enrollment__c'
        );
    }
    //Method to process the related values of ProgramEnrollment On contact
    global void execute(Database.BatchableContext bc, List < hed__Program_Enrollment__c > scope) {
        // process each batch of records
        set < Id > contactId = new set < Id > ();
        Map < id, Contact > mapContacts = new Map < id, Contact > ();
        for (hed__Program_Enrollment__c qPE: scope) {
            if (qPE.hed__Contact__c != null)
            {
                contactId.add(qPE.hed__Contact__c);
            }
        }
        List < AggregateResult > vLstAggr = [SELECT Sum(Program_Action_Value__c) NPAV, hed__Contact__c FROM hed__Program_Enrollment__c WHERE hed__Program_Enrollment__c.hed__Contact__c In: contactId GROUP BY hed__Contact__c];
        Contact vContact;
        Map < id, Contact > vLstContacts = new Map < id, Contact > ();
        If(vLstAggr.size() > 0) {
            for (AggregateResult vAggr: vLstAggr) {
                string conId = (string) vAggr.get('hed__Contact__c');
                decimal countOfChild = (decimal) vAggr.get('NPAV');
                vContact = new Contact(Id = conId, Number_of_active_program_enrollments__c = countOfChild);
                vLstContacts.put(vContact.Id, vContact);
            }
        }
        if (vLstContacts.size() > 0) {
            Database.update(vLstContacts.values(), false);
        }
        if (mapContacts.size() > 0) {
            Database.update(mapContacts.values(), false);
        }
    }
    //Method of Impemented class
    global void finish(Database.BatchableContext bc) {
         system.debug('UpdateContactBatch--finish');
    }
}