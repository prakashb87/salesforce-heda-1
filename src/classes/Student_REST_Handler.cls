//Author: Khushman Deomurari
//Cyclomatic Complexity Reached. No new code can be added in this file.

public with sharing class Student_REST_Handler {

    public static Map<String,Id> studentAndContactId;
    public static List<hed__Address__c> returningAddressList;
    
    
    public static void handlePersonalDetails(List<Date> personalEffDateList, Map<Date, Student_Rest.PersonalDetail> mapOfPersonalEffDate, Contact extractedContact){
        if(personalEffDateList.size() > 0){
            personalEffDateList.sort();
            Integer listSize = personalEffDateList.size()-1; 
            extractedContact.External_Unique_ID__c =   'CN-ST:' + mapOfPersonalEffDate.get(personalEffDateList[listSize]).studentId;       
            extractedContact.Student_ID__c = mapOfPersonalEffDate.get(personalEffDateList[listSize]).studentId;
            extractedContact.Effective_Date__c = Date.valueOf(mapOfPersonalEffDate.get(personalEffDateList[listSize]).effectiveDate);
            String postGender = mapOfPersonalEffDate.get(personalEffDateList[listSize]).gender;
			
			Map<String, String > genderMap=new Map<String, String >{ 'M' => 'Male', 'F' => 'Female', 'U' => 'Unspecified', 'X' => 'Other'};
			for (String currentGender: genderMap.keySet()){
			    if(postGender == currentGender){
                	extractedContact.hed__Gender__c = genderMap.get(currentGender);
            	}
			}
            //extractedContact.hed__Gender__c = mapOfPersonalEffDate.get(personalEffDateList[listSize]).gender;
        }
    }
    
    public static void setPreferredEmail(String emailType, String emailPreferred, Contact extractedContact){
    	if ((emailPreferred == 'Y') || (emailPreferred == 'Yes') || (emailPreferred == 'True') || (emailPreferred == 'T')){
    		extractedContact.hed__Preferred_Email__c = emailType;
    	}
    }

	public static void handleEmails(List<Student_REST.Email> emails, Contact extractedContact){

        if(emails != null){
            for(Student_REST.Email srEmail: emails){
                
                String emailPreferred = '';     
                emailPreferred = srEmail.preferedEmailIndicator;
                
                if(srEmail.emailType == 'CAMP') { 
                    extractedContact.hed__UniversityEmail__c = srEmail.emailAddress; 
                    Student_REST_Handler.setPreferredEmail('University', emailPreferred, extractedContact);
                } if(srEmail.emailType == 'BUSN'){
                    extractedContact.hed__WorkEmail__c = srEmail.emailAddress;
                    extractedContact.Work_Email__c = srEmail.emailAddress;
                    Student_REST_Handler.setPreferredEmail('Work', emailPreferred, extractedContact);
                } if (srEmail.emailType == 'HOME'){
                    extractedContact.Email = srEmail.emailAddress;
					Student_REST_Handler.setPreferredEmail('Home', emailPreferred, extractedContact);
                } if(srEmail.emailType == 'Alternate'){
                    extractedContact.hed__AlternateEmail__c = srEmail.emailAddress;
					Student_REST_Handler.setPreferredEmail('Alternate', emailPreferred, extractedContact);
                } if(srEmail.emailType == 'PART'){
                    extractedContact.SAMS_Partner_Email__c = srEmail.emailAddress;
					Student_REST_Handler.setPreferredEmail('Partner', emailPreferred, extractedContact);
                }
                /*else if(srEmail.emailType == 'SCHL'){ //Not Required Confirmd by SIMI and NIKHIL - RM-1681
                    extractedContact.{Not Required Confirmd by SIMI and NIKHIL} = srEmail.emailAddress; 
                    if ((emailPreferred == 'Y') || (emailPreferred == 'Yes') || (emailPreferred == 'True') || (emailPreferred == 'T')){
                        extractedContact.hed__Preferred_Email__c = 'Not Required Confirmd by SIMI and NIKHIL';
                    }
                }*/
                
                
            }
        }

	}
	
    public static void setPreferredPhone(String phoneType, String preferredPhoneIndicator, Contact extractedContact){
                        if(preferredPhoneIndicator == 'Y'){
                            extractedContact.hed__PreferredPhone__c = phoneType;    
                        }
    }
    
    public static void handlePhones(List<Student_REST.Phone> phones, Contact extractedContact){
                for(Student_REST.Phone srPhone: phones){
                    if(srPhone.phoneType == 'HOME'){
                        extractedContact.HomePhone = srPhone.phoneNumber;
                        setPreferredPhone('Home', srPhone.preferredPhoneIndicator, extractedContact);
                    }
                    if(srPhone.phoneType == 'WKPL'){
                        extractedContact.hed__WorkPhone__c = srPhone.phoneNumber;
                        setPreferredPhone('Work', srPhone.preferredPhoneIndicator, extractedContact);
                    } 
                    if(srPhone.phoneType == 'MOBL'){
                        extractedContact.MobilePhone = srPhone.phoneNumber;
                        setPreferredPhone('Mobile', srPhone.preferredPhoneIndicator, extractedContact);
                    }
                    if(srPhone.phoneType == 'MAIL'){
                        extractedContact.Mailing_Phone__c = srPhone.phoneNumber;
                        setPreferredPhone('Mailing', srPhone.preferredPhoneIndicator, extractedContact);
                    }
                    if(srPhone.phoneType == 'ARES'){
                        extractedContact.Australian_Residence_phone__c = srPhone.phoneNumber;
                        setPreferredPhone('Australian Residence', srPhone.preferredPhoneIndicator, extractedContact);
                    }
                    if(srPhone.phoneType == 'FAX'){
                        extractedContact.Fax = srPhone.phoneNumber;
                        setPreferredPhone('Fax', srPhone.preferredPhoneIndicator, extractedContact);
                    }
                    if(srPhone.phoneType == 'AGNT'){
                        extractedContact.Agent_Phone__c = srPhone.phoneNumber;
                        setPreferredPhone('Educational Agent', srPhone.preferredPhoneIndicator, extractedContact);
                    }
                    //extractedContact.Phone = srPhone.phoneNumber;RM-1696
                    
                }
    }
    

    public static void handleAccountFlags(List<Student_REST.AccountFlag> accountFlagsList, Contact extractedContact){
        if(accountFlagsList !=null){
            for(Student_REST.AccountFlag accountFlags : accountFlagsList){
                if(accountFlags.positiveFlag == 'N'){
                	handlePostiveFlagN(accountFlags, extractedContact);
                }else{
                    extractedContact.NSI_status__c = FALSE;
                }
                
                
                /*else if(accountFlags.positiveFlag == 'Y' && (accountFlags.flagEndDate != NULL || accountFlags.flagEndDate !='')){
                    if(Date.ValueOf(accountFlags.flagEndDate) != null && Date.ValueOf(accountFlags.flagEndDate) <= System.today()){
                    extractedContact.NSI_status__c = TRUE;
                    }
                }*/
            }
        }
    }

	private static void handlePostiveFlagN(Student_REST.AccountFlag accountFlags, Contact extractedContact){
                    if(accountFlags.flagEndDate.length() != 0){
                        if(Date.valueOf(accountFlags.flagEndDate) >= System.today()){
                            extractedContact.NSI_status__c = TRUE;    
                        }else{
                            extractedContact.NSI_status__c = FALSE;       
                        }
                    }else{
                        extractedContact.NSI_status__c = TRUE;
                    }
	}

   public static Integer calculateListSize(List<Date> effectiveDateAddress)
   {
   	Integer listSizeHome;
        if(effectiveDateAddress.size() > 0){
            effectiveDateAddress.sort();
            listSizeHome = effectiveDateAddress.size() - 1;
        }
        
     return listSizeHome;
   }
   
  public static void addingExtractedAddress(String addressType,List<Date> effectiveDateAddress,Map<Date, Student_REST.Address> mapOfdateandAddress)
  {
  	Integer listSize = calculateListSize(effectiveDateAddress);
  	if(effectiveDateAddress.size() > 0){
            hed__Address__c extractedAddress = new hed__Address__c();
            extractedAddress.id = NULL;
            extractedAddress.hed__Parent_Contact__c = studentAndContactId.get(mapOfdateandAddress.get(effectiveDateAddress[listSize]).studentId);
            extractedAddress.hed__Address_Type__c = addressType;
            extractedAddress.Effective_Date__c = Date.ValueOf(mapOfdateandAddress.get(effectiveDateAddress[listSize]).effectiveDate);
            extractedAddress.hed__MailingCountry__c = mapOfdateandAddress.get(effectiveDateAddress[listSize]).country;
            extractedAddress.hed__MailingCity__c = mapOfdateandAddress.get(effectiveDateAddress[listSize]).city;
            extractedAddress.hed__MailingStreet__c = mapOfdateandAddress.get(effectiveDateAddress[listSize]).addressLine1;
            extractedAddress.hed__MailingStreet2__c = mapOfdateandAddress.get(effectiveDateAddress[listSize]).addressLine2;
            extractedAddress.Mailing_Street_3__c = mapOfdateandAddress.get(effectiveDateAddress[listSize]).addressLine3;
            extractedAddress.Mailing_Street_4__c = mapOfdateandAddress.get(effectiveDateAddress[listSize]).addressLine4;
            extractedAddress.hed__MailingState__c = mapOfdateandAddress.get(effectiveDateAddress[listSize]).state;
            extractedAddress.hed__MailingPostalCode__c = mapOfdateandAddress.get(effectiveDateAddress[listSize]).postalCode;
            returningAddressList.add(extractedAddress);
            
    }
    //return returningAddressList;
  }

	
}