//Wrapper Class for ECB-4504
public class DeliveryModeWrapper {
	
    @AuraEnabled
    public String name {get;set;}
    
    @AuraEnabled
    public Set<String> deliveryModeList {get;set;}
    
}