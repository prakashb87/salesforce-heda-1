/*********************************************************************************
*** @ClassName         : Enrolment_REST
*** @Author            : Shubham Singh 
*** @Requirement       : Getting records from external system through json structure
                         and upsert records into salesforce object(Course Connection).
*** @Created date      : 21/05/2018
*** @Modified by       : Shubham Singh 
*** @modified date     : 19/11/2018
**********************************************************************************/
@RestResource(urlMapping = '/HEDA/v1/Enrolment')
global with sharing class Enrolment_REST {
    //Class Variable Declaration
    public String studentId;
    public String academicCareer;
    public String institution;
    public String term;
    public Integer classNumber;
    public String session;
    public String status;
    public String statusReason;
    public String lastAction;
    public String lastActionReason;
    public String statusDate;
    public String addDate;
    public String dropDate;
    public Integer unitsTaken;
    public Integer progressUnits;
    public Integer billingUnits;
    public String gradingBasis;
    public String gradingBasisDate;
    public String officialGrade;
    public String gradeInput;
    public String gradeDate;
    public String academicProgram;
    public String hecsExemptionStatusCode;
    public String fundingSourceCode;
    public Integer studentCareerNumber;
    
    @HttpPost
    global static Map < String, String > upsertEnrolment() {
        
        //method variables 
        List<String> studentIdEnrol = new List<String>();
        List<String> academicCareerEnrol = new List<String>();
        String institutionEnrol;
        String ccUniqueKey;
        Account institution = new Account();
        List<String> listUniqueKey = new List<String>();
        List<String> termEnrol = new List<String>();
        List<String> classNumberEnrol = new List<String>();
        List<String> academicProgramEnrol = new List<String>();
        List<String> fundingSourceCodeEnrol = new List<String>();
        List<String> studentCareerNumberEnroll = new List<String>();
        List<String> hecsName = new List<String>();
        
        Map < String, String > responsetoSend = new Map < String, String > ();
        Map < string, List < Account >> accountMap = new Map < string, List < Account >> ();
        Map < String, Account > mapOfAccount = new Map < String, Account > ();
        Map<string,Id> mapProgramEnroll = new Map<String,Id>(); 
	    Map<string,Fund_Source__c> mapFundSource = new Map<string,Fund_Source__c>();
		Map < String,ID > mapContact = new Map <string,ID>();
        Map <string,Id> mapCourseConnection = new map<string,Id>();
        Map<string,Id> mapHecs = new Map<string,Id>();
        Map < String,Id > mapAccount = new Map<string,ID>();
		Map < String,Id > mapCourseOffering = new Map<string,ID>();
        Map<string,hed__Course_Enrollment__c> mapOfCourseConnection = new Map<string,hed__Course_Enrollment__c>();

        List < Account > tempListAcc = new List < Account > ();
        List < Enrolment_REST > postString = new List < Enrolment_REST > ();
        List < hed__Course_Enrollment__c > courseConnectionTOUpdate = new List < hed__Course_Enrollment__c > ();
        List < Account > listProgramAcc = new List < Account > ();
		List<hed__Course_Offering__c >listCourseOfferring = new List<hed__Course_Offering__c>();
        List < Account > attachAccount = new List < Account > ();
        List < Fund_Source__c > listFundSource = new List < Fund_Source__c > ();
        List < hed__Course_Enrollment__c > upsertCourseConnection = new List < hed__Course_Enrollment__c > ();
        List < hed__Term__c > listTerm = new List < hed__Term__c > ();
        List < hed__Course_Offering__c > listcourseOffering = new List < hed__Course_Offering__c > ();
        List < hed__Program_Enrollment__c > listprogramEnrollment = new List < hed__Program_Enrollment__c > ();
        List<HECS__c> listHecs = new List<HECS__c>();
        
        List<Account> addAccount = new List<Account>();
        List<String> strngaccount = new List<String>();
        List<Contact> contactstudent = new List<Contact>();
        List<Date> termStartDate = new List<Date>();
        List<Fund_Source__c> fundSourceDesc = new List<Fund_Source__c>();
        
        try {
            
        	Id studentrecodTypeID = Schema.SObjectType.hed__Course_Enrollment__c.getrecordtypeinfosbydevelopername().get('Student').getRecordTypeId();
            
        	// Request/Respond variable
            RestRequest request = RestContext.request;
            
            RestResponse response = RestContext.response;
            
            //No need to add [] in JSON
            //String reqStr = '[' + request.requestBody.toString() + ']';
            String reqStr = request.requestBody.toString();
            
            if(!reqStr.startsWith('[')){
                //no need to add [] in JSON
                reqStr = '[' + request.requestBody.toString() + ']';
            }else{
                reqStr =  request.requestBody.toString();
            }
            
            response.addHeader('Content-Type', 'application/json');
            
            //getting the data from JSON and Deserialize it
            postString = (List < Enrolment_REST > ) System.JSON.deserialize(reqStr, List < Enrolment_REST > .class);
            system.debug('postString--->'+postString);
            //checking for key values and storing them
            if (postString != null) {
                for (Enrolment_REST enrollRest : postString) {
                    if (enrollRest.studentId != null && enrollRest.academicCareer != null && enrollRest.institution != null && enrollRest.term != null && enrollRest.classNumber != null /*&& //no need as this is not a key value now// enrollRest.academicProgram != null*/) {
                        studentIdEnrol.add(enrollRest.studentId);
                        academicCareerEnrol.add(enrollRest.academicCareer);
                        institutionEnrol = enrollRest.institution;
                        termEnrol.add(enrollRest.term);
                        classNumberEnrol.add(String.ValueOf(enrollRest.classNumber));
                        academicProgramEnrol.add(enrollRest.academicProgram);
                        studentCareerNumberEnroll.add(String.ValueOF(enrollRest.studentCareerNumber));
                        fundingSourceCodeEnrol.add(enrollRest.fundingSourceCode);
                        hecsName.add(enrollRest.hecsExemptionStatusCode);
                        ccUniqueKey = enrollRest.academicCareer+''+enrollRest.classNumber+''+enrollRest.term+''+enrollRest.studentId+''+enrollRest.institution;
                        listUniqueKey.add(ccUniqueKey);
                    } else {
                        //if key values are null throw error
                        responsetoSend = throwError();
                        return responsetoSend;
                    }
                }
            }
            //getting student from contact based in studentID
			if (Schema.sObjectType.contact.fields.Student_ID__c.isUpdateable()){
            contactstudent = [SELECT id, Student_ID__c FROM contact WHERE Student_ID__c IN : studentIdEnrol];
			}

            
            if(contactstudent != null && contactstudent.size() > 0){
                for(contact con : contactstudent){
                    if(con.Student_ID__c != null){
                        string str = con.Student_ID__c;
                        mapContact.put(str,con.ID);
                    }
                }
            }

            //if student is null throw error
            //commented as per the new requirement RM-1817
            /*if (contactstudent == null) {
                responsetoSend = throwError();
                return responsetoSend;            }*/
            
            //getting records for the update of course connection on the basis of key values      
			if (Schema.sObjectType.hed__Course_Enrollment__c.fields.Enrolment_Add_Date__c.isUpdateable()){			
			courseConnectionTOUpdate = [SELECT id,Institution__r.Name,hed__Contact__r.Student_ID__c,hed__Course_Enrollment__c.Effective_Date__c,RecordTypeId,hed__Account__c,hed__Contact__c,Institution__c,Class_Number__c,Session__c,Academic_Career__c,hed__Status__c,Status_Reason__c,Last_Enrolment_Action__c,
                                        Last_Action_Reason__c,Enrolment_Add_Date__c,Enrolment_Drop_Date__c,Units_Taken__c,Progress_Units__c,Billing_Units__c,Grading_Basis__c,Grading_Basis_Date__c,Official_Grade__c,
                                        Grade_Input__c,Course_Connection_Unique_Key__c,hed__Program_Enrollment__c,Fund_Source_Description__c,hed__Course_Offering__c,Career__c,Term__c,Fund_Source__c,HECS_Exempt_Status_Code__c,Status_Date__c 
                                        FROM hed__Course_Enrollment__c 
                                        WHERE /*Academic_Career__c IN : academicCareerEnrol AND /*Institution__r.Name IN =: institutionEnrol and Class_Number__c IN : classNumberEnrol AND Term__c IN : termEnrol AND hed__Contact__r.Student_ID__c IN : studentIdEnrol]*/ Course_Connection_Unique_Key__c IN : listUniqueKey];            
			}										
            system.debug(courseConnectionTOUpdate);
            
            
            if(courseConnectionTOUpdate != null && courseConnectionTOUpdate.size() > 0){
                
                for(hed__Course_Enrollment__c ccn : courseConnectionTOUpdate){
                    if(ccn.Academic_Career__c != null && ccn.Class_Number__c != null && ccn.Term__c != null && ccn.hed__Contact__r.Student_ID__c != null && ccn.Institution__c != null){
                    	string str = ccn.Academic_Career__c + '' +ccn.Class_Number__c +''+ccn.Term__c+''+ccn.hed__Contact__r.Student_ID__c+''+ccn.Institution__r.Name;                
                		mapCourseConnection.put(str,ccn.ID);
                	}
                }
                System.debug('mapCourseConnection------------------> '+ mapCourseConnection);
            }       		
            
			//getting instituition record
			//as there is only one institution RMITU
			if (Schema.sObjectType.account.fields.Name.isUpdateable()){
            institution = [SELECT id, Name FROM account WHERE name =: institutionEnrol LIMIT 1];
			}
            
            if(hecsName != null && hecsName.size() > 0){
			if (Schema.sObjectType.HECS__c.fields.Name.isUpdateable()){
               listHecs = [SELECT id,name FROM HECS__c WHERE name IN : hecsName ]; 
            }
            }
                
			
			
            if(listHecs != null && listHecs.size() > 0){
                for(HECS__c hcs  : listHecs){
                    mapHecs.put(hcs.Name,hcs.ID);
                }
            }
            
            //getting program from Account
			if (Schema.sObjectType.Account.fields.Academic_Career__c.isUpdateable()){
            listProgramAcc = [SELECT id, AccountNumber,Academic_Institution__r.Name, Academic_Institution__c, Academic_Career__c,Effective_Date__c FROM Account WHERE AccountNumber =: academicProgramEnrol AND Academic_Institution__r.Name =: institutionEnrol /*and Academic_Career__c =: academicCareerEnrol commented as per the sheet*/  ORDER BY Effective_Date__c DESC];
			}

            
            if(listProgramAcc != null && listProgramAcc.size() > 0){
                for(Account acc : listProgramAcc){
                    if(acc.AccountNumber != null && acc.Academic_Institution__r.Name != null){
                        string str = acc.AccountNumber+''+acc.Academic_Institution__r.Name/*+''+acc.Academic_Career__c*/;
                        mapAccount.put(str,acc.ID);
                    }
                }
            }
			if (Schema.sObjectType.hed__Course_Offering__c.fields.name.isUpdateable()){
			listCourseOfferring  = [SELECT id,name,hed__Term__r.Term_Code__r.Term_Code__c FROM hed__Course_Offering__c  WHERE name IN : classNumberEnrol  AND hed__Term__r.Term_Code__r.Term_Code__c IN : termEnrol];
			}
            
             if(listCourseOfferring != null && listCourseOfferring.size() > 0){
                for(hed__Course_Offering__c hCO : listCourseOfferring){
                    if(hCO.name != null && hCO.hed__Term__r.Term_Code__r.Term_Code__c != null){
                        string str = hCO.name+''+hCO.hed__Term__r.Term_Code__r.Term_Code__c/*+''+acc.Academic_Career__c*/;
                        mapCourseOffering.put(str,hCO.ID);
                    }
                }
            }
            //commented as there will be only one record 
        	//traverse account to get the single or multiple accounts related to the academic career,institution and account number
            //Storing in accountMap
            /*if (listProgramAcc != null && listProgramAcc.size() > 0) {
                for (Account act: listProgramAcc) {
                    // need to check about the field mapping then we can procees furthure
                    if (act.Academic_Career__c != null && act.AccountNumber != null)
                        strngaccount = act.Academic_Career__c + '' + act.AccountNumber;
                    if(strngaccount != null)
                        strngaccount.toUpperCase();
                    if (accountMap.containsKey(strngaccount)) {
                        tempListAcc = accountMap.get(strngaccount);
                        tempListAcc.add(act);
                        accountMap.put(strngaccount, tempListAcc);
                    } else {
                        tempListAcc = new List < Account > ();
                        tempListAcc.add(act);
                        accountMap.put(strngaccount, tempListAcc);
                    }
                }
            }*/
            //SYSTEM.debug('accountMap '+accountMap);
            //as per the discussion there will be one record which will match this criteria for below objects
            //Getting term start date
            //listTerm = [select id, hed__Start_Date__c from hed__Term__c where hed__Account__r.Name =: institutionEnrol and Term_Code__r.Term_Code__c  IN : termEnrol and Academic_Career__c IN : academicCareerEnrol];
            //SYSTEM.debug('listTerm '+listTerm);
            //commented as the field is not present in mapping sheet
            //Get course offering
            /*listcourseOffering = [SELECT id,hed__Term__r.Term_Code__c, hed__Course__c,Class_Number__c FROM hed__Course_Offering__c WHERE Class_Number__c IN : classNumberEnrol AND hed__Term__r.Term_Code__c IN : termEnrol LIMIT 1];
            map<string,Id> mapCourseOffering = new map<string,Id>();
			
			for(hed__Course_Offering__c hedCourse : listcourseOffering){
                if(hedCourse.Class_Number__c != null && hedCourse.hed__Term__r.Term_Code__c != null){
					string str = hedCourse.Class_Number__c+ '' + hedCourse.hed__Term__r.Term_Code__c;
					mapCourseOffering.put(str,hedCourse.ID);
            	}
			}*/
            
            //Get programEnrollment and order by effective date and effective sequence
			 if (Schema.sObjectType.hed__Program_Enrollment__c.fields.Academic_Program__c.isUpdateable()){
            listprogramEnrollment = [SELECT Id,Institution__r.Name,hed__Contact__r.Student_ID__c,Academic_Program__c,Career__c,Student_Career_Number__c FROM hed__Program_Enrollment__c WHERE Institution__r.Name =: institutionEnrol AND hed__Contact__r.Student_ID__c IN : studentIdEnrol AND Academic_Program__c IN : academicProgramEnrol AND Career__c IN : academicCareerEnrol AND Student_Career_Number__c IN : studentCareerNumberEnroll ORDER BY Effective_Date__c, Effective_Sequence__c DESC];
			}

            if(listprogramEnrollment != null && listprogramEnrollment.size() > 0){
                for(hed__Program_Enrollment__c hedProgEnroll : listprogramEnrollment){
                    if(hedProgEnroll.Institution__r.Name != null && hedProgEnroll.hed__Contact__r.Student_ID__c != null && hedProgEnroll.Academic_Program__c != null && hedProgEnroll.Career__c != null && hedProgEnroll.Student_Career_Number__c != null){
                        string str = hedProgEnroll.Institution__r.Name+''+hedProgEnroll.hed__Contact__r.Student_ID__c+''+hedProgEnroll.Academic_Program__c+''+hedProgEnroll.Career__c+''+hedProgEnroll.Student_Career_Number__c;
                        mapProgramEnroll.put(str,hedProgEnroll.ID);
                    }
                }
            }
            //throw error if course Offering or course is not there
            //commented as per the new requirement RM-1817
            /*if (listcourseOffering == null || listcourseOffering.size() == 0 || listcourseOffering[0].hed__Course__c == null) {
            	responsetoSend = throwError();
            	return responsetoSend;
            }*/
            
            //to match the term start date with Program and fund source
            /*if (listTerm != null && listTerm.size() > 0 ){
				for(hed__Term__c hedTerm : listTerm){
					
				}hed__Start_Date__c
				termStartDate = listTerm[0].hed__Start_Date__c;
		    }*/
            //SYSTEM.debug('termStartDate '+termStartDate);
            //Get fundsource oder by effective date desc
			if (Schema.sObjectType.Fund_Source__c.fields.Description__c.isUpdateable()){
            listFundSource = [SELECT id,Name,Description__c, Effective_Date__c FROM Fund_Source__c WHERE Name IN : fundingSourceCodeEnrol ORDER BY Effective_Date__c Desc];
			}
            
            if(listFundSource != null && listFundSource.size() > 0){
                for(Fund_Source__c fund : listFundSource){
                    mapFundSource.put(fund.Name,fund);			
                }
            }
			
            //commented as per new requirement RM-1817
            //get the fund source description by matching it with term start date
            /*for (Fund_Source__c fs: listFundSource) {
                if(fs.Effective_Date__c != null && termStartDate != null){
                	if (fs.Effective_Date__c <= termStartDate) {
                	//if match break the loop
                	fundSourceDesc = fs;
                	break;
                	}
                }
            }*/
            
            /*if (!accountMap.isEmpty()) {
                for (Enrolment_REST enrlRest: postString) {
                    //need to add upper and lower case consideration
                    String strprogram = enrlRest.academicCareer + '' + enrlRest.academicProgram;
                    
                    strprogram.toUpperCase();
                    
                    if ((!accountMap.isEmpty()) && accountMap.get(strprogram) != null)
                        attachAccount = accountMap.get(strprogram);
                    
                    if (attachAccount != null && attachAccount.size() == 1) {
                        addAccount = new Account();
                        addAccount = attachAccount[0];
                    } else if (attachAccount != null && attachAccount.size() > 1) {
                        for (Account acnt: attachAccount) {
                            //comparing account based on the effective date and
                            if(acnt.Effective_Date__c != null && termStartDate != null){
                                if (acnt.Effective_Date__c <= termStartDate) {
                                    //if one of them matches the criteria then loop will break
                                    addAccount = new Account();
                                    addAccount = acnt;
                                    break;
                                }
                            }
                        }
                    }
                    SYSTEM.debug('addAccount '+addAccount.Effective_Date__c);
                    SYSTEM.debug('termStartDate '+termStartDate);
                    if (addAccount != null && enrlRest.studentId != null) {
                        //to store account based on student id and check if single account exist in attachAccount then it should be less then term start date
                        if(addAccount.Effective_Date__c <= termStartDate)
                            mapOfAccount.put(enrlRest.studentId, addAccount);
                    }
                }
            }*/

        	//create or update course connection
            for (Enrolment_REST createUpdateEnroll : postString) {
                hed__Course_Enrollment__c newCourseConnection = new hed__Course_Enrollment__c();
                string courseOffKey = createUpdateEnroll.classNumber+''+createUpdateEnroll.term;
                string key = createUpdateEnroll.academicCareer+''+createUpdateEnroll.classNumber+''+createUpdateEnroll.term+''+createUpdateEnroll.studentId+''+createUpdateEnroll.institution;
                string accKey = createUpdateEnroll.academicProgram+''+''+createUpdateEnroll.institution/*+''+createUpdateEnroll.academicCareer*/;
                string programEnrollKey = createUpdateEnroll.institution+''+createUpdateEnroll.studentId+''+createUpdateEnroll.academicProgram+''+createUpdateEnroll.academicCareer+''+createUpdateEnroll.studentCareerNumber ;
                newCourseConnection.Course_Connection_Unique_Key__c = key;
                newCourseConnection.RecordTypeId = studentrecodTypeID;
                newCourseConnection.hed__Account__c = !mapAccount.isEmpty() && mapAccount.get(accKey) != null ? mapAccount.get(accKey) : null;
                newCourseConnection.hed__Contact__c = !mapContact.isEmpty() && mapContact.get(createUpdateEnroll.studentId) != null ? mapContact.get(createUpdateEnroll.studentId) : null;
                newCourseConnection.Institution__c = institution != null ? institution.ID : null;
                newCourseConnection.Class_Number__c = String.ValueOf(createUpdateEnroll.classNumber);
                newCourseConnection.Session__c = createUpdateEnroll.session;
                newCourseConnection.Academic_Career__c = createUpdateEnroll.academicCareer;
                if (createUpdateEnroll.status != null){
                    newCourseConnection.hed__Status__c = createUpdateEnroll.status;
                    newCourseConnection.Enrolment_Status__c = createUpdateEnroll.status;
                }
                newCourseConnection.Status_Reason__c = createUpdateEnroll.statusReason;
                newCourseConnection.Last_Enrolment_Action__c = createUpdateEnroll.lastAction;
                newCourseConnection.Last_Action_Reason__c = createUpdateEnroll.lastActionReason;
                if (createUpdateEnroll.addDate != null && createUpdateEnroll.addDate.length() > 0){
                    newCourseConnection.Enrolment_Add_Date__c = Date.ValueOf(createUpdateEnroll.addDate);
                }    
                if (createUpdateEnroll.dropDate != null && createUpdateEnroll.dropDate.length() > 0){
                    newCourseConnection.Enrolment_Drop_Date__c = Date.ValueOf(createUpdateEnroll.dropDate);
                }
                newCourseConnection.Units_Taken__c = String.ValueOf(createUpdateEnroll.unitsTaken);
                newCourseConnection.Progress_Units__c = createUpdateEnroll.progressUnits != null ? createUpdateEnroll.progressUnits : null;
                newCourseConnection.Billing_Units__c = createUpdateEnroll.billingUnits != null ? createUpdateEnroll.billingUnits : null;
                newCourseConnection.Grading_Basis__c = createUpdateEnroll.gradingBasis;
                if (createUpdateEnroll.gradingBasisDate != null && createUpdateEnroll.gradingBasisDate.length() > 0){
                    newCourseConnection.Grading_Basis_Date__c = Date.ValueOf(createUpdateEnroll.gradingBasisDate);
                }
				newCourseConnection.hed__Course_Offering__c = !mapCourseOffering.isEmpty() && mapCourseOffering.get(courseOffKey) != null ? mapCourseOffering.get(courseOffKey) : null;
                newCourseConnection.Official_Grade__c = createUpdateEnroll.officialGrade;
                newCourseConnection.Grade_Input__c = createUpdateEnroll.gradeInput;
                if (createUpdateEnroll.gradeDate != null && createUpdateEnroll.gradeDate.length() > 0){
                   newCourseConnection.Grade_Date__c = Date.ValueOf(createUpdateEnroll.gradeDate); 
                }
                newCourseConnection.hed__Program_Enrollment__c = mapProgramEnroll != null && mapProgramEnroll.size() > 0 && mapProgramEnroll.get(programEnrollKey) != null ? mapProgramEnroll.get(programEnrollKey) : null;
                newCourseConnection.Fund_Source_Description__c = mapFundSource != null && mapFundSource.size() > 0 && mapFundSource.get(createUpdateEnroll.fundingSourceCode) != null ? mapFundSource.get(createUpdateEnroll.fundingSourceCode).Description__c : '';
                //newCourseConnection.hed__Course_Offering__c = mapCourseOffering != null && mapCourseOffering.size() > 0 && mapCourseOffering.get(courseOffKey) != null ? mapCourseOffering.get(courseOffKey) : null;
                newCourseConnection.Career__c = String.ValueOf(createUpdateEnroll.studentCareerNumber);
                newCourseConnection.Term__c = createUpdateEnroll.term;
                //RM-1817
                newCourseConnection.Fund_Source__c = mapFundSource != null && mapFundSource.size() > 0 && mapFundSource.get(createUpdateEnroll.fundingSourceCode) != null ? mapFundSource.get(createUpdateEnroll.fundingSourceCode).Id : null;
                //RM-1817
                newCourseConnection.HECS_Exempt_Status_Code__c = mapHecs != null && mapHecs.size() > 0 && mapHecs.get(createUpdateEnroll.hecsExemptionStatusCode) != null ? mapHecs.get(createUpdateEnroll.hecsExemptionStatusCode) : null; 
                
                if (createUpdateEnroll.statusDate != null && createUpdateEnroll.statusDate.length() > 0){
                    newCourseConnection.Status_Date__c = Date.ValueOf(createUpdateEnroll.statusDate);
                }
                    
              
				//As there will be only one record to update
                if (mapCourseConnection != null && mapCourseConnection.Size() > 0) {
                    //if(courseConnectionTOUpdate[0].Effective_date__c <= newCourseConnection.Effective_date__c)
                    	newCourseConnection.Id = mapCourseConnection.get(key) != null ? mapCourseConnection.get(key) : null;
                    //else
                    //newCourseConnection = null;
                   
                }
                if(newCourseConnection !=null){
                    upsertCourseConnection.add(newCourseConnection);
                }
            }
            
            if(upsertCourseConnection != null && upsertCourseConnection.size() > 0){
                for(hed__Course_Enrollment__c hecCourseEnroll: upsertCourseConnection){
                    string uniquekey = hecCourseEnroll.Academic_Career__c+''+hecCourseEnroll.Class_Number__c+''+hecCourseEnroll.Term__c+''+hecCourseEnroll.hed__Contact__c;
                    if(uniquekey != null){
                    	mapOfCourseConnection.put(uniquekey,hecCourseEnroll);
                    }
                }
            }
            system.debug('mapOfCourseConnection '+mapOfCourseConnection);
            //insert/update the course connection 
            if (mapOfCourseConnection != null && mapOfCourseConnection.size() > 0) {
                //Database.upsert(mapOfCourseConnection.values(),false);
                //
                upsert mapOfCourseConnection.values() Course_Connection_Unique_Key__c;
            }
            responsetoSend.put('message', 'OK');
            responsetoSend.put('status', '200');
            return responsetoSend;
        } catch (Exception excpt) {
            //If exception occurs then return this message to IPASS
            responsetoSend = throwError();
            return responsetoSend;
        }
    }
    
    public static Map < String, String > throwError() {
        //to store the error response
        Map < String, String > errorResponse = new Map < String, String > ();
        errorResponse.put('message', 'Oops! Enrolment was not well defined, amigo.');
        errorResponse.put('status', '404');
        //If error occurs then return this message to IPASS
        return errorResponse;
    }
}