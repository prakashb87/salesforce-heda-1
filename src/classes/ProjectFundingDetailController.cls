/*************************************************************************************
Purpose: View Project Funding Detail- expenses view
History:
Created by Ankit Bhagat	 on 10/10/2018 for RPORW-135 
Modified By Subhajit for comment addition and Budget code added 
*************************************************************************************/
public with Sharing class ProjectFundingDetailController {
    
      
    /*****************************************************************************************
     Global variable Declaration
	//****************************************************************************************/
    public static Boolean isForceExceptionRequired=false;//added by Ali
    
    /************************************************************************************
    // JIRA No      :  RPORW-135          
    // SPRINT       :  SPRINT-5
    // Purpose      :  getting Project funding Expenses Details
    // Parameters   :  String		   	
    // Developer    :  Ankit
    // Created Date :  10/10/2018                 
    //***********************************************************************************/
    @AuraEnabled    
    public static String getProjectFundingExpenseDetails(String projectId, String selectedYear){
        ProjectFundingWrapper.ProjectFundingExpensesTable projectFundingExpensesTable = new ProjectFundingWrapper.ProjectFundingExpensesTable();
        
        try{
            
            projectFundingExpensesTable  = ProjectFundingDetailHelper.getProjectFundingTable(projectId,selectedYear);
            system.debug('Expense Details==>'+JSON.serializePretty(projectFundingExpensesTable));
           
            ResearcherExceptionHandlingUtil.createForceExceptionForTestCoverage(isForceExceptionRequired);
            
        }
        catch(Exception ex)
        {   
            //Exception handling Error Log captured :: Starts
            ResearcherExceptionHandlingUtil.addExceptionLog(ex,ResearcherExceptionHandlingUtil.getParamMap()); //added by Ali
            //Exception handling Error Log captured :: Ends
            system.debug('Exception Occured =>'+ex.getMessage());            
        }
        
        return JSON.serialize(projectFundingExpensesTable);
     }
    
    /************************************************************************************
    // Purpose      : To check current login user is community user or salesforce user
                   
    //***********************************************************************************/ 
    
	@AuraEnabled     
    public static Boolean checkInternalDualAccessUser(){
            Boolean isInternalDualAccessUser = ApexWithoutSharingUtils.checkInternalDualAccessUser();
            return isInternalDualAccessUser;
    } 
 
    
    /************************************************************************************
    // JIRA No      :  RPORW-282         
    // SPRINT       :  SPRINT-5
    // Purpose      :  View Project Funding Detail- budget view
    // Parameters   :  String projectId		   	
    // Developer    :  Subhajit
    // Created Date :  10/12/2018                 
    //***********************************************************************************/
    @AuraEnabled    
    public static String getProjectFundingBudgetDetails(String projectId){
        
        list<ProjectFundingWrapper.ProjectFundingBudgetDetails> projectFundingBudgetDetails = new list<ProjectFundingWrapper.ProjectFundingBudgetDetails>();
        
        try
        {
            projectFundingBudgetDetails = ProjectFundingDetailHelper.getProjectFundingBudgetDetails(projectId);
            system.debug('Budget Details==>'+JSON.serialize(projectFundingBudgetDetails));
           
            ResearcherExceptionHandlingUtil.createForceExceptionForTestCoverage(isForceExceptionRequired);
        }
        catch(Exception ex)
        {            
            //Exception handling Error Log captured :: Starts
           	ResearcherExceptionHandlingUtil.addExceptionLog(ex,ResearcherExceptionHandlingUtil.getParamMap()); //added by Ali
            //Exception handling Error Log captured :: Ends
            system.debug('Exception Occured =>'+ex.getMessage());            
        }
        return JSON.serialize(projectFundingBudgetDetails);
     }
    
    /************************************************************************************
    // JIRA No      :  RPORW-135           
    // SPRINT       :  SPRINT-5
    // Purpose      :  Get only the project in which my role is CI from my project list view 
    // Parameters   :  void	   	
    // Developer    :  Raj
    // Created Date :  10/10/2018                 
    //***********************************************************************************/
    @AuraEnabled    
    public static ProjectDetailsWrapper.ResearchProjectListView getMyResearchProjectListsForFunding(){
        
        ProjectDetailsWrapper.ResearchProjectListView myResearchProjectListView = new  ProjectDetailsWrapper.ResearchProjectListView(); 
       
        try
        {
            // Pass the CI role to get all funding related records.
            // Assumption all the CI role researcher will be able to access the funding information.
            myResearchProjectListView = ResearchProjectHelper.getMyResearchProjectListViewByRole(userinfo.getuserId(),label.CI);
           
            ResearcherExceptionHandlingUtil.createForceExceptionForTestCoverage(isForceExceptionRequired);          
            system.debug('****myResearchProjectListView ==>'+JSON.serializePretty(myResearchProjectListView));
        }
        catch(Exception ex)
        {   
            //Exception handling Error Log captured :: Starts
            ResearcherExceptionHandlingUtil.addExceptionLog(ex,ResearcherExceptionHandlingUtil.getParamMap()); //added by Ali
            //Exception handling Error Log captured :: Ends
            system.debug('Exception Occured =>'+ex.getMessage());            
        }
        return myResearchProjectListView;
    }
    
    /************************************************************************************
    // JIRA No      :  RPORW-135            
    // SPRINT       :  SPRINT-5
    // Purpose      :  Get only the projectDetail view  in which my role is CI from my project list view 
    // Parameters   :  void	   	
    // Developer    :  Raj
    // Created Date :  10/10/2018                 
    //***********************************************************************************/
	@AuraEnabled    
    public static String getProjectDetailViewByProjectId(Id projectId){
        
        System.debug('@@@Inside the method with ProjectId'+projectId);
        ProjectDetailsWrapper.ResearchProjectDetailData projectDetailsRecords = 
	        new ProjectDetailsWrapper.ResearchProjectDetailData();
       
        try
        {
            projectDetailsRecords = ResearchProjectHelper.getProjectDetailsHelper(projectId); 
            
            ResearcherExceptionHandlingUtil.createForceExceptionForTestCoverage(isForceExceptionRequired);
        }
		catch(Exception ex)
        {            
            //Exception handling Error Log captured :: Starts
            ResearcherExceptionHandlingUtil.addExceptionLog(ex,ResearcherExceptionHandlingUtil.getParamMap()); //added by Ali
            //Exception handling Error Log captured :: Ends
            system.debug('Exception Occured =>'+ex.getMessage());          
        }	
        return JSON.serialize(projectDetailsRecords); 
      
     }
}