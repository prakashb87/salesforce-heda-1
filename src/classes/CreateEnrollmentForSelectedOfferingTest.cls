/*
  ***************************************************************************************************************
  * @author       :Resmi Ramakrishnan
  * @date         :09/10/2018 
  * @description  :Test Class For CreateEnrollmentForSectectedOffering class 
  ***************************************************************************************************************/
@isTest
public  class CreateEnrollmentForSelectedOfferingTest
 {
	static testMethod void testSelectedOfferingEnrollment()
	{
		Test.startTest();
        TestDataFactoryUtil.dummycustomsetting();
        TestDataFactoryUtil.testDataForSelectedOffering();
        
        csord__Service__c selectServiceObj = [SELECT id,name from csord__Service__c LIMIT 1];
        hed__Program_Enrollment__c pgmEnrollId = [SELECT id from hed__Program_Enrollment__c LIMIT 1];
        hed__Course_Offering__c courseOffObj = [SELECT id,name from hed__Course_Offering__c where name ='test course offering 2'];

        if( selectServiceObj != null && pgmEnrollId != null && courseOffObj != null )
        {
        	System.debug('Objects are  not empty');
        	CreateEnrollmentForSectectedOffering.createCourseConnectionForSelectedOffering(selectServiceObj.id, courseOffObj.id, pgmEnrollId.id );
            
            hed__Course_Enrollment__c newCourseCC = [SELECT id from hed__Course_Enrollment__c where hed__Course_Offering__c =:courseOffObj.id ];
            System.assert(newCourseCC!=null);
        }
	}

	
}