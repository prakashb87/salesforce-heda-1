/* 
   @Author <Shalu Chaudhary>
   @name <CourseEnrollmentHandler>
   @CreateDate <29/02/2019>
   @Description 
   @Version <1.0>
   */
@SuppressWarnings('PMD.ApexCRUDViolation')
public without sharing class CourseEnrollmentHandler {
	
	public static List<Course_Connection_Life_Cycle__c> getOpenEdxUnEnrollCClifeCycleList(String userId,Set<String> courseIds)
    {
            validateUserCCLifeCycleReadAccess(); 
            User usr = [Select Id, name, ContactId from user WHERE FederationIdentifier =: userId LIMIT 1]; 
            List<Course_Connection_Life_Cycle__c> courseConnectionLifeCycleList = [SELECT Id,
                                                                              Name,Status__c,Course_Connection__c,Course_Connection__r.hed__Course_Offering__r.Name
                                                                              FROM Course_Connection_Life_Cycle__c 
                                                                              WHERE Course_Connection__r.hed__Course_Offering__r.Name IN :courseIds  
                                                                              AND Course_Connection__r.hed__Contact__c =:usr.ContactId AND 
                                                                              (Course_Connection__r.hed__Status__c='Opt Out' OR Course_Connection__r.hed__Status__c='Transferred' OR Course_Connection__r.hed__Status__c='Cancelled' )AND Stage__c ='Dropped in EDX'];                     
           return courseConnectionLifeCycleList; 
    }
        
    public static List<Course_Connection_Life_Cycle__c> getOpenEdxEnrollCClifeCycleList(String userId,Set<String> courseIds)
    {
            validateUserCCLifeCycleReadAccess();
            User usr = [Select Id, name, ContactId from user WHERE FederationIdentifier =: userId LIMIT 1];             
            List<Course_Connection_Life_Cycle__c> courseConnectionLifeCycleList = [SELECT Id,
                                                                                  Name,Status__c,Course_Connection__c,Course_Connection__r.hed__Course_Offering__r.Name
                                                                                  FROM Course_Connection_Life_Cycle__c 
                                                                                  WHERE Course_Connection__r.hed__Course_Offering__r.Name IN :courseIds  
                                                                                  AND Course_Connection__r.hed__Contact__c =:usr.ContactId AND 
                                                                                  Course_Connection__r.hed__Status__c='Current' AND Stage__c ='Post to Edx' ];
           return courseConnectionLifeCycleList;
    }
    // Common methods for Enrolled and UnEnrolled
    public static void getOfferSetsAndUpdateCClifeCycle(OpenEdxResponseWrapper jsonResponse,List<Course_Connection_Life_Cycle__c> courseConnectionLifeCycleList)
    {
    	Set<String> successCourseOfferSet = new Set<String>();
        Set<String> errorCourseOfferSet = new Set<String>();
        for(OpenEdxResponseWrapper.Response res : jsonResponse.payload.response)
        {
            getSuccessErrOfferSets(res,successCourseOfferSet,errorCourseOfferSet);
        }   
        if(!courseConnectionLifeCycleList.isEmpty())
        {
            for(Course_Connection_Life_Cycle__c lifecycle : courseConnectionLifeCycleList)
            {
                updateLifeCycleStatus(lifecycle,successCourseOfferSet,errorCourseOfferSet);
            }
            validateCCLifeCycleUpdateAccess();
            update courseConnectionLifeCycleList;
        }
    }
    public static HttpResponse setHttpReqResAndInsertRMErrorLogger(String requestJSONString,string methodName)
    {    	
    	    HttpResponse response = new HttpResponse();
            response = setHTTPRequestResponse(requestJSONString,methodName);
            
            //Creating RMErrorLog incase iPaaS returns an error
            if(response.getStatusCode() != 200)
            {
            	insertRMErrorLogger();              
            }
            return response;
    }
    public static List<CourseEnrollmentHelper.enrollmentToOpenEdx_Courses> getcourseWrapperList(Set<String> courseIds)
    {
    	List<CourseEnrollmentHelper.enrollmentToOpenEdx_Courses> courseWrapperList = new List<CourseEnrollmentHelper.enrollmentToOpenEdx_Courses>();
    	for (String courseId : courseIds) {             
            CourseEnrollmentHelper.enrollmentToOpenEdx_Courses enrollOpenEdx = new CourseEnrollmentHelper.enrollmentToOpenEdx_Courses(courseId);          
            courseWrapperList.add(enrollOpenEdx);
        }
        return courseWrapperList;
    }
    public static HttpResponse setHTTPRequestResponse(String requestJSONString, string methodName)
    {
    	HttpResponse response = new HttpResponse();
    	String endpoint = ConfigDataMapController.getCustomSettingValue('OpenEdxIpassAPI'); 
        HttpRequest req = new HttpRequest();
        req.setEndpoint(endpoint);
        req.setHeader('client_id', ConfigDataMapController.getCustomSettingValue('OpenEdxIpassClientID')); 
        req.setHeader('client_secret',ConfigDataMapController.getCustomSettingValue('OpenEdxIpassCientSecret')); 
        req.setHeader('Content-Type','application/json');  
        if(methodName=='iPassOpenEdxApiEnroll')  
        {       
           req.setMethod('POST');    
           req.setTimeout(120000);
        }
        else if(methodName=='iPassOpenEdxApiUnEnrollCall')
        {
        	req.setMethod('DELETE');
        }           
        req.setbody(requestJSONString);
        Http http = new Http();
        response = http.send(req);          
        System.debug('response---------------'+response.getbody());
        return response; 
    }
    public static void insertRMErrorLogger()
    {
        validateRMErrorCreateAccess();
        RMErrLog__c errlog = new RMErrLog__c();
        errlog.Error_Cause__c = ConfigDataMapController.getCustomSettingValue('IpassErrorCause'); 
        errlog.Error_Message__c = ConfigDataMapController.getCustomSettingValue('OpenEdxCall'); 
        errlog.Error_Type__c = ConfigDataMapController.getCustomSettingValue('IpassErrorType');
        errlog.Error_Payload__c = ConfigDataMapController.getCustomSettingValue('IpassErrorPayLoad');
        errlog.Class_Name__c = 'csapi_BasketExtension';
        errlog.Method_Name__c = 'iPassOpenEdxApiCall';              
        system.debug('errlog----------------'+errlog);
        Insert errlog; 
    }
    public static void insertResponseLogObject(Integration_Logs__c responseLogObjectToInsert)
    {   
    	if(responseLogObjectToInsert != null)
    	{
        	validateIntegrationLogsCreateAccess();
            insert responseLogObjectToInsert;  
    	}      
    }
    public static void getSuccessErrOfferSets(OpenEdxResponseWrapper.Response res,Set<String> successCourseOfferSet,Set<String> errorCourseOfferSet)
    {
        if(res.result == 'Success')
        {
           successCourseOfferSet.add(res.course_details.course_id);
        }
        else if(res.result == 'Error')
        {
           errorCourseOfferSet.add(res.course_details.course_id);
        }    
    }
    public static void updateLifeCycleStatus(Course_Connection_Life_Cycle__c lifecycle,Set<String> successCourseOfferSet,Set<String> errorCourseOfferSet)
    {
        if(successCourseOfferSet.contains(lifecycle.Course_Connection__r.hed__Course_Offering__r.Name))
        {
            lifecycle.Status__c = 'Success';
        }
        else if(errorCourseOfferSet.contains(lifecycle.Course_Connection__r.hed__Course_Offering__r.Name))
        {
            lifecycle.Status__c = 'Error';
        }    
    }
    public static void validateUserCCLifeCycleReadAccess()
    {
            if(!Schema.sObjectType.Course_Connection_Life_Cycle__c.isAccessible())
            {
               throw new DMLException();           		
            }
    }
    public static void validateCCLifeCycleUpdateAccess()
    {
        if(!Schema.sObjectType.Course_Connection_Life_Cycle__c.isUpdateable())
        {
           throw new DMLException();           		
        }
    }
    public static void validateRMErrorCreateAccess()
    {
    	if(!Schema.sObjectType.RMErrLog__c.isCreateable())
    	{
    		throw new DMLException();            		
    	}
    }
    public static void validateIntegrationLogsCreateAccess()
    {
    	if(!Schema.sObjectType.Integration_Logs__c.isCreateable())
        {
              throw new DMLException();
        } 
    }
    
}