@isTest
private class HEDAProgram2CSBatchSchedulerTest {
	
	public static String cronExp = '0 0 0 15 3 ? 2022';

	@testSetup static void setup() {
		List<Account> accountsToCreate = new List<Account>();
		Account account21CC = New Account(name = '21CC');
		accountsToCreate.add(account21CC);

		Account accountRMITOnline = New Account(name = 'RMIT Online');
		accountsToCreate.add(accountRMITOnline);

		insert accountsToCreate;

		List<Config_Data_Map__c> configurations = new List<Config_Data_Map__c>();
        Config_Data_Map__c account21CCCustomSetting = new Config_Data_Map__c();
        account21CCCustomSetting.Name = 'AccountID_21CC';
        account21CCCustomSetting.Config_Value__c = account21CC.id; 
        configurations.add(account21CCCustomSetting);

        Config_Data_Map__c accountRMITOnlineCustomSetting = new Config_Data_Map__c();
        accountRMITOnlineCustomSetting.Name = 'AccountID_RMITOnline';
        accountRMITOnlineCustomSetting.Config_Value__c = accountRMITOnline.id; 
        configurations.add(accountRMITOnlineCustomSetting);

		insert configurations;

		List<cspmb__Add_On_Price_Item__c> priceItems = new List<cspmb__Add_On_Price_Item__c>();
        cspmb__Add_On_Price_Item__c addOn1 = new cspmb__Add_On_Price_Item__c();
		addOn1.name = 'All';
		priceItems.add(addOn1);

		cspmb__Add_On_Price_Item__c addOn2 = new cspmb__Add_On_Price_Item__c();
		addOn2.name = 'Assessment Only';
		priceItems.add(addOn2);


		cspmb__Add_On_Price_Item__c addOn3 = new cspmb__Add_On_Price_Item__c();
		addOn3.name = 'Content Only';
		priceItems.add(addOn3);

		insert priceItems;
    }

	@isTest static void test() {

		// Create test data
		Test.startTest();
		// Schedule the test job

		
		HEDAProgram2CSBatchScheduler job= new HEDAProgram2CSBatchScheduler();
		String jobId = System.schedule('ScheduledApexTest',
		                               cronExp,
		                               job);
		CronTrigger ct = [SELECT Id, CronExpression, TimesTriggered, NextFireTime FROM CronTrigger WHERE id = :jobId];
        System.assertEquals(cronExp, ct.CronExpression);
        System.assertEquals(0, ct.TimesTriggered);
		//Stopping the test will run the job synchronously
		Test.stopTest();	
	}
	
}