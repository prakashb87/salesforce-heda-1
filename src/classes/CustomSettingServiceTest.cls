/**
 * Created by marcelowork on 2019-05-06.
 */

@IsTest
public with sharing class CustomSettingServiceTest {

    @isTest
    public static void testExists(){
        insert new SystemConfiguration__c();
        Test.startTest();
        String str = CustomSettingsService.getActivatorExternalId();
        Test.stopTest();
        System.assertnotEquals(null,str);
    }

    @isTest
    public static void testnotExists(){

        Test.startTest();
        String str = CustomSettingsService.getActivatorExternalId();
        Test.stopTest();
        System.assertnotEquals(null,str);
    }

}