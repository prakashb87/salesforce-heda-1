/*****************************************************************************************************
 *** @Class             : EmbeddedMicroCreds
 *** @Author            : Avinash Machineni
 *** @Requirement       : Transforming the embedded micro credentials data from HEDA into PriceItem table
 *** @Created date      : 25/06/2018
 *** @JIRA ID           : ECB-3731
 *****************************************************************************************************/
/*****************************************************************************************************
 *** @About Class
 *** This class is used to transform the embedded micro credentials data from HEDA
 *****************************************************************************************************/
public without sharing class EmbeddedMicroCreds {
  public static void createPriceItemRec() { 

    List<cspmb__Price_Item__c> csPriceItems = new  List<cspmb__Price_Item__c>();
    List<Course_Offering_Relationship__c> coRelationships = new List<Course_Offering_Relationship__c>(
      [Select Course_Offering__c,
       Course_Offering__r.hed__course__c,
       Course_Offering__r.hed__course__r.name,
       Course_Offering__r.Status__c,
       Related_Course_Offering__c,
       Related_Course_Offering__r.hed__course__r.hed__Course_ID__c,
       Related_Course_Offering__r.hed__course__r.name,
       Status__c,
       Enrolment_Trigger_Date__c
       FROM Course_Offering_Relationship__c
       WHERE RecordType.DeveloperName = : ConfigDataMapController.getCustomSettingValue('EmbeddedCourseRecordType') AND
                                        Status__c = : ConfigDataMapController.getCustomSettingValue('EmbeddedCourseStatus')]);

    System.debug('EmbeddedMicroCreds.createPriceItemRec coRelationships.size ' + coRelationships.size());

    if (coRelationships.size() > 0 ) {
      for (Course_Offering_Relationship__c coRelationship : coRelationships) {

        cspmb__Price_Item__c priceItem = New cspmb__Price_Item__c();
        priceItem.Name = coRelationship.Course_Offering__r.hed__course__r.name;
        priceItem.Course_ID__c = coRelationship.Course_Offering__r.hed__course__c;
        priceItem.Course_Offering_ID__c = coRelationship.Course_Offering__c;
        priceItem.cspmb__Product_Definition_Name__c = ConfigDataMapController.getCustomSettingValue('EmbeddedProductDefinitionName');
        //priceItem.Parent__c = coRelationship.Related_Course_Offering__r.hed__course__r.hed__Course_ID__c;
        if (coRelationship.Course_Offering__r.Status__c == 'Active') {
            priceItem.cspmb__Is_Active__c = true;
          } else {
            priceItem.cspmb__Is_Active__c = false;
          }
        //priceItem.cspmb__Is_Active__c = true;
        priceItem.TriggerDate__c = coRelationship.Enrolment_Trigger_Date__c;
        priceItem.External_Unique_ID__c = coRelationship.Course_Offering__c + '-' + coRelationship.Related_Course_Offering__c;
        csPriceItems.add(priceItem);

      }

    }

    try {
      //Upsert csPriceItems External_Unique_ID__c;

      List<Database.UpsertResult> result =  Database.upsert(csPriceItems, cspmb__Price_Item__c.External_Unique_ID__c);
      System.debug('upsert result: ' + result);

      // Iterate through each returned result
      for (Database.UpsertResult dbResultRecord : result) {
        if (dbResultRecord.isSuccess()) {
          // Operation was successful, so get the ID of the record that was processed
          System.debug('Successfully upserted record. ID: ' + dbResultRecord.getId());
        } else {
          // Operation failed, so get all errors
          for (Database.Error err : dbResultRecord.getErrors()) {
            System.debug('The following error has occurred.');
            System.debug(err.getStatusCode() + ': ' + err.getMessage());
            System.debug(' Fields that affected this error: ' + err.getFields());
          }
        }
      }

    } catch (exception e) {
      System.debug('Upsert exception: ' + e);
      ApplicationErrorLogger.logError(e);
    }

  }
}