/*****************************************************************************************************
 *** @Class             : EmbeddedMicroCredsTest
 *** @Author            : Avinash Machineni
 *** @Requirement       : Test class for transforming the embedded micro credentials data from HEDA into PriceItem table
 *** @Created date      : 27/06/2018
 *** @JIRA ID           : ECB-3731
 *****************************************************************************************************/
/*****************************************************************************************************
 *** @About Class
 *** This class is used to test how to transform the embedded micro credentials data from HEDA
 *****************************************************************************************************/
@isTest
public class EmbeddedMicroCredsTest {
	 
     public static void schedulerTest() 
    {
        String cronExp = '0 15 17 ? * MON-FRI';
        
        EmbeddedMicroCredsScheduler p = new EmbeddedMicroCredsScheduler();
        
        String jobId = System.schedule('Embedded Data sync',  cronExp, p);
        //CronTrigger ct = [SELECT Id, state, PreviousFireTime, CronExpression, TimesTriggered, NextFireTime FROM CronTrigger WHERE id = :jobId];
        //System.assertEquals(cronExp, ct.CronExpression);
        //System.assertEquals(0, ct.TimesTriggered);
    }
    
    @isTest
    public static void createPriceItemRecTest(){
        schedulerTest();
        TestDataFactoryUtil.testEmbeddedMicroCreds();
        
        Test.startTest();
        	EmbeddedMicroCreds.createPriceItemRec();
        Test.stopTest();
        System.assert(true);
    }
    
}