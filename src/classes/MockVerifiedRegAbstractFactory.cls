/**
 * @description This class creates verified user existing response from IDAM mock dependencies for the
 * registration process
 */
public  class MockVerifiedRegAbstractFactory implements RegistrationAbstractFactory 
{
	 /**
     * @description Creates the mock success captcha instance
     */
    public CaptchaVerificationRequest createCaptcha() 
    {
        return new MockSuccessCaptchaVerificationRequest();
    }

    /**
     * @description Creates the mock Idp user status request instance
     */
    public IdentityProviderUserStatusReq createIdpUserStatusReq(String email) 
    {
        return new MockVerifiedIdentityProviderUserStatus();
    }

    /**
     * @description Creates the mock Idp user creation request instance
     */
    public IdentityProviderUserRequest createIdpUserCreateReq() 
    {
        return new MockIdentityProviderUserRequest();
    }

    /**
     * @description Creates the mock Idp user delete request instance
     */
   public IdentityProviderUserDeleteRequest createIdpUserDeleteReq() {
        return new MockSuccesIdpUserDeleteRequest();
    }
	
}