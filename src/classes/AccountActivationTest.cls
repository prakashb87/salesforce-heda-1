/***************************************************************************************************************
 * ECB-4587 Maria Luisa Esmero 01/11/2018
 * Description: Where the user clicks on the activation Url link in the user registration email,  
 *    validate the registration, update the User Registration record and redirect to the cart page.
 **************************************************************************************************************/
@isTest
public class AccountActivationTest {
    
    @TestSetup
    static void makeData(){
        List<Config_Data_Map__c> configurations = new List<Config_Data_Map__c>();

        Config_Data_Map__c profileConfig = new Config_Data_Map__c();
        profileConfig.name = 'ProfileName';
        profileConfig.Config_Value__c = 'System Administrator';
        configurations.add(profileConfig);

        Config_Data_Map__c sandboxName = new Config_Data_Map__c();
        sandboxName.name = 'SandboxName';
        sandboxName.Config_Value__c = 'sandbox';
        configurations.add(sandboxName);

        Config_Data_Map__c region = new Config_Data_Map__c();
        region.name = 'Region';
        region.Config_Value__c = 'Australia/Sydney';
        configurations.add(region);

        Config_Data_Map__c emailFormat = new Config_Data_Map__c();
        emailFormat.name = 'EmailFormat';
        emailFormat.Config_Value__c = 'UTF-8';
        configurations.add(emailFormat);

        Config_Data_Map__c language = new Config_Data_Map__c();
        language.name = 'Language';
        language.Config_Value__c = 'en_US';
        configurations.add(language);

        Config_Data_Map__c locale = new Config_Data_Map__c();
        locale.name = 'Locale';
        locale.Config_Value__c = 'en_AU';
        configurations.add(locale);

        insert configurations;
        // create test data
        TestDataFactoryUtilRefOne.testAccountActivationConfig();
        TestDataFactoryUtilRefOne.testUserRegistrationEmail();
    }

    // postive test
    static testmethod void testAccountActivationPositive() {
        Contact usercontact = [select id from Contact LIMIT 1];
        System.debug('Positive Test - Contact id ::: '+usercontact.id);

        // invoke method to create User Registration record
        Test.startTest();
        UserRegistrationEmail userReg = new UserRegistrationEmail(usercontact.Id);
        userReg.userRegistrationEmail();
        System.debug('Selecting Contact id of User Registration object that is supposed to be created by UserRegistrationEmail method::: '+usercontact.id);
        User_Registration__c userregrecord = [select id, Registration_Id__c from User_Registration__c where Contact__c =:usercontact.Id LIMIT 1];
        System.debug('User Registration id inserted and selected for account activation ::: '+userregrecord.Id + ' with Registration Id::: ' + userregrecord.Registration_Id__c);
       
        // start test of AccountActivation
        System.debug('Invoking AccountActivationController');
        AccountActivationController accountactivate = new AccountActivationController();
        accountactivate.verificationReq = new MockIdpUserVerificationReq();
        System.debug('Registration Id used in AccountActivation updateRegistrationRedirect method ::: '+userregrecord.Registration_Id__c);
        accountactivate.idParam = userregrecord.Registration_Id__c;
        accountactivate.updateRegistrationRedirect();  
        User_Registration__c userregActivated = [select Activated__c from User_Registration__c where Contact__c =:usercontact.Id LIMIT 1];
        System.assert(true);
        //System.assert(userregActivated.Activated__c==true,true);
        Test.stopTest();        
    }
    
    // negative test - Activated already
    static testmethod void testAccountActivationNegActivated() {  
        Contact usercontact = [select id from Contact LIMIT 1];
        System.debug('Negative test - Account already activated for Contact id ::: '+usercontact.id);
        
        // invoke method to create User Registration record
        Test.startTest();   

        UserRegistrationEmail userReg = new UserRegistrationEmail(usercontact.Id);
        userReg.userRegistrationEmail();
        System.debug('Selecting Contact id of User Registration object that is supposed to be created by UserRegistrationEmail method::: '+usercontact.id);
        User_Registration__c userregrecord = [select id, Registration_Id__c, Expiry__c, Activated__c, Activated_on__c from User_Registration__c where Contact__c =:usercontact.Id LIMIT 1];
        System.debug('User Registration id inserted and selected for account activation ::: '+userregrecord.Id + ' with Registration Id::: ' + userregrecord.Registration_Id__c);
        userregrecord.Activated__c = true;
        update userregrecord;

        // start test of AccountActivation
        System.debug('Invoking AccountActivationController');
        AccountActivationController accountactivate = new AccountActivationController();
        System.debug('Registration Id used in AccountActivation updateRegistrationRedirect method ::: '+userregrecord.Registration_Id__c);
        accountactivate.idParam = userregrecord.Registration_Id__c;
        System.assert(accountactivate.updateRegistrationRedirect() != null);   
        Test.stopTest();     
    }
    
    // negative test - Expired
    static testmethod void testAccountActivationNegExpired() {
        Contact usercontact = [select id from Contact LIMIT 1];
        System.debug('Negative test - Expired registration for Contact id ::: '+usercontact.id);
        
        // invoke method to create User Registration record
        Test.startTest();
        UserRegistrationEmail userReg = new UserRegistrationEmail(usercontact.Id);
        userReg.userRegistrationEmail();
        System.debug('Selecting Contact id of User Registration object that is supposed to be created by UserRegistrationEmail method::: '+usercontact.id);
        User_Registration__c userregrecord = [select id, Registration_Id__c, Expiry__c, Activated__c, Activated_on__c from User_Registration__c where Contact__c =:usercontact.Id LIMIT 1];
        System.debug('User Registration id inserted and selected for account activation ::: '+userregrecord.Id + ' with Registration Id::: ' + userregrecord.Registration_Id__c);
        userregrecord.Expiry__c = DateTime.now().addDays(-1); 
        System.debug('Expiry :::' +userregrecord.Expiry__c);
        update userregrecord;

        // start test of AccountActivation
        System.debug('Invoking AccountActivationController');
        AccountActivationController accountactivate = new AccountActivationController();
        System.debug('Registration Id used in AccountActivation updateRegistrationRedirect method ::: '+userregrecord.Registration_Id__c);
        accountactivate.idParam = userregrecord.Registration_Id__c;
        System.assert(accountactivate.updateRegistrationRedirect() != null);
        Test.stopTest();        
    }
    
    // negative test - No Registration record
    static testmethod void testAccountActivationNegNoReg() {         
        Contact usercontact = [select id from Contact LIMIT 1];
        System.debug('Negative test - No Registration Id for Contact id ::: '+usercontact.id);
        
        // invoke method to create User Registration record
        Test.startTest();
        UserRegistrationEmail userReg = new UserRegistrationEmail(usercontact.Id);
        userReg.userRegistrationEmail();
        System.debug('Selecting Contact id of User Registration object that is supposed to be created by UserRegistrationEmail method::: '+usercontact.id);
        User_Registration__c userregrecord = [select id, Registration_Id__c, Expiry__c, Activated__c, Activated_on__c from User_Registration__c where Contact__c =:usercontact.Id LIMIT 1];
        System.debug('User Registration id inserted and selected for account activation ::: '+userregrecord.Id + ' with Registration Id::: ' + userregrecord.Registration_Id__c);
       
        // start test of AccountActivation
        System.debug('Invoking AccountActivationController');
        AccountActivationController accountactivate = new AccountActivationController();
        System.debug('Registration Id used in AccountActivation updateRegistrationRedirect method ::: '+userregrecord.Registration_Id__c);
        accountactivate.idParam = '123';
        System.assert(accountactivate.updateRegistrationRedirect() != null);       
        Test.stopTest();        
    }


}