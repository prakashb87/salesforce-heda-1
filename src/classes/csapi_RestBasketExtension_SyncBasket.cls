@RestResource(urlMapping='/csapi_BasketExtension/SyncBasket')
global class csapi_RestBasketExtension_SyncBasket {
    
    @HttpPost
    global static csapi_BasketRequestWrapper.SyncBasketResponse doPost(csapi_BasketRequestWrapper.SyncBasketRequest csapi_request) {
        
        RestRequest req = RestContext.request;
        RestResponse res = RestContext.response;
        
        Exception ee = null;
        csapi_BasketRequestWrapper.SyncBasketResponse csapi_response;
        try {
            csapi_response = csapi_BasketExtension.syncBasket(csapi_request);
        } catch(Exception e ) {
            ee = e;
        } finally {
            if(ee != null) 
            {
                system.debug('ee >> ' + ee.getMessage());
            }
        }
                
        return csapi_response;
    }
    
}