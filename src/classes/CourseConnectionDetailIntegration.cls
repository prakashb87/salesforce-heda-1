/*****************************************************************************************************
*** @Class             : CourseConnectionDetailIntegration
*** @Author            : Avinash Machineni
*** @Requirement       : Integration between Salesforce and IPaaS for sending course connection details
*** @Created date      : 15/05/2018
*** @JIRA ID           : ECB-2954
*****************************************************************************************************/
/*****************************************************************************************************
*** @About Class
*** This class is used integrate the records from Salesforce and IPaas for sending course connection details
*****************************************************************************************************/
@SuppressWarnings('PMD.AvoidGlobalModifier')
global with sharing class CourseConnectionDetailIntegration {

@future(callout = true)

  // Sending Request from Salesforce to IPaas
  global static void sendCourseConn(Set<Id> incomingCourseEnrolments) {
      
     system.debug('AAAAAAA');
    //Start: 06-06-2018 creating  SAMS request log to Integration log object.
    Integration_Logs__c requestLogObjectToInsert = new Integration_Logs__c();
    Integration_Logs__c responseLogObjectToInsert = new Integration_Logs__c(); 
    //Ends: 06-06-2018 creating  SAMS request log to Integration log object.
      
    List<hed__Course_Enrollment__c> ListCrsconn = [Select Id,
                                    Enrolment_Status__c,
                                    hed__Course_Offering__r.Name,
                                    hed__Contact__r.Id,
                                    hed__Contact__r.Student_ID__c,
                                    hed__Course_Offering__r.hed__Course__r.hed__Course_ID__c
                                    FROM hed__Course_Enrollment__c
                                    WHERE Id IN: incomingCourseEnrolments];
	List<CourseConnectionWrapper> crsconns = new List<CourseConnectionWrapper>();
    if (ListCrsconn.size() > 0) {
      for (hed__Course_Enrollment__c crsenr : ListCrsconn) 
      {          
        CourseConnectionWrapper cw = New CourseConnectionWrapper();
        cw.courseConnectionId = crsenr.Id;
          
        String subStringWithoutS ='';
        if( ! String.isBlank(crsenr.hed__Contact__r.Student_ID__c))
        {
            if( crsenr.hed__Contact__r.Student_ID__c.startsWith('S'))
   			{
            	subStringWithoutS = crsenr.hed__Contact__r.Student_ID__c.substringAfter('S');
            }
            else if(crsenr.hed__Contact__r.Student_ID__c.startsWith('s'))
            {
                subStringWithoutS = crsenr.hed__Contact__r.Student_ID__c.substringAfter('s');
            }
            else
            {
                subStringWithoutS = crsenr.hed__Contact__r.Student_ID__c;
            }
        }
        else
        {
            subStringWithoutS = '';
        }
        cw.studentId = subStringWithoutS;
        cw.courseId = crsenr.hed__Course_Offering__r.hed__Course__r.hed__Course_ID__c;
        cw.courseOfferingId = crsenr.hed__Course_Offering__r.Name;
        cw.enrolStatus = ConfigDataMapController.getCustomSettingValue('CourseConnectionEnrolmentStatusR');
        crsconns.add(cw);
        
      }
    }
    // HTTP Request Creation
    String requestJSONString = JSON.serialize(crsconns);
    String endpoint = ConfigDataMapController.getCustomSettingValue('CourseConnectionIPaasEndPoint');
    HttpRequest req = new HttpRequest();
    req.setEndpoint(endpoint);
    req.setHeader('client_id', ConfigDataMapController.getCustomSettingValue('CourseConnectionIPaasClientId'));
    req.setHeader('client_secret', ConfigDataMapController.getCustomSettingValue('CourseConnectionIPaasClientSecretId'));
    req.setHeader('Content-Type', 'application/json');
    req.setMethod('POST');
    req.setbody(requestJSONString);
    Http http = new Http();
    HTTPResponse response;
    try
    {
        //Start: 06-06-2018 creating  SAMS request log to Integration log object.
		requestLogObjectToInsert = IntegrationLogWrapper.createIntegrationLog('SAMS_IPaaS',requestJSONString, 'Outbound Service','',false);
		//Ends: 06-06-2018 creating SAMS request log to Integration log object.
		
        response = http.send(req); 
		System.debug('Http Response+++++++'+response);        
        //Start: 06-06-2018 creating SAMS response log to Integration log object
        responseLogObjectToInsert = IntegrationLogWrapper.createIntegrationLog('SAMS_IPaaS',response.getBody(), 'Acknowledgement','',false);
        //Ends: 06-06-2018 creating SAMS response log to Integration log object
    }
    catch(Exception e)
    {
        System.debug('Exception++++++'+e);
    }    
     
    System.debug('Response Code ---------->' + response.getStatusCode() + '\nResponse Code ---------->' + response.getStatus());

    List<Course_Connection_Life_Cycle__c> cclifecycles = [SELECT Id,
                                          Name,
                                          Status__c,
                                          Course_Connection__c
                                          FROM Course_Connection_Life_Cycle__c
                                          WHERE Course_Connection__c in : ListCrsconn AND Stage__c = : ConfigDataMapController.getCustomSettingValue('CourseConnectionLifeCycleStage')];

    if (cclifecycles.size() > 0) {
       
      if (response.getStatusCode() == 200) {
          for (Course_Connection_Life_Cycle__c cclyfcycle : cclifecycles) {
              cclyfcycle.Status__c = ConfigDataMapController.getCustomSettingValue('CourseConnectionLifeCycleInitiated');
          }
      } else {
        for (Course_Connection_Life_Cycle__c cclyfcycle : cclifecycles) {
          cclyfcycle.Status__c = ConfigDataMapController.getCustomSettingValue('CourseConnectionLifeCycleError');

        }
      }
        try {
            
            update cclifecycles;
        } catch (exception e) {
        ApplicationErrorLogger.logError(e);
      }

    }

    //Response from IPaas
    System.debug('response body ---->' + response.getBody());

    JsonCourseWrapper jstr = (JsonCourseWrapper) JSON.deserializeStrict(response.getBody(), JsonCourseWrapper.class);

    Map<String, JsonCourseWrapper.IpaasResponseWrapper> ipaasResWrp = new Map<String, JsonCourseWrapper.IpaasResponseWrapper>();

    for (JsonCourseWrapper.IpaasResponseWrapper jsoncrswrp : jstr.payload) {
      ipaasResWrp.put(jsoncrswrp.courseConnectionId, jsoncrswrp);
    }
    List<Course_Connection_Life_Cycle__c> cclifecycless = [SELECT Id,
                                                                  Name,
                                                                  Status__c,
                                          						  Course_Connection__c,
                                                                  Course_Connection__r.Enrolment_Status__c,
                                                                  Course_Connection__r.Name
                                                                  FROM Course_Connection_Life_Cycle__c
                                                                  WHERE Course_Connection__c in :ipaasResWrp.keyset() AND Stage__c = : ConfigDataMapController.getCustomSettingValue('CourseConnectionLifeCycleStage')];

    Set<id> ccId = new Set<id>();
	System.debug('Response from IPaas+++'+ipaasResWrp.keyset());
    List<hed__Course_Enrollment__c> crsconnections = new List<hed__Course_Enrollment__c>();

    if (cclifecycless.size() > 0) {

      for (Course_Connection_Life_Cycle__c cclyf : cclifecycless) {
          if (ipaasResWrp.get(cclyf.Course_Connection__c).result == ConfigDataMapController.getCustomSettingValue('CourseConnectionLifeCycleSuccess')) {
              cclyf.Status__c = ConfigDataMapController.getCustomSettingValue('CourseConnectionLifeCycleSuccess');
              crsconnections.add(new hed__Course_Enrollment__c(id = cclyf.Course_Connection__c, Enrolment_Status__c = ConfigDataMapController.getCustomSettingValue('CourseConnectionEnrolmentStatusE')));
          } else {
          cclyf.Status__c = ConfigDataMapController.getCustomSettingValue('CourseConnectionLifeCycleError');
          crsconnections.add(new hed__Course_Enrollment__c(id = cclyf.Course_Connection__c, Enrolment_Status__c = ''));
          ApplicationErrorLogger.logError(cclyf, 'SAMS Integration', ipaasResWrp);
            if(!Test.isRunningTest()){
                ErrorLogger.sendErrorEmailAlert(cclyf.Course_Connection__c);
            }else{
                System.debug('Test is Running');
            }
        }
      }
      try 
      {
        update cclifecycless;
        update crsconnections;
         // Starts: 06-06-2018 Below code is inserting  log records on integration log object.
        if( requestLogObjectToInsert != null)
        {
            insert requestLogObjectToInsert;   
        }
        if( responseLogObjectToInsert != null )
        {
            insert responseLogObjectToInsert;
        }
        // Ends:06-06-2018 Below code is inserting  log records on integration log object.
      } 
      catch (exception e) 
      {
        ApplicationErrorLogger.logError(e);
      }

    }
    System.debug(ipaasResWrp.keySet());     
  }
  
  public class CourseConnectionWrapper {
    Id courseConnectionId;
    String studentId;
    String courseId;
    String courseOfferingId;
    String enrolStatus;
  }
}