@isTest
public class TestResearcherPublicProjectsHelper {
    
    /************************************************************************************
// Purpose      :  Creating Test data for Community User for all test methods 
// Developer    :  Ankit
// Created Date :  10/24/2018                 
//***********************************************************************************/
    @testSetup static void testDataSetup() {
        
        RSTP_TestDataFactoryUtils.createNonCommunityUsersWithProfile(1,'System Administrator','Activator');
        
        User adminUser =[SELECT Id,ContactId FROM User WHERE UserName='test.nonCommunityUser1@test.com.testDev' LIMIT 1];
        System.runAs(adminUser)   
        {
            Map<String,Integer> allnumbersOfDataMap = new Map<String,Integer>();
            allnumbersOfDataMap.put('user',1);
            allnumbersOfDataMap.put('account',1);
            allnumbersOfDataMap.put('contact',1);
            
            Map<String,String> requiredInfoMap = new Map<String,String>();
            requiredInfoMap.put('userType','CommunityUsers');
            requiredInfoMap.put('profile',Label.testProfileName);
            
            Boolean userCreatedFlag = RSTP_TestDataFactoryUtils.testRSTPDataSetup(allnumbersOfDataMap,requiredInfoMap);
            System.assert(userCreatedFlag==true,'Community User Created'); 
            // Custom setting data preparation//
            CustomPermissionToProfile__c cPTP = new CustomPermissionToProfile__c(
                isAccessible__c=true,
                SetupOwnerId= [SELECT id FROM Profile WHERE NAME =:Label.testProfileName].Id    
            );
            insert cPTP;
            System.assert(cPTP!= null,'Custom setting records created');
			
			
			Contact con =[SELECT Id,Firstname FROM contact WHERE LastName='Contact1' LIMIT 1 ];
			con.hed__UniversityEmail__c='raj@rmit.edu.au';
			update con;
			
			Id userCreatedProjectRecordType = Schema.SObjectType.ResearcherPortalProject__c.getRecordTypeInfosByName().get('Researcher Portal Project').getRecordTypeId();
            Id rmProjectRecordType = Schema.SObjectType.ResearcherPortalProject__c.getRecordTypeInfosByName().get('Researcher Master Project').getRecordTypeId();
            
            String accessType = Label.RMITPublic;                                  
            
			//Researcher Project Data Creation Starts
            //Creating RMIT Public Rpp Project with required values and it's RMS 
            ResearcherPortalProject__c rppProject = new ResearcherPortalProject__c();
            rppProject.Status__c='Idea';
            rppProject.Access__c='RMIT Public';
            rppProject.Start_Date__c= date.newInstance(2018, 10, 08) ;
            rppProject.End_Date__c=date.newInstance(2018, 12, 14);
            rppProject.Impact_Category__c='Social';
            rppProject.Title__c='Test 1';
            rppProject.Keywords__c='Test keyword';
            rppProject.Description__c='Test';
            rppProject.RecordTypeId = userCreatedProjectRecordType;
            //rppProject.Portal_Project_Status__c='Conducting';
            
			//Creating RMIT Public Rpp Project 2 without required values and it's RMS 
			ResearcherPortalProject__c rppProject2 = new ResearcherPortalProject__c();
            rppProject2.Status__c='Idea';
            rppProject2.Access__c='RMIT Public';
            rppProject2.Start_Date__c= date.newInstance(2018, 10, 08) ;
            rppProject2.End_Date__c=date.newInstance(2018, 12, 14);
            rppProject2.Impact_Category__c='Social';
            rppProject2.Title__c='Others';
            rppProject2.Keywords__c='Others';
            rppProject2.Description__c='Others';
            rppProject2.RecordTypeId = userCreatedProjectRecordType;
            rppProject2.Portal_Project_Status__c='Conducting';
           
			//Creating RMIT Public RM Project and it's RMS
            ResearcherPortalProject__c rmProject = new ResearcherPortalProject__c();
            rmProject.Status__c='Idea';
            rmProject.Access__c='RMIT Public';          
            rmProject.Start_Date__c= date.newInstance(2018, 10, 08) ;
            rmProject.End_Date__c=date.newInstance(2018, 12, 14);
            rmProject.Impact_Category__c='Social';
            rmProject.Title__c='Test 2';
            rmProject.Keywords__c='Test keyword';
            rmProject.Description__c='Test';
            rmProject.RecordTypeId = rmProjectRecordType;
            rmProject.Portal_Project_Status__c='Conducting';            
           
			Insert new List<ResearcherPortalProject__c>{rppProject,rppProject2,rmProject};
			//Researcher Project Data Creation Ends
            
			
		   //RMS Records Creation Starts
            Researcher_Member_Sharing__c memberSharingRpp = new Researcher_Member_Sharing__c();
            memberSharingRpp.MemberRole__c = 'CI';
            memberSharingRpp.Contact__c = con.Id;
            memberSharingRpp.Researcher_Portal_Project__c = rppProject.id;
            memberSharingRpp.Primary_Contact_Flag__c = true  ;
			memberSharingRpp.Currently_Linked_Flag__c=true;
            memberSharingRpp.RecordTypeId = Schema.SObjectType.Researcher_Member_Sharing__c.getRecordTypeInfosByName().get(Label.RSTP_ResearchProjectRecordtype).getRecordTypeId();
            
            Researcher_Member_Sharing__c memberSharingRpp2 = new Researcher_Member_Sharing__c();
            memberSharingRpp2.MemberRole__c = 'CI';
            memberSharingRpp2.Contact__c = con.Id;
            memberSharingRpp2.Researcher_Portal_Project__c = rppProject2.id;
			memberSharingRpp2.Currently_Linked_Flag__c=true;
            memberSharingRpp2.Primary_Contact_Flag__c = true  ;
            memberSharingRpp2.RecordTypeId = Schema.SObjectType.Researcher_Member_Sharing__c.getRecordTypeInfosByName().get(Label.RSTP_ResearchProjectRecordtype).getRecordTypeId();
            
            Researcher_Member_Sharing__c rmMemberSharing = new Researcher_Member_Sharing__c();
            rmMemberSharing.MemberRole__c = 'CI';
            rmMemberSharing.Contact__c = con.Id;
            rmMemberSharing.Researcher_Portal_Project__c = rmProject.id;
			rmMemberSharing.Currently_Linked_Flag__c=true;
            rmMemberSharing.Primary_Contact_Flag__c = true  ;
            rmMemberSharing.RecordTypeId = Schema.SObjectType.Researcher_Member_Sharing__c.getRecordTypeInfosByName().get(Label.RSTP_ResearchProjectRecordtype).getRecordTypeId();
           
            Researcher_Member_Sharing__c rmMemberSharing1 = new Researcher_Member_Sharing__c();
            rmMemberSharing1.MemberRole__c = 'CI';
            rmMemberSharing1.Contact__c = con.Id;
            rmMemberSharing1.Researcher_Portal_Project__c = rmProject.id;
            rmMemberSharing1.Primary_Contact_Flag__c = true  ;
            rmMemberSharing1.RecordTypeId = Schema.SObjectType.Researcher_Member_Sharing__c.getRecordTypeInfosByName().get(Label.RSTP_ResearchProjectRecordtype).getRecordTypeId();
            rmMemberSharing1.AccessLevel__c='All';
            rmMemberSharing1.Currently_Linked_Flag__c=true;
            
			Insert new List<Researcher_Member_Sharing__c>{memberSharingRpp,memberSharingRpp2,rmMemberSharing1};
			
			//RMS Records Creation Ends
			
			
            Research_Project_Classification__c researchProjectClassification1    = new Research_Project_Classification__c();
            researchProjectClassification1.For_Four_Digit_Detail__c              = '1020 - Applied Science';
            researchProjectClassification1.ResearcherPortalProject__c            =  rmProject.id; 
            researchProjectClassification1.For_Six_Digit_Detail__c               = '1023 - Artificial Intelligence';

            Research_Project_Classification__c researchProjectClassification2    = new Research_Project_Classification__c();
            researchProjectClassification2.For_Four_Digit_Detail__c              = '1020 - Applied Science';
            researchProjectClassification2.ResearcherPortalProject__c            =  rppProject2.id; 
            researchProjectClassification2.For_Six_Digit_Detail__c               = '1023 - Artificial Intelligence';

            insert new List<Research_Project_Classification__c>{researchProjectClassification1,researchProjectClassification2};            
            
			
            
        }     
    }
    
    @isTest
    public static void researcherPublicHelperPositiveMethod(){
            String searchText = 'Test Contact keyword';
			
			//Creating Configuration Object
            ResearcherPublicProjectsHelper.ResearchProjectPublicListViewConfig configuration = new ResearcherPublicProjectsHelper.ResearchProjectPublicListViewConfig();
            configuration.tabSelected = 'RM';
            configuration.searchText = searchText;
            configuration.forCodeDetailSearch = new Set<String>();
            configuration.forCodeDetailSearch.add('1020 - Applied Science');
            configuration.impactCodeDetailSearch = new Set<String>();
            configuration.impactCodeDetailSearch.add('social');
            System.assert(configuration!= null,'Configuration records created');
            
			ResearcherPublicProjectsHelper.ResearchProjectPublicListViewConfig configuration2 = new ResearcherPublicProjectsHelper.ResearchProjectPublicListViewConfig();
            configuration2.tabSelected = 'RM';
            configuration2.searchText = '';
            configuration2.forCodeDetailSearch = new Set<String>();
            configuration2.impactCodeDetailSearch = new Set<String>(); 
            System.assert(configuration2!= null,'Configuration records created');
			/*
            
            Map<Id,List<Researcher_Member_Sharing__c>> rmsMapList = new Map<Id,List<Researcher_Member_Sharing__c>>();
            rmsMapList.put(rmProject.Id,rmsList);
            
            Map<Id,List<Researcher_Member_Sharing__c>> rmsMapList1 = new Map<Id,List<Researcher_Member_Sharing__c>>();
            rmsMapList1.put(rmMemberSharing.Id,rmsList);
			*/
            
            Test.startTest();
            
				ResearcherPublicProjectsHelper researcherPublicProjectsHelperObj = new ResearcherPublicProjectsHelper();
				researcherPublicProjectsHelperObj.getRMITPublicResearcherProject(configuration);
				researcherPublicProjectsHelperObj.getRMITPublicResearcherProjectFiltered(configuration);                        
				
				configuration.tabSelected = 'UC';           
				
				researcherPublicProjectsHelperObj.getRMITPublicResearcherProject(configuration);
				researcherPublicProjectsHelperObj.getRMITPublicResearcherProjectFiltered(configuration);
				
				  
				researcherPublicProjectsHelperObj.getRMITPublicResearcherProject(configuration2);
				researcherPublicProjectsHelperObj.getRMITPublicResearcherProjectFiltered(configuration2); 
				
				//ResearcherPublicProjectsHelperUtil.getRMSMap(rmsMapList, rmMemberSharing);
				//ResearcherPublicProjectsHelperUtil.getRMSMap(rmsMapList1, rmMemberSharing);
            
            Test.stopTest();    
            
       // }
        
    }
    @isTest
    public static void researcherPublicHelperNegativeMethod(){
        
        User usr =[SELECT Id,ContactId FROM User WHERE UserName='test.communityUser1@test.com.testDev' LIMIT 1];           
        system.runAs(usr)
        {
            
            Boolean result = false;
			
            Contact con =[SELECT Id,Firstname,lastname FROM contact WHERE LastName='Contact1' LIMIT 1 ];
            con.hed__UniversityEmail__c='raj@rmit.edu.au';
            con.lastname='';
            //update con; 
            try
            {
                update con;    
            }
            catch(DmlException ex)
            {
                result =true;                   
                
            }
            system.assert(result);
            ResearcherPortalProject__c researchProject = new ResearcherPortalProject__c();
            researchProject.Status__c='Idea';
            researchProject.Access__c='Private';
            insert researchProject;
            system.assert(researchProject!=null); 
            List<id> researchId = new List<id>();
            researchId.add(researchProject.id);
            
            Researcher_Member_Sharing__c memberResearch = new Researcher_Member_Sharing__c();
            
            try
            {
                memberResearch .Contact__c = con.Id;
                memberResearch.User__c = usr.Id;
                insert memberResearch;   
            }
            catch(Exception ex)
            {
                result =true;                   
                
            }  
            system.assert(result);
            
            ResearcherPublicProjectsHelperUtil.RMITPublicProjectWrapper publicProjectWrapper = new ResearcherPublicProjectsHelperUtil.RMITPublicProjectWrapper();
            
            ResearcherPublicProjectsHelper.ResearchProjectPublicListViewConfig configuration = new ResearcherPublicProjectsHelper.ResearchProjectPublicListViewConfig();
            configuration.tabSelected = 'All';
            configuration.searchText = 'Test User Aerospace Gourav';
            configuration.forCodeDetailSearch = new Set<String>();
            configuration.impactCodeDetailSearch = new Set<String>();               
            
            Test.startTest();
            
            ResearcherPublicProjectsHelper researcherPublicProjectsHelperObj = new ResearcherPublicProjectsHelper();
            researcherPublicProjectsHelperObj.getRMITPublicResearcherProject(configuration);
            researcherPublicProjectsHelperObj.getRMITPublicResearcherProjectFiltered(configuration);                                                                    
           
            
            Test.stopTest();    
            
            
        }        
    }
}