public without sharing class ReSendMail {
    String ID;
    public ReSendMail(Apexpages.StandardController controller) {
        ID = ApexPages.currentPage().getParameters().get('ID');
    }
    public PageReference sendMailConfirm() {
        System.debug('ID===' + ID);
        try {
            EmailMessage emailDetails = new EmailMessage();
            emailDetails = [SELECT Subject, BccAddress, CcAddress, FromAddress, FromName, HasAttachment, Headers, HtmlBody, Id, ReplyToEmailMessageId, TextBody, ToAddress FROM EmailMessage WHERE Id =: ID Limit 1];
            System.debug('emailDetails ===' + emailDetails);
            Messaging.SingleEmailMessage mail = new Messaging.SingleEmailMessage();
            mail.setUseSignature(false);
            //mail.setSaveAsActivity(true);
            IF(emailDetails.ToAddress != null)
            {
            mail.setToAddresses(new String[] {
                emailDetails.ToAddress
            });
            }
            IF(emailDetails.FromAddress != null)
            {
             mail.setReplyTo(emailDetails.FromAddress);
            }
            IF(emailDetails.FromName != null)
            {
             mail.setSenderDisplayName(emailDetails.FromName);
            }
            IF(emailDetails.CcAddress != null)
            {
            mail.setCcAddresses(new String[] {
                emailDetails.CcAddress
            });
            }
            // Step 4. Set email contents - you can use variables!
            IF(emailDetails.Subject != null)
            {
              mail.setSubject(emailDetails.Subject);
            }
            If(emailDetails.HtmlBody != null)
            {
              mail.setHtmlBody(emailDetails.HtmlBody);
            }
            else
            {
                mail.setPlainTextBody(emailDetails.TextBody);
            }
            //mail.setSaveAsActivity(true);
            // Step 6: Send all emails in the master list
            Contact vCon = [SELECT Id, Name, Email
                FROM Contact
                WHERE Email =: emailDetails.ToAddress
                LIMIT 1
            ];
            System.debug('contactId ===' + vCon.Id);
            //mail.whatid(vCon.Id);
            List <Messaging.Emailfileattachment > fileAttachments = new List < Messaging.Emailfileattachment > ();
            for (Attachment a: [select Name, Body, BodyLength from Attachment where ParentId =: ID]) {
                // Add to attachment file list
                Messaging.Emailfileattachment efa = new Messaging.Emailfileattachment();
                efa.setFileName(a.Name);
                efa.setBody(a.Body);
                fileAttachments.add(efa);
            }
            mail.setFileAttachments(fileAttachments);
            mail.setTargetObjectId(vCon.Id);
            mail.setSaveAsActivity(true);
            Messaging.SingleEmailMessage[] emailList = new Messaging.SingleEmailMessage[] {
                mail
            };
            IF(emailList != null)
            {
            Messaging.sendEmail(emailList);
            System.debug('mail ===' + emailList);
            }
            return new PageReference('/' + vCon.Id);
        } catch (Exception e) {
            ApexPages.addmessage(new ApexPages.message(ApexPages.severity.WARNING, 'Something went wrong, Please check the Values'));
            return null;
        }
    }

}