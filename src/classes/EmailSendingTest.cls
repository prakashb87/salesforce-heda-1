/*****************************************************************
Name: EmailSendingTest
Author: Capgemini 
Purpose: Test Class of EmailSending for JIRA ID 3217 & 2451
*****************************************************************/
/*==================================================================
History
--------
Version   Author                   Date              Detail
1.0      Shreya/Praneet           08/05/2018         Initial Version
********************************************************************/
@isTest
public class EmailSendingTest {

    static testmethod void test(){
      
        TestDataFactoryUtil.test();
        //Querying data from test datasetup method
        List<csord__Service__c> servicesList = [Select Id from csord__Service__c];
        system.assert(servicesList.size()>0,true);
        
        List<id> serviceIDList = new List<Id>();
        
        for(csord__Service__c ser : servicesList)
        {
            serviceIDList.add(ser.id);  
        }
        
        Test.startTest();
        
        PDFGenerationObserver.generatePDF(serviceIDList); 
        EmbeddedPdfAndIntegrationController.getServices(serviceIDList);
            
        Test.stopTest();   
         system.assert(servicesList.size()>0,true);
       
    }
}