public with sharing class UserVerificationResponse
{
	public String id;
	public String result;
	public String code;
	public String application;
	public String provider;
	public Payload payload;

	public class Payload 
	{
		public Boolean success;
		public String message;		
	}
	
	public static UserVerificationResponse parse(String json) {
		return (UserVerificationResponse) System.JSON.deserialize(json, UserVerificationResponse.class);
	}
}