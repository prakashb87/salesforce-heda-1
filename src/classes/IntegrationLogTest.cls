@isTest
public class IntegrationLogTest {

    @isTest
    static void testDefaultConstructorValues() {
        IntegrationLog integrationLog = new IntegrationLog();
        System.assertEquals('', integrationLog.requestBody);
    }

    @isTest
    static void testSetRequestBody() {
        IntegrationLog integrationLog = new IntegrationLog();
        integrationLog.setRequestBody('{ "money": "100", "product": "ABC123" }');
        System.assertEquals('{ "money": "100", "product": "ABC123" }', integrationLog.requestBody);
    }

    @isTest
    static void testSetResponsebody() {
        IntegrationLog integrationLog = new IntegrationLog();
        integrationLog.setResponseBody('{ "code": "100", "success": "ABC123"true }');
        System.assertEquals('{ "code": "100", "success": "ABC123"true }', integrationLog.responseBody);
    }

    @isTest
    static void testSetIsExceptionTrue() {
        IntegrationLog integrationLog = new IntegrationLog();
        integrationLog.setIsException(true);
        System.assertEquals(true, integrationLog.isException);
    }

    @isTest
    static void testSetIsExceptionFalse() {
        IntegrationLog integrationLog = new IntegrationLog();
        integrationLog.setIsException(false);
        System.assertEquals(false, integrationLog.isException);
    }

    @isTest
    static void testSetRecordIdStringType() {
        IntegrationLog integrationLog = new IntegrationLog();
        integrationLog.setRecordId('Id1223564');
        System.assertEquals('Id1223564', integrationLog.recordId);
    }

    @isTest
    static void testSetRecordIdStringTypeExceedLength() {
        IntegrationLog integrationLog = new IntegrationLog();
        String tooLongRecordId = '';
        for (Integer characterPosition = 0; characterPosition < 101; characterPosition++) {
            tooLongRecordId += 'a';
        }
        try {
            integrationLog.setRecordId(tooLongRecordId);
            System.assert(false);
        } catch(ArgumentException e) {
            System.assert(true);
        }
    }

    @isTest
    static void testSetRecordIdWithIdType() {
        // Use the log object to ensure there is no validation rules running
        // since we only need the record id
        RMErrLog__c	randomRecord = new RMErrLog__c();
        randomRecord.Class_Name__c = 'TestClass';
        insert randomRecord;

        Test.startTest();
        IntegrationLog integrationLog = new IntegrationLog();
        integrationLog.setRecordId(randomRecord.Id);
        System.assertEquals((String) randomRecord.Id, integrationLog.recordId);
        Test.stopTest();
    }

    @isTest
    static void testSetServiceType() {
        IntegrationLog integrationLog = new IntegrationLog();
        integrationLog.setServiceType('RANDOM');
        System.assertEquals('RANDOM', integrationLog.serviceType);
    }

    @isTest
    static void testSetType() {
        IntegrationLog integrationLog = new IntegrationLog();
        integrationLog.setType('CRITICAL');
        System.assertEquals('CRITICAL', integrationLog.type);
    }

    @isTest
    static void testLogToDatabaseTurnedOff() {

        Config_Data_Map__c logConfig = new Config_Data_Map__c();
        logConfig.name = 'LogIntegrationRequestToIntegrationLog';
        logConfig.Config_Value__c = 'false';
        insert logConfig;

        Integer integrationLogCount = [SELECT COUNT() FROM Integration_Logs__c];

        Test.startTest();
        IntegrationLog integrationLog = new IntegrationLog();
        integrationLog.setIsException(false);
        integrationLog.setRequestBody('{"courseId": "CAL123", "courseName": "Calculus 101"}');
        integrationLog.setType('INFO');
        integrationLog.log();
        Test.stopTest();

        Integer integrationLogCountAfterTest = [SELECT COUNT() FROM Integration_Logs__c];
        System.assertEquals(integrationLogCount, integrationLogCountAfterTest);
    }

    @isTest
    static void testLogToDatabaseTurnedOn() {

        Config_Data_Map__c logConfig = new Config_Data_Map__c();
        logConfig.name = 'LogIntegrationRequestToIntegrationLog';
        logConfig.Config_Value__c = 'true';
        insert logConfig;

        Integer integrationLogCount = [SELECT COUNT() FROM Integration_Logs__c];

        Test.startTest();
        IntegrationLog integrationLog = new IntegrationLog();
        integrationLog.setIsException(false);
        integrationLog.setRequestBody('{"courseId": "ALG123", "courseName": "Alegbra 101"}');
        integrationLog.setType('WARN');
        integrationLog.log();
        Test.stopTest();

        Integer integrationLogCountAfterTest = [SELECT COUNT() FROM Integration_Logs__c];
        System.assertEquals(integrationLogCount + 1, integrationLogCountAfterTest);
    }


    @isTest
    static void testLogReqResToDatabaseTurnedOn() {

        Config_Data_Map__c logConfig = new Config_Data_Map__c();
        logConfig.name = 'LogIntegrationRequestToIntegrationLog';
        logConfig.Config_Value__c = 'true';
        insert logConfig;

        Integer integrationLogCount = [SELECT COUNT() FROM Integration_Logs__c];

        Test.startTest();
        IntegrationLog integrationLog = new IntegrationLog();
        integrationLog.setIsException(false);
        integrationLog.setRequestBody('{"courseId": "ALG123", "courseName": "Alegbra 101"}');
        integrationLog.setResponseBody('{"message": "No disk space", "success":false}');
        integrationLog.setType('SEVERE');
        integrationLog.log();
        Test.stopTest();

        Integer integrationLogCountAfterTest = [SELECT COUNT() FROM Integration_Logs__c];
        System.assertEquals(integrationLogCount + 2, integrationLogCountAfterTest);
    }


    @isTest
    static void testLogToDatabase() {

        Integer integrationLogCount = [SELECT COUNT() FROM Integration_Logs__c];

        Test.startTest();
        IntegrationLog integrationLog = new IntegrationLog();
        integrationLog.setIsException(false);
        integrationLog.setRequestBody('{"courseId": "ALG123", "courseName": "Alegbra 101"}');
        integrationLog.setType('WARN');
        integrationLog.logToIntegrationObject();
        Test.stopTest();

        Integer integrationLogCountAfterTest = [SELECT COUNT() FROM Integration_Logs__c];
        System.assertEquals(integrationLogCount + 1, integrationLogCountAfterTest);
    }


    @isTest
    static void testBulkLogToDatabase(){
        Integer integrationLogCount = [SELECT COUNT() FROM Integration_Logs__c];

        Test.startTest();
        IntegrationLog integrationLog = new IntegrationLog();
        integrationLog.setIsException(false);
        integrationLog.setRequestBody('{"courseId": "ALG123", "courseName": "Alegbra 101"}');
        integrationLog.setResponseBody('{"message":"OK", "success":true}');
        integrationLog.setType('WARN');
        integrationLog.logRequestResponseToIntObject();
        Test.stopTest();

        Integer integrationLogCountAfterTest = [SELECT COUNT() FROM Integration_Logs__c];
        System.assertEquals(integrationLogCount + 2, integrationLogCountAfterTest);
    }

}