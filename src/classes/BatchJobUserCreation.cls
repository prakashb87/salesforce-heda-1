/********************************************************************
// Purpose              : Student User creation in Salesforce
// Author               : Capgemini [Rishabh]
// Parameters           :
//  Returns             : null
//JIRA Reference        : ECB-3249 Student User creation in Salesforce
//********************************************************************/
public class BatchJobUserCreation implements Database.Batchable<SObject> {

  public string querystring;
  UserCreationHelper batchhelper;

  public BatchJobUserCreation() {
    batchhelper = new UserCreationHelper();
  }

  public Database.QueryLocator start(Database.BatchableContext bc) {
    //querystring = 'select Id, FirstName, LastName, hed__Preferred_Email__c, Email,Student_ID__c, Title,Community_User_Provisioned__c from Contact where Community_User_Provisioned__c=false and id NOT IN (select contactId from User) and System_Student__c=true ';
    
    //ECB-4238 need to filter out stuents without valide sutdent id and email
    querystring = 'select Id, FirstName, LastName, hed__Preferred_Email__c, Email,Student_ID__c, Title,Community_User_Provisioned__c from Contact where Community_User_Provisioned__c=false and id NOT IN (select contactId from User) and System_Student__c=true AND Student_ID__c !=null AND Email !=null';


    system.debug('Query--->' + querystring);
    return Database.getQueryLocator(querystring);

  }


  public void execute(Database.BatchableContext bc, List<Contact> scope) {
    System.debug('Inside Exec');
    batchhelper.createUserOnContactCreation(scope);
    system.debug('Scope Size-->' + scope.size());
    System.debug('Execute--->' + scope);
  }

  public void finish(Database.BatchableContext bc) {
    System.debug('Inside Finish Block');

  }
}