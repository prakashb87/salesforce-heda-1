<apex:page standardController="Lead" extensions="CampaignEventNewStudentController" sidebar="false" showHeader="false">
  <head>
      <meta name="viewport" content="width=device-width, initial-scale=1"/>
      <apex:stylesheet value="{!URLFOR($Resource.bootstrap_3_3_5, '/bootstrap-3.3.5-dist/css/bootstrap.min.css')}"/>
      <apex:includeScript value="{!URLFOR($Resource.bootstrap_3_3_5, '/bootstrap-3.3.5-dist/js/bootstrap.min.js')}"/>
      <script src="https://ajax.googleapis.com/ajax/libs/jquery/3.1.1/jquery.min.js"></script>
      <script src="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.7/js/bootstrap.min.js"></script>
      <style>
          body{
            font-family: sans-serif;
          }
          
          .head{
              background-color: #DCDDD7;
          }
          .title{
              background-color: #242424;
              color: white;
              padding-top: 50px;
              padding-bottom: 10px;
              padding-left: 10px;
              padding-right: 10px;
          }
          .wrapper{
            background-color: #DADADA;
            padding-left: 10px;
            padding-right: 10px;
            padding-top: 20px;
            padding-bottom: 20px;
          }
          .content{
              background-color: #DADADA;
              padding-left: 10px;
              padding-right: 10px;
          }
          .content a,.content a:hover{
            color: #cf120f;
          }
          h3{
              margin-bottom: 10px;
          }
          .rmit-stn{
            margin-top: 5px;
            margin-bottom: 5px;
            color: #fff;
            background: #d9534f;
            border-color: #d43f3a;
            display: inline-block;
            padding: 6px 12px;
            font-size: 14px;
            font-weight: 400;
            line-height: 1.42857143;
            text-align: center;
            white-space: nowrap;
            vertical-align: middle;
          }
      </style>
  </head>
  <body>  
    <div class="container-fluid">
        <apex:form styleClass="form-horizontal" id="mainForm">
            <!--<apex:pageMessages escape="false"></apex:pageMessages>-->
            <div class="row head">
              <div class="col-md-2">
                  <apex:image id="theImage" value="{!$Resource.RMIT_Logo}" width="250"/>
              </div>
              <div class="col-md-10"></div>
            </div>
            
            <div class="row title">
                <H1>Get Prepared</H1>
            </div>
            
            <div class="row wrapper">
                <div class="col-md-6">
                    <H3>Your details</H3>
                </div>  
            </div>
            <div class="row content">
                
                <div class="col-md-6">
                    <div class="form-group">
                        <label for="Firstname" class="col-sm-4 control-label">First name: *</label>
                        <div class="col-sm-4">
                            <apex:inputText styleClass="form-control" id="Firstname" value="{!firstName}"/>
                        </div>
                    </div>
                    <div class="form-group">
                        <label for="Lastname" class="col-sm-4 control-label">Last name: *</label>
                        <div class="col-sm-4">
                          <apex:inputText styleClass="form-control" id="Lastname" value="{!lastName}"/>
                        </div>
                    </div>
                    <div class="form-group">
                        <label for="inputEmail" class="col-sm-4 control-label">Email address: *</label>
                        <div class="col-sm-4">
                          <apex:inputText styleClass="form-control" id="Email" value="{!email}"/>
                        </div>
                        
                    </div>
                    <div class="form-group">
                        <label for="Mobile" class="col-sm-4 control-label">Mobile: </label>
                        <div class="col-sm-4">
                          <apex:inputText styleClass="form-control" id="Mobile" value="{!mobile}"/>
                        </div>
                    </div>
                    <div class="form-group">
                        <label for="PostCode" class="col-sm-4 control-label">Post Code: </label>
                        <div class="col-sm-4">
                          <apex:inputText styleClass="form-control" id="PostCode" value="{!Lead.PostCode__c}"/>
                        </div>
                    </div>
                    <div class="form-group">
                        <label for="School" class="col-sm-4 control-label">School: </label>
                        <div class="col-sm-4">
                          <apex:inputText styleClass="form-control" disabled="{!hasSchool}" id="School" value="{!school}"/>
                        </div>
                    </div>
                    <div class="form-group">
                        <label for="currently" class="col-sm-4 control-label">I am currently: *</label>
                        <div class="col-sm-5">
                            <apex:selectList id="Currently" styleClass="form-control" style="margin-top: 7px;" value="{!Lead.I_am_currently__c}" size="1">
                                <apex:selectOptions value="{!IAmCurrently}"/>
                            </apex:selectList>  
                        </div>
                    </div>                    
                    <div class="form-group">
                        <label for="currently" class="col-sm-4 control-label">Residency Status: *</label>
                        <div class="col-sm-5">
                            <apex:selectList id="Picklist_Residency_Status" styleClass="form-control" style="margin-top: 7px;" value="{!Lead.Residency_status__c}" size="1">
                                <apex:selectOptions value="{!ResidencyStatus}"/>
                            </apex:selectList>  
                        </div>
                    </div>
                </div>
                <div class="col-md-6">
                    <div class="form-group">
                        
                        <label for="" class="col-sm-4 control-label">Select an area you are interested in: *</label>
                        <div class="col-sm-5">
                            <apex:selectList id="Picklist_Select_Area_Interested" styleClass="form-control" style="margin-top: 10px;" value="{!interestArea}" size="1">
                              <apex:selectOptions value="{!SelectAreaInterested}"/>
                            </apex:selectList>
                        </div>
                        
                    </div>
                </div>
            </div>
            

            <div class="row content" style="padding-top: 20px;">
                <div class="col-md-2"></div>
                <div class="col-md-10">
                    <p>
                        RMIT University values privacy and is committed to handling personal, sensitive and health information in accordance with the <br/>
                        <a href="http://www1.rmit.edu.au/browse;ID=eislllwhc1mb">RMIT University Privacy and Data Protection Policy</a> and the privacy principles contained in the Privacy and Data Protection Act 2014 (Vic) and the Health<br/>
                        Records Act 2001 (Vic).
                    </p>
                    <p>
                        The information you provide in this form will be kept securely and accessed only by staff for the purpose of University functions. <br/>
                        De-identified information may be used for improvement processes.
                    </p>
                </div>
            </div>
        
            <div id="button_panel" class="row content">
                <div class="col-md-2"></div>
                <div class="col-md-10">
                    <button type="button" id="myBtn"
                        style="margin-top: 5px;
                                    margin-bottom: 5px;
                                    color: #fff;
                                    background: #d9534f;
                                    border-color: #d43f3a;
                                    display: inline-block;
                                    padding: 6px 12px;
                                    font-size: 14px;
                                    font-weight: 400;
                                    line-height: 1.42857143;
                                    text-align: center;
                                    white-space: nowrap;
                                    vertical-align: middle;"
                                value="Submit">Submit</button>
                        <apex:actionFunction action="{!createNewStudent}" name="savechange"/>
                </div>
            </div>
        </apex:form>
    </div> 

      <!-- Modal -->
      <div class="modal fade" id="myModal" role="dialog">
        <div class="modal-dialog">
        
          <!-- Modal content-->
          <div class="modal-content">
            <div class="modal-header">
            </div>
            <div class="modal-body">
                <p style="text-align: center">Thank You for your request. We will contact you soon!<span id="count" style="display: none">1</span></p>
            </div>
            <div class="modal-footer">
              
            </div>
          </div>
          
        </div>
      </div>
                                          
    <script type="text/javascript">
      
      $(document).ready(function(){
          $("#myBtn").click(function(){
              
          var mailformat = /^\w+([\.-]?\w+)*@\w+([\.-]?\w+)*(\.\w{2,3})+$/;
              //var nameFormat = /(\b[a-z](?!\s))/g;
          var firstname = true;
          var lastname = true;
          var email = true;
          var currently = true;
          var area_Interested = true;
          var residency_Status = true;
          
          
        if(document.getElementById('{!$component.mainForm.Firstname}').value == ''){
            firstname = false;
            alert("Please enter value in First Name.");
            return false;           
        }
        document.getElementById('{!$component.mainForm.FirstName}').value = getFname();
        function getFname(){
            var fname = document.getElementById('{!$component.mainForm.Firstname}').value;
            fname = checkforchar(fname);
            return fname;
        } 
        if(document.getElementById('{!$component.mainForm.Lastname}').value == ''){
            lastname = false;
            alert("Please enter value in Last Name.");                    
            return false;  
        }
        document.getElementById('{!$component.mainForm.LastName}').value = getLname();
        function getLname(){
            var lname = document.getElementById('{!$component.mainForm.LastName}').value;
            lname = checkforchar(lname);
            return lname;
        }

        function checkforchar(flname){
            flname = flname.toLowerCase();
            if (flname.split(' ').length >1){
                flname = flname.charAt(0).toUpperCase() + flname.substr(1).toLowerCase();
            }
            flname = flname.charAt(0).toUpperCase()+ flname.slice(1);
            if (flname.split(' ').length >1){
                flname = flname.substr(0,flname.indexOf(' '));
            }
            if (flname.indexOf('-') >= 0){
                var words = flname.match(/([^-]+)/g) || [];
                words.forEach(function(word, i) {
                    words[i] = word[0].toUpperCase() + word.slice(1);
                    words[i] = words[i].charAt(0).toUpperCase() + words[i].substr(1).toLowerCase();
                    flname = words.join('-');
                });
            }
            return flname;
        }           

              
        if(document.getElementById('{!$component.mainForm.Email}').value == ''){
            email = false;
            alert("Please enter value in Email.");                    
            return false;   
        }else if(document.getElementById('{!$component.mainForm.Email}').value != ""){
            var x = document.getElementById('{!$component.mainForm.Email}').value;
            var atpos = x.indexOf("@");
            var dotpos = x.lastIndexOf(".");
            if (atpos<1 || dotpos<atpos+2 || dotpos+2>=x.length) {
                email = false;
                alert("Not a valid e-mail address");
                return false;
            }
        }
        if(document.getElementById('{!$component.mainForm.Currently}').value == '--None--'){
            currently = false;
            alert("Please select value in 'I am currently'.");
            return false;  
        }
        if(document.getElementById('{!$component.mainForm.Picklist_Residency_Status}').value == '--None--'){
            residency_Status = false;
            alert("Please select value in 'Residency Status'.");
            return false;  
        }    
        if(document.getElementById('{!$component.mainForm.Picklist_Select_Area_Interested}').value == '--None--'){
            area_Interested = false;
            alert("Please select value in 'Select an area you are interested in'.");
            return false;
        }
        
        if((firstname = true) && (lastname = true) && (email = true) && (currently = true) && (area_Interested = true)){
            $("#myModal").modal();
            var counter = 1;
            setInterval(function() {
                counter--;
                if (counter >= 0) {
                    span = document.getElementById("count");
                    span.innerHTML = counter;
                }
                if (counter === 0) {
                    savechange();
                    clearInterval(counter);
                }
            }, 800);
        }
    });
});
        
      
      </script>                                      
  </body>
</apex:page>