/*********************************************************************************
 *** @Trigger           : AccountTrigger
 *** @Author            : Anupam Singhal
 *** @Requirement       : To update values Of Account
 *** @Created date      : 04/09/2018
 **********************************************************************************/
trigger AccountTrigger on Account (before insert) {
    AccountTriggerHandler classAccountTriggerHandler = new AccountTriggerHandler();
	if(trigger.isInsert && trigger.isBefore)
    {           
        classAccountTriggerHandler.accountDeDupeFieldUpdate(trigger.new);
    }
}