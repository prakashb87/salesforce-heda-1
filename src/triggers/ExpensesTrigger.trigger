trigger ExpensesTrigger on Expenses__c (before delete, after insert, after update) {

    if(Trigger.isBefore){

        if(Trigger.isDelete){

            ExpenseRollupToContact.rollupExpensesTotalHoursCompletedToContact_ForDelete(Trigger.old);
            ExpenseRollupToContact.rollupExpensesTotalPaymentToContact_ForDelete(Trigger.old);
            ExpenseRollupToContact.rollupExpensesTotalVouchersToContact_ForDelete(Trigger.old);
        }
        
    }else if(Trigger.isAfter){
        
        if(Trigger.isInsert){
        
            //Rollup to Contact.Total_Hours_Completed
            ExpenseRollupToContact.rollupExpensesTotalHoursCompletedToContact(Trigger.new);
            ExpenseRollupToContact.rollupExpensesTotalPaymentToContact(Trigger.new);
            ExpenseRollupToContact.rollupExpensesTotalVouchersToContact(Trigger.new);
        
        }else if(Trigger.isUpdate){
            ExpenseRollupToContact.rollupExpensesTotalHoursCompletedToContact(Trigger.new);
            ExpenseRollupToContact.rollupExpensesTotalPaymentToContact(Trigger.new);
            ExpenseRollupToContact.rollupExpensesTotalVouchersToContact(Trigger.new);            
        }    
    }
}