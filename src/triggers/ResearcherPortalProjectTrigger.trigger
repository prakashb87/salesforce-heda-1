/**************************************************************
Purpose: Trigger for RM project inserted (only on insert), 
         status should set “Conduct” and Access level should 
         be set “Member”  
History:
Created by Subhajit on 10/08/2018
*************************************************************/
trigger ResearcherPortalProjectTrigger on ResearcherPortalProject__c (before Insert ,after Insert, before update, after update, before delete) {

  
  
  // Passing Trigger.new records to change status in rm projects
  if(Trigger.isBefore && Trigger.isInsert)
  {
      ResearcherPortalProjectTriggerHandler.changeStatusForRMProj(Trigger.new);
  }
  //RPORW-152 :: Added by subhajit :: 10.10.18   
  //Purpose :: When RM project get updated and project access scenario handled
  if(Trigger.isAfter && Trigger.isUpdate)
   {
      ResearcherPortalProjectTriggerHandler.assignProjectAccessProject(Trigger.new, Trigger.old); 
      if(CheckRecursive.runOnce()){
            

         ResearcherPortalProjectTriggerHandler.assignRMITPublicAccessType(Trigger.new, Trigger.oldMap);
               

      }
   } 
    
   if(Trigger.isBefore && Trigger.isUpdate)
   {
      
      ResearcherPortalProjectTriggerHandler.changeGoupStatusAsPerApplicationStatus(Trigger.new, Trigger.oldMap); 
      
      
   }  
   
    //RPORW-998 :: Added by Ankit:: 03.03.19   
  //Purpose :: To delete Project RMS records when a Project is deleted
  if(Trigger.isBefore && Trigger.isDelete)
   {
      ResearcherPortalProjectTriggerHandler.deleteProjectMemberSharingRecords(Trigger.old); 
   }   
}
