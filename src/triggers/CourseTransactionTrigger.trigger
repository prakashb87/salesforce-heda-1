/********************************************************************************************************/
/**    Author : Resmi Ramakrishnan                     Date : 04/12/2018     ****************************/               
/***   Trigger handles all the events for course transaction object  *******/ 
/********************************************************************************************************/
trigger CourseTransactionTrigger on Course_Transaction__c (before insert, before update, before delete, after insert, after update, after delete, after undelete ){
   TriggerDispatcher.runTriggerLogic(new CourseTransactionTriggerHandler ());
}