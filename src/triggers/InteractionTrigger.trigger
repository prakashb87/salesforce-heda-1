trigger InteractionTrigger on Interaction__c (before insert, after insert, before update, after update) {
    new InteractionTriggerHandler().run(); 
}