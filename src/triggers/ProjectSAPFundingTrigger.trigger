/*
Author: Khushman Deomurari
RPORW-284: Integration - BI to SF
Purpose: To find record Ids to fill lookup fields
*/
trigger ProjectSAPFundingTrigger on ProjectSAPFunding__c (before insert) {
    ProjectSAPFundingTriggerHelper.onInsert(Trigger.NEW);
}