/*-----------------------------------------------------------------------
File name:      CaseTrigger.trigger
Author:         SF HEDA Team
Company:        Capgemini  
Description:    
Test Class:     CaseTrigger_Test.cls
Created Date :  16/08/2017
History

<Date>            <Authors Name>            <Brief Description of Change>
-----------------------------------------------------------------------*/

trigger CaseTrigger on Case (after insert, after update,before insert) 
{
    CaseTriggerHandler objCaseTriggerHandler = new CaseTriggerHandler();
    
    if (trigger.isInsert && trigger.isBefore)
    {
      objCaseTriggerHandler.OnBeforeInsert(trigger.new);
    }     
    else if(trigger.isInsert && trigger.isAfter)
    {                  
       
        objCaseTriggerHandler.OnAfterInsert(trigger.new); 
     }
     
     if(trigger.isUpdate  && trigger.isAfter)
     { 
       
          objCaseTriggerHandler.OnAfterUpdate(trigger.oldMap,trigger.new);
     }         
}