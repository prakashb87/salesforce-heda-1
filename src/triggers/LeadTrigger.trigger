/*****************************************************************************************************
 *** @Class             : LeadTrigger
 *** @Author            : Deployment User
 *** @Requirement       : - 
 *** @Created date      : 11/01/2018
 *** @Modified by       : Anupam Singhal
 *** @Reference 		: RM-2436
 *****************************************************************************************************/
/*****************************************************************************************************
 *** @About Class
 *** This class is trigger which will execute on insert and update of lead and the respected operations
 *** for the lead will take place
 *****************************************************************************************************/
trigger LeadTrigger on Lead(before insert, After update, After insert, before update) {
    
    	new LeadTriggerHandler().run();
    
}