/********************************************************************************************************/
/**    Author : Resmi Ramakrishnan                     Date : 27/05/2018     ****************************/               
/***   Trigger:When custom opportunity status changes to 'Closed Won' this trigger will updates related 
       Parent Opportunity status to Closed Won **********************************************************/ 
/********************************************************************************************************/

trigger CustomOpportunityTrigger on Custom_Opportunity__c (after insert, after update) 
{
     if( trigger.isAfter )
     {
        if(trigger.isUpdate /* || trigger.isInsert */)
        {
            System.debug('RRRR- inside after update/insert = ' + Trigger.new);
        	CustomOpportunityTriggerHelper.updateOpportunityStageField(Trigger.new);       
        } 
     }
}