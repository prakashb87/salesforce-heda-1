<?xml version="1.0" encoding="UTF-8"?>
<Workflow xmlns="http://soap.sforce.com/2006/04/metadata">
    <fieldUpdates>
        <fullName>Update_Role_value</fullName>
        <description>Update the role value to staff if affiliation type is Educational Institution</description>
        <field>hed__Role__c</field>
        <literalValue>Staff</literalValue>
        <name>Update Role value</name>
        <notifyAssignee>false</notifyAssignee>
        <operation>Literal</operation>
        <protected>false</protected>
    </fieldUpdates>
    <fieldUpdates>
        <fullName>Upsert_Key_Set_to_Contact_Account</fullName>
        <description>Sets the Upsert Key to Contact ID + &quot;.&quot; + Account ID</description>
        <field>UpsertKey__c</field>
        <formula>CASESAFEID(hed__Contact__r.Id)+&quot;.&quot;+CASESAFEID(hed__Account__r.Id)+ &quot;.&quot; +   RecordType.DeveloperName + &quot;.&quot; +  TEXT(hed__Role__c)</formula>
        <name>Upsert Key: Set to Contact.Account</name>
        <notifyAssignee>false</notifyAssignee>
        <operation>Formula</operation>
        <protected>false</protected>
    </fieldUpdates>
    <rules>
        <fullName>Affiliation%3A Update Upsert Key</fullName>
        <actions>
            <name>Upsert_Key_Set_to_Contact_Account</name>
            <type>FieldUpdate</type>
        </actions>
        <active>true</active>
        <description>Sets the Affiliation Upsert Key field to the Contact ID + &quot;.&quot; + Account ID for matching records through Interactions.</description>
        <formula>AND(  NOT(ISBLANK( hed__Contact__c )) , NOT(ISBLANK(  hed__Account__c  )) ,UpsertKey__c  &lt;&gt; CASESAFEID(hed__Contact__r.Id)+&quot;.&quot;+CASESAFEID(hed__Account__r.Id)+&quot;.&quot; +  RecordType.DeveloperName +&quot;.&quot; + TEXT( hed__Role__c ),  NOT(ISPICKVAL(hed__Role__c,&apos;&apos; ))  )</formula>
        <triggerType>onCreateOrTriggeringUpdate</triggerType>
    </rules>
    <rules>
        <fullName>Update Role for Affiliation</fullName>
        <actions>
            <name>Update_Role_value</name>
            <type>FieldUpdate</type>
        </actions>
        <active>true</active>
        <criteriaItems>
            <field>hed__Affiliation__c.hed__Affiliation_Type__c</field>
            <operation>equals</operation>
            <value>Educational Institution</value>
        </criteriaItems>
        <criteriaItems>
            <field>hed__Affiliation__c.RecordTypeId</field>
            <operation>equals</operation>
            <value>null</value>
        </criteriaItems>
        <description>This workflow will update the Role field value to &apos;Staff&apos; if the affiliation type is Educational Institution</description>
        <triggerType>onCreateOnly</triggerType>
    </rules>
</Workflow>
